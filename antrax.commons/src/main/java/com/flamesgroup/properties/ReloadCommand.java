/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.properties;

import com.flamesgroup.daemonext.IDaemonExtStatus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReloadCommand implements IDaemonExtStatus {

  private final List<IReloadListener> reloadListeners = new ArrayList<>();

  @Override
  public String getName() {
    return "reload";
  }

  @Override
  public String[] getDescription() {
    return new String[] {"reload configuration without reboot server"};
  }

  @Override
  public String execute(final String[] strings) {
    String result = "";
    Iterator<IReloadListener> reloadListenerIterator = reloadListeners.iterator();
    while (reloadListenerIterator.hasNext()) {
      IReloadListener reloadListener = reloadListenerIterator.next();
      if (reloadListener != null) {
        if (reloadListener.reload()) {
          result += "reloaded " + reloadListener.getName();
        } else {
          result += "can't reload" + reloadListener.getName() + ", look errors in logs";
        }
        if (reloadListenerIterator.hasNext()) {
          result += System.lineSeparator();
        }
      }
    }
    return result;
  }

  public void addReloadListener(final IReloadListener reloadListener) {
    reloadListeners.add(reloadListener);
  }

  public void removeReloadListener(final IReloadListener reloadListener) {
    reloadListeners.remove(reloadListener);
  }

}

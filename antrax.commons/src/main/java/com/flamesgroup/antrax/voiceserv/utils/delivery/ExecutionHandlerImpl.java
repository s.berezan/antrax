/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserv.utils.delivery;

public class ExecutionHandlerImpl implements ExecutionHandler {

  private volatile Throwable exception;
  private volatile Object result;
  private volatile boolean executed = false;

  @Override
  public synchronized void handleException(final Throwable e) {
    this.exception = e;
    executed = true;
    notifyAll();
  }

  @Override
  public synchronized void handleExecuted(final Object result) {
    this.result = result;
    executed = true;
    notifyAll();
  }

  public synchronized Object getResult() throws Throwable {
    while (!executed) {
      try {
        this.wait();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    if (exception != null) {
      throw exception;
    }
    return result;
  }

  @Override
  public void interrupt() {
    handleException(new RuntimeException("System is terminating"));
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.commons.impl;

import com.flamesgroup.antrax.commons.Timeout;

import java.util.Random;

public class VariableTimeout implements Timeout {

  private static final long serialVersionUID = 4346697784371780839L;
  private final Random random;
  private final long timeout;
  private final long mediana;
  private final long delta;

  public VariableTimeout(final long mediana, final long delta) {

    if (delta < 0 || delta > mediana) {
      throw new IllegalArgumentException("delta: " + delta + "mediana: " + mediana);
    }

    this.mediana = mediana;
    this.delta = delta;
    random = new Random();

    if (mediana == 0L && delta == 0L) {
      timeout = 0L;
      return;
    }
    if (delta <= 0) {
      timeout = mediana;
      return;
    }

    double minValue = mediana - delta;
    double maxValue = mediana + delta;

    double res = minValue + (maxValue - minValue) * random.nextDouble();
    timeout = Math.round(res);

  }

  @Override
  public int hashCode() {
    return (int) delta + 3 * (int) mediana;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof VariableTimeout) {
      VariableTimeout that = (VariableTimeout) obj;
      return that.delta == delta && that.mediana == mediana;
    }
    return false;
  }

  @Override
  public long getTimeout() {
    return timeout;
  }

  @Override
  public long getDelta() {
    return mediana;
  }

  @Override
  public long getMediana() {
    return delta;
  }

}

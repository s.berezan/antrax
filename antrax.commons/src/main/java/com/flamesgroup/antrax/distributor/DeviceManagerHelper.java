/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.distributor;

import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.gsmb.GSMB2Device;
import com.flamesgroup.device.gsmb.GSMB3Device;
import com.flamesgroup.device.gsmb.GSMBDevice;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.simb.SIMBDevice;

import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class DeviceManagerHelper {

  private static final String DEVICES_PROPERTIES = "devices.properties";
  private static final String NETWORK_PROPERTIES = "network.properties";
  private static final String NETMASK = "antrax.device.manager.netmask";
  private static final String GATEWAY = "antrax.device.manager.gateway";
  private static final String MASTER_IP = "antrax.device.manager.masterIP";
  private static final String BROADCAST = "antrax.device.manager.broadcast";

  private DeviceManagerHelper() {
  }

  public static InetAddress getBroadcast() throws IOException {
    Properties properties = new Properties();
    InputStream inputStream = DeviceManagerHelper.class.getClassLoader().getResourceAsStream(NETWORK_PROPERTIES);
    properties.load(inputStream);
    return InetAddress.getByName(properties.getProperty(BROADCAST));
  }

  public static Map<DeviceUID, IPConfig> parseDevicesFromProperties(final Server server, final UUID setIPConfigUUID) throws IOException {
    Map<DeviceUID, IPConfig> devicesFromProperties = new HashMap<>();
    Properties properties = new Properties();
    InputStream inputStream = DeviceManagerHelper.class.getClassLoader().getResourceAsStream(NETWORK_PROPERTIES);
    properties.load(inputStream);
    InetAddress netMask = InetAddress.getByName(properties.getProperty(NETMASK));
    InetAddress gateway = InetAddress.getByName(properties.getProperty(GATEWAY));
    InetAddress masterIP = InetAddress.getByName(properties.getProperty(MASTER_IP));

    properties.clear();
    inputStream = DeviceManagerHelper.class.getClassLoader().getResourceAsStream(DEVICES_PROPERTIES);
    properties.load(inputStream);
    for (Map.Entry<Object, Object> devicesProp : properties.entrySet()) {
      DeviceUID deviceUID = DeviceUID.valueFromCanonicalName(devicesProp.getKey().toString());
      if (!server.getType().getDeviceTypes().contains(deviceUID.getDeviceType())) {
        continue;
      }
      IPConfig ipConfig = new IPConfig(Inet4Address.getByName(devicesProp.getValue().toString()), netMask, gateway, masterIP, setIPConfigUUID);
      devicesFromProperties.put(deviceUID, ipConfig);
    }
    return devicesFromProperties;
  }

  public static int getCountOfModules(final DeviceType deviceType) {
    switch (deviceType) {
      case GSMB:
        return GSMBDevice.GSM_ENTRY_COUNT;
      case GSMB2:
        return GSMB2Device.GSM_ENTRY_COUNT;
      case GSMB3:
        return GSMB3Device.GSM_ENTRY_COUNT;
      case SIMB:
        return SIMBDevice.SIM_ENTRY_COUNT;
    }
    return 0;
  }

}

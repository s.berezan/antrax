/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyFactory {

  private ProxyFactory() {
  }

  private static class DefaultInvocationHandler implements InvocationHandler {

    private final Object delegate;

    public DefaultInvocationHandler(final Object delegate) {
      this.delegate = delegate;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      try {
        return method.invoke(delegate, args);
      } catch (InvocationTargetException e) {
        throw e.getTargetException();
      }
    }
  }

  public static Object createProxy(final Class<?> proxyInterface, final Object delegate) {
    return Proxy.newProxyInstance(proxyInterface.getClassLoader(), new Class[] {proxyInterface}, new DefaultInvocationHandler(delegate));
  }

  public static Object createProxy(final Class<?> proxyInterface, final InvocationHandler invocationHandler) {
    return Proxy.newProxyInstance(proxyInterface.getClassLoader(), new Class[] {proxyInterface}, invocationHandler);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class CompressingSocket extends Socket {

  private static final int COMPRESSED_BUFFER_SIZE = 8192;

  private InputStream in;
  private OutputStream out;

  public CompressingSocket() {
    super();
  }

  @Override
  public InputStream getInputStream() throws IOException {
    if (in == null) {
      in = new InflaterInputStream(super.getInputStream(), new Inflater(true));
    }
    return in;
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    if (out == null) {
      out = new DeflaterOutputStream(super.getOutputStream(), new Deflater(Deflater.DEFAULT_COMPRESSION, true), COMPRESSED_BUFFER_SIZE, true);
    }
    return out;
  }

}

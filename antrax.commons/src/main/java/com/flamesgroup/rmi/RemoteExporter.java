/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import com.flamesgroup.rmi.utils.RMISocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.util.Objects;

public final class RemoteExporter {

  protected static final Logger logger = LoggerFactory.getLogger(RemoteExporter.class);

  private final Object service;
  private final Class<?> serviceInterface;
  private final String serviceName;

  private final int servicePort;
  private final RMIClientSocketFactory clientSocketFactory;
  private final RMIServerSocketFactory serverSocketFactory;

  private final String registryHost;
  private final int registryPort;
  private final RMIClientSocketFactory registryClientSocketFactory;
  private final RMIServerSocketFactory registryServerSocketFactory;

  private final boolean alwaysCreateRegistry;
  private final boolean replaceExistingBinding;

  public static class Builder {

    private final Object service;
    private final Class<?> serviceInterface;
    private final String serviceName;

    private int servicePort;
    private RMIClientSocketFactory clientSocketFactory;
    private RMIServerSocketFactory serverSocketFactory;

    private String registryHost;
    private int registryPort = Registry.REGISTRY_PORT;
    private RMIClientSocketFactory registryClientSocketFactory;
    private RMIServerSocketFactory registryServerSocketFactory;

    private boolean alwaysCreateRegistry = false;
    private boolean replaceExistingBinding = true;

    public Builder(final Object service, final Class<?> serviceInterface, final String serviceName) {
      Objects.requireNonNull(service, "'service' is required");
      Objects.requireNonNull(serviceInterface, "'serviceInterface' is required");
      if (service instanceof String) {
        throw new IllegalArgumentException("service [" + service + "] is a String");
      }
      if (!serviceInterface.isInterface()) {
        throw new IllegalArgumentException("serviceInterface [" + serviceInterface + "] must be an interface");
      }
      if (!serviceInterface.isInstance(service)) {
        throw new IllegalArgumentException("service interface [" + serviceInterface.getName() +
            "] needs to be implemented by service [" + service + "] of class [" + service.getClass().getName() + "]");
      }

      this.service = service;
      this.serviceInterface = serviceInterface;
      this.serviceName = serviceName;
    }

    public Builder(final Object service, final Class<?> serviceInterface) {
      this(service, serviceInterface, serviceInterface.getSimpleName());
    }

    public Builder setServicePort(final int servicePort) {
      this.servicePort = servicePort;
      return this;
    }

    public Builder setClientSocketFactory(final RMIClientSocketFactory clientSocketFactory) {
      this.clientSocketFactory = clientSocketFactory;
      return this;
    }

    public Builder setServerSocketFactory(final RMIServerSocketFactory serverSocketFactory) {
      this.serverSocketFactory = serverSocketFactory;
      return this;
    }

    public Builder setRegistryHost(final String registryHost) {
      this.registryHost = registryHost;
      return this;
    }

    public Builder setRegistryPort(final int registryPort) {
      this.registryPort = registryPort;
      return this;
    }

    public Builder setRegistryClientSocketFactory(final RMIClientSocketFactory registryClientSocketFactory) {
      this.registryClientSocketFactory = registryClientSocketFactory;
      return this;
    }

    public Builder setRegistryServerSocketFactory(final RMIServerSocketFactory registryServerSocketFactory) {
      this.registryServerSocketFactory = registryServerSocketFactory;
      return this;
    }

    public Builder setAlwaysCreateRegistry(final boolean alwaysCreateRegistry) {
      this.alwaysCreateRegistry = alwaysCreateRegistry;
      return this;
    }

    public Builder setReplaceExistingBinding(final boolean replaceExistingBinding) {
      this.replaceExistingBinding = replaceExistingBinding;
      return this;
    }

    public RemoteExporter build() {
      if (clientSocketFactory == null && serverSocketFactory == null) {
        clientSocketFactory = RMISocketFactory.createSocketFactory();
      }
      if (clientSocketFactory instanceof RMIServerSocketFactory) {
        serverSocketFactory = (RMIServerSocketFactory) clientSocketFactory;
      }
      if ((clientSocketFactory != null && serverSocketFactory == null) ||
          (clientSocketFactory == null && serverSocketFactory != null)) {
        throw new IllegalArgumentException("Both RMIClientSocketFactory and RMIServerSocketFactory or none required");
      }
      if (clientSocketFactory instanceof RMISocketFactory) {
        RMISocketFactory rmiSocketFactory = (RMISocketFactory) clientSocketFactory;
        logger.debug("{}[{}:{}] - compressing socket streams {}",
            new Object[] {getClass().getSimpleName(), serviceName, serviceInterface.getCanonicalName(), rmiSocketFactory.isCompressing() ? "on" : "off"});
      }

      if (registryClientSocketFactory instanceof RMIServerSocketFactory) {
        registryServerSocketFactory = (RMIServerSocketFactory) registryClientSocketFactory;
      }
      if (registryClientSocketFactory == null && registryServerSocketFactory != null) {
        throw new IllegalArgumentException("RMIServerSocketFactory without RMIClientSocketFactory for registry not supported");
      }
      return new RemoteExporter(this);
    }

  }

  private RemoteExporter(final Builder builder) {
    this.service = builder.service;
    this.serviceInterface = builder.serviceInterface;
    this.serviceName = builder.serviceName;
    this.servicePort = builder.servicePort;
    this.clientSocketFactory = builder.clientSocketFactory;
    this.serverSocketFactory = builder.serverSocketFactory;
    this.registryHost = builder.registryHost;
    this.registryPort = builder.registryPort;
    this.registryClientSocketFactory = builder.registryClientSocketFactory;
    this.registryServerSocketFactory = builder.registryServerSocketFactory;
    this.alwaysCreateRegistry = builder.alwaysCreateRegistry;
    this.replaceExistingBinding = builder.replaceExistingBinding;
  }

  public Object getService() {
    return service;
  }

  public Class<?> getServiceInterface() {
    return serviceInterface;
  }

  public String getServiceName() {
    return serviceName;
  }

  public int getServicePort() {
    return servicePort;
  }

  public RMIClientSocketFactory getClientSocketFactory() {
    return clientSocketFactory;
  }

  public RMIServerSocketFactory getServerSocketFactory() {
    return serverSocketFactory;
  }

  public String getRegistryHost() {
    return registryHost;
  }

  public int getRegistryPort() {
    return registryPort;
  }

  public RMIClientSocketFactory getRegistryClientSocketFactory() {
    return registryClientSocketFactory;
  }

  public RMIServerSocketFactory getRegistryServerSocketFactory() {
    return registryServerSocketFactory;
  }

  public boolean isAlwaysCreateRegistry() {
    return alwaysCreateRegistry;
  }

  public boolean isReplaceExistingBinding() {
    return replaceExistingBinding;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import com.flamesgroup.rmi.utils.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class RemoteServiceExporter {

  private static final Logger logger = LoggerFactory.getLogger(RemoteServiceExporter.class);

  private static final List<RemoteService> remoteServices = new ArrayList<>();

  private RemoteServiceExporter() {
  }

  public static void export(final RemoteExporter remoteExporter) {
    RemoteService remoteService = new RemoteService(remoteExporter);
    try {
      remoteService.export();
      remoteServices.add(remoteService);
    } catch (RemoteException e) {
      logger.error("[{}] - couldn't export rmi service '{}'", RemoteServiceExporter.class, remoteExporter.getServiceName(), e);
    }
  }

  public static void destroyAll() {
    Iterator<RemoteService> iterator = remoteServices.iterator();
    while (iterator.hasNext()) {
      RemoteService remoteService = iterator.next();
      try {
        remoteService.destroy();
      } catch (RemoteException e) {
        logger.error("[{}] - couldn't destroy export rmi service for '{}'", RemoteServiceExporter.class, remoteService.remoteExporter.getServiceName(), e);
      }
      iterator.remove();
    }
  }

  private static class RemoteService {

    private static final Logger logger = LoggerFactory.getLogger(RemoteService.class);

    private final RemoteExporter remoteExporter;

    private Registry registry;
    private Remote exportedObject;

    private RemoteService(final RemoteExporter remoteExporter) {
      this.remoteExporter = remoteExporter;
    }

    private void export() throws RemoteException {
      registry = getRegistry(remoteExporter.getRegistryHost(), remoteExporter.getRegistryPort(),
          remoteExporter.getRegistryClientSocketFactory(), remoteExporter.getRegistryServerSocketFactory());

      exportedObject = getObjectToExport();
      if (logger.isInfoEnabled()) {
        logger.info("[{}] - binding service '{}' to RMI registry: {}", this, remoteExporter.getServiceName(), registry);
      }

      if (remoteExporter.getClientSocketFactory() != null) {
        UnicastRemoteObject.exportObject(exportedObject, remoteExporter.getServicePort(),
            remoteExporter.getClientSocketFactory(), remoteExporter.getServerSocketFactory());
      } else {
        UnicastRemoteObject.exportObject(exportedObject, remoteExporter.getServicePort());
      }

      try {
        if (remoteExporter.isReplaceExistingBinding()) {
          registry.rebind(remoteExporter.getServiceName(), exportedObject);
        } else {
          registry.bind(remoteExporter.getServiceName(), exportedObject);
        }
      } catch (AlreadyBoundException ex) {
        unexportObject();
        throw new IllegalStateException("Already an RMI object bound for name '" + remoteExporter.getServiceName() + "': " + ex.toString());
      } catch (RemoteException ex) {
        unexportObject();
        throw ex;
      }
    }

    private Registry getRegistry(final String registryHost, final int registryPort, final RMIClientSocketFactory clientSocketFactory,
        final RMIServerSocketFactory serverSocketFactory) throws RemoteException {
      if (registryHost != null) {
        if (logger.isInfoEnabled()) {
          logger.info("[{}] - looking for RMI registry at port '{}' of host [{}]", this, registryPort, registryHost);
        }
        Registry reg = LocateRegistry.getRegistry(registryHost, registryPort, clientSocketFactory);
        testRegistry(reg);
        return reg;
      } else {
        return getRegistry(registryPort, clientSocketFactory, serverSocketFactory);
      }
    }

    private Registry getRegistry(final int registryPort, final RMIClientSocketFactory clientSocketFactory, final RMIServerSocketFactory serverSocketFactory)
        throws RemoteException {
      if (clientSocketFactory != null) {
        if (remoteExporter.isAlwaysCreateRegistry()) {
          logger.info("[{}] - creating new RMI registry", this);
          return LocateRegistry.createRegistry(registryPort, clientSocketFactory, serverSocketFactory);
        }
        if (logger.isInfoEnabled()) {
          logger.info("[{}] - looking for RMI registry at port '{}', using custom socket factory", this, registryPort);
        }
        try {
          Registry reg = LocateRegistry.getRegistry(null, registryPort, clientSocketFactory);
          testRegistry(reg);
          return reg;
        } catch (RemoteException ex) {
          logger.debug("[{}] - RMI registry access threw exception", this, ex);
          logger.info("[{}] - could not detect RMI registry - creating new one", this);
          return LocateRegistry.createRegistry(registryPort, clientSocketFactory, serverSocketFactory);
        }
      } else {
        return getRegistry(registryPort);
      }
    }

    private Registry getRegistry(final int registryPort) throws RemoteException {
      if (remoteExporter.isAlwaysCreateRegistry()) {
        logger.info("[{}] - creating new RMI registry", this);
        return LocateRegistry.createRegistry(registryPort);
      }
      if (logger.isInfoEnabled()) {
        logger.info("[{}] - looking for RMI registry at port '{}'", this, registryPort);
      }
      try {
        Registry reg = LocateRegistry.getRegistry(registryPort);
        testRegistry(reg);
        return reg;
      } catch (RemoteException ex) {
        logger.debug("[{}] - RMI registry access threw exception", this, ex);
        logger.info("[{}] - could not detect RMI registry - creating new one", this);
        return LocateRegistry.createRegistry(registryPort);
      }
    }

    private void testRegistry(final Registry registry) throws RemoteException {
      registry.list();
    }

    private Remote getObjectToExport() {
      if (remoteExporter.getService() instanceof Remote &&
          (remoteExporter.getServiceInterface() == null || Remote.class.isAssignableFrom(remoteExporter.getServiceInterface()))) {
        return (Remote) remoteExporter.getService();
      } else {
        if (logger.isDebugEnabled()) {
          logger.debug("[{}] - RMI service [{}] is an RMI invoker", this, remoteExporter.getService());
        }
        return new RemoteInvocationWrapper(ProxyFactory.createProxy(remoteExporter.getServiceInterface(), remoteExporter.getService()));
      }
    }

    public void destroy() throws RemoteException {
      if (logger.isInfoEnabled()) {
        logger.info("[{}] - unbinding RMI service '{}' from registry at port '{}'", this, remoteExporter.getServiceName(), remoteExporter.getRegistryPort());
      }
      try {
        this.registry.unbind(remoteExporter.getServiceName());
      } catch (NotBoundException ex) {
        if (logger.isWarnEnabled()) {
          logger.warn("[{}] - RMI service '{}' is not bound to registry at port '{}' anymore", this, remoteExporter.getServiceName(), remoteExporter.getRegistryPort(), ex);
        }
      } finally {
        unexportObject();
      }
    }

    private void unexportObject() {
      try {
        UnicastRemoteObject.unexportObject(this.exportedObject, true);
      } catch (NoSuchObjectException ex) {
        if (logger.isWarnEnabled()) {
          logger.warn("[{}] - RMI object for service '{}' isn't exported anymore", remoteExporter.getServiceName(), ex);
        }
      }
    }

  }

}

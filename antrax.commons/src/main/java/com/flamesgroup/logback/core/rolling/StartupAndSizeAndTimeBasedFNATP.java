/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.logback.core.rolling;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import ch.qos.logback.core.rolling.RolloverFailure;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.helper.ArchiveRemover;
import ch.qos.logback.core.rolling.helper.PrimaryTokenFilenamePattern;
import ch.qos.logback.core.rolling.helper.SizeAndTimeBasedArchiveRemover;

import java.io.File;

@NoAutoStart
public class StartupAndSizeAndTimeBasedFNATP<E> extends SizeAndTimeBasedFNATP<E> {

  @Override
  public void start() {
    super.start();
    nextCheck = 0L;
    isTriggeringEvent(null, null);
    File logFile = new File(tbrp.getActiveFileName());
    if (logFile.length() > 0) {
      try {
        tbrp.rollover();
      } catch (RolloverFailure ignored) {
        // if log file doesn't exist, that's fine, just continue
      }
    }
  }

  /**
   * Replace default FileNamePattern class to hacked PrimaryTokenFilenamePattern class
   */
  @Override
  protected ArchiveRemover createArchiveRemover() {
    return new SizeAndTimeBasedArchiveRemover(new PrimaryTokenFilenamePattern(tbrp.getFileNamePattern(), context), rc);
  }

}

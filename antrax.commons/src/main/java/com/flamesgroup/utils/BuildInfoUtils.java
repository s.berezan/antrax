/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

public final class BuildInfoUtils {

  private static final Logger logger = LoggerFactory.getLogger(BuildInfoUtils.class);

  private static final String PROJECT_VERSION = "Project-Version";
  private static final String BUILD_TIME = "Build-Time";
  private static final String BUILD_GIT_SHA = "Build-Git-SHA";
  private static final String BUILD_GIT_BRANCH = "Build-Git-Branch";
  private static final String BUILD_JDK = "Build-Jdk";

  private BuildInfoUtils() {
  }

  public static BuildInfo readBuildInfo(final Class aClass) {
    if (!aClass.getResource(aClass.getSimpleName() + ".class").toString().startsWith("jar:")) {
      logger.info("[{}] - run from IDE", BuildInfoUtils.class);
      return null;
    }

    URL location = aClass.getProtectionDomain().getCodeSource().getLocation();
    try (JarInputStream jis = new JarInputStream(location.openStream())) {
      Manifest manifest = jis.getManifest();
      Attributes mainAttributes = manifest.getMainAttributes();
      String projectVersion = mainAttributes.getValue(PROJECT_VERSION);
      String buildTime = mainAttributes.getValue(BUILD_TIME);
      String buildGitSHA = mainAttributes.getValue(BUILD_GIT_SHA);
      String buildGitBranch = mainAttributes.getValue(BUILD_GIT_BRANCH);
      String buildJdk = mainAttributes.getValue(BUILD_JDK);
      return new BuildInfo(projectVersion, buildTime, buildGitSHA, buildGitBranch, buildJdk);
    } catch (IOException e) {
      logger.error("[{}] - can't read file [{}]", BuildInfoUtils.class, location, e);
    }
    return null;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.Objects;

public class SmsRouteConfig implements Serializable {

  private static final long serialVersionUID = 4260399138778085405L;

  private final String dnisRegex;
  private final int priority;

  public SmsRouteConfig(final String dnisRegex, final int priority) {
    Objects.requireNonNull(dnisRegex, "dnisRegex mustn't be null");
    this.dnisRegex = dnisRegex;
    this.priority = priority;
  }

  public String getDnisRegex() {
    return dnisRegex;
  }

  public int getPriority() {
    return priority;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SmsRouteConfig)) {
      return false;
    }
    final SmsRouteConfig that = (SmsRouteConfig) object;

    return priority == that.priority
        && Objects.equals(dnisRegex, that.dnisRegex);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(dnisRegex);
    result = prime * result + priority;
    return result;
  }

}

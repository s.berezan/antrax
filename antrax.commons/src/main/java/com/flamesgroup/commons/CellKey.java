/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;

public final class CellKey implements Comparable<CellKey>, Serializable {

  private static final long serialVersionUID = 1571640754400602750L;

  private final int lac;
  private final int cellId;
  private final int bsic;
  private final int arfcn;

  public CellKey(final int lac, final int cellId, final int bsic, final int arfcn) {
    this.lac = lac;
    this.cellId = cellId;
    this.bsic = bsic;
    this.arfcn = arfcn;
  }

  public int getLac() {
    return lac;
  }

  public int getCellId() {
    return cellId;
  }

  public int getBsic() {
    return bsic;
  }

  public int getArfcn() {
    return arfcn;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellKey)) {
      return false;
    }
    CellKey that = (CellKey) object;

    return lac == that.lac && cellId == that.cellId && bsic == that.bsic && arfcn == that.arfcn;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = lac;
    result = prime * result + cellId;
    result = prime * result + bsic;
    result = prime * result + arfcn;
    return result;
  }

  @Override
  public int compareTo(final CellKey o) {
    int cmp = Integer.compare(lac, o.lac);
    if (cmp == 0) {
      cmp = Integer.compare(cellId, o.cellId);
      if (cmp == 0) {
        cmp = Integer.compare(bsic, o.bsic);
        if (cmp == 0) {
          cmp = Integer.compare(arfcn, o.arfcn);
        }
      }
    }
    return cmp;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode()));
    sb.append("[lac:").append(lac);
    sb.append(" cellId:").append(cellId);
    sb.append(" bsic:").append(bsic);
    sb.append(" arfcn:").append(arfcn);
    sb.append(']');
    return sb.toString();
  }

}

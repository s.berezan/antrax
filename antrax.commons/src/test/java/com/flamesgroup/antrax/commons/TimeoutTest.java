/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.commons.impl.VariableTimeout;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class TimeoutTest {

  @Test(expected = IllegalArgumentException.class)
  public void testTimeoutInvariable() {
    Timeout timeout = new InvariableTimeout(50, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTimeoutInvariable2() {
    Timeout timeout = new InvariableTimeout(50, -2);
  }

  @Test
  public void testGetTimeoutInvariable() {

    long mediana = 30000;
    long delta = 2000;
    int size = 10000;
    long guessRes = mediana + delta - (mediana - delta);
    long resArr[] = new long[size];
    List<Long> duplicate = new LinkedList<>();

    Timeout timeout = new InvariableTimeout(mediana, delta);

    for (int i = 0; i < size; i++) {
      long res = timeout.getTimeout();
      assertTrue(Integer.toString(i) + Long.toString(res), res <= mediana + delta);
      assertTrue(Integer.toString(i) + Long.toString(res), res >= mediana - delta);
      for (int j = 0; j < i; j++) {
        if (resArr[j] == res) {
          duplicate.add(res);
          break;
        }
      }
      resArr[i] = res;
    }
    assertTrue(Integer.toString(duplicate.size()), (size - guessRes) * 1.1 > duplicate.size());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTimeoutVariable() {
    Timeout timeout = new VariableTimeout(50, 100);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTimeoutVariable2() {
    Timeout timeout = new VariableTimeout(50, -2);
  }

  @Test
  public void testGetTimeoutVariable() {

    long mediana = 30000;
    long delta = 2000;

    Timeout timeoutA = new VariableTimeout(mediana, delta);
    Timeout timeoutB = new VariableTimeout(mediana, delta);
    Timeout timeoutC = new VariableTimeout(mediana, delta);

    assertEquals(timeoutA.getTimeout(), timeoutA.getTimeout());

    assertFalse(timeoutA.getTimeout() == timeoutB.getTimeout());
    assertFalse(timeoutA.getTimeout() == timeoutC.getTimeout());
    assertFalse(timeoutB.getTimeout() == timeoutC.getTimeout());
  }

}

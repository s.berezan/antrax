/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PairTest {

  private String strA = "A";
  private String strB = "B";
  private Integer intA = 23;
  private Integer intB = 48;

  private Pair<Integer, String> pairA;
  private Pair<String, Integer> pairB;
  private Pair<Integer, Integer> pairC;

  @Before
  public void setUpBefore() {
    strA = "A";
    strB = "B";
    intA = 23;
    intB = 48;

    pairA = new Pair<>(intA, strA);
    pairB = new Pair<>(strB, intB);
    pairC = new Pair<>(intA, intB);
  }

  @Test
  public void testFirstSecond() {

    assertEquals(pairA.first(), intA);
    assertEquals(pairA.second(), strA);

    assertFalse(pairA.first().equals(intB));
    assertFalse(pairA.second().equals(strB));
  }

  @Test
  public void testEquals() {
    assertTrue(pairA.equals(pairA));
    assertTrue(pairA.equals(new Pair<>(intA, strA)));
    assertFalse(pairA.equals(new Pair<>(strA, intA)));
    assertFalse(pairA.equals(pairB));
    assertFalse(pairA.equals(pairC));
    assertFalse(pairA.equals(new Integer(6)));
  }

  @Test
  public void testHashCode() {
    assertEquals(pairA.hashCode(), pairA.hashCode());
    assertEquals(pairA.hashCode(), new Pair<>(intA, strA).hashCode());
    assertFalse(pairA.hashCode() == pairB.hashCode());
    assertFalse(pairA.hashCode() == pairC.hashCode());
    assertFalse(pairA.hashCode() == new Pair<>(strA, intA).hashCode());
    assertFalse(pairC.hashCode() == new Integer(23).hashCode());
  }

  @Test
  public void testToString() {
    assertEquals("[23:A]", new Pair<>(23, "A").toString());
    assertEquals("[A:B]", new Pair<>("A", "B").toString());
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.unit.PhoneNumber;
import org.junit.Test;

public class IMEITest {

  @Test
  public void testIMEIGeneratedProperly() throws IMEIException {
    IMEIPattern pattern = new IMEIPattern("123X4567890123");
    assertTrue(IMEI.generate(pattern, null).getValue().matches("123\\d4567890123"));
    pattern = new IMEIPattern("XXXXXXXXXXXXXX");
    assertTrue(IMEI.generate(pattern, null).getValue().matches("\\d{14}"));
    assertFalse((IMEI.generate(pattern, null).equals(IMEI.generate(pattern, null))));
    assertArrayEquals(new byte[] {0x31, 0x32, 0x33, 0x34, 0x35, 0x36}, IMEI.valueOf("12345678901234").getTac());
    assertArrayEquals(new byte[] {0x37, 0x38}, IMEI.valueOf("12345678901234").getFac());
    assertArrayEquals(new byte[] {0x39, 0x30, 0x31, 0x32, 0x33, 0x34}, IMEI.valueOf("12345678901234").getSnr());

    assertArrayEquals(new byte[] {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34}, IMEI.valueOf("12345678901234").toByteArray());
  }

  @Test
  public void generateIMEIFromPhoneNumber() throws IMEIException {
    PhoneNumber phoneNumber = new PhoneNumber("+380936023732");
    IMEIPattern imeiPattern = new IMEIPattern("35X(N3-5)XXXXXXX(N4+8)XX");

    IMEI imei = IMEI.generate(imeiPattern, phoneNumber);
    assertEquals(imei.getValue().charAt(3), '2');
    assertEquals(imei.getValue().charAt(11), '0');
  }

  @Test(expected = IMEIException.class)
  public void generateIMEIFromNullPhoneNumber() throws IMEIException {
    IMEIPattern imeiPattern = new IMEIPattern("35X(N3-5)XXXXXXX(N4+8)XX");

    IMEI imei = IMEI.generate(imeiPattern, null);
    assertEquals(imei.getValue().charAt(3), '2');
    assertEquals(imei.getValue().charAt(11), '0');
  }

  @Test(expected = IMEIException.class)
  public void generateIMEIWithBadPatternPhoneNumber() throws IMEIException {
    PhoneNumber phoneNumber = new PhoneNumber("6023732");
    IMEIPattern imeiPattern = new IMEIPattern("35X(N9-5)XXXXXXX(N4+8)XX");

    IMEI imei = IMEI.generate(imeiPattern, phoneNumber);
    assertEquals(imei.getValue().charAt(3), '2');
    assertEquals(imei.getValue().charAt(11), '0');
  }

}

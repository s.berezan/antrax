CREATE OR REPLACE FUNCTION tr_set_curr_rev()
  RETURNS trigger AS
$BODY$
DECLARE
    sql_str text;
    next_rev bigint;
BEGIN
    next_rev = get_next_rev(TG_TABLE_NAME);
    IF(TG_TABLE_NAME = 'link_with_sim_group' or TG_TABLE_NAME = 'server') then
     sql_str = 'update revisions set '||TG_TABLE_NAME|| ' = ' ||next_rev + 1||';';
     execute sql_str;
    end if;
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        NEW.rev = next_rev;
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

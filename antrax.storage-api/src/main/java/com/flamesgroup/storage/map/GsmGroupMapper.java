/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GSMGroupImpl;
import com.flamesgroup.storage.jooq.tables.records.GsmGroupRecord;

import java.util.Objects;

public final class GsmGroupMapper {

  private GsmGroupMapper() {
  }

  public static GSMGroup mapGsmGroupRecordToGsmGroup(final GsmGroupRecord gsmGroupRecord) {
    Objects.requireNonNull(gsmGroupRecord, "gsmGroupRecord mustn't be null");

    GSMGroupImpl gsmGroup = new GSMGroupImpl(gsmGroupRecord.getId());
    gsmGroup.setName(gsmGroupRecord.getName());
    return gsmGroup;
  }

  public static void mapGsmGroupToGsmGroupRecord(final GSMGroup gsmGroup, final GsmGroupRecord gsmGroupRecord) {
    Objects.requireNonNull(gsmGroup, "gsmGroup mustn't be null");
    Objects.requireNonNull(gsmGroupRecord, "gsmGroupRecord mustn't be null");

    gsmGroupRecord.setId(gsmGroup.getID());
    gsmGroupRecord.setName(gsmGroup.getName());
  }

}

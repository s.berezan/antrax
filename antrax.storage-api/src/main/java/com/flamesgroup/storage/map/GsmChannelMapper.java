/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.storage.jooq.tables.records.GsmChannelRecord;
import com.flamesgroup.storage.jooq.tables.records.GsmGroupRecord;

import java.util.Objects;

public final class GsmChannelMapper {

  private GsmChannelMapper() {
  }

  public static GSMChannel mapGsmChannelRecordToGsmChannel(final GsmChannelRecord gsmChannelRecord, final GsmGroupRecord gsmGroupRecord) {
    Objects.requireNonNull(gsmChannelRecord, "gsmChannelRecord mustn't be null");

    GSMGroup gsmGroup = gsmGroupRecord.getId() == null ? new GsmNoGroup() : GsmGroupMapper.mapGsmGroupRecordToGsmGroup(gsmGroupRecord);

    return mapGsmChannelRecordToGsmChannel(gsmChannelRecord, gsmGroup);
  }

  public static GSMChannel mapGsmChannelRecordToGsmChannel(final GsmChannelRecord gsmChannelRecord, final GSMGroup gsmGroup) {
    Objects.requireNonNull(gsmChannelRecord, "gsmChannelRecord mustn't be null");

    GSMChannelImpl gsmChannel = new GSMChannelImpl();
    gsmChannel.setID(gsmChannelRecord.getId());
    return gsmChannel.setGsmGroup(gsmGroup)
        .setChannelUID(new ChannelUID(new DeviceUID(gsmChannelRecord.getDeviceUid()), gsmChannelRecord.getChannelNum().byteValue()))
        .setLock(gsmChannelRecord.getLock()).setLockReason(gsmChannelRecord.getLockReason())
        .setLockTime(gsmChannelRecord.getLockTime() == null ? 0 : gsmChannelRecord.getLockTime().getTime())
        .setLockArfcn(gsmChannelRecord.getLockArfcn());
  }

  public static void mapGsmChannelToGsmChannelRecord(final GSMChannel gsmChannel, final GsmChannelRecord gsmChannelRecord) {
    Objects.requireNonNull(gsmChannel, "gsmChannel mustn't be null");
    Objects.requireNonNull(gsmChannelRecord, "gsmChannelRecord mustn't be null");

    gsmChannelRecord.setGsmGroupId(gsmChannel.getGSMGroup().getID() > 0 ? gsmChannel.getGSMGroup().getID() : null)
        .setChannelNum((short) gsmChannel.getChannelUID().getChannelNumber())
        .setDeviceUid(gsmChannel.getChannelUID().getDeviceUID().getUID())
        .setLockArfcn(gsmChannel.getLockArfcn());
  }

}

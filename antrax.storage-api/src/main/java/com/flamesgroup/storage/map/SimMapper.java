/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.storage.jooq.tables.records.SimRecord;

import java.util.Date;
import java.util.Objects;

public final class SimMapper {

  private SimMapper() {
  }

  public static SimData mapSimRecordToSimData(final SimRecord simRecord, final SIMGroup simGroup) {
    Objects.requireNonNull(simRecord, "simRecord mustn't be null");

    return new SimData(simRecord.getId())
        .setUid(simRecord.getIccid())
        .setCallDuration(simRecord.getSuccessfulCallDuration())
        .setEnabled(simRecord.getEnabled())
        .setImei(simRecord.getImei())
        .setIncomingCallDuration(simRecord.getIncomingSuccessfulCallDuration())
        .setIncomingSuccessfulCallsCount(simRecord.getIncomingSuccessfulCallCount())
        .setIncomingTotalCallsCount(simRecord.getIncomingCallCount())
        .setLockReason(simRecord.getLockReason())
        .setLockTime(simRecord.getLockTime() == null ? 0 : simRecord.getLockTime().getTime())
        .setLocked(simRecord.getLocked())
        .setPhoneNumber(simRecord.getPhoneNumber())
        .setSimGroup(simGroup == null ? new SimNoGroup() : simGroup)
        .setSuccessfulCallsCount(simRecord.getSuccessfulCallCount())
        .setTotalCallsCount(simRecord.getAllCallCount())
        .setSuccessOutgoingSmsCount(simRecord.getSuccessSmsCount())
        .setTotalOutgoingSmsCount(simRecord.getTotalSmsCount())
        .setCountSmsStatuses(simRecord.getTotalSmsStatuses())
        .setIncomingSMSCount(simRecord.getIncomingSmsCount())
        .setNote(simRecord.getNote())
        .setAllowInternet(simRecord.getAllowedInternet())
        .setTariffPlanEndDate(simRecord.getTariffPlanEndDate())
        .setBalance(simRecord.getBalance());
  }

  public static void mapSimDataToSimRecord(final SimData simData, final SimRecord simRecord) {
    Objects.requireNonNull(simRecord, "simData mustn't be null");
    Objects.requireNonNull(simRecord, "simRecord mustn't be null");

    simRecord.setIccid(simData.getUid())
        .setSuccessfulCallDuration(simData.getCallDuration())
        .setEnabled(simData.isEnabled())
        .setImei(simData.getImei())
        .setIncomingSuccessfulCallDuration(simData.getIncomingCallDuration())
        .setIncomingSuccessfulCallCount(simData.getIncomingSuccessfulCallsCount())
        .setIncomingCallCount(simData.getIncomingSuccessfulCallsCount())
        .setLockReason(simData.getLockReason())
        .setLocked(simData.isLocked())
        .setLockTime(new Date(simData.getLockTime()))
        .setPhoneNumber(simData.getPhoneNumber());
    if (simData.getSimGroup() != null) {
      simRecord.setSimGroupId(simData.getSimGroup().getID());
    }
    simRecord.setSuccessfulCallCount(simData.getSuccessfulCallsCount())
        .setAllCallCount(simData.getTotalCallsCount())
        .setNote(simData.getNote())
        .setTariffPlanEndDate(simData.getTariffPlanEndDate())
        .setBalance(simData.getBalance());
  }

}

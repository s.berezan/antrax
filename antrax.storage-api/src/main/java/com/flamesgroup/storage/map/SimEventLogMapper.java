/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.storage.jooq.tables.records.SimEventLogRecord;

import java.util.Objects;

public final class SimEventLogMapper {

  private SimEventLogMapper() {
  }

  public static SIMEventRec mapSimEventLogRecordToSimEventLog(final SimEventLogRecord simEventLogRecord) {
    Objects.requireNonNull(simEventLogRecord, "simEventLogRecord mustn't be null");

    return new SIMEventRec(simEventLogRecord.getId())
        .setSimUid(simEventLogRecord.getIccid())
        .setEvent(simEventLogRecord.getEvent())
        .setArgs(simEventLogRecord.getArgs())
        .setStartTime(simEventLogRecord.getStartTime());
  }

  public static void mapSimEventLogToSimEventLogRecord(final SIMEventRec simEventRec, final SimEventLogRecord simEventLogRecord) {
    Objects.requireNonNull(simEventRec, "simEventRec mustn't be null");
    Objects.requireNonNull(simEventLogRecord, "simEventLogRecord mustn't be null");

    simEventLogRecord.setIccid(simEventRec.getSimUid())
        .setEvent(simEventRec.getEvent())
        .setArgs(simEventRec.getArgs())
        .setStartTime(simEventRec.getStartTime());
  }

}

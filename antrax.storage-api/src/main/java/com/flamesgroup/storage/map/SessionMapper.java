/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.SessionParams;
import com.flamesgroup.storage.jooq.tables.records.SessionRecord;

import java.util.Objects;

public final class SessionMapper {

  private SessionMapper() {
  }

  public static SessionParams mapSessionRecordToSessionParams(final SessionRecord sessionRecord) {
    Objects.requireNonNull(sessionRecord, "sessionRecord mustn't be null");

    return new SessionParams().setClientUid(sessionRecord.getClientUid())
        .setUserName(sessionRecord.getUserName())
        .setGroup(sessionRecord.getGroup())
        .setIp(sessionRecord.getIp())
        .setSessionStartTime(sessionRecord.getStartTime())
        .setSessionLastAccessTime(sessionRecord.getLastAccessTime())
        .setSessionEndTime(sessionRecord.getEndTime());
  }

  public static void mapSessionParamsToSessionRecord(final SessionParams sessionParams, final SessionRecord sessionRecord) {
    Objects.requireNonNull(sessionParams, "sessionParams mustn't be null");
    Objects.requireNonNull(sessionRecord, "sessionRecord mustn't be null");

    sessionRecord.setClientUid(sessionParams.getClientUid())
        .setUserName(sessionParams.getUserName())
        .setGroup(sessionParams.getGroup())
        .setIp(sessionParams.getIp())
        .setStartTime(sessionParams.getSessionStartTime())
        .setLastAccessTime(sessionParams.getSessionLastAccessTime())
        .setEndTime(sessionParams.getSessionEndTime());
  }

}

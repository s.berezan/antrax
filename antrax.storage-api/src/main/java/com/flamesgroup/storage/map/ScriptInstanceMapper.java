/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.automation.meta.ScriptField;
import com.flamesgroup.antrax.automation.meta.ScriptInstanceImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameterImpl;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.storage.jooq.tables.records.ScriptInstanceRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamRecord;

import java.util.Objects;

public final class ScriptInstanceMapper {

  private ScriptInstanceMapper() {
  }

  public static void mapScriptInstanceToScriptInstanceRecord(final ScriptInstanceImpl scriptInstance, final SIMGroup simGroup, final int fieldIndex,
      final ScriptField field, final ScriptInstanceRecord scriptInstanceRecord) {
    Objects.requireNonNull(scriptInstance, "scriptInstance mustn't be null");
    Objects.requireNonNull(simGroup, "simGroup mustn't be null");
    Objects.requireNonNull(field, "field mustn't be null");
    Objects.requireNonNull(scriptInstanceRecord, "scriptInstanceRecord mustn't be null");
    scriptInstanceRecord.setScriptDefId(scriptInstance.getDefinition().getID())
        .setSimGroupId(simGroup.getID())
        .setField(field).setFieldIndex((long) fieldIndex);
  }

  public static void mapScriptParameterToScriptParameterRecord(final ScriptParameterImpl scriptParameter, final ScriptParamRecord scriptParamRecord) {
    Objects.requireNonNull(scriptParameter, "scriptParameter mustn't be null");
    Objects.requireNonNull(scriptParamRecord, "scriptParamRecord mustn't be null");
    scriptParamRecord.setScriptInstanceId(((ScriptInstanceImpl) scriptParameter.getInstance()).getID())
        .setScriptParamDefId(((ScriptParameterDefinitionImpl) scriptParameter.getDefinition()).getID())
        .setValue(scriptParameter.getSerializedValue() == null ? new byte[0] : scriptParameter.getSerializedValue());
  }

}

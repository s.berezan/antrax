/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.commons.voipantispam.BlackListConfig;
import com.flamesgroup.commons.voipantispam.FasConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamConfigurationRecord;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Objects;

public final class VoipAntiSpamConfigurationMapper {

  private VoipAntiSpamConfigurationMapper() {
  }

  public static VoipAntiSpamConfiguration mapVoipAntiSpamConfigurationRecordToVoipAntiSpamConfiguration(final VoipAntiSpamConfigurationRecord voipAntiSpamConfigurationRecord) {
    Objects.requireNonNull(voipAntiSpamConfigurationRecord, "voipAntiSpamConfigurationRecord mustn't be null");
    return new VoipAntiSpamConfiguration()
        .setEnable(voipAntiSpamConfigurationRecord.getEnable())
        .setGrayListConfigs(new Gson().fromJson(voipAntiSpamConfigurationRecord.getGrayListConfig(), new TypeToken<List<GrayListConfig>>() {
        }.getType()))
        .setBlackListConfigs(new Gson().fromJson(voipAntiSpamConfigurationRecord.getBlackListConfig(), new TypeToken<List<BlackListConfig>>() {
        }.getType()))
        .setGsmAlertingAnalyze(voipAntiSpamConfigurationRecord.getGsmAlertingAnalyzeEnable())
        .setGsmAlertingConfigs(new Gson().fromJson(voipAntiSpamConfigurationRecord.getGsmAlertingConfig(), new TypeToken<List<GsmAlertingConfig>>() {
        }.getType()))
        .setVoipAlertingAnalyze(voipAntiSpamConfigurationRecord.getVoipAlertingAnalyzeEnable())
        .setVoipAlertingConfigs(new Gson().fromJson(voipAntiSpamConfigurationRecord.getVoipAlertingConfig(), new TypeToken<List<VoipAlertingConfig>>() {
        }.getType()))
        .setFasAnalyze(voipAntiSpamConfigurationRecord.getFasAnalyzeEnable())
        .setFasConfigs(new Gson().fromJson(voipAntiSpamConfigurationRecord.getFasConfig(), new TypeToken<List<FasConfig>>() {
        }.getType()));
  }

  public static void mapVoipAntiSpamConfigurationToVoipAntiSpamConfigurationRecord(final VoipAntiSpamConfiguration voipAntiSpamConfiguration,
      final VoipAntiSpamConfigurationRecord voipAntiSpamConfigurationRecord) {
    Objects.requireNonNull(voipAntiSpamConfiguration, "voipAntiSpamConfiguration mustn't be null");
    Objects.requireNonNull(voipAntiSpamConfigurationRecord, "voipAntiSpamConfigurationRecord mustn't be null");
    voipAntiSpamConfigurationRecord
        .setEnable(voipAntiSpamConfiguration.isEnable())
        .setGrayListConfig(new Gson().toJsonTree(voipAntiSpamConfiguration.getGrayListConfigs(), new TypeToken<List<GrayListConfig>>() {
        }.getType()))
        .setBlackListConfig(new Gson().toJsonTree(voipAntiSpamConfiguration.getBlackListConfigs(), new TypeToken<List<BlackListConfig>>() {
        }.getType()))
        .setGsmAlertingAnalyzeEnable(voipAntiSpamConfiguration.isGsmAlertingAnalyze())
        .setGsmAlertingConfig(new Gson().toJsonTree(voipAntiSpamConfiguration.getGsmAlertingConfigs(), new TypeToken<List<GsmAlertingConfig>>() {
        }.getType()))
        .setVoipAlertingAnalyzeEnable(voipAntiSpamConfiguration.isVoipAlertingAnalyze())
        .setVoipAlertingConfig(new Gson().toJsonTree(voipAntiSpamConfiguration.getVoipAlertingConfigs(), new TypeToken<List<VoipAlertingConfig>>() {
        }.getType()))
        .setFasAnalyzeEnable(voipAntiSpamConfiguration.isFasAnalyze())
        .setFasConfig(new Gson().toJsonTree(voipAntiSpamConfiguration.getFasConfigs(), new TypeToken<List<FasConfig>>() {
        }.getType()));
  }

}

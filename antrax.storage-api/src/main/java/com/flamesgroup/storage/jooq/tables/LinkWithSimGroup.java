/**
 * This class is generated by jOOQ
 */
package com.flamesgroup.storage.jooq.tables;


import com.flamesgroup.storage.jooq.Keys;
import com.flamesgroup.storage.jooq.Public;
import com.flamesgroup.storage.jooq.tables.records.LinkWithSimGroupRecord;

import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class LinkWithSimGroup extends TableImpl<LinkWithSimGroupRecord> {

	private static final long serialVersionUID = 1454301088;

	/**
	 * The reference instance of <code>public.link_with_sim_group</code>
	 */
	public static final LinkWithSimGroup LINK_WITH_SIM_GROUP = new LinkWithSimGroup();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<LinkWithSimGroupRecord> getRecordType() {
		return LinkWithSimGroupRecord.class;
	}

	/**
	 * The column <code>public.link_with_sim_group.id</code>.
	 */
	public final TableField<LinkWithSimGroupRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.link_with_sim_group.gsm_group_id</code>.
	 */
	public final TableField<LinkWithSimGroupRecord, Long> GSM_GROUP_ID = createField("gsm_group_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>public.link_with_sim_group.sim_group_id</code>.
	 */
	public final TableField<LinkWithSimGroupRecord, Long> SIM_GROUP_ID = createField("sim_group_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * Create a <code>public.link_with_sim_group</code> table reference
	 */
	public LinkWithSimGroup() {
		this("link_with_sim_group", null);
	}

	/**
	 * Create an aliased <code>public.link_with_sim_group</code> table reference
	 */
	public LinkWithSimGroup(String alias) {
		this(alias, LINK_WITH_SIM_GROUP);
	}

	private LinkWithSimGroup(String alias, Table<LinkWithSimGroupRecord> aliased) {
		this(alias, aliased, null);
	}

	private LinkWithSimGroup(String alias, Table<LinkWithSimGroupRecord> aliased, Field<?>[] parameters) {
		super(alias, Public.PUBLIC, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<LinkWithSimGroupRecord, Long> getIdentity() {
		return Keys.IDENTITY_LINK_WITH_SIM_GROUP;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<LinkWithSimGroupRecord> getPrimaryKey() {
		return Keys.LINK_WITH_SIM_GROUP_PKEY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<LinkWithSimGroupRecord>> getKeys() {
		return Arrays.<UniqueKey<LinkWithSimGroupRecord>>asList(Keys.LINK_WITH_SIM_GROUP_PKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<LinkWithSimGroupRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<LinkWithSimGroupRecord, ?>>asList(Keys.LINK_WITH_SIM_GROUP__LINK_WITH_GSM_GROUP_GSM_GROUP_ID_FKEY, Keys.LINK_WITH_SIM_GROUP__LINK_WITH_GSM_GROUP_SIM_GROUP_ID_FKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LinkWithSimGroup as(String alias) {
		return new LinkWithSimGroup(alias, this);
	}

	/**
	 * Rename this table
	 */
	public LinkWithSimGroup rename(String name) {
		return new LinkWithSimGroup(name, null);
	}
}

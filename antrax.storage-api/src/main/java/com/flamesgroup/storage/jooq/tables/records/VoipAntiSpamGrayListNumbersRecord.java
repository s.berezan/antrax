/**
 * This class is generated by jOOQ
 */
package com.flamesgroup.storage.jooq.tables.records;


import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.storage.jooq.tables.VoipAntiSpamGrayListNumbers;

import java.util.Date;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VoipAntiSpamGrayListNumbersRecord extends UpdatableRecordImpl<VoipAntiSpamGrayListNumbersRecord> implements Record7<String, Date, Integer, Date, Integer, VoipAntiSpamListNumbersStatus, String> {

	private static final long serialVersionUID = -481805301;

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.number</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setNumber(String value) {
		setValue(0, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.number</code>.
	 */
	public String getNumber() {
		return (String) getValue(0);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.block_time</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setBlockTime(Date value) {
		setValue(1, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.block_time</code>.
	 */
	public Date getBlockTime() {
		return (Date) getValue(1);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.block_count</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setBlockCount(Integer value) {
		setValue(2, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.block_count</code>.
	 */
	public Integer getBlockCount() {
		return (Integer) getValue(2);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.block_time_left</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setBlockTimeLeft(Date value) {
		setValue(3, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.block_time_left</code>.
	 */
	public Date getBlockTimeLeft() {
		return (Date) getValue(3);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.routing_request_count</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setRoutingRequestCount(Integer value) {
		setValue(4, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.routing_request_count</code>.
	 */
	public Integer getRoutingRequestCount() {
		return (Integer) getValue(4);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.status</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setStatus(VoipAntiSpamListNumbersStatus value) {
		setValue(5, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.status</code>.
	 */
	public VoipAntiSpamListNumbersStatus getStatus() {
		return (VoipAntiSpamListNumbersStatus) getValue(5);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_gray_list_numbers.status_description</code>.
	 */
	public VoipAntiSpamGrayListNumbersRecord setStatusDescription(String value) {
		setValue(6, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_gray_list_numbers.status_description</code>.
	 */
	public String getStatusDescription() {
		return (String) getValue(6);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<String> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record7 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row7<String, Date, Integer, Date, Integer, VoipAntiSpamListNumbersStatus, String> fieldsRow() {
		return (Row7) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row7<String, Date, Integer, Date, Integer, VoipAntiSpamListNumbersStatus, String> valuesRow() {
		return (Row7) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field1() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Date> field2() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field3() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_COUNT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Date> field4() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME_LEFT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field5() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<VoipAntiSpamListNumbersStatus> field6() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.STATUS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field7() {
		return VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.STATUS_DESCRIPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value1() {
		return getNumber();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date value2() {
		return getBlockTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value3() {
		return getBlockCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date value4() {
		return getBlockTimeLeft();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value5() {
		return getRoutingRequestCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamListNumbersStatus value6() {
		return getStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value7() {
		return getStatusDescription();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value1(String value) {
		setNumber(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value2(Date value) {
		setBlockTime(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value3(Integer value) {
		setBlockCount(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value4(Date value) {
		setBlockTimeLeft(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value5(Integer value) {
		setRoutingRequestCount(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value6(VoipAntiSpamListNumbersStatus value) {
		setStatus(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord value7(String value) {
		setStatusDescription(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamGrayListNumbersRecord values(String value1, Date value2, Integer value3, Date value4, Integer value5, VoipAntiSpamListNumbersStatus value6, String value7) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		value5(value5);
		value6(value6);
		value7(value7);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached VoipAntiSpamGrayListNumbersRecord
	 */
	public VoipAntiSpamGrayListNumbersRecord() {
		super(VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
	}

	/**
	 * Create a detached, initialised VoipAntiSpamGrayListNumbersRecord
	 */
	public VoipAntiSpamGrayListNumbersRecord(String number, Date blockTime, Integer blockCount, Date blockTimeLeft, Integer routingRequestCount, VoipAntiSpamListNumbersStatus status, String statusDescription) {
		super(VoipAntiSpamGrayListNumbers.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);

		setValue(0, number);
		setValue(1, blockTime);
		setValue(2, blockCount);
		setValue(3, blockTimeLeft);
		setValue(4, routingRequestCount);
		setValue(5, status);
		setValue(6, statusDescription);
	}
}

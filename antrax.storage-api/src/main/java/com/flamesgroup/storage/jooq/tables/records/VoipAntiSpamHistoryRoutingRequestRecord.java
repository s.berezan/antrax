/**
 * This class is generated by jOOQ
 */
package com.flamesgroup.storage.jooq.tables.records;


import com.flamesgroup.storage.jooq.tables.VoipAntiSpamHistoryRoutingRequest;

import java.util.Date;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VoipAntiSpamHistoryRoutingRequestRecord extends TableRecordImpl<VoipAntiSpamHistoryRoutingRequestRecord> implements Record2<String, Date> {

	private static final long serialVersionUID = 308287684;

	/**
	 * Setter for <code>public.voip_anti_spam_history_routing_request.number</code>.
	 */
	public VoipAntiSpamHistoryRoutingRequestRecord setNumber(String value) {
		setValue(0, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_history_routing_request.number</code>.
	 */
	public String getNumber() {
		return (String) getValue(0);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_history_routing_request.time</code>.
	 */
	public VoipAntiSpamHistoryRoutingRequestRecord setTime(Date value) {
		setValue(1, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_history_routing_request.time</code>.
	 */
	public Date getTime() {
		return (Date) getValue(1);
	}

	// -------------------------------------------------------------------------
	// Record2 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<String, Date> fieldsRow() {
		return (Row2) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<String, Date> valuesRow() {
		return (Row2) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field1() {
		return VoipAntiSpamHistoryRoutingRequest.VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.NUMBER;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Date> field2() {
		return VoipAntiSpamHistoryRoutingRequest.VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value1() {
		return getNumber();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date value2() {
		return getTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamHistoryRoutingRequestRecord value1(String value) {
		setNumber(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamHistoryRoutingRequestRecord value2(Date value) {
		setTime(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamHistoryRoutingRequestRecord values(String value1, Date value2) {
		value1(value1);
		value2(value2);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached VoipAntiSpamHistoryRoutingRequestRecord
	 */
	public VoipAntiSpamHistoryRoutingRequestRecord() {
		super(VoipAntiSpamHistoryRoutingRequest.VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);
	}

	/**
	 * Create a detached, initialised VoipAntiSpamHistoryRoutingRequestRecord
	 */
	public VoipAntiSpamHistoryRoutingRequestRecord(String number, Date time) {
		super(VoipAntiSpamHistoryRoutingRequest.VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);

		setValue(0, number);
		setValue(1, time);
	}
}

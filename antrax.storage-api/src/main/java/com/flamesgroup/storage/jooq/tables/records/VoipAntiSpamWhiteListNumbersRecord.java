/**
 * This class is generated by jOOQ
 */
package com.flamesgroup.storage.jooq.tables.records;


import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.storage.jooq.tables.VoipAntiSpamWhiteListNumbers;

import java.util.Date;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VoipAntiSpamWhiteListNumbersRecord extends UpdatableRecordImpl<VoipAntiSpamWhiteListNumbersRecord> implements Record4<String, VoipAntiSpamListNumbersStatus, Date, Integer> {

	private static final long serialVersionUID = 1482675046;

	/**
	 * Setter for <code>public.voip_anti_spam_white_list_numbers.number</code>.
	 */
	public VoipAntiSpamWhiteListNumbersRecord setNumber(String value) {
		setValue(0, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_white_list_numbers.number</code>.
	 */
	public String getNumber() {
		return (String) getValue(0);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_white_list_numbers.status</code>.
	 */
	public VoipAntiSpamWhiteListNumbersRecord setStatus(VoipAntiSpamListNumbersStatus value) {
		setValue(1, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_white_list_numbers.status</code>.
	 */
	public VoipAntiSpamListNumbersStatus getStatus() {
		return (VoipAntiSpamListNumbersStatus) getValue(1);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_white_list_numbers.add_time</code>.
	 */
	public VoipAntiSpamWhiteListNumbersRecord setAddTime(Date value) {
		setValue(2, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_white_list_numbers.add_time</code>.
	 */
	public Date getAddTime() {
		return (Date) getValue(2);
	}

	/**
	 * Setter for <code>public.voip_anti_spam_white_list_numbers.routing_request_count</code>.
	 */
	public VoipAntiSpamWhiteListNumbersRecord setRoutingRequestCount(Integer value) {
		setValue(3, value);
		return this;
	}

	/**
	 * Getter for <code>public.voip_anti_spam_white_list_numbers.routing_request_count</code>.
	 */
	public Integer getRoutingRequestCount() {
		return (Integer) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<String> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<String, VoipAntiSpamListNumbersStatus, Date, Integer> fieldsRow() {
		return (Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<String, VoipAntiSpamListNumbersStatus, Date, Integer> valuesRow() {
		return (Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field1() {
		return VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<VoipAntiSpamListNumbersStatus> field2() {
		return VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.STATUS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Date> field3() {
		return VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ADD_TIME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field4() {
		return VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ROUTING_REQUEST_COUNT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value1() {
		return getNumber();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamListNumbersStatus value2() {
		return getStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date value3() {
		return getAddTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value4() {
		return getRoutingRequestCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamWhiteListNumbersRecord value1(String value) {
		setNumber(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamWhiteListNumbersRecord value2(VoipAntiSpamListNumbersStatus value) {
		setStatus(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamWhiteListNumbersRecord value3(Date value) {
		setAddTime(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamWhiteListNumbersRecord value4(Integer value) {
		setRoutingRequestCount(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VoipAntiSpamWhiteListNumbersRecord values(String value1, VoipAntiSpamListNumbersStatus value2, Date value3, Integer value4) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached VoipAntiSpamWhiteListNumbersRecord
	 */
	public VoipAntiSpamWhiteListNumbersRecord() {
		super(VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);
	}

	/**
	 * Create a detached, initialised VoipAntiSpamWhiteListNumbersRecord
	 */
	public VoipAntiSpamWhiteListNumbersRecord(String number, VoipAntiSpamListNumbersStatus status, Date addTime, Integer routingRequestCount) {
		super(VoipAntiSpamWhiteListNumbers.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);

		setValue(0, number);
		setValue(1, status);
		setValue(2, addTime);
		setValue(3, routingRequestCount);
	}
}

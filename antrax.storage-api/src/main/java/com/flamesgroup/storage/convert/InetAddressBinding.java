/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

public class InetAddressBinding implements Binding<Object, InetAddress> {

  private static final long serialVersionUID = -6077729975911253874L;

  @Override
  public Converter<Object, InetAddress> converter() {
    return new InetAddressConverter();
  }

  @Override
  public void sql(final BindingSQLContext<InetAddress> bsqlc) throws SQLException {
    bsqlc.render().visit(DSL.val(bsqlc.convert(converter()).value())).sql("::inet");
  }

  @Override
  public void register(final BindingRegisterContext<InetAddress> brc) throws SQLException {
    brc.statement().registerOutParameter(brc.index(), Types.VARCHAR);
  }

  @Override
  public void set(final BindingSetStatementContext<InetAddress> bssc) throws SQLException {
    bssc.statement().setString(bssc.index(), Objects.toString(bssc.convert(converter()).value(), null));
  }

  @Override
  public void set(final BindingSetSQLOutputContext<InetAddress> bsqlc) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void get(final BindingGetResultSetContext<InetAddress> bgrsc) throws SQLException {
    bgrsc.convert(converter()).value(bgrsc.resultSet().getString(bgrsc.index()));
  }

  @Override
  public void get(final BindingGetStatementContext<InetAddress> bgsc) throws SQLException {
    bgsc.convert(converter()).value(bgsc.statement().getString(bgsc.index()));
  }

  @Override
  public void get(final BindingGetSQLInputContext<InetAddress> bgsqlc) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  private static class InetAddressConverter implements Converter<Object, InetAddress> {

    private static final long serialVersionUID = 2886875296957087983L;

    private static final Logger logger = LoggerFactory.getLogger(InetAddressConverter.class);

    @Override
    public InetAddress from(final Object databaseObject) {
      if (databaseObject != null) {
        try {
          return InetAddress.getByName(databaseObject.toString());
        } catch (UnknownHostException e) {
          logger.error("[{}] - can't extract InetAddress from object [{}]", this, databaseObject, e);
        }
      }
      return null;
    }

    @Override
    public String to(final InetAddress userObject) {
      return userObject == null ? null : userObject.getHostAddress();
    }

    @Override
    public Class<Object> fromType() {
      return Object.class;
    }

    @Override
    public Class<InetAddress> toType() {
      return InetAddress.class;
    }

  }

}

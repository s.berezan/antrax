/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import com.flamesgroup.commons.ChannelUID;
import org.jooq.Converter;

public final class ChannelUIDConverter implements Converter<String, ChannelUID> {

  private static final long serialVersionUID = -8539345054689672904L;

  @Override
  public ChannelUID from(final String databaseObject) {
    try {
      return databaseObject == null ? null : ChannelUID.valueFromCanonicalName(databaseObject);
    } catch (IllegalArgumentException ignored) {
      return null;
    }
  }

  @Override
  public String to(final ChannelUID userObject) {
    return userObject == null ? null : userObject.getCanonicalName();
  }

  @Override
  public Class<String> fromType() {
    return String.class;
  }

  @Override
  public Class<ChannelUID> toType() {
    return ChannelUID.class;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.CDR;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_CONFIGURATION;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST;
import static com.flamesgroup.storage.jooq.Tables.VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS;

import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamBlackListNumbersRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamConfigurationRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamGrayListNumbersRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamHistoryBlockNumbersRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamHistoryRoutingRequestRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamWhiteListNumbersRecord;
import com.flamesgroup.storage.map.CdrMapper;
import com.flamesgroup.storage.map.VoipAntiSpamConfigurationMapper;
import com.flamesgroup.storage.map.VoipAntiSpamStatisticMapper;
import org.jooq.BatchBindStep;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.DatePart;
import org.jooq.DeleteQuery;
import org.jooq.InsertQuery;
import org.jooq.JoinType;
import org.jooq.Operator;
import org.jooq.Record;
import org.jooq.SelectQuery;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.*;

public class VoipAntiSpamDAO implements IVoipAntiSpamDAO {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpamDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public VoipAntiSpamDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void upsertVoipAntiSpamConfiguration(final VoipAntiSpamConfiguration voipAntiSpamConfiguration) throws DataModificationException {
    logger.debug("[{}] - trying to store voipAntiSpamConfiguration [{}]", this, voipAntiSpamConfiguration);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<VoipAntiSpamConfigurationRecord> voipAntiSpamConfigurationRecordDeleteQuery = context.deleteQuery(VOIP_ANTI_SPAM_CONFIGURATION);
        voipAntiSpamConfigurationRecordDeleteQuery.execute();

        VoipAntiSpamConfigurationRecord voipAntiSpamConfigurationRecord = context.newRecord(VOIP_ANTI_SPAM_CONFIGURATION);
        VoipAntiSpamConfigurationMapper.mapVoipAntiSpamConfigurationToVoipAntiSpamConfigurationRecord(voipAntiSpamConfiguration, voipAntiSpamConfigurationRecord);
        int res = voipAntiSpamConfigurationRecord.insert();

        if (res > 0) {
          logger.debug("[{}] - store voipAntiSpamConfiguration [{}]", this, voipAntiSpamConfiguration);
        } else {
          logger.warn("[{}] - failed to store voipAntiSpamConfiguration [{}]", this, voipAntiSpamConfiguration);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store voipAntiSpamConfiguration", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public VoipAntiSpamConfiguration getVoipAntiSpamConfiguration() {
    logger.trace("[{}] - trying to select VoipAntiSpamConfiguration", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      SelectQuery<VoipAntiSpamConfigurationRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_CONFIGURATION);

      VoipAntiSpamConfigurationRecord voipAntiSpamConfigurationRecord = selectQuery.fetchOneInto(VOIP_ANTI_SPAM_CONFIGURATION);
      VoipAntiSpamConfiguration voipAntiSpamConfiguration = null;
      if (voipAntiSpamConfigurationRecord != null) {
        voipAntiSpamConfiguration = VoipAntiSpamConfigurationMapper.mapVoipAntiSpamConfigurationRecordToVoipAntiSpamConfiguration(voipAntiSpamConfigurationRecord);
      }
      logger.trace("[{}] - selected voipAntiSpamConfiguration [{}]", this, voipAntiSpamConfiguration);
      return voipAntiSpamConfiguration;
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting voipAntiSpamConfiguration", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean incrementRoutingRequestWhiteNumber(final String number) throws DataModificationException {
    logger.debug("[{}] - trying to increment request count for number [{}] in white list", this, number);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        UpdateQuery<VoipAntiSpamWhiteListNumbersRecord> updateQuery = context.updateQuery(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);
        updateQuery.addConditions(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.eq(number));
        updateQuery.addValue(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ROUTING_REQUEST_COUNT, VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ROUTING_REQUEST_COUNT.plus(1));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - incremented request count for number [{}] in white list", this, number);
          return true;
        } else {
          logger.debug("[{}] - didn't find number [{}] in white list", this, number);
          return false;
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while increment request count for number [%s] in white list", this, number), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertVoipAntiSpamWhiteListNumbers(final Set<WhiteListNumber> whiteListNumbers) throws DataModificationException {
    logger.debug("[{}] - trying to store whiteListNumbers@{}", this, whiteListNumbers.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        BatchBindStep batch = context.batch(context.insertInto(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS)
            .values(null, null, null, (Integer) null)
            .onDuplicateKeyIgnore());

        whiteListNumbers.forEach(whiteListNumber -> {
          batch.bind(whiteListNumber.getNumber(), whiteListNumber.getStatus(), whiteListNumber.getAddTime(), whiteListNumber.getRoutingRequestCount());
        });


        batch.execute();
        logger.debug("[{}] - store whiteListNumbers@{}", this, whiteListNumbers.hashCode());
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store whiteListNumbers", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void deleteVoipAntiSpamWhiteListNumbers(final Set<String> whiteListNumbers) throws DataModificationException {
    deleteVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS, VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.in(whiteListNumbers), whiteListNumbers);
  }

  @Override
  public void deleteAllVoipAntiSpamWhiteListNumbers() throws DataModificationException {
    deleteAllVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);
  }

  @Override
  public List<WhiteListNumber> listWhiteListNumbers(final String search, final Pair<String, SortOrder> sort, final int offset, final int limit) {
    logger.trace("[{}] - trying to select whiteListNumbers offset [{}], limit [{}]", this, offset, limit);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamWhiteListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);
        if (sort == null) {
          selectQuery.addOrderBy(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ADD_TIME);
        } else {
          selectQuery.addOrderBy(getOrderForTable(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS, sort));
        }
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ADD_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }
        if (limit > 0) {
          if (offset > 0) {
            selectQuery.addLimit(offset, limit);
          } else {
            selectQuery.addLimit(limit);
          }
        }

        List<WhiteListNumber> whiteListNumbers = selectQuery.fetchInto(VoipAntiSpamWhiteListNumbersRecord.class).stream()
            .map(VoipAntiSpamStatisticMapper::mapVoipAntiSpamWhiteListNumbersRecordToWhiteListNumbers).collect(Collectors.toList());

        if (whiteListNumbers.size() > 0) {
          logger.trace("[{}] - whiteListNumbers selected@{}", this, whiteListNumbers.hashCode());
        } else {
          logger.trace("[{}] - whiteListNumbers aren't found", this);
        }
        return whiteListNumbers;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while selecting whiteListNumbers offset [{}], limit [{}]", this, offset, limit, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<WhiteListNumber> getListNumbersForCdrAnalyze(final long time, final long duration) {
    logger.trace("[{}] - trying to select whiteListNumbers time [{}], duration [{}]", this, time, duration);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<Record> selectQuery = context.selectQuery();
        selectQuery.addFrom(CDR);
        selectQuery.addSelect(CDR.CALLED_PHONE_NUMBER, CDR.CALLED_PHONE_NUMBER.count());
        selectQuery.addJoin(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS, JoinType.LEFT_OUTER_JOIN, CDR.CALLED_PHONE_NUMBER.cast(String.class).eq(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER));
        selectQuery.addJoin(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS, JoinType.LEFT_OUTER_JOIN, CDR.CALLED_PHONE_NUMBER.cast(String.class).eq(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER));
        selectQuery.addConditions(Operator.AND,
            CDR.SETUP_TIME.greaterOrEqual(new Date(time)),
            CDR.STOP_TIME.minus(CDR.START_TIME).cast(String.class).greaterThan(CdrMapper.convertDurationToString(duration)),
            VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.isNull(),
            VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.isNull());
        selectQuery.addGroupBy(CDR.CALLED_PHONE_NUMBER);

        List<WhiteListNumber> whiteListNumbers = selectQuery.fetch().stream().map(record -> {
          WhiteListNumber whiteListNumber = new WhiteListNumber();
          whiteListNumber.setNumber(record.getValue(0).toString());
          whiteListNumber.setRoutingRequestCount(Integer.parseInt(record.getValue(1).toString()));
          return whiteListNumber;
        }).collect(Collectors.toList());

        if (whiteListNumbers.size() > 0) {
          logger.trace("[{}] - whiteListNumbers selected@{}", this, whiteListNumbers.hashCode());
        } else {
          logger.trace("[{}] - whiteListNumbers aren't found", this);
        }
        return whiteListNumbers;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while selecting whiteListNumbers time [{}], duration [{}]", this, time, duration, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfWhiteListNumbers(final String search) {
    logger.trace("[{}] - trying to get count of whiteListNumbers", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamWhiteListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS);
        selectQuery.addSelect(VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.count());
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ADD_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_WHITE_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }

        int res = selectQuery.fetchOneInto(Integer.class);
        logger.trace("[{}] - got count of whiteListNumbers", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get count of whiteListNumbers", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public GrayListNumber incrementRoutingRequestGrayNumber(final String number) throws DataModificationException {
    logger.debug("[{}] - trying increment request count for number [{}] in gray list", this, number);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        UpdateQuery<VoipAntiSpamGrayListNumbersRecord> updateQuery = context.updateQuery(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
        updateQuery.addConditions(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.eq(number));
        updateQuery.addValue(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT, VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT.plus(1));

        updateQuery.setReturning();

        updateQuery.execute();
        VoipAntiSpamGrayListNumbersRecord voipAntiSpamGrayListNumbersRecord = updateQuery.getReturnedRecord();
        if (voipAntiSpamGrayListNumbersRecord == null) {
          logger.debug("[{}] - didn't find number [{}] in gray list", this, number);
          return null;
        } else {
          logger.debug("[{}] - incremented request count for number [{}] in gray list", this, number);
          return VoipAntiSpamStatisticMapper.mapVoipAntiSpamGrayListNumbersRecordToGrayListNumbers(voipAntiSpamGrayListNumbersRecord);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while increment request count for number [%s] in gray list", this, number), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertVoipAntiSpamGrayListNumbers(final Set<GrayListNumber> grayListNumbers) throws DataModificationException {
    logger.debug("[{}] - trying to store grayListNumbers@{}", this, grayListNumbers.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<VoipAntiSpamGrayListNumbersRecord> insertQuery = context.insertQuery(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
        insertQuery.onDuplicateKeyUpdate(true);

        grayListNumbers.forEach(grayListNumber -> {
          insertQuery.addValueForUpdate(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME, grayListNumber.getBlockTime());
          insertQuery.addValueForUpdate(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_COUNT, grayListNumber.getBlockCount());
          insertQuery.addValueForUpdate(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME_LEFT, grayListNumber.getBlockTimeLeft());
          insertQuery.addValueForUpdate(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT, grayListNumber.getRoutingRequestCount());
          insertQuery.addValueForUpdate(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.STATUS_DESCRIPTION, grayListNumber.getStatusDescription());

          VoipAntiSpamGrayListNumbersRecord voipAntiSpamGrayListNumbersRecord = context.newRecord(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
          VoipAntiSpamStatisticMapper.mapGrayListNumberToVoipAntiSpamGrayListNumbersRecord(grayListNumber, voipAntiSpamGrayListNumbersRecord);
          insertQuery.addRecord(voipAntiSpamGrayListNumbersRecord);
        });

        int res = insertQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - store grayListNumbers@{}", this, grayListNumbers.hashCode());
        } else {
          logger.warn("[{}] - stored 0 elements of grayListNumbers@{}", this, grayListNumbers.hashCode());
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store grayListNumbers", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void deleteAllVoipAntiSpamGrayListNumbers() throws DataModificationException {
    deleteAllVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
  }

  @Override
  public void deleteVoipAntiSpamGrayListNumbers(final Set<String> grayListNumbers) throws DataModificationException {
    deleteVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS, VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.in(grayListNumbers), grayListNumbers);
  }

  @Override
  public GrayListNumber getGrayListNumber(final String number) {
    logger.debug("[{}] - trying to get GrayListNumber by number [{}]", this, number);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamGrayListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
        selectQuery.addConditions(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.eq(number));

        VoipAntiSpamGrayListNumbersRecord voipAntiSpamGrayListNumbersRecord = selectQuery.fetchOneInto(VoipAntiSpamGrayListNumbersRecord.class);
        if (voipAntiSpamGrayListNumbersRecord != null) {
          logger.debug("[{}] - got GrayListNumber by number [{}]", this, number);
          return VoipAntiSpamStatisticMapper.mapVoipAntiSpamGrayListNumbersRecordToGrayListNumbers(voipAntiSpamGrayListNumbersRecord);
        } else {
          logger.debug("[{}] - can't get GrayListNumber by number [{}]", this, number);
          return null;
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get GrayListNumber by number [{}]", this, number, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public List<GrayListNumber> listGrayListNumbers(final String search, final Pair<String, SortOrder> sort, final int offset, final int limit) {
    logger.trace("[{}] - trying to select grayListNumbers offset [{}], limit [{}]", this, offset, limit);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamGrayListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
        if (sort == null) {
          selectQuery.addOrderBy(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME);
        } else {
          selectQuery.addOrderBy(getOrderForTable(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS, sort));
        }
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME_LEFT.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_COUNT.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }
        if (limit > 0) {
          if (offset > 0) {
            selectQuery.addLimit(offset, limit);
          } else {
            selectQuery.addLimit(limit);
          }
        }

        List<GrayListNumber> grayListNumbers = selectQuery.fetchInto(VoipAntiSpamGrayListNumbersRecord.class).stream()
            .map(VoipAntiSpamStatisticMapper::mapVoipAntiSpamGrayListNumbersRecordToGrayListNumbers).collect(Collectors.toList());

        if (grayListNumbers.size() > 0) {
          logger.trace("[{}] - grayListNumbers selected@{}", this, grayListNumbers.hashCode());
        } else {
          logger.trace("[{}] - grayListNumbers aren't found", this);
        }
        return grayListNumbers;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while selecting grayListNumbers offset [{}], limit [{}]", this, offset, limit, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfGrayListNumbers(final String search) {
    logger.trace("[{}] - trying to get count of GrayListNumbers", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamGrayListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS);
        selectQuery.addSelect(VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.count());
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_TIME_LEFT.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.BLOCK_COUNT.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_GRAY_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }

        int res = selectQuery.fetchOneInto(Integer.class);
        logger.trace("[{}] - got count of GrayListNumbers", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get count of GrayListNumbers", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertVoipAntiSpamBlackListNumbers(final Set<BlackListNumber> blackListNumbers) throws DataModificationException {
    logger.debug("[{}] - trying to store blackListNumbers@{}", this, blackListNumbers.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        BatchBindStep batch = context.batch(context.insertInto(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS)
            .values(null, null, null, null, (Integer) null)
            .onDuplicateKeyIgnore());

        blackListNumbers.forEach(blackListNumber -> {
          batch.bind(blackListNumber.getNumber(), blackListNumber.getStatus(), blackListNumber.getStatusDescription(), blackListNumber.getAddTime(),
              blackListNumber.getRoutingRequestCount());
        });

        batch.execute();
        logger.debug("[{}] - store blackListNumbers@{}", this, blackListNumbers.hashCode());
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store blackListNumbers", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean incrementRoutingRequestBlackNumber(final String number) throws DataModificationException {
    logger.debug("[{}] - trying to store blackListNumbers: [{}]", this, number);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        UpdateQuery<VoipAntiSpamBlackListNumbersRecord> updateQuery = context.updateQuery(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS);
        updateQuery.addConditions(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.eq(number));
        updateQuery.addValue(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ROUTING_REQUEST_COUNT, VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ROUTING_REQUEST_COUNT.plus(1));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - incremented request count for number [{}] in black list", this, number);
          return true;
        } else {
          logger.debug("[{}] - didn't find number [{}] in black list", this, number);
          return false;
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while increment request count for number [%s] in black list", this, number), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void deleteAllVoipAntiSpamBlackListNumbers() throws DataModificationException {
    deleteAllVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS);
  }

  @Override
  public void deleteVoipAntiSpamBlackListNumbers(final Set<String> blackListNumbers) throws DataModificationException {
    deleteVoipAntiSpamListNumbers(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS, VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.in(blackListNumbers), blackListNumbers);
  }

  @Override
  public BlackListNumber getBlackListNumber(final String number) {
    logger.debug("[{}] - trying to get BlackListNumber by number [{}]", this, number);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamBlackListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS);
        selectQuery.addConditions(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.eq(number));

        VoipAntiSpamBlackListNumbersRecord voipAntiSpamBlackListNumbersRecord = selectQuery.fetchOneInto(VoipAntiSpamBlackListNumbersRecord.class);
        if (voipAntiSpamBlackListNumbersRecord != null) {
          logger.debug("[{}] - got BlackListNumber by number [{}]", this, number);
          return VoipAntiSpamStatisticMapper.mapVoipAntiSpamBlackListNumbersRecordToBlackListNumbers(voipAntiSpamBlackListNumbersRecord);
        } else {
          logger.debug("[{}] - can't get BlackListNumber by number [{}]", this, number);
          return null;
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get BlackListNumber by number [{}]", this, number, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public List<BlackListNumber> listBlackListNumbers(final String search, final Pair<String, SortOrder> sort, final int offset, final int limit) {
    logger.trace("[{}] - trying to select blackListNumbers offset [{}], limit [{}]", this, offset, limit);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamBlackListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS);
        if (sort == null) {
          selectQuery.addOrderBy(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ADD_TIME);
        } else {
          selectQuery.addOrderBy(getOrderForTable(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS, sort));
        }
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ADD_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }
        if (limit > 0) {
          if (offset > 0) {
            selectQuery.addLimit(offset, limit);
          } else {
            selectQuery.addLimit(limit);
          }
        }

        List<BlackListNumber> blackListNumbers = selectQuery.fetchInto(VoipAntiSpamBlackListNumbersRecord.class).stream()
            .map(VoipAntiSpamStatisticMapper::mapVoipAntiSpamBlackListNumbersRecordToBlackListNumbers).collect(Collectors.toList());

        if (blackListNumbers.size() > 0) {
          logger.trace("[{}] - blackListNumbers selected@{}", this, blackListNumbers.hashCode());
        } else {
          logger.trace("[{}] - blackListNumbers aren't found", this);
        }
        return blackListNumbers;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while selecting blackListNumbers offset [{}], limit [{}]", this, offset, limit, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfBlackListNumbers(final String search) {
    logger.trace("[{}] - trying to get count of blackListNumbers", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamBlackListNumbersRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS);
        selectQuery.addSelect(VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.count());
        if (search != null && !search.equalsIgnoreCase("")) {
          String localSearch = search.toLowerCase();
          selectQuery.addConditions(Operator.OR,
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.NUMBER.contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.STATUS.cast(String.class).lower().contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ADD_TIME.cast(String.class).contains(localSearch),
              VOIP_ANTI_SPAM_BLACK_LIST_NUMBERS.ROUTING_REQUEST_COUNT.cast(String.class).contains(localSearch));
        }

        int res = selectQuery.fetchOneInto(Integer.class);
        logger.trace("[{}] - got count of blackListNumbers", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get count of blackListNumbers", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertHistoryRoutingRequest(final String number, final long time) throws DataModificationException {
    logger.debug("[{}] - trying to store HistoryRoutingRequest number [{}], time [{}]", this, number, time);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        VoipAntiSpamHistoryRoutingRequestRecord voipAntiSpamHistoryRoutingRequestRecord = context.newRecord(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);
        voipAntiSpamHistoryRoutingRequestRecord.setNumber(number);
        voipAntiSpamHistoryRoutingRequestRecord.setTime(new Date(time));

        int res = voipAntiSpamHistoryRoutingRequestRecord.insert();
        if (res > 0) {
          logger.debug("[{}] - store HistoryRoutingRequest number [{}], time [{}]", this, number, time);
        } else {
          logger.debug("[{}] - can't store HistoryRoutingRequest number [{}], time [{}]", this, number, time);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store HistoryRoutingRequest number [%s], time [%d]", this, number, time), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void clearHistoryRoutingRequest() throws DataModificationException {
    logger.debug("[{}] - trying to clear HistoryRoutingRequest", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      int maxPeriod = getVoipAntiSpamConfiguration().getGrayListConfigs().stream().map(GrayListConfig::getPeriod).max(Integer::compareTo).orElse(0) * 1000 * 60;
      long time = System.currentTimeMillis() - maxPeriod;
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<VoipAntiSpamHistoryRoutingRequestRecord> deleteQuery = context.deleteQuery(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);
        deleteQuery.addConditions(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME.lessThan(new Date(time)));

        int res = deleteQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - cleared HistoryRoutingRequest", this);
        } else {
          logger.debug("[{}] - can't clear HistoryRoutingRequest", this);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while clear HistoryRoutingRequest", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public int getRoutingCount(final String number, final long period) {
    logger.debug("[{}] - trying to get count of history routing request", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<VoipAntiSpamHistoryRoutingRequestRecord> selectQuery = context.selectQuery(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);
        selectQuery.addConditions(Operator.AND,
            VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.NUMBER.eq(number),
            VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME.greaterOrEqual(new Date(period)));

        int res = context.fetchCount(selectQuery);
        logger.debug("[{}] - got count of history routing request", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get count of history routing request", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public List<VoipAntiSpamPlotData> getPlotDataForRoutingRequest() {
    logger.debug("[{}] - trying to get PlotDataForRoutingRequest", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        String timeAlias = "hour";
        SelectQuery<Record> selectQuery = context.selectQuery();
        selectQuery.addFrom(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST);
        selectQuery.addSelect(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME.extract(DatePart.HOUR).as(timeAlias));
        selectQuery.addSelect(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.NUMBER.count());

        selectQuery.addOrderBy(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME.as(timeAlias));
        selectQuery.addGroupBy(VOIP_ANTI_SPAM_HISTORY_ROUTING_REQUEST.TIME.as(timeAlias));


        List<VoipAntiSpamPlotData> res = selectQuery.fetch().stream()
            .map(record -> new VoipAntiSpamPlotData(Integer.parseInt(record.getValue(0).toString()), Integer.parseInt(record.getValue(1).toString())))
            .collect(Collectors.toList());
        logger.debug("[{}] - got PlotDataForRoutingRequest", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get PlotDataForRoutingRequest", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertHistoryBlockNumbers(final String number, final long time) throws DataModificationException {
    logger.debug("[{}] - trying to store HistoryBlockNumbers number [{}], time [{}]", this, number, time);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        VoipAntiSpamHistoryBlockNumbersRecord voipAntiSpamHistoryBlockNumbersRecord = context.newRecord(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS);
        voipAntiSpamHistoryBlockNumbersRecord.setNumber(number);
        voipAntiSpamHistoryBlockNumbersRecord.setTime(new Date(time));

        int res = voipAntiSpamHistoryBlockNumbersRecord.insert();
        if (res > 0) {
          logger.debug("[{}] - store HistoryBlockNumbers number [{}], time [{}]", this, number, time);
        } else {
          logger.debug("[{}] - can't store HistoryBlockNumbers number [{}], time [{}]", this, number, time);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store HistoryBlockNumbers number [%s], time [%d]", this, number, time), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void clearHistoryBlockNumbers() throws DataModificationException {
    logger.debug("[{}] - trying to clear HistoryBlockNumbers", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<VoipAntiSpamHistoryBlockNumbersRecord> deleteQuery = context.deleteQuery(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS);

        int res = deleteQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - cleared HistoryBlockNumbers", this);
        } else {
          logger.debug("[{}] - can't clear HistoryBlockNumbers", this);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while clear HistoryBlockNumbers", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<VoipAntiSpamPlotData> getPlotDataForBlockNumbers() {
    logger.debug("[{}] - trying to get PlotDataForBlockNumbers", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        String timeAlias = "hour";
        SelectQuery<Record> selectQuery = context.selectQuery();
        selectQuery.addFrom(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS);
        selectQuery.addSelect(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS.TIME.extract(DatePart.HOUR).as(timeAlias));
        selectQuery.addSelect(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS.NUMBER.count());

        selectQuery.addOrderBy(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS.TIME.as(timeAlias));
        selectQuery.addGroupBy(VOIP_ANTI_SPAM_HISTORY_BLOCK_NUMBERS.TIME.as(timeAlias));


        List<VoipAntiSpamPlotData> res = selectQuery.fetch().stream()
            .map(record -> new VoipAntiSpamPlotData(Integer.parseInt(record.getValue(0).toString()), Integer.parseInt(record.getValue(1).toString())))
            .collect(Collectors.toList());
        logger.debug("[{}] - got PlotDataForBlockNumbers", this);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - error while get PlotDataForBlockNumbers", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  private void deleteVoipAntiSpamListNumbers(final Table table, final Condition condition, final Set<String> listNumbers) throws DataModificationException {
    logger.debug("[{}] - trying to delete {}@{}", this, table.getName().toLowerCase(), listNumbers.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        BatchBindStep batch = context.batch(context.deleteFrom(table).where(condition));
        batch.execute();
        logger.debug("[{}] - deleted {}@{}", this, table.getName().toLowerCase(), listNumbers.hashCode());
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete %s", this, table.getName().toLowerCase()), e);
    } finally {
      connection.close();
    }
  }

  private void deleteAllVoipAntiSpamListNumbers(final Table table) throws DataModificationException {
    logger.debug("[{}] - trying to delete all {}", this, table.getName().toLowerCase());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {

        DeleteQuery deleteQuery = context.deleteQuery(table);

        int res = deleteQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - deleted all from {}", this, table.getName().toLowerCase());
        } else {
          logger.warn("[{}] - failed to deleted all from {}", this, table.getName().toLowerCase());
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete %s", this, table.getName().toLowerCase()), e);
    } finally {
      connection.close();
    }
  }

  private SortField getOrderForTable(final Table table, final Pair<String, SortOrder> sort) {
    switch (sort.second()) {
      case ASCENDING:
        return table.field(sort.first().replaceAll(" ", "_")).asc();
      case DESCENDING:
        return table.field(sort.first().replaceAll(" ", "_")).desc();
      case UNSORTED:
        break;
    }
    return null;
  }

}

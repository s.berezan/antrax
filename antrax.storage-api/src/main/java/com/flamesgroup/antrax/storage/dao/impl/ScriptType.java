/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

public enum ScriptType {

  ACTIVITY_PERIOD_SCRIPT("activity_period_script"),
  FACTOR_SCRIPT("factor_script"),
  VS_FACTOR_SCRIPT("vs_factor_script"),
  BUSINESS_ACTIVITY_SCRIPT("business_activity_script"),
  GW_SELECTOR_SCRIPT("gw_selector_script"),
  SESSION_SCRIPT("session_script"),
  INCOMING_CALL_MANAGEMENT_SCRIPT("incoming_call_management_script"), // TODO: remove management word from string
  ACTION_PROVIDER_SCRIPT("action_provider_script"),
  CALL_FILTER_SCRIPT("call_filter_script"),
  SMS_FILTER_SCRIPT("sms_filter_script"),
  IMEI_GENERATOR_SCRIPT("imei_generator_script");

  private final String field;

  ScriptType(final String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

  @Override
  public String toString() {
    return field;
  }

}

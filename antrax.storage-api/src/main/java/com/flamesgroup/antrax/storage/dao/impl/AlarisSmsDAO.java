/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.ALARIS_SMS;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IAlarisSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.tables.records.AlarisSmsRecord;
import com.flamesgroup.storage.map.AlarisSmsMapper;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AlarisSmsDAO implements IAlarisSmsDAO {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public AlarisSmsDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void insertSms(final AlarisSms alarisSms) throws DataModificationException {
    logger.debug("[{}] - trying to insert alarisSms: [{}]", this, alarisSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisSmsRecord alarisSmsRecord = context.newRecord(ALARIS_SMS);
        AlarisSmsMapper.mapAlarisSmsToAlarisSmsRecord(alarisSms, alarisSmsRecord);

        int insert = alarisSmsRecord.insert();
        if (insert == 1) {
          logger.debug("[{}] - inserted alarisSms: [{}]", this, alarisSms);
        } else {
          logger.debug("[{}] - can't insert alarisSms: [{}]", this, alarisSms);
          throw new DataModificationException(String.format("[%s] - can't insert alarisSms: [%s]", this, alarisSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while insert alarisSms [{}]", this, alarisSms, e);
      throw new DataModificationException(String.format("[%s] - execution error, while insert alarisSms: [%s]", this, alarisSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateSms(final AlarisSms alarisSms) throws DataModificationException {
    logger.debug("[{}] - trying to update alarisSms: [{}]", this, alarisSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisSmsRecord alarisSmsRecord = context.newRecord(ALARIS_SMS);
        AlarisSmsMapper.mapAlarisSmsToAlarisSmsRecord(alarisSms, alarisSmsRecord);

        int update = alarisSmsRecord.update();
        if (update == 1) {
          logger.debug("[{}] - updated alarisSms: [{}]", this, alarisSms);
        } else {
          logger.debug("[{}] - can't update alarisSms: [{}]", this, alarisSms);
          throw new DataModificationException(String.format("[%s] - execution error, while update alarisSms: [%s]", this, alarisSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while update alarisSms", this, e);
      throw new DataModificationException(String.format("[%s] - execution error, while update alarisSms: [%s]", this, alarisSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisSms> listSmsAtRouting() {
    logger.debug("[{}] - trying to select alarisSms at routing", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmsRecord> selectQuery = context.selectQuery(ALARIS_SMS);
        selectQuery.addConditions(ALARIS_SMS.STATUS.eq(AlarisSms.Status.ENROUTE));

        return selectQuery.fetchInto(AlarisSmsRecord.class).stream().map(AlarisSmsMapper::mapAlarisSmsRecordToAlarisSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of alarisSms at routing", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfSendSmsToday(final String clientIp) {
    logger.debug("[{}] - trying to select count of alarisSms for today", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmsRecord> selectQuery = context.selectQuery(ALARIS_SMS);
        selectQuery.addConditions(ALARIS_SMS.CREATE_TIME.greaterOrEqual(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())));
        if (clientIp != null) {
          selectQuery.addConditions(ALARIS_SMS.CLIENT_IP.eq(clientIp));
        }
        return context.fetchCount(selectQuery);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting count of alarisSms for today", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisSms getBySmsIdAndPartNumber(final UUID smsId, final int numberOfSmsPart) {
    logger.debug("[{}] - trying to select alarisSms by smsId: [{}]", this, smsId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmsRecord> selectQuery = context.selectQuery(ALARIS_SMS);
        selectQuery.addConditions(ALARIS_SMS.SMS_ID.eq(smsId));
        selectQuery.addConditions(ALARIS_SMS.SAR_PART_NUMBER.isNull().or(ALARIS_SMS.SAR_PART_NUMBER.eq(numberOfSmsPart)));

        AlarisSmsRecord alarisSmsRecord = selectQuery.fetchOne();
        return alarisSmsRecord == null ? null : AlarisSmsMapper.mapAlarisSmsRecordToAlarisSms(alarisSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisSms by smsId: [{}]", this, smsId, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisSms getById(final UUID id) {
    logger.debug("[{}] - trying to select alarisSms by id: [{}]", this, id);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmsRecord> selectQuery = context.selectQuery(ALARIS_SMS);
        selectQuery.addConditions(ALARIS_SMS.ID.eq(id));

        AlarisSmsRecord alarisSmsRecord = selectQuery.fetchOne();
        return alarisSmsRecord == null ? null : AlarisSmsMapper.mapAlarisSmsRecordToAlarisSms(alarisSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisSms by id: [{}]", this, id, e);
      return null;
    } finally {
      connection.close();
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;


import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.impl.AlarisSmsDAO;
import com.flamesgroup.antrax.storage.dao.impl.CallDAO;
import com.flamesgroup.antrax.storage.dao.impl.CellMonitorDAO;
import com.flamesgroup.antrax.storage.dao.impl.ConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.impl.ConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.impl.GsmViewDAO;
import com.flamesgroup.antrax.storage.dao.impl.NetworkSurveyDAO;
import com.flamesgroup.antrax.storage.dao.impl.PhoneNumberPrefixDAO;
import com.flamesgroup.antrax.storage.dao.impl.ReceivedSmsDAO;
import com.flamesgroup.antrax.storage.dao.impl.RemoteRegistryDAO;
import com.flamesgroup.antrax.storage.dao.impl.SIMHistoryDAO;
import com.flamesgroup.antrax.storage.dao.impl.SentSmsDAO;
import com.flamesgroup.antrax.storage.dao.impl.SessionDAO;
import com.flamesgroup.antrax.storage.dao.impl.SimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.impl.SimpleConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.impl.VoipAntiSpamDAO;

public class DAOFactory {

  public static IConfigEditDAO createConfigEditDAO(final IDbConnectionPool dbConnectionPool) {
    return new ConfigEditDAO(dbConnectionPool);
  }

  public static IConfigViewDAO createConfigViewDAO(final IDbConnectionPool dbConnectionPool) {
    return new ConfigViewDAO(dbConnectionPool);
  }

  public static ISIMHistoryDAO createSIMHistoryDAO(final IDbConnectionPool dbConnectionPool) {
    return new SIMHistoryDAO(dbConnectionPool);
  }

  public static ICallDAO createCallDAO(final IDbConnectionPool dbConnectionPool) {
    return new CallDAO(dbConnectionPool);
  }

  public static ISessionDAO createSessionDAO(final IDbConnectionPool dbConnectionPool) {
    return new SessionDAO(dbConnectionPool);
  }

  public static RemoteRegistryAccess createRemoteRegistryAccess(final IDbConnectionPool dbConnectionPool) {
    return new RemoteRegistryDAO(dbConnectionPool);
  }

  public static ISimpleConfigViewDAO createSimpleConfigViewDAO(final IDbConnectionPool dbConnectionPool) {
    return new SimpleConfigViewDAO(dbConnectionPool);
  }

  public static ISimpleConfigEditDAO createSimpleConfigEditDAO(final IDbConnectionPool dbConnectionPool) {
    return new SimpleConfigEditDAO(dbConnectionPool);
  }

  public static IReceivedSmsDAO receivedSmsDAO(final IDbConnectionPool dbConnectionPool) {
    return new ReceivedSmsDAO(dbConnectionPool);
  }

  public static ISentSmsDAO sentSmsDAO(final IDbConnectionPool dbConnectionPool) {
    return new SentSmsDAO(dbConnectionPool);
  }

  public static IVoipAntiSpamDAO createVoipAntiSpamDAO(final IDbConnectionPool dbConnectionPool) {
    return new VoipAntiSpamDAO(dbConnectionPool);
  }

  public static INetworkSurveyDAO createNetworkSurveyDAO(final IDbConnectionPool dbConnectionPool) {
    return new NetworkSurveyDAO(dbConnectionPool);
  }

  public static IGsmViewDAO createGsmViewDAO(final IDbConnectionPool dbConnectionPool) {
    return new GsmViewDAO(dbConnectionPool);
  }

  public static IAlarisSmsDAO createAlarisSmsDAO(final IDbConnectionPool dbConnectionPool) {
    return new AlarisSmsDAO(dbConnectionPool);
  }

  public static IPhoneNumberPrefixDAO createPhoneNumberPrefixDAO(final IDbConnectionPool dbConnectionPool) {
    return new PhoneNumberPrefixDAO(dbConnectionPool);
  }

  public static ICellMonitorDAO createCellMonitorDAO(final IDbConnectionPool dbConnectionPool) {
    return new CellMonitorDAO(dbConnectionPool);
  }

}

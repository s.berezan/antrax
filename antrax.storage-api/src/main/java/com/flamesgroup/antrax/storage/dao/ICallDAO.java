/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import java.util.Date;
import java.util.List;

public interface ICallDAO {

  long insertCDR(CDR cdr) throws SQLConnectionException, UpdateFailedException;

  /**
   * Returns a list of CDRs.
   *
   * @param startTime if startTime is less than 0 then this parameter ignoring
   * @param endTime   if endTime is less than 0 then this parameter ignoring
   * @param limit     if limit is less than 1 then this parameter ignoring
   * @param offset    offset says to skip that many rows before beginning to return
   *                  rows. offset 0 is the same as omitting the offset clause
   */
  CDR[] listCDRs(long startTime, long endTime, String simGroupName, String gsmGroupName, String callerNumber, String calledNumber, ICCID simUID, int limit, int offset);

  CDR getCDRById(long id);

  int getCountNumbersInPeriod(String number, long durationTime, long addTime);

  Map<ICCID, Set<PhoneNumber>> getSimCalledNumbers(long fromTime);

  Map<PhoneNumber, Map<ICCID, Integer>> getCalledNumbersToSim(long fromTime);

  List<VoiceServerCallStatistic> listVoiceServerCallStatistic(Date fromDate, Date toDate, String prefix, String server);

}

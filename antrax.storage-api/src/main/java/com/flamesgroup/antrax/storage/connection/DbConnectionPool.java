/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.connection;

import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class DbConnectionPool implements IDbConnectionPool {

  private static final Logger logger = LoggerFactory.getLogger(DbConnectionPool.class);

  private final DBConnectionProps dbConnectionProps;
  private final BlockingQueue<IDbConnection> dbConnections;
  private final AtomicInteger countConnections = new AtomicInteger(0);

  public DbConnectionPool(final DBConnectionProps dbConnectionProps) {
    this.dbConnectionProps = dbConnectionProps;
    dbConnections = new LinkedBlockingQueue<>(dbConnectionProps.getMaxConnection());
  }

  @Override
  public IDbConnection getConnection() {
    IDbConnection connection = null;
    if (countConnections.get() <= dbConnectionProps.getMaxConnection()) {
      connection = dbConnections.poll();
    } else {
      try {
        logger.info("[{}] - wait until will be available connection", this);
        connection = dbConnections.take();
      } catch (InterruptedException e) {
        logger.warn("[{}] - can't wait for available connection", this, e);
      }
    }
    if (connection != null && !isValidConnection(connection)) {
      connection = null;
    }
    if (connection == null) {
      connection = new DbConnection(dbConnectionProps, this);
    }
    countConnections.incrementAndGet();
    return connection;
  }

  @Override
  public void offerConnection(final IDbConnection dbConnection) {
    try {
      if (!isValidConnection(dbConnection)) {
        logger.info("[{}] - dbConnection [{}] is closed", this, dbConnection);
      } else if (!dbConnections.offer(dbConnection)) {
        logger.info("[{}] - dbConnections is full, therefore current connection: [{}] will close", this, dbConnection);
        dbConnection.getConnection().close();
      }
    } catch (SQLException e) {
      logger.warn("[{}] - can't close connection: [{}]", this, dbConnection, e);
    }
    countConnections.decrementAndGet();
  }

  @Override
  public void release() {
    for (IDbConnection dbConnection : dbConnections) {
      try {
        dbConnection.getConnection().close();
      } catch (SQLException e) {
        logger.warn("[{}] - can't close connection: [{}]", this, dbConnection, e);
      }
    }
    dbConnections.clear();
    countConnections.set(0);
  }

  private boolean isValidConnection(final IDbConnection dbConnection) {
    try {
      return !dbConnection.getConnection().isClosed();
    } catch (SQLException e) {
      logger.warn("[{}] - can't get state of connection: [{}]", this, dbConnection, e);
    }
    return false;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.conn;

import java.io.Serializable;

public class DBConnectionProps implements Serializable {

  private static final long serialVersionUID = -8902529006690841696L;

  private final String driver;
  private final String url;
  private final String username;
  private final String password;
  private final int maxConnection;

  public DBConnectionProps(final String driver, final String url, final String username, final String password, final int maxConnection) {
    assert driver != null : "driver shouldn't be null";
    assert url != null : "url shouldn't be null";
    assert username != null : "username shouldn't be null";
    assert password != null : "password shouldn't be null";
    this.driver = driver;
    this.url = url;
    this.username = username;
    this.password = password;
    this.maxConnection = maxConnection;
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof DBConnectionProps)) {
      return false;
    }
    DBConnectionProps that = (DBConnectionProps) obj;
    return driver.equals(that.driver) && url.equals(that.url) && username.equals(that.username) && password.equals(that.password);
  }

  @Override
  public int hashCode() {
    return driver.hashCode() + 5 * url.hashCode() + 167 * username.hashCode() + 18827 * password.hashCode() + maxConnection;
  }

  @Override
  public String toString() {
    return "Connection: url:[" + url + "]; " + "user:[" + username + "]; " + "password:[" + password + "]" + "maxConnection:[" + maxConnection + "]";
  }

  public String getDriver() {
    return driver;
  }

  public String getUrl() {
    return url;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public int getMaxConnection() {
    return maxConnection;
  }

}

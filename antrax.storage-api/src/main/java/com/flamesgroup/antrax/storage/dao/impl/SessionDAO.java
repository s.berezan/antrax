/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import com.flamesgroup.antrax.storage.commons.SessionParams;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ISessionDAO;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.SessionRecord;
import com.flamesgroup.storage.map.SessionMapper;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.Date;

public class SessionDAO implements ISessionDAO {

  private final Logger logger = LoggerFactory.getLogger(SessionDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public SessionDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void insertNewSession(final String clientUid, final String userName, final String group, final String ip) {
    logger.debug("[{}] - trying to insert new session, clientUid: [{}], userName: [{}]", this, clientUid, userName);
    if (clientUid == null || clientUid.isEmpty()) {
      logger.warn("[{}] - can't insert new session (clientUid is not valid: [{}])", this, clientUid);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      context.transaction(configuration -> {
        SessionParams session = new SessionParams()
            .setClientUid(clientUid)
            .setUserName(userName)
            .setGroup(group)
            .setIp(InetAddress.getByName(ip));
        SessionRecord sessionRecord = context.newRecord(Tables.SESSION);
        SessionMapper.mapSessionParamsToSessionRecord(session, sessionRecord);
        Date currentTime = new Date();
        sessionRecord.setStartTime(currentTime);
        sessionRecord.setLastAccessTime(currentTime);
        sessionRecord.setEndTime(null);

        sessionRecord.store();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while inserting new session (clientUid: [{}], userName: [{}])", this, clientUid, userName, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public SessionParams[] listSessions() {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SessionRecord> selectQuery = context.selectQuery(Tables.SESSION);

        selectQuery.addOrderBy(Tables.SESSION.START_TIME.desc());
        selectQuery.addLimit(100);

        return selectQuery.fetch().into(SessionRecord.class).stream().map(record -> {
          return SessionMapper.mapSessionRecordToSessionParams(record);
        }).toArray(SessionParams[]::new);
      });

    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list sessions", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public void closeSession(final String clientUid) {
    logger.debug("[{}] - trying to update endTime for session with clientUid: [{}]", this, clientUid);
    if (clientUid == null || clientUid.isEmpty()) {
      logger.warn("[{}] - can't update endTime for session(clientUid is not valid: [{}])", this, clientUid);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      context.transaction(configuration -> {
        UpdateQuery<SessionRecord> updateQuery = context.updateQuery(Tables.SESSION);
        updateQuery.addConditions(Tables.SESSION.CLIENT_UID.eq(clientUid));
        updateQuery.addValue(Tables.SESSION.END_TIME, new Date());

        updateQuery.execute();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating endTime for session with clientUid: [{}]", this, clientUid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void cleanup() {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      context.transaction(configuration -> {
        UpdateQuery<SessionRecord> updateQuery = context.updateQuery(Tables.SESSION);
        updateQuery.addConditions(Tables.SESSION.END_TIME.isNull());
        updateQuery.addValue(Tables.SESSION.END_TIME, new Date());

        updateQuery.execute();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating endTime for all sessions", this, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void renewSession(final String clientUid) {
    logger.debug("[{}] - trying to update  lastAccessTime for session with clientUid: [{}]", this, clientUid);
    if (clientUid == null || clientUid.isEmpty()) {
      logger.warn("[{}] - can't update lastAccessTime (clientUid is not valid: [{}])", this, clientUid);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      context.transaction(configuration -> {
        UpdateQuery<SessionRecord> updateQuery = context.updateQuery(Tables.SESSION);
        updateQuery.addConditions(Tables.SESSION.CLIENT_UID.eq(clientUid));
        updateQuery.addValue(Tables.SESSION.LAST_ACCESS_TIME, new Date());

        updateQuery.execute();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating lastAccessTime (clientUid: [{}])", this, clientUid, e);
    } finally {
      connection.close();
    }
  }

}

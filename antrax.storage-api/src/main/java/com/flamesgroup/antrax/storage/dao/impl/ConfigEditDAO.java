/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.GSM_GROUP;
import static com.flamesgroup.storage.jooq.Tables.LINK_WITH_SIM_GROUP;
import static com.flamesgroup.storage.jooq.Tables.ROUTE_CONFIG;
import static com.flamesgroup.storage.jooq.Tables.SCRIPTS_COMMONS;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_DEFINITION;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_FILE;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_PARAM_DEF;
import static com.flamesgroup.storage.jooq.Tables.SIM_GROUP;
import static com.flamesgroup.storage.jooq.tables.ScriptInstance.SCRIPT_INSTANCE;
import static com.flamesgroup.storage.jooq.tables.ScriptParam.SCRIPT_PARAM;
import static com.flamesgroup.storage.jooq.tables.Server.SERVER;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptField;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptInstanceImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameterImpl;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GSMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.antrax.storage.commons.impl.ServerData;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IConfigEditDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.Sequences;
import com.flamesgroup.storage.jooq.tables.records.GsmGroupRecord;
import com.flamesgroup.storage.jooq.tables.records.LinkWithSimGroupRecord;
import com.flamesgroup.storage.jooq.tables.records.RouteConfigRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptDefinitionRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptFileRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptInstanceRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamDefRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptsCommonsRecord;
import com.flamesgroup.storage.jooq.tables.records.ServerRecord;
import com.flamesgroup.storage.jooq.tables.records.SimGroupRecord;
import com.flamesgroup.storage.map.GsmGroupMapper;
import com.flamesgroup.storage.map.RouteConfigMapper;
import com.flamesgroup.storage.map.ScriptDefinitionMapper;
import com.flamesgroup.storage.map.ScriptInstanceMapper;
import com.flamesgroup.storage.map.ServerDataMapper;
import com.flamesgroup.storage.map.SimGroupMapper;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.jooq.DSLContext;
import org.jooq.DeleteQuery;
import org.jooq.InsertQuery;
import org.jooq.Result;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ConfigEditDAO implements IConfigEditDAO {

  private final Logger logger = LoggerFactory.getLogger(ConfigEditDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public ConfigEditDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public IServerData createServer() throws DataModificationException {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        long id = context.nextval(Sequences.SERVER_ID_SEQ);
        return new ServerData(id);
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while create server", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateServer(final IServerData server) throws DataModificationException {
    logger.debug("[{}] - trying to update server: [{}]", this, server);
    if (!(server instanceof ServerData)) {
      throw new DataModificationException(String.format("[%s] - can't update server [%s] (server is null or not instance of [%s])", this, server, ServerData.class.getSimpleName()));
    }
    if (server.getName() == null || server.getName().isEmpty()) {
      throw new DataModificationException(String.format("[%s] - can't update server [%s] (name is not valid)", this, server));
    }
    ServerData serverObj = (ServerData) server;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        InsertQuery<ServerRecord> insertQuery = context.insertQuery(SERVER);

        insertQuery.onDuplicateKeyUpdate(true);

        insertQuery.addValueForUpdate(SERVER.NAME, serverObj.getName());
        insertQuery.addValueForUpdate(SERVER.VS_ENABLE, serverObj.isVoiceServerEnabled());
        insertQuery.addValueForUpdate(SERVER.SS_ENABLE, serverObj.isSimServerEnabled());
        insertQuery.addValueForUpdate(SERVER.PUBLIC_ADDRESS, serverObj.getPublicAddress());
        if (serverObj.getVoiceServerConfig() != null) {
          insertQuery.addValueForUpdate(SERVER.CALL_ROUTE_CONFIG, serverObj.getVoiceServerConfig().getCallRouteConfig());
          insertQuery.addValueForUpdate(SERVER.SMS_ROUTE_CONFIG, serverObj.getVoiceServerConfig().getSmsRouteConfig());
          insertQuery.addValueForUpdate(SERVER.AUDIO_CAPTURE, serverObj.getVoiceServerConfig().isAudioCaptureEnabled());
        }

        final ServerRecord serverRecord = context.newRecord(SERVER);
        ServerDataMapper.mapServerDataToServerRecord(serverObj, serverRecord);
        insertQuery.addRecord(serverRecord);

        int res = insertQuery.execute();

        if (res > 0) {
          logger.info("[{}] - server: [{}] was updated", this, server);
        } else {
          throw new DataModificationException(String.format("[%s] - can't update server: [%s] (result after execution < 1)", this, server));
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while updating server [%s]", this, server), e);
    } finally {
      connection.close();
    }
  }

  private boolean isOnUpdate(final long id) {
    return id >= 1;
  }

  @Override
  public void deleteServer(final IServerData server) throws DataModificationException {
    logger.debug("[{}] - trying to delete server: [{}]", this, server);
    if (!(server instanceof ServerData)) {
      logger.warn("[{}] - can't delete server: [{}] (server is null or not instance of {})", this, server, ServerData.class.getSimpleName());
      return;
    }
    ServerData serverObj = (ServerData) server;
    if (serverObj.getID() < 1) {
      logger.warn("[{}] - can't delete server [{}] (id < 1)", this, server);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        final ServerRecord serverRecord = context.newRecord(SERVER);
        serverRecord.setId(serverObj.getID());
        serverRecord.delete();
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete server [%s]", this, server), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public GSMGroup createGSMGroup() throws DataModificationException {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        long id = context.nextval(Sequences.GSM_GROUP_ID_SEQ);
        GSMGroupImpl gsmGroup = new GSMGroupImpl();
        gsmGroup.setID(id);
        return gsmGroup;
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while create GSM group", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateGSMGroup(final GSMGroup gsmGroup, final Collection<SIMGroup> linkedSimGroups) throws DataModificationException {
    logger.debug("[{}] - trying to update gsmGroup [{}]", this, gsmGroup);
    if (!(gsmGroup instanceof GSMGroupImpl)) {
      throw new DataModificationException(String.format("[%s] - can't update gsmGroup [%s] (gsmGroup isn't instance of [%s])", this, gsmGroup, GSMGroupImpl.class.getSimpleName()));
    }
    if (gsmGroup.getName() == null || gsmGroup.getName().isEmpty()) {
      throw new DataModificationException(String.format("[%s] - can't gsmGroup (name is not valid [%s])", this, gsmGroup.getName()));
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        InsertQuery<GsmGroupRecord> insertQuery = context.insertQuery(GSM_GROUP);

        insertQuery.onDuplicateKeyUpdate(true);

        insertQuery.addValueForUpdate(GSM_GROUP.NAME, gsmGroup.getName());

        GsmGroupRecord gsmGroupRecord = context.newRecord(GSM_GROUP);
        GsmGroupMapper.mapGsmGroupToGsmGroupRecord(gsmGroup, gsmGroupRecord);

        insertQuery.addRecord(gsmGroupRecord);
        int res = insertQuery.execute();

        SelectQuery<LinkWithSimGroupRecord> linkWithSimGroupPrevValuesSelect = context.selectQuery(LINK_WITH_SIM_GROUP);
        linkWithSimGroupPrevValuesSelect.addConditions(LINK_WITH_SIM_GROUP.GSM_GROUP_ID.eq(gsmGroup.getID()));

        Result<LinkWithSimGroupRecord> prevLinks = linkWithSimGroupPrevValuesSelect.fetch();

        List<Long> prevSimGroupIds = prevLinks.stream().map(LinkWithSimGroupRecord::getSimGroupId).collect(Collectors.toList());
        List<Long> newSimGroupIds = linkedSimGroups.stream().map(SIMGroup::getID).collect(Collectors.toList());
        List<Long> idForDelete = prevSimGroupIds.stream().filter(l -> !newSimGroupIds.contains(l)).collect(Collectors.toList());

        DeleteQuery<LinkWithSimGroupRecord> deleteQueryLinkGsmGroupWithSimGroup = context.deleteQuery(LINK_WITH_SIM_GROUP);
        deleteQueryLinkGsmGroupWithSimGroup.addConditions(LINK_WITH_SIM_GROUP.SIM_GROUP_ID.in(idForDelete));
        deleteQueryLinkGsmGroupWithSimGroup.addConditions(LINK_WITH_SIM_GROUP.GSM_GROUP_ID.eq(gsmGroup.getID()));
        deleteQueryLinkGsmGroupWithSimGroup.execute();

        InsertQuery<LinkWithSimGroupRecord> linkWithSimGroupRecordInsertQuery = context.insertQuery(LINK_WITH_SIM_GROUP);
        newSimGroupIds.removeAll(prevSimGroupIds);

        for (Long newSimId : newSimGroupIds) {
          LinkWithSimGroupRecord linkWithSimGroupRecord = new LinkWithSimGroupRecord();
          linkWithSimGroupRecord.setGsmGroupId(gsmGroup.getID());
          linkWithSimGroupRecord.setSimGroupId(newSimId);
          linkWithSimGroupRecordInsertQuery.addRecord(linkWithSimGroupRecord);
        }

        res += linkWithSimGroupRecordInsertQuery.execute();

        if (res > 0) {
          logger.info("[{}] - gsmGroup [{}] was updated", this, gsmGroup);
        } else {
          throw new DataModificationException(String.format("[%s] - can't update gsmGroup [%s] (result after execution < 1)", this, gsmGroup));
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - can't update gsm group: ", this, e);
      throw new DataModificationException(String.format("[%s] - execution error, while updating gsmGroup [%s]", this, gsmGroup), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void deleteGSMGroup(final GSMGroup gsmGroup) throws DataModificationException {
    logger.debug("[{}] - trying to delete gsmGroup [{}]", this, gsmGroup);
    if (!(gsmGroup instanceof GSMGroupImpl)) {
      logger.warn("[{}] - can't delete gsmGroup [{}] (gsmGroup isn't instance of [{}])", this, gsmGroup, GSMGroupImpl.class.getSimpleName());
      return;
    }

    if (gsmGroup.getID() < 1) {
      logger.warn("[{}] - can't delete gsmGroup [{}] (id < 1)", this, gsmGroup);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        DeleteQuery<LinkWithSimGroupRecord> deleteQueryLinkGsmGroupWithSimGroup = context.deleteQuery(LINK_WITH_SIM_GROUP);
        deleteQueryLinkGsmGroupWithSimGroup.addConditions(LINK_WITH_SIM_GROUP.GSM_GROUP_ID.eq(gsmGroup.getID()));
        deleteQueryLinkGsmGroupWithSimGroup.execute();

        DeleteQuery<GsmGroupRecord> deleteQuery = context.deleteQuery(GSM_GROUP);
        deleteQuery.addConditions(GSM_GROUP.ID.eq(gsmGroup.getID()));

        int res = deleteQuery.execute();
        if (res > 0) {
          logger.info("[{}] - deleted gsmGroup [{}]", this, gsmGroup);
        } else {
          throw new DataModificationException(String.format("[%s] - can't delete gsmGroup [%s] (result after execution < 1)", this, gsmGroup));
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete gsmGroup [%s]", this, gsmGroup), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public SIMGroup createSIMGroup() throws DataModificationException {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        long id = context.nextval(Sequences.SIM_GROUP_ID_SEQ);
        SIMGroupImpl simGroup = new SIMGroupImpl();
        simGroup.setID(id);
        return simGroup;
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while create SIM group", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateSIMGroup(final SIMGroup simGroup) throws DataModificationException {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> updateSIMGroupWithoutTransaction(context, simGroup));
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while updating simGroup [%s]", this, simGroup), e);
    } finally {
      connection.close();
    }
  }

  private void updateSIMGroupWithoutTransaction(final DSLContext context, final SIMGroup simGroup) throws DataModificationException {
    logger.debug("[{}] - trying to update simGroup: [{}]", this, simGroup);
    if (!(simGroup instanceof SIMGroupImpl)) {
      throw new DataModificationException(String.format("[%s] - can't update simGroup: [%s] (simGroup is null or not instance of [%s])", this, simGroup, SIMGroupImpl.class.getSimpleName()));
    }
    if (simGroup.getName() == null || simGroup.getName().isEmpty()) {
      throw new DataModificationException(String.format("[%s] - can't insert new simGroup [%s] to db", this, simGroup.getName()));
    }
    SIMGroupImpl simGroupObj = (SIMGroupImpl) simGroup;
    InsertQuery<SimGroupRecord> insertQuery = context.insertQuery(SIM_GROUP);

    insertQuery.onDuplicateKeyUpdate(true);

    insertQuery.addValueForUpdate(SIM_GROUP.NAME, simGroup.getName());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_REGISTRATION_MEDIANA, simGroup.getIdleAfterRegistration() == null ? 0 : simGroup.getIdleAfterRegistration().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_REGISTRATION_DELTA, simGroup.getIdleAfterRegistration() == null ? 0 : simGroup.getIdleAfterRegistration().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_SUCCESSFUL_CALL_MEDIANA, simGroup.getIdleAfterSuccessfullCall() == null ? 0 : simGroup.getIdleAfterSuccessfullCall().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_SUCCESSFUL_CALL_DELTA, simGroup.getIdleAfterSuccessfullCall() == null ? 0 : simGroup.getIdleAfterSuccessfullCall().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_BEFORE_UNREGISTRATION_MEDIANA, simGroup.getIdleBeforeUnregistration() == null ? 0 : simGroup.getIdleBeforeUnregistration().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_BEFORE_UNREGISTRATION_DELTA, simGroup.getIdleBeforeUnregistration() == null ? 0 : simGroup.getIdleBeforeUnregistration().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.SELF_DIAL_FACTOR, simGroup.getSelfDialFactor());
    insertQuery.addValueForUpdate(SIM_GROUP.SELF_DIAL_CHANCE, simGroup.getSelfDialChance());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_BEFORE_SELF_CALL_MEDIANA, simGroup.getIdleBeforeSelfCall() == null ? 0 : simGroup.getIdleBeforeSelfCall().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_BEFORE_SELF_CALL_DELTA, simGroup.getIdleBeforeSelfCall() == null ? 0 : simGroup.getIdleBeforeSelfCall().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_SELF_CALL_TIMEOUT_MEDIANA, simGroup.getIdleAfterSelfCallTimeout() == null ? 0 : simGroup.getIdleAfterSelfCallTimeout().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_SELF_CALL_TIMEOUT_DELTA, simGroup.getIdleAfterSelfCallTimeout() == null ? 0 : simGroup.getIdleAfterSelfCallTimeout().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.DESCRIPTION, simGroup.getDescription());
    insertQuery.addValueForUpdate(SIM_GROUP.CAN_BE_APPOINTED, simGroup.canBeAppointed());
    insertQuery.addValueForUpdate(SIM_GROUP.SYNTHETIC_RINGING, simGroup.isSyntheticRinging());
    insertQuery.addValueForUpdate(SIM_GROUP.FAS_DETECTION, simGroup.isFASDetection());
    insertQuery.addValueForUpdate(SIM_GROUP.SUPPRESS_INCOMING_CALL_SIGNAL, simGroup.isSuppressIncomingCallSignal());
    insertQuery.addValueForUpdate(SIM_GROUP.SMS_DELIVERY_REPORT, simGroup.isSmsDeliveryReport());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_ZERO_CALL_DELTA, simGroup.getIdleAfterZeroCall() == null ? 0 : simGroup.getIdleAfterZeroCall().getDelta());
    insertQuery.addValueForUpdate(SIM_GROUP.IDLE_AFTER_ZERO_CALL_MEDIANA, simGroup.getIdleAfterZeroCall() == null ? 0 : simGroup.getIdleAfterZeroCall().getMediana());
    insertQuery.addValueForUpdate(SIM_GROUP.REGISTRATION_PERIOD, simGroup.getRegistrationPeriod());
    insertQuery.addValueForUpdate(SIM_GROUP.REGISTRATION_COUNT, simGroup.getRegistrationCount());
    insertQuery.addValueForUpdate(SIM_GROUP.OPERATOR_SELECTION, simGroup.getOperatorSelection());

    SimGroupRecord simGroupRecord = context.newRecord(SIM_GROUP);
    SimGroupMapper.mapSimGroupToSimGroupRecord(simGroupObj, simGroupRecord);
    insertQuery.addRecord(simGroupRecord);

    insertQuery.setReturning();

    insertQuery.execute();
    SimGroupRecord returnedRecord = insertQuery.getReturnedRecord();
    if (returnedRecord != null) {
      logger.info("[{}] - new simGroup [{}] was updated", this, simGroup);
      simGroupObj.setID(returnedRecord.getId());
    } else {
      throw new DataModificationException(String.format("[%s] - can't update simGroup [%s] (result after execution < 1)", this, simGroup));
    }

    updateScriptInstances(context, simGroupObj);
    updateAllScriptCommons(context, simGroupObj);
  }

  private void updateScriptInstances(final DSLContext context, final SIMGroup simGroup) throws DataModificationException {
    logger.debug("[{}] - updating scriptInstances for simGroup: [{}]", this, simGroup);
    if (simGroup == null) {
      throw new DataModificationException(String.format("[%s] - can't update scriptInstances by simGroup (simGroup is null)", this));
    }

    try {
      DeleteQuery<ScriptInstanceRecord> scriptInstanceRecordDeleteQuery = context.deleteQuery(SCRIPT_INSTANCE);
      scriptInstanceRecordDeleteQuery.addConditions(SCRIPT_INSTANCE.SIM_GROUP_ID.eq(simGroup.getID()));

      scriptInstanceRecordDeleteQuery.execute();
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - failed to update script instance parameters", this), e);
    }

    updateScriptInstances(context, simGroup, ScriptField.SS_ACTIVITY, simGroup.getActivityPeriodScript());
    updateScriptInstances(context, simGroup, ScriptField.VS_BUSINESS, simGroup.getBusinessActivityScripts());
    updateScriptInstances(context, simGroup, ScriptField.SS_FACTOR, simGroup.getFactorScript());
    updateScriptInstances(context, simGroup, ScriptField.SS_GW_SELECTOR, simGroup.getGWSelectorScript());
    updateScriptInstances(context, simGroup, ScriptField.SS_SESSION, simGroup.getSessionScript());
    updateScriptInstances(context, simGroup, ScriptField.VS_ACTION_PROVIDER, simGroup.getActionProviderScripts());
    updateScriptInstances(context, simGroup, ScriptField.VS_INCOMING_CALL_MANAGER, simGroup.getIncomingCallManagementScript());
    updateScriptInstances(context, simGroup, ScriptField.VS_FACTOR, simGroup.getVSFactorScript());
    updateScriptInstances(context, simGroup, ScriptField.VS_CALL_FILTER, simGroup.getCallFilterScripts());
    updateScriptInstances(context, simGroup, ScriptField.VS_SMS_FILTER, simGroup.getSmsFilterScripts());
    updateScriptInstances(context, simGroup, ScriptField.SS_IMEI_GENERATOR, simGroup.getIMEIGeneratorScript());
  }

  private void updateScriptInstances(final DSLContext context, final SIMGroup simGroup, final ScriptField field, final ScriptInstance... scriptInstances) throws DataModificationException {
    if (scriptInstances != null) {
      for (int index = 0; index < scriptInstances.length; ++index) {
        ScriptInstance inst = scriptInstances[index];
        if (inst != null) {
          ((ScriptInstanceImpl) inst).setID(-1);
          updateScriptInstance(context, inst, simGroup, field, index);
        }
      }
    }
  }

  private void updateScriptInstance(final DSLContext context, final ScriptInstance scriptInstance, final SIMGroup simGroup, final ScriptField field,
      final int index) throws DataModificationException {
    logger.trace("[{}] - updating scriptInstance: [{} {}]", this, field, scriptInstance);
    if (!(scriptInstance instanceof ScriptInstanceImpl)) {
      throw new DataModificationException(String.format("[%s] - can't update scriptInstance: [%s] (scriptInstance is null or not instance of [%s])",
          this, scriptInstance, ScriptInstanceImpl.class.getSimpleName()));
    }
    if (!(scriptInstance.getDefinition() instanceof ScriptDefinitionImpl)) {
      throw new DataModificationException(String.format("[%s] - can't update scriptInstance: [%s] (scriptDefinition is null or not instance of [%s])",
          this, scriptInstance, ScriptDefinitionImpl.class.getSimpleName()));
    }
    ScriptInstanceImpl scriptInstanceObj = (ScriptInstanceImpl) scriptInstance;

    long id = scriptInstanceObj.getID();

    int res;
    ScriptInstanceRecord scriptInstanceRecord = context.newRecord(SCRIPT_INSTANCE);
    ScriptInstanceMapper.mapScriptInstanceToScriptInstanceRecord(scriptInstanceObj, simGroup, index, field, scriptInstanceRecord);
    if (isOnUpdate(id)) {
      scriptInstanceRecord.setId(id);
      res = scriptInstanceRecord.update();
    } else {
      res = scriptInstanceRecord.insert();
    }
    if (res > 0) {
      logger.trace("[{}] - new scriptInstance: [{}] was updated", this, scriptInstance);
      scriptInstanceObj.setID(scriptInstanceRecord.getId());
    } else {
      throw new DataModificationException(String.format("[%s] - can't update scriptInstance: [%s] (result after execution < 1)", this, scriptInstance));
    }
    updateScriptParameters(context, scriptInstanceObj);
  }

  private void updateScriptParameters(final DSLContext context, final ScriptInstanceImpl scriptInstance) throws DataModificationException, DataAccessException {
    logger.trace("[{}] - trying to update scriptParameter by scriptInstance: [{}]", this, scriptInstance);
    if (scriptInstance == null) {
      throw new DataModificationException(String.format("[%s] - can't update scriptParameter by scriptInstance (scriptInstance is null)", this));
    }

    DeleteQuery<ScriptParamRecord> scriptParamRecordDeleteQuery = context.deleteQuery(SCRIPT_PARAM);
    scriptParamRecordDeleteQuery.addConditions(SCRIPT_PARAM.SCRIPT_INSTANCE_ID.eq(scriptInstance.getID()));
    scriptParamRecordDeleteQuery.execute();

    for (final ScriptParameter scriptParameter : scriptInstance.getParameters()) {
      ScriptParamRecord scriptParamRecord = context.newRecord(SCRIPT_PARAM);
      ScriptInstanceMapper.mapScriptParameterToScriptParameterRecord((ScriptParameterImpl) scriptParameter, scriptParamRecord);

      int res = scriptParamRecord.insert();

      if (res > 0) {
        logger.trace("[{}] - new scriptParameter: [{}] was updated", this, scriptParameter);
        ((ScriptParameterImpl) scriptParameter).setID(scriptParamRecord.getId());
      } else {
        throw new DataModificationException(String.format("[%s] - can't update scriptParameter: [%s] (result after execution < 1)", this, scriptParameter));
      }
    }
  }

  private void updateAllScriptCommons(final DSLContext context, final SIMGroup simGroup) throws DataModificationException {
    logger.debug("[{}] - trying to delete all scriptCommons by simGroup: [{}]", this, simGroup);
    if (simGroup == null) {
      throw new DataModificationException(String.format("[%s] - can't delete all scriptCommons by simGroup (simGroup is null)", this));
    }
    DeleteQuery<ScriptsCommonsRecord> scriptsCommonsRecordDeleteQuery = context.deleteQuery(SCRIPTS_COMMONS);
    scriptsCommonsRecordDeleteQuery.addConditions(SCRIPTS_COMMONS.SIM_GROUP_ID.eq(simGroup.getID()));

    int res = scriptsCommonsRecordDeleteQuery.execute();
    if (res > 0) {
      logger.info("[{}] - scriptsCommons was deleted for simGroup: [{}]", this, simGroup);
    } else {
      logger.warn("[{}] - can't delete scriptsCommons for simGroup: [{}](result after execution is false)", this, simGroup);
    }
    insertScriptCommons(context, simGroup);
  }

  private void insertScriptCommons(final DSLContext context, final SIMGroup simGroup) throws DataModificationException {
    ScriptCommons[] scriptCommons = simGroup.listScriptCommons();
    if (logger.isDebugEnabled()) {
      logger.debug("[{}] - going to update scriptCommons: [{}]", this, Arrays.toString(scriptCommons));
    }
    if (scriptCommons == null) {
      return;
    }
    for (ScriptCommons sc : scriptCommons) {
      insertScriptCommons(context, sc, simGroup);
    }
  }

  private void insertScriptCommons(final DSLContext context, final ScriptCommons scriptCommons, final SIMGroup simGroup) throws DataModificationException {
    logger.debug("[{}] - trying to insert scriptCommons: [{}]", this, scriptCommons);
    if (scriptCommons.getName() == null || scriptCommons.getName().isEmpty()) {
      throw new DataModificationException(String.format("[%s] - can't insert new scriptCommons (name =[%s] (name is not valid))", this, scriptCommons));
    }
    if (simGroup == null) {
      throw new DataModificationException(String.format("[%s] - can't insert new scriptCommons (simGroup is null)", this));
    }
    ScriptsCommonsRecord scriptsCommonsRecord = context.newRecord(SCRIPTS_COMMONS);
    scriptsCommonsRecord.setValue(scriptCommons.getSerializedValue() == null ? new byte[0] : scriptCommons.getSerializedValue());
    scriptsCommonsRecord.setName(scriptCommons.getName());
    scriptsCommonsRecord.setSimGroupId(simGroup.getID());

    int res = scriptsCommonsRecord.insert();

    if (res > 0) {
      logger.info("[{}] - new scriptCommons: [{}] was inserted", this, scriptCommons);
      scriptCommons.setID(scriptsCommonsRecord.getId());
    } else {
      throw new DataModificationException(String.format("[%s] - can't insert scriptCommons: [%s] (result after execution < 1)", this, scriptCommons));
    }
  }

  @Override
  public void deleteSIMGroup(final SIMGroup simGroup) throws DataModificationException {
    logger.debug("[{}] - trying to delete simGroup: [{}]", this, simGroup);
    if (!(simGroup instanceof SIMGroupImpl)) {
      logger.warn("[{}] - can't delete simGroup: [{}] (simGroup is null or not instance of [{}])", this, simGroup, SIMGroupImpl.class.getSimpleName());
      return;
    }
    SIMGroupImpl simGroupObj = (SIMGroupImpl) simGroup;
    if (simGroupObj.getID() < 1) {
      logger.warn("[{}] - can't delete simGroup [{}] (id < 1)", this, simGroup);
      return;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        DeleteQuery<SimGroupRecord> deleteQuery = context.deleteQuery(SIM_GROUP);
        deleteQuery.addConditions(SIM_GROUP.ID.eq(simGroup.getID()));

        int res = deleteQuery.execute();
        if (res > 0) {
          logger.info("[{}] - deleted simGroup [{}]", this, simGroup);
        } else {
          throw new DataModificationException(String.format("[%s] - can't delete simGroup [%s] (result after execution < 1)", this, simGroup));
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete simGroup [%s]", this, simGroup), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void applyScriptConflicts(final ScriptFile scriptFile, final List<SIMGroup> newSimGroups, final ScriptDefinition[] scriptDefinitions) throws DataModificationException {
    logger.debug("[{}] - trying to applyScriptConflicts", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(t -> {
        clearSimGroupScripts(context);
        deleteAllScriptDefinitions(context);
        insertScriptFile(context, scriptFile);
        for (ScriptDefinition scriptDefinition : scriptDefinitions) {
          insertScriptDefinition(context, scriptDefinition);
        }
        for (SIMGroup newSimGroup : newSimGroups) {
          updateSIMGroupWithoutTransaction(context, newSimGroup);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while applyScriptConflicts", this), e);
    } finally {
      connection.close();
    }
  }

  private void clearSimGroupScripts(final DSLContext context) {
    DeleteQuery<ScriptInstanceRecord> deleteQuery = context.deleteQuery(SCRIPT_INSTANCE);
    deleteQuery.execute();
  }

  private void deleteAllScriptDefinitions(final DSLContext context) {
    logger.debug("[{}] - trying to delete all scriptDefinitions", this);
    final DeleteQuery<ScriptDefinitionRecord> deleteQuery = context.deleteQuery(SCRIPT_DEFINITION);

    int res = deleteQuery.execute();
    if (res > 0) {
      logger.info("[{}] - {} script definitions was deleted", this, res);
    } else {
      logger.debug("[{}] - can't delete all script definitions from db (result after execution < 1)", this);
    }
  }

  private void insertScriptFile(final DSLContext context, final ScriptFile scriptFile) {
    logger.debug("[{}] - trying to insert script file", this);
    ScriptFileRecord scriptFileRecord = context.newRecord(SCRIPT_FILE);
    scriptFileRecord.setName(scriptFile.getName());
    scriptFileRecord.setVersion(scriptFile.getScriptFileInfo().getVersion());
    scriptFileRecord.setData(scriptFile.getData());
    scriptFileRecord.setCheckSum(scriptFile.getScriptFileInfo().getCheckSum());
    scriptFileRecord.setCompilationTime(scriptFile.getScriptFileInfo().getCompilationTime());
    scriptFileRecord.setUploadTime(scriptFile.getScriptFileInfo().getUploadTime());
    scriptFileRecord.setFileCount(scriptFile.getScriptFileInfo().getFileCount());
    scriptFileRecord.setClassCount(scriptFile.getScriptFileInfo().getClassCount());

    int res = scriptFileRecord.insert();
    if (res > 0) {
      logger.debug("[{}] - inserted script files", this);
    }
  }

  private void insertScriptDefinition(final DSLContext context, final ScriptDefinition scriptDefinition) throws DataModificationException {
    logger.debug("[{}] - trying to update scriptDefinition: [{}]", this, scriptDefinition);

    ScriptDefinitionRecord scriptDefinitionRecord = context.newRecord(SCRIPT_DEFINITION);
    ScriptDefinitionMapper.mapScriptDefinitionToScriptDefinitionRecord(scriptDefinition, scriptDefinitionRecord);

    int res = scriptDefinitionRecord.insert();

    if (res > 0) {
      ((ScriptDefinitionImpl) scriptDefinition).setID(scriptDefinitionRecord.getId());
      logger.info("[{}] - scriptDefinitions was inserted", this);
    } else {
      throw new DataModificationException(String.format("[%s] - can't update scriptDefinition: [%s] (result after execution < 1)", this, scriptDefinition));
    }
    for (ScriptParameterDefinition paramDef : scriptDefinition.getParameters()) {
      if (!(paramDef instanceof ScriptParameterDefinitionImpl)) {
        logger.warn("[{}] - can't check scriptParameterDefinition onUpdate : [{}] (scriptParameterDefinition is not instance of[{}])",
            this, paramDef, ScriptParameterDefinitionImpl.class.getSimpleName());
        continue;
      }
      ScriptParameterDefinitionImpl paramDefObject = (ScriptParameterDefinitionImpl) paramDef;
      if (paramDef.getName() == null || paramDef.getType() == null) {
        throw new DataModificationException(String.format("[%s] - can't update scriptParameterDefinition (name=[%s], type=[%s], definition=[%s])",
            this, paramDef.getName(), paramDef.getType(), paramDef.getScriptDefinition()));
      }
      ScriptParamDefRecord scriptParameterDefinitionRecord = context.newRecord(SCRIPT_PARAM_DEF);
      ScriptDefinitionMapper.mapScriptParameterDefinitionToScriptParameterDefinitionRecord(scriptDefinitionRecord.getId(),
          paramDefObject,
          scriptParameterDefinitionRecord);
      res = scriptParameterDefinitionRecord.insert();
      if (res > 0) {
        logger.info("[{}] - new scriptParameterDefinition: [{}] was updated", this, paramDef);
        paramDefObject.setID(scriptParameterDefinitionRecord.getId());
      } else {
        throw new DataModificationException(String.format("[%s] - can't update scriptParameterDefinition: [%s] (result after execution < 1)", this, paramDef));
      }
    }
  }

  @Override
  public void updateRouteConfig(final RouteConfig routeConfig) throws DataModificationException {
    logger.debug("[{}] - trying to insert route config: [{}]", this, routeConfig);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      InsertQuery<RouteConfigRecord> insertQuery = context.insertQuery(ROUTE_CONFIG);
      insertQuery.onDuplicateKeyUpdate(true);
      insertQuery.addValueForUpdate(ROUTE_CONFIG.CALL_ALLOCATION_ALGORITHM, routeConfig.getCallAllocationAlgorithm());
      insertQuery.addValueForUpdate(ROUTE_CONFIG.SMS_ALLOCATION_ALGORITHM, routeConfig.getSmsAllocationAlgorithm());

      RouteConfigRecord routeConfigRecord = new RouteConfigRecord();
      RouteConfigMapper.mapRouteConfigToRouteConfigRecord(routeConfig, routeConfigRecord);

      insertQuery.addRecord(routeConfigRecord);
      int res = insertQuery.execute();
      if (res > 0) {
        logger.info("[{}] - new route config: [{}] was inserted", this, routeConfig);
      } else {
        throw new DataModificationException(String.format("[%s] - can't insert route config: [%s] (result after execution < 1)", this, routeConfig));
      }
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while inserting route config: [%s]", this, routeConfig), e);
    } finally {
      connection.close();
    }
  }

}

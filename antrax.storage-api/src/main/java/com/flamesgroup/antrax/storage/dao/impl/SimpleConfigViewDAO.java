/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigViewDAO;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.SimRecord;
import com.flamesgroup.storage.jooq.tables.records.SimSerializableDataRecord;
import com.flamesgroup.unit.ICCID;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.TableField;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SimpleConfigViewDAO implements ISimpleConfigViewDAO {

  private final Logger logger = LoggerFactory.getLogger(SimpleConfigViewDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public SimpleConfigViewDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  private byte[] getScript(final ICCID iccid, final ScriptType scriptType) {
    String scriptTypeField = scriptType.getField();
    logger.debug("[{}] - trying to select {} by ICCID :[{}]", this, scriptTypeField, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't select {} response (key is not valid: [{}])", this, scriptTypeField, key);
      return null;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SimSerializableDataRecord> selectQuery = context.selectQuery(Tables.SIM_SERIALIZABLE_DATA);
        selectQuery.addConditions(Tables.SIM_SERIALIZABLE_DATA.ICCID.eq(iccid));
        TableField<SimSerializableDataRecord, byte[]> type = null;
        switch (scriptType) {
          case ACTIVITY_PERIOD_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.ACTIVITY_PERIOD_SCRIPT;
            break;
          case FACTOR_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.FACTOR_SCRIPT;
            break;
          case VS_FACTOR_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.VS_FACTOR_SCRIPT;
            break;
          case BUSINESS_ACTIVITY_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.BUSINESS_ACTIVITY_SCRIPT;
            break;
          case GW_SELECTOR_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.GW_SELECTOR_SCRIPT;
            break;
          case SESSION_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.SESSION_SCRIPT;
            break;
          case ACTION_PROVIDER_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.ACTION_PROVIDER_SCRIPT;
            break;
          case INCOMING_CALL_MANAGEMENT_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.INCOMING_CALL_MANAGEMENT_SCRIPT;
            break;
          case CALL_FILTER_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.CALL_FILTER_SCRIPT;
            break;
          case SMS_FILTER_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.SMS_FILTER_SCRIPT;
            break;
          case IMEI_GENERATOR_SCRIPT:
            type = Tables.SIM_SERIALIZABLE_DATA.IMEI_GENERATOR_SCRIPT;
            break;
        }
        if (type == null) {
          return null;
        }
        selectQuery.addSelect(type);
        return selectQuery.fetchOne(type);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting [{}] by ICCID: [{}]", this, scriptTypeField, iccid, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public byte[] getSessionScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.SESSION_SCRIPT);
  }

  @Override
  public byte[] getActivityPeriodScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.ACTIVITY_PERIOD_SCRIPT);
  }

  @Override
  public byte[] getCallFilterScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.CALL_FILTER_SCRIPT);
  }

  @Override
  public byte[] getSmsFilterScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.SMS_FILTER_SCRIPT);
  }

  @Override
  public byte[] getFactorScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.FACTOR_SCRIPT);
  }

  @Override
  public byte[] getGwSelectorScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.GW_SELECTOR_SCRIPT);
  }

  @Override
  public byte[] getBusinessActivityScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.BUSINESS_ACTIVITY_SCRIPT);
  }

  @Override
  public byte[] getActionProviderScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.ACTION_PROVIDER_SCRIPT);
  }

  @Override
  public byte[] getIncomingCallManagementScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.ACTION_PROVIDER_SCRIPT);
  }

  @Override
  public byte[] getVSFactorScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.VS_FACTOR_SCRIPT);
  }

  @Override
  public byte[] getIMEIGeneratorScript(final ICCID iccid) {
    return getScript(iccid, ScriptType.IMEI_GENERATOR_SCRIPT);
  }

  @Override
  public ICCID[] findSIMList(final SimSearchingParams params) {
    logger.trace("[{}] - trying to find ICCID list by simSearchingParams:[{}]", this, params);
    if (params == null) {
      logger.warn("[{}] - can't find ICCID list (simSearchingParams is null)", this);
      return new ICCID[0];
    }

    List<ICCID> result = null;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      result = context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addSelect(Tables.SIM.ICCID);
        if (params.getPhoneNumber() != null) {
          selectQuery.addConditions(Tables.SIM.PHONE_NUMBER.like(params.getPhoneNumber().getValue()));
        }
        if (params.getEnabled() != null) {
          selectQuery.addConditions(Tables.SIM.ENABLED.eq(params.getEnabled()));
        }
        if (params.getLocked() != null) {
          selectQuery.addConditions(Tables.SIM.LOCKED.eq(params.getLocked()));
        }
        if (params.getSimGroupName() != null) {
          SelectQuery subSelectQuery = context.selectQuery(Tables.SIM_GROUP);
          subSelectQuery.addSelect(Tables.SIM_GROUP.ID);
          subSelectQuery.addConditions(Tables.SIM_GROUP.NAME.eq(params.getSimGroupName()));

          selectQuery.addConditions(Tables.SIM.SIM_GROUP_ID.in(subSelectQuery));
        }
        selectQuery.addOrderBy(Tables.SIM.ICCID);

        return selectQuery.fetch(Tables.SIM.ICCID);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while searching ICCID list by simSearchingParams:[{}]", this, params, e);
    } finally {
      connection.close();
    }

    if (result != null && !result.isEmpty()) {
      logger.trace("[{}] - ICCIDs found. size=[{}]", this, result.size());
      return result.stream().toArray(ICCID[]::new);
    } else {
      logger.trace("[{}] - ICCIDs not found", this);
      return new ICCID[0];
    }
  }

  @Override
  public boolean shouldResetSsScripts(final ICCID iccid) {
    logger.debug("[{}] - trying to select resetSsScriptsFlag by ICCID: [{}]", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't select resetSsScriptsFlag (key is not valid: [{}])", this, key);
      return false;
    }

    Boolean flag = null;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      flag = context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addSelect(Tables.SIM.RESET_SS_SCRIPTS_FLAG);
        selectQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        return selectQuery.fetchOne(Tables.SIM.RESET_SS_SCRIPTS_FLAG);
      });

    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting resetSsScriptsFlag by ICCID:[{}]", this, iccid, e);
    } finally {
      connection.close();
    }

    if (flag != null) {
      return flag;
    } else {
      return false;
    }
  }

  @Override
  public boolean shouldResetVsScripts(final ICCID iccid) {
    logger.debug("[{}] - trying to select resetVsScriptsFlag by ICCID:[{}]", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't select resetVsScriptsFlag (key is not valid: [{}])", this, key);
      return false;
    }

    Boolean flag = null;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      flag = context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addSelect(Tables.SIM.RESET_VS_SCRIPTS_FLAG);
        selectQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        return selectQuery.fetchOne(Tables.SIM.RESET_VS_SCRIPTS_FLAG);
      });

    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting resetVsScriptsFlag by ICCID:[{}]", this, iccid, e);
    } finally {
      connection.close();
    }

    if (flag != null) {
      return flag;
    } else {
      return false;
    }
  }

}

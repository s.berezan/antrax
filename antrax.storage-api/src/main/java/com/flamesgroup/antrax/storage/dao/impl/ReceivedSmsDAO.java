/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.RECEIVED_SMS;

import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IReceivedSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.storage.jooq.tables.records.ReceivedSmsRecord;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import org.jooq.DSLContext;
import org.jooq.Operator;
import org.jooq.SelectQuery;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ReceivedSmsDAO implements IReceivedSmsDAO {

  private static final Logger logger = LoggerFactory.getLogger(ReceivedSmsDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public ReceivedSmsDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void saveReceivedSms(final ICCID iccid, final SMSMessageInbound smsMessageInbound) {
    logger.debug("[{}] - trying to insert sms [{}] for iccid [{}]", this, smsMessageInbound, iccid);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        ReceivedSmsRecord receivedSmsRecord = context.newRecord(RECEIVED_SMS);
        receivedSmsRecord.setIccid(iccid)
            .setSmsCenterNumber(smsMessageInbound.getSmsCenterNumber())
            .setSenderPhoneNumber(smsMessageInbound.getSenderPhoneNumber())
            .setMessage(smsMessageInbound.getMessage())
            .setMessageClass(smsMessageInbound.getMessageClass())
            .setEncoding(smsMessageInbound.getEncoding())
            .setSenderTime(smsMessageInbound.getSenderTime())
            .setReferenceNumber(smsMessageInbound.getReferenceNumber())
            .setNumberOfSmsPart(smsMessageInbound.getNumberOfSmsPart())
            .setCountOfSmsParts(smsMessageInbound.getCountOfSmsParts());
        if (receivedSmsRecord.insert() > 0) {
          logger.debug("[{}] - inserted sms [{}] for iccid [{}]", this, smsMessageInbound, iccid);
        } else {
          throw new DataModificationException(String.format("[%s] - can't insert sms [%s] for iccid [%s]", this, smsMessageInbound, iccid));
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while inserting sms [{}] for iccid [{}]", this, smsMessageInbound, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public UUID updateSmsIdForSms(final ICCID iccid, final String senderPhoneNumber, final int referenceNumber) {
    logger.debug("[{}] - trying to update sms id for iccid: [{}], senderPhoneNumber: [{}], referenceNumber: [{}]", this, iccid, senderPhoneNumber,
        referenceNumber);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      UUID smsId = UUID.randomUUID();
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<ReceivedSmsRecord> updateQuery = context.updateQuery(RECEIVED_SMS);
        updateQuery.addConditions(Operator.AND,
            RECEIVED_SMS.ICCID.eq(iccid),
            RECEIVED_SMS.SENDER_PHONE_NUMBER.eq(senderPhoneNumber),
            RECEIVED_SMS.REFERENCE_NUMBER.eq(referenceNumber));

        updateQuery.addValue(RECEIVED_SMS.SMS_ID, smsId);
        updateQuery.addValue(RECEIVED_SMS.REFERENCE_NUMBER, (Integer) null);

        if (updateQuery.execute() > 0) {
          logger.debug("[{}] - updated sms for iccid: [{}], senderPhoneNumber: [{}], referenceNumber: [{}]", this, iccid, senderPhoneNumber,
              referenceNumber);
        } else {
          throw new DataModificationException(String.format("[%s] - can't update sms for iccid: [%s], senderPhoneNumber: [%s], referenceNumber: [%d]",
              this, iccid, senderPhoneNumber, referenceNumber));
        }
      });
      return smsId;
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating sms for iccid: [{}], senderPhoneNumber: [{}], referenceNumber: [{}]", this, iccid, senderPhoneNumber,
          referenceNumber, e);
    } finally {
      connection.close();
    }
    return null;
  }

  @Override
  public int getCountOfStoredSms(final ICCID iccid, final String senderPhoneNumber, final int referenceNumber) {
    logger.debug("[{}] - trying to get count of stored sms for by iccid: [{}], senderPhoneNumber: [{}], referenceNumber: [{}]", this, iccid,
        senderPhoneNumber, referenceNumber);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<ReceivedSmsRecord> selectQuery = context.selectQuery(RECEIVED_SMS);
        selectQuery.addSelect(RECEIVED_SMS.ID);
        selectQuery.addConditions(Operator.AND,
            RECEIVED_SMS.ICCID.eq(iccid),
            RECEIVED_SMS.SENDER_PHONE_NUMBER.eq(senderPhoneNumber),
            RECEIVED_SMS.REFERENCE_NUMBER.eq(referenceNumber));

        return selectQuery.fetch(RECEIVED_SMS.ID).size();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get count of stored sms for by iccid: [{}], senderPhoneNumber: [{}], referenceNumber: [{}]", this, iccid,
          senderPhoneNumber, referenceNumber, e);
    } finally {
      connection.close();
    }
    return 0;
  }

  @Override
  public Pair<String, Date> getFullSmsBySimId(final UUID smsId) {
    logger.debug("[{}] - trying to select full sms by smsId: [{}]", this, smsId);
    List<ReceivedSmsRecord> smsList = null;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      smsList = context.transactionResult(configuration -> {
        SelectQuery<ReceivedSmsRecord> selectQuery = context.selectQuery(RECEIVED_SMS);
        selectQuery.addSelect(RECEIVED_SMS.MESSAGE);
        selectQuery.addSelect(RECEIVED_SMS.SENDER_TIME);
        selectQuery.addConditions(RECEIVED_SMS.SMS_ID.eq(smsId));
        selectQuery.addOrderBy(RECEIVED_SMS.NUMBER_OF_SMS_PART);

        return selectQuery.fetchInto(ReceivedSmsRecord.class);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting sms by smsId: [{}]", this, smsId, e);
    } finally {
      connection.close();
    }
    return smsList == null ? null : smsList.isEmpty() ? null : new Pair<>(smsList.stream().map(ReceivedSmsRecord::getMessage).collect(Collectors.joining()),
        smsList.get(0).getSenderTime());
  }

}

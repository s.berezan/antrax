/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;

import java.util.List;
import java.util.Set;

import javax.swing.*;

public interface IVoipAntiSpamDAO {

  /*
    Configuration
   */

  void upsertVoipAntiSpamConfiguration(VoipAntiSpamConfiguration voipAntiSpamConfiguration) throws DataModificationException;

  VoipAntiSpamConfiguration getVoipAntiSpamConfiguration();

  /*
    White List
  */

  void insertVoipAntiSpamWhiteListNumbers(Set<WhiteListNumber> whiteListNumbers) throws DataModificationException;

  boolean incrementRoutingRequestWhiteNumber(String number) throws DataModificationException;

  void deleteVoipAntiSpamWhiteListNumbers(Set<String> whiteListNumbers) throws DataModificationException;

  void deleteAllVoipAntiSpamWhiteListNumbers() throws DataModificationException;

  List<WhiteListNumber> listWhiteListNumbers(String search, Pair<String, SortOrder> sort, int offset, int limit);

  List<WhiteListNumber> getListNumbersForCdrAnalyze(long time, long duration);

  int getCountOfWhiteListNumbers(String search);

  /*
    Gray List
  */

  GrayListNumber incrementRoutingRequestGrayNumber(String number) throws DataModificationException;

  void insertVoipAntiSpamGrayListNumbers(Set<GrayListNumber> grayListNumbers) throws DataModificationException;

  void deleteAllVoipAntiSpamGrayListNumbers() throws DataModificationException;

  void deleteVoipAntiSpamGrayListNumbers(Set<String> grayListNumbers) throws DataModificationException;

  GrayListNumber getGrayListNumber(String number);

  List<GrayListNumber> listGrayListNumbers(String search, Pair<String, SortOrder> sort, int offset, int limit);

  int getCountOfGrayListNumbers(String search);

  /*
    Black List
  */

  void insertVoipAntiSpamBlackListNumbers(Set<BlackListNumber> blackListNumbers) throws DataModificationException;

  boolean incrementRoutingRequestBlackNumber(String number) throws DataModificationException;

  void deleteAllVoipAntiSpamBlackListNumbers() throws DataModificationException;

  void deleteVoipAntiSpamBlackListNumbers(Set<String> blackListNumbers) throws DataModificationException;

  BlackListNumber getBlackListNumber(String number);

  List<BlackListNumber> listBlackListNumbers(String search, Pair<String, SortOrder> sort, int offset, int limit);

  int getCountOfBlackListNumbers(String search);

  /*
    History routing request
  */

  void insertHistoryRoutingRequest(String number, long time) throws DataModificationException;

  void clearHistoryRoutingRequest() throws DataModificationException;

  int getRoutingCount(String number, long period);

  List<VoipAntiSpamPlotData> getPlotDataForRoutingRequest();

  /*
    History block numbers
  */

  void insertHistoryBlockNumbers(String number, long time) throws DataModificationException;

  void clearHistoryBlockNumbers() throws DataModificationException;

  List<VoipAntiSpamPlotData> getPlotDataForBlockNumbers();

}

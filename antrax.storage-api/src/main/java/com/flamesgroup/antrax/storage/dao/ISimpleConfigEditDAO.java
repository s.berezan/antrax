/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.util.Map;

public interface ISimpleConfigEditDAO {

  void enableSIM(ICCID uid, boolean enabled);

  void lockSIM(ICCID uid, boolean locked, String reason);

  void increaseSIMCalledTime(ICCID uid, long duration);

  void increaseSIMIncomingCalledTime(ICCID uid, long duration);

  void incrementSIMAllCallCount(ICCID uid);

  void incrementSIMIncomingAllCallCount(ICCID uid);

  void incrementSIMSuccessfulCallCount(ICCID uid);

  void incrementSIMIncomingSuccessfulCallCount(ICCID uid);

  void incrementSIMSMSCount(ICCID iccid, int totalSmsParts, int successSendSmsParts);

  void incrementSIMSMSStatuses(ICCID iccid, int totalSmsStatuses);

  void incrementSIMIncomingSMSCount(ICCID iccid, final int parts);

  void saveScript(ICCID uid, ScriptType scriptType, byte[] script);

  void updatePhoneNumber(ICCID uid, PhoneNumber number);

  /**
   * @param uid
   * @return true if card was added by this call
   */
  boolean ensureSIMUIDIsPresent(ICCID uid);

  boolean isIMEIPresent(ICCID simUID);

  void updateIMEI(ICCID simUID, IMEI imei);

  void updateResetVsScriptsFlag(ICCID uid, boolean flag);

  void updateResetSsScriptsFlag(ICCID uid, boolean flag);

  void changeSimGroup(ICCID uid, long simGroupID) throws DataModificationException;

  void importSimUIDs(Map<ICCID, PhoneNumber> simUIDs);

  void updateGSMChannel(GSMChannel gsmChannel) throws DataModificationException;

  void lockGsmChannel(ChannelUID channel, boolean lock, String lockReason);

  void lockGsmChannelToArfcn(ChannelUID channel, Integer arfcn);

  void noteSIM(ICCID iccid, String note);

  void setAllowedInternet(ICCID iccid, boolean allowed);

  void setTariffPlanEndDate(ICCID iccid, long endDate);

  void setBalance(ICCID iccid, double balance);

  void resetStatistic(ICCID iccid);
}

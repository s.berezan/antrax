/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils.codebase;

import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

public class CompilationFailedException extends Exception {

  private static final long serialVersionUID = -4106596192341667637L;

  public CompilationFailedException(final List<Diagnostic<? extends JavaFileObject>> diagnostics) {
    super(translate(diagnostics));
  }

  private static String translate(final List<Diagnostic<? extends JavaFileObject>> diagnostics) {
    StringBuilder buf = new StringBuilder();
    for (Object d : diagnostics) {
      buf.append(d);
      buf.append('\n');
    }
    return buf.toString();
  }

}

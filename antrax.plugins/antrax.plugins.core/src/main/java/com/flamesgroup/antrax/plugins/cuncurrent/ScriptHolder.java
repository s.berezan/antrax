/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.plugins.cuncurrent;

import com.flamesgroup.antrax.automation.statefulscripts.IScriptDeserializer;
import com.flamesgroup.antrax.automation.statefulscripts.RestateHelper;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class ScriptHolder<T> {

  private static final Logger logger = LoggerFactory.getLogger(ScriptHolder.class);

  private T[] scripts;
  private T[] newScripts;
  private final T[] defaultScript;
  private ScriptSaver scriptSaver;
  private IScriptDeserializer scriptDeserializer;
  private boolean resetScriptStates;

  private final Lock newInstanceLock = new ReentrantLock();
  private final List<Instance> instances = new ArrayList<>();

  protected abstract void saveScripts(T[] scripts);

  protected abstract void feedWithEvents(T script);

  public ScriptHolder(final T... defaultScript) {
    this.defaultScript = defaultScript;
  }

  public T[] takeScriptsForUse() {
    update();
    if (hasScript()) {
      return scripts;
    }
    return defaultScript;
  }

  public void untakeScripts() {
    save();
  }

  private void save() {
    if (scriptSaver == null) {
      return;
    }
    if (scriptSaver.isRequiresSave()) {
      saveScripts(scripts);
      scriptSaver.handleSaved();
    }
  }

  public void setNewInstance(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final T... newInstance) {
    if (newInstance == null) {
      logger.debug("[{}] - try setting null instance for scripts, ignoring it", this);
      return;
    }

    logger.trace("[{}] - setting newInstance={}", this, newInstance);
    newInstanceLock.lock();
    try {
      if (newScripts == null) {
        this.scriptDeserializer = scriptDeserializer;
        this.resetScriptStates = resetScriptStates;
        this.newScripts = newInstance;
      } else {
        this.instances.add(new Instance(scriptDeserializer, resetScriptStates, newInstance));
      }
    } finally {
      newInstanceLock.unlock();
    }
  }

  private void update() {
    newInstanceLock.lock();
    try {
      if (newScripts == null) {
        return;
      }

      if (!instances.isEmpty()) {
        restateInstances();
        instances.clear();
      }

      logger.trace("[{}] - asked to update script", this);
      if (scripts != null && !resetScriptStates && scriptDeserializer != null) {
        if (logger.isTraceEnabled()) {
          logger.trace("[{}] - restating {} with {}", this, Arrays.toString(newScripts), Arrays.toString(scripts));
        }
        feedWithEvents(newScripts, RestateHelper.restate(newScripts, scripts, scriptDeserializer));
      }
      scripts = newScripts;
      newScripts = null;
    } finally {
      newInstanceLock.unlock();
    }

    resetScriptStates = false;
    scriptSaver = collectSaver(scripts);
    save();
    logger.trace("[{}] - now updated", this);
  }

  private void restateInstances() {
    for (Instance instance : instances) {
      if (!instance.isResetScriptStates()) {
        RestateHelper.restate(instance.getInstance(), newScripts, instance.getScriptDeserializer());
      }
      newScripts = instance.getInstance();
    }
  }

  /**
   * All unrestated scripts should be feed with events. For example newly
   * created activity script, which was not restated shold know whether
   * session and activity sctarted or not.
   *
   * @param newScripts
   * @param restated
   */
  private void feedWithEvents(final T[] newScripts, final Set<Integer> restated) {
    for (int i = 0; i < newScripts.length; ++i) {
      if (restated.contains(i)) {
        continue;
      }
      feedWithEvents(newScripts[i]);
    }
  }

  private ScriptSaver collectSaver(final T[] scripts) {
    LinkedList<ScriptSaver> savers = new LinkedList<>();
    for (StatefullScript script : filter(scripts)) {
      ScriptSaver saver = script.getScriptSaver();
      if (saver == null) {
        throw new NullPointerException("Null ScriptsSaver in " + script);
      }
      savers.add(saver);
    }
    return new ScriptSaver(savers.toArray(new ScriptSaver[savers.size()]));
  }

  protected StatefullScript[] filter(final T[] scripts) {
    ArrayList<StatefullScript> statefullScripts = new ArrayList<>();
    for (T script : scripts) {
      if (script instanceof StatefullScript) {
        statefullScripts.add((StatefullScript) script);
      }
    }
    return statefullScripts.toArray(new StatefullScript[statefullScripts.size()]);
  }

  public boolean hasScript() {
    return scripts != null;
  }

  @Override
  public String toString() {
    String classUID = super.toString();
    String[] superToString = classUID.split("[$]");
    if (superToString.length == 2) {
      classUID = superToString[1];
    }
    return String.format("ScriptsHolder(%s): %s", Arrays.toString(scripts), classUID);
  }

  private final class Instance {

    private final IScriptDeserializer scriptDeserializer;
    private final boolean resetScriptStates;
    private final T[] instance;

    Instance(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final T[] instance) {
      this.scriptDeserializer = scriptDeserializer;
      this.resetScriptStates = resetScriptStates;
      this.instance = instance;
    }

    IScriptDeserializer getScriptDeserializer() {
      return scriptDeserializer;
    }

    boolean isResetScriptStates() {
      return resetScriptStates;
    }

    T[] getInstance() {
      return instance;
    }

  }

}

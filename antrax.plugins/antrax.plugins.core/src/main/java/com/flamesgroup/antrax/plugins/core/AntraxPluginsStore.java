/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.plugins.core;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.automation.scripts.IMEIGeneratorScript;
import com.flamesgroup.antrax.automation.statefulscripts.IScriptDeserializer;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.BooleanPropertyEditor;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.NumericPropertyEditor;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.SimGroupReferencePropertyEditor;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.StringPropertyEditor;
import com.flamesgroup.utils.codebase.AntraxJavaClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AntraxPluginsStore implements IScriptDeserializer {

  private static final long serialVersionUID = 1325957825930532626L;

  private final Logger logger = LoggerFactory.getLogger(AntraxPluginsStore.class);

  private final AntraxJavaClassLoader antraxJavaClassLoader = new AntraxJavaClassLoader();

  private final static String[] builtinEditors = {"int", "byte", "boolean", "long", "short", "float", "double", SharedReference.class.getCanonicalName()};

  private ScriptDefinition[] definitions = null;

  private volatile long revision;

  private transient PropertyEditor<?>[] propertyEditors;

  public synchronized void analyze(final InputStream inputStream, final long revision) throws Exception {
    antraxJavaClassLoader.analyze(inputStream);
    logger.debug("[{}] - determining definitions", this);
    definitions = ScriptsHelper.determineScriptDefinitions(antraxJavaClassLoader);
    if (logger.isDebugEnabled()) {
      logger.debug("[{}] - got {}", this, Arrays.toString(definitions));
    }
    logger.debug("[{}] - determining property editors", this);
    propertyEditors = determinePropertyEditors();
    if (logger.isDebugEnabled()) {
      logger.debug("[{}] - got {}", this, Arrays.toString(propertyEditors));
    }
    logger.debug("[{}] - validating Serializations", this);
    validateSerialization(definitions);
    logger.debug("[{}] - validating IMEIGenerators", this);
    validateIMEIGenerators(definitions);
    logger.debug("[{}] - validating ScriptSavers", this);
    validateScriptSavers(definitions);
    logger.debug("[{}] - validating properties", this);
    validateEditors(definitions, propertyEditors);
    this.revision = revision;
  }

  private PropertyEditor<?>[] determinePropertyEditors() {
    List<PropertyEditor<?>> propertyEditors = new ArrayList<>();
    propertyEditors.add(new StringPropertyEditor());
    propertyEditors.add(new BooleanPropertyEditor());
    propertyEditors.add(new NumericPropertyEditor<>(Byte.class));
    propertyEditors.add(new NumericPropertyEditor<>(Short.class));
    propertyEditors.add(new NumericPropertyEditor<>(Integer.class));
    propertyEditors.add(new NumericPropertyEditor<>(Long.class));
    propertyEditors.add(new NumericPropertyEditor<>(Float.class));
    propertyEditors.add(new NumericPropertyEditor<>(Double.class));
    propertyEditors.add(new SimGroupReferencePropertyEditor());
    try {
      propertyEditors.addAll(ScriptsHelper.determinePropertyEditors(antraxJavaClassLoader));
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      logger.warn("[{}] - can't determine PropertyEditors", this, e);
    }
    return propertyEditors.toArray(new PropertyEditor<?>[propertyEditors.size()]);
  }

  private void validateScriptSavers(final ScriptDefinition[] definitions) throws Exception {
    for (ScriptDefinition d : definitions) {
      Object script = instantiate(d.createInstance(), ScriptsHelper.getClassOf(d.getType()), null);
      if (script instanceof StatefullScript) {
        try {
          ScriptSaver saver = ((StatefullScript) script).getScriptSaver();
          if (saver == null) {
            throw new NullPointerException("null enstead of ScriptsSaver");
          }
        } catch (Exception e) {
          logger.warn("[{}] - validation of failed", this, e);
          throw new RuntimeException("Validation of " + script + " failed", e);
        }
      }
    }
  }

  private void validateSerialization(final ScriptDefinition[] definitions) throws Exception {
    for (ScriptDefinition d : definitions) {
      Object script = instantiate(d.createInstance(), ScriptsHelper.getClassOf(d.getType()), null);
      if (script instanceof StatefullScript) {
        try {
          ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
          ObjectOutputStream out = new ObjectOutputStream(byteArray);
          out.writeObject(script);
          deserialize(byteArray.toByteArray());
        } catch (Exception e) {
          logger.warn("[{}] - validation of failed", this, e);
          throw new RuntimeException("Validation of " + script + " failed", e);
        }
      }
    }
  }

  private void validateEditors(final ScriptDefinition[] defs, final PropertyEditor<?>[] propEditors)
      throws UnsupportedScriptParamTypeException, NotSerializableException {
    Set<String> supportedTypes = collectPropEditorsSupportingTypes(propEditors);
    supportedTypes.addAll(Arrays.asList(builtinEditors));

    for (ScriptDefinition d : defs) {
      for (ScriptParameterDefinition param : d.getParameters()) {
        if (!supportedTypes.contains(param.getType())) {
          logger.warn("[{}] - {} is not supported", this, param.getType());
          throw new UnsupportedScriptParamTypeException(param);
        }
      }
    }
  }

  private void validateIMEIGenerators(final ScriptDefinition[] definitions) throws Exception {
    for (ScriptDefinition d : definitions) {
      Object script = instantiate(d.createInstance(), ScriptsHelper.getClassOf(d.getType()), null);
      if (script instanceof IMEIGeneratorScript) {
        if (script instanceof ActivityPeriodListener) {
          throw new RuntimeException("IMEI generator script can't implement interface ActivityPeriodListener");
        }
        if (script instanceof CallsListener) {
          throw new RuntimeException("IMEI generator script can't implement interface CallsListener");
        }
        if (script instanceof GenericEventListener) {
          throw new RuntimeException("IMEI generator script can't implement interface GenericEventListener");
        }
        if (script instanceof IncomingCallsListener) {
          throw new RuntimeException("IMEI generator script can't implement interface IncomingCallsListener");
        }
        if (script instanceof SessionListener) {
          throw new RuntimeException("IMEI generator script can't implement interface SessionListener");
        }
      }
    }
  }

  private Set<String> collectPropEditorsSupportingTypes(final PropertyEditor<?>[] propEditors) throws NotSerializableException {
    Set<String> retval = new HashSet<>();

    for (PropertyEditor<?> e : propEditors) {
      if (!Serializable.class.isAssignableFrom(e.getType())) {
        throw new NotSerializableException(e.getType().getCanonicalName());
      }
      retval.add(e.getType().getCanonicalName());
    }

    return retval;
  }

  public ScriptDefinition[] listDefinitions() {
    return definitions;
  }

  public PropertyEditor<?>[] listPropertyEditors() {
    if (propertyEditors == null) {
      synchronized (this) {
        if (propertyEditors == null) {
          propertyEditors = determinePropertyEditors();
        }
      }
    }
    return propertyEditors;
  }

  public <T> T instantiate(final ScriptInstance scriptInstance, final Class<T> scriptType, final ScriptCommons[] references) throws Exception {
    return ScriptsHelper.instantiate(scriptInstance, scriptType, references, antraxJavaClassLoader.getClassLoader());
  }

  public long getRevision() {
    return revision;
  }

  public Serializable deserialize(final byte[] serializedStream) {
    return deserialize(Serializable.class, serializedStream);
  }

  @Override
  public <T> T deserialize(final Class<T> clazz, final byte[] serializedStream) {
    if (serializedStream == null) {
      return null;
    }

    try (ObjectInputStream stream = createObjectInputStream(serializedStream)) {
      return clazz.cast(stream.readObject());
    } catch (IOException | ClassNotFoundException e) {
      logger.warn("[{}] - failed to deserialize parameter value", this, e);
    }
    return null;
  }

  public Class<?> getClassByName(final String type) throws ClassNotFoundException {
    return ScriptParameterDefinitionImpl.convert(type, antraxJavaClassLoader.getClassLoader());
  }

  public ClassLoader getClassLoader() {
    return antraxJavaClassLoader.getClassLoader();
  }

  public ScriptDefinition getDefinition(final String name) {
    for (ScriptDefinition d : definitions) {
      if (d.getName().equals(name)) {
        return d;
      }
    }
    throw new IllegalArgumentException("Failed to find definition with name = " + name);
  }

  private ObjectInputStream createObjectInputStream(final byte[] serializedStream) throws IOException {
    return new ObjectInputStream(new ByteArrayInputStream(serializedStream)) {

      @Override
      protected Class<?> resolveClass(final ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        return getClassByName(desc.getName());
      }

    };
  }

}

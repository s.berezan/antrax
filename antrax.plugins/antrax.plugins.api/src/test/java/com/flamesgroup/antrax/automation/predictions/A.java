/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

public final class A extends BasePrediction {

  private static final long serialVersionUID = 1L;

  private final int count;

  public A(final int count) {
    this.count = count;
  }

  @Override
  public String toLocalizedString() {
    return "a" + count;
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof A;
  }

  @Override
  protected int compareTo(final Prediction other) {
    return count - ((A) other).count;
  }

}

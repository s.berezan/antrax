/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

public class B extends BasePrediction {

  private static final long serialVersionUID = -2271576079396827949L;

  private final long weight;

  public B(final long weight) {
    this.weight = weight;
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof B;
  }

  @Override
  protected int compareTo(final Prediction other) {
    return (int) (weight - ((B) other).weight);
  }

  @Override
  public String toLocalizedString() {
    return "b" + weight;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.businesscripts;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

/**
 * Provides access to the sim card registered in gsm.
 * <p>
 * Implementation of this interface passed to the
 * {@link BusinessActivityScript#invokeBusinessActivity(RegisteredInGSMChannel)}
 * </p>
 */
public interface RegisteredInGSMChannel {

  /**
   * Fires generic event which can be caught and processed with other scripts,
   * which implements {@link GenericEventListener} and knows your event.
   *
   * @param event
   * @param args
   */
  void fireGenericEvent(String event, Serializable... args);

  /**
   * Silently drops outgoing call if it exists. It does nothing on no call.
   */
  void dropCall();

  /**
   * Silently drops outgoing call if it exists. It does nothing on no call.
   */
  void dropCall(byte callDropCauseCode, CdrDropReason cdrDropReason);

  /**
   * Locks sim card with the specified message
   *
   * @param reason
   */
  void lock(String reason);

  /**
   * Starts dialing to the specified number with the specified call state.
   * handler.
   *
   * @param number
   * @param handler
   * @throws Exception
   * @see CallStateChangeHandler
   */
  void dial(PhoneNumber number, CallStateChangeHandler handler) throws Exception;

  /**
   * Sends simple ussd (for example to check account).
   *
   * @param ussd
   * @return response from operator
   * @throws Exception in case of problems
   */
  String sendUSSD(String ussd) throws Exception;

  /**
   * Starts an ussd session.
   *
   * @param ussd
   * @return instance of current USSDSession
   * @throws Exception when first ussd failed
   */
  USSDSession startUSSDSession(String ussd) throws Exception;

  /**
   * Delegates to the activity script and returns whether activity should be
   * stopped.
   * <p>
   * This can be checked for the long operations or sleeps
   *
   * @return true when session requires to be stopped
   */
  boolean shouldStopActivity();

  /**
   * Delegates to the session script and returns whether session should be
   * stopped.
   * <p>
   * This can be checked for the long operations or sleeps
   *
   * @return true when session requires to be stopped
   */
  boolean shouldStopSession();

  /**
   * Dual-tone multi-frequency signaling.
   *
   * @param dtmfString String of ASCII characters in the set 0-9,#,*,A, B, C, D. Maximal
   *                   length of the string is 29
   * @throws java.lang.Exception in case of unsuccessful sending
   */
  void sendDTMF(String dtmfString) throws Exception;

  /**
   * Dual-tone multi-frequency signaling.
   *
   * @param dtmf     DTMF single character
   * @param duration tone duration in milliseconds. The minimum duration is 300ms
   *                 (equivalent to 1 for AT command),
   *                 maximum - 25700ms (equivalent to 255 for AT command)
   * @throws java.lang.Exception in case of unsuccessful sending
   */
  void sendDTMF(char dtmf, int duration) throws Exception;

  /**
   * Sends an sms text to the specified phoneNumber.
   *
   * @param phoneNumber
   * @param text
   * @throws Exception
   */
  void sendSMS(PhoneNumber phoneNumber, String text) throws Exception;


  /**
   * Executes an action.
   * <p>
   * Requires at least one active card in the system provided such action. In
   * case of failure throws an exception.
   * <p>
   * Algorythm of action execution is simple: Server searches for first card
   * which is ready to provide this action and ask it to execute action. In
   * case of problem, next card will be searched. When no card found - thrown
   * an exception.
   *
   * @param action
   * @param maxTime limit of time per one execution. If execution timeuted, next
   *                card is taken.
   * @throws Exception thron on failure
   */
  void executeAction(Action action, long maxTime) throws Exception;

  /**
   * Provides caller with up to date sim data.
   *
   * @return sim data
   */
  SimData getSimData();

  /**
   * Adds user message to the log of the user messages shown in GUI
   * application.
   * <p>
   * Use this fnctionality wisely. Only 3 messages is stored, others removed.
   * <p>
   * Use this functionality to notify user about important event:
   * <ul>
   * <li>activation failure</li>
   * <li>payment failure</li>
   * <li>state of bill</li>
   * <li>etc</li>
   * </ul>
   *
   * @param message
   */
  void addUserMessage(String message);

  /**
   * Add number to black list
   *
   * @param phoneNumber
   */
  void addToBlackList(BlackListNumber phoneNumber) throws Exception;

  /**
   * Changes sim card group. This happens immediately, so it might happen that
   * invoker script will forget its state.
   * Please use this call from scripts, which is final and don't need states.
   *
   * @param simGroup
   */
  void changeGroup(SimGroupReference simGroup) throws Exception;

  /**
   * Update phone number on sim card
   *
   * @param phoneNumber
   */

  void updatePhoneNumber(PhoneNumber phoneNumber) throws Exception;

  /**
   * Reset IMEI
   */
  void resetIMEI() throws Exception;
  
  HTTPResponse sendHTTPRequest(String apn, String url, String caCertificate) throws Exception;

  void changeAllowedInternet(boolean allowed);

  void setTariffPlanEndDate(long endDate);

  void setBalance(double balance);

}

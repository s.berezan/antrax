/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

import java.io.Serializable;

/**
 * Declares how prediction should be
 * <p>
 * Prediction is information which told when (or in what situation) card will
 * change it's activity period.
 * </p>
 */
public interface Prediction extends Serializable {

  /**
   * Returns localized prediction message
   * <p>
   * For example:
   * <p>
   * <pre>
   * 1 call
   * 4 minutes
   * 5 minutes of call duration
   * 12 april 2012
   * </pre>
   */
  String toLocalizedString();

  /**
   * Logical or operation with <code>other</code> prediction
   *
   * @param other
   * @return result of logical or
   */
  Prediction or(Prediction other);

  /**
   * Logical and operation with <code>other</code> prediction
   *
   * @param other
   * @return result of logical and
   */
  Prediction and(Prediction other);

  /**
   * Trims this and that to smaller.
   * <p>
   * <p>
   * For example:
   * <p>
   * <pre>
   * 1_minute.trimToSmaller(2_minute) == 1_minute
   * </pre>
   * <p>
   * </p>
   *
   * @param other
   */
  Prediction trimToSmaller(Prediction other);

  /**
   * Trims this and that to bigger
   * <p>
   * <p>
   * For example:
   * <p>
   * <pre>
   * 1_minute.trimToSmaller(2_minute) == 2_minute
   * </pre>
   * <p>
   * </p>
   *
   * @param other
   */
  Prediction trimToBigger(Prediction other);

}

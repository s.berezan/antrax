/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.statefulscripts;

import com.flamesgroup.antrax.automation.annotations.StateField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * Provides utility methods for restation of objects.
 * <p>
 * It uses reflection and annotation {@link StateField} to determine which
 * fields shold be restated. If field is instance of @link
 * {@link StatefullScript} it will be restated using reflection the same way as
 * script.
 * </p>
 * <p>
 * This class is for internal purpose only. You probably will never need to use
 * it in scripts.
 * </p>
 *
 * @see StatefullScript
 * @see StateField
 * @since 0.9.5
 */
public class RestateHelper {

  private static final Logger logger = LoggerFactory.getLogger(RestateHelper.class);

  public static boolean restate(final Object dest, final Object src, final IScriptDeserializer scriptDeserializer) {
    validateArguments(dest, src);
    synchronized (src) {
      if (!dest.getClass().getName().equals(src.getClass().getName())) {
        return false;
      }
      copyFields(dest, src, scriptDeserializer);
      return true;
    }
  }

  private static boolean copyFields(final Object dest, final Object src, final IScriptDeserializer scriptDeserializer) {
    return copyFields(dest, src, dest.getClass(), src.getClass(), scriptDeserializer);
  }

  private static boolean copyFields(final Object dest, final Object src, final Class<?> destClass, final Class<?> srcClass, final IScriptDeserializer scriptDeserializer) {
    boolean retval = true;
    if (srcClass == null || destClass == null) {
      return true;
    }
    for (Field f : srcClass.getDeclaredFields()) {
      if (!f.isAnnotationPresent(StateField.class)) {
        continue;
      }
      try {
        f.setAccessible(true);
        Object fieldValue = f.get(src);
        Field destField = destClass.getDeclaredField(f.getName());
        destField.setAccessible(true);
        if (fieldValue instanceof StatefullScript) {
          retval &= restate(destField.get(dest), fieldValue, scriptDeserializer);
          logger.debug("[{}] - restating {}:{} from {} to {}", RestateHelper.class, f, fieldValue, src, dest);
        } else {
          destField.set(dest, normalize(fieldValue, scriptDeserializer));
          if (logger.isDebugEnabled()) {
            logger.debug("[{}] - copying field {}:{} from {} to {}", RestateHelper.class, f, fieldValue, src, dest);
          }
        }
      } catch (Exception e) {
        if (logger.isWarnEnabled()) {
          logger.warn("[{}] - failed to copy {} \"{}\" from {} to {}", RestateHelper.class, StateField.class.getSimpleName(), f.getName(), src, dest, e);
        }
        return false;
      }
    }
    return retval && copyFields(dest, src, destClass.getSuperclass(), srcClass.getSuperclass(), scriptDeserializer);
  }

  private static Object normalize(final Object fieldValue, final IScriptDeserializer scriptDeserializer) throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    ObjectOutputStream outputStream = new ObjectOutputStream(out);
    outputStream.writeObject(fieldValue);
    outputStream.close();
    out.close();

    return scriptDeserializer.deserialize(Object.class, out.toByteArray());
  }

  /**
   * @param dest
   * @param src
   * @param scriptDeserializer
   * @return indexes of successfully restated scripts
   */
  public static Set<Integer> restate(final Object[] dest, final Object[] src, final IScriptDeserializer scriptDeserializer) {
    Set<Integer> retval = new TreeSet<>();
    Object[] srcCopy = Arrays.copyOf(src, src.length);
    for (int i = 0; i < dest.length; ++i) {
      if (dest[i] instanceof StatefullScript) {
        Object s = findSame(dest[i], srcCopy);
        if (s != null) {
          if (restate(dest[i], s, scriptDeserializer)) {
            retval.add(i);
          }
        }
      }
    }
    return retval;
  }

  private static Object findSame(final Object d, final Object[] src) {
    Class<?> clazz = d.getClass();
    for (int i = 0; i < src.length; ++i) {
      Object s = src[i];
      if (s != null && clazz.getName() == s.getClass().getName()) {
        src[i] = null;
        return s;
      }
    }
    return null;
  }

  private static void validateArguments(final Object dest, final Object src) {
    if (src == null) {
      throw new NullPointerException("src is null");
    }
    if (dest == null) {
      throw new NullPointerException("dest is null");
    }
  }

}

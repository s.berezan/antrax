/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Provides classes to create predictions for
 * {@linkplain com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript activity period scripts}
 * <p>
 * Prediction is information which tolds when (or in what situation) card will change it's activity period.
 * For example it can tell client that session will stop after 3 successfull calls.
 * Predictions can be combined using logical operations.
 * For example: card is active until 3 calls or 2 minutes of activity.
 */
package com.flamesgroup.antrax.automation.predictions;


/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

/**
 * Provides class with already implemented some routine of {@link Prediction}
 * <p>
 * Prediction is information which told when (or in what situation) card will
 * change it's activity period.
 * </p>
 */
public abstract class BasePrediction implements Prediction {

  private static final long serialVersionUID = 3619448892334977722L;

  /**
   * Returns whether prediction can be compared to other
   */
  protected abstract boolean canCompareTo(Prediction other);

  /**
   * Returns value of comparing this with other prediction.
   *
   * @return -1 if this is less than other 0 if this is equals to other 1 if
   * this is bigger than other
   */
  protected abstract int compareTo(Prediction other);

  /**
   * @see Prediction#trimToSmaller(Prediction)
   */
  @Override
  public Prediction trimToSmaller(final Prediction other) {
    if (other instanceof AndComposite) {
      return trimToSmaller((AndComposite) other);
    }
    if (canCompareTo(other)) {
      if (compareTo(other) < 0) {
        return this;
      }
      return other;
    }
    return null;
  }

  private Prediction trimToSmaller(final AndComposite other) {
    for (Prediction p : other.getPredictions()) {
      Prediction trimmed = trimToSmaller(p);
      if (trimmed != null) {
        return trimmed;
      }
    }
    return null;
  }

  /**
   * @see Prediction#trimToBigger(Prediction)
   */
  @Override
  public Prediction trimToBigger(final Prediction other) {
    if (canCompareTo(other)) {
      if (compareTo(other) > 0) {
        return this;
      }
      return other;
    }
    return null;
  }

  /**
   * For compatibility delegates to the {@link #toLocalizedString()}
   */
  @Override
  public String toString() {
    return toLocalizedString();
  }

  /**
   * @see Prediction#and(Prediction)
   */
  @Override
  public Prediction and(final Prediction other) {
    if (other instanceof AlwaysTruePrediction) {
      return this;
    }
    if (other instanceof AlwaysFalsePrediction) {
      return other;
    }
    return new AndComposite(this, other);
  }

  /**
   * @see Prediction#or(Prediction)
   */
  @Override
  public Prediction or(final Prediction other) {
    if (other instanceof AlwaysTruePrediction) {
      return other;
    }
    if (other instanceof AlwaysFalsePrediction) {
      return this;
    }
    return new OrComposite(this, other);
  }

}

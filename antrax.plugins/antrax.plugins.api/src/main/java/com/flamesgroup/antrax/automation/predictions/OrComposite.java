/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

public class OrComposite extends BaseComposit {

  private static final long serialVersionUID = 1527841095752760491L;

  public OrComposite(final Prediction first, final Prediction second) {
    add(first);
    add(second);
  }

  private void add(final Prediction p) {
    if (p instanceof OrComposite) {
      addPrediction(((OrComposite) p).getPredictions());
    } else {
      addPrediction(p);
    }
  }

  @Override
  protected Prediction trim(final Prediction a, final Prediction b) {
    return a.trimToSmaller(b);
  }

  @Override
  protected String getDelim() {
    return "or";
  }

  @Override
  public Prediction trimToBigger(final Prediction other) {
    return null;
  }

  @Override
  public Prediction trimToSmaller(final Prediction other) {
    return null;
  }

}

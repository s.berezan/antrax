/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.editors;

import java.awt.*;
import java.util.EventObject;

import javax.swing.event.CellEditorListener;

/**
 * Introduces functionality required to be implemented to become proper script
 * property editor.
 *
 * @param <T>
 */
public interface PropertyEditor<T> {

  /**
   * Returns java type supported by this editor
   */
  Class<? extends T> getType();

  /**
   * Returns the current value
   */
  T getValue();

  /**
   * Should update ui presentation using new value
   */
  void setValue(T value);

  /**
   * Returns whether cell should be selected. In major cases returning true is
   * ok
   */
  boolean shouldSelectCell(EventObject anEvent);

  /**
   * Called to stop editing.
   * <p>
   * Value should be extracted from ui component.
   */
  boolean stopEditing();

  boolean isValid();

  /**
   * Returns number of clicks on the table cell
   */
  int getClickCountToEdit();

  /**
   * Returns component in which editing occurs.
   * <p>
   * If component will not fit size of cell it will be paced to the dialog. So
   * any sizes is ok.
   */
  Component getEditorComponent();

  /**
   * Returns component for rendering value in the table cell. It should fit
   * size of cell.
   */
  Component getRendererComponent(T value);

  /**
   * Adds editor listener, which is used to notify about stop editing or
   * cancel editing
   */
  void addCellEditorListener(CellEditorListener l);

  /**
   * Removes editor listener
   */
  void removeCellEditorListener(CellEditorListener l);

}

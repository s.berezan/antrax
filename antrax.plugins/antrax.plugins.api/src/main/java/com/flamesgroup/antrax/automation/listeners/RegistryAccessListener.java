/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.listeners;

import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;

/**
 * Injected with {@linkplain RegistryAccess registry access}
 */
public interface RegistryAccessListener {
  /**
   * This method called each time script is initializing. Pay attention, that
   * registry is not Serializable, so should be marked as transient
   *
   * @param registry
   */
  void setRegistryAccess(RegistryAccess registry);

}

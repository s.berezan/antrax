/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.editors;

import java.awt.*;
import java.util.EventObject;
import java.util.LinkedList;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

/**
 * Base implementation of {@link PropertyEditor}
 * <p>
 * Contains simple renderer, which uses {@link Object#toString()} to get string
 * representation of object.
 * </p>
 * <p>
 * Also it implements {@link #addCellEditorListener(CellEditorListener)} and
 * {@link #removeCellEditorListener(CellEditorListener)}. You can call
 * {@link #fireEditingCanceled()} and {@link #fireEditingStopped()}.
 * </p>
 *
 * @param <T>
 */
public abstract class BasePropertyEditor<T> implements PropertyEditor<T> {

  private static final PropertyTableRenderer<Object> renderer = new DefaultPropertyTableRenderer<>();

  private final LinkedList<CellEditorListener> listeners = new LinkedList<>();

  protected PropertyTableRenderer<Object> getRenderer() {
    //    if (renderer == null) {
    //      renderer = new DefaultPropertyTableRenderer<T>();
    //    }
    return renderer;
  }

  @Override
  public int getClickCountToEdit() {
    return 1;
  }

  @Override
  public boolean stopEditing() {
    return true;
  }

  @Override
  public boolean shouldSelectCell(final EventObject anEvent) {
    return true;
  }

  @Override
  public Component getRendererComponent(final T value) {
    return getRenderer().getRendererComponent(value);
  }

  protected void fireEditingStopped() {
    for (CellEditorListener l : listeners) {
      l.editingStopped(new ChangeEvent(this));
    }
  }

  protected void fireEditingCanceled() {
    for (CellEditorListener l : listeners) {
      l.editingCanceled(new ChangeEvent(this));
    }
  }

  @Override
  public final void addCellEditorListener(final CellEditorListener l) {
    listeners.add(l);
  }

  @Override
  public final void removeCellEditorListener(final CellEditorListener l) {
    listeners.remove(l);
  }

  @Override
  public boolean isValid() {
    return true;
  }

}

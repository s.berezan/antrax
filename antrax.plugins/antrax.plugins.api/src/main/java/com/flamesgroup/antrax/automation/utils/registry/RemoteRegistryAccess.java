/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.utils.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Provides access to the store of string lists assigned to the path
 * <p>
 * In the other words it is much like map, but works with the lists associated
 * with key.
 * </p>
 * <p>
 * The other difference is that registry is central store, accessed from many
 * points. So there is unblocking synchronization on modification of registry
 * nodes. Any modification can be applied or failed if made with outdated node.
 * </p>
 * <p>
 * Example of using synchronization:
 * <p>
 * <pre>
 * for (entry: registryAccess.listEntries("myPath", 10)) {
 *   try {
 *     registryAccess.move(entry, "myPath.taken"); // Moving entry to the path where noone will search for it (blocking)
 *     // We successfully owned entry
 *     doLotsOfWorkWithEntry(entry);
 *     ...
 *   } catch (UnsyncRegistryEntryException e) {
 *     // This is normal situation, ignoring
 *     continue;
 *   }
 *   registryAccess.move(entry, "myPath"); // Returning back entry for future use by someone else
 *   // We did our job
 * }
 * </pre>
 * <p>
 * </p>
 */
public interface RemoteRegistryAccess extends Remote {
  /**
   * Lists entries under path ordered by update time. Older elements goes
   * first.
   *
   * @param path
   * @param maxSize
   * @return list of registry entries. It can be shorter than
   * <code>maxSize</code> but never null
   */
  RegistryEntry[] listEntries(String path, int maxSize) throws RemoteException;

  /**
   * Moves registry entry to another path
   *
   * @param entry
   * @param path
   * @return moved registry entry
   * @throws UnsyncRegistryEntryException if entry was updated elsewhere between list and remove
   */
  RegistryEntry move(RegistryEntry entry, String path) throws UnsyncRegistryEntryException, DataModificationException, RemoteException;

  /**
   * Removes entry
   *
   * @param entry
   * @throws UnsyncRegistryEntryException if entry was updated elsewhere between list and remove
   */
  void remove(RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException, RemoteException;

  /**
   * Adds value to the path. It will add values until meet limitations of
   * maxSize. Under limit of maxSize oldest values rewritten with new ones Will
   * remove oldest value if size of list become greater than maxSize
   *
   * @param path
   * @param value
   * @param maxSize
   */
  void add(String path, String value, int maxSize) throws RemoteException;

  /**
   * Lists all paths in order from oldest to newer
   *
   * @return all paths in the registry
   */
  String[] listAllPaths() throws RemoteException;

  /**
   * Removes all values under path
   *
   * @param path string path (can be anything, but preferred to be dot
   *             separated path in java-like style)
   */
  void removePath(String path) throws RemoteException;

}

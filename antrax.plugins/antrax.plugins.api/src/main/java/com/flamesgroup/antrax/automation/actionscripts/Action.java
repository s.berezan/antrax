/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.actionscripts;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Action used by
 * {@linkplain RegisteredInGSMChannel#executeAction(Action, long) execute
 * action from business activity scripts}
 * <p>
 * Action should be supported by both requester and provider.
 * </p>
 * <p>
 * For example you can use such action for asking for call:
 * </p>
 * <p>
 * <p>
 * <pre>
 * myPhoneNumber = simCardStatistic.getCalledPhoneNumber(); // my phone number
 * boolean dropYourself = true; // a accept provider to drop call by itself
 * action = new Action(&quot;make-a-call&quot;, myPhoneNumber, dropYourself);
 * </pre>
 * <p>
 * </p>
 *
 * @see BusinessActivityScript
 * @see RegisteredInGSMChannel
 */
public class Action implements Serializable {

  private static final long serialVersionUID = 8060240370753754075L;

  private final String action;
  private final Serializable[] args;

  /**
   * Constructs action with arguments.
   *
   * @param action
   * @param arguments
   */
  public Action(final String action, final Serializable... arguments) {
    assert action != null;
    this.action = action;
    this.args = arguments;
  }

  /**
   * Returns action name
   *
   * @return action name
   */
  public String getAction() {
    return action;
  }

  /**
   * Returns action arguments
   * <p>
   * Be careful to check for argument types
   * </p>
   *
   * @return arguments of an Action
   */
  public Serializable[] getArgs() {
    return args;
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof Action)) {
      return false;
    }

    Action that = (Action) obj;
    if (!this.action.equals(that.action)) {
      return false;
    }
    return Arrays.deepEquals(this.args, that.args);
  }

  @Override
  public int hashCode() {
    return action.hashCode() + 15 * Arrays.hashCode(args);
  }

  @Override
  public String toString() {
    return action + Arrays.toString(args);
  }

}

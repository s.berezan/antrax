/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.commons.DeviceJournalHelper;
import com.flamesgroup.antrax.commons.DeviceLoggerHelper;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.utils.AliasDeviceUID;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.LicenseState;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.protocol.cudp.ExpireTime;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SpiGsmChannelManager implements IGsmChannelManager {

  private static final Logger logger = LoggerFactory.getLogger(SpiGsmChannelManager.class);

  private static final long UPDATE_LICENSE_CURRENT_TIME_INTERVAL = 50 * 60 * 1000; // 50 minutes

  private final DeviceUID deviceUID;
  private final IGSMDevice gsmDevice;

  private final UnitActivator activator;
  private final IConfigurator configurator;
  private final IActivityLogger activityLogger;

  private final List<GSMUnit> activeGsmUnits = new ArrayList<>();

  private IGsmChannelManagerHandler channelManagerHandler;

  private ThreadPoolExecutor channelExecutor;

  private Timer updateLicenseCurrentTimeTimer;

  public SpiGsmChannelManager(final DeviceUID deviceUID, final IGSMDevice gsmDevice, final UnitActivator activator, final IConfigurator configurator, final IActivityLogger activityLogger) {
    this.deviceUID = deviceUID;
    this.gsmDevice = gsmDevice;
    this.activator = activator;
    this.configurator = configurator;
    this.activityLogger = activityLogger;
  }

  @Override
  public synchronized void start(final IGsmChannelManagerHandler channelManagerHandler) {
    if (this.channelManagerHandler != null) {
      throw new IllegalStateException(String.format("[%s] - SimChannelManager has already connected", this));
    }

    channelExecutor = new WaitThreadPoolExecutor(6, 24, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(6), r -> {
      Thread thread = new Thread(r);
      thread.setName("GsmChannelExecutor@" + thread.hashCode());
      return thread;
    });

    List<ChannelUID> declaredChannels = new ArrayList<>();
    for (int i = 0; i < gsmDevice.getGSMEntryCount(); i++) {
      declaredChannels.add(createChannelUID(deviceUID, (byte) i));
    }

    try {
      channelManagerHandler.handleDeclaredChannels(AntraxProperties.SERVER_NAME, declaredChannels);
    } catch (RemoteException e) {
      logger.error("[{}] - can't handle declared channels", this, e);
      return;
    }

    declaredChannels.forEach(c -> activityLogger.logDeclaredGsmUnit(configurator.configure(c)));
    try {
      gsmDevice.attach();
    } catch (MudpException | InterruptedException e) {
      logger.error("[{}] - can't attach to [{}]", deviceUID.getCanonicalName(), e);
      return;
    }

    activator.start(channelManagerHandler);
    this.channelManagerHandler = channelManagerHandler;

    for (ICPUEntry cpuEntry : gsmDevice.getCPUEntries()) {
      channelExecutor.execute(new ChannelReadConfigTask(cpuEntry));
    }

    try {
      DeviceJournalHelper.readJournal(gsmDevice, deviceUID);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't read device journal", this, e);
    }

    try {
      DeviceLoggerHelper.startLogDevice(gsmDevice, deviceUID);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't start LogDevice", this, e);
    }

    updateLicenseCurrentTimeTimer = new Timer();
    updateLicenseCurrentTimeTimer.schedule(new UpdateLicenseCurrentTime(), 0, UPDATE_LICENSE_CURRENT_TIME_INTERVAL);
  }

  @Override
  public synchronized void stop() {
    if (channelManagerHandler == null) {
      return;
    }

    updateLicenseCurrentTimeTimer.cancel();
    updateLicenseCurrentTimeTimer = null;

    for (GSMUnit gsmUnit : activeGsmUnits) {
      channelExecutor.execute(new ChannelRemoveTask(gsmUnit));
    }

    channelExecutor.shutdown();
    try {
      while (!channelExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of DeviceRemoveExecutor", this);
      }
    } catch (InterruptedException e) {
      logger.debug("[{}] - while awaiting completion of executor", this, e);
    }

    activator.stop();
    channelManagerHandler = null;

    try {
      DeviceLoggerHelper.stopLogDevice(gsmDevice);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't stop LogDevice", this, e);
    }

    try {
      gsmDevice.detach();
    } catch (MudpException | InterruptedException e) {
      logger.warn("[{}] - can't detach from device {}", this, gsmDevice, e);
    }
  }

  private ChannelUID createChannelUID(final DeviceUID deviceUID, final byte channelNumber) {
    return new ChannelUID(deviceUID, AliasDeviceUID.getAlias(deviceUID), channelNumber);
  }

  private class ChannelReadConfigTask implements Runnable {

    private final ICPUEntry cpuEntry;

    public ChannelReadConfigTask(final ICPUEntry cpuEntry) {
      this.cpuEntry = cpuEntry;
    }

    @Override
    public void run() {
      IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
      try {
        serviceChannel.enslave();
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't enslave service channel for entry {}", this, cpuEntry, e);
        return;
      }

      ChannelConfig channelConfig;
      try {
        long expireTime = serviceChannel.readLicenseExpireTime();
        LicenseState licenseState = serviceChannel.getLicenseState();
        channelConfig = new ChannelConfig(new ExpireTime(expireTime), convertLicenseState2ChannelState(licenseState));
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't create channel config for entry {}", this, cpuEntry, e);
        return;
      } finally {
        try {
          serviceChannel.free();
        } catch (ChannelException e) {
          logger.warn("[{}] - can't free service channel for entry {}", this, cpuEntry, e);
        }
      }

      int cpuEntryCount = gsmDevice.getCPUEntryCount();
      int gsmEntryCount = gsmDevice.getGSMEntryCount();
      int multiplier = gsmEntryCount / cpuEntryCount;
      for (int i = 0; i < multiplier; i++) {
        IGSMEntry gsmEntry = gsmDevice.getGSMEntries().get(cpuEntry.getNumber() * multiplier + i);
        channelExecutor.execute(new ChannelAddTask(gsmEntry, channelConfig));
      }
    }

    private ChannelConfig.ChannelState convertLicenseState2ChannelState(final LicenseState licenseState) {
      switch (licenseState) {
        case ACTIVE:
          return ChannelConfig.ChannelState.ACTIVE;
        case LICENSE_STATE_FREEZE:
          return ChannelConfig.ChannelState.LICENSE_INVALID;
        case LICENSE_STATE_EXPIRE:
          return ChannelConfig.ChannelState.LICENSE_EXPIRE;
        case LICENSE_STATE_LOCK:
          return ChannelConfig.ChannelState.LICENSE_LOCK;
        default:
          return null;
      }
    }

  }

  private class ChannelAddTask implements Runnable {

    private final IGSMEntry gsmEntry;
    private final ChannelConfig channelConfig;

    private final ChannelUID channel;

    public ChannelAddTask(final IGSMEntry gsmEntry, final ChannelConfig channelConfig) {
      this.gsmEntry = gsmEntry;
      this.channelConfig = channelConfig;

      channel = createChannelUID(deviceUID, (byte) gsmEntry.getNumber());
    }

    @Override
    public void run() {
      GSMUnit gsmUnit = new GSMUnit(gsmEntry, channel, channelManagerHandler);
      configurator.configure(gsmUnit);

      try {
        gsmUnit.plugIn();
      } catch (GsmChannelError e) {
        logger.error("[{}] - can't plug in for channel {}", this, channel, e);
        return;
      }

      try {
        channelManagerHandler.handleAvailableChannel(AntraxProperties.SERVER_NAME, channel);
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle available channel {}", this, channel, e);
        return;
      }

      activator.addGSMUnit(gsmUnit);
      activityLogger.logActiveGsmUnit(channel, gsmUnit.getGsmChannel(), channelConfig);
      activeGsmUnits.add(gsmUnit);
    }

    @Override
    public String toString() {
      return "ChannelAddTask[" + channel + "]";
    }

  }

  private class ChannelRemoveTask implements Runnable {

    private final GSMUnit gsmUnit;

    public ChannelRemoveTask(final GSMUnit gsmUnit) {
      this.gsmUnit = gsmUnit;
    }

    @Override
    public void run() {
      gsmUnit.unplug();
      activator.removeGSMUnit(gsmUnit);
      activityLogger.logGSMUnitReleased(gsmUnit.getChannelUID());

      try {
        channelManagerHandler.handleLostChannels(AntraxProperties.SERVER_NAME, Collections.singletonList(gsmUnit.getChannelUID()));
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle lost channel {}", this, gsmUnit, e);
      }
    }

    @Override
    public String toString() {
      return "ChannelRemoveTask[" + gsmUnit + "]";
    }

  }

  private final class UpdateLicenseCurrentTime extends TimerTask {

    @Override
    public void run() {
      long currentTime = System.currentTimeMillis();
      try {
        for (ICPUEntry cpuEntry : gsmDevice.getCPUEntries()) {
          IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
          try {
            serviceChannel.enslave();
          } catch (ChannelException e) {
            logger.warn("[{}] - can't enslave service channel to update license current time for {}", this, cpuEntry, e);
            continue;
          }

          try {
            serviceChannel.updateLicenseCurrentTime(currentTime);
          } catch (ChannelException e) {
            logger.warn("[{}] - can't update license current time for {}", this, cpuEntry, e);
          } finally {
            try {
              serviceChannel.free();
            } catch (ChannelException e) {
              logger.warn("[{}] - can't free service channel after update license current time for {}", this, cpuEntry, e);
            }
          }
        }
      } catch (InterruptedException e) {
        logger.warn("[{}] - while update license current time was interrupted", this, e);
      }
    }

  }

}

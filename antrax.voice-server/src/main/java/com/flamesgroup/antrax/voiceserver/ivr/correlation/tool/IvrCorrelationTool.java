/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation.tool;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.AudioDataCorrelationHelper;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.Correlator;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplate;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.ProcessCorrelationData;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.TemplateCorrelationData;
import com.flamesgroup.device.tool.parameter.CommonParameters;

import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;

public class IvrCorrelationTool {

  public static final int FRAME_SIZE = 160;

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    IvrCorrelationToolParameters commonParameters = new IvrCorrelationToolParameters();

    clp.addObject(commonParameters);

    if (args.length == 0) {
      extendedUsage(clp);
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        extendedUsage(clp);
      } else {
        byte[] templateData = Files.readAllBytes(commonParameters.getTemplateDataPath());
        byte[] processData = Files.readAllBytes(commonParameters.getProcessDataPath());

        double[] templateAvgData = new double[templateData.length / FRAME_SIZE];
        for (int i = 0; i < templateAvgData.length; i++) {
          templateAvgData[i] = AudioDataCorrelationHelper.avgAsAbsShort(templateData, i * FRAME_SIZE, FRAME_SIZE);
        }
        TemplateCorrelationData tcd = new TemplateCorrelationData(templateAvgData);
        ProcessCorrelationData pcd = new ProcessCorrelationData(tcd.getLength());

        double correlateMax = Double.MIN_VALUE;
        int correlateMaxIndex = -1;
        for (int i = 0; i < processData.length / FRAME_SIZE; i++) {
          double processAvgFrame = AudioDataCorrelationHelper.avgAsAbsShort(processData, i * FRAME_SIZE, FRAME_SIZE);
          pcd.add(processAvgFrame);

          double correlate = Correlator.correlate(tcd, pcd);
          if (correlate > correlateMax) {
            correlateMax = correlate;
            correlateMaxIndex = i;
          }
        }

        System.out.println("Correlation max: " + correlateMax + " on position: " + correlateMaxIndex
            + " and time: " + (correlateMaxIndex * FRAME_SIZE / IvrTemplate.BIT_RATE_IN_BYTES) + " seconds");
      }
    }
  }

  private static void extendedUsage(final JCommander clp) {
    StringBuilder sb = new StringBuilder();
    clp.usage(sb);
    sb.append("\n");
    sb.append("For example: IvrCorrelationTool --templateDataPath ~/test_ivr/no_money#16.sln  --processDataPath ~/test_ivr/gsm_to_voip.sln\n");
    JCommander.getConsole().println(sb.toString());
  }

  public static class IvrCorrelationToolParameters extends CommonParameters {

    @Parameter(names = "--templateDataPath", required = true, description = "Path to ivr template file")
    private Path templateDataPath;

    @Parameter(names = "--processDataPath", required = true, description = "Path to full ivr processing file")
    private Path processDataPath;

    public Path getTemplateDataPath() {
      return templateDataPath;
    }

    public Path getProcessDataPath() {
      return processDataPath;
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerBasicAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.ImsiCatcher;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmConnectError;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import com.flamesgroup.antrax.voiceserver.channels.error.CallManagementError;
import com.flamesgroup.antrax.voiceserver.channels.error.DTMFError;
import com.flamesgroup.antrax.voiceserver.channels.error.DialingError;
import com.flamesgroup.antrax.voiceserver.channels.error.MediaManagementError;
import com.flamesgroup.antrax.voiceserver.channels.error.SetupIMEIError;
import com.flamesgroup.antrax.voiceserver.channels.error.USSDError;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.OperatorSelectionMode;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.IChannelData;
import com.flamesgroup.device.channel.IChannelDataHandler;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.gsmb.atengine.ATEngineTimeoutException;
import com.flamesgroup.device.gsmb.atengine.at.ATErrorStatusException;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.sc.SCReaderSubChannelsJoiningException;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.unit.CellAdjustment;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.IAudioStream;
import com.flamesgroup.unit.ICellEqualizer;
import com.flamesgroup.unit.IIncomingMobileCallHandler;
import com.flamesgroup.unit.IIncomingSMSHandler;
import com.flamesgroup.unit.IMobileOriginatingCall;
import com.flamesgroup.unit.IMobileOriginatingCallHandler;
import com.flamesgroup.unit.IMobileStationUnit;
import com.flamesgroup.unit.IMobileStationUnitExternalErrorHandler;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.IServiceInfoHandler;
import com.flamesgroup.unit.MobileStationUnitSimInsertedTimeoutException;
import com.flamesgroup.unit.NetworkSurveyInfo;
import com.flamesgroup.unit.Options;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.ms.GSMSiemensMobileStationUnit;
import com.flamesgroup.unit.ms.GSMTelitEasyScanUnit;
import com.flamesgroup.unit.ms.GSMTelitMobileStationUnit;
import com.flamesgroup.unit.ms.ITelitMobileStationTraceHandler;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSMessageClass;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.utils.AntraxProperties;
import com.flamesgroup.utils.OperatorsStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.rmi.RemoteException;
import java.security.CodeSource;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class GSMUnit implements IGSMUnit {

  private static final Logger logger = LoggerFactory.getLogger(GSMUnit.class);

  private static byte[] ringingControlSignal;
  private static final List<byte[]> fakeAnswers;

  private final IGSMEntry gsmEntry;
  private final ChannelUID channelUID;
  private final IGsmChannelManagerHandler gsmChannelManagerHandler;

  private IMobileStationUnit mobileStationUnit;

  private volatile boolean owned = false;
  private volatile int hardwareErrorCount = 0;
  private volatile int initErrorCount = 0;
  private final AtomicBoolean connect = new AtomicBoolean();
  private CellEqualizerExecutor cellEqualizerExecutor;

  private volatile SIMUnit simUnit;
  private IMobileOriginatingCall originatingCall;

  private final AtomicBoolean plugged = new AtomicBoolean();

  private final Lock mobileStationUnitLock = new ReentrantLock();

  private IRemoteHistoryLogger historyLogger;
  private IActivityLogger activityLogger;
  private ImsiCatcher imsiCatcher;
  private CellEqualizerManager cellEqualizerManager;

  private final AtomicReference<GSMChannel> gsmChannel = new AtomicReference<>();

  private IMediaStream mediaStream;

  static {
    String resourceName = "ringing_control.pcm16LSBMSB.pcm";
    InputStream in = GSMUnit.class.getResourceAsStream("/" + resourceName);
    if (in == null) {
      logger.warn("[{}] - can't find resource {} in classpath", GSMUnit.class, resourceName);
    } else {
      in = new BufferedInputStream(in);
      try {
        ringingControlSignal = new byte[in.available()];
        in.read(ringingControlSignal, 0, ringingControlSignal.length);
      } catch (IOException e) {
        logger.warn("[{}] - can't read bytes from {}", GSMUnit.class, resourceName, e);
        ringingControlSignal = null;
      }
    }
    fakeAnswers = getFakeAnswers(GSMUnit.class, "/fake_answer");
  }

  public GSMUnit(final IGSMEntry gsmEntry, final ChannelUID channelUID, final IGsmChannelManagerHandler gsmChannelManagerHandler) {
    this.channelUID = channelUID;
    this.gsmEntry = gsmEntry;
    this.gsmChannelManagerHandler = gsmChannelManagerHandler;
  }

  @Override
  public ChannelUID getChannelUID() {
    return channelUID;
  }

  public IGSMEntry getGsmEntry() {
    return gsmEntry;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof GSMUnit)) {
      return false;
    }
    GSMUnit that = (GSMUnit) object;

    return channelUID.equals(that.channelUID);
  }

  @Override
  public int hashCode() {
    return channelUID.hashCode();
  }

  @Override
  public String toString() {
    return channelUID.toString();
  }

  @Override
  public void connect(final SIMUnit simUnit, final IIncomingMobileCallHandler incomingMobileCallHandler, final IIncomingSMSHandler incomingSMSHandler,
      final IServiceInfoHandler serviceInfoHandler, final IMobileStationUnitInternalErrorHandler mobileStationUnitInternalErrorHandler, final CellEqualizerManager cellEqualizerManager,
      final IMobileStationUnitExternalErrorHandler mobileStationUnitExternalErrorHandler, final ISCReaderSubChannel scReaderSubChannel, final IMEI imei, final String description)
      throws GsmChannelError {
    Objects.requireNonNull(simUnit, "simUnit should not be null");
    Objects.requireNonNull(incomingMobileCallHandler, "incomingMobileCallHandler should not be null");
    Objects.requireNonNull(incomingSMSHandler, "incomingSMSHandler should not be null");
    Objects.requireNonNull(serviceInfoHandler, "serviceInfoHandler should not be null");

    mobileStationUnitLock.lock();
    try {
      switch (channelUID.getDeviceUID().getDeviceType()) {
        case GSMB3:
        case GSMBOX4:
        case GSMBOX8:
          String traceMask = null;
          ITelitMobileStationTraceHandler traceHandler = null;
          if (VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerTelitTraceEnabled()) {
            traceMask = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerTelitTraceMask();
            Path tracePath = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerTelitTracePath();
            traceHandler = new TelitMobileStationTraceFileWriterHandler(tracePath);
          }

          executeNetworkSurvey(simUnit, description);

          mobileStationUnit = new GSMTelitMobileStationUnit(gsmEntry.getGSMChannel(), gsmEntry.getAudioChannel(), new DummySCReaderChannel(scReaderSubChannel),
              mobileStationUnitInternalErrorHandler, mobileStationUnitExternalErrorHandler, description, traceMask, traceHandler);
          break;
        case GSMB:
        case GSMB2:
          mobileStationUnit = new GSMSiemensMobileStationUnit(gsmEntry.getGSMChannel(), gsmEntry.getAudioChannel(), gsmEntry.getSCEmulatorChannel(),
              new DummySCReaderChannel(scReaderSubChannel), mobileStationUnitInternalErrorHandler, description);
          break;
      }

      setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.INIT);
      activityLogger.logGsmUnitChangedState(channelUID, CallChannelState.State.SETTING_IMEI, "setting IMEI", -1);
      try {
        logger.debug("[{}] - setting IMEI [{}]", this, imei);
        mobileStationUnit.setIMEI(imei.toByteArray());
      } catch (ChannelException | InterruptedException e) {
        activityLogger.logGsmUnitChangedState(channelUID, null, "", -1);
        setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.NONE);
        mobileStationUnit = null;

        logger.warn("[{}] - failed to set IMEI", this, e);
        GsmChannelError error = new SetupIMEIError(e.getMessage(), e);
        handleInitError(error);
        throw error;
      }

      activityLogger.logGsmUnitChangedState(channelUID, CallChannelState.State.TURNING_ON_MODULE, "turning on module", -1);
      try {
        mobileStationUnit.turnOn(getOptions(simUnit), incomingMobileCallHandler, incomingSMSHandler, serviceInfoHandler);
      } catch (ChannelException | InterruptedException e) {
        setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.NONE);
        mobileStationUnit = null;

        GsmChannelError error = new GsmConnectError("Cannot turn on mobileStationUnit: " + e.getMessage(), e);
        // FIXME: refactor ugly inc init module error count
        if (e instanceof SCReaderSubChannelsJoiningException || e instanceof MobileStationUnitSimInsertedTimeoutException) {
          simUnit.handleSCReaderChannelError();
        } else {
          handleInitError(error);
        }

        throw error;
      } finally {
        activityLogger.logGsmUnitChangedState(channelUID, null, "", -1);
      }

    } finally {
      mobileStationUnitLock.unlock();
    }

    simUnit.handleEvent(SimEvent.imeiSetupped(imei));

    try {
      this.executeCellEqualizer(cellEqualizerManager.getServerCellAdjustments());
      this.cellEqualizerManager = cellEqualizerManager;
    } catch (GsmHardwareError e) {
      mobileStationUnitLock.lock();
      try {
        mobileStationUnit.turnOff();
      } catch (InterruptedException | ChannelException ex) {
        ex.addSuppressed(e);
        handleHardwareError(new GsmHardwareError("Cannot turn off mobileStationUnit", ex));
        throw e;
      } finally {
        mobileStationUnit = null;
        mobileStationUnitLock.unlock();
      }
      throw e;
    }

    simUnit.connect();
    setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.READY);

    this.simUnit = simUnit;
    this.hardwareErrorCount = 0;
    this.initErrorCount = 0;
    this.connect.set(true);
  }

  public boolean isConnect() {
    return connect.get();
  }

  public void executeCellEqualizer(final long lastTimeUpdateCellAdjustments, final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws GsmHardwareError {
    if (gsmChannel.get().getLockArfcn() != null || cellEqualizerAlgorithm == null) {
      return;
    }
    if (cellEqualizerAlgorithm instanceof CellEqualizerBasicAlgorithm) {
      if (!(cellEqualizerExecutor instanceof CellEqualizerBasicAlgorithmExecutor)) {
        if (cellEqualizerExecutor != null) {
          cellEqualizerExecutor.terminate();
        }
        cellEqualizerExecutor = new CellEqualizerBasicAlgorithmExecutor(this, cellEqualizerManager);
      }
      CellEqualizerBasicAlgorithmExecutor cellEqualizerBasicAlgorithmExecutor = (CellEqualizerBasicAlgorithmExecutor) cellEqualizerExecutor;
      cellEqualizerBasicAlgorithmExecutor.setLastTimeUpdateCellAdjustments(lastTimeUpdateCellAdjustments);
    } else if (cellEqualizerAlgorithm instanceof CellEqualizerRandomAlgorithm) {
      if (!(cellEqualizerExecutor instanceof CellEqualizerRandomAlgorithmExecutor)) {
        if (cellEqualizerExecutor != null) {
          cellEqualizerExecutor.terminate();
        }
        cellEqualizerExecutor = new CellEqualizerRandomAlgorithmExecutor(this, cellEqualizerManager);
      }
    } else if (cellEqualizerAlgorithm instanceof CellEqualizerStatisticAlgorithm) {
      if (!(cellEqualizerExecutor instanceof CellEqualizerStatisticAlgorithmExecutor)) {
        if (cellEqualizerExecutor != null) {
          cellEqualizerExecutor.terminate();
        }
        cellEqualizerExecutor = new CellEqualizerStatisticAlgorithmExecutor(this, cellEqualizerManager);
      }
    } else {
      throw new IllegalStateException("Unsupported value: " + cellEqualizerAlgorithm.getClass().getName());
    }
    cellEqualizerExecutor.execute(cellEqualizerAlgorithm);
  }

  public void executeCellEqualizer(final Map<Integer, Integer> cellAdjustments) throws GsmHardwareError {
    if (gsmChannel.get().getLockArfcn() != null) {
      return;
    }
    DeviceType deviceType = channelUID.getDeviceUID().getDeviceType();
    if (deviceType == DeviceType.GSMB || deviceType == DeviceType.GSMB2 || !VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCellEqualizerEnabled()) {
      return;
    }

    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit == null || originatingCall != null) {
        return;
      }
      logger.debug("[{}] - try execute CellEqualizer", this);
      logger.trace("[{}] - cellAdjustments [{}]", this, cellAdjustments);
      activityLogger.logGsmUnitChangedState(channelUID, CallChannelState.State.CELL_EQUALIZER, "cell equalizer", -1);
      ICellEqualizer cellEqualizer = mobileStationUnit.getCellEqualizer();
      if (!cellAdjustments.isEmpty()) {
        List<CellAdjustment> currentCellAdjustments = cellEqualizer.read();
        logger.trace("[{}] - currentCellAdjustments [{}]", this, currentCellAdjustments.stream().map(ca -> "[" + ca.getArfcn() + "=" + ca.getRxLevAdjust() + "]").collect(Collectors.joining(", ")));
        List<CellAdjustment> diffCellAdjustments = new ArrayList<>();
        List<CellAdjustment> newCellAdjustmentList = cellAdjustments.entrySet().stream().map(e -> new CellAdjustment(e.getKey(), e.getValue())).collect(Collectors.toList());
        diffCellAdjustments.addAll(newCellAdjustmentList.stream().filter(cellAdjustment -> !currentCellAdjustments.contains(cellAdjustment)).collect(Collectors.toList()));
        for (CellAdjustment currentCellAdjustment : currentCellAdjustments) {
          if (!newCellAdjustmentList.contains(currentCellAdjustment)) {
            boolean containsArfcn = false;
            for (CellAdjustment cellAdjustment : newCellAdjustmentList) {
              if (cellAdjustment.getArfcn() == currentCellAdjustment.getArfcn()) {
                containsArfcn = true;
              }
            }
            if (!containsArfcn) {
              diffCellAdjustments.add(new CellAdjustment(currentCellAdjustment.getArfcn(), 0));
            }
          }
        }
        logger.debug("[{}] - try write diffCellAdjustments [{}]",
            this, diffCellAdjustments.stream().map(ca -> "[" + ca.getArfcn() + "=" + ca.getRxLevAdjust() + "]").collect(Collectors.joining(", ")));
        for (CellAdjustment adjustment : diffCellAdjustments) {
          cellEqualizer.write(new CellAdjustment(adjustment.getArfcn(), adjustment.getRxLevAdjust()));
        }
      }
    } catch (ChannelException | InterruptedException e) {
      throw new GsmHardwareError(String.format("[%s] - can't execute CellEqualizer", this), e);
    } finally {
      CallChannelState.State state = simUnit == null ? null : simUnit.getShowState();
      activityLogger.logGsmUnitChangedState(channelUID, state, "", -1);
      mobileStationUnitLock.unlock();
    }
    logger.debug("[{}] - executed CellEqualizer", this);
  }

  private Options getOptions(final SIMUnit simUnit) {
    String operatorCode = null;
    String operatorSelection = simUnit.getSimData().getSimGroup().getOperatorSelection();
    if (operatorSelection.equals("home")) {
      OperatorsStorage.Operator operator = OperatorsStorage.getInstance().findOperatorByImsi(simUnit.getIMSI());
      if (operator == null) {
        logger.warn("[{}] - can't find home operator in the operators list, use auto selection mode", this);
      } else {
        operatorCode = operator.getCode();
        logger.info("[{}] - select home operator {}", this, operator);
      }
    } else if (!operatorSelection.equals("auto")) {
      operatorCode = operatorSelection; // custom operator selection - contains operator code
    }

    simUnit.setOperatorSelectionMode(operatorCode == null ? OperatorSelectionMode.AUTOMATIC : OperatorSelectionMode.MANUAL);

    boolean suppressIncomingCallSignal = simUnit.getSimData().getSimGroup().isSuppressIncomingCallSignal();

    long registrationTimeout = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreRegistrationTimeout();

    long ignoreRegistrationDeniedTimeout = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreIgnoreRegistrationDeniedTimeout();

    boolean disablePhase2pSupport = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreRegistrationPhase2PlusDisable();

    boolean disableSat = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreSatDisable();

    boolean disableVolumeOfIncomingSms = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIncomingSmsVolumeDisabled();

    int cpuMode = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreCpuMode();

    Options.Builder builder = new Options.Builder().operatorCode(operatorCode).suppressIncomingCallSignal(suppressIncomingCallSignal)
        .registrationTimeout(registrationTimeout).igonreRegistrationDeniedTimeout(ignoreRegistrationDeniedTimeout)
        .cellLock(gsmChannel.get().getLockArfcn()).disableVolumeOfIncomingSms(disableVolumeOfIncomingSms).cpuMode(cpuMode);
    if (disablePhase2pSupport) {
      builder.disablePhase2pSupport();
    }
    if (disableSat) {
      builder.disableSat();
    }
    return builder.build();
  }

  public IActivityLogger getActivityLogger() {
    return activityLogger;
  }

  @Override
  public void disconnect() {
    setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.DEINIT);
    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit != null) {
        try {
          mobileStationUnit.turnOff();
        } catch (ChannelException | InterruptedException e) {
          // FIXME: refactor ugly inc hardware module error count
          if (!(e instanceof SCReaderSubChannelsJoiningException)) {
            handleHardwareError(new GsmHardwareError("Cannot turn off mobileStationUnit", e));
          }
        }
      }
    } finally {
      mobileStationUnit = null;
      mobileStationUnitLock.unlock();
    }

    if (simUnit != null) {
      simUnit.disconnect();
      simUnit = null;
    }

    if (getGsmChannel().isLock()) {
      setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.BUSINESS_PROBLEM);
    } else {
      setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.NONE);
    }

    hardwareErrorCount = 0;
    initErrorCount = 0;
    connect.set(false);
    cellEqualizerExecutor = null;
  }

  @Override
  public void dropCall() throws GsmChannelError {
    if (originatingCall == null) {
      return;
    }

    try {
      originatingCall.drop();
    } catch (ChannelException | InterruptedException e) {
      throw new CallManagementError(String.format("[%s] - can't drop call", this), e);
    } finally {
      originatingCall = null;
    }
    hardwareErrorCount = 0;
  }

  @Override
  public void turnOffAudio() {
    setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.READY);
    if (mediaStream.isOpen()) {
      mediaStream.close();
    }

    IAudioStream audioStream = mobileStationUnit.getAudioStream();
    try {
      audioStream.close();
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't stop audio", this, e);
    }
  }

  @Override
  public void dial(final PhoneNumber number, final IMobileOriginatingCallHandler originatingCallHandler) throws GsmChannelError {
    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit != null) {
        originatingCall = mobileStationUnit.dial(number, originatingCallHandler);

        hardwareErrorCount = 0;
      } else {
        throw new DialingError(String.format("[%s] - can't dial - mobileStationUnit isn't created", this));
      }
    } catch (ATEngineTimeoutException e) {
      throw new GsmHardwareError(String.format("[%s] - can't dial", this), e);
    } catch (ATErrorStatusException e) {
      throw new DialingError(e.getStatus());
    } catch (ChannelException | InterruptedException e) {
      GsmHardwareError error = new GsmHardwareError(String.format("[%s] - can't dial", this), e);
      handleHardwareError(error);
      throw error;
    } finally {
      mobileStationUnitLock.unlock();
    }
  }

  public CyclicBuffer getRingingControlSignal() {
    return new CyclicBuffer(ringingControlSignal);
  }

  public CyclicBuffer getFakeAnswerSignal() {
    if (fakeAnswers.isEmpty()) {
      if (ringingControlSignal == null) {
        return null;
      } else {
        return new CyclicBuffer(ringingControlSignal);
      }
    } else {
      int randomIndex = new Random().nextInt(fakeAnswers.size());
      return new CyclicBuffer(fakeAnswers.get(randomIndex));
    }
  }

  @Override
  public void turnOnAudio(final IMediaStream mediaStreamLocal) throws GsmChannelError {
    setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.ACTIVE);

    mediaStream = mediaStreamLocal;
    IAudioStream audioStream = mobileStationUnit.getAudioStream();
    try {
      audioStream.open((timestamp, data, offset, length) -> {
        mediaStream.write((int) timestamp, data, offset, length); // TODO: remove cast to int after change signature of interface method
      });
    } catch (ChannelException | InterruptedException e) {
      GsmHardwareError error = new MediaManagementError("Can't start audio: " + e.getMessage(), e);
      handleHardwareError(error);
      throw error;
    }
    hardwareErrorCount = 0;
  }

  @Override
  public IUSSDSession openUSSDSession(final String ussd) throws GsmChannelError {
    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit != null) {
        IUSSDSession retval = mobileStationUnit.getUSSDSession();
        retval.open(ussd);
        return retval;
      }
      throw new USSDError(String.format("[%s] - can't open USSD session [%s] - mobileStationUnit isn't turned on", this, ussd));
    } catch (ChannelException | InterruptedException e) {
      throw new USSDError(String.format("[%s] - can't open USSD session [%s]", this, ussd), e);
    } finally {
      mobileStationUnitLock.unlock();
    }
  }

  @Override
  public void activateRinging() {
    mediaStream.open((timestamp, data, offset, length) -> {
      IAudioStream audioStream = mobileStationUnit.getAudioStream();
      try {
        audioStream.write(timestamp, data, offset, length);
      } catch (ChannelException e) {
        logger.warn("[{}] - can't write audio media data with timestamp [{}] to [{}]", this, timestamp, audioStream, e);
      }
    });
  }

  @Override
  public void sendDTMF(final String dtmfString) throws GsmChannelError {
    if (originatingCall == null) {
      throw new GsmHardwareError(String.format("[%s] - can't send DTMF [%s] - there is no active call at this moment", this, dtmfString));
    }
    try {
      originatingCall.sendDTMF(dtmfString);
      hardwareErrorCount = 0;
    } catch (IllegalArgumentException ignored) {
      throw new DTMFError("Is not allowed string [" + dtmfString + "] for sending DTMF");
    } catch (ChannelException | InterruptedException e) {
      GsmHardwareError error = new GsmHardwareError(String.format("[%s] - can't send DTMF [%s]", this, dtmfString), e);
      handleHardwareError(error);
      throw error;
    }
  }

  @Override
  public void sendDTMF(final char dtmf, final int duration) throws GsmChannelError {
    if (originatingCall == null) {
      throw new GsmHardwareError(String.format("[%s] - can't send DTMF [%c] with duration [%d] - there is no active call at this moment", this, dtmf, duration));
    }
    try {
      originatingCall.sendDTMF(dtmf, duration);
      hardwareErrorCount = 0;
    } catch (IllegalArgumentException ignored) {
      throw new DTMFError("Is not allowed character [" + dtmf + "] for sending DTMF");
    } catch (ChannelException | InterruptedException e) {
      GsmHardwareError error = new GsmHardwareError(String.format("[%s] - can't send DTMF [%c] with duration [%d]", this, dtmf, duration), e);
      handleHardwareError(error);
      throw error;
    }
  }

  public void plugIn() throws GsmChannelError {
    if (plugged.compareAndSet(false, true)) {
      IIndicationChannel indicationChannel = gsmEntry.getIndicationChannel();
      try {
        indicationChannel.enslave();
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't enslave indicationChannel [{}]", this, gsmEntry, e);
        throw new GsmConnectError(e.getMessage(), e);
      }

      GSMChannel gsmChannelLocal = gsmChannel.get();
      if (gsmChannelLocal != null && gsmChannelLocal.isLock()) {
        setIndicationModeLocal(indicationChannel, IndicationMode.BUSINESS_PROBLEM);
      }
    }
  }

  public boolean isPlugged() {
    return plugged.get();
  }

  public void unplug() {
    if (plugged.compareAndSet(true, false)) {
      IIndicationChannel indicationChannel = gsmEntry.getIndicationChannel();
      try {
        indicationChannel.free();
      } catch (ChannelException e) {
        logger.warn("[{}] - can't free indicationChannel [{}]", this, gsmEntry, e);
      }
    }
  }

  @Override
  public List<SMSMessageOutbound> sendSMS(final PhoneNumber number, final String text, final boolean needReport) throws SMSException {
    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit != null) {
        return mobileStationUnit.sendSMS(new com.flamesgroup.unit.PhoneNumber(number.getValue()), text, SMSMessageClass.NONE, -1, needReport);
      }
      throw new SMSException(String.format("[%s] - can't send SMS - mobileStationUnit isn't turned on ", this));
    } catch (ChannelException | InterruptedException e) {
      throw new SMSException(String.format("[%s] - can't send SMS", this), e);
    } finally {
      mobileStationUnitLock.unlock();
    }
  }

  @Override
  public IHTTPSession getHttpSession() {
    mobileStationUnitLock.lock();
    try {
      if (mobileStationUnit != null) {
        return mobileStationUnit.getHTTPSession();
      }
      return null;
    } finally {
      mobileStationUnitLock.unlock();
    }
  }

  // TODO: require to review of architectural solutions
  public void lockGsmChannel(final boolean lock, final String lockReason) {
    if (lock) {
      setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.BUSINESS_PROBLEM);
    } else {
      setIndicationModeLocal(gsmEntry.getIndicationChannel(), IndicationMode.READY);
    }

    try {
      historyLogger.handleGsmChannelLock(channelUID, lock, lockReason);
      activityLogger.logGsmUnitLock(channelUID, lock, lockReason);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't handle lock [{}:{}] for gsm channel [{}]", this, lock, lockReason, channelUID, e);
    }
  }

  public void lockGsmChannelToArfcn(final int arfcn) throws LockArfcnException {
    try {
      if (mobileStationUnit != null) {
        mobileStationUnit.getCellLocker().lock(arfcn);
      }
    } catch (ChannelException | InterruptedException e) {
      throw new LockArfcnException(String.format("can't lock gsmChannel: %s on arfcn: %d", this, arfcn), e);
    }
    gsmChannel.get().setLockArfcn(arfcn);
  }

  public void unLockGsmChannelToArfcn() throws LockArfcnException {
    try {
      if (mobileStationUnit != null) {
        mobileStationUnit.getCellLocker().unlock();
      }
    } catch (ChannelException | InterruptedException e) {
      throw new LockArfcnException(String.format("can't unlock gsmChannel: %s from  arfcn: %d", this, gsmChannel.get().getLockArfcn()), e);
    }
    gsmChannel.get().setLockArfcn(null);
  }

  public void executeNetworkSurvey() {
    DeviceType deviceType = channelUID.getDeviceUID().getDeviceType();
    if (deviceType == DeviceType.GSMB || deviceType == DeviceType.GSMB2) {
      logger.warn("[{}] - can't execute NetworkSurvey at [{}]", this, deviceType);
      return;
    }
    if (isOwned()) {
      logger.warn("[{}] - can't execute NetworkSurvey, because owned [{}]", this, isOwned());
      return;
    }
    new Thread(() -> {
      mobileStationUnitLock.lock();
      try {
        own();
        executeNetworkSurvey(null, gsmEntry.getGSMChannel().toString());
      } finally {
        unOwn();
        mobileStationUnitLock.unlock();
      }
    }).start();
  }

  private void executeNetworkSurvey(final SIMUnit simUnit, final String description) {
    if (VoiceServerPropUtils.getInstance().getVoiceServerProperties().geNetworkSurveyEnabled()) {
      if (simUnit != null) {
        long lastUpdateGsmViewsTime = imsiCatcher.getLastUpdateGsmViewsTime().getAndUpdate(prevUpdateGsmViewsTime -> {
          long currentPeriod = System.currentTimeMillis() - prevUpdateGsmViewsTime;
          if (currentPeriod < VoiceServerPropUtils.getInstance().getVoiceServerProperties().getNetworkSurveyPeriod()) {
            SimpleDateFormat dt = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSXXX");
            logger.debug("[{}] - [{}] last execute network survey at [{}], next network survey after [{}]", this, channelUID.toString(), dt.format(new Date(prevUpdateGsmViewsTime)),
                dt.format(new Date(System.currentTimeMillis() + (VoiceServerPropUtils.getInstance().getVoiceServerProperties().getNetworkSurveyPeriod() - currentPeriod))));
            return prevUpdateGsmViewsTime;
          }
          return System.currentTimeMillis();
        });
        long currentPeriod = System.currentTimeMillis() - lastUpdateGsmViewsTime;
        if (currentPeriod < VoiceServerPropUtils.getInstance().getVoiceServerProperties().getNetworkSurveyPeriod()) {
          return;
        }
        simUnit.handleEvent(SimEvent.networkSurvey("Network Survey started"));
        simUnit.changeState(CallChannelState.State.NETWORK_SURVEY);
      } else {
        imsiCatcher.getLastUpdateGsmViewsTime().set(System.currentTimeMillis());
      }
      logger.debug("[{}] - start network survey", this);
      activityLogger.logGsmUnitChangedState(channelUID, CallChannelState.State.NETWORK_SURVEY, "network survey", -1);
      GSMTelitEasyScanUnit gsmTelitEasyScanUnit = new GSMTelitEasyScanUnit(gsmEntry.getGSMChannel(), description);
      try {
        gsmTelitEasyScanUnit.turnOn();
        try {
          List<NetworkSurveyInfo> allNetworkSurvey = gsmTelitEasyScanUnit.executeNetworkSurvey();
          Date handleTime = new Date();
          try {
            gsmChannelManagerHandler.handleChannelNetworkSurveys(AntraxProperties.SERVER_NAME, channelUID, allNetworkSurvey, handleTime);
          } catch (RemoteException e) {
            logger.warn("[{}] - can't handle cell network survey for gsm channel [{}]", this, channelUID, e);
          }

          List<NetworkSurveyInfo> validNetworkSurveyInfo = allNetworkSurvey.stream().filter(nsi -> nsi.getBsic() != null
              && nsi.getLac() != null
              && nsi.getCellId() != null
              && (nsi.getArfcn() != 0
              || nsi.getBsic() != 0
              || nsi.getCellId() != 0
              || nsi.getLac() != 0))
              .collect(Collectors.toList());

          imsiCatcher.updateGsmViews(validNetworkSurveyInfo, handleTime);

          logger.debug("[{}] - end network survey", this);
          if (simUnit != null) {
            simUnit.handleEvent(SimEvent.networkSurvey("Network Survey ended"));
          }
        } catch (ChannelException | InterruptedException e) {
          logger.warn("[{}] - can't get all network survey", this, e);
          if (simUnit != null) {
            simUnit.handleEvent(SimEvent.networkSurvey("Network Survey error"));
          }
        } finally {
          gsmTelitEasyScanUnit.turnOff();
        }
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't turnOn/turnOff GSMTelitEasyScanUnit", this, e);
      } finally {
        activityLogger.logGsmUnitChangedState(channelUID, null, "", -1);
      }
    }
  }

  public void handleCellInfos(final CellInfo[] cellInfos) {
    if (cellEqualizerExecutor != null) {
      cellEqualizerExecutor.setLastCellInfos(cellInfos);
    }

    imsiCatcher.handleCellInfos(cellInfos, simUnit == null ? null : simUnit.getIMSI(), channelUID);

    Pair<Boolean, String> isImsiCatcher = imsiCatcher.checkImsiCatcher(cellInfos);

    if (isImsiCatcher.first()) {
      logger.info("[{}] - {}", this, isImsiCatcher.second());
      getGsmChannel().setLock(true); // to prevent re-lock until waiting reconfiguration
      lockGsmChannel(true, isImsiCatcher.second());
    }
  }

  public synchronized boolean isOwned() {
    return owned;
  }

  public synchronized void own() throws IllegalStateException {
    if (isOwned()) {
      throw new IllegalStateException("GSMUnit already owned");
    }
    owned = true;
  }

  public synchronized void unOwn() {
    owned = false;
  }

  public void setGsmChannel(final GSMChannel gsmChannel) {
    this.gsmChannel.set(gsmChannel);
  }

  public GSMChannel getGsmChannel() {
    return gsmChannel.get();
  }

  public void setHistoryLogger(final IRemoteHistoryLogger historyLogger) {
    this.historyLogger = historyLogger;
  }

  public void setActivityLogger(final IActivityLogger activityLogger) {
    this.activityLogger = activityLogger;
  }

  public void setImsiCatcher(final ImsiCatcher imsiCatcher) {
    this.imsiCatcher = imsiCatcher;
  }

  private static List<byte[]> getFakeAnswers(final Class<?> clazz, final String resourcePath) {
    URL url = clazz.getResource(resourcePath);
    if (url == null) {
      logger.warn("[{}] - can't find resource {} in classpath", GSMUnit.class, resourcePath);
      return Collections.emptyList();
    }

    if (url.getProtocol().equals("jar")) {
      CodeSource codeSource = clazz.getProtectionDomain().getCodeSource();
      if (codeSource != null) {
        url = codeSource.getLocation();
      }
    }

    try {
      Path path = Paths.get(url.toURI());
      if (!Files.isDirectory(path)) {
        FileSystem fs = FileSystems.newFileSystem(path, clazz.getClassLoader());
        path = fs.getPath(resourcePath);
      }

      List<byte[]> fakeAnswers = new ArrayList<>();
      DirectoryStream<Path> stream = Files.newDirectoryStream(path);
      for (Path file : stream) {
        fakeAnswers.add(Files.readAllBytes(file));
      }
      return fakeAnswers;
    } catch (Exception e) {
      logger.warn("[{}] - can't load resources from path {}", GSMUnit.class, url, e);
      return Collections.emptyList();
    }
  }

  private void handleHardwareError(final GsmHardwareError e) {
    logger.debug("[{}] - hardware error found. It is {}", this, ++hardwareErrorCount, e);
    if (hardwareErrorCount >= 6) {
      lockGsmChannel(true, "hardware problems on channel");
    }
  }

  private void handleInitError(final GsmChannelError e) {
    logger.debug("[{}] - init error found. It is {}", this, ++initErrorCount, e);
    if (initErrorCount >= 3) {
      lockGsmChannel(true, "init problems on channel");
    }
  }

  private void setIndicationModeLocal(final IIndicationChannel indicationChannel, final IndicationMode indicationMode) {
    try {
      indicationChannel.setIndicationMode(indicationMode);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't set indication mode [{}] for {}", this, indicationMode, indicationChannel, e);
    }
  }

  private static class DummySCReaderChannel implements ISCReaderChannel {

    private final ISCReaderSubChannel sCReaderSubChannel;

    public DummySCReaderChannel() {
      this(new DummySCReaderSubChannel());
    }

    public DummySCReaderChannel(final ISCReaderSubChannel scReaderSubChannel) {
      Objects.requireNonNull("scReaderSubChannel mustn't be null");
      this.sCReaderSubChannel = scReaderSubChannel;
    }

    @Override
    public ISCReaderSubChannel getSCReaderSubChannel() {
      return sCReaderSubChannel;
    }

    @Override
    public boolean isEnslaved() {
      return false;
    }

    @Override
    public void enslave() throws ChannelException, InterruptedException {
    }

    @Override
    public void free() throws ChannelException {
    }

    @Override
    public IChannelData createChannelData() {
      return new DummyChannelData();
    }

    private static class DummySCReaderSubChannel implements ISCReaderSubChannel {

      @Override
      public ATR start(final ISCReaderSubChannelHandler scReaderSubChannelHandler) throws ChannelException, InterruptedException {
        return null;
      }

      @Override
      public void stop() throws ChannelException, InterruptedException {
      }

      @Override
      public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
      }

      @Override
      public SCReaderState getState() throws ChannelException, InterruptedException {
        return null;
      }

    }

    private static class DummyChannelData implements IChannelData {

      @Override
      public boolean isAttached() {
        return false;
      }

      @Override
      public void attach(final byte type, final IChannelDataHandler channelDataHandler) throws ChannelException {
      }

      @Override
      public void detach() {
      }

      @Override
      public void sendReliable(final ByteBuffer data) throws ChannelException, InterruptedException {
      }

      @Override
      public void sendUnreliable(final ByteBuffer data) throws ChannelException {
      }

    }

  }

  private class TelitMobileStationTraceFileWriterHandler implements ITelitMobileStationTraceHandler {

    private final Path tracePath;
    private ByteChannel traceFileByteChannel;

    private TelitMobileStationTraceFileWriterHandler(final Path tracePath) {
      this.tracePath = tracePath.resolve(OffsetDateTime.now() + "_" + channelUID.toString() + ".bin");
    }

    @Override
    public void handleOpen() {
      try {
        Files.createDirectories(tracePath.getParent());
        traceFileByteChannel = Files.newByteChannel(tracePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
      } catch (IOException e) {
        throw new IllegalStateException("Can't open trace file by path " + tracePath, e);
      }
    }

    @Override
    public void handleClose() {
      try {
        traceFileByteChannel.close();
      } catch (IOException e) {
        logger.warn("[{}] - can't close trace file by path {}", GSMUnit.this, tracePath, e);
      } finally {
        traceFileByteChannel = null;
      }
    }

    @Override
    public void handleTraceData(final ByteBuffer traceData) {
      while (traceData.hasRemaining()) {
        try {
          traceFileByteChannel.write(traceData);
        } catch (IOException e) {
          throw new IllegalStateException("Can't write trace data to trace file by path " + tracePath, e);
        }
      }
    }

  }

}

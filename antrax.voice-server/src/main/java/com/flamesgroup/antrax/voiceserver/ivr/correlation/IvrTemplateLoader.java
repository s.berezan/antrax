/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.jiax2.CallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class IvrTemplateLoader {

  private static final Logger logger = LoggerFactory.getLogger(IvrTemplateLoader.class);

  private static final Pattern IVR_TEMPLATE_PATTERN = Pattern.compile("(?<name>[a-zA-Z0-9_]+)#(?<dropReason>[01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).sln");

  private static final int FRAME_SIZE = 160; // bytes in 10 ms

  private IvrTemplateLoader() {
  }

  public static Map<String, List<IvrTemplate>> loadIvrTemplates(final Path ivrTemplatePath, final int maxTemplateLengthOfTime) throws IOException {
    Map<String, List<IvrTemplate>> ivrTemplateMap = new HashMap<>();
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(ivrTemplatePath)) {
      for (Path directory : directoryStream) {
        if (!Files.isDirectory(directory)) {
          continue;
        }

        List<IvrTemplate> ivrTemplates = new ArrayList<>();
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {

          @Override
          public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
            Matcher matcher = IVR_TEMPLATE_PATTERN.matcher(file.getFileName().toString());
            if (!matcher.matches()) {
              return FileVisitResult.CONTINUE;
            }

            byte[] templateData;
            try {
              templateData = Files.readAllBytes(file);
            } catch (IOException e) {
              logger.warn("[{}] - can't read ivr template from file {}", this, file, e);
              return FileVisitResult.CONTINUE;
            }

            int templateDataTime = templateData.length / IvrTemplate.BIT_RATE_IN_BYTES;
            if (templateDataTime > maxTemplateLengthOfTime) {
              logger.info("[{}] - template file [{}] time [{}] will be trimmed to [{}] seconds", this, file, templateData.length, maxTemplateLengthOfTime);
              templateData = Arrays.copyOf(templateData, IvrTemplate.BIT_RATE_IN_BYTES * maxTemplateLengthOfTime);
            }

            double[] templateAvgData = new double[templateData.length / FRAME_SIZE];
            for (int i = 0; i < templateAvgData.length; i++) {
              templateAvgData[i] = AudioDataCorrelationHelper.avgAsAbsShort(templateData, i * FRAME_SIZE, FRAME_SIZE);
            }

            CallDropReason callDropReason = CallDropReason.fromCauseCode((byte) Integer.parseInt(matcher.group("dropReason")));
            if (callDropReason == CallDropReason.UNDEFINED) {
              logger.warn("[{}] - incorrect call drop reason in template file name [{}]", this, callDropReason);
            }

            ivrTemplates.add(new IvrTemplate(matcher.group("name"), callDropReason, new TemplateCorrelationData(templateAvgData)));
            return FileVisitResult.CONTINUE;
          }

        });

        ivrTemplateMap.put(directory.getFileName().toString(), ivrTemplates);
      }

    }

    return ivrTemplateMap;
  }

  public static void createNewDirectory(final Path ivrTemplatesPath, final String simGroup) throws IOException {
    Path subPath = ivrTemplatesPath.resolve(simGroup);
    Files.createDirectory(subPath);
  }

  public static void removeDirectories(final Path ivrTemplatesPath, final List<String> simGroups) throws IOException {
    for (String simGroup : simGroups) {
      Path subPath = ivrTemplatesPath.resolve(simGroup);
      Files.walk(subPath, Integer.MAX_VALUE)
          .map(Path::toFile)
          .sorted(Comparator.reverseOrder())
          .forEach(File::delete);
    }
  }

  public static void uploadIvrTemplate(final Path ivrTemplatesPath, final String simGroup, final byte[] bytes, final String fileName) throws IOException {
    Path subPath = ivrTemplatesPath.resolve(simGroup).resolve(fileName);
    Files.write(subPath, bytes);
  }

  public static void removeIvrTemplates(final Path ivrTemplatesPath, final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IOException {
    Path subPath = ivrTemplatesPath.resolve(simGroup);
    List<String> files = ivrTemplates.stream().map(e -> e.getName() + "#" + e.getCallDropCode() + ".sln").collect(Collectors.toList());
    Files.walk(subPath, Integer.MAX_VALUE)
        .map(Path::toFile)
        .filter(File::isFile)
        .filter(e -> files.contains(e.getName()))
        .forEach(File::delete);
  }
}

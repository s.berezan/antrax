/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.jiax2.ITerminatingCallHandler;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class TerminatingCallHandlerAdapter implements Runnable, ITerminatingCallHandler, ITerminatingCallFASHandler {

  private static final Logger logger = LoggerFactory.getLogger(TerminatingCallHandlerAdapter.class);

  private final ITerminatingCall terminatingCall;
  private final CallType callType;
  private final CallChannelPool callChannelPool;
  private final IRemoteHistoryLogger historyLogger;
  private final long callProcessDelay;

  private final int fasAttempts;

  private final AtomicReference<ITerminatingCallHandler> terminatingCallHandler = new AtomicReference<>();
  private final AtomicReference<CallDropReason> callDropReason = new AtomicReference<>();

  private int callAttempts;

  public TerminatingCallHandlerAdapter(final ITerminatingCall terminatingCall, final CallType callType, final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger) {
    this(terminatingCall, callType, callChannelPool, historyLogger, 0);
  }

  public TerminatingCallHandlerAdapter(final ITerminatingCall terminatingCall, final CallType callType, final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger,
      final long callProcessDelay) {
    this.terminatingCall = terminatingCall;
    this.callType = callType;
    this.callChannelPool = callChannelPool;
    this.historyLogger = historyLogger;
    this.callProcessDelay = callProcessDelay;

    fasAttempts = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreFasAttempts();
  }

  @Override
  public void run() {
    if (callProcessDelay > 0) {
      try {
        Thread.sleep(callProcessDelay);
      } catch (InterruptedException e) {
        logger.warn("[{}] - while sleep to process terminating call was interrupted", this, e);
        terminatingCall.hangup(CallDropReason.SWITCH_CONGESTION);
        return;
      }

      CallDropReason callDropReasonLocal = callDropReason.get();
      if (callDropReasonLocal != null) {
        logger.debug("[{}] - terminating call [{}] was already dropped with reason [{}]", this, terminatingCall, callDropReasonLocal);
        return;
      }
    }

    PhoneNumber calledPhoneNumber;
    try {
      calledPhoneNumber = new PhoneNumber(terminatingCall.getCalled());
    } catch (IllegalArgumentException ignored) {
      logger.info("[{}] - invalid called number [{}]", this, terminatingCall.getCalled());
      terminatingCall.hangup(CallDropReason.INVALID_NUMBER);
      return;
    }

    PhoneNumber callerPhoneNumber;
    try {
      callerPhoneNumber = new PhoneNumber(terminatingCall.getCaller());
    } catch (IllegalArgumentException ignored) {
      callerPhoneNumber = new PhoneNumber();
    }

    CDR cdr = CDR.createCall(callerPhoneNumber, calledPhoneNumber, callType).setVoiceServerName(AntraxProperties.SERVER_NAME);
    processTerminatingCall(terminatingCall, cdr);
  }

  @Override
  public void handleHangup(final CallDropReason callDropReasonLocal) {
    callDropReason.set(callDropReasonLocal);
    ITerminatingCallHandler terminatingCallHandlerLocal = terminatingCallHandler.get();
    if (terminatingCallHandlerLocal != null) {
      terminatingCallHandlerLocal.handleHangup(callDropReasonLocal);
    }
  }

  @Override
  public void handleDtmf(final char dtmf) {
    ITerminatingCallHandler terminatingCallHandlerLocal = terminatingCallHandler.get();
    if (terminatingCallHandlerLocal != null) {
      terminatingCallHandlerLocal.handleDtmf(dtmf);
    }
  }

  @Override
  public void handleFAS(final ITerminatingCall terminatingCall, final CDR cdr) {
    terminatingCallHandler.set(null);
    CallDropReason callDropReasonLocal = callDropReason.get();
    if (callDropReasonLocal != null) {
      logger.debug("[{}] - terminating call [{}] was already dropped with reason [{}]", this, terminatingCall, callDropReasonLocal);
      return;
    }

    if (++callAttempts >= fasAttempts) {
      logger.info("[{}] - exceeded call attempts [{}] from [{}]", this, callAttempts, fasAttempts);
      closeCall(cdr, CdrDropReason.LIMIT_FAS_ATTEMPTS, CallDropReason.SWITCH_CONGESTION);
      return;
    }

    processTerminatingCall(terminatingCall, cdr);
  }

  private void processTerminatingCall(final ITerminatingCall terminatingCall, final CDR cdr) {
    List<ICCID> iccids = new ArrayList<>();
    String calledContext = terminatingCall.getCalledContext();
    if (calledContext != null && !calledContext.isEmpty()) {
      iccids = Arrays.stream(calledContext.split(";")).map(ICCID::new).collect(Collectors.toList());
    }
    CallChannel callChannel = callChannelPool.takeForCall(cdr.getCallerPhoneNumber(), cdr.getCalledPhoneNumber(), iccids);
    if (callChannel == null || callChannel.isReleased()) {
      closeCall(cdr.addCallPath(new CallPath()), CdrDropReason.NO_CHANNELS, CallDropReason.CHANNEL_CONGESTION);
      return;
    }

    ITerminatingCallHandler terminatingCallHandlerLocal = callChannel.processTerminatingCall(terminatingCall, cdr, this);
    callChannel.untake();
    if (terminatingCallHandlerLocal == null) {
      logger.error("[{}] - can't process terminating call [{}], because another call is active", this, terminatingCall);
      closeCall(cdr, CdrDropReason.TERMINATING_ERROR, CallDropReason.SWITCH_CONGESTION);
      return;
    }

    CallDropReason callDropReasonLocal = callDropReason.get();
    if (callDropReasonLocal == null) {
      terminatingCallHandler.set(terminatingCallHandlerLocal);
    } else { // already drop in VoIP
      terminatingCallHandlerLocal.handleHangup(callDropReasonLocal);
    }
  }

  private void closeCall(final CDR cdr, final CdrDropReason cdrDropReason, final CallDropReason callDropReason) {
    terminatingCall.hangup(callDropReason);

    cdr.closeCall(cdrDropReason, callDropReason.getCauseCode());
    try {
      historyLogger.handleSimEvent(SimEvent.callEnded(cdr));
    } catch (RemoteException ex) {
      logger.warn("[{}] - can't handle about call ended for {}", this, terminatingCall, ex);
    }
  }

}

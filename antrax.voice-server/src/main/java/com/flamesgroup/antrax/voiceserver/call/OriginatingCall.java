/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.SpyMediaStream;
import com.flamesgroup.antrax.voiceserver.VoIPPeer;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IOriginatingCall;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.MobileCallDropReason;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

public class OriginatingCall extends IncomingCall {

  private static final Logger logger = LoggerFactory.getLogger(OriginatingCall.class);

  private final VoIPPeer voipPeer;

  private IOriginatingCall originationCall;

  public OriginatingCall(final CallChannel callChannel, final CDR cdr, final IMobileTerminatingCall terminatedCall, final VoIPPeer voipPeer) {
    super(callChannel, cdr, terminatedCall);

    this.voipPeer = voipPeer;
    state = CallChannelState.State.INCOMING_CALL;
  }

  @Override
  protected IMediaStream wrapMediaStream(final IMediaStream mediaStream) {
    IMediaStream mediaStreamLocal = super.wrapMediaStream(mediaStream);
    if (callChannel.isAudioCaptureEnabled()) {
      Path path = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
      mediaStreamLocal = new SpyMediaStream(mediaStreamLocal, path, cdr.getCallType(), cdr.getCallerPhoneNumber(), callChannel.toString());
    }
    return mediaStreamLocal;
  }

  @Override
  public void run() {
    PhoneNumber called;
    try {
      called = callChannel.getSIMUnit().getOriginationCalledNumber();
    } catch (IllegalArgumentException e) {
      logger.info("[{}] - can't get called number for incoming call", callChannel, e);
      dropGSM();
      resetCall(CallDropReason.SWITCH_CONGESTION, CdrDropReason.INVALID_CALLED_FILTER);
      return;
    }

    PhoneNumber caller;
    try {
      caller = callChannel.getSIMUnit().substituteOriginationCallerNumber(cdr.getCallerPhoneNumber());
    } catch (IllegalArgumentException e) {
      logger.info("[{}] - can't substitute caller number for incoming call", callChannel, e);
      dropGSM();
      resetCall(CallDropReason.SWITCH_CONGESTION, CdrDropReason.INVALID_CALLER_FILTER);
      return;
    }

    String target = callChannel.getSIMUnit().getTargetAddress();

    IOriginatingCall originationCallLocal = voipPeer.newCall(target, called.getValue(), caller.getValue(), null, this);
    if (originationCallLocal == null) {
      logger.info("[{}] - can't start new call for incoming call", callChannel);
      dropGSM();
      resetCall(CallDropReason.SWITCH_CONGESTION, CdrDropReason.ORIGINATING_ERROR);
      return;
    }

    originationCall = originationCallLocal;
    try {
      callChannel.getGSMUnit().turnOnAudio(wrapMediaStream(originationCallLocal.getAudioMediaStream()));
    } catch (GsmChannelError e) {
      logger.warn("[{}] - can't start incoming call", callChannel, e);
      dropGSM(CallDropReason.SWITCH_CONGESTION, CdrDropReason.START_AUDIO_PROBLEM);
      return;
    }

    callChannel.handleCallSetup(caller);
    callChannel.getSIMUnit().startShowingState(state, "");
    callChannel.getSIMUnit().handleIncomingCall(caller);

    super.run();
  }

  // IMobileTerminatingCallHandler
  @Override
  public void handleDTMF(final char dtmf) {
    logger.debug("[{}] - got DTMF from GSM: [{}]", callChannel, dtmf);
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        states.offer(State.DTMF);
        stateAdapter = () -> originationCall.sendDtmf(dtmf);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  protected void dropVoIP(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    originationCall.hangup(CallDropReason.NORMAL_CLEARING); // TODO: get correct drop reason
    super.dropVoIP(reason, report);
  }

  @Override
  protected void dropAll(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    originationCall.hangup(callDropReason);
    super.dropAll(callDropReason, cdrDropReason);
  }

}

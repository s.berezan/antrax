/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.voiceserver.call.ICallStatusListener;
import com.flamesgroup.antrax.voiceserver.channels.CyclicBuffer;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;
import com.flamesgroup.jiax2.helper.AdaptiveDelay;

public class FakeAnswerMediaStream implements IMediaStream, ICallStatusListener {

  private final IMediaStream mediaStream;
  private final CyclicBuffer answerBuffer;

  private final AdaptiveDelay adaptiveDelay = new AdaptiveDelay();

  private volatile IMediaStreamHandler mediaStreamHandler;

  public FakeAnswerMediaStream(final IMediaStream mediaStream, final CyclicBuffer answerBuffer) {
    this.mediaStream = mediaStream;
    this.answerBuffer = answerBuffer;
  }

  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    this.mediaStreamHandler = mediaStreamHandler;
    mediaStream.open(mediaStreamHandler);
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    mediaStream.write(timestamp, data, offset, length);
  }

  @Override
  public void close() {
    mediaStreamHandler = null;
    mediaStream.close();
  }

  @Override
  public boolean isOpen() {
    return mediaStream.isOpen();
  }

  @Override
  public void handleAlerting() {
  }

  @Override
  public void handleAnswer() {
    new Thread(new RingingControlSignalRunnable(), "FakeAnswerMediaStreamThread").start();
  }

  @Override
  public void handleHangup() {
  }

  private class RingingControlSignalRunnable implements Runnable {

    private static final int AUDIO_CHUNK_SIZE = 160;
    private static final int TIME_STEP_MS = 10;

    private final byte[] data = new byte[AUDIO_CHUNK_SIZE];

    @Override
    public void run() {
      adaptiveDelay.restart();
      int timestamp = 0;
      IMediaStreamHandler mediaStreamHandlerLocal;
      while ((mediaStreamHandlerLocal = mediaStreamHandler) != null) {
        answerBuffer.copySlice(data, 0, data.length);
        mediaStreamHandlerLocal.handleMedia(timestamp, data, 0, data.length);
        timestamp += TIME_STEP_MS;
        try {
          adaptiveDelay.delay(TIME_STEP_MS);
        } catch (InterruptedException ignored) {
          break;
        }
      }
    }
  }

}

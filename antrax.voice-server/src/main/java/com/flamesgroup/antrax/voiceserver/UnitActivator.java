/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecution;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplateManager;
import com.flamesgroup.antrax.voiceserver.sim.SCReaderSubChannelAdapter;
import com.flamesgroup.antrax.voiceserver.sim.SCReaderSubChannelsHandler;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.antrax.voiceserver.sim.SessionSimUnitManager;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.IMobileStationUnitExternalErrorHandler;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class UnitActivator {

  private static final Logger logger = LoggerFactory.getLogger(UnitActivator.class);

  private final List<GSMUnit> gsmUnits = new CopyOnWriteArrayList<>();
  private final List<SIMUnit> simUnits = new CopyOnWriteArrayList<>();

  private ImsiCatcher imsiCatcher;
  private CellEqualizerManager cellEqualizerManager;
  private IvrTemplateManager ivrTemplateManager;

  private VoIPPeer voipPeer;

  private ISimUnitManager simUnitManager;
  private IGsmUnitManger gsmUnitManger;
  private IConfigurator configurator;
  private CallChannelPool pool;
  private VSStatus antraxStatus;
  private IActivityLogger activityLogger;
  private ScriptStateSaver scriptStateSaver;
  private Configuration cfg;
  private IActionExecution actionExecution;
  private SCReaderSubChannelsHandler scReaderSubChannelsHandler;
  private RawATSubChannels rawATSubChannels;
  private SCEmulatorSubChannels scEmulatorSubChannels;

  private IRemoteHistoryLogger historyLogger;
  private ISmsManager smsManager;
  private IGsmViewManager gsmViewManager;

  private RegistryAccess registry;

  private UnitActivatorThread unitActivatorThread;
  private ISimCallHistory simCallHistory;

  public UnitActivator setVoipPeer(final VoIPPeer voipPeer) {
    Objects.requireNonNull(voipPeer, "voipPeer mustn't be null");
    this.voipPeer = voipPeer;
    return this;
  }

  public UnitActivator setSimUnitManager(final ISimUnitManager simUnitManager) {
    Objects.requireNonNull(simUnitManager, "simUnitManager mustn't be null");
    this.simUnitManager = simUnitManager;
    return this;
  }

  public UnitActivator setGsmUnitManger(final IGsmUnitManger gsmUnitManger) {
    Objects.requireNonNull(gsmUnitManger, "gsmUnitManger mustn't be null");
    this.gsmUnitManger = gsmUnitManger;
    return this;
  }

  public UnitActivator setConfigurator(final IConfigurator configurator) {
    Objects.requireNonNull(configurator, "configurator mustn't be null");
    this.configurator = configurator;
    return this;
  }

  public UnitActivator setCallChannelPool(final CallChannelPool pool) {
    Objects.requireNonNull(pool, "pool mustn't be null");
    this.pool = pool;
    return this;
  }

  public UnitActivator setAntraxStatus(final VSStatus antraxStatus) {
    Objects.requireNonNull(antraxStatus, "antraxStatus mustn't be null");
    this.antraxStatus = antraxStatus;
    return this;
  }

  public UnitActivator setActivityLogger(final IActivityLogger activityLogger) {
    Objects.requireNonNull(activityLogger, "activityLogger mustn't be null");
    this.activityLogger = activityLogger;
    return this;
  }

  public UnitActivator setScriptStateSaver(final ScriptStateSaver scriptStateSaver) {
    Objects.requireNonNull(scriptStateSaver, "scriptStateSaver mustn't be null");
    this.scriptStateSaver = scriptStateSaver;
    return this;
  }

  public UnitActivator setConfiguration(final Configuration cfg) {
    Objects.requireNonNull(cfg, "cfg mustn't be null");
    this.cfg = cfg;
    return this;
  }

  public UnitActivator setActionExecution(final IActionExecution actionExecution) {
    Objects.requireNonNull(actionExecution, "actionExecution mustn't be null");
    this.actionExecution = actionExecution;
    return this;
  }

  public UnitActivator setSCReaderSubChannelsHandler(final SCReaderSubChannelsHandler scReaderSubChannelsHandler) {
    Objects.requireNonNull(scReaderSubChannelsHandler, "scReaderSubChannelsHandler mustn't be null");
    this.scReaderSubChannelsHandler = scReaderSubChannelsHandler;
    return this;
  }

  public UnitActivator setRawATSubChannels(final RawATSubChannels rawATSubChannels) {
    Objects.requireNonNull(rawATSubChannels, "rawATSubChannels mustn't be null");
    this.rawATSubChannels = rawATSubChannels;
    return this;
  }

  public UnitActivator setSCEmulatorSubChannels(final SCEmulatorSubChannels scEmulatorSubChannels) {
    Objects.requireNonNull(scEmulatorSubChannels, "scEmulatorSubChannels mustn't be null");
    this.scEmulatorSubChannels = scEmulatorSubChannels;
    return this;
  }

  public UnitActivator setHistoryLogger(final IRemoteHistoryLogger historyLogger) {
    Objects.requireNonNull(historyLogger, "historyLogger mustn't be null");
    this.historyLogger = historyLogger;
    return this;
  }

  public UnitActivator setSmsManager(final ISmsManager smsManager) {
    Objects.requireNonNull(smsManager, "smsManager mustn't be null");
    this.smsManager = smsManager;
    return this;
  }

  public UnitActivator setGsmViewManager(final IGsmViewManager gsmViewManager) {
    Objects.requireNonNull(gsmViewManager, "gsmViewManager mustn't be null");
    this.gsmViewManager = gsmViewManager;
    return this;
  }

  public UnitActivator setRegistryAccess(final RegistryAccess registry) {
    Objects.requireNonNull(registry, "registry mustn't be null");
    this.registry = registry;
    return this;
  }

  public UnitActivator setSimCallHistory(final ISimCallHistory simCallHistory) {
    Objects.requireNonNull(simCallHistory, "simCallHistory mustn't be null");
    this.simCallHistory = simCallHistory;
    return this;
  }

  public UnitActivator setIvrTemplateManager(final IvrTemplateManager ivrTemplateManager) {
    Objects.requireNonNull(ivrTemplateManager, "ivrTemplateManager mustn't be null");
    this.ivrTemplateManager = ivrTemplateManager;
    return this;
  }

  public void addGSMUnit(final GSMUnit gsmUnit) {
    gsmUnits.add(gsmUnit);
    gsmUnit.setHistoryLogger(historyLogger);
    gsmUnit.setActivityLogger(activityLogger);
    gsmUnit.setImsiCatcher(imsiCatcher);
    rawATSubChannels.addGsmEntry(gsmUnit.getChannelUID(), gsmUnit.getGsmEntry());
    scEmulatorSubChannels.addGsmEntry(gsmUnit.getChannelUID(), gsmUnit.getGsmEntry());
  }

  public void removeGSMUnit(final GSMUnit gsmUnit) {
    gsmUnits.remove(gsmUnit);
    rawATSubChannels.removeGsmEntry(gsmUnit.getChannelUID());
    scEmulatorSubChannels.removeGsmEntry(gsmUnit.getChannelUID());
  }

  public List<GSMUnit> getGSMUnits() {
    return new ArrayList<>(gsmUnits);
  }

  public List<SIMUnit> getSIMUnits() {
    return new ArrayList<>(simUnits);
  }

  public void releaseSIMUnit(final SIMUnit simUnit) {
    simUnit.release();
    activityLogger.logSIMReturnedToSimServer(simUnit.getSimUnitId());
    simUnits.remove(simUnit);
  }

  public void start(final IGsmChannelManagerHandler channelManagerHandler) {
    if (unitActivatorThread != null) {
      throw new IllegalStateException("UnitActivator is already started");
    }

    logger.debug("[{}] - starting UnitActivator", this);
    try {
      gsmUnitManger.startVoiceServerSession(AntraxProperties.SERVER_NAME, rawATSubChannels, scEmulatorSubChannels);
    } catch (RemoteException e) {
      logger.warn("[{}] - while start server session", this, e);
    }

    unitActivatorThread = new UnitActivatorThread();
    unitActivatorThread.start();

    imsiCatcher = new ImsiCatcher(gsmViewManager, channelManagerHandler);
    imsiCatcher.start();

    cellEqualizerManager = new CellEqualizerManager(gsmViewManager, gsmUnits);
    cellEqualizerManager.start();
  }

  public void stop() {
    if (unitActivatorThread == null) {
      throw new IllegalStateException("UnitActivator is already stopped");
    }

    logger.debug("[{}] - destroying unitActivator", this);
    releaseSIMUnits();
    try {
      gsmUnitManger.endVoiceServerSession(AntraxProperties.SERVER_NAME);
    } catch (RemoteException e) {
      logger.warn("[{}] - while end server session", this, e);
    }

    unitActivatorThread = null;
  }

  private void releaseSIMUnits() {
    for (SIMUnit unit : getSIMUnits()) {
      unit.release();
      activityLogger.logSIMReturnedToSimServer(unit.getSimUnitId());
    }
  }

  private class UnitActivatorThread extends Thread {

    private final long timeout = 5000;

    private int channelNumber = 0;

    public UnitActivatorThread() {
      super("UnitActivatorThread");
    }

    @Override
    public void run() {
      while (!antraxStatus.terminating) {
        List<GSMUnit> gsmUnits = getGSMUnits();
        for (GSMUnit gsmUnit : gsmUnits) {
          if (antraxStatus.terminating) {
            return;
          }

          if (gsmUnit.isOwned()) {
            continue; // registered unit
          }

          GSMChannel gsmChannel = gsmUnit.getGsmChannel();
          if (gsmUnit.isPlugged() && !gsmChannel.isLock() && !(gsmChannel.getGSMGroup() instanceof GsmNoGroup)) {
            logger.trace("[{}] - going to activate {}", this, gsmUnit);
            if (!tryActivateWithSessionSim(gsmUnit)) {
              tryActivateWithNewSim(gsmUnit);
            }
          }
        }

        try {
          Thread.sleep(timeout);
        } catch (InterruptedException e) {
          logger.debug("[{}] - interrupted while sleeping. Breaking loop", this, e);
          break;
        }
      }
    }

    private boolean canBeActivated(final GSMUnit gsmUnit, final SIMUnit simUnit) {
      return simUnit.shouldStartActivity()
          && cfg.applies(gsmUnit.getGsmChannel().getGSMGroup(), simUnit.getSimData().getSimGroup())
          && (simUnit.getImei() != null || simUnit.shouldGenerateIMEI());
    }

    private boolean tryActivateWithSessionSim(final GSMUnit gsmUnit) {
      logger.trace("[{}] - iterating over session sim units", this);
      for (SIMUnit simUnit : getSIMUnits()) {
        // TODO: check to refactor lockGsmChannel and waitRelease state of SIMUnit
        if (simUnit.isOwned() || !simUnit.isPlugged() || simUnit.isLocked() || simUnit.isWaitRelease() || !canBeActivated(gsmUnit, simUnit)) {
          continue;
        }

        logger.trace("[{}] - {} is ok to activate", this, simUnit);
        activate(gsmUnit, simUnit);
        return true;
      }
      return false;
    }

    private void activate(final GSMUnit gsmUnit, final SIMUnit simUnit) {
      ISCReaderSubChannels scReaderSubChannels = simUnit.activate();
      if (scReaderSubChannels == null) {
        return;
      }

      logger.info("[{}] - Activating {} with {} and {}", this, gsmUnit, simUnit, simUnit.getUid());
      gsmUnit.own();
      simUnit.setOwned(true);
      activityLogger.logCallChannelCreated(gsmUnit.getChannelUID(), simUnit.getSimUnitId());
      CallChannel callChannel = new CallChannel(gsmUnit, simUnit, ++channelNumber, activityLogger, cfg, actionExecution, ivrTemplateManager, historyLogger);
      CallChannelActivityThread callChannelActivityThread = new CallChannelActivityThread(callChannel, antraxStatus, activityLogger, voipPeer,
          new SCReaderSubChannelAdapter(simUnit.getChannelUID(), scReaderSubChannels, scReaderSubChannelsHandler),
          new DefaultMobileStationUnitExternalErrorHandler(callChannel), pool, historyLogger,
          cellEqualizerManager);
      callChannelActivityThread.start();
    }

    private boolean tryActivateWithNewSim(final GSMUnit gsmUnit) {
      logger.trace("[{}] - going to ask sim server for new simCard", this);
      Pair<ChannelUID, UUID> sessionPair;
      try {
        sessionPair = simUnitManager.takeSimUnit(AntraxProperties.SERVER_NAME, gsmUnit.getGsmChannel().getGSMGroup());
      } catch (RemoteException e) {
        logger.warn("[{}] - while taking session pair", this, e);
        return false;
      }

      if (sessionPair == null) {
        logger.trace("[{}] - simUnitManager returned null", this);
        return false;
      }

      SIMUnit simUnit = new SIMUnit(new SessionSimUnitManager(simUnitManager, sessionPair.first()), historyLogger, smsManager, sessionPair.first(), sessionPair.second(), scriptStateSaver,
          activityLogger, registry, simCallHistory);
      logger.trace("[{}] - simServer returned {}", this, sessionPair);
      try {
        simUnit.beginRefreshData();
      } catch (Exception ignored) {
        logger.warn("[{}] - failed to get initial sim card data for {}. Returning it back", this, simUnit);
        simUnit.release();
        return false;
      }

      AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
      if (!configurator.configure(simUnit, pluginsStore)) {
        logger.warn("[{}] - {} failed configuration. Returning it to the server", this, simUnit);
        simUnit.release();
        return false;
      }

      boolean resetScriptStates = configurator.shouldResetVsScripts(simUnit);
      simUnit.endRefreshData(pluginsStore, resetScriptStates);
      if (resetScriptStates) {
        configurator.clearResetVsScriptsFlag(simUnit);
      }

      activityLogger.logSIMTakenFromSimServer(simUnit.getChannelUID(), simUnit.getUid());
      activityLogger.logSIMChangedEnableStatus(simUnit.getSimUnitId(), simUnit.isEnabled());
      SIMGroup group = simUnit.getSimData().getSimGroup();
      activityLogger.logSIMChangedGroup(simUnit.getSimUnitId(), group.getName());

      if (canBeActivated(gsmUnit, simUnit)) {
        logger.trace("[{}] - {} is ok to activate", this, simUnit);
        activate(gsmUnit, simUnit);
      }

      simUnits.add(simUnit);
      return true;
    }

  }

  private class DefaultMobileStationUnitExternalErrorHandler implements IMobileStationUnitExternalErrorHandler {

    private final CallChannel callChannel;

    public DefaultMobileStationUnitExternalErrorHandler(final CallChannel callChannel) {
      this.callChannel = callChannel;
    }

    @Override
    public void handleIMSICatcherDetected() {
      logger.info("[{}] - IMSI catcher detect on gsm unit [{}]", this, callChannel.getGSMUnit());
    }

    @Override
    public void handleJammedDetected() {
      logger.info("[{}] - Jammed detect on gsm unit [{}]", this, callChannel.getGSMUnit());
    }

    @Override
    public void handleNormalOperatingRestored() {
      logger.info("[{}] - Normal operating restored on gsm unit [{}]", this, callChannel.getGSMUnit());
    }

  }

}

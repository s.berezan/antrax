/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.ITerminatingCallFASHandler;
import com.flamesgroup.antrax.voiceserver.SpyMediaStream;
import com.flamesgroup.antrax.voiceserver.channels.error.DialingError;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.MobileCallDropReason;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

public abstract class OutgoingCall extends Call {

  protected static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  protected PhoneNumber dialingNumber;

  private final ITerminatingCall terminatingCall;
  private final ITerminatingCallFASHandler terminatingCallFASHandler;

  private final long fasTimeout;
  private boolean fasDetected = true;
  private long answerTime;

  public OutgoingCall(final CallChannel callChannel, final CDR cdr, final ITerminatingCall terminatingCall, final ITerminatingCallFASHandler terminatingCallFASHandler) {
    super(callChannel, cdr);

    this.terminatingCall = terminatingCall;
    this.terminatingCallFASHandler = terminatingCallFASHandler;

    this.fasTimeout = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreFasActiveTimeout();
  }

  protected abstract PhoneNumber substitutePhoneNumber(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber);

  @Override
  protected IMediaStream wrapMediaStream(final IMediaStream mediaStream) {
    IMediaStream mediaStreamLocal = super.wrapMediaStream(mediaStream);
    if (callChannel.isAudioCaptureEnabled()) {
      Path path = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
      mediaStreamLocal = new SpyMediaStream(mediaStreamLocal, path, cdr.getCallType(), cdr.getCalledPhoneNumber(), callChannel.toString());
    }
    return mediaStreamLocal;
  }

  // Runnable
  @Override
  public void run() {
    callChannel.own();
    cdr.addCallPath(callChannel.createCallPath());

    dialingNumber = substitutePhoneNumber(cdr.getCallerPhoneNumber(), cdr.getCalledPhoneNumber());
    if (dialingNumber == null) {
      logger.warn("[{}] - can't substitute called number [{}] for terminating call", this, terminatingCall.getCalled());
      dropVoIP(CallDropReason.SWITCH_CONGESTION, CdrDropReason.INVALID_CALLED_FILTER);
      return;
    }

    CdrDropReason cdrDropReason = null;
    try {
      callChannel.handleCallSetup(dialingNumber);
      logger.debug("[{}] - dial to [{}]", callChannel, dialingNumber);

      long dialTime = System.currentTimeMillis();
      try {
        callChannel.getGSMUnit().dial(dialingNumber, this);
      } catch (GsmChannelError e) {
        if (e instanceof DialingError) {
          callChannel.getSIMUnit().handleDialError(((DialingError) e).getErrorStatus());
        }
        cdrDropReason = CdrDropReason.TERMINATING_ERROR;
        throw e;
      }

      cdr.setDialTime(dialTime);
      callChannel.getSIMUnit().handleCallSetup(dialingNumber);

      callChannel.getGSMUnit().turnOnAudio(wrapMediaStream(terminatingCall.getAudioMediaStream()));
    } catch (GsmChannelError e) {
      callChannel.handleCallRelease();
      if (cdrDropReason == null) {
        cdrDropReason = CdrDropReason.START_AUDIO_PROBLEM;
      }

      logger.warn("[{}] - can't process terminating call [{}]", this, terminatingCall, e);
      dropVoIP(CallDropReason.SWITCH_CONGESTION, cdrDropReason);
      return;
    }

    callChannel.handleCallStarted(dialingNumber);

    super.run();
  }

  // IMobileTerminatingCallHandler
  @Override
  public void handleDTMF(final char dtmf) {
    logger.debug("[{}] - got DTMF from GSM: [{}]", callChannel, dtmf);
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        states.offer(State.DTMF);
        stateAdapter = () -> terminatingCall.sendDtmf(dtmf);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // ITerminatingCallHandler
  @Override
  public void handleDtmf(final char dtmf) {
    logger.debug("[{}] - got DTMF from VoIP: [{}]", callChannel, dtmf);
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        states.offer(State.DTMF);
        stateAdapter = () -> {
          try {
            sendDTMF(String.valueOf(dtmf));
          } catch (GsmChannelError e) {
            logger.warn("[{}] - can't process DTMF {}", callChannel, dtmf, e);
          }
        };
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // Call
  @Override
  protected void processAlerting() {
    try {
      if (fasDetected = waitingOneOfCallStates(fasTimeout, TimeUnit.MILLISECONDS, EnumSet.of(State.ANSWER, State.HANGUP))) {
        logger.debug("[{}] - receive ANSWER or HANGUP while waiting FAS timeout", callChannel);
        return;
      }
    } catch (InterruptedException e) {
      logger.warn("[{}] - while wait FAS timeout was interrupted", callChannel, e);
      return;
    }

    callChannel.changeCallState(CallState.State.ALERTING);

    logger.debug("[{}] - activating ringing", callChannel);
    callChannel.getGSMUnit().activateRinging();

    logger.debug("[{}] - telling about ALERTING to the VoIP", callChannel);
    cdr.startAlert();

    callChannel.handelPdd(cdr.getPDD());

    terminatingCall.ringing();
  }

  @Override
  protected void processAnswer() {
    if (fasDetected && callChannel.isFASDetection()) {
      logger.debug("[{}] - FAS detected", callChannel);
      callChannel.getSIMUnit().handleFAS();

      dropGSM(CallDropReason.UNDEFINED.getCauseCode());
      cdr.closeCall(CdrDropReason.FAS, CallDropReason.UNDEFINED.getCauseCode());
      terminatingCallFASHandler.handleFAS(terminatingCall, cdr);
      resetCall();
      return;
    }

    if (fasDetected && !callChannel.isFASDetection()) {
      logger.debug("[{}] - activating ringing", callChannel);
      callChannel.getGSMUnit().activateRinging();
    }

    callChannel.changeCallState(CallState.State.ACTIVE);
    cdr.startCall();

    logger.debug("[{}] - telling about ANSWER to the VoIP", callChannel);
    answerTime = System.currentTimeMillis();
    callChannel.getSIMUnit().handleCallStarted(dialingNumber);
    terminatingCall.answer();
  }

  @Override
  protected void dropGSM(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    dropGSM(callDropReason.getCauseCode());
    resetCall();

    cdr.closeCall(cdrDropReason, callDropReason.getCauseCode());
    callChannel.getSIMUnit().handleEvent(SimEvent.callEnded(cdr));
  }

  @Override
  protected void dropVoIP(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    if (reason != null) {
      logger.info("[{}] - detected [{}]", callChannel, reason);
      callChannel.getSIMUnit().handleCallError(report.getCallControlConnectionManagementCause());
    }

    CallDropReason callDropReason = CallDropReason.fromCauseCode((byte) report.getCallControlConnectionManagementCause()); // TODO: remove cast to byte after change signature of interface method
    if (cdr.getStartTime() <= 0 && callDropReason == CallDropReason.NORMAL_CLEARING) {
      callDropReason = CallDropReason.NORMAL; // FIXME: fix sip 200 OK response to originator on ringing state by drop 31 code instead of 16
    }

    logger.debug("[{}] - dropping VoIP with reason [{}({})] for caller [{}] and called [{}]", callChannel, callDropReason, callDropReason.getCauseCode(),
        cdr.getCallerPhoneNumber().getValue(), cdr.getCalledPhoneNumber().getValue());

    releaseCall(callDropReason.getCauseCode());
    dropVoIP(callDropReason, CdrDropReason.GSM);
  }

  @Override
  protected void dropAll(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    logger.debug("[{}] - dropping both GSM and VoIP", callChannel);
    dropGSM(callDropReason.getCauseCode());
    dropVoIP(callDropReason, cdrDropReason);
  }

  public void sendDTMF(final String dtmfString) throws GsmChannelError {
    callChannel.getGSMUnit().sendDTMF(dtmfString);
  }

  public void sendDTMF(final char dtmf, final int duration) throws GsmChannelError {
    callChannel.getGSMUnit().sendDTMF(dtmf, duration);
  }

  private void releaseCall(final int causeCode) {
    logger.debug("[{}] - releasing call", callChannel);
    callChannel.getGSMUnit().turnOffAudio();
    callChannel.handleCallEnd(answerTime > 0 ? System.currentTimeMillis() - answerTime : 0, causeCode);
  }

  private void resetCall() {
    callReleased.set(true);
    callChannel.resetOutgoingCall();
  }

  private void dropGSM(final int causeCode) {
    logger.debug("[{}] - dropping GSM", callChannel);
    try {
      callChannel.getGSMUnit().dropCall();
      logger.debug("[{}] - GSM dropped", callChannel);
    } catch (GsmChannelError e) {
      logger.warn("[{}] - while dropping gsm", callChannel, e);
    }

    releaseCall(causeCode);
  }

  private void dropVoIP(final CallDropReason callDropReason, final CdrDropReason code) {
    logger.debug("[{}] - telling about DROP to the VoIP with reason {}", callChannel, callDropReason);
    terminatingCall.hangup(callDropReason);
    cdr.closeCall(code, callDropReason.getCauseCode());
    callChannel.getSIMUnit().handleEvent(SimEvent.callEnded(cdr));
    resetCall();
  }

  private boolean waitingOneOfCallStates(final long time, final TimeUnit timeUnit, final EnumSet<State> waitingStates) throws InterruptedException {
    lock.lock();
    try {
      long waitingTime;
      long startTime = System.currentTimeMillis();
      while ((waitingTime = System.currentTimeMillis() - startTime) <= time) {
        if (condition.await(time - waitingTime, timeUnit) && waitingStates.contains(states.peek())) {
          return true;
        }
      }
    } finally {
      lock.unlock();
    }
    return false;
  }

}

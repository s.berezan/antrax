/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.control.communication.CellInfoStatistic;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.unit.CellInfo;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class CellEqualizerStatisticAlgorithmExecutor extends CellEqualizerExecutor {

  private static final int DESTINATION_RX_LEV_DELTA = 10;

  private final ProgressiveCellEqualizerData progressiveCellEqualizerData = new ProgressiveCellEqualizerData();
  private final Set<Integer> usedArfcns = new HashSet<>();

  private ProgressiveCellEqualizer progressiveCellEqualizer;
  private int targetSuccessfulCallCount;
  private int initialSuccessfulCallCount;
  private int targetUssdCount;
  private int initialUssdCount;
  private int targetOutgoingSmsCount;
  private int initialOutgoingSmsCount;

  private boolean isInit = true;

  public CellEqualizerStatisticAlgorithmExecutor(final GSMUnit gsmUnit, final CellEqualizerManager cellEqualizerManager) {
    super(gsmUnit, cellEqualizerManager);
  }

  @Override
  public void execute(final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws GsmHardwareError {
    if (lastCellInfos == null) {
      return;
    }
    if (progressiveCellEqualizer != null && progressiveCellEqualizer.isAlive()) {
      logger.trace("[{}] - executing progressiveCellEqualizer, waiting while current equalizer will be executed", this);
      return;
    }

    CellInfo lastServingCellInfo = lastCellInfos[0];
    CellKey servingCellKey = new CellKey(lastServingCellInfo.getLac(), lastServingCellInfo.getCellId(), lastServingCellInfo.getBsic(), lastServingCellInfo.getArfcn());
    CellInfoStatistic servingCellInfoStatistic = gsmUnit.getActivityLogger().getVoiceServerChannelsInfo(gsmUnit.getChannelUID()).getCellStatistic().get(servingCellKey);
    if (servingCellInfoStatistic == null) {
      servingCellInfoStatistic = new CellInfoStatistic();
    }

    CellEqualizerStatisticAlgorithm.CellEqualizerStatisticConfiguration cellEqualizerStatisticConfiguration = ((CellEqualizerStatisticAlgorithm) cellEqualizerAlgorithm).getConfiguration();

    if (isInit) {
      isInit = false;
      initialSuccessfulCallCount = servingCellInfoStatistic.getSuccessfulCallsCount();
      initialUssdCount = servingCellInfoStatistic.getUssdCount();
      initialOutgoingSmsCount = servingCellInfoStatistic.getSuccessOutgoingSmsCount();
      targetSuccessfulCallCount = getNextRandom(cellEqualizerStatisticConfiguration.getMinSuccessfulCallCount(), cellEqualizerStatisticConfiguration.getMaxSuccessfulCallCount());
      targetUssdCount = getNextRandom(cellEqualizerStatisticConfiguration.getMinUssdCount(), cellEqualizerStatisticConfiguration.getMaxUssdCount());
      targetOutgoingSmsCount = getNextRandom(cellEqualizerStatisticConfiguration.getMinOutgoingSmsCount(), cellEqualizerStatisticConfiguration.getMaxOutgoingSmsCount());
      return;
    }

    logger.trace("[{}] - servingCellKey {}", this, servingCellKey);
    logger.trace("[{}] - servingCellInfoStatistic {}", this, servingCellInfoStatistic);
    logger.trace("[{}] - info: {}", this, getInfo());
    if (!(servingCellInfoStatistic.getSuccessfulCallsCount() - initialSuccessfulCallCount >= targetSuccessfulCallCount
        || servingCellInfoStatistic.getUssdCount() - initialUssdCount >= targetUssdCount
        || servingCellInfoStatistic.getSuccessOutgoingSmsCount() - initialOutgoingSmsCount >= targetOutgoingSmsCount)) {
      logger.trace("[{}] - waiting one of the rule will be true", this);
      return;
    }

    Map<Integer, Integer> serverCellAdjustments = cellEqualizerManager.getServerCellAdjustments();
    List<CellInfo> cellInfos = Arrays.stream(lastCellInfos)
        .filter(cellInfo -> cellInfo.getCellId() > 0 && cellInfo.getLac() > 0 && !serverCellAdjustments.containsKey(cellInfo.getArfcn()))
        .collect(Collectors.toList());

    if (cellInfos.size() <= 1) {
      return;
    }

    logger.debug("[{}] - start executeCellEqualizer on [{}]", this, cellEqualizerStatisticConfiguration);

    LocalTime nowTime = LocalTime.now();
    LocalTime fromLeadTime = cellEqualizerStatisticConfiguration.getFromLeadTime();
    LocalTime toLeadTime = cellEqualizerStatisticConfiguration.getToLeadTime();

    if (fromLeadTime == null || toLeadTime == null) {
      logger.warn("[{}] - please set fromLeadTime and toLeadTime", this);
      return;
    }

    //check lead time
    if (!(fromLeadTime.isBefore(nowTime) && toLeadTime.isAfter(nowTime))) {
      logger.info("[{}] - lead time is different from now time, info: [{}]", this, getInfo());
      return;
    }

    if (progressiveCellEqualizerData.getCurrentArfcn() < 0) {
      progressiveCellEqualizerData.setDestinationRxLev(cellInfos.get(0).getRxLev() + DESTINATION_RX_LEV_DELTA);
    }

    List<CellInfo> filteredCellInfos = cellInfos.stream().filter(c -> !usedArfcns.contains(c.getArfcn())).collect(Collectors.toList());
    logger.trace("[{}] - filteredCellInfos {}", this, filteredCellInfos);
    if (filteredCellInfos.size() == 0) {
      usedArfcns.clear();
      filteredCellInfos = cellInfos;
    }
    Random nextRandom = new Random();
    int posAtCellInfo = nextRandom.nextInt(filteredCellInfos.size());
    CellInfo nextCellInfo = filteredCellInfos.get(posAtCellInfo);
    progressiveCellEqualizerData.setCurrentArfcn(nextCellInfo.getArfcn());
    if (progressiveCellEqualizerData.getPrevArfcn() == progressiveCellEqualizerData.getCurrentArfcn()) {
      posAtCellInfo = nextRandom.nextInt(filteredCellInfos.size());
      nextCellInfo = filteredCellInfos.get(posAtCellInfo);
      progressiveCellEqualizerData.setCurrentArfcn(nextCellInfo.getArfcn());
    }
    progressiveCellEqualizerData.setCurrentRxLev(nextCellInfo.getRxLev());
    usedArfcns.add(nextCellInfo.getArfcn());

    progressiveCellEqualizerData.setMinWaitingTime(cellEqualizerStatisticConfiguration.getMinWaitingTime());
    progressiveCellEqualizerData.setMaxWaitingTime(cellEqualizerStatisticConfiguration.getMaxWaitingTime());

    logger.debug("[{}] - start progressive cellEqualizer with info: [{}]", this, getInfo());
    progressiveCellEqualizer = new ProgressiveCellEqualizer(gsmUnit, cellEqualizerManager, progressiveCellEqualizerData);
    progressiveCellEqualizer.start();
    isInit = true;
  }

  @Override
  public void terminate() {
    if (progressiveCellEqualizer != null && progressiveCellEqualizer.isAlive()) {
      progressiveCellEqualizer.terminate();
      try {
        progressiveCellEqualizer.join();
      } catch (InterruptedException ignore) {
      }
      logger.debug("[{}] - terminate thread: [{}]", this, progressiveCellEqualizer);
    }
  }

  private String getInfo() {
    return " targetSuccessfulCallCount:" + targetSuccessfulCallCount +
        " initialSuccessfulCallCount:" + initialSuccessfulCallCount +
        " targetUssdCount:" + targetUssdCount +
        " initialUssdCount:" + initialUssdCount +
        " targetOutgoingSmsCount:" + targetOutgoingSmsCount +
        " initialOutgoingSmsCount:" + initialOutgoingSmsCount +
        " progressiveCellEqualizerData:" + progressiveCellEqualizerData +
        " lastCellInfos:" + Arrays.toString(lastCellInfos);
  }

  private int getNextRandom(final int min, final int max) {
    return new Random().nextInt(max - min + 1) + min;
  }

}

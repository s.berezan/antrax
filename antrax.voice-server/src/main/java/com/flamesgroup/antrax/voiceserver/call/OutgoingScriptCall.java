/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.FakeRingingMediaStream;
import com.flamesgroup.antrax.voiceserver.ITerminatingCallFASHandler;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.unit.PhoneNumber;

public class OutgoingScriptCall extends OutgoingCall {

  public OutgoingScriptCall(final CallChannel callChannel, final CDR cdr, final ITerminatingCall terminatingCall, final ITerminatingCallFASHandler terminatingCallFASHandler) {
    super(callChannel, cdr, terminatingCall, terminatingCallFASHandler);
  }

  @Override
  protected PhoneNumber substitutePhoneNumber(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber) {
    return calledPhoneNumber;
  }

  @Override
  protected IMediaStream wrapMediaStream(final IMediaStream mediaStream) {
    IMediaStream mediaStreamLocal = super.wrapMediaStream(mediaStream);
    if (callChannel.getSIMUnit().getSimData().getSimGroup().isSyntheticRinging()) {
      FakeRingingMediaStream fakeRingingMediaStream = new FakeRingingMediaStream(mediaStreamLocal, callChannel.getGSMUnit().getRingingControlSignal());
      addCallStatusListener(fakeRingingMediaStream);
      mediaStreamLocal = fakeRingingMediaStream;
    }
    return mediaStreamLocal;
  }

}

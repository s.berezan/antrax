/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.timemachine.ConditionalStateImpl;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

// TODO: remove after architecture reconsideration
public class ReadSignalQualityState extends ConditionalStateImpl {

  private final AtomicBoolean finished = new AtomicBoolean();
  private static final Logger logger = LoggerFactory.getLogger(ReadSignalQualityState.class);

  private final CallChannel callChannel;
  private State idleBetweenChecks;

  private int rate;
  private int strength;

  public ReadSignalQualityState(final CallChannel callChannel) {
    assert callChannel != null;
    this.callChannel = callChannel;
  }

  @Override
  public boolean isFinished() {
    logger.trace("[{}] - isFinished asked. finished={}", this, finished);
    return finished.get();
  }

  @Override
  public void enterState() {
    finished.set(false);
    int bitErrorRate = callChannel.getSignalBitErrorRate();
    int receivedStrength = callChannel.getSignalReceivedStrength();
    if (rate != bitErrorRate || strength != receivedStrength) {
      rate = bitErrorRate;
      strength = receivedStrength;

      callChannel.updateSignalQuality(strength, rate);
      logger.trace("[{}] - signal quality updated [rssi: {} ber: {}]", this, strength, rate);
    }
    finished.set(true);
  }

  @Override
  public State getNextState() {
    return idleBetweenChecks;
  }

  public void setNextState(final State nextState) {
    idleBetweenChecks = nextState;
  }

}

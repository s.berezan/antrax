/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import static org.slf4j.LoggerFactory.getLogger;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.autoactions.ExecutionStatus;
import com.flamesgroup.antrax.autoactions.IActionExecution;
import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.actionscripts.ActionExecutionFailed;
import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.antrax.voiceserver.call.Call;
import com.flamesgroup.antrax.voiceserver.call.IncomingCall;
import com.flamesgroup.antrax.voiceserver.call.IncomingScriptCall;
import com.flamesgroup.antrax.voiceserver.call.OriginatingCall;
import com.flamesgroup.antrax.voiceserver.call.OutgoingCall;
import com.flamesgroup.antrax.voiceserver.call.OutgoingScriptCall;
import com.flamesgroup.antrax.voiceserver.call.ScriptTerminatingCallAdapter;
import com.flamesgroup.antrax.voiceserver.call.TerminatingCall;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.CallManagementError;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.channels.error.HTTPError;
import com.flamesgroup.antrax.voiceserver.channels.error.USSDError;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplateManager;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.jiax2.ITerminatingCallHandler;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.IMobileTerminatingCallHandler;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus;
import com.flamesgroup.unit.SupplementaryServiceNotificationCode;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import com.flamesgroup.unit.sms.SendSMSException;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// TODO: replace synchronized instructions by java.util.concurrent.*
public class CallChannel {

  private static final Runnable releaseTask = () -> {}; // poison pill to stop call process thread

  private final Logger logger;
  private IRemoteHistoryLogger historyLogger;

  private final GSMUnit gsmUnit;
  private final SIMUnit simUnit;
  private final int channelNumber;
  private final IActivityLogger activityLogger;
  private final Configuration configuration;
  private final IActionExecution actionExecution;
  private final IvrTemplateManager ivrTemplateManager;

  private final AtomicReference<Call> currentCall = new AtomicReference<>();

  private volatile Thread businessActivityThread;
  private volatile IUSSDSession ussdSession;

  private volatile boolean owned = true;
  private volatile boolean released = false;
  private volatile boolean taken = false;

  private volatile boolean sendingSms = false;

  private CellInfo servingCell;

  private int rate;
  private int strength;
  private CellInfo[] cellInfos;
  private Pair<NetworkRegistrationStatus, String> registrationStatus;

  private final CellInfoComparator cellInfoComparator = new CellInfoComparator();

  private String releaseReason;

  private CallProcessThread callProcessThread;
  private final Lock callProcessThreadLock = new ReentrantLock();
  private RmiMediatorUssdProcessThread rmiMediatorUssdProcessThread;
  private CallState.State callState = CallState.State.IDLE;

  private boolean httpSessionOpened;
  private String prevHttpServer = "";

  public CallChannel(final GSMUnit gsmUnit, final SIMUnit simUnit, final int channelNumber, final IActivityLogger activityLogger, final Configuration configuration,
      final IActionExecution actionExecution, final IvrTemplateManager ivrTemplateManager, final IRemoteHistoryLogger historyLogger) {
    logger = getLogger(CallChannel.class.getName() + "(" + channelNumber + ")");
    assert gsmUnit != null : "GSMUnit couldn't be null";
    assert simUnit != null : "SIMUnit couldn't be null";

    this.gsmUnit = gsmUnit;
    this.simUnit = simUnit;
    this.channelNumber = channelNumber;
    this.activityLogger = activityLogger;
    this.configuration = configuration;
    this.actionExecution = actionExecution;
    this.ivrTemplateManager = ivrTemplateManager;
    this.historyLogger = historyLogger;
  }

  int getChannelNumber() {
    return channelNumber;
  }

  public synchronized void own() {
    logger.debug("[{}] - owned", this);
    owned = true;
  }

  public synchronized boolean owned() { //TODO: rename to isOwned
    return owned;
  }

  public synchronized void share() {
    logger.debug("[{}] - shared", this);
    owned = false;
  }

  public synchronized void sendingSms(final boolean sendingSms) {
    this.sendingSms = sendingSms;
  }

  public synchronized boolean isSendingSms() {
    return sendingSms;
  }

  public synchronized void release(final String releaseReason) {
    if (!released) {
      logger.debug("[{}] - asked to release by reason [{}]", this, releaseReason);
      released = true;
      this.releaseReason = releaseReason;
    }
  }

  public synchronized String getReleaseReason() {
    return releaseReason;
  }

  synchronized boolean isReleased() {
    return released;
  }

  public boolean isGroupApplies() {
    return configuration.applies(gsmUnit.getGsmChannel().getGSMGroup(), simUnit.getSimData().getSimGroup());
  }

  public boolean isAudioCaptureEnabled() {
    return configuration.isAudioCaptureEnabled();
  }

  public boolean isEnabled() {
    return simUnit.isEnabled();
  }

  public boolean acceptsCallPhoneNumber(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber) {
    return simUnit.acceptsCallPhoneNumber(callerPhoneNumber, calledPhoneNumber);
  }

  public boolean acceptsSmsPhoneNumber(final PhoneNumber phoneNumber) {
    return simUnit.acceptsSmsPhoneNumber(phoneNumber);
  }

  public boolean acceptsSmsParts(final int parts) {
    return simUnit.acceptsSmsParts(parts);
  }

  public boolean acceptsSmsText(final String text) {
    return simUnit.acceptsSmsText(text);
  }

  public SIMUnit getSIMUnit() {
    return simUnit;
  }

  public GSMUnit getGSMUnit() {
    return gsmUnit;
  }

  public IvrTemplateManager getIvrTemplateManager() {
    return ivrTemplateManager;
  }

  public CallPath createCallPath() {
    CallPath callPath = new CallPath();
    callPath.setGsmChannelUID(getGSMUnit().getChannelUID());
    callPath.setSimChannelUID(getSIMUnit().getChannelUID());
    callPath.setGsmGroupName(getGSMUnit().getGsmChannel().getGSMGroup().getName());
    callPath.setSimGroupName(getSIMUnit().getSimData().getSimGroup().getName());
    callPath.setSimUID(getSIMUnit().getUid());
    return callPath;
  }

  public void startProcessCallThread() {
    callProcessThreadLock.lock();
    try {
      callProcessThread = new CallProcessThread();
      callProcessThread.start();
    } finally {
      callProcessThreadLock.unlock();
    }
  }

  public void stopProcessCallThread() {
    CallProcessThread callProcessThreadLocal;
    callProcessThreadLock.lock();
    try {
      callProcessThreadLocal = callProcessThread;
      callProcessThread = null;
    } finally {
      callProcessThreadLock.unlock();
    }

    dropCall(getCall(), CallDropReason.NORMAL_CLEARING, CdrDropReason.VS);
    callProcessThreadLocal.execute(releaseTask);
  }

  public ITerminatingCallHandler processTerminatingCall(final ITerminatingCall terminatingCall, final CDR cdr, final ITerminatingCallFASHandler terminatingCallFASHandler) {
    return processCall(new TerminatingCall(this, cdr, terminatingCall, terminatingCallFASHandler));
  }

  public IMobileTerminatingCallHandler startIncomingCall(final PhoneNumber phoneNumber, final IMobileTerminatingCall terminatedCall, final VoIPPeer voipPeer) {

    if (simUnit.isSupportOrigination()) {
      CDR cdr = CDR.createCall(phoneNumber, getSIMUnit().getSimData().getPhoneNumber(), CallType.GSM_TO_VOIP).setVoiceServerName(AntraxProperties.SERVER_NAME).addCallPath(createCallPath());
      return processCall(new OriginatingCall(this, cdr, terminatedCall, voipPeer));
    } else {
      CDR cdr = CDR.createCall(phoneNumber, getSIMUnit().getSimData().getPhoneNumber(), CallType.GSM_TO_VS).setVoiceServerName(AntraxProperties.SERVER_NAME).addCallPath(createCallPath());
      return processCall(new IncomingScriptCall(this, cdr, terminatedCall));
    }
  }

  @Override
  public String toString() {
    return gsmUnit + ":" + simUnit;
  }

  public boolean isFASDetection() {
    return simUnit.getSimData().getSimGroup().isFASDetection();
  }

  public String sendUSSD(final String ussd) throws GsmChannelError {
    logger.debug("[{}] - sendUSSD={}", this, ussd);
    ussdSession = gsmUnit.openUSSDSession(ussd);
    String response;
    try {
      response = ussdSession.readRequest();
      activityLogger.logCallChannelSendUSSD(getSIMUnit().getChannelUID(), ussd, response);
      simUnit.handleEvent(SimEvent.ussd(ussd, response));
    } catch (ChannelException | InterruptedException e) {
      throw new USSDError(String.format("[%s] - can't send USSD [%s]", this, ussd), e);
    } finally {
      try {
        ussdSession.close();
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't close USSD [{}]", this, ussd, e);
      }
      ussdSession = null;
    }
    return response;
  }

  public String startUSSDSession(final String ussd) throws GsmChannelError {
    if (rmiMediatorUssdProcessThread != null && rmiMediatorUssdProcessThread.isAlive()) {
      throw new USSDError(String.format("[%s] - can't start USSD session, because other session started, please wait 3 minutes and try to send USSD again [%s]", this, ussd));
    }
    logger.debug("[{}] - start USSD session={}", this, ussd);
    simUnit.handleEvent(SimEvent.ussdSessionRequest(ussd));
    rmiMediatorUssdProcessThread = new RmiMediatorUssdProcessThread(ussdSession, gsmUnit);
    rmiMediatorUssdProcessThread.start();
    rmiMediatorUssdProcessThread.startUSSDSession(ussd);
    String response = rmiMediatorUssdProcessThread.readResponseCommand();
    logger.debug("[{}] - receive USSD response [{}]", CallChannel.this, response);
    simUnit.handleEvent(SimEvent.ussdSessionResponse(response));
    return response;
  }

  public String sendUSSDSessionCommand(final String command) throws GsmChannelError {
    rmiMediatorUssdProcessThread.writeRequestCommand(command);
    logger.debug("[{}] - send USSD command [{}]", CallChannel.this, command);
    simUnit.handleEvent(SimEvent.ussdSessionRequest(command));
    String response = rmiMediatorUssdProcessThread.readResponseCommand();
    logger.debug("[{}] - receive USSD response [{}]", CallChannel.this, response);
    simUnit.handleEvent(SimEvent.ussdSessionResponse(response));
    return response;
  }

  public void endUSSDSession() {
    logger.debug("[{}] - end USSD session", CallChannel.this);
    try {
      rmiMediatorUssdProcessThread.endUSSDSession();
    } catch (GsmChannelError gsmChannelError) {
      logger.warn("[{}] - close USSD session with ERROR", this, gsmChannelError);
    }
  }

  public void sendDTMF(final String dtmf) throws GsmChannelError, DTMFException {
    OutgoingCall call = getOutgoingCall();
    if (call != null) {
      call.sendDTMF(dtmf);
    } else {
      throw new DTMFException("there is no active call at this moment");
    }
  }

  public HTTPResponse sendHTTPRequest(final String apn, final String urlStr, final String caCertificate) throws GsmChannelError {
    logger.info("[{}] - try send HTTP request to: [{}]", this, urlStr);
    IHTTPSession httpSession = gsmUnit.getHttpSession();
    URL url;
    try {
      url = new URL(urlStr);
    } catch (MalformedURLException e) {
      throw new HTTPError("can't decode url", e);
    }
    try {
      if (!prevHttpServer.equals(url.getProtocol() + url.getHost())) {
        if (httpSessionOpened) {
          httpSession.close();
          httpSessionOpened = false;
        }
        int port = url.getPort();
        if (port == -1) {
          port = url.getDefaultPort();
        }

        String caCertificateWithCRLF = null;
        if (url.getProtocol().equals("https") && caCertificate != null && !caCertificate.isEmpty()) {
          StringTokenizer stringTokenizer = new StringTokenizer(caCertificate, "\r\n");
          caCertificateWithCRLF = "";
          while (stringTokenizer.hasMoreElements()) {
            caCertificateWithCRLF += stringTokenizer.nextElement() + "\r\n";
          }
        }

        httpSession.open(apn, new InetSocketAddress(url.getHost(), port), caCertificateWithCRLF);
        httpSessionOpened = true;
      }
      prevHttpServer = url.getProtocol() + url.getHost();
      HTTPResponse httpResponse = httpSession.executeRequest(url.getPath());
      logger.info("[{}] - HTTP request send successful", this);
      logger.trace("[{}] - httpStatusCode: [{}], ContentType: [{}], data: [{}]", this, httpResponse.getHttpStatusCode(), httpResponse.getContentType(), httpResponse.getData());
      simUnit.handleEvent(SimEvent.httpRequest(String.format("Received HTTP response: [%s], from url: [%s], httpStatusCode: [%d], ContentType: [%s],",
          httpResponse.getData(), url, httpResponse.getHttpStatusCode(), httpResponse.getContentType())));
      return httpResponse;
    } catch (ChannelException | InterruptedException e) {
      logger.warn("Can't end HTTP request to: [{}]", urlStr);
      simUnit.handleEvent(SimEvent.httpRequest(String.format("Can't execute HTTP request: [%s]", e.getMessage())));
      throw new HTTPError("can't execute HTTP request", e);
    }
  }

  public void handleCallSetup(final PhoneNumber phoneNumber) {
    activityLogger.logCallSetup(getSIMUnit().getChannelUID(), phoneNumber);
    changeCallState(CallState.State.DIALING);
  }

  public void handleCallStarted(final PhoneNumber phoneNumber) {
    activityLogger.logCallStarted(getSIMUnit().getChannelUID(), phoneNumber);
  }

  public void handleCallEnd(final long callDuration, final int causeCode) {
    logger.debug("[{}] - call Duration {}", this, callDuration);
    getSIMUnit().handleCallEnded(callDuration, causeCode);
    activityLogger.logCallEnded(getSIMUnit().getChannelUID(), callDuration);
  }

  public void handleCallRelease() {
    activityLogger.logCallRelease(getSIMUnit().getChannelUID());
    changeCallState(CallState.State.IDLE);
  }

  public void handelPdd(final long pdd) {
    activityLogger.logPdd(getSIMUnit().getChannelUID(), pdd);
  }

  public OutgoingCall getOutgoingCall() {
    Call call = getCall();
    if (call instanceof OutgoingCall) {
      return (OutgoingCall) call;
    } else {
      return null;
    }
  }

  public boolean isOutgoingCallReleased() {
    return isCallReleased(getOutgoingCall());
  }

  public void dropOutgoingCall(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    dropCall(getOutgoingCall(), callDropReason, cdrDropReason);
  }

  public void resetOutgoingCall() {
    resetCall(getOutgoingCall());
  }

  public IncomingCall getIncomingCall() {
    Call call = getCall();
    if (call instanceof IncomingCall) {
      return (IncomingCall) call;
    } else {
      return null;
    }
  }

  public boolean isIncomingCallReleased() {
    return isCallReleased(getIncomingCall());
  }

  public void dropIncomingCall(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    dropCall(getIncomingCall(), callDropReason, cdrDropReason);
  }

  public void resetIncomingCall() {
    resetCall(getIncomingCall());
  }

  public long getCallDuration() {
    return simUnit.getSimData().getCallDuration();
  }

  private UUID internalSendSMS(final PhoneNumber number, final String text, final boolean needReport) throws SMSException {
    activityLogger.logCallChannelChangedAdvInfo(simUnit.getSimUnitId(), "sending SMS to " + number.getValue());
    List<SMSMessageOutbound> smsParts;
    try {
      smsParts = gsmUnit.sendSMS(number, text, needReport);
    } catch (SendSMSException e) {
      activityLogger.logCallChannelFailSentSMS(getSIMUnit().getChannelUID(), number.getValue(), text, e.getTotalSmsParts(), e.getAmountOfSendSmsParts());
      simUnit.handleFailSentSms(e.getTotalSmsParts(), e.getAmountOfSendSmsParts());
      activityLogger.logCallChannelChangedAdvInfo(simUnit.getSimUnitId(), "");
      throw e;
    }
    UUID localSmsId = UUID.randomUUID();

    activityLogger.logCallChannelSuccessSentSMS(getSIMUnit().getChannelUID(), number.getValue(), text, smsParts.size());
    simUnit.handleEvent(SimEvent.smsSent(number.getValue(), text, smsParts.size(), localSmsId));
    simUnit.handleSentSms(number, text, smsParts.size());
    simUnit.handleMultiPartSentSms(localSmsId, smsParts);
    activityLogger.logCallChannelChangedAdvInfo(simUnit.getSimUnitId(), "");
    return localSmsId;
  }

  public UUID sendSMS(final PhoneNumber number, final String text) throws SMSException {
    return internalSendSMS(simUnit.substituteSmsPhoneNumber(number), simUnit.substituteSmsText(number, text), simUnit.getSimData().getSimGroup().isSmsDeliveryReport());
  }

  public void handleIvr(final String ivrTemplateName, final CallDropReason callDropReason) {
    simUnit.handleIvr(ivrTemplateName, callDropReason);
  }

  private final class MyRegisteredInGsmCallChannel implements RegisteredInGSMChannel {

    @Override
    public void lock(final String reason) {
      simUnit.lock(true, reason);
    }

    @Override
    public void fireGenericEvent(final String event, final Serializable... args) {
      simUnit.handleGenericEvent(event, args);
    }

    @Override
    public void dropCall() {
      dropOutgoingCall(CallDropReason.NORMAL_CLEARING, CdrDropReason.VS);
    }

    @Override
    public void dropCall(final byte callDropCauseCode, final CdrDropReason cdrDropReason) {
      dropOutgoingCall(CallDropReason.fromCauseCode(callDropCauseCode), cdrDropReason);
    }

    @Override
    public void addToBlackList(final BlackListNumber phoneNumber) throws Exception {
      historyLogger.handleNewBlackListPhoneNumber(phoneNumber);
    }

    @Override
    public void changeGroup(final SimGroupReference simGroup) throws Exception {
      simUnit.changeGroup(simGroup, configuration.getPluginsStore());
    }

    @Override
    public void dial(final PhoneNumber number, final CallStateChangeHandler handler) throws Exception {
      CallChannel.this.dropCall(getCall(), CallDropReason.NORMAL_CLEARING, CdrDropReason.VS);

      CDR cdr = CDR.createCall(simUnit.getSimData().getPhoneNumber(), number, CallType.VS_TO_GSM).setVoiceServerName(AntraxProperties.SERVER_NAME);
      ScriptTerminatingCallAdapter scriptTerminatingCallAdapter = new ScriptTerminatingCallAdapter(number.getValue(), handler, simUnit);
      ITerminatingCallHandler terminatingCallHandlerLocal = processCall(new OutgoingScriptCall(CallChannel.this, cdr, scriptTerminatingCallAdapter, scriptTerminatingCallAdapter));
      if (terminatingCallHandlerLocal == null) {
        throw new CallManagementError("another call is active");
      }
    }

    @Override
    public void sendDTMF(final String dtmfString) throws Exception {
      OutgoingCall call = getOutgoingCall();
      if (call != null) {
        call.sendDTMF(dtmfString);
      } else {
        throw new Exception(String.format("[%s] - can't send DTMF [%s] - there is no active call at this moment", CallChannel.this, dtmfString));
      }
    }

    @Override
    public void sendDTMF(final char dtmf, final int duration) throws Exception {
      OutgoingCall call = getOutgoingCall();
      if (call != null) {
        call.sendDTMF(dtmf, duration);
      } else {
        throw new Exception(String.format("[%s] - can't send DTMF [%c] with duration [%d] - there is no active call at this moment", CallChannel.this, dtmf, duration));
      }
    }

    @Override
    public String sendUSSD(final String ussd) throws Exception {
      return CallChannel.this.sendUSSD(ussd);
    }

    @Override
    public USSDSession startUSSDSession(final String ussd) throws Exception {
      logger.debug("[{}] - start USSD session={}", this, ussd);
      simUnit.handleEvent(SimEvent.ussdSessionRequest(ussd));
      ussdSession = gsmUnit.openUSSDSession(ussd);
      return new USSDSession() {

        @Override
        public void sendCommand(final String command) throws Exception {
          ussdSession.writeResponse(command);
          logger.debug("[{}] - send USSD command [{}]", CallChannel.this, command);
          simUnit.handleEvent(SimEvent.ussdSessionRequest(command));
        }

        @Override
        public String readResponse() throws Exception {
          String response = ussdSession.readRequest();
          logger.debug("[{}] - receive USSD response [{}]", CallChannel.this, response);
          simUnit.handleEvent(SimEvent.ussdSessionResponse(response));
          return response;
        }

        @Override
        public void endSession() {
          logger.debug("[{}] - end USSD session", CallChannel.this);
          try {
            ussdSession.close();
          } catch (ChannelException | InterruptedException e) {
            logger.error(String.format("[%s] - can't close USSD session correctly", CallChannel.this), e);
          }
          ussdSession = null;
        }

      };
    }

    @Override
    public String toString() {
      return CallChannel.this.toString();
    }

    @Override
    public boolean shouldStopActivity() {
      return simUnit.shouldStopActivity();
    }

    @Override
    public boolean shouldStopSession() {
      return simUnit.shouldStopSession();
    }

    @Override
    public void sendSMS(final PhoneNumber phoneNumber, final String text) throws Exception {
      internalSendSMS(phoneNumber, text, false);
    }

    @Override
    public void executeAction(final Action action, final long maxTime) throws Exception {
      logger.debug("[{}] - asking to execute action {}", this, action);
      UUID exec = actionExecution.execute(action, maxTime);
      logger.debug("[{}] - got executionID {} for action {}", this, exec, action);
      getSIMUnit().handleEvent(SimEvent.automationActionExecutionStarted(action, exec));
      ExecutionStatus execStatus = ExecutionStatus.READY;
      int errors = 0;
      do {
        if (released) {
          throw new IllegalStateException("channel released");
        }

        Thread.sleep(1000);
        try {
          execStatus = actionExecution.getExecutionStatus(exec);
          logger.trace("[{}] - execution status {}", this, execStatus);
        } catch (RemoteAccessException ignored) {
          errors++;
          if (errors >= 3) {
            logger.debug("[{}] - it seems that sim server is down. Failing execution", this);
            getSIMUnit().handleEvent(SimEvent.automationActionFailed(action, exec, "no route to sim-server"));
            throw new ActionExecutionFailed("SimServer is down");
          }
        }
      } while (execStatus == ExecutionStatus.IN_PROGRESS || execStatus == ExecutionStatus.READY);
      if (execStatus == ExecutionStatus.FAILED) {
        logger.debug("[{}] - execution failed", this);
        getSIMUnit().handleEvent(SimEvent.automationActionFailed(action, exec, "failed"));
        throw new ActionExecutionFailed("No any voice-server succeed on action execution");
      }
      getSIMUnit().handleEvent(SimEvent.automationActionDone(action, exec));
    }

    @Override
    public SimData getSimData() {
      return getSIMUnit().getSimData();
    }

    @Override
    public void addUserMessage(final String message) {
      getSIMUnit().addUserMessage(message);
    }

    @Override
    public void updatePhoneNumber(final PhoneNumber phoneNumber) throws Exception {
      simUnit.updatePhoneNumber(phoneNumber);
    }

    @Override
    public void resetIMEI() throws Exception {
      simUnit.resetIMEI();
    }

    @Override
    public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
      return CallChannel.this.sendHTTPRequest(apn, url, caCertificate);
    }

    @Override
    public void changeAllowedInternet(final boolean allowed) {
      simUnit.handleAllowedInternet(allowed);
    }

    public void setTariffPlanEndDate(final long endDate) {
      simUnit.setTariffPlanEndDate(endDate);
    }

    @Override
    public void setBalance(final double balance) {
      simUnit.setBalance(balance);
    }

  }

  void saveSMS(final String number, final String text, final Date date, final int parts) {
    try {
      activityLogger.logCallChannelReceivedSMS(getSIMUnit().getChannelUID(), number, text, parts);
      simUnit.handleEvent(SimEvent.smsReceived(number, text, date, parts));
      simUnit.handleIncomingSMS(number, text, parts);
    } catch (Exception e) {
      logger.warn("[{}] - while saving SMS", this, e);
    }
  }

  void saveSMS(final String number, final String text, final Date date) {
    saveSMS(number, text, date, 1);
  }

  void saveSMS(final SMSMessageInbound smsMessageInbound) {
    simUnit.handleReceivedSms(smsMessageInbound);
  }

  void handleReceivedSmsStatusReport(final SMSStatusReport smsStatusReport) {
    simUnit.handleReceivedSmsStatusReport(smsStatusReport);
  }

  public synchronized void processBusinessActivity() {
    if (businessActivityThread != null && businessActivityThread.isAlive()) {
      return;
    }
    if (simUnit.shouldStartBusinessActivity()) {
      businessActivityThread = new Thread("Business" + this) {
        @Override
        public void run() {
          MyRegisteredInGsmCallChannel channel = new MyRegisteredInGsmCallChannel();
          try {
            logger.debug("[{}] - starting business activity", this);
            simUnit.invokeBusinessActivity(channel);
            logger.debug("[{}] - business activity is finished", this);
          } catch (Throwable e) {
            logger.warn("[{}] - failed to invoke business activity", this, e);
          }
        }
      };
      businessActivityThread.start();
    }
  }

  public synchronized boolean isInBusinessActivity() {
    return (businessActivityThread != null && businessActivityThread.isAlive());
  }

  public void changeChannelState(final CallChannelState.State state, final long period) {
    activityLogger.logCallChannelChangedState(simUnit.getSimUnitId(), state, "", period);
    simUnit.changeState(state);
  }

  public void changeCallState(final CallState.State state) {
    activityLogger.logCallChangeState(simUnit.getSimUnitId(), state);
    callState = state;
  }

  public CallState.State getCallState() {
    return callState;
  }

  public void updateSignalQuality(final int rssi, final int ber) { //TODO: divide to 2 methods
    activityLogger.logSignalQualityChange(gsmUnit.getChannelUID(), rssi, ber);
  }

  public void updateGSMNetworkInfo(final GSMNetworkInfo info) {
    activityLogger.logGSMNetworkInfoChange(gsmUnit.getChannelUID(), info);
    if (servingCell == null || cellInfoComparator.compare(servingCell, info.getCellInfo()[0]) != 0) {
      servingCell = info.getCellInfo()[0];
      simUnit.handleEvent(SimEvent.servingCellChanged(servingCell));
    }
  }

  public void take() {
    taken = true;
  }

  public void untake() {
    taken = false;
  }

  public boolean isTaken() {
    return taken;
  }

  public boolean isSupportsAction(final String action) {
    return simUnit.isSupportsAction(action);
  }

  public boolean isUSSDSessionOpen() {
    return ussdSession != null;
  }

  public ActionProviderScript getActionProvider(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    return simUnit.getActionProvider(action, channel);
  }

  public void executeAction(final ActionProviderScript script, final Action action, final RegisteredInGSMChannel channel, final UUID executionID) throws Exception {
    simUnit.executeAction(script, action, channel, executionID);
  }

  public RegisteredInGSMChannel toRegisteredInGSMChannel() {
    return new MyRegisteredInGsmCallChannel();
  }

  private static class CellInfoComparator implements Comparator<CellInfo> {

    @Override
    public int compare(final CellInfo cellInfo1, final CellInfo cellInfo2) {
      int result = cellInfo1.getLac() - cellInfo2.getLac();
      if (result != 0) {
        return result;
      }
      result = cellInfo1.getCellId() - cellInfo2.getCellId();
      if (result != 0) {
        return result;
      }
      result = cellInfo1.getBsic() - cellInfo2.getBsic();
      if (result != 0) {
        return result;
      }
      return cellInfo1.getArfcn() - cellInfo2.getArfcn();
    }

  }

  //TODO: remove these setters after architecture review

  public void setSignalBitErrorRate(final int rate) {
    this.rate = rate;
  }

  public void setSignalReceivedStrength(final int strength) {
    this.strength = strength;
  }

  public int getSignalBitErrorRate() {
    return rate;
  }

  public int getSignalReceivedStrength() {
    return strength;
  }

  public void setCellMonitoringInfo(final CellInfo[] cellInfos) {
    this.cellInfos = cellInfos;
    gsmUnit.handleCellInfos(cellInfos);
  }

  public CellInfo[] getCellMonitoringInfo() {
    return cellInfos == null ? null : cellInfos.clone();
  }

  public void setRegistrationStatus(final Pair<NetworkRegistrationStatus, String> registrationStatus) {
    this.registrationStatus = registrationStatus;
  }

  public void handleSupplementaryServiceNotification(final SupplementaryServiceNotificationCode code) {
    if (code == SupplementaryServiceNotificationCode.CALL_HAS_BEEN_FORWARDED) {
      simUnit.handleCallForwarding();
    }
  }

  public Pair<NetworkRegistrationStatus, String> getNetworkRegistrationStatus() {
    return new Pair<>(registrationStatus.first(), registrationStatus.second());
  }

  private Call processCall(final Call call) {
    if (!currentCall.compareAndSet(null, call)) {
      return null;
    }

    callProcessThreadLock.lock();
    try {
      if (callProcessThread != null) {
        callProcessThread.execute(call);
        return call;
      }
    } finally {
      callProcessThreadLock.unlock();
    }

    logger.debug("[{}] - process call [{}] before creating channel or while waiting release", this, call);
    resetCall(call);
    return null;
  }


  private Call getCall() {
    return currentCall.get();
  }

  private boolean isCallReleased(final Call call) {
    return call == null || call.isReleased();
  }

  private void dropCall(final Call call, final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    if (!isCallReleased(call)) {
      call.drop(callDropReason, cdrDropReason);
      resetCall(call);
    }
  }

  private void resetCall(final Call call) {
    currentCall.compareAndSet(call, null);
    handleCallRelease();
  }

  private class CallProcessThread extends Thread {

    private final Exchanger<Runnable> exchanger = new Exchanger<>();

    public CallProcessThread() {
      super("CallProcessThread[" + CallChannel.this + "]");
    }

    @Override
    public void run() {
      while (true) {
        Runnable runnable;
        try {
          runnable = exchanger.exchange(null);
        } catch (InterruptedException e) {
          logger.warn("[{}] - while wait call task to process states was interrupted", CallChannel.this, e);
          break;
        }

        if (runnable == releaseTask) {
          break;
        } else {
          runnable.run();
        }
      }
    }

    private void execute(final Runnable runnable) {
      try {
        exchanger.exchange(runnable);
      } catch (InterruptedException e) {
        logger.warn("[{}] - while execute call task to process states was interrupted", CallChannel.this, e);
      }
    }

  }

  // INFO: This class created for Siemens gsm module.
  private static class RmiMediatorUssdProcessThread extends Thread {

    private final long WAITING_TIME_LAST_EXECUTE_REMOTE_USSD = 3;

    private final Lock lock = new ReentrantLock();
    private final Condition conditionRequest = lock.newCondition();
    private final Condition conditionResponse = lock.newCondition();

    private final GSMUnit gsmUnit;

    private Command command = Command.UNDEFINED;
    private String request;
    private String response;
    private Exception lastException;
    private IUSSDSession ussdSession;

    private enum Command {
      OPEN, CLOSE, WRITE_REQUEST, READ_RESPONSE, UNDEFINED
    }

    public RmiMediatorUssdProcessThread(final IUSSDSession ussdSession, final GSMUnit gsmUnit) {
      super("RmiMediatorUssdProcessThread[" + RmiMediatorUssdProcessThread.class + "]");
      setDaemon(true);
      this.ussdSession = ussdSession;
      this.gsmUnit = gsmUnit;
    }

    @Override
    public void run() {
      while (true) {
        lock.lock();
        try {
          if (command == Command.UNDEFINED) {
            conditionRequest.await(WAITING_TIME_LAST_EXECUTE_REMOTE_USSD, TimeUnit.MINUTES);
          }
          response = null;
          lastException = null;
          switch (command) {
            case OPEN:
              ussdSession = gsmUnit.openUSSDSession(request);
              break;
            case WRITE_REQUEST:
              ussdSession.writeResponse(request);
              break;
            case READ_RESPONSE:
              response = ussdSession.readRequest();
              break;
            case CLOSE:
            case UNDEFINED:
              closeUssdSession();
              return;
          }
        } catch (Exception e) {
          lastException = e;
          response = null;
          closeUssdSession();
          return;
        } finally {
          request = null;
          command = Command.UNDEFINED;
          conditionResponse.signalAll();
          lock.unlock();
        }
      }
    }

    public void startUSSDSession(final String ussd) throws GsmChannelError {
      lock.lock();
      try {
        command = Command.OPEN;
        request = ussd;
        signalAndWaitResult();
      } catch (Exception e) {
        throw new USSDError(String.format("[%s] - can't start USSD session [%s]", this, ussd), e);
      } finally {
        lock.unlock();
      }
    }

    public void writeRequestCommand(final String ussd) throws GsmChannelError {
      lock.lock();
      try {
        command = Command.WRITE_REQUEST;
        request = ussd;
        signalAndWaitResult();
      } catch (Exception e) {
        throw new USSDError(String.format("[%s] - can't write request USSD [%s]", this, ussd), e);
      } finally {
        lock.unlock();
      }
    }

    public String readResponseCommand() throws GsmChannelError {
      lock.lock();
      try {
        command = Command.READ_RESPONSE;
        signalAndWaitResult();
        return response;
      } catch (Exception e) {
        throw new USSDError(String.format("[%s] - can't read USSD response", this), e);
      } finally {
        lock.unlock();
      }
    }

    public void endUSSDSession() throws GsmChannelError {
      lock.lock();
      try {
        command = Command.CLOSE;
        signalAndWaitResult();
      } catch (Exception e) {
        throw new USSDError(String.format("[%s] - can't end USSD [%s]", this, command), e);
      } finally {
        lock.unlock();
      }
    }

    private void signalAndWaitResult() throws Exception {
      conditionRequest.signal();
      conditionResponse.await(WAITING_TIME_LAST_EXECUTE_REMOTE_USSD, TimeUnit.MINUTES);
      if (lastException != null) {
        throw lastException;
      }
    }

    private void closeUssdSession() {
      if (ussdSession != null) {
        try {
          ussdSession.close();
        } catch (ChannelException | InterruptedException ignore) {
        } finally {
          ussdSession = null;
        }
      }
    }

  }

}

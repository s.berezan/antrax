/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.ChannelUID;

import java.util.List;

public interface IConfigurator {

  void configure(List<GSMUnit> gsmUnits);

  void configure(GSMUnit gsmUnit);

  void updateGsmToSimGroupLinks(Configuration cfg);

  void updatePluginsStore(AntraxPluginsStore antraxPluginsStore);

  boolean configure(SIMUnit simUnit, AntraxPluginsStore pluginsStore);

  void clearResetVsScriptsFlag(SIMUnit simUnit);

  boolean shouldResetVsScripts(SIMUnit simUnit);

  GSMChannel configure(ChannelUID channelUID);

  void initAudioCaptureEnabled(Configuration cfg);

}

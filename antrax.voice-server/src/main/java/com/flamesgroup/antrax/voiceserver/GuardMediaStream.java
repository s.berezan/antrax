/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;

public class GuardMediaStream implements IMediaStream {

  private final IMediaStream mediaStream;

  private volatile IMediaStreamHandler mediaStreamHandler;

  public GuardMediaStream(final IMediaStream mediaStream) {
    this.mediaStream = mediaStream;
  }

  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    this.mediaStreamHandler = mediaStreamHandler;
    mediaStream.open(mediaStreamHandler);
  }

  @Override
  public void close() {
    mediaStream.close();
    mediaStreamHandler = null;
  }

  @Override
  public boolean isOpen() {
    return mediaStreamHandler != null;
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    if (isOpen()) { // to prevent sending audio data without ringing state
      mediaStream.write(timestamp, data, offset, length);
    }
  }

}

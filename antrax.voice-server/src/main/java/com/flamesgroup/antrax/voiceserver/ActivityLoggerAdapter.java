/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.control.communication.IActivityRemoteLogger;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerLongInformation;
import com.flamesgroup.antrax.control.communication.IVoiceServerShortInfo;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.List;

public class ActivityLoggerAdapter implements IActivityLogger {

  private static final Logger logger = LoggerFactory.getLogger(ActivityLoggerAdapter.class);

  private final Server server = new Server(AntraxProperties.SERVER_NAME, ServerType.VOICE_SERVER);

  private final IActivityLogger activityLogger;
  private final IActivityRemoteLogger activityRemoteLogger;

  public ActivityLoggerAdapter(final IActivityLogger activityLogger, final IActivityRemoteLogger activityRemoteLogger) {
    this.activityLogger = activityLogger;
    this.activityRemoteLogger = activityRemoteLogger;
  }

  @Override
  public List<IVoiceServerChannelsInfo> getVoiceServerChannelsInfos() {
    return activityLogger.getVoiceServerChannelsInfos();
  }

  @Override
  public IVoiceServerChannelsInfo getVoiceServerChannelsInfo(final ChannelUID gsmUnit) {
    return activityLogger.getVoiceServerChannelsInfo(gsmUnit);
  }

  @Override
  public IVoiceServerShortInfo getShortServerInfo() {
    return activityLogger.getShortServerInfo();
  }

  @Override
  public IVoiceServerLongInformation getLongServerInfo() {
    return activityLogger.getLongServerInfo();
  }

  @Override
  public void changeServerStatus(final ServerStatus status) {
    activityLogger.changeServerStatus(status);
    try {
      activityRemoteLogger.changeServerStatus(server, status);
    } catch (RemoteException e) {
      logger.error("[{}] - change server status error", this, e);
    }
  }

  @Override
  public void logDeclaredGsmUnit(final GSMChannel channel) {
    activityLogger.logDeclaredGsmUnit(channel);
    try {
      activityRemoteLogger.logDeclaredGsmUnit(server, channel);
    } catch (RemoteException e) {
      logger.error("[{}] - log declared gsm unit error", this, e);
    }
  }

  @Override
  public void logActiveGsmUnit(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    activityLogger.logActiveGsmUnit(gsmUnit, gsmChannel, channelConfig);
    try {
      activityRemoteLogger.logActiveGsmUnit(server, gsmUnit, gsmChannel, channelConfig);
    } catch (RemoteException e) {
      logger.error("[{}] - log active gsm unit error", this, e);
    }
  }

  @Override
  public void updateChannelConfig(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    activityLogger.updateChannelConfig(gsmUnit, gsmChannel, channelConfig);
    try {
      activityRemoteLogger.updateChannelConfig(server, gsmUnit, gsmChannel, channelConfig);
    } catch (RemoteException e) {
      logger.error("[{}] - update channel config error", this, e);
    }
  }

  @Override
  public void logGsmUnitChangedGroup(final ChannelUID gsmUnit, final GSMGroup group) {
    activityLogger.logGsmUnitChangedGroup(gsmUnit, group);
    try {
      activityRemoteLogger.logGsmUnitChangedGroup(server, gsmUnit, group);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm unit changed group error", this, e);
    }
  }

  @Override
  public void logGsmUnitLock(final ChannelUID gsmUnit, final boolean lock, final String lockReason) {
    activityLogger.logGsmUnitLock(gsmUnit, lock, lockReason);
    try {
      activityRemoteLogger.logGsmUnitLock(server, gsmUnit, lock, lockReason);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm unit lock error", this, e);
    }
  }

  @Override
  public void logGSMUnitReleased(final ChannelUID gsmUnit) {
    activityLogger.logGSMUnitReleased(gsmUnit);
    try {
      activityRemoteLogger.logGSMUnitReleased(server, gsmUnit);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm unit released error", this, e);
    }
  }

  @Override
  public void logSIMTakenFromSimServer(final ChannelUID simUnit, final ICCID simUid) {
    activityLogger.logSIMTakenFromSimServer(simUnit, simUid);
    try {
      activityRemoteLogger.logSIMTakenFromSimServer(server, simUnit, simUid);
    } catch (RemoteException e) {
      logger.error("[{}] - log sim taken from sim server error", this, e);
    }
  }

  @Override
  public void logSIMChangedEnableStatus(final ChannelUID simUnit, final boolean enable) {
    activityLogger.logSIMChangedEnableStatus(simUnit, enable);
    try {
      activityRemoteLogger.logSIMChangedEnableStatus(server, simUnit, enable);
    } catch (RemoteException e) {
      logger.error("[{}] - log sim changed enable status error", this, e);
    }
  }

  @Override
  public void logSIMChangedGroup(final ChannelUID simUnit, final String group) {
    activityLogger.logSIMChangedGroup(simUnit, group);
    try {
      activityRemoteLogger.logSIMChangedGroup(server, simUnit, group);
    } catch (RemoteException e) {
      logger.error("[{}] - log sim changed group error", this, e);
    }
  }

  @Override
  public void logSIMReturnedToSimServer(final ChannelUID simUnit) {
    activityLogger.logSIMReturnedToSimServer(simUnit);
    try {
      activityRemoteLogger.logSIMReturnedToSimServer(server, simUnit);
    } catch (RemoteException e) {
      logger.error("[{}] - log sim returned to sim server error", this, e);
    }
  }

  @Override
  public void logCallChannelCreated(final ChannelUID gsmUnit, final ChannelUID simUnit) {
    activityLogger.logCallChannelCreated(gsmUnit, simUnit);
    try {
      activityRemoteLogger.logCallChannelCreated(server, gsmUnit, simUnit);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel created error", this, e);
    }
  }

  @Override
  public void logCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) {
    activityLogger.logCallChannelChangedState(simUnit, state, advInfo, periodPrediction);
    try {
      activityRemoteLogger.logCallChannelChangedState(server, simUnit, state, advInfo, periodPrediction);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel changed state error", this, e);
    }
  }

  @Override
  public void logCallChannelChangedAdvInfo(final ChannelUID simUnit, final String advInfo) {
    activityLogger.logCallChannelChangedAdvInfo(simUnit, advInfo);
    try {
      activityRemoteLogger.logCallChannelChangedAdvInfo(server, simUnit, advInfo);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel changed adv info error", this, e);
    }
  }

  @Override
  public void logGsmUnitChangedState(final ChannelUID gsmUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) {
    activityLogger.logGsmUnitChangedState(gsmUnit, state, advInfo, periodPrediction);
    try {
      activityRemoteLogger.logGsmUnitChangedState(server, gsmUnit, state, advInfo, periodPrediction);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm unit changed state error", this, e);
    }
  }

  @Override
  public void logCallChannelStartActivity(final ChannelUID simUnit) {
    activityLogger.logCallChannelStartActivity(simUnit);
    try {
      activityRemoteLogger.logCallChannelStartActivity(server, simUnit);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel start activity error", this, e);
    }
  }

  @Override
  public void logCallChannelReleased(final ChannelUID simUnit) {
    activityLogger.logCallChannelReleased(simUnit);
    try {
      activityRemoteLogger.logCallChannelReleased(server, simUnit);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel released error", this, e);
    }
  }

  @Override
  public void logCallStarted(final ChannelUID simUnit, final PhoneNumber phoneNumber) {
    activityLogger.logCallStarted(simUnit, phoneNumber);
    try {
      activityRemoteLogger.logCallStarted(server, simUnit, phoneNumber);
    } catch (RemoteException e) {
      logger.error("[{}] - log call started error", this, e);
    }
  }

  @Override
  public void logCallChangeState(final ChannelUID simUnit, final CallState.State callState) {
    activityLogger.logCallChangeState(simUnit, callState);
    try {
      activityRemoteLogger.logCallChangeState(server, simUnit, callState);
    } catch (RemoteException e) {
      logger.error("[{}] - log call change state error", this, e);
    }
  }

  @Override
  public void logCallEnded(final ChannelUID simUnit, final long duration) {
    activityLogger.logCallEnded(simUnit, duration);
    try {
      activityRemoteLogger.logCallEnded(server, simUnit, duration);
    } catch (RemoteException e) {
      logger.error("[{}] - log call ended error", this, e);
    }
  }

  @Override
  public void logCallChannelSendUSSD(final ChannelUID simUnit, final String ussd, final String response) {
    activityLogger.logCallChannelSendUSSD(simUnit, ussd, response);
    try {
      activityRemoteLogger.logCallChannelSendUSSD(server, simUnit, ussd, response);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel send USSD error", this, e);
    }
  }

  @Override
  public void logCallChannelSuccessSentSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    activityLogger.logCallChannelSuccessSentSMS(simUnit, number, text, parts);
    try {
      activityRemoteLogger.logCallChannelSuccessSentSMS(server, simUnit, number, text, parts);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel success sent SMS error", this, e);
    }
  }

  @Override
  public void logCallChannelFailSentSMS(final ChannelUID simUnit, final String number, final String text, final int totalSmsParts, final int successSendSmsParts) {
    activityLogger.logCallChannelFailSentSMS(simUnit, number, text, totalSmsParts, successSendSmsParts);
    try {
      activityRemoteLogger.logCallChannelFailSentSMS(server, simUnit, number, text, totalSmsParts, successSendSmsParts);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel fail sent SMS error", this, e);
    }
  }

  @Override
  public void logCallChannelReceivedSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    activityLogger.logCallChannelReceivedSMS(simUnit, number, text, parts);
    try {
      activityRemoteLogger.logCallChannelReceivedSMS(server, simUnit, number, text, parts);
    } catch (RemoteException e) {
      logger.error("[{}] - log call channel received SMS error", this, e);
    }
  }

  @Override
  public void logSignalQualityChange(final ChannelUID gsmUnit, final int signalStrength, final int bitErrorRate) {
    activityLogger.logSignalQualityChange(gsmUnit, signalStrength, bitErrorRate);
    try {
      activityRemoteLogger.logSignalQualityChange(server, gsmUnit, signalStrength, bitErrorRate);
    } catch (RemoteException e) {
      logger.error("[{}] - log signal quality change error", this, e);
    }
  }

  @Override
  public void logGSMNetworkInfoChange(final ChannelUID gsmUnit, final GSMNetworkInfo info) {
    activityLogger.logGSMNetworkInfoChange(gsmUnit, info);
    try {
      activityRemoteLogger.logGSMNetworkInfoChange(server, gsmUnit, info);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm network info change error", this, e);
    }
  }

  @Override
  public void logPdd(final ChannelUID simUnit, final long pdd) {
    activityLogger.logPdd(simUnit, pdd);
    try {
      activityRemoteLogger.logPdd(server, simUnit, pdd);
    } catch (RemoteException e) {
      logger.error("[{}] - log pdd error", this, e);
    }
  }

  @Override
  public void resetStatistic() {
    activityLogger.resetStatistic();
    try {
      activityRemoteLogger.resetStatistic(server);
    } catch (RemoteException e) {
      logger.error("[{}] - reset statistic error", this, e);
    }
  }

  @Override
  public void logGsmUnitLockToArfcn(final ChannelUID channel, final Integer arfcn) {
    activityLogger.logGsmUnitLockToArfcn(channel, arfcn);
    try {
      activityRemoteLogger.logGsmUnitLockToArfcn(server, channel, arfcn);
    } catch (RemoteException e) {
      logger.error("[{}] - log gsm unit lock to arfcn error", this, e);
    }
  }

  @Override
  public void logCallSetup(final ChannelUID simUID, final PhoneNumber phoneNumber) {
    activityLogger.logCallSetup(simUID, phoneNumber);
    try {
      activityRemoteLogger.logCallSetup(server, simUID, phoneNumber);
    } catch (RemoteException e) {
      logger.error("[{}] - log call setup error", this, e);
    }
  }

  @Override
  public void logCallRelease(final ChannelUID simUID) {
    activityLogger.logCallRelease(simUID);
    try {
      activityRemoteLogger.logCallRelease(server, simUID);
    } catch (RemoteException e) {
      logger.error("[{}] - log call release error", this, e);
    }
  }

}

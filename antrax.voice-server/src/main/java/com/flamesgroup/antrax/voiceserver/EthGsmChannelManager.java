/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.commons.DeviceJournalHelper;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.utils.AliasDeviceUID;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.ChannelConfig.ChannelState;
import com.flamesgroup.antrax.distributor.DeviceManagerHelper;
import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpStream;
import com.flamesgroup.device.protocol.cudp.ExpireTime;
import com.flamesgroup.device.protocol.cudp.ICudpConnection;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigLicenseExpireException;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigLicenseInvalidException;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigParametersErrException;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EthGsmChannelManager implements IGsmChannelManager {

  private static final Logger logger = LoggerFactory.getLogger(EthGsmChannelManager.class);

  private static final int CUDP_DEVCIE_PORT = 9010;

  private final SocketAddress remoteBroadcastAddress;
  private final Map<DeviceUID, IPConfig> deviceIPConfigs;
  private final UUID ipConfigUUID;

  private final UnitActivator activator;
  private final IConfigurator configurator;
  private final IActivityLogger activityLogger;

  private final Map<DeviceUID, Integer> lostAttempts = new HashMap<>();
  private final Map<DeviceUID, Long> setIPConfigTimeouts = new HashMap<>();

  private final Lock cudpConnectionLock = new ReentrantLock();
  private final ICudpConnection cudpConnection = new CudpConnection(new CudpStream(), 1000); //TODO: move to constructor

  private final Map<DeviceUID, IGSMDevice> activeDevices = new HashMap<>();
  private final Map<DeviceUID, List<GSMUnit>> activeChannels = new ConcurrentHashMap<>();

  private IGsmChannelManagerHandler channelManagerHandler;
  private GsmChannelManagerThread channelManagerThread;

  private ThreadPoolExecutor channelExecutor;

  private Timer setIPConfigTimer;

  public EthGsmChannelManager(final InetAddress broadcastAddress, final Map<DeviceUID, IPConfig> deviceIPConfigs,
      final UUID ipConfigUUID, final UnitActivator activator, final IConfigurator configurator, final IActivityLogger activityLogger) {
    this.remoteBroadcastAddress = new InetSocketAddress(broadcastAddress, CUDP_DEVCIE_PORT);
    this.deviceIPConfigs = deviceIPConfigs;
    this.ipConfigUUID = ipConfigUUID;
    this.activator = activator;
    this.configurator = configurator;
    this.activityLogger = activityLogger;
  }

  @Override
  public synchronized void start(final IGsmChannelManagerHandler channelManagerHandler) {
    if (channelManagerThread != null) {
      throw new IllegalStateException(String.format("[%s] - OldChannelManager has already connected", this));
    }

    cudpConnectionLock.lock();
    try {
      try {
        cudpConnection.connect(remoteBroadcastAddress);
      } catch (CudpException e) {
        throw new IllegalStateException(String.format("[%s] - can't connect to remote broadcast address %s", this, remoteBroadcastAddress), e);
      }
    } finally {
      cudpConnectionLock.unlock();
    }

    activator.start(channelManagerHandler);
    this.channelManagerHandler = channelManagerHandler;

    channelExecutor = new WaitThreadPoolExecutor(5, 10, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(5), r -> {
      Thread thread = new Thread(r);
      thread.setName("GsmChannelExecutor@" + thread.hashCode());
      return thread;
    });

    channelManagerThread = new GsmChannelManagerThread();
    channelManagerThread.start();

    setIPConfigTimer = new Timer();
    long setIPConfigInterval = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSetIpConfigInterval();
    setIPConfigTimer.scheduleAtFixedRate(new SetIPConfigTask(), setIPConfigInterval, setIPConfigInterval);
  }

  @Override
  public synchronized void stop() {
    if (channelManagerThread == null) {
      return;
    }

    setIPConfigTimer.cancel();
    setIPConfigTimer = null;

    Thread localDeviceThread = channelManagerThread;
    channelManagerThread = null;
    try {
      localDeviceThread.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - while join thread was interrupted", this, e);
    }

    try {
      for (Map.Entry<DeviceUID, IGSMDevice> entry : activeDevices.entrySet()) {
        channelExecutor.execute(new UnitRemoveTask(entry.getValue(), activeChannels.get(entry.getKey())));
      }
      channelExecutor.shutdown();
      while (!channelExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of DeviceRemoveExecutor", this);
      }
    } catch (InterruptedException e) {
      logger.debug("[{}] - while awaiting completion of executor", this, e);
    }

    activator.stop();
    channelManagerHandler = null;

    cudpConnectionLock.lock();
    try {
      try {
        cudpConnection.disconnect();
      } catch (CudpException | InterruptedException e) {
        logger.warn("[{}] - can't disconnect from remote broadcast address", this, e);
      }
    } finally {
      cudpConnectionLock.unlock();
    }
  }

  private ChannelUID createChannelUID(final DeviceUID deviceUID, final byte channelNumber) {
    return new ChannelUID(deviceUID, AliasDeviceUID.getAlias(deviceUID), channelNumber);
  }

  private class UnitAddTask implements Runnable {

    private final ChannelUID channel;
    private final IGSMEntry gsmEntry;
    private final ChannelConfig channelConfig;

    public UnitAddTask(final ChannelUID channel, final IGSMEntry gsmEntry, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.gsmEntry = gsmEntry;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      DeviceUID deviceUID = channel.getDeviceUID();
      List<GSMUnit> channels = activeChannels.get(deviceUID);
      if (channels == null) {
        return;
      }

      GSMUnit gsmUnit = new GSMUnit(gsmEntry, channel, channelManagerHandler);
      configurator.configure(gsmUnit);

      try {
        gsmUnit.plugIn();
      } catch (GsmChannelError e) {
        logger.error("[{}] - can't plug in for channel {}", this, channel, e);
        return;
      }

      try {
        channelManagerHandler.handleAvailableChannel(AntraxProperties.SERVER_NAME, channel);
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle available channel {}", this, channel, e);
        return;
      }

      activator.addGSMUnit(gsmUnit);
      activityLogger.logActiveGsmUnit(channel, gsmUnit.getGsmChannel(), channelConfig);
      channels.add(gsmUnit);
    }

    @Override
    public String toString() {
      return "UnitAddTask[" + channel + "]";
    }

  }

  private class UnitRemoveTask implements Runnable {

    private final IGSMDevice gsmDevice;
    private final List<GSMUnit> gsmUnits;

    public UnitRemoveTask(final IGSMDevice gsmDevice, final List<GSMUnit> gsmUnits) {
      this.gsmDevice = gsmDevice;
      this.gsmUnits = gsmUnits;
    }

    @Override
    public void run() {
      for (GSMUnit gsmUnit : gsmUnits) {
        gsmUnit.unplug();
        activator.removeGSMUnit(gsmUnit);
        activityLogger.logGSMUnitReleased(gsmUnit.getChannelUID());

        try {
          channelManagerHandler.handleLostChannels(AntraxProperties.SERVER_NAME, Collections.singletonList(gsmUnit.getChannelUID()));
        } catch (RemoteException e) {
          logger.warn("[{}] - can't handle lost channel {}", this, gsmUnit, e);
        }
      }

      try {
        gsmDevice.detach();
      } catch (MudpException | InterruptedException e) {
        logger.warn("[{}] - can't detach from device {}", this, gsmDevice, e);
      }
    }

    @Override
    public String toString() {
      return "UnitRemoveTask[" + gsmDevice + "]";
    }

  }

  private class GsmChannelManagerThread extends Thread {

    private static final int MUDP_DEVCIE_PORT = 9020;

    private static final long READ_DEVICE_UID_TIMEOUT = 3000;
    private static final long WAIT_RESPONSE_INTERVAL = 1000;
    private static final long RESET_DELAY = 100;

    private static final int LOST_DEVICE_ATTEMPT_COUNT = 2;
    private static final long SET_IP_CONFIG_TIMEOUT = 60_000;

    private final Set<DeviceType> supportedDeviceTypes = EnumSet.of(DeviceType.GSMB, DeviceType.GSMB2, DeviceType.GSMB3);

    public GsmChannelManagerThread() {
      setName("ChannelManagerThread[" + ipConfigUUID + "]");
    }

    @Override
    public void run() {
      for (DeviceUID deviceUID : deviceIPConfigs.keySet()) {
        List<ChannelUID> declaredChannels = new ArrayList<>();
        for (int i = 0; i < DeviceManagerHelper.getCountOfModules(deviceUID.getDeviceType()); i++) {
          declaredChannels.add(createChannelUID(deviceUID, (byte) i));
        }

        try {
          channelManagerHandler.handleDeclaredChannels(AntraxProperties.SERVER_NAME, declaredChannels);
        } catch (RemoteException e) {
          logger.error("[{}] - can't handle declared channels", this, e);
          return;
        }

        declaredChannels.forEach(c -> activityLogger.logDeclaredGsmUnit(configurator.configure(c)));
      }

      Thread currentThread = Thread.currentThread();
      while (currentThread == channelManagerThread) {
        try {
          Thread.sleep(READ_DEVICE_UID_TIMEOUT);

          Collection<Future<?>> futures = new LinkedList<>();
          Map<DeviceUID, Pair<IPConfig, ExpireTime>> availableDevices = new HashMap<>();
          cudpConnectionLock.lock();
          try {
            Set<DeviceUID> deviceUIDs;
            try {
              deviceUIDs = cudpConnection.getDeviceUIDs(WAIT_RESPONSE_INTERVAL);
            } catch (CudpException e) {
              logger.warn("[{}] - can't find devices", this, e);
              continue;
            }

            for (DeviceUID deviceUID : deviceUIDs) {
              if (!supportedDeviceTypes.contains(deviceUID.getDeviceType()) || !deviceIPConfigs.containsKey(deviceUID)) {
                continue;
              }

              try {
                IPConfig ipConfig = cudpConnection.getIPConfig(deviceUID);
                ExpireTime expireTime = cudpConnection.getExpireTime(deviceUID);
                availableDevices.put(deviceUID, new Pair<>(ipConfig, expireTime));
              } catch (CudpException e) {
                logger.warn("[{}] - {} is not responding", this, deviceUID.getCanonicalName(), e);
              }
            }

            for (Map.Entry<DeviceUID, Pair<IPConfig, ExpireTime>> entry : availableDevices.entrySet()) {
              DeviceUID deviceUID = entry.getKey();
              IPConfig ipConfig = entry.getValue().first();
              if (ipConfig.getUUID().equals(ipConfigUUID) && activeDevices.containsKey(deviceUID)) {
                continue;
              }

              if (ipConfig.getUUID().equals(IPConfig.UNINITIALIZED_UUID)) {
                if (activeDevices.containsKey(deviceUID)) {
                  if (activeChannels.get(deviceUID).stream().filter(Objects::nonNull).anyMatch(GSMUnit::isOwned)) {
                    logger.debug("[{}] - waiting released call channels for device {}", this, deviceUID);
                    continue;
                  }

                  logger.debug("[{}] - lost device {}", this, deviceUID.getCanonicalName());
                  futures.add(channelExecutor.submit(new UnitRemoveTask(activeDevices.remove(deviceUID), activeChannels.remove(deviceUID))));
                  continue;
                }

                if (setIPConfigTimeouts.containsKey(deviceUID)) {
                  if (System.currentTimeMillis() - setIPConfigTimeouts.get(deviceUID) >= SET_IP_CONFIG_TIMEOUT) {
                    setIPConfigTimeouts.remove(deviceUID);
                  } else {
                    continue;
                  }
                }

                ChannelState errorStatus = ChannelState.ACTIVE;
                IPConfig deviceIPConfig = deviceIPConfigs.get(deviceUID);
                try {
                  cudpConnection.setIPConfig(deviceUID, deviceIPConfig);
                } catch (SetIPConfigParametersErrException ignored) {
                  logger.warn("[{}] - device {} get invalid one of ip config parameters {}", this, deviceUID.getCanonicalName(), deviceIPConfig);
                  errorStatus = ChannelState.IP_CONFIG_PARAMETERS_INVALID;
                } catch (SetIPConfigLicenseInvalidException ignored) {
                  logger.warn("[{}] - device {} has invalid license", this, deviceUID);
                  errorStatus = ChannelState.LICENSE_INVALID;
                } catch (SetIPConfigLicenseExpireException ignored) {
                  logger.warn("[{}] - device {} has expired license", this, deviceUID);
                  errorStatus = ChannelState.LICENSE_EXPIRE;
                } catch (CudpException e) {
                  logger.warn("[{}] - can't set IP config for device {}", this, deviceUID, e);
                  errorStatus = ChannelState.SET_IP_CONFIG_ERROR;
                }

                if (errorStatus != ChannelState.ACTIVE) {
                  for (int i = 0; i < DeviceManagerHelper.getCountOfModules(deviceUID.getDeviceType()); i++) {
                    activityLogger.updateChannelConfig(createChannelUID(deviceUID, (byte) i),
                        new GSMChannelImpl().setGsmGroup(new GsmNoGroup()), new ChannelConfig(entry.getValue().second(), errorStatus));
                  }

                  setIPConfigTimeouts.put(deviceUID, System.currentTimeMillis());
                  continue;
                }

                IGSMDevice gsmDevice;
                try {
                  gsmDevice = DeviceUtil.createEthGSMDevice(new InetSocketAddress(deviceIPConfig.getIP(), MUDP_DEVCIE_PORT));
                } catch (ChannelException | MudpException e) {
                  logger.warn("[{}] - can't create gsm device for [{}]", this, deviceUID.getCanonicalName(), e);
                  continue;
                }

                logger.debug("[{}] - attach to gsm device [{}]", this, deviceUID.getCanonicalName());
                try {
                  gsmDevice.attach();
                } catch (MudpException | InterruptedException e) {
                  logger.warn("[{}] - can't attach to [{}]", this, deviceUID.getCanonicalName(), e);
                  continue;
                }

                activeDevices.put(deviceUID, gsmDevice);
                activeChannels.put(deviceUID, new ArrayList<>());

                int channelNumber = 0;
                ChannelConfig channelConfig = new ChannelConfig(entry.getValue().second(), errorStatus);
                for (IGSMEntry gsmEntry : gsmDevice.getGSMEntries()) {
                  futures.add(channelExecutor.submit(new UnitAddTask(createChannelUID(deviceUID, (byte) channelNumber++), gsmEntry, channelConfig)));
                }
                try {
                  DeviceJournalHelper.readJournal(gsmDevice, deviceUID);
                } catch (ChannelException | InterruptedException e) {
                  logger.warn("[{}] - can't read device journal", this, e);
                }
              } else {
                try {
                  cudpConnection.reset(deviceUID, RESET_DELAY);
                } catch (CudpException e) {
                  logger.warn("[{}] - can't reset device {}", this, deviceUID.getCanonicalName(), e);
                }
              }
            }
          } finally {
            cudpConnectionLock.unlock();
          }

          Set<DeviceUID> lostDevices = new HashSet<>(activeDevices.keySet());
          lostDevices.removeAll(availableDevices.keySet());
          lostAttempts.keySet().retainAll(lostDevices);
          for (DeviceUID deviceUID : lostDevices) {
            if (activeChannels.get(deviceUID).stream().anyMatch(GSMUnit::isOwned)) {
              logger.debug("[{}] - waiting released call channels for device {}", this, deviceUID);
              continue;
            }

            int attempts = lostAttempts.getOrDefault(deviceUID, 0);
            if (attempts <= LOST_DEVICE_ATTEMPT_COUNT) {
              lostAttempts.put(deviceUID, attempts + 1);
              continue;
            }

            logger.debug("[{}] - lost device {}", this, deviceUID.getCanonicalName());
            lostAttempts.remove(deviceUID);
            futures.add(channelExecutor.submit(new UnitRemoveTask(activeDevices.remove(deviceUID), activeChannels.remove(deviceUID))));
          }

          for (Future<?> future : futures) {
            try {
              future.get();
            } catch (ExecutionException e) {
              logger.warn("[{}] - execution exception", this, e);
            }
          }

        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected interruption", this, e);
          break;
        }
      }
    }
  }

  private final class SetIPConfigTask extends TimerTask {

    @Override
    public void run() {
      cudpConnectionLock.lock();
      try {
        for (DeviceUID deviceUID : activeDevices.keySet()) {
          try {
            cudpConnection.setIPConfig(deviceUID, deviceIPConfigs.get(deviceUID));
          } catch (CudpException | InterruptedException ignored) {
            logger.warn("[{}] - can't set ip config for [{}] by timer", this, deviceUID);
          }
        }
      } finally {
        cudpConnectionLock.unlock();
      }
    }

  }

}

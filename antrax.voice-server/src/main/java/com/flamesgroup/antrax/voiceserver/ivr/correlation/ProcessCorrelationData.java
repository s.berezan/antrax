/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

public final class ProcessCorrelationData implements ICorrelationData {

  private final double[] data;
  private int offset;
  private double sum;
  private double sumOfSquares;

  public ProcessCorrelationData(final int length) {
    this.data = new double[length];
  }

  @Override
  public int getLength() {
    return data.length;
  }

  @Override
  public double get(final int index) {
    int realIndex = index + offset;
    if (realIndex >= data.length) {
      realIndex -= data.length;
    }
    return data[realIndex];
  }

  @Override
  public double getSum() {
    return sum;
  }

  @Override
  public double getSumOfSquares() {
    return sumOfSquares;
  }

  public void add(final double value) {
    final double replacedValue = data[offset];
    sum -= replacedValue;
    sumOfSquares -= replacedValue * replacedValue;
    data[offset++] = value;
    sum += value;
    sumOfSquares += value * value;
    if (offset >= data.length) {
      offset = 0;
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.MobileCallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class IncomingCall extends Call {

  private static final Logger logger = LoggerFactory.getLogger(IncomingCall.class);

  protected CallChannelState.State state;

  private final IMobileTerminatingCall terminatedCall;

  private long callStartTime;

  private boolean fasDetected = true;

  IncomingCall(final CallChannel callChannel, final CDR cdr, final IMobileTerminatingCall terminatedCall) {
    super(callChannel, cdr);

    this.terminatedCall = terminatedCall;
  }

  // ITerminatingCallHandler
  @Override
  public void handleDtmf(final char dtmf) {
    logger.debug("[{}] - got DTMF from VoIP: [{}]", callChannel, dtmf);
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        states.offer(State.DTMF);
        stateAdapter = () -> {
          try {
            terminatedCall.sendDTMF(String.valueOf(dtmf));
          } catch (ChannelException | InterruptedException e) {
            logger.warn("[{}] - can't process DTMF {}", callChannel, dtmf, e);
          }
        };
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // Call
  @Override
  protected void processAlerting() {
    callChannel.changeCallState(CallState.State.ALERTING);
    callChannel.getGSMUnit().activateRinging();
    fasDetected = false;
  }

  @Override
  protected void processAnswer() {
    try {
      terminatedCall.answer();
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't answer for incoming call", callChannel, e);
      drop(CallDropReason.SWITCH_CONGESTION, CdrDropReason.ANSWER_ERROR);
      return;
    }

    if (fasDetected) {
      callChannel.getGSMUnit().activateRinging();
    }

    callChannel.changeCallState(CallState.State.ACTIVE);
    cdr.startCall();
    callStartTime = System.currentTimeMillis();
    callChannel.getSIMUnit().handleIncomingCallAnswered();
  }

  @Override
  protected void dropGSM(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    dropGSM();
    releaseCall(callDropReason, cdrDropReason);
  }

  @Override
  protected void dropVoIP(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    CallDropReason callDropReason = CallDropReason.fromCauseCode((byte) report.getCallControlConnectionManagementCause());
    releaseCall(callDropReason, CdrDropReason.GSM);
  }

  @Override
  protected void dropAll(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    dropGSM(callDropReason, cdrDropReason);
  }

  protected void dropGSM() {
    try {
      terminatedCall.drop();
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't drop incoming call", callChannel, e);
    }
  }

  protected void releaseCall(final CallDropReason callDropReason, final CdrDropReason dropCode) {
    logger.debug("[{}] - release incoming call", callChannel);
    callChannel.getGSMUnit().turnOffAudio();
    callChannel.getSIMUnit().handleIncomingCallDropped(callStartTime == 0 ? 0 : System.currentTimeMillis() - callStartTime);

    resetCall(callDropReason, dropCode);
  }

  protected void resetCall(final CallDropReason callDropReason, final CdrDropReason dropCode) {
    cdr.closeCall(dropCode, callDropReason.getCauseCode());
    callChannel.getSIMUnit().handleEvent(SimEvent.callEnded(cdr));

    callReleased.set(true);
    callChannel.handleCallRelease();
    callChannel.getSIMUnit().finishShowingState(state, CallChannelState.State.READY_TO_CALL);

    callChannel.resetIncomingCall();
  }

}

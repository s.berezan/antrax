/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.scripts.CallFilterScript;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.automation.scripts.IncomingCallScript;
import com.flamesgroup.antrax.automation.scripts.SmsFilterScript;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.AntraxProperties;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.rmi.RemoteException;
import java.util.List;

public class Configurator implements IConfigurator {

  private final Logger logger = LoggerFactory.getLogger(Configurator.class);

  private final IRemoteVSConfigurator remoteVSConfigurator;

  public Configurator(final IRemoteVSConfigurator remoteVSConfigurator) {
    this.remoteVSConfigurator = remoteVSConfigurator;
  }

  @Override
  public void updateGsmToSimGroupLinks(final Configuration cfg) {
    try {
      long latestRevision = remoteVSConfigurator.getLatestGSMToSIMGroupLinksRevision();
      if (cfg.getLinksRevision() < latestRevision) {
        logger.debug("[{}] - updating GSMGroup -> SIMGroup links", this);
        cfg.setLinks(getLinks(remoteVSConfigurator.listSIMGSMLinks()), latestRevision);
      }
    } catch (RemoteException e) {
      logger.error("[{}] - can't run remote method", this, e);
    }
  }

  private CfgLinksBuilder getLinks(final LinkWithSIMGroup[] listSIMGSMLinks) {
    CfgLinksBuilder cfgLinksBuilder = new CfgLinksBuilder();
    for (LinkWithSIMGroup link : listSIMGSMLinks) {
      cfgLinksBuilder.addLink(link.getGSMGroup(), link.getSIMGroup());
    }
    return cfgLinksBuilder;
  }

  @Override
  public boolean configure(final SIMUnit simUnit, final AntraxPluginsStore pluginsStore) {
    logger.trace("[{}] - configuring {}", this, simUnit);
    try {
      ICCID simUID = simUnit.getUid();
      BusinessActivityScript[] businessActivityScripts = getSerializedBusinessActivityScripts(remoteVSConfigurator.getBusinessActivityScript(simUID), pluginsStore);
      if (businessActivityScripts != null) {
        simUnit.setBusinessActivityScripts(businessActivityScripts, pluginsStore, false);
      }
      ActionProviderScript[] actionProviderScripts = getSerializedActionProviderScripts(remoteVSConfigurator.getActionProviderScript(simUID), pluginsStore);
      if (actionProviderScripts != null) {
        simUnit.setActionProviderScripts(actionProviderScripts, pluginsStore, false);
      }
      FactorEvaluationScript factorEvaluationScript = getSerializedFactorEvaluationScript(remoteVSConfigurator.getVSFactorScript(simUID), pluginsStore);
      if (factorEvaluationScript != null) {
        simUnit.setFactorScript(factorEvaluationScript, pluginsStore, false);
      }
      IncomingCallScript incomingCallScript = getSerializedCallManagementScript(remoteVSConfigurator.getIncomingCallManagementScript(simUID), pluginsStore);
      if (incomingCallScript != null) {
        simUnit.setIncomingCallScript(incomingCallScript, pluginsStore, false);
      }
      CallFilterScript[] callFilterScript = getSerializedCallFilterScripts(remoteVSConfigurator.getCallFilterScript(simUID), pluginsStore);
      if (callFilterScript != null) {
        simUnit.setCallFilterScripts(callFilterScript, pluginsStore, false);
      }
      SmsFilterScript[] smsFilterScripts = getSerializedSmsFilterScripts(remoteVSConfigurator.getSmsFilterScript(simUID), pluginsStore);
      if (smsFilterScripts != null) {
        simUnit.setSmsFilterScripts(smsFilterScripts, pluginsStore, false);
      }
      return true;
    } catch (RemoteException e) {
      logger.warn("[{}] - failed to configure {}", this, simUnit, e);
      return false;
    }
  }

  private ActionProviderScript[] getSerializedActionProviderScripts(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);
    int scriptsCount = (scripts != null) ? scripts.length : 0;
    ActionProviderScript[] actionProviderScripts = new ActionProviderScript[scriptsCount];
    for (int i = 0; i < scriptsCount; i++) {
      actionProviderScripts[i] = (ActionProviderScript) scripts[i];
    }
    return actionProviderScripts;
  }

  private IncomingCallScript getSerializedCallManagementScript(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);
    return (scripts != null && scripts.length > 0) ? (IncomingCallScript) scripts[0] : null;
  }

  private CallFilterScript[] getSerializedCallFilterScripts(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);
    int scriptsCount = (scripts != null) ? scripts.length : 0;
    CallFilterScript[] callFilterScripts = new CallFilterScript[scriptsCount];
    for (int i = 0; i < scriptsCount; i++) {
      callFilterScripts[i] = (CallFilterScript) scripts[i];
    }
    return callFilterScripts;
  }

  private SmsFilterScript[] getSerializedSmsFilterScripts(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);
    int scriptsCount = (scripts != null) ? scripts.length : 0;
    SmsFilterScript[] smsFilterScripts = new SmsFilterScript[scriptsCount];
    for (int i = 0; i < scriptsCount; i++) {
      smsFilterScripts[i] = (SmsFilterScript) scripts[i];
    }
    return smsFilterScripts;
  }

  private FactorEvaluationScript getSerializedFactorEvaluationScript(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);
    return (scripts != null && scripts.length > 0) ? (FactorEvaluationScript) scripts[0] : null;
  }

  private BusinessActivityScript[] getSerializedBusinessActivityScripts(final byte[] serializedScript, final AntraxPluginsStore pluginsStore) {
    StatefullScript[] scripts = pluginsStore.deserialize(StatefullScript[].class, serializedScript);

    int scriptsCount = (scripts != null) ? scripts.length : 0;
    BusinessActivityScript[] businessActivityScripts = new BusinessActivityScript[scriptsCount];
    for (int i = 0; i < scriptsCount; i++) {
      businessActivityScripts[i] = (BusinessActivityScript) scripts[i];
    }
    return businessActivityScripts;
  }

  @Override
  public void configure(final List<GSMUnit> gsmUnits) {
    gsmUnits.forEach(this::configure);
  }

  @Override
  public void configure(final GSMUnit gsmUnit) {
    try {
      GSMChannel gsmChannel = remoteVSConfigurator.getGSMChannel(gsmUnit.getChannelUID());
      logger.trace("[{}] - configuring {} with {}", this, gsmUnit, gsmChannel);
      gsmUnit.setGsmChannel(gsmChannel);
    } catch (RemoteException e) {
      logger.warn("[{}] - failed to configure gsm units", this, e);
    }
  }

  @Override
  public void updatePluginsStore(final AntraxPluginsStore pluginsStore) {
    try {
      long revision = remoteVSConfigurator.getLatestScriptsRevision();
      if (revision > pluginsStore.getRevision()) {
        ScriptFile scriptFile = remoteVSConfigurator.getLastScriptFile();
        pluginsStore.analyze(new ByteArrayInputStream(scriptFile.getData()), revision);
      }
    } catch (Exception e) {
      logger.warn("[{}] - failed to analyze plugins", this, e);
    }
  }

  @Override
  public void clearResetVsScriptsFlag(final SIMUnit simUnit) {
    try {
      remoteVSConfigurator.updateResetVsScriptsFlag(simUnit.getUid(), false);
    } catch (RemoteException e) {
      logger.warn("[{}] - failed to clear ResetVsScriptsFlag {}", this, simUnit, e);
    }
  }

  @Override
  public boolean shouldResetVsScripts(final SIMUnit simUnit) {
    try {
      return remoteVSConfigurator.shouldResetVsScripts(simUnit.getUid());
    } catch (RemoteException e) {
      logger.warn("[{}] - failed to read ResetVsScriptsFlag {}", this, simUnit, e);
      return false;
    }
  }

  @Override
  public GSMChannel configure(final ChannelUID channelUID) {
    try {
      return remoteVSConfigurator.getGSMChannel(channelUID);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't configure channel: {}", this, channelUID, e);
      return null;
    }
  }

  @Override
  public void initAudioCaptureEnabled(final Configuration cfg) {
    try {
      cfg.updateAudioCaptureEnabled(remoteVSConfigurator.isAudioCaptureEnabled(AntraxProperties.SERVER_NAME));
      logger.info("[{}] - init audio capture: [{}]",this, cfg.isAudioCaptureEnabled());
    } catch (RemoteException e) {
      logger.error("[{}] - can't run remote method", this, e);
    }
  }

}

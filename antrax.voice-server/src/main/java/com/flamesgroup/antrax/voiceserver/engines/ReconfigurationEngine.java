/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.engines;

import com.flamesgroup.antrax.voiceserver.Configuration;
import com.flamesgroup.antrax.voiceserver.IConfigurator;
import com.flamesgroup.antrax.voiceserver.UnitActivator;
import com.flamesgroup.antrax.voiceserver.VSStatus;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ReconfigurationEngine extends Thread {

  private static final Logger logger = LoggerFactory.getLogger(ReconfigurationEngine.class);

  private final long timeout;

  private final VSStatus stat;
  private final IConfigurator configurator;
  private final UnitActivator unitActivator;
  private final Configuration cfg;

  private final IActivityLogger activityLogger;

  public ReconfigurationEngine(final Configuration cfg, final IConfigurator configurator, final VSStatus stat, final UnitActivator unitActivator, final IActivityLogger activityLogger) {
    super("ReconfigurationEngine");
    this.stat = stat;
    this.configurator = configurator;
    this.cfg = cfg;
    this.unitActivator = unitActivator;
    this.activityLogger = activityLogger;
    this.timeout = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreReconfigurationTimeout();
  }

  @Override
  public void run() {
    configurator.initAudioCaptureEnabled(cfg);
    while (!stat.terminating) {
      synchronized (stat) {
        updateGsmToSimGroupLinks();
        updateGSMUnits();
        updatePluginsStore();
        updateSimUnits();

        try {
          if (!stat.terminating) {
            stat.wait(timeout);
          }
        } catch (InterruptedException ignored) {
          Thread.currentThread().interrupt();
          break;
        }
      }
    }
  }

  private void updatePluginsStore() {
    configurator.updatePluginsStore(cfg.getPluginsStore());
  }

  private void updateSimUnits() {
    for (SIMUnit s : unitActivator.getSIMUnits()) {
      boolean resetScriptStates = configurator.shouldResetVsScripts(s);
      s.refreshData(cfg.getPluginsStore(), resetScriptStates);
      if (resetScriptStates) {
        configurator.clearResetVsScriptsFlag(s);
      }

      if (!s.isOwned() && s.isWaitRelease()) {
        unitActivator.releaseSIMUnit(s);
        continue;
      }

      if (!s.isPlugged()) {
        logger.trace("[{}] - released {} due to device was unplugged", this, s);
      } else if (s.isLocked()) {
        logger.trace("[{}] - released {} due to sim card was locked", this, s);
      } else if (s.shouldStopSession() || !simUnitCanBeAcceptedGSMGroup(s)) {
        logger.trace("[{}] - released {} due to session end", this, s);
      } else {
        continue;
      }

      s.setWaitRelease(true);
    }
  }

  private boolean simUnitCanBeAcceptedGSMGroup(final SIMUnit simUnit) {
    for (GSMUnit gsm : unitActivator.getGSMUnits()) {
      if (cfg.applies(gsm.getGsmChannel().getGSMGroup(), simUnit.getSimData().getSimGroup())) {
        return true;
      }
    }
    return false;
  }

  private void updateGSMUnits() {
    List<GSMUnit> units = unitActivator.getGSMUnits();
    configurator.configure(units);
    for (GSMUnit unit : units) {
      activityLogger.logGsmUnitChangedGroup(unit.getChannelUID(), unit.getGsmChannel().getGSMGroup());
    }
  }

  private void updateGsmToSimGroupLinks() {
    configurator.updateGsmToSimGroupLinks(cfg);
  }

}

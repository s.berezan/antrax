/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccessAdapter;
import com.flamesgroup.antrax.configuration.AntraxConfigurationHelper;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.communication.IActivityRemoteLogger;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.voiceserver.ActivityLogger;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.distributor.DeviceManagerHelper;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.voiceserv.utils.delivery.DeliveryConfig;
import com.flamesgroup.antrax.voiceserv.utils.delivery.GuaranteedDeliveryProxyServer;
import com.flamesgroup.antrax.voiceserver.engines.AutomationActionsEngine;
import com.flamesgroup.antrax.voiceserver.engines.HttpServerEngine;
import com.flamesgroup.antrax.voiceserver.engines.HttpServerEngine.HttpServerCommandHandler;
import com.flamesgroup.antrax.voiceserver.engines.ReconfigurationEngine;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplateManager;
import com.flamesgroup.antrax.voiceserver.manager.VoiceServerManager;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.antrax.voiceserver.sim.SCReaderSubChannelsHandler;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.gsmb.IGSMDevice;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.mudp.IMudpConnectionErrorHandler;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class VoiceServerBuilder {

  private final VoiceServerManager voiceServerManager = new VoiceServerManager();

  private GuaranteedDeliveryProxyServer simServerDeliveryServer;
  private GuaranteedDeliveryProxyServer simHistoryDeliveryServer;
  private GuaranteedDeliveryProxyServer activityRemoteLoggerServer;

  private IActivityLogger activityLogger;
  private IActivityRemoteLogger activityRemoteLogger;
  private IRemoteHistoryLogger historyLogger;
  private ISmsManager smsManager;
  private IGsmViewManager gsmViewManager;

  private ISimUnitManager simUnitManager;

  private IConfigurator configurator;
  private Configuration cfg;

  private ScriptStateSaver scriptStateSaver;
  private CallChannelPool pool;

  private UnitActivator unitActivator;
  private VSStatus antraxStatus;
  private IActionExecutionManager actionExecutionManager;
  private RegistryAccess registryAccess;
  private ISimCallHistory simCallHistory;
  private SCReaderSubChannelsHandler scReaderSubChannelsHandler;
  private RawATSubChannels rawATSubChannels;
  private SCEmulatorSubChannels scEmulatorSubChannels;

  private IGsmChannelManager channelManager;
  private IvrTemplateManager ivrTemplateManager;
  private ReconfigurationEngine reconfigurationEngine;
  private AutomationActionsEngine actionsEngine;
  private HttpServerEngine httpServerEngine;
  private HttpServerCommandHandler defaultHttpServerCommandHandler;
  private ControlServerPingThread controlServerPingThread;

  private VoIPPeer voipPeer;

  public VoiceServerManager createVoiceServerManager() throws Exception {
    voiceServerManager.setActivityLogger(getActivityLogger());
    voiceServerManager.setSimServerDeliveryServer(getSimServerDeliveryServer());
    voiceServerManager.setSimHistoryDeliveryServer(getHistoryDeliveryServer());
    voiceServerManager.setActivityRemoteLoggerServer(getActivityRemoteLoggerServer());
    voiceServerManager.setConfigurator(getConfigurator());
    voiceServerManager.setCfg(getCfg());
    voiceServerManager.setScriptStateSaver(getScriptStateSaver());
    voiceServerManager.setCallChannelPool(getCallChannelPool());
    voiceServerManager.setUnitActivator(getUnitActivator());
    voiceServerManager.setChannelManager(getChannelManager());
    voiceServerManager.setReconfigurationEngine(getReconfigurationEngine());
    voiceServerManager.setVoipPeer(getVoIPPeer());
    voiceServerManager.setActionsEngine(getActionsEngine());
    voiceServerManager.setHttpServerEngine(getHttpServerEngine());
    voiceServerManager.setAntraxStatus(getAntraxStatus());
    voiceServerManager.setControlServerPingThread(getControlServerPingThread());
    voiceServerManager.setGsmChannelManagerHandler(getGsmChannelManagerHandler());
    voiceServerManager.setIvrTemplateManager(getIvrTemplateManager());
    return voiceServerManager;
  }

  public ISCReaderSubChannelsHandler getOrCreateSCReaderSubChannelsHandler() {
    if (scReaderSubChannelsHandler == null) {
      scReaderSubChannelsHandler = new SCReaderSubChannelsHandler();
    }
    return scReaderSubChannelsHandler;
  }

  public RawATSubChannels getOrCreateRawATSubChannels() {
    if (rawATSubChannels == null) {
      rawATSubChannels = new RawATSubChannels();
    }
    return rawATSubChannels;
  }

  public SCEmulatorSubChannels getOrCreateSCEmulatorSubChannels() {
    if (scEmulatorSubChannels == null) {
      scEmulatorSubChannels = new SCEmulatorSubChannels();
    }
    return scEmulatorSubChannels;
  }

  private static Set<Class<? extends Throwable>> getIOExceptions() {
    HashSet<Class<? extends Throwable>> set = new HashSet<>();
    set.add(RemoteException.class);
    return set;
  }

  public IActivityLogger getActivityLogger() {
    if (activityLogger == null) {
      activityLogger = new ActivityLoggerAdapter(new ActivityLogger(), getActivityRemoteLogger());
    }
    return activityLogger;
  }

  private GuaranteedDeliveryProxyServer getSimServerDeliveryServer() {
    if (simServerDeliveryServer == null) {
      simServerDeliveryServer = new GuaranteedDeliveryProxyServer("SimServerDelivery", getIOExceptions());
    }
    return simServerDeliveryServer;
  }

  private GuaranteedDeliveryProxyServer getHistoryDeliveryServer() {
    if (simHistoryDeliveryServer == null) {
      simHistoryDeliveryServer = new GuaranteedDeliveryProxyServer("SimHistoryDelivery", getIOExceptions());
    }
    return simHistoryDeliveryServer;
  }

  private GuaranteedDeliveryProxyServer getActivityRemoteLoggerServer() {
    if (activityRemoteLoggerServer == null) {
      activityRemoteLoggerServer = new GuaranteedDeliveryProxyServer("ActivityRemoteLoggerDelivery", getIOExceptions());
    }
    return activityRemoteLoggerServer;
  }

  public ISmsManager getSmsManager() {
    if (null == smsManager) {
      smsManager = AntraxConfigurationHelper.getInstance().getSmsManager();
    }
    return smsManager;
  }

  public IGsmViewManager getGsmViewManager() {
    if (null == gsmViewManager) {
      gsmViewManager = AntraxConfigurationHelper.getInstance().getGsmViewManager();
    }
    return gsmViewManager;
  }

  public IRemoteHistoryLogger getHistoryLogger() {
    if (historyLogger == null) {
      historyLogger = getHistoryDeliveryServer().proxy(AntraxConfigurationHelper.getInstance().getHistoryLogger(), IRemoteHistoryLogger.class,
          getSimHistoryDeliveryConfig());
    }
    return historyLogger;
  }

  public IActivityRemoteLogger getActivityRemoteLogger() {
    if (activityRemoteLogger == null) {
      activityRemoteLogger = getActivityRemoteLoggerServer().proxy(AntraxConfigurationHelper.getInstance().getActivityRemoteLogger(), IActivityRemoteLogger.class, getActivityRemoteLoggerConfig());
    }
    return activityRemoteLogger;
  }

  private DeliveryConfig getSimHistoryDeliveryConfig() {
    return new DeliveryConfig() {

      @Override
      public Object defaultValue(final Method method) {
        return null;
      }

      @Override
      public boolean isAsync(final Method method) {
        return true;
      }

      @Override
      public boolean isExceptionless(final Method method) {
        return true;
      }

      @Override
      public boolean isGuaranteed(final Method method) {
        return true;
      }

    };
  }

  private DeliveryConfig getActivityRemoteLoggerConfig() {
    return new DeliveryConfig() {
      @Override
      public boolean isGuaranteed(final Method method) {
        return true;
      }

      @Override
      public boolean isExceptionless(final Method method) {
        return true;
      }

      @Override
      public boolean isAsync(final Method method) {
        return true;
      }

      @Override
      public Object defaultValue(final Method method) {
        return null;
      }
    };
  }

  public IGsmUnitManger getGsmUnitManger() {
    return AntraxConfigurationHelper.getInstance().getGsmUnitManager();
  }

  public ISimUnitManager getSimUnitManager() {
    if (simUnitManager == null) {
      simUnitManager = getSimServerDeliveryServer().proxy(AntraxConfigurationHelper.getInstance().getSimUnitManager(), ISimUnitManager.class, getSimServerDeliveryConfig());
    }
    return simUnitManager;
  }

  private DeliveryConfig getSimServerDeliveryConfig() {
    return new DeliveryConfig() {

      @Override
      public Object defaultValue(final Method method) {
        return null;
      }

      @Override
      public boolean isAsync(final Method method) {
        return false;
      }

      @Override
      public boolean isExceptionless(final Method method) {
        return false;
      }

      @Override
      public boolean isGuaranteed(final Method method) {
        return method.getName().equals("handleSimUnitCallEnded") || method.getName().equals("handleSimUnitIncomingCallEnded") || method.getName().equals("handleGenericEvent");
      }

    };
  }

  public Configuration getCfg() {
    if (cfg == null) {
      cfg = new Configuration();
    }
    return cfg;
  }

  public IConfigurator getConfigurator() {
    if (configurator == null) {
      configurator = new Configurator(AntraxConfigurationHelper.getInstance().getRemoteVSConfigurator());
    }
    return configurator;
  }

  public ScriptStateSaver getScriptStateSaver() {
    if (scriptStateSaver == null) {
      scriptStateSaver = new ScriptStateSaver(AntraxConfigurationHelper.getInstance().getRemoteVSConfigurator());
    }
    return scriptStateSaver;
  }

  public CallChannelPool getCallChannelPool() {
    if (pool == null) {
      pool = new CallChannelPool(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreResortCallChannelsTimeout());
    }
    return pool;
  }

  public UnitActivator getUnitActivator() throws Exception {
    if (unitActivator == null) {
      unitActivator = new UnitActivator();
      unitActivator.setSimUnitManager(getSimUnitManager()).setHistoryLogger(getHistoryLogger()).setSmsManager(getSmsManager());
      unitActivator.setCallChannelPool(getCallChannelPool()).setAntraxStatus(getAntraxStatus()).setActivityLogger(getActivityLogger());
      unitActivator.setConfigurator(getConfigurator()).setScriptStateSaver(getScriptStateSaver()).setConfiguration(getCfg());
      unitActivator.setActionExecution(getActionExecutionManager()).setRegistryAccess(getRegistryAccess()).setSimCallHistory(getSimCallHistory());
      unitActivator.setSCReaderSubChannelsHandler((SCReaderSubChannelsHandler) getOrCreateSCReaderSubChannelsHandler());
      unitActivator.setRawATSubChannels(getOrCreateRawATSubChannels()).setSCEmulatorSubChannels(getOrCreateSCEmulatorSubChannels());
      unitActivator.setGsmUnitManger(getGsmUnitManger()).setVoipPeer(getVoIPPeer()).setGsmViewManager(getGsmViewManager());
      unitActivator.setIvrTemplateManager(getIvrTemplateManager());
    }
    return unitActivator;
  }

  public VSStatus getAntraxStatus() {
    if (antraxStatus == null) {
      antraxStatus = new VSStatus();
    }
    return antraxStatus;
  }

  public RegistryAccess getRegistryAccess() {
    if (registryAccess == null) {
      registryAccess = new RegistryAccessAdapter(AntraxConfigurationHelper.getInstance().getRemoteRegistryAccess());
    }
    return registryAccess;
  }

  public ISimCallHistory getSimCallHistory() {
    if (simCallHistory == null) {
      simCallHistory = AntraxConfigurationHelper.getInstance().getSimCallHistory();
    }
    return simCallHistory;
  }

  public IGsmChannelManager getChannelManager() throws Exception {
    if (channelManager == null) {
      switch (DeviceUtil.getDeviceClass()) {
        case ETH:
          Server server = new Server(AntraxProperties.SERVER_NAME, ServerType.VOICE_SERVER);
          InetAddress broadcast = DeviceManagerHelper.getBroadcast();
          UUID uuid = UUID.randomUUID();
          Map<DeviceUID, IPConfig> deviceIPConfigs = DeviceManagerHelper.parseDevicesFromProperties(server, uuid);
          channelManager = new EthGsmChannelManager(broadcast, deviceIPConfigs, uuid, getUnitActivator(), getConfigurator(), getActivityLogger());
          break;
        case SPI:
          DeviceUID deviceUID = BOXUtil.readDeviceUID();
          IGSMDevice gsmDevice = DeviceUtil.createSpiGSMDevice(createMudpConnectionErrorHandler());
          channelManager = new SpiGsmChannelManager(deviceUID, gsmDevice, getUnitActivator(), getConfigurator(), getActivityLogger());
          break;
      }
    }
    return channelManager;
  }

  public ReconfigurationEngine getReconfigurationEngine() throws Exception {
    if (reconfigurationEngine == null) {
      reconfigurationEngine = new ReconfigurationEngine(getCfg(), getConfigurator(), getAntraxStatus(), getUnitActivator(), getActivityLogger());
    }
    return reconfigurationEngine;
  }

  public VoIPPeer getVoIPPeer() throws Exception {
    if (voipPeer == null) {
      voipPeer = new VoIPPeer(getCallChannelPool(), getHistoryLogger(), getAntraxStatus());
    }
    return voipPeer;
  }

  public AutomationActionsEngine getActionsEngine() throws Exception {
    if (actionsEngine == null) {
      actionsEngine = new AutomationActionsEngine(getAntraxStatus(), getActionExecutionManager(), getCallChannelPool());
    }
    return actionsEngine;
  }

  private class DefaultHttpServerCommandHandler implements HttpServerCommandHandler {

    private final Logger logger = LoggerFactory.getLogger(DefaultHttpServerCommandHandler.class);
    private final CallChannelPool pool;

    public DefaultHttpServerCommandHandler(final CallChannelPool pool) {
      this.pool = pool;
    }

    private void untake(final CallChannel cc) {
      if (null != cc) {
        cc.untake();
        cc.sendingSms(false);
      }
    }

    @Override
    public UUID sendSMS(final String number, final String text) throws SMSException {
      logger.debug("[{}] - sending sms to {} with text:'{}'", this, number, text);
      CallChannel cc;
      PhoneNumber phoneNumber;
      try {
        phoneNumber = new PhoneNumber(number);
      } catch (IllegalArgumentException e) {
        logger.warn("[{}] - SMS sending error: ", this, e);
        throw new SMSException(e.getMessage());
      }
      
      int parts = text.length() / SMSCodec.getTextEncoding(text).getLength() + 1;

      cc = pool.takeForSms(phoneNumber, parts, text);
      if (null != cc) {
        try {
          UUID smsId = cc.sendSMS(phoneNumber, text);
          logger.debug("[{}] - SMS was successfully sent by {}. SmsId: {}", this, cc, smsId);
          return smsId;
        } catch (Exception e) {
          logger.warn("[{}] - SMS sending error: ", this, e);
          throw new SMSException(e.getMessage(), e.getCause());
        } finally {
          untake(cc);
        }
      } else {
        logger.warn("[{}] - SMS sending error: no free channels left.", this);
        throw new SMSException("No free channels left.");
      }
    }
  }

  private HttpServerCommandHandler getDefaultHttpServerCommandHandler() {
    if (defaultHttpServerCommandHandler == null) {
      defaultHttpServerCommandHandler = new DefaultHttpServerCommandHandler(getCallChannelPool());
    }
    return defaultHttpServerCommandHandler;
  }

  public HttpServerEngine getHttpServerEngine() {
    if (null == httpServerEngine) {
      try {
        httpServerEngine = new HttpServerEngine(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerHttpPort(),
            getDefaultHttpServerCommandHandler());
      } catch (IOException e) {
        throw new RuntimeException("Failed to build HttpServerEngine: " + e.getMessage(), e);
      }
    }
    return httpServerEngine;
  }

  private IActionExecutionManager getActionExecutionManager() {
    if (actionExecutionManager == null) {
      actionExecutionManager = AntraxConfigurationHelper.getInstance().getActionExecutionManager();
    }
    return actionExecutionManager;
  }

  private ControlServerPingThread getControlServerPingThread() {
    if (controlServerPingThread == null) {
      controlServerPingThread = new ControlServerPingThread(AntraxConfigurationHelper.getInstance().getPingServerManager(), getActivityLogger(),
          VoiceServerPropUtils.getInstance().getVoiceServerProperties().getPingControlServerTimeout());
    }
    return controlServerPingThread;
  }

  public IGsmChannelManagerHandler getGsmChannelManagerHandler() {
    return AntraxConfigurationHelper.getInstance().getGsmChannelManagerHandler();
  }

  public IvrTemplateManager getIvrTemplateManager() {
    if (ivrTemplateManager == null) {
      ivrTemplateManager = new IvrTemplateManager();
    }
    return ivrTemplateManager;
  }

  private IMudpConnectionErrorHandler createMudpConnectionErrorHandler() {
    return new IMudpConnectionErrorHandler() {
      @Override
      public void handleMudpStreamException(final MudpException e) {
        throw new IllegalStateException(e);
      }
    };
  }

}

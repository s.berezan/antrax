/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.control.communication.IVoiceServerLongInformation;
import com.flamesgroup.antrax.control.communication.CallsStatistic;
import com.flamesgroup.antrax.control.communication.ChannelsStatistic;
import com.flamesgroup.antrax.control.communication.HardwareStatistic;
import com.flamesgroup.antrax.control.voiceserver.ActivityLogger;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.unit.ICCID;
import org.junit.Test;

public class LongServerRuntimeTest {
  @Test
  public void testInitialState() {
    IVoiceServerLongInformation info = new ActivityLogger().getLongServerInfo();
    assertEquals("Unsupported", info.getVersion());
    assertEquals(0, info.getACD());
    assertEquals(0, info.getASR());
    check(0, 0, 0, 0, 0, 0, 0, 0, info.getChannelsStatistic());
    check(0, 0, 0, info.getHardwareStatistic());
    check(0, 0, 0, info.getCallsStatistic());
  }

  @Test
  public void testSimChannelCounting() {
    IActivityLogger rt = new ActivityLogger();
    rt.logSIMTakenFromSimServer(new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1), new ICCID("1111"));
    check(0, 0, 1, rt.getLongServerInfo().getHardwareStatistic());
    rt.logSIMTakenFromSimServer(new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2), new ICCID("1111"));
    check(0, 0, 2, rt.getLongServerInfo().getHardwareStatistic());
    rt.logSIMReturnedToSimServer(new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2));
    check(0, 0, 1, rt.getLongServerInfo().getHardwareStatistic());
    rt.logSIMReturnedToSimServer(new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1));
    check(0, 0, 0, rt.getLongServerInfo().getHardwareStatistic());
  }

  private void check(final int total, final int succ, final int bad, final CallsStatistic callsStatistic) {
    assertEquals(total, callsStatistic.getTotalPerHistory().getCount());
    assertEquals(succ, callsStatistic.getSuccessfulPerHistory().getCount());
    assertEquals(bad, callsStatistic.getZeroPerHistory().getCount());
  }

  private void check(final int bekkies, final int bekkiChannels, final int cemChannels, final HardwareStatistic stat) {
    assertEquals(bekkies, stat.getGsmCount());
    assertEquals(bekkiChannels, stat.getGsmChannels());
    assertEquals(cemChannels, stat.getSimChannels());
  }

  private void check(final int total, final int bandwidth, final int free, final int talking, final int sleeping, final int business, final int selfCall, final int notConn,
      final ChannelsStatistic stat) {
    assertEquals("total", total, stat.getTotalChannelsCount());
    assertEquals("bandwidth", bandwidth, stat.getBandwidth());
    assertEquals("free", free, stat.getFreeChannels());
    assertEquals("talking", talking, stat.getTalkingChannels());
    assertEquals("sleeping", sleeping, stat.getSleepingChannels());
    assertEquals("business", business, stat.getInBusinessActivity());
    assertEquals("selfCall", selfCall, stat.getWaitingSelfCallsChannels());
    assertEquals("notConn", notConn, stat.getNotConnectedChannels());
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;

public class CyclicBufferTest {

  @Test
  public void testZeroSize() {
    byte[] data = {1};
    byte[] dest = new byte[0];
    new CyclicBuffer(data).copySlice(dest, 0, 0); // should be no exception
  }

  @Test
  public void testOffset() {
    byte[] data = {1};
    byte[] dest = {2, 2, 2};
    byte[] expecting = {2, 1, 2};
    new CyclicBuffer(data).copySlice(dest, 1, 1);
    assertEquals(Arrays.toString(expecting), Arrays.toString(dest));
  }

  @Test
  public void testBufferMoves() {
    byte[] data = {1, 2, 3, 4};
    byte[] dest = new byte[2];
    byte[] expecting1 = {1, 2};
    byte[] expecting2 = {3, 4};
    CyclicBuffer cyclicBuffer = new CyclicBuffer(data);
    cyclicBuffer.copySlice(dest, 0, 2);
    assertEquals(Arrays.toString(expecting1), Arrays.toString(dest));
    cyclicBuffer.copySlice(dest, 0, 2);
    assertEquals(Arrays.toString(expecting2), Arrays.toString(dest));
  }

  @Test
  public void testBufferMovesOverTail() {
    byte[] data = {1, 2, 3};
    byte[] dest = new byte[3];
    byte[] expecting = new byte[] {1, 2, 3};
    CyclicBuffer cyclicBuffer = new CyclicBuffer(data);
    cyclicBuffer.copySlice(dest, 0, 3);
    assertEquals(Arrays.toString(expecting), Arrays.toString(dest));
    cyclicBuffer.copySlice(dest, 0, 3);
    assertEquals(Arrays.toString(expecting), Arrays.toString(dest));
  }

  @Test
  public void testSmallerBuffer() {
    byte[] data = {0, 1, 2};
    byte[] dest = new byte[5];
    byte[] expecting = {0, 1, 2, 0, 1};
    CyclicBuffer cyclicBuffer = new CyclicBuffer(data);
    cyclicBuffer.copySlice(dest, 0, 5);
    assertEquals(Arrays.toString(expecting), Arrays.toString(dest));
  }

  @Test
  public void testSmallerMovedBuffer() {
    byte[] data = {0, 1, 2};
    byte[] dest = new byte[5];
    byte[] expecting1 = {0, 1, 2, 0, 1};
    byte[] expecting2 = {2, 0, 1, 2, 0};
    CyclicBuffer cyclicBuffer = new CyclicBuffer(data);
    cyclicBuffer.copySlice(dest, 0, 5);
    assertEquals(Arrays.toString(expecting1), Arrays.toString(dest));
    cyclicBuffer.copySlice(dest, 0, 5);
    assertEquals(Arrays.toString(expecting2), Arrays.toString(dest));
  }
}

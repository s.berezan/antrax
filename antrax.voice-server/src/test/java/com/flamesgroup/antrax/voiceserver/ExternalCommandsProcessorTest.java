/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.unit.ICCID;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ExternalCommandsProcessorTest {

  @Mocked
  private VoiceServer mock;

  @Test
  public void testSendUSSD() throws InterruptedException {
    new Expectations() {{
      mock.sendUSSD(new ICCID("113341987"), "*101*1233354645123#");
    }};

    new ExternalCommandsProcessor(mock, new VSStatus()).processUSSD("send-USSD 113341987 *101*1233354645123#");
    Thread.sleep(100);
  }

  @Test
  public void testSendUSSDWithWhitespace() throws InterruptedException {
    new Expectations() {{
      mock.sendUSSD(new ICCID("1111"), "lots of spaces");
    }};

    new ExternalCommandsProcessor(mock, new VSStatus()).processUSSD("send-USSD 1111 lots of spaces");
    Thread.sleep(100);
  }

  @Test
  public void testEnable() throws InterruptedException {
    new Expectations() {{
      mock.enable(new ICCID("1111"));
    }};

    new ExternalCommandsProcessor(mock, null).processEnable("enable 1111");
    Thread.sleep(100);
  }

  @Test
  public void testDisable() {
    new Expectations() {{
      mock.disable(new ICCID("1111"));
    }};

    new ExternalCommandsProcessor(mock, null).processDisable("enable 1111");
  }
}

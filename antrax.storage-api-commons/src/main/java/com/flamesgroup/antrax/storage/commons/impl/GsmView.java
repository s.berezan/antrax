/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.commons.CellKey;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class GsmView implements Serializable {

  private static final long serialVersionUID = -3104277182501233702L;

  private Boolean trusted;
  private Boolean servingCell;

  private CellKey cellKey;

  private Date firstAppearance;
  private Date lastAppearance;
  private Integer networkSurveyLastRxLev;
  private Integer cellInfoLastRxLev;
  private Short lastMcc;
  private Short lastMnc;
  private Integer networkSurveyNumberOccurrences;
  private Integer servingNumberOccurrences;
  private Integer neighborsNumberOccurrences;
  private Long serverId;
  private String serverName;
  private String notes;

  //Statistic
  private int successfulCallsCount;
  private int totalCallsCount;
  private long callsDuration;
  private int outgoingSmsCount;
  private int incomingSmsCount;
  private int ussdCount;
  private long amountPdd;

  public Boolean isTrusted() {
    return trusted;
  }

  public GsmView setTrusted(final Boolean trusted) {
    this.trusted = trusted;
    return this;
  }

  public Boolean isServingCell() {
    return servingCell;
  }

  public GsmView setServingCell(final Boolean servingCell) {
    this.servingCell = servingCell;
    return this;
  }

  public CellKey getCellKey() {
    return cellKey;
  }

  public GsmView setCellKey(final CellKey cellKey) {
    this.cellKey = cellKey;
    return this;
  }

  public Date getFirstAppearance() {
    return firstAppearance;
  }

  public GsmView setFirstAppearance(final Date firstAppearance) {
    this.firstAppearance = firstAppearance;
    return this;
  }

  public Date getLastAppearance() {
    return lastAppearance;
  }

  public GsmView setLastAppearance(final Date lastAppearance) {
    this.lastAppearance = lastAppearance;
    return this;
  }

  public Integer getNetworkSurveyLastRxLev() {
    return networkSurveyLastRxLev;
  }

  public GsmView setNetworkSurveyLastRxLev(final Integer networkSurveyLastRxLev) {
    this.networkSurveyLastRxLev = networkSurveyLastRxLev;
    return this;
  }

  public Integer getCellInfoLastRxLev() {
    return cellInfoLastRxLev;
  }

  public GsmView setCellInfoLastRxLev(final Integer cellInfoLastRxLev) {
    this.cellInfoLastRxLev = cellInfoLastRxLev;
    return this;
  }

  public Short getLastMcc() {
    return lastMcc;
  }

  public GsmView setLastMcc(final Short lastMcc) {
    this.lastMcc = lastMcc;
    return this;
  }

  public Short getLastMnc() {
    return lastMnc;
  }

  public GsmView setLastMnc(final Short lastMnc) {
    this.lastMnc = lastMnc;
    return this;
  }

  public Long getServerId() {
    return serverId;
  }

  public GsmView setServerId(final Long serverId) {
    this.serverId = serverId;
    return this;
  }

  public String getServerName() {
    return serverName;
  }

  public GsmView setServerName(final String serverName) {
    this.serverName = serverName;
    return this;
  }

  public String getNotes() {
    return notes;
  }

  public GsmView setNotes(final String notes) {
    this.notes = notes;
    return this;
  }

  public Integer getNetworkSurveyNumberOccurrences() {
    return networkSurveyNumberOccurrences;
  }

  public GsmView setNetworkSurveyNumberOccurrences(final Integer networkSurveyNumberOccurrences) {
    this.networkSurveyNumberOccurrences = networkSurveyNumberOccurrences;
    return this;
  }

  public void incrementNetworkSurveyNumberOccurrences() {
    networkSurveyNumberOccurrences++;
  }

  public Integer getServingNumberOccurrences() {
    return servingNumberOccurrences;
  }

  public GsmView setServingNumberOccurrences(final Integer servingNumberOccurrences) {
    this.servingNumberOccurrences = servingNumberOccurrences;
    return this;
  }

  public void incrementServingNumberOccurrences() {
    servingNumberOccurrences++;
  }

  public Integer getNeighborsNumberOccurrences() {
    return neighborsNumberOccurrences;
  }

  public GsmView setNeighborsNumberOccurrences(final Integer neighborsNumberOccurrences) {
    this.neighborsNumberOccurrences = neighborsNumberOccurrences;
    return this;
  }

  public void incrementNeighborsNumberOccurrences() {
    neighborsNumberOccurrences++;
  }

  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  public GsmView setSuccessfulCallsCount(final int successfulCallsCount) {
    this.successfulCallsCount = successfulCallsCount;
    return this;
  }

  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  public GsmView setTotalCallsCount(final int totalCallsCount) {
    this.totalCallsCount = totalCallsCount;
    return this;
  }

  public long getCallsDuration() {
    return callsDuration;
  }

  public GsmView setCallsDuration(final long callsDuration) {
    this.callsDuration = callsDuration;
    return this;
  }

  public int getOutgoingSmsCount() {
    return outgoingSmsCount;
  }

  public GsmView setOutgoingSmsCount(final int outgoingSmsCount) {
    this.outgoingSmsCount = outgoingSmsCount;
    return this;
  }

  public int getIncomingSmsCount() {
    return incomingSmsCount;
  }

  public GsmView setIncomingSmsCount(final int incomingSmsCount) {
    this.incomingSmsCount = incomingSmsCount;
    return this;
  }

  public int getUssdCount() {
    return ussdCount;
  }

  public GsmView setUssdCount(final int ussdCount) {
    this.ussdCount = ussdCount;
    return this;
  }

  public long getAmountPdd() {
    return amountPdd;
  }

  public GsmView setAmountPdd(final long amountPdd) {
    this.amountPdd = amountPdd;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof GsmView)) {
      return false;
    }
    final GsmView that = (GsmView) object;

    return successfulCallsCount == that.successfulCallsCount
        && totalCallsCount == that.totalCallsCount
        && callsDuration == that.callsDuration
        && outgoingSmsCount == that.outgoingSmsCount
        && incomingSmsCount == that.incomingSmsCount
        && ussdCount == that.ussdCount
        && amountPdd == that.amountPdd
        && Objects.equals(trusted, that.trusted)
        && Objects.equals(servingCell, that.servingCell)
        && Objects.equals(cellKey, that.cellKey)
        && Objects.equals(firstAppearance, that.firstAppearance)
        && Objects.equals(lastAppearance, that.lastAppearance)
        && Objects.equals(networkSurveyLastRxLev, that.networkSurveyLastRxLev)
        && Objects.equals(cellInfoLastRxLev, that.cellInfoLastRxLev)
        && Objects.equals(lastMcc, that.lastMcc)
        && Objects.equals(lastMnc, that.lastMnc)
        && Objects.equals(networkSurveyNumberOccurrences, that.networkSurveyNumberOccurrences)
        && Objects.equals(servingNumberOccurrences, that.servingNumberOccurrences)
        && Objects.equals(neighborsNumberOccurrences, that.neighborsNumberOccurrences)
        && Objects.equals(serverId, that.serverId)
        && Objects.equals(serverName, that.serverName)
        && Objects.equals(notes, that.notes);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(trusted);
    result = prime * result + Objects.hashCode(servingCell);
    result = prime * result + Objects.hashCode(cellKey);
    result = prime * result + Objects.hashCode(firstAppearance);
    result = prime * result + Objects.hashCode(lastAppearance);
    result = prime * result + Objects.hashCode(networkSurveyLastRxLev);
    result = prime * result + Objects.hashCode(cellInfoLastRxLev);
    result = prime * result + Objects.hashCode(lastMcc);
    result = prime * result + Objects.hashCode(lastMnc);
    result = prime * result + Objects.hashCode(networkSurveyNumberOccurrences);
    result = prime * result + Objects.hashCode(servingNumberOccurrences);
    result = prime * result + Objects.hashCode(neighborsNumberOccurrences);
    result = prime * result + Objects.hashCode(serverId);
    result = prime * result + Objects.hashCode(serverName);
    result = prime * result + Objects.hashCode(notes);
    result = prime * result + successfulCallsCount;
    result = prime * result + totalCallsCount;
    result = prime * result + Long.hashCode(callsDuration);
    result = prime * result + outgoingSmsCount;
    result = prime * result + incomingSmsCount;
    result = prime * result + ussdCount;
    result = prime * result + Long.hashCode(amountPdd);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[trusted:" + trusted +
        " servingCell:" + servingCell +
        " cellKey:" + cellKey +
        " firstAppearance:" + firstAppearance +
        " lastAppearance:" + lastAppearance +
        " networkSurveyLastRxLev:" + networkSurveyLastRxLev +
        " cellInfoLastRxLev:" + cellInfoLastRxLev +
        " lastMcc:" + lastMcc +
        " lastMnc:" + lastMnc +
        " networkSurveyNumberOccurrences:" + networkSurveyNumberOccurrences +
        " servingNumberOccurrences:" + servingNumberOccurrences +
        " neighborsNumberOccurrences:" + neighborsNumberOccurrences +
        " serverId:" + serverId +
        " serverName:'" + serverName + "'" +
        " notes:'" + notes + "'" +
        " successfulCallsCount:" + successfulCallsCount +
        " totalCallsCount:" + totalCallsCount +
        " callsDuration:" + callsDuration +
        " outgoingSmsCount:" + outgoingSmsCount +
        " incomingSmsCount:" + incomingSmsCount +
        " ussdCount:" + ussdCount +
        " amountPdd:" + amountPdd +
        ']';
  }

}

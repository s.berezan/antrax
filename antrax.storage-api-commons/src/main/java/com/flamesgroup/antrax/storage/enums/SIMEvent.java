/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.enums;

import java.util.Arrays;

public enum SIMEvent {

  INCOMING_CALL_ENDED,
  OUTGOING_CALL_ENDED,
  USSD,
  USSD_SESSION_REQUEST,
  USSD_SESSION_RESPONSE,
  SMS_SENT,
  SMS_RECIEVED,
  SMS_STATUS,
  BUSINESS_ACTIVITY_STOPED,
  BUSINESS_ACTIVITY_STARTED,
  CALL_CHANNEL_BUILD,
  IMEI_SETUPED,
  GSM_REGISTRATION,
  GSM_REGISTERED,
  GSM_NOT_REGISTERED,
  CALL_CHANNEL_RELEASED,
  SELF_CALL_EXPECTATION,
  GENERIC_EVENT,
  SIM_TAKEN,
  SIM_FOUND,
  SIM_ADDED,
  IMEI_GENERATED,
  SIM_LOST,
  SIM_RETURNED,
  GSM_UNREGISTERED,
  SET_GROUP,
  SIM_LOCKED,
  AUTOMATION_ACTION_EXECUTION_STARTED,
  AUTOMATION_ACTION_EXECUTION_STOPED,
  AUTOMATION_ACTION_EXECUTION_FAILED,
  ACTION_FAILED,
  ACTION_DONE,
  SIM_STATUS_CHANGED,
  SERVING_CELL_CHANGED,
  RESET_IMEI,
  NETWORK_SURVEY,
  RESET_SCRIPTS,
  RESET_STATISTIC,
  HTTP_REQUEST,
  ALLOWED_INTERNET,
  SET_TARIFF_PLAN_END_DATE;

  private final SIMEvent[] parents;
  private boolean hasChildren;

  SIMEvent(final SIMEvent... parents) {
    Arrays.sort(parents);
    this.parents = parents;

    for (SIMEvent e : parents) {
      e.hasChildren = true;
    }
  }

  public boolean isChildrenOf(final SIMEvent event) {
    return Arrays.binarySearch(parents, event) >= 0;
  }

  public boolean hasChildren() {
    return hasChildren;
  }

  public static SIMEvent strToSIMEvent(final String type) {
    return (type == null) ? null : valueOf(type);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPConvertor implements Serializable {

  private static final long serialVersionUID = 1922224507829557373L;

  public static long toLong(final InetAddress inetAddress) {
    long compacted = 0;
    byte[] bytes = inetAddress.getAddress();
    for (int i = 0; i < bytes.length; i++) {
      if (bytes[i] < 0) {
        compacted += (256 + bytes[i]) * Math.pow(256, 3 - i); //4 - i - 1
      } else {
        compacted += bytes[i] * Math.pow(256, 3 - i); //4 - i - 1
      }
    }
    return compacted;
  }

  public static InetAddress toInetAddress(final long ipAddress) throws UnknownHostException {
    InetAddress inetAddress;
    long upperBytes = 0;
    long longByte;
    byte[] bytes = new byte[4];
    for (int i = 3; i >= 0; i--) {
      longByte = (new Double(Math.floor((ipAddress - Math.abs(upperBytes)) >> (8 * i)))).longValue();
      bytes[4 - i - 1] = (byte) longByte;
      upperBytes += longByte << (8 * i);
    }
    inetAddress = InetAddress.getByAddress(bytes);
    return inetAddress;
  }

}

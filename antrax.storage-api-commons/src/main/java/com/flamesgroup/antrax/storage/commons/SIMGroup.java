/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.io.Serializable;

public interface SIMGroup extends Serializable {

  long getID();

  long getRevision();

  void setRevision(long revision);

  String getName();

  String getOperatorSelection();

  Timeout getIdleAfterRegistration();

  Timeout getIdleBeforeUnregistration();

  Timeout getIdleAfterSuccessfullCall();

  Timeout getIdleAfterZeroCall();

  int getSelfDialFactor();

  int getSelfDialChance();

  Timeout getIdleBeforeSelfCall();

  Timeout getIdleAfterSelfCallTimeout();

  ScriptInstance getActivityPeriodScript();

  ScriptInstance[] getBusinessActivityScripts();

  ScriptInstance[] getActionProviderScripts();

  ScriptInstance[] getCallFilterScripts();

  ScriptInstance[] getSmsFilterScripts();

  ScriptInstance getSessionScript();

  ScriptInstance getGWSelectorScript();

  ScriptInstance getFactorScript();

  ScriptInstance getIncomingCallManagementScript();

  ScriptInstance getVSFactorScript();

  ScriptInstance getIMEIGeneratorScript();

  boolean isSyntheticRinging();

  SIMGroup setName(String name);

  SIMGroup setOperatorSelection(String operatorSelection);

  String getDescription();

  boolean canBeAppointed();

  SIMGroup setIdleAfterRegistration(Timeout idleAfterRegistration);

  SIMGroup setCanBeAppointed(boolean appointed);

  SIMGroup setSyntheticRinging(boolean enabled);

  SIMGroup setBusinessActivityScripts(ScriptInstance[] businessActivityScripts);

  SIMGroup setFactorScript(ScriptInstance factorScript);

  SIMGroup setGWSelectorScript(ScriptInstance gwSelectorScript);

  SIMGroup setSessionScript(ScriptInstance sessionScript);

  SIMGroup setActivityPeriodScript(ScriptInstance activityPeriodScript);

  SIMGroup setActionProviderScripts(ScriptInstance[] actionProviderScripts);

  SIMGroup setIncomingCallManagementScript(ScriptInstance incomingCallManagementScript);

  SIMGroup setVSFactorScript(ScriptInstance vsFactorScript);

  ScriptCommons[] listScriptCommons();

  SIMGroup setScriptCommons(ScriptCommons[] scriptCommons);

  SIMGroup setFASDetection(boolean fasDetection);

  boolean isFASDetection();

  SIMGroup setSmsDeliveryReport(boolean smsDeliveryReport);

  boolean isSmsDeliveryReport();

  boolean isSuppressIncomingCallSignal();

  SIMGroup setSuppressIncomingCallSignal(boolean extendedAT);

  SIMGroup setCallFilterScripts(ScriptInstance[] scriptInstances);

  SIMGroup setSmsFilterScripts(ScriptInstance[] scriptInstances);

  SIMGroup setIMEIGeneratorScript(ScriptInstance scriptInstance);

  int getRegistrationPeriod();

  SIMGroup setRegistrationPeriod(int registrationPeriod);

  int getRegistrationCount();

  SIMGroup setRegistrationCount(int registrationCount);

  SIMGroup setIdleAfterSuccessfullCall(Timeout idleAfterSuccessfullCall);

  SIMGroup setIdleAfterZeroCall(Timeout idleAfterZeroCall);

  SIMGroup setIdleBeforeUnregistration(Timeout idleBeforeUnregistration);

  SIMGroup setIdleBeforeSelfCall(Timeout idleBeforeSelfCall);

  SIMGroup setIdleAfterSelfCallTimeout(Timeout idleAfterSelfCallTimeout);

  SIMGroup setSelfDialFactor(int selfDialFactor);

  SIMGroup setSelfDialChance(int selfDialChance);

  SIMGroup setDescription(String description);

}

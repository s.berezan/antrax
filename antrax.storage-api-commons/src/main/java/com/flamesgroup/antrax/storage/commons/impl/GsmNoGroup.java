/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.commons.GSMGroup;

public class GsmNoGroup implements GSMGroup {

  private static final long serialVersionUID = 1262184103198096412L;

  @Override
  public String getName() {
    return "NO GROUP";
  }

  @Override
  public void setName(final String name) {
  }

  @Override
  public long getID() {
    return 0;
  }

  @Override
  public boolean equals(final Object object) {
    return this == object || object instanceof GsmNoGroup;
  }

  @Override
  public int hashCode() {
    return (int) getID();
  }

}

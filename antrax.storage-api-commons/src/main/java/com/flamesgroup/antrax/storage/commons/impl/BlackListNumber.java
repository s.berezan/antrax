/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

import java.util.Date;

public class BlackListNumber extends ListNumber {

  private static final long serialVersionUID = -4332910721771924767L;

  private String statusDescription;
  private Date addTime;

  public BlackListNumber() {
  }

  public BlackListNumber(final String number, final VoipAntiSpamListNumbersStatus status, final int routingRequestCount, final String statusDescription, final Date addTime) {
    super(number, status, routingRequestCount);
    this.statusDescription = statusDescription;
    this.addTime = addTime;
  }

  public String getStatusDescription() {
    return statusDescription;
  }

  public BlackListNumber setStatusDescription(final String statusDescription) {
    this.statusDescription = statusDescription;
    return this;
  }

  public Date getAddTime() {
    return addTime;
  }

  public BlackListNumber setAddTime(final Date addTime) {
    this.addTime = addTime;
    return this;
  }

  @Override
  protected void appendToStringProperty(final StringBuilder sb) {
    super.appendToStringProperty(sb);
    sb.append(" sd:'").append(statusDescription).append('\'');
    sb.append(" at:").append(addTime);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import com.flamesgroup.antrax.storage.commons.impl.Domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ScriptInstanceImpl extends Domain implements ScriptInstance {

  private static final long serialVersionUID = 5051948807582705200L;

  private final ScriptDefinition definition;
  private ArrayList<ScriptParameter> params;

  public ScriptInstanceImpl(final ScriptDefinition def) {
    if (def == null) {
      throw new IllegalArgumentException("Can't create script instance: def can't be null");
    }
    this.definition = def;
    this.params = new ArrayList<>(definition.getParameters().length);
    for (ScriptParameterDefinition p : definition.getParameters()) {
      if (p != null) {
        params.add(p.createParameterInstance(this));
      }
    }
  }

  public void setParameters(final ScriptParameter[] params) {
    this.params = new ArrayList<>(Arrays.asList(params));
  }

  @Override
  public ScriptDefinition getDefinition() {
    return definition;
  }

  @Override
  public ScriptParameter[] getParameters() {
    return params.toArray(new ScriptParameter[params.size()]);
  }

  @Override
  public ScriptParameter getParameter(final String name) {
    for (ScriptParameter p : params) {
      if (p.getDefinition().getName().equals(name)) {
        return p;
      }
    }
    return null;
  }

  @Override
  public ScriptParameter[] getParameters(final String name) {
    List<ScriptParameter> retval = new LinkedList<>();
    for (ScriptParameter p : params) {
      if (p.getDefinition().getName().equals(name)) {
        retval.add(p);
      }
    }
    return retval.toArray(new ScriptParameter[retval.size()]);
  }

  @Override
  public void setParameter(final String propName, final Object value, final ClassLoader classLoader) {
    ScriptParameter parameter = getParameter(propName);
    if (parameter == null) {
      throw new IllegalArgumentException("Cannot find parameter with name " + propName);
    }
    parameter.setValue(value, classLoader);
  }

  @Override
  public ScriptParameter insertParameter(final ScriptParameter previousParam) {
    if (previousParam.getInstance() != this) {
      throw new IllegalArgumentException("this param is not mine: " + previousParam);
    }
    if (!previousParam.getDefinition().isArray()) {
      throw new IllegalArgumentException("this param is not an array: " + previousParam);
    }
    ScriptParameter parameter = previousParam.getDefinition().createParameterInstance(this);
    params.add(indexOfParam(previousParam) + 1, parameter);
    return parameter;
  }

  private int indexOfParam(final ScriptParameter param) {
    int i = 0;
    for (; i < params.size(); ++i) {
      if (params.get(i) == param) {
        return i;
      }
    }
    throw new IllegalArgumentException("Could not found " + param);
  }

  @Override
  public void moveParamDown(final ScriptParameter param) {
    int index = indexOfParam(param);
    params.remove(index);
    params.add(index + 1, param);
  }

  @Override
  public void moveParamUp(final ScriptParameter param) {
    int index = indexOfParam(param);
    params.remove(index);
    params.add(index - 1, param);
  }

  private ScriptParameter addParameter(final ScriptParameterDefinition paramDef) {
    ScriptParameter parameter = paramDef.createParameterInstance(this);
    params.add(parameter);
    return parameter;
  }

  private ScriptParameter addParameter(final int index, final ScriptParameterDefinition paramDef) {
    ScriptParameter parameter = paramDef.createParameterInstance(this);
    params.add(index, parameter);
    return parameter;
  }

  @Override
  public ScriptParameter addParameter(final String propName, final byte[] serializedValue) {
    ScriptParameter parameter = getParameter(propName);
    if (parameter == null) {
      throw new IllegalArgumentException("Illegal property name " + propName);
    }
    if (!parameter.getDefinition().isArray()) {
      throw new IllegalStateException("Property " + propName + " is not array and can't be added");
    }
    if (parameter.hasValue()) {
      parameter = addParameter(parameter.getDefinition());
    }
    parameter.setSerializedValue(serializedValue);

    return parameter;
  }

  @Override
  public ScriptParameter addParameter(final String propName, final Object value, final ClassLoader classLoader) {
    ScriptParameter parameter = getParameter(propName);
    if (parameter == null) {
      throw new IllegalArgumentException("Illegal property name " + propName);
    }
    if (!parameter.getDefinition().isArray()) {
      throw new IllegalStateException("Property " + propName + " is not array and cann't be added");
    }
    if (parameter.hasValue()) {
      parameter = addParameter(parameter.getDefinition());
    }
    parameter.setValue(value, classLoader);

    return parameter;
  }

  @Override
  public String toString() {
    return getDefinition().toString() + ": " + Arrays.toString(getParameters());
  }

  private boolean validateInstance(final ScriptInstance src) {
    boolean valid = true;
    if (getDefinition().getType().equals(src.getDefinition().getType())) {
      for (ScriptParameter parameter : getParameters()) {
        ScriptParameter srcParameter = src.getParameter(parameter.getDefinition().getName());
        if (srcParameter == null || parameter.getDefinition().isArray() != srcParameter.getDefinition().isArray() || !parameter.getDefinition().getType()
            .equals(srcParameter.getDefinition().getType())) {
          valid = false;
          break;
        }
      }
    }

    return valid;
  }

  private void clenaupArrayValues() {
    Map<String, ScriptParameter> arraysMap = new HashMap<>();

    Iterator<ScriptParameter> it = params.iterator();
    while (it.hasNext()) {
      ScriptParameter param = it.next();
      String propName = param.getDefinition().getName();

      if (param.getDefinition().isArray()) {
        if (arraysMap.containsKey(propName)) {
          it.remove();
        } else {
          arraysMap.put(propName, param);
          if (param.hasValue()) {
            param.setValue(null, null);
          }
        }
      }
    }
  }

  @Override
  public void setParameters(final ScriptInstance src) throws IllegalArgumentException {
    if (!validateInstance(src)) {
      throw new IllegalArgumentException("Can't copy parameters from incompatible source script instance: " + src);
    }

    clenaupArrayValues();

    // copy parameters values
    ScriptParameter[] srcParameters = src.getParameters();
    for (int i = 0; i < srcParameters.length; i++) {
      String propName = srcParameters[i].getDefinition().getName();
      ScriptParameter destParameter = getParameter(propName);
      if (srcParameters[i].getDefinition().isArray() && destParameter.hasValue()) {
        destParameter = addParameter(i, destParameter.getDefinition());
      }

      destParameter.setSerializedValue(srcParameters[i].getSerializedValue());
    }
  }

  public boolean isLastArrayParameter(final String name) {
    int count = 0;
    for (ScriptParameter p : params) {
      if (p.getDefinition().getName().equals(name)) {
        count++;
      }
    }
    return (count <= 1);
  }

  @Override
  public void removeParameter(final ScriptParameter param) {
    String paramName = param.getDefinition().getName();
    if (!param.getDefinition().isArray()) {
      throw new IllegalArgumentException("Property " + paramName + " is not array and can't be removed");
    }

    if (!params.contains(param)) {
      throw new IllegalArgumentException("Property " + paramName + " is not contains in script instance and can't be removed");
    }

    if (isLastArrayParameter(paramName)) {
      removeFromParams(param);
      addParameter(param.getDefinition());
    } else {
      removeFromParams(param);
    }
  }

  private void removeFromParams(final ScriptParameter param) {
    Iterator<ScriptParameter> iter = params.iterator();
    while (iter.hasNext()) {
      if (iter.next() == param) {
        iter.remove();
        return;
      }
    }
    throw new IllegalArgumentException("Failed to remove script param");
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import com.flamesgroup.antrax.storage.commons.impl.Domain;

public class ScriptParameterDefinitionImpl extends Domain implements ScriptParameterDefinition {

  private static final long serialVersionUID = -4058024255429768425L;

  private final String name;
  private final String type;
  private final boolean array;
  private String description;
  private ScriptDefinition scriptDefinition;
  private final String method;
  private final byte[] defaultValue;

  public ScriptParameterDefinitionImpl(final String name, final String method, final String description, final String type, final boolean array, final byte[] defaultValue) {
    checkArgument(name, "name");
    checkArgument(type, "type");
    this.name = name;
    this.type = type;
    this.array = array;
    this.method = method;
    this.description = description;
    this.defaultValue = defaultValue;
  }

  @Override
  public String getMethodName() {
    return method;
  }

  private void checkArgument(final Object o, final String name) {
    if (o == null) {
      throw new IllegalArgumentException("Can't create script parameter definition: " + name + " can't be null");
    }
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public boolean isArray() {
    return array;
  }

  @Override
  public String toString() {
    String retval = String.format("%s: %s", name, type);
    if (array) {
      retval += "[]";
    }
    return retval;
  }

  @Override
  public ScriptParameter createParameterInstance(final ScriptInstance scriptInstance) {
    return new ScriptParameterImpl(this, scriptInstance, defaultValue);
  }

  @Override
  public void checkAssign(final Object value, final ClassLoader classLoader) throws IllegalArgumentException {
    if (value == null) {
      return;
    }
    Class<?> valueType = value.getClass();

    Class<?> type;
    try {
      if (value instanceof SharedReference) {
        SharedReference reference = (SharedReference) value;
        valueType = convert(reference.getType(), classLoader);
      }
      type = convert(getType(), classLoader);
      type = convert(type);
      valueType = convert(valueType);
    } catch (ClassNotFoundException e) {
      throw new IllegalArgumentException(e);
    }
    if (type.isAssignableFrom(valueType) || type.isAssignableFrom(getClassOf(value))) {
      return;
    }
    throw new IllegalArgumentException("Bad argument: " + type + " is not assignable with " + valueType);
  }

  private Class<?> convert(final Class<?> type) {
    if (Boolean.class == type) {
      return Boolean.TYPE;
    }
    if (Character.class == type) {
      return Character.TYPE;
    }
    if (Byte.class == type) {
      return Byte.TYPE;
    }
    if (Short.class == type) {
      return Short.TYPE;
    }
    if (Integer.class == type) {
      return Integer.TYPE;
    }
    if (Long.class == type) {
      return Long.TYPE;
    }
    if (Float.class == type) {
      return Float.TYPE;
    }
    if (Double.class == type) {
      return Double.TYPE;
    }
    return type;
  }

  public static Class<?> convert(final String type, final ClassLoader classLoader) throws ClassNotFoundException {
    if ("boolean".equals(type)) {
      return Boolean.TYPE;
    } else if ("char".equals(type)) {
      return Character.TYPE;
    } else if ("byte".equals(type)) {
      return Byte.TYPE;
    } else if ("short".equals(type)) {
      return Short.TYPE;
    } else if ("int".equals(type)) {
      return Integer.TYPE;
    } else if ("long".equals(type)) {
      return Long.TYPE;
    } else if ("float".equals(type)) {
      return Float.TYPE;
    } else if ("double".equals(type)) {
      return Double.TYPE;
    } else {
      return Class.forName(type, true, classLoader);
    }
  }

  private Class<?> getClassOf(final Object value) {
    if (value == null) {
      return Void.TYPE;
    }
    return convert(value.getClass());
  }

  @Override
  public ScriptParameterDefinition setDescription(final String description) {
    this.description = description;
    return this;
  }

  @Override
  public ScriptDefinition getScriptDefinition() {
    return scriptDefinition;
  }

  @Override
  public void setScriptDefinition(final ScriptDefinition scriptDef) {
    this.scriptDefinition = scriptDef;
  }

  public byte[] getDefaultValue() {
    return defaultValue;
  }

}

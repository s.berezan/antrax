/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.registry;

import com.flamesgroup.antrax.storage.commons.impl.Domain;

import java.util.Date;
import java.util.Objects;

public class RegistryEntryImpl extends Domain implements RegistryEntry {

  private static final long serialVersionUID = 3403915347727098334L;

  private String path;
  private String value;
  private Date updatingTime;

  public RegistryEntryImpl setPath(final String path) {
    this.path = path;
    return this;
  }

  public RegistryEntryImpl setValue(final String value) {
    this.value = value;
    return this;
  }

  public RegistryEntryImpl setUpdatingTime(final Date updatingTime) {
    this.updatingTime = updatingTime;
    return this;
  }

  @Override
  public Date getUpdatingTime() {
    return updatingTime;
  }

  @Override
  public String getPath() {
    return path;
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof RegistryEntryImpl)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    RegistryEntryImpl that = (RegistryEntryImpl) object;

    return Objects.equals(path, that.path)
        && Objects.equals(value, that.value)
        && Objects.equals(updatingTime, that.updatingTime);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(path);
    result = prime * result + Objects.hashCode(value);
    result = prime * result + Objects.hashCode(updatingTime);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder(getClass().getSimpleName());
    builder.append('@').append(Integer.toHexString(hashCode())).append(':');
    builder.append("[P:[").append(path).append("]");
    builder.append("V:[").append(value).append("]]");
    return builder.toString();
  }

}

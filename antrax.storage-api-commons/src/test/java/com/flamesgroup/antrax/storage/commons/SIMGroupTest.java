/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import org.junit.Test;

public class SIMGroupTest {

  @Test
  public void testSIMGroupEquals() throws Exception {
    SIMGroup simGroup1 = new SIMGroupImpl(1L);
    SIMGroup simGroup2 = new SIMGroupImpl(2L);
    SIMGroup simGroup3 = new SIMGroupImpl(1L);
    SIMGroup simGroup4 = new SIMGroupImpl(1L);
    assertTrue(simGroup1.equals(simGroup3));
    assertFalse(simGroup1.equals(simGroup2));
    assertTrue(simGroup1.equals(simGroup4));
    assertFalse(simGroup2.equals(simGroup4));
    assertTrue(simGroup3.equals(simGroup4));
  }

  @Test
  public void testSIMGroupHashCode() throws Exception {
    SIMGroup simGroup1 = new SIMGroupImpl(1L);
    SIMGroup simGroup3 = new SIMGroupImpl(1L);
    assertEquals(simGroup1.hashCode(), simGroup3.hashCode());
  }

}

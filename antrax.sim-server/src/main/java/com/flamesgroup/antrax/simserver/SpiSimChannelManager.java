/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.commons.DeviceJournalHelper;
import com.flamesgroup.antrax.commons.DeviceLoggerHelper;
import com.flamesgroup.antrax.control.distributor.ISimChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.simserver.PINRemoveFailedException;
import com.flamesgroup.antrax.control.simserver.SimChannelConfig;
import com.flamesgroup.antrax.control.utils.AliasDeviceUID;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.state.CHVState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IIndicationChannel;
import com.flamesgroup.device.commonb.IServiceChannel;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.device.commonb.LicenseState;
import com.flamesgroup.device.protocol.cudp.ExpireTime;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.sc.APDUException;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.ISCMonitorChannel;
import com.flamesgroup.device.simb.ISCMonitorSubChannel;
import com.flamesgroup.device.simb.ISIMDevice;
import com.flamesgroup.device.simb.ISIMEntry;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderErrorStateException;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.ISIMCardUnit;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sc.SIMCardUnit;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SpiSimChannelManager extends SimChannelManager {

  private static final Logger logger = LoggerFactory.getLogger(SpiSimChannelManager.class);

  private static final long UPDATE_LICENSE_CURRENT_TIME_INTERVAL = 50 * 60 * 1000; // 50 minutes

  private static final int HARDWARE_PROBLEM_ATTEMPT_COUNT = 3;
  private static final long HARDWARE_PROBLEM_ATTEMPT_TIMEOUT = 1000;

  private static final long WAITING_PRESENT_STATE_TIMEOUT = 1000;

  private final EnumSet<SCReaderError> softwareProblems = EnumSet.of(SCReaderError.TIMEOUT, SCReaderError.NOT_SUPPORT, SCReaderError.REJECTED, SCReaderError.SERIAL_PROBLEM);
  private final Map<ChannelUID, SCReaderState> hardwareProblemChannels = new ConcurrentHashMap<>();

  private DeviceUID deviceUID;
  private ISIMDevice simDevice;

  private final Lock activeChannelLock = new ReentrantLock();
  private final Set<ChannelUID> activeChannels = new HashSet<>();

  private final Map<ChannelUID, Integer> hardwareProblemAttempts = new ConcurrentHashMap<>();
  private final Map<ChannelUID, Boolean> presentStates = new ConcurrentHashMap<>();

  private final Map<ChannelUID, ChannelConfig> channelConfigs = new HashMap<>();

  private ISimChannelManagerHandler channelManagerHandler;
  private ThreadPoolExecutor channelExecutor;

  private Timer updateLicenseCurrentTimeTimer;

  public SpiSimChannelManager(final DeviceUID deviceUID, final ISIMDevice simDevice) {
    this.deviceUID = deviceUID;
    this.simDevice = simDevice;
  }

  @Override
  public ATR start(final ChannelUID channel, final ISCReaderSubChannelsHandler scReaderSubChannelsHandler) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    return super.start(channel, new SCReaderSubChannelsProblemHandler(scReaderSubChannelsHandler));
  }

  @Override
  public void stop(final ChannelUID channel) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    super.stop(channel);

    SCReaderState hardwareProblemSCReaderState = hardwareProblemChannels.remove(channel);
    if (hardwareProblemSCReaderState != null) {
      channelExecutor.execute(new ChannelAddTask(channel, simDevice.getSIMEntries().get(channel.getChannelNumber()), channelConfigs.get(channel)));
    }
  }

  @Override
  public void removeCHV(final ChannelUID channel, final CHV chv1) throws RemoteException, PINRemoveFailedException {
    super.removeCHV(channel, chv1);
    channelExecutor.execute(new ChannelAddTask(channel, simDevice.getSIMEntries().get(channel.getChannelNumber()), channelConfigs.get(channel)));
  }

  @Override
  public void setIndicationMode(final ChannelUID channel, final IndicationMode indicationMode) throws RemoteException {
    setIndicationModeLocal(simDevice.getSIMEntries().get(channel.getChannelNumber()).getIndicationChannel(), indicationMode);
  }

  @Override
  public synchronized void startManager(final ISimChannelManagerHandler channelManagerHandler) {
    if (this.channelManagerHandler != null) {
      throw new IllegalStateException(String.format("[%s] - SpiSimChannelManager has already connected", this));
    }

    this.channelManagerHandler = channelManagerHandler;
    channelExecutor = new WaitThreadPoolExecutor(6, 24, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(6), r -> {
      Thread thread = new Thread(r);
      thread.setName("SimChannelExecutor@" + thread.hashCode());
      return thread;
    });

    List<ChannelUID> declaredChannels = new ArrayList<>();
    for (int i = 0; i < simDevice.getSIMEntryCount(); i++) {
      declaredChannels.add(createChannelUID(deviceUID, (byte) i));
    }

    try {
      channelManagerHandler.handleDeclaredChannels(AntraxProperties.SERVER_NAME, declaredChannels);
    } catch (RemoteException e) {
      logger.error("[{}] - can't handle declared channels", this, e);
      return;
    }

    try {
      simDevice.attach();
    } catch (MudpException | InterruptedException e) {
      logger.error("[{}] - can't attach to [{}]", deviceUID.getCanonicalName(), e);
      return;
    }

    for (ICPUEntry cpuEntry : simDevice.getCPUEntries()) {
      channelExecutor.execute(new ChannelReadConfigTask(cpuEntry));
    }

    try {
      DeviceJournalHelper.readJournal(simDevice, deviceUID);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't read device journal", this, e);
    }

    try {
      DeviceLoggerHelper.startLogDevice(simDevice, deviceUID);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't start LogDevice", this, e);
    }

    updateLicenseCurrentTimeTimer = new Timer();
    updateLicenseCurrentTimeTimer.schedule(new UpdateLicenseCurrentTime(), 0, UPDATE_LICENSE_CURRENT_TIME_INTERVAL);
  }

  @Override
  public synchronized void stopManager() {
    if (channelManagerHandler == null) {
      return;
    }

    updateLicenseCurrentTimeTimer.cancel();
    updateLicenseCurrentTimeTimer = null;

    int channelNumber = 0;
    for (ISIMEntry simEntry : simDevice.getSIMEntries()) {
      channelExecutor.execute(new ChannelRemoveTask(createChannelUID(deviceUID, (byte) channelNumber++), simEntry));
    }

    channelExecutor.shutdown();
    try {
      while (!channelExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of DeviceRemoveExecutor", this);
      }
    } catch (InterruptedException e) {
      logger.debug("[{}] - while awaiting completion of executor", this, e);
    }
    try {
      DeviceLoggerHelper.stopLogDevice(simDevice);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't stop LogDevice", this, e);
    }
    try {
      simDevice.detach();
    } catch (MudpException | InterruptedException e) {
      logger.warn("[{}] - can't detach from device {}", this, simDevice, e);
    }

    channelManagerHandler = null;
    simDevice = null;
    deviceUID = null;
  }

  private ChannelUID createChannelUID(final DeviceUID deviceUID, final byte channelNumber) {
    return new ChannelUID(deviceUID, AliasDeviceUID.getAlias(deviceUID), channelNumber);
  }

  private void setIndicationModeLocal(final IIndicationChannel indicationChannel, final IndicationMode indicationMode) {
    logger.info("[{}] - set indication mode [{}] for {}", this, indicationMode, indicationChannel);
    try {
      indicationChannel.setIndicationMode(indicationMode);
    } catch (ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't set indication mode [{}] for {}", this, indicationMode, indicationChannel, e);
    }
  }

  private void handleLostChannel(final ChannelUID channel, final ISIMEntry simEntry) {
    setIndicationModeLocal(simEntry.getIndicationChannel(), IndicationMode.NONE);
    activeChannelLock.lock();
    try {
      if (activeChannels.remove(channel)) {
        removeSimEntry(channel);
        try {
          channelManagerHandler.handleLostChannels(AntraxProperties.SERVER_NAME, Collections.singletonList(channel));
        } catch (RemoteException e) {
          logger.warn("[{}] - can't handle lost channel {}", this, channel, e);
        }
      }
    } finally {
      activeChannelLock.unlock();
    }
  }

  private class ChannelReadConfigTask implements Runnable {

    private final ICPUEntry cpuEntry;

    public ChannelReadConfigTask(final ICPUEntry cpuEntry) {
      this.cpuEntry = cpuEntry;
    }

    @Override
    public void run() {
      IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
      try {
        serviceChannel.enslave();
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't enslave service channel for entry {}", this, cpuEntry, e);
        return;
      }

      ChannelConfig channelConfig;
      try {
        long expireTime = serviceChannel.readLicenseExpireTime();
        LicenseState licenseState = serviceChannel.getLicenseState();
        channelConfig = new ChannelConfig(new ExpireTime(expireTime), convertLicenseState2ChannelState(licenseState));
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't create channel config for entry {}", this, cpuEntry, e);
        return;
      } finally {
        try {
          serviceChannel.free();
        } catch (ChannelException e) {
          logger.warn("[{}] - can't free service channel for entry {}", this, cpuEntry, e);
        }
      }

      int cpuEntryCount = simDevice.getCPUEntryCount();
      int simEntryCount = simDevice.getSIMEntryCount();
      int multiplier = simEntryCount / cpuEntryCount;
      for (int i = 0; i < multiplier; i++) {
        ISIMEntry simEntry = simDevice.getSIMEntries().get(cpuEntry.getNumber() * multiplier + i);
        channelExecutor.execute(new ChannelInitTask(simEntry, channelConfig));
      }
    }

    private ChannelConfig.ChannelState convertLicenseState2ChannelState(final LicenseState licenseState) {
      switch (licenseState) {
        case ACTIVE:
          return ChannelConfig.ChannelState.ACTIVE;
        case LICENSE_STATE_FREEZE:
          return ChannelConfig.ChannelState.LICENSE_INVALID;
        case LICENSE_STATE_EXPIRE:
          return ChannelConfig.ChannelState.LICENSE_EXPIRE;
        case LICENSE_STATE_LOCK:
          return ChannelConfig.ChannelState.LICENSE_LOCK;
        default:
          return null;
      }
    }

  }

  private class ChannelInitTask implements Runnable {

    private final ISIMEntry simEntry;
    private final ChannelConfig channelConfig;

    private final ChannelUID channel;

    public ChannelInitTask(final ISIMEntry simEntry, final ChannelConfig channelConfig) {
      this.simEntry = simEntry;
      this.channelConfig = channelConfig;

      channel = createChannelUID(deviceUID, (byte) simEntry.getNumber());
    }

    @Override
    public void run() {
      ISCMonitorChannel scMonitorChannel = simEntry.getSCMonitorChannel();
      try {
        scMonitorChannel.enslave();
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't enslave scMonitorChannel [{}]", simEntry, e);
        return;
      }

      IIndicationChannel indicationChannel = simEntry.getIndicationChannel();
      try {
        indicationChannel.enslave();
      } catch (ChannelException | InterruptedException e) {
        Exception le = e;
        try {
          scMonitorChannel.free();
        } catch (ChannelException ex) {
          ex.addSuppressed(le);
          le = ex;
        }

        logger.error("[{}] - can't enslave indicationChannel [{}]", simEntry, le);
        return;
      }

      final ISCMonitorSubChannel scMonitorSubChannel = scMonitorChannel.getSCMonitorSubChannel();
      try {
        scMonitorSubChannel.start(presentState -> {
          logger.info("[{}] - handlePresentState {} for {}", this, presentState, channel);
          Boolean previousState = presentStates.put(channel, presentState);
          if (Objects.isNull(previousState)) {
            channelExecutor.execute(new ProcessPresentStateTask(channel, simEntry, channelConfig));
          }
        });
      } catch (ChannelException | InterruptedException e) {
        logger.error("[{}] - can't start scMonitorSubChannel [{}]", simEntry, e);
      }

      channelConfigs.put(channel, channelConfig);
    }

    @Override
    public String toString() {
      return "ChannelInitTask[" + channel + "]";
    }

  }

  private final class ChannelAddTask implements Runnable {

    private final ChannelUID channel;
    private final ISIMEntry simEntry;
    private final ChannelConfig channelConfig;

    private ChannelAddTask(final ChannelUID channel, final ISIMEntry simEntry, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.simEntry = simEntry;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      int localHardwareProblemAttempts = hardwareProblemAttempts.getOrDefault(channel, 0);
      if (localHardwareProblemAttempts > 0) { // another thread will retry to read sim data
        return;
      }

      long hardwareProblemAttemptTimeout = 0;
      ISIMCardUnit simCardUnit = new SIMCardUnit(simEntry.getSCReaderChannel());
      try {
        while (true) {
          if (hardwareProblemAttemptTimeout > 0) {
            Thread.sleep(hardwareProblemAttemptTimeout);
          }

          SimChannelConfig simChannelConfig;
          try {
            simCardUnit.turnOn();
            try {
              ICCID iccid = simCardUnit.readICCID();
              if (simCardUnit.isCHV1Enabled()) {
                simChannelConfig = new SimChannelConfig(iccid, CHVState.REQUIRES_CHV, channelConfig);
              } else {
                IMSI imsi = simCardUnit.readIMSI();
                PhoneNumber phoneNumber;
                try {
                  phoneNumber = simCardUnit.readPhoneNumber();
                } catch (APDUException e) {
                  phoneNumber = null;
                  logger.warn("[{}] - can't read phone number", this, e);
                }

                simChannelConfig = new SimChannelConfig(iccid, CHVState.NO_CHV, channelConfig, imsi, phoneNumber);
              }
            } finally {
              simCardUnit.turnOff();
            }
          } catch (SCReaderErrorStateException e) {
            if (softwareProblems.contains(e.getError())) {
              setIndicationModeLocal(simEntry.getIndicationChannel(), IndicationMode.SOFTWARE_PROBLEM);
              throw e;
            }

            if (localHardwareProblemAttempts++ < HARDWARE_PROBLEM_ATTEMPT_COUNT) {
              logger.warn("[{}] - get {} on attempt [{} of {}]", this, e, localHardwareProblemAttempts, HARDWARE_PROBLEM_ATTEMPT_COUNT);
              hardwareProblemAttemptTimeout = HARDWARE_PROBLEM_ATTEMPT_TIMEOUT;
              hardwareProblemAttempts.put(channel, localHardwareProblemAttempts);
              continue;
            } else {
              setIndicationModeLocal(simEntry.getIndicationChannel(), IndicationMode.HARDWARE_PROBLEM);
              throw e;
            }
          }

          setIndicationModeLocal(simEntry.getIndicationChannel(), IndicationMode.READY);
          activeChannelLock.lock();
          try {
            try {
              channelManagerHandler.handleAvailableChannel(AntraxProperties.SERVER_NAME, channel, simChannelConfig);
            } catch (RemoteException e) {
              logger.warn("[{}] - can't handle available channel {}", this, channel, e);
              return;
            }

            addSimEntry(channel, simEntry);
            activeChannels.add(channel);
          } finally {
            activeChannelLock.unlock();
          }
          break;
        }
      } catch (ChannelException | APDUException e) {
        logger.warn("[{}] - has a problem with sim card {}", this, channel, e);
        activeChannelLock.lock();
        try {
          channelManagerHandler.handleEmptyChannel(AntraxProperties.SERVER_NAME, channel, channelConfig);
        } catch (RemoteException ex) {
          logger.warn("[{}] - can't handle empty channel {}", this, channel, ex);
        } finally {
          activeChannelLock.unlock();
        }
      } catch (InterruptedException e) {
        logger.error("[{}] - was interrupted", this, e);
      }

      hardwareProblemAttempts.remove(channel);
    }

    @Override
    public String toString() {
      return "ChannelAddTask[" + channel + "]";
    }
  }

  private final class ChannelLostTask implements Runnable {

    private final ChannelUID channel;
    private final ISIMEntry simEntry;
    private final ChannelConfig channelConfig;

    private ChannelLostTask(final ChannelUID channel, final ISIMEntry simEntry, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.simEntry = simEntry;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      activeChannelLock.lock();
      try {
        handleLostChannel(channel, simEntry);
        try {
          channelManagerHandler.handleEmptyChannel(AntraxProperties.SERVER_NAME, channel, channelConfig);
        } catch (RemoteException e) {
          logger.warn("[{}] - can't handle empty channel {}", this, channel, e);
        }
      } finally {
        activeChannelLock.unlock();
      }
    }

    @Override
    public String toString() {
      return "ChannelLostTask[" + channel + "]";
    }

  }

  private final class ProcessPresentStateTask implements Runnable {

    private final ChannelUID channel;
    private final ISIMEntry simEntry;
    private final ChannelConfig channelConfig;

    private ProcessPresentStateTask(final ChannelUID channel, final ISIMEntry simEntry, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.simEntry = simEntry;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      try {
        Thread.sleep(WAITING_PRESENT_STATE_TIMEOUT); // give some time to prevent contact bounce
      } catch (InterruptedException e) {
        logger.error("[{}] - was interrupted", this, e);
        return;
      }

      boolean presentState = presentStates.remove(channel);
      if (presentState) {
        channelExecutor.execute(new ChannelAddTask(channel, simEntry, channelConfig));
      } else {
        channelExecutor.execute(new ChannelLostTask(channel, simEntry, channelConfig));
      }
    }

    @Override
    public String toString() {
      return "ProcessPresentStateTask[" + channel + "]";
    }
  }

  private class ChannelRemoveTask implements Runnable {

    private final ChannelUID channel;
    private final ISIMEntry simEntry;

    public ChannelRemoveTask(final ChannelUID channel, final ISIMEntry simEntry) {
      this.channel = channel;
      this.simEntry = simEntry;
    }

    @Override
    public void run() {
      handleLostChannel(channel, simEntry);

      Exception le = null;
      ISCMonitorChannel scMonitorChannel = simEntry.getSCMonitorChannel();
      try {
        scMonitorChannel.getSCMonitorSubChannel().stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      IIndicationChannel indicationChannel = simEntry.getIndicationChannel();
      try {
        indicationChannel.free();
      } catch (ChannelException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }
      try {
        scMonitorChannel.free();
      } catch (ChannelException e) {
        if (le != null) {
          e.addSuppressed(le);
        }
        le = e;
      }

      if (le != null) {
        logger.warn("[{}] - can't correctly remove channel [{}]", channel, le);
      }
    }

    @Override
    public String toString() {
      return "ChannelRemoveTask[" + channel + "]";
    }

  }

  private class SCReaderSubChannelsProblemHandler implements ISCReaderSubChannelsHandler {

    private final ISCReaderSubChannelsHandler scReaderSubChannelsHandler;

    private SCReaderSubChannelsProblemHandler(final ISCReaderSubChannelsHandler scReaderSubChannelsHandler) {
      this.scReaderSubChannelsHandler = scReaderSubChannelsHandler;
    }

    @Override
    public void handleAPDUResponse(final ChannelUID channel, final APDUResponse response) throws RemoteException, IllegalChannelException {
      scReaderSubChannelsHandler.handleAPDUResponse(channel, response);
    }

    @Override
    public void handleErrorState(final ChannelUID channel, final SCReaderError error, final SCReaderState state) throws RemoteException, IllegalChannelException {
      if (error == SCReaderError.HARDWARE_PROBLEM) {
        hardwareProblemChannels.put(channel, state);
      } else if (softwareProblems.contains(error)) {
        setIndicationMode(channel, IndicationMode.SOFTWARE_PROBLEM);
      }

      scReaderSubChannelsHandler.handleErrorState(channel, error, state);
    }

  }

  private final class UpdateLicenseCurrentTime extends TimerTask {

    @Override
    public void run() {
      long currentTime = System.currentTimeMillis();
      try {
        for (ICPUEntry cpuEntry : simDevice.getCPUEntries()) {
          IServiceChannel serviceChannel = cpuEntry.getServiceChannel();
          try {
            serviceChannel.enslave();
          } catch (ChannelException e) {
            logger.warn("[{}] - can't enslave service channel to update license current time for {}", this, cpuEntry, e);
            continue;
          }

          try {
            serviceChannel.updateLicenseCurrentTime(currentTime);
          } catch (ChannelException e) {
            logger.warn("[{}] - can't update license current time for {}", this, cpuEntry, e);
          } finally {
            try {
              serviceChannel.free();
            } catch (ChannelException e) {
              logger.warn("[{}] - can't free service channel after update license current time for {}", this, cpuEntry, e);
            }
          }
        }
      } catch (InterruptedException e) {
        logger.warn("[{}] - while update license current time was interrupted", this, e);
      }
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.configuration.AntraxConfigurationHelper;
import com.flamesgroup.antrax.configuration.ControlServerTimeDifferenceChecker;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.control.communication.ISimChannelManager;
import com.flamesgroup.antrax.simserver.properties.SimServerPropUtils;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.daemonext.DaemonExt;
import com.flamesgroup.daemonext.IDaemonExtStatus;
import com.flamesgroup.properties.ReloadCommand;
import com.flamesgroup.rmi.RemoteExporter;
import com.flamesgroup.rmi.RemoteServiceExporter;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.utils.BuildInfo;
import com.flamesgroup.utils.BuildInfoUtils;
import com.flamesgroup.utils.UncaughtExceptionHandlerWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

public class SimServerLauncher extends DaemonExt {

  private final static Logger logger = LoggerFactory.getLogger(SimServerLauncher.class);

  private SimServerStatus simServerStatus;
  private ReloadCommand reloadCommand;
  private SimServer simServer;

  public static void main(final String[] args) throws Exception {
    SimServerLauncher simServerLauncher = new SimServerLauncher();
    simServerLauncher.main(simServerLauncher.getClass().getSimpleName(), args);
  }

  @Override
  public List<IDaemonExtStatus> getStatuses() {
    return Arrays.asList(simServerStatus, reloadCommand);
  }

  @Override
  public void start() throws Exception {
    BuildInfo buildInfo = BuildInfoUtils.readBuildInfo(this.getClass());
    logDuplicateInfo("Sim Server start: " + (buildInfo == null ? "buildInfo didn't found at JAR" : buildInfo.toString()));
    try {
      start0();
      super.start();
    } catch (Exception e) {
      logDuplicateError("Sim Server start failed", e);
      System.exit(EXIT_CODE_TO_BE_RESTARTED);
    }
  }

  @Override
  public void stop() throws Exception {
    logDuplicateInfo("Sim Server stop");
    super.stop();
    simServer.shutdown();
  }

  private void start0() throws Exception {
    ControlServerTimeDifferenceChecker.isAllowable(System.currentTimeMillis());

    reloadCommand = new ReloadCommand();
    reloadCommand.addReloadListener(SimServerPropUtils.getInstance());
    SimServerBuilder builder = new SimServerBuilder();
    simServer = builder.createSimServer();
    ISimChannelManager simCardManager = builder.getSimChannelManager();
    simServerStatus = new SimServerStatus(simServer);

    IServerData serverData = AntraxConfigurationHelper.getInstance().getMyServer();
    System.setProperty("java.rmi.server.hostname", serverData.getPublicAddress().getHostAddress());
    int port = SimServerPropUtils.getInstance().getSimServerProperties().getRmiRegistryPort();

    RemoteServiceExporter.export(new RemoteExporter.Builder(simServer, ISimServer.class).setRegistryPort(port).build());
    RemoteServiceExporter.export(new RemoteExporter.Builder(simCardManager, ISimChannelManager.class).setRegistryPort(port).build());

    simServer.startActivity();

    Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerWrapper((t, e) -> {
      logger.error("Sim Server - Uncaught Exception in thread [{}]", t, e);

      logDuplicateInfo("Sim Server stop on Uncaught Exception");
      simServer.shutdown();

      if (!(e instanceof ControlServerPingException)) {
        System.exit(EXIT_CODE_TO_BE_RESTARTED);
      }

      long controlServerPingTimeout = SimServerPropUtils.getInstance().getSimServerProperties().getPingControlServerTimeout();
      while (true) {
        try {
          Thread.sleep(controlServerPingTimeout);
        } catch (InterruptedException ignored) {
          throw new AssertionError(e); // actually never can happen, because no one have reference to this thread
        }

        try {
          logDuplicateInfo("Sim Server internal start");
          start0();
        } catch (RemoteAccessException ignored) {
          logDuplicateInfo("Control server is not responding, try more time late");
          continue;
        } catch (Exception ex) {
          logDuplicateError("Sim Server internal start failed", ex);
          System.exit(EXIT_CODE_TO_BE_RESTARTED);
        }
        break;
      }
    }));
  }

  private void logDuplicateInfo(final String message) {
    logger.info(message);
    getOut().println(OffsetDateTime.now() + " - " + message);
  }

  private void logDuplicateError(final String message, final Exception e) {
    logger.error(message, e);
    getOut().println(OffsetDateTime.now() + " - " + message + ": " + e.getMessage());
  }

}

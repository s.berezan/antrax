/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import static com.flamesgroup.rmi.RemoteServiceExporter.destroyAll;

import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SimServerShortInfo;
import com.flamesgroup.antrax.control.simserver.ISimServerManager;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

public class SimServer implements ISimServer {

  private static final Logger logger = LoggerFactory.getLogger(SimServer.class);

  private final long startTime = System.currentTimeMillis();

  private SimChannelManager simChannelManager;
  private ISimServerManager simServerManager;

  private ControlServerPingThread controlServerPingThread;

  private final AtomicReference<ServerStatus> simServerStatus = new AtomicReference<>();

  public void setSimChannelManager(final SimChannelManager simChannelManager) {
    this.simChannelManager = simChannelManager;
  }

  public void setSimServerManager(final ISimServerManager simServerManager) {
    this.simServerManager = simServerManager;
  }

  public void setControlServerPingThread(final ControlServerPingThread controlServerPingThread) {
    this.controlServerPingThread = controlServerPingThread;
  }

  public ServerStatus getServerStatus() {
    return simServerStatus.get();
  }

  public synchronized void startActivity() throws Exception {
    simServerStatus.set(ServerStatus.STARTING);
    logger.debug("Starting SimServerManager");
    simServerManager.startSimServerSession(AntraxProperties.SERVER_NAME, simChannelManager);
    logger.debug("Starting PlugAndPlay");
    simChannelManager.start(simServerManager);
    logger.debug("Starting ControlServerPingThread");
    controlServerPingThread.start();
    simServerStatus.set(ServerStatus.STARTED);
  }

  @Override
  public synchronized void shutdown() {
    simServerStatus.set(ServerStatus.STOPPING);
    logger.debug("Stopping ControlServerPingThread");
    controlServerPingThread.terminate();
    logger.debug("Stopping PlugAndPlay");
    simChannelManager.stop();
    try {
      logger.debug("Waiting until ControlServerPingThread finishes");
      controlServerPingThread.join();
    } catch (InterruptedException e) {
      logger.warn("Interrupted while waiting for engines to stop an activity", e);
    }
    logger.debug("Stopping SimServerManager");
    try {
      simServerManager.endSimServerSession(AntraxProperties.SERVER_NAME);
    } catch (RemoteException e) {
      e.printStackTrace();
    }

    logger.debug("All done");

    destroyAll();
  }

  @Override
  public long getUptime() {
    return System.currentTimeMillis() - startTime;
  }

}

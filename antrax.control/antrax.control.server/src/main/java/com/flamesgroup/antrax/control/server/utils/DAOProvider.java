/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.utils;

import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.storage.connection.DbConnectionPool;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.DAOFactory;
import com.flamesgroup.antrax.storage.dao.IAlarisSmsDAO;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.antrax.storage.dao.ICellMonitorDAO;
import com.flamesgroup.antrax.storage.dao.IConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.IGsmViewDAO;
import com.flamesgroup.antrax.storage.dao.INetworkSurveyDAO;
import com.flamesgroup.antrax.storage.dao.IPhoneNumberPrefixDAO;
import com.flamesgroup.antrax.storage.dao.IReceivedSmsDAO;
import com.flamesgroup.antrax.storage.dao.ISIMHistoryDAO;
import com.flamesgroup.antrax.storage.dao.ISentSmsDAO;
import com.flamesgroup.antrax.storage.dao.ISessionDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;

public class DAOProvider {

  {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        destroyDAO();
      }
    });
  }

  private final IDbConnectionPool dbConnectionPool;

  private final IConfigEditDAO configEditDAO;
  private final IConfigViewDAO configViewDAO;
  private final ICallDAO callDAO;
  private final ISIMHistoryDAO simHistoryDAO;
  private final IReceivedSmsDAO receivedSmsDAO;
  private final ISentSmsDAO sentSmsDAO;
  private final IAlarisSmsDAO alarisSmsDAO;
  private final IVoipAntiSpamDAO voipAntiSpamDAO;
  private final INetworkSurveyDAO networkSurveyDAO;
  private final IGsmViewDAO gsmViewDAO;
  private final IPhoneNumberPrefixDAO phoneNumberPrefixDAO;
  private final ICellMonitorDAO cellMonitorDAO;

  private final ISimpleConfigViewDAO simpleCfgViewDAO;
  private final ISimpleConfigEditDAO simpleCfgEditDAO;

  private final RemoteRegistryAccess remoteRegistryAccess;
  private final ISessionDAO generalSessionDAO;

  public DAOProvider(final DBConnectionProps dbConnectionProps) {

    dbConnectionPool = new DbConnectionPool(dbConnectionProps);

    configViewDAO = DAOFactory.createConfigViewDAO(dbConnectionPool);
    configEditDAO = DAOFactory.createConfigEditDAO(dbConnectionPool);
    generalSessionDAO = DAOFactory.createSessionDAO(dbConnectionPool);
    remoteRegistryAccess = DAOFactory.createRemoteRegistryAccess(dbConnectionPool);
    simpleCfgViewDAO = DAOFactory.createSimpleConfigViewDAO(dbConnectionPool);
    simpleCfgEditDAO = DAOFactory.createSimpleConfigEditDAO(dbConnectionPool);
    callDAO = DAOFactory.createCallDAO(dbConnectionPool);
    simHistoryDAO = DAOFactory.createSIMHistoryDAO(dbConnectionPool);
    receivedSmsDAO = DAOFactory.receivedSmsDAO(dbConnectionPool);
    sentSmsDAO = DAOFactory.sentSmsDAO(dbConnectionPool);
    voipAntiSpamDAO = DAOFactory.createVoipAntiSpamDAO(dbConnectionPool);
    networkSurveyDAO = DAOFactory.createNetworkSurveyDAO(dbConnectionPool);
    gsmViewDAO = DAOFactory.createGsmViewDAO(dbConnectionPool);
    alarisSmsDAO = DAOFactory.createAlarisSmsDAO(dbConnectionPool);
    phoneNumberPrefixDAO = DAOFactory.createPhoneNumberPrefixDAO(dbConnectionPool);
    cellMonitorDAO = DAOFactory.createCellMonitorDAO(dbConnectionPool);
  }

  public ICallDAO getCallDAO() {
    return callDAO;
  }

  public ISIMHistoryDAO getSIMHistoryDAO() {
    return simHistoryDAO;
  }

  public IReceivedSmsDAO getReceivedSmsDAO() {
    return receivedSmsDAO;
  }

  public ISentSmsDAO getSentSmsDAO() {
    return sentSmsDAO;
  }

  public IVoipAntiSpamDAO getVoipAntiSpamDAO() {
    return voipAntiSpamDAO;
  }

  public INetworkSurveyDAO getNetworkSurveyDAO() {
    return networkSurveyDAO;
  }

  public IGsmViewDAO getGsmViewDAO() {
    return gsmViewDAO;
  }

  public IAlarisSmsDAO getAlarisSmsDAO() {
    return alarisSmsDAO;
  }

  public IPhoneNumberPrefixDAO getPhoneNumberPrefixDAO() {
    return phoneNumberPrefixDAO;
  }

  public ICellMonitorDAO getCellMonitorDAO() {
    return cellMonitorDAO;
  }

  public ISimpleConfigViewDAO getSimpleConfigViewDAO() {
    return simpleCfgViewDAO;
  }

  public ISimpleConfigEditDAO getSimpleConfigEditDAO() {
    return simpleCfgEditDAO;
  }

  public IConfigViewDAO getConfigViewDAO() {
    return configViewDAO;
  }

  public IConfigEditDAO getConfigEditDAO() {
    return configEditDAO;
  }

  public void destroyDAO() {
    if (dbConnectionPool != null) {
      dbConnectionPool.release();
    }
  }

  public RemoteRegistryAccess getRegistryDAO() {
    return remoteRegistryAccess;
  }

  public ISessionDAO getSessionDAO() {
    return generalSessionDAO;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.control.commons.ExpireMap;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.simserver.IllegalSessionUuidException;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.device.channel.ChannelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class SimUnitSessionPool {

  private static final Logger logger = LoggerFactory.getLogger(SimUnitSessionPool.class);

  private final ExpireMap<UUID, SIMUnitSession> sessions;
  private final long sessionExpireTimeout;

  private final List<String> voiceServers = new CopyOnWriteArrayList<>();

  public SimUnitSessionPool() {
    this.sessions = new ExpireMap<>(createSessionExpireHandler());
    this.sessionExpireTimeout = ControlPropUtils.getInstance().getControlServerProperties().getSessionExpireTimeout();
  }

  public void startVoiceServerSession(final String serverName) {
    if (!voiceServers.contains(serverName)) {
      voiceServers.add(serverName);
      return;
    }

    // session expire, need to end session and return simUnit
    for (Map.Entry<UUID, SIMUnitSession> entry : new HashMap<>(sessions).entrySet()) {
      if (!entry.getValue().getGateway().getName().equals(serverName)) {
        continue;
      }
      endSessionAndReturnSimUnit(entry.getKey());
    }
  }

  public void endVoiceServerSession(final String serverName) {
    voiceServers.remove(serverName);
  }

  public UUID startSimUnitSession(final SimUnit simUnit, final IServerData gateway) {
    if (sessions.entrySet().stream().anyMatch(s -> s.getValue().getSimUnit().equals(simUnit))) {
      return null;
    }

    UUID uuid = UUID.randomUUID();
    sessions.putExpireTimeout(uuid, sessionExpireTimeout);
    sessions.put(uuid, new SIMUnitSession(simUnit, gateway));
    return uuid;
  }

  public void endSimUnitSession(final UUID uuid) {
    sessions.remove(uuid);
    sessions.removeExpireTimeout(uuid);
  }

  public SimUnit getSimUnit(final UUID uuid) throws IllegalSessionUuidException {
    if (!sessions.containsKey(uuid)) {
      throw new IllegalSessionUuidException(uuid);
    }

    sessions.updateExpireKey(uuid);
    return sessions.get(uuid).getSimUnit();
  }

  public IServerData getGateway(final UUID uuid) throws IllegalSessionUuidException {
    if (!sessions.containsKey(uuid)) {
      throw new IllegalSessionUuidException(uuid);
    }

    //TODO is timeout touch needed?
    sessions.updateExpireKey(uuid);
    return sessions.get(uuid).getGateway();
  }

  public void endSessionAndReturnSimUnit(final UUID uuid) {
    if (!sessions.containsKey(uuid)) {
      return;
    }

    SimUnit simUnit = sessions.get(uuid).getSimUnit();
    ISCReaderSubChannels scReaderSubChannels = simUnit.getSimChannelManager();
    try {
      scReaderSubChannels.stop(simUnit.getChannelUID());
    } catch (RemoteException | IllegalChannelException | ChannelException | InterruptedException e) {
      logger.warn("[{}] - can't stop SCReaderChannels when handle session expire for [{}]", this, simUnit, e);
    }

    simUnit.untake();
    endSimUnitSession(uuid);
  }

  public int cleanup() {
    return sessions.clearExpireKeys();
  }

  private ExpireMap.IExpireHandler<UUID> createSessionExpireHandler() {
    return new ExpireMap.IExpireHandler<UUID>() {

      private static final long serialVersionUID = 8759680845415798100L;

      @Override
      public void handleExpire(final UUID uuid) {
        endSessionAndReturnSimUnit(uuid);
      }
    };
  }

  private class SIMUnitSession {

    private final SimUnit simUnit;
    private final IServerData gateway;

    public SIMUnitSession(final SimUnit simUnit, final IServerData gateway) {
      this.simUnit = simUnit;
      this.gateway = gateway;
    }

    public SimUnit getSimUnit() {
      return simUnit;
    }

    public IServerData getGateway() {
      return gateway;
    }

  }

}

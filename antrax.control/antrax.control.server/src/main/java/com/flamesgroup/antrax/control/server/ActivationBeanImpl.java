/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ActivationBean;
import com.flamesgroup.antrax.control.communication.ConnectionException;
import com.flamesgroup.antrax.control.communication.NoSuchSimException;
import com.flamesgroup.antrax.control.communication.PinOperationException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.simserver.NoSuchSimError;
import com.flamesgroup.antrax.control.simserver.PINRemoveFailedException;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;

public class ActivationBeanImpl implements ActivationBean {

  private static final Logger logger = LoggerFactory.getLogger(ActivationBeanImpl.class);

  private final DAOProvider daoProvider;
  private final ISimUnitManager simUnitManager;
  private final Lock lock;

  public ActivationBeanImpl(final DAOProvider daoProvider, final ISimUnitManager simUnitManager, final Lock concurrentModificationLock) {
    this.daoProvider = daoProvider;
    this.simUnitManager = simUnitManager;
    // Remove this DAO after ICCID or refreshing SIM arrays table implemented
    this.lock = concurrentModificationLock;
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return false;
  }

  @Override
  public void removeCHV(final ClientUID client, final ICCID[] simUIDs, final CHV chv1)
      throws NotPermittedException, NoSuchSimException, PinOperationException, ConnectionException {
    try {
      logger.debug("[{}] - remove '{}' for channel '{}'", this, chv1, Arrays.toString(simUIDs));
      simUnitManager.removeCHV(simUIDs, chv1);
    } catch (PINRemoveFailedException e) {
      throw new PinOperationException(e.getMessage(), e);
    } catch (RemoteException e) {
      throw new ConnectionException(e.getMessage(), e);
    }
  }

  @Override
  public void enableSimUnit(final ClientUID client, final ICCID[] simUIDs, final boolean enableStatus) throws NotPermittedException, NoSuchSimException, ConnectionException {
    lock.lock();
    try {
      simUnitManager.enableSimUnit(simUIDs, enableStatus);
    } catch (RemoteException e) {
      throw new ConnectionException(e.getMessage(), e);
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void lockSimUnit(final ClientUID client, final ICCID[] simUIDs, final boolean lockStatus, final String reason) throws NotPermittedException, NoSuchSimException,
      ConnectionException {
    lock.lock();
    try {
      simUnitManager.lockSimUnit(simUIDs, lockStatus, reason);
    } catch (RemoteException e) {
      throw new ConnectionException(e.getMessage(), e);
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void resetScriptsStates(final ClientUID clientuid, final ICCID[] simUIDs) {
    lock.lock();
    try {
      for (ICCID simUID : simUIDs) {
        logger.debug("[{}] - reset scripts states for {} executed", this, simUID);
        try {
          daoProvider.getSimpleConfigEditDAO().updateResetSsScriptsFlag(simUID, true);
          daoProvider.getSimpleConfigEditDAO().updateResetVsScriptsFlag(simUID, true);
        } catch (Exception e) {
          logger.warn("[{}] - failed to lockGsmChannel sim {}", this, simUID, e);
        }
        logger.debug("[{}] - reset scripts states for {} finished", this, simUID);
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void resetIMEI(final ClientUID clientuid, final List<ICCID> simUIDs) {
    lock.lock();
    try {
      for (final ICCID simUID : simUIDs) {
        logger.debug("[{}] - reset IMEI for {} executed", this, simUID);
        try {
          simUnitManager.resetIMEI(simUID, "by hand");
        } catch (final Exception e) {
          logger.warn("[{}] - failed to reset IMEI for sim [{}] ", this, simUID, e);
        }
        logger.debug("[{}] - reset imei for sim [{}] finished", this, simUID);
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void updateGSMChannels(final ClientUID client, final GSMChannel[] channels) throws NotPermittedException, StorageException {
    if (channels == null) {
      return;
    }

    ISimpleConfigEditDAO simpleConfigEditDAO = daoProvider.getSimpleConfigEditDAO();
    for (GSMChannel channel : channels) {
      try {
        simpleConfigEditDAO.updateGSMChannel(channel);
      } catch (DataModificationException e) {
        throw new StorageException(e.getMessage());
      }
    }
  }

  @Override
  public void noteSimUnit(final ClientUID clientUID, final ICCID[] simUIDs, final String note) throws ConnectionException, NoSuchSimException {
    try {
      simUnitManager.noteSimUnit(simUIDs, note);
    } catch (RemoteException e) {
      throw new ConnectionException(e.getMessage(), e);
    }
  }

  @Override
  public void invertAllowInternet(final ClientUID clientUID, final ICCID[] uids) throws NotPermittedException, ConnectionException, NoSuchSimException {
    try {
      simUnitManager.invertAllowInternet(uids);
    } catch (RemoteException | NoSuchSimError e) {
      throw new ConnectionException(e.getMessage(), e);
    }
  }

  public void setTariffPlanEndDate(final ClientUID clientUID, final ICCID[] uids, final long endDate) throws ConnectionException, NoSuchSimException {
    for (ICCID uid : uids) {
      try {
        simUnitManager.setTariffPlanEndDate(uid, endDate);
      } catch (RemoteException | NoSuchSimError e) {
        throw new ConnectionException(e.getMessage(), e);
      }
    }
  }

  @Override
  public void resetSimCardStatistic(final ClientUID clientUID, final ICCID[] iccids) throws ConnectionException {
    lock.lock();
    try {
      simUnitManager.resetSimCardStatistic(iccids);
    } catch (RemoteException e) {
      throw new ConnectionException(e.getMessage(), e);
    } finally {
      lock.unlock();
    }
  }

}

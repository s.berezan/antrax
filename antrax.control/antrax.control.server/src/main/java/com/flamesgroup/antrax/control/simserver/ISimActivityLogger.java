/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.unit.ICCID;

public interface ISimActivityLogger {

  void logSimLocked(ICCID simUID, String reason);

  void logSimUnlocked(ICCID simUID, String reason);

  void logSimEnabled(ICCID simUID);

  void logSimDisabled(ICCID simUID);

  void logCallEnded(ICCID simUID, long callDuration);

  void logIncomingCallEnded(ICCID simUID, long callDuration);

  void logSentSMS(ICCID simUID, int totalSmsParts, int successSendSmsParts);

  void logSMSStatus(ICCID simUID);

  void logIncomingSMS(ICCID simUID, int parts);

  void logSimNote(ICCID uid, String note);

  void logAllowedInternet(ICCID simUID, boolean allowed);

  void logTariffPlanEndDate(ICCID uid, long endDate);

  void logBalance(ICCID simUid, double balance);

  void resetStatistic(ICCID iccid);
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.routing;

import com.flamesgroup.commons.CallRouteConfig;
import com.flamesgroup.unit.ICCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class RouteServer {

  private static final Logger logger = LoggerFactory.getLogger(RouteServer.class);

  private static final int PRIORITY_GAIN = 100;

  private final String name;
  private final Pattern calledPattern;
  private final List<RouteDirection> directions;
  private final int priority;

  private final Lock serverLock = new ReentrantLock();

  private int free;
  private int limit;
  private int occupy;

  private List<ICCID> freeChannelUIDS;

  public RouteServer(final String name, final CallRouteConfig routeConfig, final Map<String, TrunkOptions> trunkOptionsMap) {
    this.name = name;

    calledPattern = Pattern.compile(routeConfig.getCalledRegex());
    directions = routeConfig.getDirections().stream().map(d -> new RouteDirection(d.getAddress(), d.getCapacity(), d.getPriority() + PRIORITY_GAIN, trunkOptionsMap.get(d.getAddress())))
        .collect(Collectors.toList());
    priority = routeConfig.getPriority() + PRIORITY_GAIN;
    logger.debug("[{}] - created with priority {} and called regex: {}", this, priority, routeConfig.getCalledRegex());
  }

  public void reset() {
    occupy = 0;
    directions.forEach(RouteDirection::reset);
  }

  public boolean testCalled(final String called) {
    return calledPattern.matcher(called).matches();
  }

  public void updateFreeChannels(final List<ICCID> free, final int limit) {
    serverLock.lock();
    try {
      this.free = free.size();
      this.freeChannelUIDS = free;
      this.limit = limit;
    } finally {
      serverLock.unlock();
    }
  }

  public RouteDirection acquireRoute() {
    serverLock.lock();
    logger.trace("[{}] - acquiring route, occupy: {}, free: {}, limit: {}", this, occupy, free, limit);
    try {
      if (free <= 0 || occupy >= limit || directions.size() <= 0) {
        return null;
      } else {
        Iterator<RouteDirection> iterator = directions.stream().sorted(RouteDirection::compareTo).iterator();
        while (iterator.hasNext()) {
          RouteDirection direction = iterator.next();
          if (direction.acquire()) {
            occupy++;
            return direction;
          }
        }
        return null;
      }
    } finally {
      serverLock.unlock();
    }
  }

  public void releaseRoute(final RouteDirection route) {
    serverLock.lock();
    logger.trace("[{}] - releasing route, occupy: {}, free: {}, limit: {}", this, occupy, free, limit);
    try {
      if (occupy == 0) {
        logger.error("[{}] - someone tried to decrease occupy counter below 0 on route [{}]", this, route);
      } else {
        occupy--;
        route.release();
      }
    } finally {
      serverLock.unlock();
    }
  }

  public int getPriority() {
    return priority;
  }

  public int getLimit() {
    return limit;
  }

  public int getOccupy() {
    return occupy;
  }

  public List<ICCID> getFreeChannelUIDS() {
    return freeChannelUIDS;
  }

  public final class RouteDirection implements Comparable<RouteDirection> {

    private final String address;
    private final int capacity;
    private final int priority;
    private final TrunkOptions trunkOptions;

    private int occupy;

    private RouteDirection(final String address, final int capacity, final int priority, final TrunkOptions trunkOptions) {
      this.address = address;
      this.capacity = capacity;
      this.priority = priority;
      this.trunkOptions = trunkOptions;
      logger.trace("[{}] - created RouteDirection", this);
    }

    private void reset() {
      occupy = 0;
    }

    private int countPriority() {
      return (int) (priority * (1.0f - (float) occupy / (float) limit)); // UNIFORMLY_LOAD_CHANNELS
    }

    private boolean acquire() {
      logger.trace("[{}] - acquiring direction, occupy: {}, capacity: {}", this, occupy, capacity);
      if (capacity == occupy) {
        return false;
      } else {
        occupy++;
        return true;
      }
    }

    private void release() {
      logger.trace("[{}] - releasing direction, occupy: {}, capacity: {}", this, occupy, capacity);
      if (occupy == 0) {
        logger.error("[{}] - someone tried to decrease occupy counter below 0 on route {}", this, address);
      } else {
        occupy--;
      }
    }

    public TrunkOptions getTrunkOptions() {
      return trunkOptions;
    }

    public String getAddress() {
      return address;
    }

    public String getFormatString() {
      return "g729,alaw,mulaw";
    }

    @Override
    public int compareTo(final RouteDirection o) {
      return o.countPriority() - this.countPriority(); // UNIFORMLY_LOAD_CHANNELS
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) + "[address:'" + address + "'" + ']';
    }

  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) + "[name:'" + name + ']';
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.properties;

import com.flamesgroup.antrax.control.server.routing.TrunkOptions;
import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ControlServerProperties implements ServerProperties {

  private static final String RMI_SERVICE_PORT = "rmi.service.port";
  private static final String RMI_SERVER_HOSTNAME = "rmi.server.hostname";

  private static final String DB_CONFIG_DRIVER = "db.config.driver";
  private static final String DB_CONFIG_URL = "db.config.url";
  private static final String DB_CONFIG_USERNAME = "db.config.username";
  private static final String DB_CONFIG_PASSWORD = "db.config.password";
  private static final String DB_CONFIG_MAX_CONNECTION = "db.config.max.connection";

  private static final String WAITING_SERVER_PING_TIMEOUT = "waiting.server.ping.timeout";
  private static final String TRANSACTION_TIMEOUT = "transaction.timeout";
  private static final String RECONFIGURATION_TIMEOUT = "reconfiguration.timeout";
  private static final String EXPIRE_ACTION_TIMEOUT = "expire.action.timeout";
  private static final String SESSION_EXPIRE_TIMEOUT = "session.expire.timeout";
  private static final String SESSION_EXPIRE_CHECK_TIMEOUT = "session.expire.check.timeout";
  private static final String REPORT_SIM_STATUS_TIMEOUT = "report.sim.status.timeout";

  private static final String EXTERNAL_MODULE_YATE_ADDRESS = "external.module.yate.address";
  private static final String EXTERNAL_MODULE_YATE_PORT = "external.module.yate.port";
  private static final String EXTERNAL_MODULE_RECONNECT_TIMEOUT = "external.module.reconnect.timeout";

  private static final String CALL_REROUTE_REASONS = "call.reroute.reasons";
  private static final String CALL_TO_SAME_SIM_CARD = "call.to.same.sim.card";

  private static final String TRUNK_SEND_INTERVAL = "trunk.send.interval";
  private static final String TRUNK_MAX_LENGTH = "trunk.max.length";
  private static final String TRUNK_ADDRESSES = "trunk.addresses";

  private static final String VOIPANTISPAM_CDR_NORMAL_ACD = "voipantispam.cdr.normal.acd";
  private static final String VOIPANTISPAM_CDR_ANALYZE_PERIOD = "voipantispam.cdr.analyze.period";
  private static final String VOIPANTISPAM_CDR_MAX_NORMAL_CALL_PER_PERIOD = "voipantispam.cdr.max.normal.call.per.period";

  private static final String ALARIS_SMS_SERVER_HOSTNAME = "alaris.sms.server.hostname";
  private static final String ALARIS_SMS_SERVER_PORT = "alaris.sms.server.port";
  private static final String ALARIS_SMS_SERVER_USE_SSL = "alaris.sms.server.useSsl";
  private static final String ALARIS_SMS_SERVER_MULTI_USER = "alaris.sms.server.multiUser";
  private static final String ALARIS_SMS_RESENT_AMOUNT = "alaris.sms.resent.amount";
  private static final String ALARIS_SMS_LIMIT_PEAR_DAY = "alaris.sms.limit.pear.day";
  private static final String ALARIS_SMS_LIMIT_PEAR_DAY_BY_IP = "alaris.sms.limit.pear.day.by.ip";
  private static final String ALARIS_SMS_ALLOW_LIMIT_PEAR_DAY_BY_IP = "alaris.sms.allow.limit.pear.day.by.ip";
  private static final String ALARIS_SMS_SENDER_BUFFER = "alaris.sms.sender.buffer";
  private static final String ALARIS_ALTERNATIVE_RESPONSE = "alaris.alternative.response";

  private static final String SIM_CALL_HISTORY_PERIOD = "sim.call.history.period";

  private final AtomicInteger rmiServicePort = new AtomicInteger();
  private final AtomicReference<String> rmiServerHostname = new AtomicReference<>();

  private final AtomicReference<String> dbConfigDriver = new AtomicReference<>();
  private final AtomicReference<String> dbConfigUrl = new AtomicReference<>();
  private final AtomicReference<String> dbConfigUsername = new AtomicReference<>();
  private final AtomicReference<String> dbConfigPassword = new AtomicReference<>();
  private final AtomicInteger dbConfigMaxConnection = new AtomicInteger();

  private final AtomicLong waitingServerPingTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(35));
  private final AtomicLong transactionTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(1));
  private final AtomicLong reconfigurationTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(3));
  private final AtomicLong expireActionTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(30));
  private final AtomicLong sessionExpireTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(5));
  private final AtomicLong sessionExpireCheckTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(15));
  private final AtomicLong reportSimStatusTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(5));

  private final AtomicReference<String> externalModuleYateAddress = new AtomicReference<>();
  private final AtomicInteger externalModuleYatePort = new AtomicInteger();
  private final AtomicLong externalModuleReconnectTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(1));

  private final AtomicReference<List<String>> callRerouteReasons = new AtomicReference<>();
  private final AtomicBoolean callToSameSimCard = new AtomicBoolean();

  private final AtomicReference<Map<String, TrunkOptions>> trunkOptionsMap = new AtomicReference<>();

  private final AtomicLong voipantispamCdrNormalAcd = new AtomicLong();
  private final AtomicLong voipantispamCdrAnalyzePeriod = new AtomicLong();
  private final AtomicInteger voipantispamCdrMaxNormalCallPerPeriod = new AtomicInteger();

  private final AtomicReference<String> alarisSmsServerHostname = new AtomicReference<>();
  private final AtomicInteger alarisSmsServerPort = new AtomicInteger();
  private final AtomicBoolean alarisSmsServerUseSsl = new AtomicBoolean();
  private final AtomicReference<Map<String, AlarisMultiUser>> alarisSmsServerMultiUser = new AtomicReference<>();
  private final AtomicInteger alarisSmsResentAmount = new AtomicInteger();
  private final AtomicInteger alarisSmsLimitPearDay = new AtomicInteger();
  private final AtomicReference<Map<String, Integer>> alarisSmsLimitPearDayByIp = new AtomicReference<>();
  private final AtomicBoolean alarisSmsAllowLimitPearDayByIp = new AtomicBoolean();

  private final AtomicInteger alarisSmsSenderBuffer = new AtomicInteger();
  private final AtomicBoolean alarisIsAlternativeResponse = new AtomicBoolean();;

  private final AtomicLong simCallHistoryPeriod = new AtomicLong();

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);
    rmiServicePort.set(propertiesLoader.getInt(RMI_SERVICE_PORT));
    rmiServerHostname.set(propertiesLoader.getString(RMI_SERVER_HOSTNAME));

    dbConfigDriver.set(propertiesLoader.getString(DB_CONFIG_DRIVER));
    dbConfigUrl.set(propertiesLoader.getString(DB_CONFIG_URL));
    dbConfigUsername.set(propertiesLoader.getString(DB_CONFIG_USERNAME));
    dbConfigPassword.set(propertiesLoader.getString(DB_CONFIG_PASSWORD));
    dbConfigMaxConnection.set(propertiesLoader.getInt(DB_CONFIG_MAX_CONNECTION));

    waitingServerPingTimeout.set(propertiesLoader.getPeriod(WAITING_SERVER_PING_TIMEOUT));
    transactionTimeout.set(propertiesLoader.getPeriod(TRANSACTION_TIMEOUT));
    reconfigurationTimeout.set(propertiesLoader.getPeriod(RECONFIGURATION_TIMEOUT));
    expireActionTimeout.set(propertiesLoader.getPeriod(EXPIRE_ACTION_TIMEOUT));
    sessionExpireTimeout.set(propertiesLoader.getPeriod(SESSION_EXPIRE_TIMEOUT));
    sessionExpireCheckTimeout.set(propertiesLoader.getPeriod(SESSION_EXPIRE_CHECK_TIMEOUT));
    reportSimStatusTimeout.set(propertiesLoader.getPeriod(REPORT_SIM_STATUS_TIMEOUT));

    externalModuleYateAddress.set(propertiesLoader.getString(EXTERNAL_MODULE_YATE_ADDRESS));
    externalModuleYatePort.set(propertiesLoader.getInt(EXTERNAL_MODULE_YATE_PORT));
    externalModuleReconnectTimeout.set(propertiesLoader.getPeriod(EXTERNAL_MODULE_RECONNECT_TIMEOUT));

    List<String> callRerouteReasonsLocal;
    String callRerouteReasonsString = propertiesLoader.getString(CALL_REROUTE_REASONS);
    if (callRerouteReasonsString != null && !callRerouteReasonsString.isEmpty()) {
      callRerouteReasonsLocal = Arrays.stream(callRerouteReasonsString.split(",")).map(String::trim).collect(Collectors.toList());
    } else {
      callRerouteReasonsLocal = Collections.emptyList();
    }
    callRerouteReasons.set(callRerouteReasonsLocal);

    callToSameSimCard.set(propertiesLoader.getBoolean(CALL_TO_SAME_SIM_CARD));

    TrunkOptions trunkOptions = new TrunkOptions(propertiesLoader.getString(TRUNK_SEND_INTERVAL), propertiesLoader.getString(TRUNK_MAX_LENGTH));

    Map<String, TrunkOptions> trunkOptionsMapLocal;
    String trunkAddressesString = propertiesLoader.getString(TRUNK_ADDRESSES);
    if (trunkAddressesString != null && !trunkAddressesString.isEmpty()) {
      trunkOptionsMapLocal = Arrays.stream(trunkAddressesString.split(",")).map(String::trim).collect(Collectors.toMap(Function.identity(), s -> trunkOptions));
    } else {
      trunkOptionsMapLocal = Collections.emptyMap();
    }
    trunkOptionsMap.set(trunkOptionsMapLocal);

    voipantispamCdrNormalAcd.set(TimeUnit.MINUTES.toMillis(propertiesLoader.getInt(VOIPANTISPAM_CDR_NORMAL_ACD)));
    voipantispamCdrAnalyzePeriod.set(TimeUnit.MINUTES.toMillis(propertiesLoader.getInt(VOIPANTISPAM_CDR_ANALYZE_PERIOD)));
    voipantispamCdrMaxNormalCallPerPeriod.set(propertiesLoader.getInt(VOIPANTISPAM_CDR_MAX_NORMAL_CALL_PER_PERIOD));

    alarisSmsServerHostname.set(propertiesLoader.getString(ALARIS_SMS_SERVER_HOSTNAME));
    alarisSmsServerPort.set(propertiesLoader.getInt(ALARIS_SMS_SERVER_PORT));
    alarisSmsServerUseSsl.set(propertiesLoader.getBoolean(ALARIS_SMS_SERVER_USE_SSL));
    String multiUsers = propertiesLoader.getString(ALARIS_SMS_SERVER_MULTI_USER);
    String[] multiUserSplit = multiUsers.split(";");
    if (multiUsers.length() == 0) {
      throw new IllegalArgumentException(ALARIS_SMS_SERVER_MULTI_USER + " must be contains at least one record");
    }
    Map<String, AlarisMultiUser> alarisMultiUsers = new HashMap<>();
    for (String s : multiUserSplit) {
      String[] multiUserRecord = s.split("@");
      if (multiUserRecord.length != 3) {
        throw new IllegalArgumentException(ALARIS_SMS_SERVER_MULTI_USER + " must be contains at least one record, with contains X@Y@Z");
      }
      alarisMultiUsers.put(multiUserRecord[0], new AlarisMultiUser(multiUserRecord[0], multiUserRecord[1], Integer.valueOf(multiUserRecord[2])));
    }
    alarisSmsServerMultiUser.set(alarisMultiUsers);

    alarisSmsResentAmount.set(propertiesLoader.getInt(ALARIS_SMS_RESENT_AMOUNT));
    alarisSmsLimitPearDay.set(propertiesLoader.getInt(ALARIS_SMS_LIMIT_PEAR_DAY));
    String limitByIp = propertiesLoader.getString(ALARIS_SMS_LIMIT_PEAR_DAY_BY_IP);
    String[] limitByIpSplit = limitByIp.split(";");
    if (limitByIpSplit.length == 0) {
      throw new IllegalArgumentException(ALARIS_SMS_LIMIT_PEAR_DAY_BY_IP + " must be contains at least one record");
    }
    Map<String, Integer> alarisLimitByIp = new HashMap<>();
    for (String s : limitByIpSplit) {
      String[] multiIpRecord = s.split("@");
      if (multiIpRecord.length != 2) {
        throw new IllegalArgumentException(ALARIS_SMS_LIMIT_PEAR_DAY_BY_IP + " must be contains at least one record, with contains X@Y");
      }
      if (multiIpRecord[0] != null && !multiIpRecord[0].isEmpty()) {
        alarisLimitByIp.put(multiIpRecord[0], Integer.valueOf(multiIpRecord[1]));
      }
    }
    int allLimitsPerDay = 0;
    for (Integer limit : alarisLimitByIp.values()) {
      allLimitsPerDay += limit;
    }
    if (allLimitsPerDay != alarisSmsLimitPearDay.get()) {
      throw new IllegalArgumentException(ALARIS_SMS_LIMIT_PEAR_DAY_BY_IP + " all limits must be sum of " + ALARIS_SMS_LIMIT_PEAR_DAY);
    }

    alarisSmsAllowLimitPearDayByIp.set(propertiesLoader.getBoolean(ALARIS_SMS_ALLOW_LIMIT_PEAR_DAY_BY_IP));
    alarisSmsLimitPearDayByIp.set(alarisLimitByIp);

    alarisSmsSenderBuffer.set(propertiesLoader.getInt(ALARIS_SMS_SENDER_BUFFER));
    alarisIsAlternativeResponse.set(propertiesLoader.getBoolean(ALARIS_ALTERNATIVE_RESPONSE));

    simCallHistoryPeriod.set(propertiesLoader.getPeriod(SIM_CALL_HISTORY_PERIOD));
  }

  public int getRmiServicePort() {
    return rmiServicePort.get();
  }

  public String getRmiServerHostname() {
    return rmiServerHostname.get();
  }

  public String getDbConfigDriver() {
    return dbConfigDriver.get();
  }

  public String getDbConfigUrl() {
    return dbConfigUrl.get();
  }

  public String getDbConfigUsername() {
    return dbConfigUsername.get();
  }

  public String getDbConfigPassword() {
    return dbConfigPassword.get();
  }

  public int getDbConfigMaxConnection() {
    return dbConfigMaxConnection.get();
  }

  public long getWaitingServerPingTimeout() {
    return waitingServerPingTimeout.get();
  }

  public long getTransactionTimeout() {
    return transactionTimeout.get();
  }

  public long getReconfigurationTimeout() {
    return reconfigurationTimeout.get();
  }

  public long getExpireActionTimeout() {
    return expireActionTimeout.get();
  }

  public long getSessionExpireTimeout() {
    return sessionExpireTimeout.get();
  }

  public long getSessionExpireCheckTimeout() {
    return sessionExpireCheckTimeout.get();
  }

  public long getReportSimStatusTimeout() {
    return reportSimStatusTimeout.get();
  }

  public String getExternalModuleYateAddress() {
    return externalModuleYateAddress.get();
  }

  public int getExternalModuleYatePort() {
    return externalModuleYatePort.get();
  }

  public long getExternalModuleReconnectTimeout() {
    return externalModuleReconnectTimeout.get();
  }

  public List<String> getCallRerouteReasons() {
    return callRerouteReasons.get();
  }

  public boolean getCallToSameSimCard() {
    return callToSameSimCard.get();
  }

  public Map<String, TrunkOptions> getTrunkOptionsMap() {
    return trunkOptionsMap.get();
  }

  public long getVoipantispamCdrNormalAcd() {
    return voipantispamCdrNormalAcd.get();
  }

  public long getVoipantispamCdrAnalyzePeriod() {
    return voipantispamCdrAnalyzePeriod.get();
  }

  public int getVoipantispamCdrMaxNormalCallPerPeriod() {
    return voipantispamCdrMaxNormalCallPerPeriod.get();
  }

  public String getAlarisSmsServerHostname() {
    return alarisSmsServerHostname.get();
  }

  public int getAlarisSmsServerPort() {
    return alarisSmsServerPort.get();
  }

  public boolean getAlarisSmsServerUseSsl() {
    return alarisSmsServerUseSsl.get();
  }

  public Map<String, AlarisMultiUser> getAlarisSmsServerMultiUser() {
    return alarisSmsServerMultiUser.get();
  }

  public int getAlarisSmsResentAmount() {
    return alarisSmsResentAmount.get();
  }

  public int getAlarisSmsLimitPearDay() {
    return alarisSmsLimitPearDay.get();
  }

  public Map<String, Integer> getAlarisSmsLimitPearDayByIp() {
    return alarisSmsLimitPearDayByIp.get();
  }

  public boolean getAlarisSmsAllowLimitPearDayByIp() {
    return alarisSmsAllowLimitPearDayByIp.get();
  }

  public int getAlarisSmsSenderBuffer() {
    return alarisSmsSenderBuffer.get();
  }

  public long getSimCallHistoryPeriod() {
    return simCallHistoryPeriod.get();
  }

  public boolean isAlternativeResponse() {
    return alarisIsAlternativeResponse.get();
  }
}

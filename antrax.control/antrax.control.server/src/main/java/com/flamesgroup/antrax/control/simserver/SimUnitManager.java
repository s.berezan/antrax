/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.communication.ISimChannelManager;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.simserver.core.Configuration;
import com.flamesgroup.antrax.control.simserver.core.SimUnit;
import com.flamesgroup.antrax.control.simserver.core.SimUnitConfigurator;
import com.flamesgroup.antrax.control.simserver.core.SimUnitPool;
import com.flamesgroup.antrax.control.simserver.core.SimUnitSessionPool;
import com.flamesgroup.antrax.control.simserver.engine.ConfigurationEngine;
import com.flamesgroup.antrax.control.simserver.engine.ExpiredSessionsCleanupEngine;
import com.flamesgroup.antrax.control.simserver.engine.SimCardsStatusReportingEngine;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SimUnitManager implements ISimUnitManager, ISimServerManager {

  private static final Logger logger = LoggerFactory.getLogger(SimUnitManager.class);

  private SimUnitSessionPool simUnitSessionPool;
  private SimUnitConfigurator simUnitConfigurator;
  private SimUnitPool simUnitPool;
  private Configuration configuration;
  private ConfigurationEngine cfgEngine;
  private ExpiredSessionsCleanupEngine sessionCleanupEngine;
  private SimCardsStatusReportingEngine simCardsStatusReportingEngine;
  private DeclaredChannelPool declaredChannelPool;

  private ScriptStateSaver scriptStateSaver;

  private final Map<String, Map<ChannelUID, SimUnit>> simUnits = new ConcurrentHashMap<>();
  private final Map<String, ISimChannelManager> simChannelManagers = new ConcurrentHashMap<>();

  @Override
  public void enableSimUnit(final UUID sessionUuid, final boolean enableStatus) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.setEnabled(enableStatus);
    if (enableStatus) {
      logger.debug("[{}] - gateway={} enabled {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    } else {
      logger.debug("[{}] - gateway={} disabled {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    }
  }

  @Override
  public Pair<SimData, IMSI> getSimData(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.clearReconfiguredFlag();
    logger.trace("[{}] - gateway={} getSimData for {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return new Pair<>(simUnit.getSimData(), simUnit.getImsi());
  }

  @Override
  public void handleSimUnitCallEnded(final ICCID uid, final long callDuration) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleCallEnd(callDuration);
    logger.debug("[{}] - call ended on {}; call duration: {}", this, simUnit, callDuration);
  }

  @Override
  public void handleSimUnitIncomingCallEnded(final ICCID uid, final long callDuration) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleIncomingCallEnd(callDuration);
    logger.debug("[{}] - incoming call ended on {}; call duration: {}", this, simUnit, callDuration);
  }

  @Override
  public void handleSimUnitIncomingSMS(final ICCID uid, final int parts) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleIncomingSMS(parts);
    logger.debug("[{}] - received incoming sms on {}", this, simUnit);
  }

  @Override
  public void handleSimUnitSMSStatus(final ICCID uid) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleSMSStatus();
    logger.debug("[{}] - received sms status on {}", this, simUnit);
  }

  @Override
  public void handleSimUnitSentSMS(final ICCID uid, final int parts) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleSendSMS(parts);
    logger.debug("[{}] - send sms on {}", this, simUnit);
  }

  @Override
  public void handleSimUnitFailSentSMS(final ICCID uid, final int totalSmsParts, final int successSendSmsParts) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleFailSendSMS(totalSmsParts, successSendSmsParts);
  }

  @Override
  public void handleSimUnitStartedActivity(final UUID sessionUuid, final ChannelUID gsmChannel, final GSMGroup gsmGroup)
      throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.startActivity(gsmChannel, gsmGroup);
    logger.debug("[{}] - gateway={} started activity for {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
  }

  @Override
  public void handleSimUnitStoppedActivity(final UUID sessionUuid, final String stopReason) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.stopActivity(stopReason);
    logger.debug("[{}] - gateway={} stoped activity for {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
  }

  @Override
  public void handleGenericEvent(final ICCID uid, final String event, final byte[]... serializedArgs) throws RemoteException, NoSuchSimError {
    Serializable[] args = new Serializable[serializedArgs.length];
    for (int i = 0; i < args.length; ++i) {
      args[i] = configuration.getPluginsStore().deserialize(serializedArgs[i]);
    }
    simUnitPool.getSimUnit(uid).handleGenericEvent(event, args);
  }

  @Override
  public SIMGroup changeSimGroup(final ICCID uid, final long simGroupId) throws RemoteException, NoSuchSimError, ChangeGroupFailedError {
    try {
      return simUnitPool.getSimUnit(uid).changeGroup(simGroupId, cfgEngine);
    } catch (DataSelectionException | DataModificationException ignored) {
      throw new ChangeGroupFailedError();
    }
  }

  @Override
  public boolean isSimUnitShouldGenerateIMEI(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    return simUnit.shouldGenerateIMEI();
  }

  @Override
  public IMEI generateIMEI(final UUID sessionUuid) throws IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    return simUnit.generateIMEI();
  }

  @Override
  public void resetIMEI(final ICCID uid, final String message) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.resetIMEI(message);
  }

  @Override
  public void resetIMEI(final UUID sessionUuid, final String message) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.resetIMEI(message);
  }

  @Override
  public void gsmRegistration(final UUID sessionUuid, final int attemptNumber) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.gsmRegistration(attemptNumber);
    logger.debug("[{}] - gateway={} finished registration {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
  }

  @Override
  public void gsmRegistered(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.gsmRegistered();
    logger.debug("[{}] - gateway={} start registration {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
  }

  @Override
  public void addUserMessage(final UUID sessionUuid, final String message) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.addUserMessage(message);
    logger.debug("[{}] - gateway={} add user message {} for {}", this, simUnitSessionPool.getGateway(sessionUuid).toString(), simUnit.toString(), message);
  }

  @Override
  public boolean isSimUnitPlugged(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.debug("[{}] - gateway={} ask is {} plugged", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return simUnit.isPlugged();
  }

  @Override
  public boolean isSimUnitRequiresReconfiguration(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.debug("[{}] - gateway={} ask is {} required reconfiguration", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return simUnit.isReconfigured();
  }

  @Override
  public boolean isSimUnitShouldStartActivity(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.trace("[{}] - gateway={} ask is {} should start activity", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return simUnit.isReadyToStartActivity();
  }

  @Override
  public boolean isSimUnitShouldStopActivity(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.trace("[{}] - gateway={} ask is {} should stop activity", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return simUnit.shouldStopActivity();
  }

  @Override
  public boolean isSimUnitShouldStopSession(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.trace("[{}] - gateway={} ask is {} should stop session", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    return simUnit.shouldStopSession();
  }

  @Override
  public void lockSimUnit(final UUID sessionUuid, final boolean lockStatus, final String reason) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnit.setLocked(lockStatus, reason);
    logger.debug("[{}] - gateway={} set lockGsmChannel status for {}", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
  }

  @Override
  public void returnSimUnit(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    simUnitPool.handleSimUnitReturned(simUnit);
    logger.debug("[{}] - gateway={} returned {} to pool", this, simUnitSessionPool.getGateway(sessionUuid), simUnit);
    simUnitSessionPool.endSimUnitSession(sessionUuid);
  }

  @Override
  public Pair<ChannelUID, UUID> takeSimUnit(final String serverName, final GSMGroup gsmGroup) throws RemoteException {
    IServerData gateway = getGateway(serverName);
    if (gateway == null) {
      return null;
    }

    SimUnit simUnit = simUnitPool.take(gsmGroup, gateway);
    if (simUnit == null) {
      return null;
    }

    UUID sessionUuid = simUnitSessionPool.startSimUnitSession(simUnit, gateway);
    if (sessionUuid == null) {
      logger.debug("[{}] - waiting finishing previous session for unit {}", this, simUnit);
      return null;
    }

    logger.debug("[{}] - started session with UUID={} for simUnit={} and returning it for gateway={}", this, sessionUuid, simUnit, gateway);
    return new Pair<>(simUnit.getChannelUID(), sessionUuid);
  }

  @Override
  public ISCReaderSubChannels takeSCReaderSubChannels(final UUID sessionUuid) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    return simUnit.getSimChannelManager();
  }

  @Override
  public void enableSimUnit(final ICCID[] uids, final boolean enableStatus) throws RemoteException {
    for (SimUnit u : simUnitPool.listSimUnits(uids)) {
      u.setEnabled(enableStatus);
      logger.debug("[{}] - simCard withUID={} was setEnabled {}", this, u.getUID(), enableStatus);
    }

  }

  @Override
  public void lockSimUnit(final ICCID[] uids, final boolean lockStatus, final String reason) throws RemoteException {
    for (SimUnit simUnit : simUnitPool.listSimUnits(uids)) {
      simUnit.setLocked(lockStatus, reason);
      logger.debug("[{}] - simCard withUID={} was setLocked {}", this, simUnit.getUID(), lockStatus);
    }
  }

  @Override
  public void removeCHV(final ICCID[] uid, final CHV chv1) throws RemoteException, PINRemoveFailedException {
    logger.debug("[{}] - attempted to remove pin from sim card {}, using pin {}", this, uid, chv1);
    for (SimUnit u : simUnitPool.listSimUnits(uid)) {
      u.getSimChannelManager().removeCHV(u.getChannelUID(), chv1);
    }
    logger.debug("[{}] - successfully removed PIN from {}", this, uid);
  }

  @Override
  public void updatePhoneNumber(final UUID sessionUuid, final PhoneNumber phoneNumber) throws RemoteException, IllegalSessionUuidException {
    SimUnit simUnit = simUnitSessionPool.getSimUnit(sessionUuid);
    logger.debug("[{}] - attempted to update phone number {} for sim card {}", this, phoneNumber, simUnit);
    simUnit.setPhoneNumber(phoneNumber);
  }

  @Override
  public void handleEmptyChannel(final String serverName, final ChannelUID channel, final ChannelConfig channelConfig) throws RemoteException {
    Map<ChannelUID, SimUnit> simUnitsLocal = simUnits.get(serverName);
    if (simUnitsLocal == null) {
      return;
    }
    SimUnit simUnit = simUnitsLocal.get(channel);
    simUnit.getSimRuntimeData().setLive(true).setEmpty(true).setChannelConfig(channelConfig);
  }

  @Override
  public void handleInvalidChannel(final String serverName, final ChannelUID channel, final ChannelConfig channelConfig) throws RemoteException {
    Map<ChannelUID, SimUnit> simUnitsLocal = simUnits.get(serverName);
    if (simUnitsLocal == null) {
      return;
    }
    SimUnit simUnit = simUnitsLocal.get(channel);
    simUnit.getSimRuntimeData().setLive(true).setChannelConfig(channelConfig);
  }

  @Override
  public void handleAvailableChannel(final String serverName, final ChannelUID channel, final SimChannelConfig simChannelConfig) throws RemoteException {
    Map<ChannelUID, SimUnit> simUnitsLocal = simUnits.get(serverName);
    if (simUnitsLocal == null) {
      return;
    }
    SimUnit simUnit = simUnitsLocal.get(channel);

    // TODO: check to refactor
    simUnitConfigurator.configure(simUnit);
    simUnit.setSimData(new SimData().setUid(simChannelConfig.getIccid()).setPhoneNumber(simChannelConfig.getPhoneNumber()).setSimGroup(new SimNoGroup()));
    simUnit.logFound();

    simUnit.setCHVState(simChannelConfig.getChvState());
    simUnit.setImsi(simChannelConfig.getImsi());
    simUnit.getSimRuntimeData().setLive(true).setEmpty(false).setChannelConfig(simChannelConfig.getChannelConfig());

    simUnitPool.addSimUnit(simUnit);
  }

  @Override
  public void handleLostChannels(final String serverName, final List<ChannelUID> channels) throws RemoteException {
    Map<ChannelUID, SimUnit> simUnitLocal = simUnits.get(serverName);
    ISimChannelManager simChannelManagerLocal = simChannelManagers.get(serverName);
    if (simUnitLocal == null || simChannelManagerLocal == null) {
      return;
    }

    for (ChannelUID channel : channels) {
      SimUnit oldSimUnit = simUnitLocal.get(channel);
      simUnitPool.removeSimUnit(oldSimUnit);

      // TODO: refactor clearing data of lost sim unit
      SimUnit newSimUnit = new SimUnit(channel, simChannelManagerLocal);
      newSimUnit.getSimRuntimeData().setSimHolder(channel);
      newSimUnit.setSimData(new SimData().setSimGroup(new SimNoGroup()));
      simUnitLocal.put(channel, newSimUnit);
    }
  }

  @Override
  public void handleDeclaredChannels(final String serverName, final List<ChannelUID> channels) throws RemoteException {
    Map<ChannelUID, SimUnit> simRuntimeData = channels.stream().collect(Collectors.toMap(Function.identity(), c -> {
      SimUnit simUnit = new SimUnit(c, simChannelManagers.get(serverName));
      simUnit.getSimRuntimeData().setSimHolder(c);
      simUnit.setSimData(new SimData().setSimGroup(new SimNoGroup()));
      return simUnit;
    }));

    this.simUnits.put(serverName, simRuntimeData);
    declaredChannelPool.addDeclaredChannels(channels);
  }

  public List<SimViewData> getSimRuntimeData(final String serverName) {
    return simUnits.getOrDefault(serverName, Collections.emptyMap()).values().stream().map(this::mapSimUnitToSimViewData).collect(Collectors.toList());
  }

  @Override
  public void startSimServerSession(final String serverName, final ISimChannelManager simCardManager) throws RemoteException {
    if (simUnits.containsKey(serverName)) {
      logger.info("[{}] - starting new session of server [{}] without ending, so remove old sim units", this, serverName);
      forceEndSimServerSession(serverName);
    }
    simChannelManagers.put(serverName, simCardManager);
  }

  @Override
  public void endSimServerSession(final String serverName) throws RemoteException {
    forceEndSimServerSession(serverName);
  }

  @Override
  public ISCReaderSubChannels getSCReaderSubChannels(final ChannelUID channel) throws RemoteException {
    SimUnit simUnit = simUnitPool.getSimUnit(channel);
    return simUnit == null ? null : simUnit.getSimChannelManager();
  }

  @Override
  public void noteSimUnit(final ICCID[] simUIDs, final String note) throws RemoteException {
    for (SimUnit simUnit : simUnitPool.listSimUnits(simUIDs)) {
      simUnit.setNote(note);
      logger.debug("[{}] - simCard withUID={} was set note: {}", this, simUnit.getUID(), note);
    }
  }

  @Override
  public void handleAllowedInternet(final ICCID uid, final boolean allowed) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.handleAllowedInternet(allowed);
  }

  @Override
  public void invertAllowInternet(final ICCID[] uids) throws RemoteException, NoSuchSimError {
    for (SimUnit simUnit : simUnitPool.listSimUnits(uids)) {
      simUnit.handleAllowedInternet(!simUnit.getSimData().isAllowInternet());
    }
  }

  public void setTariffPlanEndDate(final ICCID uid, final long endDate) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.setTariffPlanEndDate(endDate);
  }

  @Override
  public void setBalance(final ICCID uid, final double balance) throws RemoteException, NoSuchSimError {
    SimUnit simUnit = simUnitPool.getSimUnit(uid);
    simUnit.setBalance(balance);
  }

  @Override
  public void resetSimCardStatistic(final ICCID[] iccids) throws RemoteException {
    for (SimUnit simUnit : simUnitPool.listSimUnits(iccids)) {
      simUnit.resetStatistic();
      logger.debug("[{}] - simCard withUID={} was resetStatistic", this, simUnit.getUID());
    }
  }

  public void forceEndSimServerSession(final String serverName) {
    Map<ChannelUID, SimUnit> channels = simUnits.remove(serverName);
    if (channels != null) {
      // filter need to prevent removing live device from another server if having two declaration of the same device on different servers
      channels.values().stream().filter(u -> u.getSimRuntimeData().isLive()).forEach(u -> simUnitPool.removeSimUnit(u));
    }
    simChannelManagers.remove(serverName);
  }

  public void setScriptStateSaver(final ScriptStateSaver scriptStateSaver) {
    this.scriptStateSaver = scriptStateSaver;
  }

  public void setUnitSessionPool(final SimUnitSessionPool simUnitSessionPool) {
    this.simUnitSessionPool = simUnitSessionPool;
  }

  public void setSimUnitConfigurator(final SimUnitConfigurator simUnitConfigurator) {
    this.simUnitConfigurator = simUnitConfigurator;
  }

  public void setSimUnitPool(final SimUnitPool simUnitPool) {
    this.simUnitPool = simUnitPool;
  }

  public SimUnitPool getSimUnitPool() {
    return simUnitPool;
  }

  public void setConfiguration(final Configuration configuration) {
    this.configuration = configuration;
  }

  public void setConfigurationEngine(final ConfigurationEngine cfgEngine) {
    this.cfgEngine = cfgEngine;
  }

  public void setSessionCleanupEngine(final ExpiredSessionsCleanupEngine sessionsCleanupEngine) {
    this.sessionCleanupEngine = sessionsCleanupEngine;
  }

  public void setSimStatusReportingEngine(final SimCardsStatusReportingEngine simCardsStatusReportingEngine) {
    this.simCardsStatusReportingEngine = simCardsStatusReportingEngine;
  }

  public void setDeclaredChannelPool(final DeclaredChannelPool declaredChannelPool) {
    this.declaredChannelPool = declaredChannelPool;
  }

  public synchronized void start() {
    logger.debug("[{}] - starting SessionCleanupEngine", this);
    sessionCleanupEngine.start();
    logger.debug("[{}] - starting ConfigurationEngine", this);
    cfgEngine.start();
    logger.debug("[{}] - starting ScriptStateSaver", this);
    scriptStateSaver.start();
    logger.debug("[{}] -starting SimCardsStatusReportingEngine", this);
    simCardsStatusReportingEngine.start();
  }

  public synchronized void stop() {
    logger.debug("[{}] - stopping SimCardsStatusReportingEngine", this);
    simCardsStatusReportingEngine.terminate();
    logger.debug("[{}] - stopping ScriptStateSaver", this);
    scriptStateSaver.terminate();
    logger.debug("[{}] - stopping ConfigurationEngine", this);
    cfgEngine.terminate();
    logger.debug("[{}] - stopping SessionCleanupEngine", this);
    sessionCleanupEngine.interrupt();
    try {
      logger.debug("[{}] - waiting until SimCardsStatusReportingEngine finishes", this);
      simCardsStatusReportingEngine.join();
      logger.debug("[{}] - waiting until ConfigurationEngine finishes", this);
      cfgEngine.join();
      logger.debug("[{}] - waiting until SessionCleanupEngine finishes", this);
      sessionCleanupEngine.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - interrupted while waiting for engines to stop an activity", this, e);
    }
    logger.debug("[{}] - all done", this);
  }

  private IServerData getGateway(final String name) {
    IServerData gateway;
    gateway = configuration.getGateway(name);
    if (gateway == null) {
      logger.warn("[{}] - can't find gateway with name={} in configuration. Returning null", this, name);
    }
    return gateway;
  }

  private SimViewData mapSimUnitToSimViewData(final SimUnit simUnit) {
    SimViewData simViewData = new SimViewData();

    SimRuntimeData simRuntimeData = simUnit.getSimRuntimeData();
    simViewData.setSimHolder(simRuntimeData.getSimHolder()).setGsmHolder(simRuntimeData.getGsmHolder())
        .setCurrentGateway(simRuntimeData.getCurrentGateway()).setPreviousGateway(simRuntimeData.getPreviousGateway())
        .setStatus(simRuntimeData.getStatus()).setStatusTime(simRuntimeData.getStatusTime())
        .setReconfigureTime(simRuntimeData.getReconfigureTime()).setLastCallDuration(simRuntimeData.getLastCallDuration())
        .setUserMessage1(simRuntimeData.getUserMessage1()).setUserMessageTime1(simRuntimeData.getUserMessageTime1())
        .setUserMessage2(simRuntimeData.getUserMessage2()).setUserMessageTime2(simRuntimeData.getUserMessageTime2())
        .setUserMessage3(simRuntimeData.getUserMessage3()).setUserMessageTime3(simRuntimeData.getUserMessageTime3())
        .setEmpty(simRuntimeData.isEmpty()).setLive(simRuntimeData.isLive()).setChannelConfig(simRuntimeData.getChannelConfig());

    SimData simData = simUnit.getSimData();
    if (simData.getSimGroup() instanceof SimNoGroup) {
      simViewData.setSimGroupName(null);
    } else {
      simViewData.setSimGroupName(simData.getSimGroup().getName());
    }

    return simViewData.setUid(simData.getUid()).setPhoneNumber(simData.getPhoneNumber()).setImei(simData.getImei()).setEnabled(simData.isEnabled())
        .setLocked(simData.isLocked()).setLockReason(simData.getLockReason()).setLockTime(simData.getLockTime())
        .setTotalCallsCount(simData.getTotalCallsCount()).setSuccessfulCallsCount(simData.getSuccessfulCallsCount()).setCallDuration(simData.getCallDuration())
        .setIncomingTotalCallsCount(simData.getIncomingTotalCallsCount()).setIncomingSuccessfulCallsCount(simData.getIncomingSuccessfulCallsCount())
        .setIncomingCallDuration(simData.getIncomingCallDuration())
        .setSuccessOutgoingSmsCount(simData.getSuccessOutgoingSmsCount())
        .setTotalOutgoingSmsCount(simData.getTotalOutgoingSmsCount())
        .setCountSmsStatuses(simData.getCountSmsStatuses())
        .setNote(simData.getNote())
        .setAllowInternet(simData.isAllowInternet())
        .setIncomingSMSCount(simData.getIncomingSMSCount())
        .setTariffPlanEndDate(simData.getTariffPlanEndDate())
        .setBalance(simData.getBalance());
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.config;

import com.flamesgroup.antrax.control.authorization.AuthorizationHelper;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class UsersConfig {

  private final Logger logger = LoggerFactory.getLogger(UsersConfig.class);

  private final Map<String, Pair<String, String>> users = new HashMap<>();

  public UsersConfig() {
    initialize("/users.properties");
    users.put("system", new Pair<>("system", "systempass"));
  }

  private void initialize(final String resource) {
    InputStream stream = UsersConfig.class.getResourceAsStream(resource);
    if (stream == null) {
      logger.error("[{}] - can't find users config in classpath: {}", this, resource);
      System.exit(5);
      return;
    }
    Properties props = new Properties();
    try {
      props.load(stream);
    } catch (IOException e) {
      logger.error("[{}] - can't read users config file", this, e);
      System.exit(7);
    }

    for (String user : props.stringPropertyNames()) {
      String[] val = props.getProperty(user).split(":");
      if (val.length != 2) {
        logger.error("[{}] - wrong format of user {} in {}", this, user, resource);
      }
      Pair<String, String> pair = new Pair<>(val[0], readPasswd(val[1]));
      users.put(user, pair);
      logger.debug("[{}] - read user: {} {}", this, user, pair);
    }
  }

  public boolean contains(final String user) {
    return users.containsKey(user);
  }

  public String getPasswd(final String user) {
    return users.get(user).second();
  }

  public String getGroup(final String user) {
    return users.get(user).first();
  }

  public static String writePasswd(final String passwd) {
    return AuthorizationHelper.digest(passwd);
  }

  public static String readPasswd(final String input) {
    return input;
  }

}

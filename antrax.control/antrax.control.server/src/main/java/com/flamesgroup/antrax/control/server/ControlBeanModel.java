/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.control.communication.SmsHistory;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.server.rmi.RemoteVoiceServerManager;
import com.flamesgroup.antrax.control.server.rmi.VoiceServerManagersPool;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.simserver.DeclaredChannelPool;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.rmi.exception.RemoteLookupFailureException;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ControlBeanModel {

  private final VoiceServerManagersPool mPool;
  private final DAOProvider daoProvider;
  private final DeclaredChannelPool declaredChannelPool;

  public ControlBeanModel(final VoiceServerManagersPool pool, final DAOProvider daoProvider, final DeclaredChannelPool declaredChannelPool) {
    this.mPool = pool;
    this.daoProvider = daoProvider;
    this.declaredChannelPool = declaredChannelPool;
  }

  public List<IServerData> listVoiceServers() throws StorageException {
    return mPool.listVoiceServers();
  }

  public List<IServerData> listSimServers(final ClientUID clientUID) {
    IConfigViewDAO configViewDAO = daoProvider.getConfigViewDAO();
    List<IServerData> simServers = new ArrayList<>();
    for (IServerData serverData : configViewDAO.listServers()) {
      if (serverData.isSimServerEnabled()) {
        simServers.add(serverData);
      }
    }
    return simServers;
  }

  public CDR[] listCDRs(final Date fromDate, final Date toDate, final String gsmGroup, final String simGroup, final String callerNumber, final String calledNumber, final ICCID simUID,
      final int pageSize,
      final int offset) {
    CDR[] cdrs = daoProvider.getCallDAO().listCDRs(fromDate.getTime(), toDate.getTime(), simGroup, gsmGroup, callerNumber, calledNumber, simUID, pageSize, offset);
    for (CDR cdr : cdrs) {
      setCallPathAliases(cdr);
    }
    return cdrs;
  }

  public List<SmsHistory> listSmsHistory(final Date fromDate, final Date toDate, final List<String> iccids, final String clientIp) {
    Map<String, Integer> countAmountForCertainTime = daoProvider.getSentSmsDAO().countAmountForCertainTime(fromDate, toDate, iccids, clientIp);
    Map<String, Integer> countAmountForAllTime = daoProvider.getSentSmsDAO().countAmountForCertainTime(new Date(0), new Date(), iccids, clientIp);

    List<SmsHistory> smsHistories = new ArrayList<>();

    for (Map.Entry<String, Integer> entry : countAmountForCertainTime.entrySet()) {
      smsHistories.add(new SmsHistory(entry.getKey(), entry.getValue(), countAmountForAllTime.get(entry.getKey())));
    }

    return smsHistories;
  }

  public List<VoiceServerCallStatistic> listVoiceServerCallStatistic(final Date fromDate, final Date toDate, final String prefix, final String server) {
    return daoProvider.getCallDAO().listVoiceServerCallStatistic(fromDate, toDate, prefix, server);
  }

  // TODO: remove ugly replace real device uid value to alias name
  public SIMEventRec[] listSIMEvents(final ICCID simUID, final Date fromDate, final Date toDate, final int limit) {
    SIMEventRec[] simEventRecords = daoProvider.getSIMHistoryDAO().listSIMEventsByUid(simUID, fromDate.getTime(), toDate.getTime(), limit);
    for (SIMEventRec simEventRecord : simEventRecords) {
      switch (simEventRecord.getEvent()) {
        case CALL_CHANNEL_BUILD:
          ChannelUID channel2 = ChannelUID.valueFromCanonicalName(simEventRecord.getArgs()[1]);
          ChannelUID declaredChannel2 = declaredChannelPool.getDeclaredChannels().stream().filter(c -> c.equals(channel2)).findFirst().orElse(channel2);
          simEventRecord.getArgs()[1] = (declaredChannel2.getAliasCanonicalName());
        case SIM_FOUND:
          ChannelUID channel1 = ChannelUID.valueFromCanonicalName(simEventRecord.getArgs()[0]);
          ChannelUID declaredChannel1 = declaredChannelPool.getDeclaredChannels().stream().filter(c -> c.equals(channel1)).findFirst().orElse(channel1);
          simEventRecord.getArgs()[0] = (declaredChannel1.getAliasCanonicalName());
      }
    }
    return simEventRecords;
  }

  public ICCID[] findSIMList(final SimSearchingParams params) {
    return daoProvider.getSimpleConfigViewDAO().findSIMList(params);
  }

  public CDR getCDR(final Long id) {
    CDR cdr = daoProvider.getCallDAO().getCDRById(id);
    setCallPathAliases(cdr);
    return cdr;
  }

  private void setCallPathAliases(final CDR cdr) {
    cdr.getCallPath().forEach(cp -> {
      cp.setGsmChannelUID(declaredChannelPool.getDeclaredChannels().stream().filter(c -> c.equals(cp.getGsmChannelUID())).findFirst().orElse(cp.getGsmChannelUID()));
      cp.setSimChannelUID(declaredChannelPool.getDeclaredChannels().stream().filter(c -> c.equals(cp.getSimChannelUID())).findFirst().orElse(cp.getSimChannelUID()));
    });
  }

  public void fireEvent(final IServerData gateway, final ICCID simUID, final String event) throws NoSuchFieldException, EventException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      manager.fireEvent(simUID, event);
    }
  }

  public void lockGsmChannel(final IServerData gateway, final ChannelUID gsmChannel, final boolean lock, final String lockReason) throws NoSuchFieldException, LockException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && gsmChannel != null) {
      manager.lockGsmChannel(gsmChannel, lock, lockReason);
    }
  }

  public void lockGsmChannelToArfcn(final IServerData gateway, final ChannelUID gsmChannel, final int arfn) throws NoSuchFieldException, LockArfcnException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      manager.lockGsmChannelToArfcn(gsmChannel, arfn);
    }
    daoProvider.getSimpleConfigEditDAO().lockGsmChannelToArfcn(gsmChannel, arfn);
  }

  public void unLockGsmChannelToArfcn(final IServerData gateway, final ChannelUID gsmChannel) throws NoSuchFieldException, LockArfcnException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      manager.unLockGsmChannelToArfcn(gsmChannel);
    }
    daoProvider.getSimpleConfigEditDAO().lockGsmChannelToArfcn(gsmChannel, null);
  }

  public void executeNetworkSurvey(final IServerData gateway, final ChannelUID gsmChannel) throws NoSuchFieldException, NetworkSurveyException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && gsmChannel != null) {
      manager.executeNetworkSurvey(gsmChannel);
    }
  }

  public void sendUssd(final IServerData gateway, final ICCID simUID, final String ussd) throws NoSuchFieldException, USSDException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      manager.sendUSSD(simUID, ussd);
    }
  }

  public String startUSSDSession(final IServerData gateway, final ICCID simUID, final String ussd) throws NoSuchFieldException, USSDException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      return manager.startUSSDSession(simUID, ussd);
    }
    return null;
  }

  public String sendUSSDSessionCommand(final IServerData gateway, final ICCID simUID, final String ussd) throws NoSuchFieldException, USSDException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      return manager.sendUSSDSessionCommand(simUID, ussd);
    }
    return null;
  }

  public void endUSSDSession(final IServerData gateway, final ICCID simUID) throws NoSuchFieldException, USSDException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      manager.endUSSDSession(simUID);
    }
  }

  public void sendSMS(final IServerData gateway, final ICCID simUID, final String number, final String smsText) throws NoSuchFieldException, SMSException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      manager.sendSMS(simUID, number, smsText);
    }
  }

  public VsSmsStatus sendSMS(final IServerData gateway, final List<AlarisSms> alarisSmses, final UUID objectUuid) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);

    if (manager == null) {
      return new VsSmsStatus().setStatus(VsSmsStatus.Status.FAIL);
    } else {
      return manager.sendSMS(alarisSmses, objectUuid);
    }
  }

  public VsSmsStatus getSMSStatus(final IServerData gateway, final UUID objectUuid) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);

    if (manager == null) {
      return new VsSmsStatus().setStatus(VsSmsStatus.Status.FAIL);
    } else {
      return manager.getSMSStatus(objectUuid);
    }
  }

  public void removeSMSStatus(final IServerData gateway, final UUID objectUuid) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);

    if (manager != null) {
      manager.removeSMSStatus(objectUuid);
    }
  }

  public void sendDTMF(final IServerData gateway, final ICCID simUID, final String dtmf) throws NoSuchFieldException, DTMFException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null && simUID != null) {
      manager.sendDTMF(simUID, dtmf);
    }
  }

  public void resetStatistic(final IServerData gateway) throws NoSuchFieldException {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      manager.resetStatistic();
    }
  }

  public void updateAudioCaptureEnable(final IServerData gateway) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      manager.updateAudioCaptureEnable(gateway.getVoiceServerConfig().isAudioCaptureEnabled());
    }
  }

  public List<AudioFile> getAudioFiles(final IServerData gateway) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      return manager.getAudioFiles();
    }
    return new ArrayList<>();
  }

  public byte[] downloadAudioFileFromServer(final IServerData gateway, final String fileName) {
    RemoteVoiceServerManager manager = mPool.getManager(gateway);
    if (manager != null) {
      return manager.downloadAudioFileFromServer(fileName);
    }
    return new byte[0];
  }

  public List<String> getIvrTemplatesSimGroups(final IServerData selectedServer) {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        return manager.getIvrTemplatesSimGroups();
      } catch (RemoteLookupFailureException ignore) {
        //ignore
      }
    }
    return new ArrayList<>();
  }

  public List<IvrTemplateWrapper> getIvrTemplates(final IServerData selectedServer, final String simGroup) {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        return manager.getIvrTemplates(simGroup);
      } catch (RemoteLookupFailureException ignore) {
        //ignore
      }
    }
    return new ArrayList<>();
  }

  public void createIvrTemplateSimGroup(final IServerData selectedServer, final String simGroup) throws IvrTemplateException {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        manager.createIvrTemplateSimGroup(simGroup);
      } catch (RemoteLookupFailureException ignore) {
        //ignore
      }
    }
  }

  public void removeIvrTemplateSimGroups(final IServerData selectedServer, final List<String> simGroups) throws IvrTemplateException {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        manager.removeIvrTemplateSimGroups(simGroups);
      } catch (RemoteLookupFailureException ignore) {
        //ignore
      }
    }
  }

  public Map<String, Integer> getCallDropReasons(final IServerData selectedServer) {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        return manager.getCallDropReasons();
      } catch (RemoteLookupFailureException ignore) {
        //ignore
      }
    }
    return Collections.emptyMap();
  }

  public void uploadIvrTemplate(final IServerData selectedServer, final String simGroup, final byte[] bytes, final String fileName) throws IvrTemplateException {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        manager.uploadIvrTemplate(simGroup, bytes, fileName);
      } catch (RemoteLookupFailureException e) {
        throw new IvrTemplateException(e);
      }
    }
  }

  public void removeIvrTemplate(final IServerData selectedServer, final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException {
    RemoteVoiceServerManager manager = mPool.getManager(selectedServer);
    if (manager != null) {
      try {
        manager.removeIvrTemplate(simGroup, ivrTemplates);
      } catch (RemoteLookupFailureException e) {
        throw new IvrTemplateException(e);
      }
    }
  }
}

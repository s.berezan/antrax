/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import static com.flamesgroup.rmi.RemoteServiceExporter.export;
import static com.flamesgroup.utils.AntraxProperties.CONTROL_SERVER_HOST_NAME;
import static com.flamesgroup.utils.AntraxProperties.CONTROL_SERVER_RMI_PORT;

import com.flamesgroup.antrax.activity.GsmViewManager;
import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.activity.RemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.configuration.IPingServerManager;
import com.flamesgroup.antrax.configuration.SysCfgBean;
import com.flamesgroup.antrax.control.authorization.AuthorizationFailedException;
import com.flamesgroup.antrax.control.authorization.AuthorizationHelper;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.autoactions.ActionExecutionManager;
import com.flamesgroup.antrax.control.communication.ActivationBean;
import com.flamesgroup.antrax.control.communication.ActivityRemoteLogger;
import com.flamesgroup.antrax.control.communication.AuthorizationBean;
import com.flamesgroup.antrax.control.communication.ConfigurationBean;
import com.flamesgroup.antrax.control.communication.ControlBean;
import com.flamesgroup.antrax.control.communication.IActivityRemoteLogger;
import com.flamesgroup.antrax.control.communication.IGsmViewBean;
import com.flamesgroup.antrax.control.communication.IPrefixListBean;
import com.flamesgroup.antrax.control.communication.IVoipAntiSpamStatisticBean;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.config.ControlServerConfig;
import com.flamesgroup.antrax.control.server.rest.alaris.sms.AlarisSmsManager;
import com.flamesgroup.antrax.control.server.rest.alaris.sms.AlarisSmsServer;
import com.flamesgroup.antrax.control.server.rmi.VoiceServerManagersPool;
import com.flamesgroup.antrax.control.server.routing.YateCallRouteEngine;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.server.utils.EditTransactionManager;
import com.flamesgroup.antrax.control.server.utils.PermissionsCheckerProxy;
import com.flamesgroup.antrax.control.simserver.DeclaredChannelPool;
import com.flamesgroup.antrax.control.simserver.GsmUnitManager;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.simserver.SimActivityLogger;
import com.flamesgroup.antrax.control.simserver.SimCallHistoryManager;
import com.flamesgroup.antrax.control.simserver.SimHistoryLogger;
import com.flamesgroup.antrax.control.simserver.SimUnitManager;
import com.flamesgroup.antrax.control.simserver.UnitManagerBuilder;
import com.flamesgroup.antrax.storage.dao.UpdateFailedException;
import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;
import com.flamesgroup.antrax.voiceserv.utils.delivery.GuaranteedDeliveryProxyServer;
import com.flamesgroup.daemonext.DaemonExt;
import com.flamesgroup.daemonext.IDaemonExtStatus;
import com.flamesgroup.properties.ReloadCommand;
import com.flamesgroup.rmi.RemoteExporter;
import com.flamesgroup.utils.BuildInfo;
import com.flamesgroup.utils.BuildInfoUtils;
import com.flamesgroup.utils.UncaughtExceptionHandlerWrapper;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ControlServerLauncher extends DaemonExt {

  private static final Logger logger = LoggerFactory.getLogger(ControlServerLauncher.class);

  private static final String AUTHORIZE_CLIENT_NAME = "system";
  private static final String AUTHORIZE_CLIENT_PASSWORD = "systempass";

  private ControlBeanImpl controlBean;
  private AuthorizationBean authorizationBean;
  private VoipAntiSpamScheduler voipAntiSpamScheduler;
  private AlarisSmsServer alarisSmsServer;

  private ControlServerStatus controlServerStatus;
  private ReloadCommand reloadCommand;

  public static void main(final String[] args) throws Exception {
    ControlServerLauncher controlServerLauncher = new ControlServerLauncher();
    controlServerLauncher.main(controlServerLauncher.getClass().getSimpleName(), args);
  }

  @Override
  public List<IDaemonExtStatus> getStatuses() {
    return Arrays.asList(controlServerStatus, reloadCommand);
  }

  @Override
  public void start() throws Exception {
    BuildInfo buildInfo = BuildInfoUtils.readBuildInfo(this.getClass());
    logDuplicateInfo("Control Server start: " + (buildInfo == null ? "buildInfo didn't found at JAR" : buildInfo.toString()));
    reloadCommand = new ReloadCommand();
    reloadCommand.addReloadListener(ControlPropUtils.getInstance());
    reloadCommand.addReloadListener(AlarisSmsManager.getInstance());
    logger.debug("[{}] - reading configuration", this);
    ControlServerConfig controlServerConfig = new ControlServerConfig();

    try {
      logger.debug("[{}] - database migration", this);
      DBConnectionProps dbConnectionProps = controlServerConfig.getDbConnectionProps();
      Flyway flyway = new Flyway();
      flyway.setDataSource(dbConnectionProps.getUrl(), dbConnectionProps.getUsername(), dbConnectionProps.getPassword());
      flyway.setBaselineDescription("init database");
      flyway.setBaselineVersionAsString("0");
      flyway.setBaselineOnMigrate(true);
      flyway.migrate();

      logger.debug("[{}] - creating DAO provider", this);
      DAOProvider daoProvider = new DAOProvider(dbConnectionProps);

      logger.debug("[{}] - creating EditTransactionManager", this);
      EditTransactionManager transactionManager = new EditTransactionManager(daoProvider.getConfigEditDAO());

      logger.debug("[{}] - creating ActionExecutionManager", this);
      ActionExecutionManager actionExecutionManager = new ActionExecutionManager();

      logger.debug("[{}] - creating delivery servers", this);
      GuaranteedDeliveryProxyServer simHistoryDAODelivery = new GuaranteedDeliveryProxyServer("SimHistoryDAODelivery", UpdateFailedException.class);

      logger.debug("[{}] - creating VoipAntiSpam", this);
      VoipAntiSpam voipAntiSpam = new VoipAntiSpam(daoProvider);

      SimCallHistoryManager simCallHistoryManager = new SimCallHistoryManager(daoProvider.getCallDAO());
      simCallHistoryManager.reload();
      reloadCommand.addReloadListener(simCallHistoryManager);

      logger.debug("[{}] - creating Sim loggers", this);
      SimActivityLogger simActivityLogger = new SimActivityLogger(daoProvider.getSimpleConfigEditDAO());
      SimHistoryLogger simHistoryLogger = new SimHistoryLogger(voipAntiSpam, simCallHistoryManager, daoProvider.getSIMHistoryDAO(), daoProvider.getCallDAO());
      ISmsManager smsManager = new SmsManager(daoProvider.getReceivedSmsDAO(), daoProvider.getSentSmsDAO(), daoProvider.getAlarisSmsDAO());
      IRemoteHistoryLogger remoteHistoryLogger = new RemoteHistoryLogger(simHistoryLogger, daoProvider.getSimpleConfigEditDAO(), daoProvider.getVoipAntiSpamDAO());
      IGsmViewManager gsmViewManager = new GsmViewManager(daoProvider.getNetworkSurveyDAO(), daoProvider.getConfigViewDAO(), daoProvider.getGsmViewDAO());

      logger.debug("[{}] - creating Unit Managers", this);
      IRemoteVSConfigurator remoteVSConfigurator = new RemoteVSConfigurator(daoProvider.getConfigViewDAO(), daoProvider.getSimpleConfigEditDAO(), daoProvider.getSimpleConfigViewDAO());

      DeclaredChannelPool declaredChannelPool = new DeclaredChannelPool();
      UnitManagerBuilder builder = new UnitManagerBuilder(daoProvider, simActivityLogger, simHistoryLogger, remoteVSConfigurator, declaredChannelPool, simCallHistoryManager);

      SimUnitManager simUnitManager = builder.buildSimUnitManager();
      GsmUnitManager gsmUnitManager = builder.buildGsmUnitManager();

      logger.debug("[{}] - creating YateCallRouteEngine", this);
      PrefixMarkLists prefixMarkLists = new PrefixMarkLists(daoProvider.getPhoneNumberPrefixDAO());
      YateCallRouteEngine yateCallRouteEngine = new YateCallRouteEngine(voipAntiSpam, prefixMarkLists, simCallHistoryManager);
      yateCallRouteEngine.reconfigure(daoProvider.getConfigViewDAO());

      ActivityRemoteLogger activityRemoteLogger = new ActivityRemoteLogger();

      logger.debug("[{}] - creating PingServersManager", this);
      PingServersManager pingServersManager = new PingServersManager(simUnitManager, yateCallRouteEngine, gsmViewManager, activityRemoteLogger);
      pingServersManager.reconfigure(daoProvider.getConfigViewDAO());

      logger.debug("[{}] - creating RemoteRegistryAccess", this);
      RemoteRegistryAccess remoteRegistryAccess = daoProvider.getRegistryDAO();

      logger.debug("[{}] - creating SysCfgBean", this);
      SysCfgBeanImpl sysCfgBean =
          new SysCfgBeanImpl(daoProvider, pingServersManager, actionExecutionManager, remoteHistoryLogger, smsManager, gsmViewManager, remoteVSConfigurator, remoteRegistryAccess,
              simCallHistoryManager, simUnitManager, gsmUnitManager, activityRemoteLogger);

      int rmiServicePort = controlServerConfig.getControlServerServicePort();

      logger.debug("[{}] - control Server host name {}", this, CONTROL_SERVER_HOST_NAME);
      logger.debug("[{}] - control Server rmi port {}", this, CONTROL_SERVER_RMI_PORT);
      logger.debug("[{}] - control Server service rmi port {}", this, rmiServicePort);

      System.setProperty("java.rmi.server.hostname", controlServerConfig.getRmiServerHostname());

      logger.debug("[{}] - exporting AuthorizationBean", this);
      authorizationBean = new AuthorizationBeanImpl();
      export(new RemoteExporter.Builder(authorizationBean, AuthorizationBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(
          rmiServicePort).build());
      logger.debug("[{}] - exporting ControlBean", this);

      ReadWriteLock lock = new ReentrantReadWriteLock();
      controlBean = new ControlBeanImpl(daoProvider.getSessionDAO(), daoProvider.getConfigViewDAO());
      voipAntiSpamScheduler = new VoipAntiSpamScheduler(daoProvider, controlServerConfig);
      controlServerStatus = new ControlServerStatus(controlBean);
      ControlBeanModel controlBeanModel = new ControlBeanModel(new VoiceServerManagersPool(daoProvider.getConfigViewDAO(), pingServersManager), daoProvider, declaredChannelPool);
      alarisSmsServer = new AlarisSmsServer(daoProvider, controlBeanModel, pingServersManager);
      alarisSmsServer.getAlarisSmsService().reconfigure(daoProvider.getConfigViewDAO());
      reloadCommand.addReloadListener(alarisSmsServer.getAlarisSmsService());
      controlBean.setModel(controlBeanModel).setPingServersManager(pingServersManager).setSimHistoryDAODeliveryServer(simHistoryDAODelivery);
      controlBean.setActionExecutionManager(actionExecutionManager).setSimUnitManager(simUnitManager);
      controlBean.setYateCallRouteEngine(yateCallRouteEngine).setAlarisSmsService(alarisSmsServer.getAlarisSmsService());
      export(new RemoteExporter.Builder(PermissionsCheckerProxy.createProxy(ControlBean.class, controlBean), ControlBean.class,
          "ControlBean").setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting ActivationBean", this);
      ActivationBean activationBean = createActivationBean(simUnitManager, daoProvider, lock.writeLock());
      export(new RemoteExporter.Builder(activationBean, ActivationBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting ConfigurationBean", this);
      ConfigurationBean configurationBean = createConfigurationBean(daoProvider, transactionManager, Arrays.asList(pingServersManager, yateCallRouteEngine, alarisSmsServer.getAlarisSmsService()),
          simUnitManager, controlBeanModel);
      export(new RemoteExporter.Builder(configurationBean, ConfigurationBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting VoipAntiSpamStatisticBean", this);
      IVoipAntiSpamStatisticBean voipAntiSpamStatistic = createVoipAntiSpamStatisticBean(daoProvider);
      export(new RemoteExporter.Builder(voipAntiSpamStatistic, IVoipAntiSpamStatisticBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting SysCfgBean", this);
      export(new RemoteExporter.Builder(sysCfgBean, SysCfgBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting RegistryAccessBean", this);
      RegistryAccessBean reqistryAccessBean = createRegistryAccess(transactionManager, daoProvider);
      export(new RemoteExporter.Builder(reqistryAccessBean, RegistryAccessBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting PingServersManagers", this);
      export(new RemoteExporter.Builder(pingServersManager, IPingServerManager.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting ActionExecutionManager", this);
      export(new RemoteExporter.Builder(actionExecutionManager, IActionExecutionManager.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting SimHistoryLogger", this);
      export(new RemoteExporter.Builder(remoteHistoryLogger, IRemoteHistoryLogger.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting SmsManager", this);
      export(new RemoteExporter.Builder(smsManager, ISmsManager.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting GsmViewManager", this);
      export(new RemoteExporter.Builder(gsmViewManager, IGsmViewManager.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting GsmViewBean", this);
      IGsmViewBean gsmViewBean = createGsmViewBean(daoProvider, gsmUnitManager, gsmViewManager);
      export(new RemoteExporter.Builder(gsmViewBean, IGsmViewBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting PrefixListBean", this);
      IPrefixListBean prefixListBean = createPrefixListBean(daoProvider);
      export(new RemoteExporter.Builder(prefixListBean, IPrefixListBean.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting ISimUnitManager", this);
      export(new RemoteExporter.Builder(simUnitManager, ISimUnitManager.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting RemoteVSConfigurator", this);
      export(new RemoteExporter.Builder(remoteVSConfigurator, IRemoteVSConfigurator.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting RemoteRegistryAccess", this);
      export(new RemoteExporter.Builder(remoteRegistryAccess, RemoteRegistryAccess.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting SimCallHistory", this);
      export(new RemoteExporter.Builder(simCallHistoryManager, ISimCallHistory.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting IGsmUnitManager", this);
      export(new RemoteExporter.Builder(gsmUnitManager, IGsmUnitManger.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());
      logger.debug("[{}] - exporting ActivityRemoteLogger", this);
      export(new RemoteExporter.Builder(activityRemoteLogger, IActivityRemoteLogger.class).setRegistryPort(CONTROL_SERVER_RMI_PORT).setServicePort(rmiServicePort).build());

      controlBean.start();
      voipAntiSpamScheduler.start();
      alarisSmsServer.start();

      Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerWrapper((t, e) -> {
        logger.error("[{}] - control Server - Uncaught Exception in thread [{}]", this, t, e);
        try {
          logDuplicateInfo("Control Server stop on Uncaught Exception");
          controlBean.shutdown(null);
        } catch (NotPermittedException ignored) {
          // if can't stop, it is ok, server will restart during next 60 seconds
        }
        System.exit(EXIT_CODE_TO_BE_RESTARTED);
      }));

      super.start();
    } catch (Exception e) {
      logDuplicateError("Control Server start failed", e);
      System.exit(EXIT_CODE_TO_BE_RESTARTED);
    }
  }

  @Override
  public void stop() throws Exception {
    logDuplicateInfo("Control Server stop");
    super.stop();
    ClientUID client = new ClientUID();
    try {
      authorize(client);
      alarisSmsServer.stop();
      voipAntiSpamScheduler.stop();
      controlBean.shutdown(client);
    } catch (NotPermittedException | AuthorizationFailedException e) {
      logDuplicateError("Control Server stop failed", e);
      System.exit(1);
    }
  }

  private RegistryAccessBean createRegistryAccess(final EditTransactionManager transactionManager, final DAOProvider daoProvider) {
    RegistryAccessBean retval = new RegistryAccessBeanImpl(daoProvider);
    retval = PermissionsCheckerProxy.createProxy(RegistryAccessBean.class, retval);
    return retval;
  }

  private ConfigurationBean createConfigurationBean(final DAOProvider daoProvider, final EditTransactionManager transactionManager,
      final List<IConfigurationHandler> configurationHandlers, final ISimUnitManager simUnitManager, final ControlBeanModel controlBeanModel) {
    ConfigurationBean cfgBean = new ConfigurationBeanImpl(transactionManager, daoProvider, configurationHandlers, simUnitManager, controlBeanModel);
    return PermissionsCheckerProxy.createProxy(ConfigurationBean.class, cfgBean);
  }

  private IVoipAntiSpamStatisticBean createVoipAntiSpamStatisticBean(final DAOProvider daoProvider) {
    IVoipAntiSpamStatisticBean voipAntiSpamStatistic = new VoipAntiSpamStatisticBean(daoProvider);
    return PermissionsCheckerProxy.createProxy(IVoipAntiSpamStatisticBean.class, voipAntiSpamStatistic);
  }

  private IGsmViewBean createGsmViewBean(final DAOProvider daoProvider, final GsmUnitManager gsmUnitManager, final IGsmViewManager gsmViewManager) {
    IGsmViewBean gsmViewBean = new GsmViewBean(daoProvider, gsmUnitManager, gsmViewManager);
    return PermissionsCheckerProxy.createProxy(IGsmViewBean.class, gsmViewBean);
  }

  private IPrefixListBean createPrefixListBean(final DAOProvider daoProvider) {
    IPrefixListBean prefixListBean = new PrefixListBean(daoProvider);
    return PermissionsCheckerProxy.createProxy(IPrefixListBean.class, prefixListBean);
  }

  private ActivationBean createActivationBean(final ISimUnitManager simUnitManager, final DAOProvider daoProvider, final Lock lock) {
    ActivationBean activationBean = new ActivationBeanImpl(daoProvider, simUnitManager, lock);
    return PermissionsCheckerProxy.createProxy(ActivationBean.class, activationBean);
  }

  private void authorize(final ClientUID client) throws AuthorizationFailedException {
    AuthorizationHelper.authorize(client, AUTHORIZE_CLIENT_NAME, AUTHORIZE_CLIENT_PASSWORD, authorizationBean);
  }

  private void logDuplicateInfo(final String message) {
    logger.info(message);
    getOut().println(OffsetDateTime.now() + " - " + message);
  }

  private void logDuplicateError(final String message, final Exception e) {
    logger.error(message, e);
    getOut().println(OffsetDateTime.now() + " - " + message + ": " + e.getMessage());
  }

}

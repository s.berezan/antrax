/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.utils;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.commons.Pair;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class SessionsPool implements SelfCleaner {

  public final static long SESSION_LIFETIME = 15 * 60 * 1000; // 15 minute

  private static volatile SessionsPool instance;

  private final Map<ClientUID, Pair<SessionData, AtomicLong>> sessions = new HashMap<>();

  private final List<ControlSessionListener> listeners = Collections.synchronizedList(new LinkedList<>());

  private SessionsPool() {
  }

  public static SessionsPool getInstance() {
    if (instance == null) {
      synchronized (SessionsPool.class) {
        if (instance == null) {
          instance = new SessionsPool();

          Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
              instance.forceCleanup();
            }
          });
        }
      }
    }
    return instance;
  }

  private void fireSessionStarted(final SessionClientData data) {
    for (ControlSessionListener l : listeners) {
      l.handleSessionStarted(data);
    }
  }

  private void fireSessionRenewed(final ClientUID client) {
    for (ControlSessionListener l : listeners) {
      l.handleSessionRenewed(client);
    }
  }

  private void fireSessionEnded(final ClientUID client) {
    for (ControlSessionListener l : listeners) {
      l.handleSessionEnded(client);
    }
  }

  public void addSessionListener(final ControlSessionListener listener) {
    listeners.add(listener);
  }

  public void removeSessionListener(final ControlSessionListener listener) {
    listeners.remove(listener);
  }

  public synchronized void startSession(final ClientUID client, final String userName, final String groupName, final String ipAddress) {
    SessionData sessionData = new SessionData(groupName);
    Pair<SessionData, AtomicLong> pair = new Pair<>(sessionData, new AtomicLong(System.currentTimeMillis()));
    sessions.put(client, pair);
    fireSessionStarted(new SessionClientData(client, userName, groupName, ipAddress));
  }

  private Pair<SessionData, AtomicLong> getCurrentSessionPair(final ClientUID client, final boolean retain) {
    Pair<SessionData, AtomicLong> pair;
    pair = sessions.get(client);

    if (pair != null && retain) {
      pair.second().set(System.currentTimeMillis()); // refreshing last access time
      fireSessionRenewed(client);
    }

    return pair;
  }

  public void markAsUsing(final ClientUID client, final boolean b) {
    Pair<SessionData, AtomicLong> pair = getCurrentSessionPair(client, true);
    if (pair != null) {
      pair.first().setUsing(b);
    }
  }

  public synchronized String getCurrentSessionGroup(final ClientUID client) {
    Pair<SessionData, AtomicLong> pair = getCurrentSessionPair(client, true);
    return (pair == null) ? null : pair.first().getGroupName();
  }

  public synchronized void setSessionProperty(final ClientUID client, final String key, final Object value) {
    Pair<SessionData, AtomicLong> pair = getCurrentSessionPair(client, false);
    if (pair != null) {
      pair.first().setProperty(key, value);
    }
  }

  public synchronized Object getSessionProperty(final ClientUID client, final String key) {
    Pair<SessionData, AtomicLong> pair = getCurrentSessionPair(client, false);
    return (key == null || pair == null) ? null : pair.first().getProperty(key);
  }

  public synchronized void removeSessionProperty(final ClientUID client, final String key) {
    Pair<SessionData, AtomicLong> pair = getCurrentSessionPair(client, false);
    if (pair != null) {
      pair.first().removeProperty(key);
    }
  }

  @Override
  public synchronized void cleanup() {
    Iterator<ClientUID> iter = sessions.keySet().iterator();
    while (iter.hasNext()) {
      ClientUID uid = iter.next();
      Pair<SessionData, AtomicLong> pair = sessions.get(uid);
      if (pair.first().isUsing()) {
        continue;
      }
      if (System.currentTimeMillis() - pair.second().doubleValue() > SESSION_LIFETIME) {
        iter.remove();
        fireSessionEnded(uid);
      }

    }
  }

  protected void forceCleanup() {
    Iterator<ClientUID> iter = sessions.keySet().iterator();
    while (iter.hasNext()) {
      ClientUID uid = iter.next();
      iter.remove();
      fireSessionEnded(uid);
    }
  }

  private static class SessionData {

    private final String groupName;
    private final Map<String, Object> properties = new HashMap<>();
    private volatile boolean using;

    public SessionData(final String groupName) {
      this.groupName = groupName;
    }

    public void setUsing(final boolean b) {
      this.using = b;
    }

    public boolean isUsing() {
      return using;
    }

    public String getGroupName() {
      return groupName;
    }

    public Object getProperty(final String key) {
      return properties.get(key);
    }

    public void setProperty(final String key, final Object value) {
      properties.put(key, value);
    }

    public void removeProperty(final String key) {
      properties.remove(key);
    }
  }

  public static synchronized SessionsPool testInstance() {
    instance = new SessionsPool() {
      private String groupName = null;

      @Override
      public synchronized void startSession(final ClientUID client, final String user, final String group, final String ipAddress) {
        groupName = group;
      }

      @Override
      public synchronized String getCurrentSessionGroup(final ClientUID client) {
        return groupName;
      }
    };
    return instance;
  }
}

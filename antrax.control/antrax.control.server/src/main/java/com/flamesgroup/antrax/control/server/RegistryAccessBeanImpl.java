/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class RegistryAccessBeanImpl implements RegistryAccessBean {

  private static final Logger logger = LoggerFactory.getLogger(RegistryAccessBeanImpl.class);

  private final RemoteRegistryAccess baseRemoteRegistryAccess;

  public RegistryAccessBeanImpl(final DAOProvider daoProvider) {
    this.baseRemoteRegistryAccess = daoProvider.getRegistryDAO();
  }

  @Override
  public void add(final ClientUID clientUID, final String path, final String value, final int maxSize) throws TransactionException {
    try {
      this.baseRemoteRegistryAccess.add(path, value, maxSize);
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
    }
  }

  @Override
  public RegistryEntry[] listEntries(final ClientUID clientUID, final String path, final int maxSize) throws TransactionException {
    try {
      return baseRemoteRegistryAccess.listEntries(path, maxSize);
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
    }
    return new RegistryEntry[0];
  }

  @Override
  public RegistryEntry move(final ClientUID clientUID, final RegistryEntry entry, final String newPath)
      throws UnsyncRegistryEntryException, DataModificationException, TransactionException {
    try {
      return this.baseRemoteRegistryAccess.move(entry, newPath);
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
      return entry;
    }
  }

  @Override
  public void remove(final ClientUID clientUID, final RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException, TransactionException {
    try {
      this.baseRemoteRegistryAccess.remove(entry);
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
    }
  }

  @Override
  public String[] listAllPaths(final ClientUID clientUid) throws TransactionException {
    try {
      return baseRemoteRegistryAccess.listAllPaths();
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
    }
    return new String[0];
  }

  @Override
  public void removePath(final ClientUID clientUid, final String path) throws DataModificationException, TransactionException {
    try {
      this.baseRemoteRegistryAccess.removePath(path);
    } catch (RemoteException e) {
      logger.error("[{}] - can't get remote method", this, e);
    }
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return false;
  }

}

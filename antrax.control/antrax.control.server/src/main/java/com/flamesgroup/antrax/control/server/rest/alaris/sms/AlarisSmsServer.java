/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rest.alaris.sms;

import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.ControlBeanModel;
import com.flamesgroup.antrax.control.server.PingServersManager;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.UUID;

public class AlarisSmsServer {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsServer.class);

  private Server server;
  private final AlarisSmsService alarisSmsService;

  public AlarisSmsServer(final DAOProvider daoProvider, final ControlBeanModel controlBeanModel, final PingServersManager pingServersManager) {
    AlarisSmsManager.getInstance().setDaoProvider(daoProvider);
    alarisSmsService = new AlarisSmsService(controlBeanModel, pingServersManager, daoProvider);
    AlarisSmsManager.getInstance().setAlarisSmsService(alarisSmsService);
  }

  public void start() throws Exception {
    logger.info("[{}] - try to start AlarisSmsService", this);
    alarisSmsService.start();
    logger.info("[{}] - started AlarisSmsService", this);
    logger.info("[{}] - try to start AlarisSmsServer", this);
    ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath("/");

    if (!ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerUseSsl()) {
      server = new Server(new InetSocketAddress(ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerHostname(),
          ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerPort()));
      server.getConnectors()[0].getConnectionFactory(HttpConnectionFactory.class);
    } else {
      server = new Server();
      HttpConfiguration https = new HttpConfiguration();
      https.addCustomizer(new SecureRequestCustomizer());
      SslContextFactory sslContextFactory = new SslContextFactory();

      //Create SSL certificate
      KeyStore keyStore = KeyStore.getInstance("JKS");
      keyStore.load(null, null);
      CertAndKeyGen keyGen = new CertAndKeyGen("RSA", "SHA1WithRSA", null);
      keyGen.generate(1024);
      //Generate self signed certificate
      X509Certificate[] chain = new X509Certificate[1];
      chain[0] = keyGen.getSelfCertificate(new X500Name("CN=" + ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerHostname() + ",O=Antrax"), (long) 365 * 24 * 3600);
      String password = UUID.randomUUID().toString();
      keyStore.setKeyEntry("AlarisSms", keyGen.getPrivateKey(), password.toCharArray(), new Certificate[] {chain[0]});

      sslContextFactory.setKeyStore(keyStore);
      sslContextFactory.setKeyStorePassword(password);
      sslContextFactory.setKeyManagerPassword(password);

      ServerConnector sslConnector = new ServerConnector(server, new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(https));
      sslConnector.setPort(ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerPort());
      sslConnector.setHost(ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerHostname());

      server.setConnectors(new Connector[] {sslConnector});
    }
    server.setHandler(context);

    ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
    jerseyServlet.setInitOrder(0);

    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, AlarisSmsExceptionMapper.class.getCanonicalName());

    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, AuthFilter.class.getCanonicalName());

    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_PACKAGES, this.getClass().getPackage().getName());

    server.start();

    logger.info("[{}] - started AlarisSmsServer", this);
  }

  public void stop() throws Exception {
    logger.info("[{}] - try to stop AlarisSmsServer", this);
    if (server == null || server.isStopped()) {
      throw new IllegalStateException("AlarisSmsServer isn't started");
    }
    server.stop();
    server = null;
    logger.info("[{}] - stopped AlarisSmsServer", this);
    logger.info("[{}] - try to stop AlarisSmsService", this);
    alarisSmsService.stop();
    logger.info("[{}] - stopped AlarisSmsService", this);
  }

  public AlarisSmsService getAlarisSmsService() {
    return alarisSmsService;
  }

}

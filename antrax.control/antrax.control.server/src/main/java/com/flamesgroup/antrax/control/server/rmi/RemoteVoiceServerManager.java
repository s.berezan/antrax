/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rmi;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.voiceserver.IVoiceServerManager;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RemoteVoiceServerManager implements IVoiceServerManager {

  private final IVoiceServerManager delegate;

  public RemoteVoiceServerManager(final IVoiceServerManager rmiManager) {
    this.delegate = rmiManager;
  }

  @Override
  public void lockSIM(final ICCID simUID) {
    delegate.lockSIM(simUID);
  }

  @Override
  public void fireEvent(final ICCID simUID, final String event) throws EventException {
    delegate.fireEvent(simUID, event);
  }

  @Override
  public void sendUSSD(final ICCID simUID, final String ussd) throws USSDException {
    delegate.sendUSSD(simUID, ussd);
  }

  @Override
  public String startUSSDSession(final ICCID simUID, final String ussd) throws USSDException {
    return delegate.startUSSDSession(simUID, ussd);
  }

  @Override
  public String sendUSSDSessionCommand(final ICCID simUID, final String command) throws USSDException {
    return delegate.sendUSSDSessionCommand(simUID, command);
  }

  @Override
  public void endUSSDSession(final ICCID simUID) throws USSDException {
    delegate.endUSSDSession(simUID);
  }

  @Override
  public void sendSMS(final ICCID simUID, final String number, final String smsText) throws SMSException {
    delegate.sendSMS(simUID, number, smsText);
  }

  @Override
  public VsSmsStatus sendSMS(final List<AlarisSms> alarisSmses, final UUID objectUuid) {
    return delegate.sendSMS(alarisSmses, objectUuid);
  }

  @Override
  public VsSmsStatus getSMSStatus(final UUID objectUuid) {
    return delegate.getSMSStatus(objectUuid);
  }

  @Override
  public void removeSMSStatus(final UUID objectUuid) {
    delegate.removeSMSStatus(objectUuid);
  }

  @Override
  public void sendDTMF(final ICCID simUID, final String dtmf) throws DTMFException {
    delegate.sendDTMF(simUID, dtmf);
  }

  @Override
  public void unlockSIM(final ICCID simUID) {
    delegate.unlockSIM(simUID);
  }

  @Override
  public void disableCallChannel(final ICCID simUID) {
    delegate.disableCallChannel(simUID);
  }

  @Override
  public void enableCallChannel(final ICCID simUID) {
    delegate.enableCallChannel(simUID);
  }

  @Override
  public void updateAudioCaptureEnable(final boolean enable) {
    delegate.updateAudioCaptureEnable(enable);
  }

  @Override
  public void shutdown() {
    delegate.shutdown();
  }

  @Override
  public void lockGsmChannel(final ChannelUID channel, final boolean lock, final String lockReason) throws LockException {
    delegate.lockGsmChannel(channel, lock, lockReason);
  }

  @Override
  public void lockGsmChannelToArfcn(final ChannelUID channel, final int arfcn) throws LockArfcnException {
    delegate.lockGsmChannelToArfcn(channel, arfcn);
  }

  @Override
  public void unLockGsmChannelToArfcn(final ChannelUID channel) throws LockArfcnException {
    delegate.unLockGsmChannelToArfcn(channel);
  }

  @Override
  public void executeNetworkSurvey(final ChannelUID channel) throws NetworkSurveyException {
    delegate.executeNetworkSurvey(channel);
  }

  @Override
  public void resetStatistic() {
    delegate.resetStatistic();
  }

  @Override
  public List<AudioFile> getAudioFiles() {
    return delegate.getAudioFiles();
  }

  @Override
  public byte[] downloadAudioFileFromServer(final String fileName) {
    return delegate.downloadAudioFileFromServer(fileName);
  }

  @Override
  public List<String> getIvrTemplatesSimGroups() {
    return delegate.getIvrTemplatesSimGroups();
  }

  @Override
  public List<IvrTemplateWrapper> getIvrTemplates(final String simGroup) {
    return delegate.getIvrTemplates(simGroup);
  }

  @Override
  public void createIvrTemplateSimGroup(final String simGroup) throws IvrTemplateException {
    delegate.createIvrTemplateSimGroup(simGroup);
  }

  @Override
  public void removeIvrTemplateSimGroups(final List<String> simGroups) throws IvrTemplateException {
    delegate.removeIvrTemplateSimGroups(simGroups);
  }

  @Override
  public Map<String, Integer> getCallDropReasons() {
    return delegate.getCallDropReasons();
  }

  @Override
  public void uploadIvrTemplate(final String simGroup, final byte[] bytes, final String fileName) throws IvrTemplateException {
    delegate.uploadIvrTemplate(simGroup, bytes, fileName);
  }

  @Override
  public void removeIvrTemplate(final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException {
    delegate.removeIvrTemplate(simGroup, ivrTemplates);
  }
}

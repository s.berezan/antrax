/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

public class SimServerStatus implements ISimServerStatus {

  private static final long serialVersionUID = 4969813380197516512L;

  private final ISimServerShortInfo shortInfo;

  public SimServerStatus(final ISimServerShortInfo shortInfo) {
    this.shortInfo = shortInfo;
  }

  @Override
  public ISimServerShortInfo getShortInfo() {
    return shortInfo;
  }

}

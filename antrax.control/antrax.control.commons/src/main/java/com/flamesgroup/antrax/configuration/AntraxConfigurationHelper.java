/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.configuration;

import static com.flamesgroup.rmi.RemoteProxyFactory.create;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.control.communication.IActivityRemoteLogger;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISimServerManager;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.rmi.RemoteAccessor;
import com.flamesgroup.utils.AntraxProperties;

public class AntraxConfigurationHelper {

  private static volatile AntraxConfigurationHelper INSTANCE;

  private volatile SysCfgBean sysConfigBean;

  public IServerData getMyServer() throws Exception {
    return getSysConfigBean().getServer(AntraxProperties.SERVER_NAME);
  }

  public long getControlServerCurrentTime() {
    return getSysConfigBean().getControlServerCurrentTime();
  }

  public IPingServerManager getPingServerManager() {
    return getSysConfigBean().getPingServerManager();
  }

  public IActionExecutionManager getActionExecutionManager() {
    return getSysConfigBean().getActionExecutionManager();
  }

  public IRemoteHistoryLogger getHistoryLogger() {
    return getSysConfigBean().getHistoryLogger();
  }

  public IRemoteVSConfigurator getRemoteVSConfigurator() {
    return getSysConfigBean().getRemoteVSConfigurator();
  }

  public RemoteRegistryAccess getRemoteRegistryAccess() {
    return getSysConfigBean().getRemoteRegistryAccess();
  }

  public ISimCallHistory getSimCallHistory() {
    return getSysConfigBean().getSimCallHistory();
  }
  
  public ISmsManager getSmsManager() {
    return getSysConfigBean().getSmsManager();
  }

  public IGsmViewManager getGsmViewManager() {
    return getSysConfigBean().getGsmViewManager();
  }

  public ISimServerManager getSimServerManager() {
    return getSysConfigBean().getSimServerManager();
  }

  public ISimUnitManager getSimUnitManager() {
    return getSysConfigBean().getSimUnitManager();
  }

  public IGsmChannelManagerHandler getGsmChannelManagerHandler() {
    return getSysConfigBean().getGsmChannelManagerHandler();
  }

  public IGsmUnitManger getGsmUnitManager() {
    return getSysConfigBean().getGsmUnitManager();
  }

  public IActivityRemoteLogger getActivityRemoteLogger() {
    return getSysConfigBean().getActivityRemoteLogger();
  }

  private SysCfgBean getSysConfigBean() {
    if (sysConfigBean == null) {
      synchronized (this) {
        if (sysConfigBean == null) {
          sysConfigBean = create(new RemoteAccessor.Builder<>(AntraxProperties.CONTROL_SERVER_HOST_NAME, AntraxProperties.CONTROL_SERVER_RMI_PORT, SysCfgBean.class).build());
        }
      }
    }
    return sysConfigBean;
  }

  private AntraxConfigurationHelper() {
  }

  public static AntraxConfigurationHelper getInstance() {
    if (INSTANCE == null) {
      synchronized (AntraxConfigurationHelper.class) {
        if (INSTANCE == null) {
          INSTANCE = new AntraxConfigurationHelper();
        }
      }
    }
    return INSTANCE;
  }

}

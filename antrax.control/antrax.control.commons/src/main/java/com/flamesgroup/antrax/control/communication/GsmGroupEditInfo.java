/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.io.Serializable;
import java.util.Collection;

public interface GsmGroupEditInfo extends Serializable {

  long getID();

  GSMGroup getGsmGroup();

  Collection<SIMGroup> getLinkedSimGroups();

  void addLinkedSimGroup(SIMGroup simGroup);

  void removeLinkedSimGroup(SIMGroup simGroup);

  void setLinkedSimGroups(Collection<SIMGroup> simGroups);

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.SIMEvent;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.ICCID;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class SimEvent implements Serializable {

  private static final long serialVersionUID = 6731984639028818535L;

  private final SIMEvent event;
  private final Object[] args;

  private final Date date = new Date();

  private ICCID sim;

  private SimEvent(final SIMEvent event, final Object... args) {
    this.event = event;
    this.args = args;
  }

  public void setSim(final ICCID sim) {
    this.sim = sim;
  }

  public Date getDate() {
    return date;
  }

  public ICCID getSim() {
    return sim;
  }

  public static SimEvent callEnded(final CDR cdr) {
    SIMEvent event = cdr.getCallType() == CallType.GSM_TO_VS ? SIMEvent.INCOMING_CALL_ENDED : SIMEvent.OUTGOING_CALL_ENDED;
    return new SimEvent(event, cdr);
  }

  public SIMEvent getEventType() {
    return event;
  }

  public Object getArg(final int index) {
    return args[index];
  }

  public void setArg(final int index, final Object data) {
    this.args[index] = data;
  }

  public String[] getArgs() {
    String[] retval = new String[args.length];
    for (int i = 0; i < retval.length; ++i) {
      retval[i] = String.valueOf(args[i]);
    }
    return retval;
  }

  public static SimEvent ussd(final String ussd, final String response) {
    return new SimEvent(SIMEvent.USSD, ussd, response);
  }

  public static SimEvent ussdSessionRequest(final String request) {
    return new SimEvent(SIMEvent.USSD_SESSION_REQUEST, request);
  }

  public static SimEvent ussdSessionResponse(final String response) {
    return new SimEvent(SIMEvent.USSD_SESSION_RESPONSE, response);
  }

  public static SimEvent smsSent(final String number, final String text, final int parts, final UUID smsId) {
    return new SimEvent(SIMEvent.SMS_SENT, number, text, parts, smsId);
  }

  public static SimEvent smsReceived(final String number, final String text, final Date date, final int parts) {
    return new SimEvent(SIMEvent.SMS_RECIEVED, number, text, new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(date), parts);
  }

  public static SimEvent allowedInternetChanged(final ICCID uid, final boolean allowed) {
    SimEvent simEvent = new SimEvent(SIMEvent.ALLOWED_INTERNET, allowed);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent smsStatus(final String number, final String status, final int part, final int parts, final UUID smsId) {
    return new SimEvent(SIMEvent.SMS_STATUS, number, status, part, parts, smsId.toString());
  }

  public static SimEvent businessActivityStopped() {
    return new SimEvent(SIMEvent.BUSINESS_ACTIVITY_STOPED);
  }

  public static SimEvent businessActivityStarted(final String description) {
    return new SimEvent(SIMEvent.BUSINESS_ACTIVITY_STARTED, description);
  }

  public static SimEvent automationActionExecutionStarted(final Action action, final UUID executionID) {
    return new SimEvent(SIMEvent.AUTOMATION_ACTION_EXECUTION_STARTED, action, executionID);
  }

  public static SimEvent automationActionExecutionStopped() {
    return new SimEvent(SIMEvent.AUTOMATION_ACTION_EXECUTION_STOPED);
  }

  public static SimEvent automationActionExecutionFailed(final String msg) {
    return new SimEvent(SIMEvent.AUTOMATION_ACTION_EXECUTION_FAILED, msg);
  }

  public static SimEvent automationActionFailed(final Action action, final UUID exec, final String msg) {
    return new SimEvent(SIMEvent.ACTION_FAILED, action, exec, msg);
  }

  public static SimEvent automationActionDone(final Action action, final UUID exec) {
    return new SimEvent(SIMEvent.ACTION_DONE, action, exec);
  }

  public static SimEvent imeiSetupped(final IMEI imei) {
    return new SimEvent(SIMEvent.IMEI_SETUPED, imei.getValue());
  }

  public static SimEvent callChannelBuildStarted(final ChannelUID gsmChannel, final ChannelUID simChannel) {
    return new SimEvent(SIMEvent.CALL_CHANNEL_BUILD, gsmChannel.getCanonicalName(), simChannel.getCanonicalName());
  }

  public static SimEvent callChannelReleased() {
    return new SimEvent(SIMEvent.CALL_CHANNEL_RELEASED);
  }

  public static SimEvent gsmRegistration(final ICCID uid, final int attemptNumber) {
    SimEvent retval = new SimEvent(SIMEvent.GSM_REGISTRATION, attemptNumber);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent gsmRegistered(final ICCID uid, final String simGroupName, final String gsmGroupName) {
    SimEvent retval = new SimEvent(SIMEvent.GSM_REGISTERED, simGroupName, gsmGroupName);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent gsmNotRegistered(final ICCID uid, final String reason) {
    SimEvent retval = new SimEvent(SIMEvent.GSM_NOT_REGISTERED, reason);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent selfCallExpectation() {
    return new SimEvent(SIMEvent.SELF_CALL_EXPECTATION);
  }

  public static SimEvent simCardLocked(final String reason) {
    return new SimEvent(SIMEvent.SIM_LOCKED, reason);
  }

  public static SimEvent simCardLocked(final ICCID uid, final String reason) {
    SimEvent simEvent = new SimEvent(SIMEvent.SIM_LOCKED, reason);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent genericEvent(final ICCID sim, final String event, final Serializable... args) {
    SimEvent retval;
    if (args.length == 0) {
      retval = new SimEvent(SIMEvent.GENERIC_EVENT, event);
    } else if (args.length == 1) {
      retval = new SimEvent(SIMEvent.GENERIC_EVENT, event, args[0]);
    } else {
      retval = new SimEvent(SIMEvent.GENERIC_EVENT, event, args[0], args[1]);
    }
    retval.setSim(sim);
    return retval;
  }

  public static SimEvent simCardTaken(final ICCID uid, final IServerData gateway, final GSMGroup gsmGroup) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_TAKEN, gateway, gsmGroup);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent simFound(final ICCID uid, final ChannelUID channelUID) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_FOUND, channelUID.getCanonicalName());
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent simAdded(final ICCID uid) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_ADDED);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent imeiGenerated(final ICCID uid, final IMEI imei) {
    SimEvent retval = new SimEvent(SIMEvent.IMEI_GENERATED, imei.getValue());
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent simLost(final ICCID uid) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_LOST);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent simReturned(final ICCID uid, final IServerData currentGateway, final GSMGroup currentGSMGroup) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_RETURNED, currentGateway, currentGSMGroup);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent gsmUnregistered(final ICCID uid, final String reason) {
    SimEvent retval = new SimEvent(SIMEvent.GSM_UNREGISTERED, reason);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent simStatusChanged(final ICCID uid, final String status) {
    SimEvent retval = new SimEvent(SIMEvent.SIM_STATUS_CHANGED, status);
    retval.setSim(uid);
    return retval;
  }

  public static SimEvent servingCellChanged(final CellInfo cell) {
    return new SimEvent(SIMEvent.SERVING_CELL_CHANGED, String.format("LAC: %d, cellID: %d, BSIC: %d, ARFCN: %d", cell.getLac(), cell.getCellId(), cell.getBsic(), cell.getArfcn()));
  }

  public static SimEvent networkSurvey(final String message) {
    return new SimEvent(SIMEvent.NETWORK_SURVEY, message);
  }

  public static SimEvent httpRequest(final String message) {
    return new SimEvent(SIMEvent.HTTP_REQUEST, message);
  }

  public static SimEvent resetIMEI(final ICCID uid, final String message) {
    SimEvent simEvent = new SimEvent(SIMEvent.RESET_IMEI, message);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent changeSimGroup(final ICCID uid, final String[] args) {
    SimEvent simEvent = new SimEvent(SIMEvent.SET_GROUP, args);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent resetScripts(final ICCID uid) {
    SimEvent simEvent = new SimEvent(SIMEvent.RESET_SCRIPTS);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent resetStatistic(final ICCID uid) {
    SimEvent simEvent = new SimEvent(SIMEvent.RESET_STATISTIC);
    simEvent.setSim(uid);
    return simEvent;
  }

  public static SimEvent setTariffPlanEndDate(final ICCID uid, final long endDate) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd-MM-yyyy HH:mm:ss");
    SimEvent simEvent = new SimEvent(SIMEvent.SET_TARIFF_PLAN_END_DATE, dateFormat.format(endDate));
    simEvent.setSim(uid);
    return simEvent;
  }

  @Override
  public String toString() {
    return String.format("%s for %s at %s", getEventType(), getSim(), getDate());
  }

}

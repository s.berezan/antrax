/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.commons;

// TODO: check to refactor or remove
public abstract class BaseEngine extends Thread {

  protected abstract void invokeRoutine() throws Exception;

  protected abstract long countSleepTime();

  protected Object getMutex() {
    return this;
  }

  protected void initialRoutine() throws Exception {
  }

  protected void finalRoutine() {
  }

  private volatile boolean terminated = false;

  public BaseEngine(final String name) {
    super(name);
  }

  @Override
  public final void run() {
    try {
      initialRoutine();
      while (!interrupted() && !terminated) {
        invokeRoutine();
        long sleepTime = countSleepTime();
        if (sleepTime == 0) {
          continue;
        }
        synchronized (getMutex()) {
          try {
            if (sleepTime == Long.MAX_VALUE) {
              getMutex().wait();
            } else {
              getMutex().wait(sleepTime);
            }
          } catch (InterruptedException ignored) {
            break;
          }
        }
      }
    } catch (Throwable e) {
      throw new IllegalStateException(e);
    } finally {
      finalRoutine();
    }
  }

  public final void terminate() {
    terminated = true;
    interrupt();
  }

}

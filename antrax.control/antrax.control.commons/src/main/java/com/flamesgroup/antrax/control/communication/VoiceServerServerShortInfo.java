/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.util.concurrent.atomic.AtomicInteger;

public class VoiceServerServerShortInfo implements IVoiceServerShortInfo {

  private static final long serialVersionUID = -7127223401290989538L;

  private final AtomicInteger freeGSMChannels = new AtomicInteger();
  private final AtomicInteger outgoingCalls = new AtomicInteger();

  private ServerStatus status = ServerStatus.NO_CONNECTION;

  @Override
  public int getFreeGSMChannels() {
    return freeGSMChannels.get();
  }

  @Override
  public int getOutgoingCalls() {
    return outgoingCalls.get();
  }

  @Override
  public ServerStatus getStatus() {
    return status;
  }

  public void setStatus(final ServerStatus status) {
    this.status = status;
  }

  public void incFreeGSMChannels() {
    freeGSMChannels.getAndIncrement();
  }

  public void decFreeGSMChannels() {
    freeGSMChannels.getAndDecrement();
  }

  public void incOutgoingCalls() {
    outgoingCalls.getAndIncrement();
  }

  public void decOutgoingCalls() {
    outgoingCalls.getAndDecrement();
  }

  @Override
  public String report() {
    return String.format("Status: %s Free GSM channels: %d Outgoing Calls: %d%n", status, freeGSMChannels.get(), outgoingCalls.get());
  }

}

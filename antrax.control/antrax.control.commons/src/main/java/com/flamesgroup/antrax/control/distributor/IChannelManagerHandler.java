/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.distributor;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.ChannelUID;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IChannelManagerHandler extends Remote {

  void handleLostChannels(String serverName, List<ChannelUID> channels) throws RemoteException;

  void handleDeclaredChannels(String serverName, List<ChannelUID> channels) throws RemoteException;

  void handleEmptyChannel(String serverName, ChannelUID channel, ChannelConfig channelConfig) throws RemoteException;

  void handleInvalidChannel(String serverName, ChannelUID channel, ChannelConfig channelConfig) throws RemoteException;

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;
import java.util.Objects;

public class SmsHistory implements Serializable {

  private static final long serialVersionUID = -9204772464944195970L;

  private final String iccid;
  private final int amountForCertainTime;
  private final int numberOfAllTime;

  public SmsHistory(final String iccid, final int amountForCertainTime, final int numberOfAllTime) {
    this.iccid = iccid;
    this.amountForCertainTime = amountForCertainTime;
    this.numberOfAllTime = numberOfAllTime;
  }

  public String getIccid() {
    return iccid;
  }

  public int getAmountForCertainTime() {
    return amountForCertainTime;
  }

  public int getNumberOfAllTime() {
    return numberOfAllTime;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SmsHistory)) {
      return false;
    }
    final SmsHistory that = (SmsHistory) object;

    return amountForCertainTime == that.amountForCertainTime
        && numberOfAllTime == that.numberOfAllTime
        && Objects.equals(iccid, that.iccid);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(iccid);
    result = prime * result + amountForCertainTime;
    result = prime * result + numberOfAllTime;
    return result;
  }

}

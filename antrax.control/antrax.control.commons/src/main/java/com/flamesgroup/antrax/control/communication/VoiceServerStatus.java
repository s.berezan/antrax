/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.util.List;

public class VoiceServerStatus implements IVoiceServerStatus {

  private static final long serialVersionUID = 4969813380197516512L;

  private final IVoiceServerShortInfo shortInfo;
  private final List<IVoiceServerChannelsInfo> channelsInfos;
  private final int rmiRegistryPort;

  public VoiceServerStatus(final IVoiceServerShortInfo shortInfo, final List<IVoiceServerChannelsInfo> channelsInfos, final int rmiRegistryPort) {
    this.shortInfo = shortInfo;
    this.channelsInfos = channelsInfos;
    this.rmiRegistryPort = rmiRegistryPort;
  }

  @Override
  public IVoiceServerShortInfo getShortInfo() {
    return shortInfo;
  }

  @Override
  public List<IVoiceServerChannelsInfo> getChannelsInfo() {
    return channelsInfos;
  }

  @Override
  public int getRmiRegistryPort() {
    return rmiRegistryPort;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GsmGroupEditInfoImpl implements GsmGroupEditInfo {

  private static final long serialVersionUID = 4126263577894147269L;

  private final GSMGroup gsmGroup;
  private List<SIMGroup> linkedSimGroups;

  public GsmGroupEditInfoImpl(final GSMGroup gsmGroup) {
    this.gsmGroup = gsmGroup;
    this.linkedSimGroups = new ArrayList<>(0);
  }

  public GsmGroupEditInfoImpl(final GSMGroup gsmGroup, final LinkWithSIMGroup[] links) {
    this(gsmGroup);
    linkedSimGroups = new ArrayList<>(links.length);
    for (LinkWithSIMGroup link : links) {
      addLinkedSimGroup(link.getSIMGroup());
    }
  }

  @Override
  public long getID() {
    return gsmGroup.getID();
  }

  @Override
  public GSMGroup getGsmGroup() {
    return gsmGroup;
  }

  @Override
  public Collection<SIMGroup> getLinkedSimGroups() {
    return linkedSimGroups;
  }

  @Override
  public void addLinkedSimGroup(final SIMGroup simGroup) {
    linkedSimGroups.add(simGroup);
  }

  @Override
  public void removeLinkedSimGroup(final SIMGroup simGroup) {
    linkedSimGroups.remove(simGroup);
  }

  @Override
  public void setLinkedSimGroups(final Collection<SIMGroup> simGroups) {
    linkedSimGroups.clear();
    linkedSimGroups.addAll(simGroups);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[gsmGroup:" + gsmGroup +
        " linkedSimGroups:" + linkedSimGroups +
        ']';
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IRemoteVSConfigurator extends Remote {

  void saveScript(ICCID uid, ScriptType scriptType, byte[] script) throws RemoteException;

  long getLatestScriptsRevision() throws RemoteException;

  ScriptFile getLastScriptFile() throws RemoteException;

  long getLatestGSMToSIMGroupLinksRevision() throws RemoteException;

  LinkWithSIMGroup[] listSIMGSMLinks() throws RemoteException;

  byte[] getBusinessActivityScript(ICCID iccid) throws RemoteException;

  byte[] getActionProviderScript(ICCID iccid) throws RemoteException;

  byte[] getIncomingCallManagementScript(ICCID iccid) throws RemoteException;

  byte[] getCallFilterScript(ICCID iccid) throws RemoteException;

  byte[] getSmsFilterScript(ICCID iccid) throws RemoteException;

  byte[] getVSFactorScript(ICCID iccid) throws RemoteException;

  GSMChannel getGSMChannel(ChannelUID channelUID) throws RemoteException;

  void updateResetVsScriptsFlag(ICCID iccid, boolean flag) throws RemoteException;

  boolean shouldResetVsScripts(ICCID iccid) throws RemoteException;

  boolean isAudioCaptureEnabled(String serverName) throws RemoteException;

}

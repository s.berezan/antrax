/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.CellKey;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Map;

public interface IVoiceServerChannelsInfo extends Serializable {

  MobileGatewayChannelInformation getMobileGatewayChannelInfo();

  Map<CellKey, CellInfoStatistic> getCellStatistic();

  SimChannelInformation getSimChannelInfo();

  CallChannelState getChannelState();

  boolean isRegistered();

  String getStateAdvInfo();

  long getCallsDuration();

  int getSuccessfulCallsCount();

  int getTotalCallsCount();

  int getSuccessOutgoingSmsCount();

  int getTotalOutgoingSmsCount();

  CallState getCallState();

  void report(PrintStream out);

  String report();

  long getServerTimeMillis();

}

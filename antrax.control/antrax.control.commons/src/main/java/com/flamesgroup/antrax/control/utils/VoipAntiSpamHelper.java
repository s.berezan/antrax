/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.utils;

import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.BlackListConfig;
import com.flamesgroup.commons.voipantispam.FasConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;

import java.util.HashSet;
import java.util.Set;

public final class VoipAntiSpamHelper {

  private VoipAntiSpamHelper() {
  }

  public static Set<String> parseNumbersFromString(final String in) {
    StringBuilder builder = new StringBuilder();
    Set<String> numbers = new HashSet<>();
    int codeChar;
    for (int i = 0; i < in.length(); i++) {
      codeChar = in.charAt(i);
      if (Character.isDigit(codeChar) || codeChar == '#') {
        builder.append((char) codeChar);
        if (i != in.length() - 1) {
          continue;
        }
      }

      if (builder.length() > 0) {
        numbers.add(builder.toString());
        builder = new StringBuilder();
      }
    }
    return numbers;
  }

  public static String createDescriptionForACDFilter(final AcdConfig acdConfig) {
    return String.format("Acd period:%d; Acd max min acd call per period:%d; Acd min acd:%d",
        acdConfig.getPeriod(), acdConfig.getMaxMinAcdCallPerPeriod(), acdConfig.getMinAcd());
  }

  public static String createDescriptionForACDBlockCountFilter(final AcdConfig acdConfig, final int maxBlockCountBeforeMoveToBlackList) {
    return String.format("Acd period:%d; Acd max min acd call per period:%d; Acd min acd:%d; Acd max block count before move to black list:%d",
        acdConfig.getPeriod(), acdConfig.getMaxMinAcdCallPerPeriod(), acdConfig.getMinAcd(), maxBlockCountBeforeMoveToBlackList);
  }

  public static String createDescriptionForGsmAlertingBlockCountFilter(final GsmAlertingConfig gsmAlertingConfig) {
    return String.format("GSM Alerting time:%d; GSM Alerting routing period:%d; GSM Alerting max routing requests per period:%d;",
        gsmAlertingConfig.getAlertingTime(), gsmAlertingConfig.getPeriod(), gsmAlertingConfig.getMaxRoutingRequestPerPeriod());
  }

  public static String createDescriptionForVoipAlertingBlockCountFilter(final VoipAlertingConfig voipAlertingConfig) {
    return String.format("VOIP Alerting time:%d; VOIP Alerting routing period:%d; VOIP Alerting max routing requests per period:%d;",
        voipAlertingConfig.getAlertingTime(), voipAlertingConfig.getPeriod(), voipAlertingConfig.getMaxRoutingRequestPerPeriod());
  }

  public static String createDescriptionForFasBlockCountFilter(final FasConfig fasConfig) {
    return String.format("Fas routing period:%d; Fas max routing requests per period:%d;",
        fasConfig.getPeriod(), fasConfig.getMaxRoutingRequestPerPeriod());
  }

  public static String createDescriptionForGrayListRoutingFilter(final GrayListConfig grayListConfig) {
    return String.format("Gray list period:%d; Gray list max routing requests per period:%d; Gray list block period:%d",
        grayListConfig.getPeriod(), grayListConfig.getMaxRoutingRequestPerPeriod(), grayListConfig.getBlockPeriod());
  }

  public static String createDescriptionForBlackListBlockCountFilter(final GrayListConfig grayListConfig) {
    return String.format("Gray list period:%d; Black list max block count before move to black list:%d",
        grayListConfig.getPeriod(), grayListConfig.getMaxBlockCountBeforeMoveToBlackList());
  }

  public static String createDescriptionForBlackListRoutingFilter(final BlackListConfig blackListConfig) {
    return String.format("Black list period:%d; Black list max routing requests per period:%d",
        blackListConfig.getPeriod(), blackListConfig.getMaxRoutingRequestPerPeriod());
  }

}

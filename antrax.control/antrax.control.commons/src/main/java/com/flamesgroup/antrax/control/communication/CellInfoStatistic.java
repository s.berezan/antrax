/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;

public class CellInfoStatistic implements Serializable {

  private static final long serialVersionUID = 2447584411146869860L;

  private int successfulCallsCount;
  private int totalCallsCount;
  private long callsDuration;
  private int successOutgoingSmsCount;
  private int totalOutgoingSmsCount;
  private int incomingSmsCount;
  private int ussdCount;
  private long amountPdd;

  public CellInfoStatistic() {
  }

  public CellInfoStatistic(final int successfulCallsCount, final int totalCallsCount, final long callsDuration, final int successOutgoingSmsCount, final int incomingSmsCount, final int ussdCount,
      final long amountPdd) {
    this.successfulCallsCount = successfulCallsCount;
    this.totalCallsCount = totalCallsCount;
    this.callsDuration = callsDuration;
    this.successOutgoingSmsCount = successOutgoingSmsCount;
    this.incomingSmsCount = incomingSmsCount;
    this.ussdCount = ussdCount;
    this.amountPdd = amountPdd;
  }

  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  public CellInfoStatistic incSuccessfulCallsCount() {
    this.successfulCallsCount++;
    return this;
  }

  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  public CellInfoStatistic incTotalCallsCount() {
    this.totalCallsCount++;
    return this;
  }

  public long getCallsDuration() {
    return callsDuration;
  }

  public CellInfoStatistic incCallsDuration(final long callsDuration) {
    this.callsDuration += callsDuration;
    return this;
  }

  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  public CellInfoStatistic incSuccessOutgoingSmsCount(final int successOutgoingSmsCount) {
    this.successOutgoingSmsCount += successOutgoingSmsCount;
    return this;
  }

  public int getTotalOutgoingSmsCount() {
    return totalOutgoingSmsCount;
  }

  public CellInfoStatistic incTotalOutgoingSmsCount(final int totalOutgoingSmsCount) {
    this.totalOutgoingSmsCount += totalOutgoingSmsCount;
    return this;
  }

  public int getIncomingSmsCount() {
    return incomingSmsCount;
  }

  public CellInfoStatistic incIncomingSmsCount(final int incomingSmsCount) {
    this.incomingSmsCount += incomingSmsCount;
    return this;
  }

  public int getUssdCount() {
    return ussdCount;
  }

  public CellInfoStatistic incUssdCount() {
    this.ussdCount++;
    return this;
  }

  public long getAmountPdd() {
    return amountPdd;
  }

  public CellInfoStatistic incAmountPdd(final long averagePdd) {
    this.amountPdd += averagePdd;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellInfoStatistic)) {
      return false;
    }
    final CellInfoStatistic that = (CellInfoStatistic) object;

    return successfulCallsCount == that.successfulCallsCount
        && totalCallsCount == that.totalCallsCount
        && callsDuration == that.callsDuration
        && successOutgoingSmsCount == that.successOutgoingSmsCount
        && totalOutgoingSmsCount == that.totalOutgoingSmsCount
        && incomingSmsCount == that.incomingSmsCount
        && ussdCount == that.ussdCount
        && amountPdd == that.amountPdd;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = successfulCallsCount;
    result = prime * result + totalCallsCount;
    result = prime * result + Long.hashCode(callsDuration);
    result = prime * result + successOutgoingSmsCount;
    result = prime * result + totalOutgoingSmsCount;
    result = prime * result + incomingSmsCount;
    result = prime * result + ussdCount;
    result = prime * result + Long.hashCode(amountPdd);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[successfulCallsCount:" + successfulCallsCount +
        " totalCallsCount:" + totalCallsCount +
        " callsDuration:" + callsDuration +
        " successOutgoingSmsCount:" + successOutgoingSmsCount +
        " totalOutgoingSmsCount:" + totalOutgoingSmsCount +
        " incomingSmsCount:" + incomingSmsCount +
        " ussdCount:" + ussdCount +
        " amountPdd:" + amountPdd +
        ']';
  }

}

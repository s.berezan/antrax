/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.commons;

import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.control.utils.PrepareBlockingQueue;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.unit.ICCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ScriptStateSaver {

  private static final Logger logger = LoggerFactory.getLogger(ScriptStateSaver.class);

  private final IRemoteVSConfigurator remoteVSConfigurator;

  private ThreadPoolExecutor scriptSaveExecutor;

  public ScriptStateSaver(final IRemoteVSConfigurator remoteVSConfigurator) {
    this.remoteVSConfigurator = remoteVSConfigurator;
  }

  public void start() {
    scriptSaveExecutor = new ThreadPoolExecutor(6, 54, 1, TimeUnit.MINUTES, new DistinctBlockingQueue(),
        new ThreadFactory() {

          private final AtomicInteger threadNumber = new AtomicInteger(1);
          private final String namePrefix = "ScriptStatesSaver:thread-";

          @Override
          public Thread newThread(final Runnable r) {
            return new Thread(r, namePrefix + threadNumber.getAndIncrement());
          }
        },
        new RejectedExecutionHandler() {

          @Override
          public void rejectedExecution(final Runnable r, final ThreadPoolExecutor executor) {
            logger.warn("[{}] - rejected execution", r);
          }
        }
    );
  }

  public void saveScript(final ICCID simUID, final ScriptType scriptType, final StatefullScript[] scripts) {
    if (scripts.length > 0) {
      scriptSaveExecutor.execute(new ScriptSaveTask(simUID, scriptType, scripts));
    }
  }

  public void terminate() {
    if (scriptSaveExecutor != null) {
      scriptSaveExecutor.shutdown();
    }
  }

  private class ScriptSaveTask implements Runnable, Comparable<ScriptSaveTask> {

    private final ICCID simUID;
    private final ScriptType scriptType;
    private final StatefullScript[] scripts;

    public ScriptSaveTask(final ICCID simUID, final ScriptType scriptType, final StatefullScript[] scripts) {
      this.scriptType = scriptType;
      this.scripts = scripts;
      this.simUID = simUID;
    }

    @Override
    public void run() {
      try {
        remoteVSConfigurator.saveScript(simUID, scriptType, serialize(scripts));
      } catch (IOException e) {
        logger.warn("[{}] - while serialize scripts for {}", this, scriptType, e);
      }
    }

    @Override
    public int compareTo(final ScriptSaveTask o) {
      int result = simUID.getValue().compareTo(o.simUID.getValue());
      if (result == 0) {
        result = scriptType.compareTo(o.scriptType);
      }
      return result;
    }

    @Override
    public String toString() {
      return "ScriptSaveTask[" + simUID + ", " + scriptType + "]";
    }

    private byte[] serialize(final StatefullScript[] scripts) throws IOException {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
      objectOutputStream.writeObject(scripts);
      objectOutputStream.close();
      return out.toByteArray();
    }

  }

  private class DistinctBlockingQueue extends PrepareBlockingQueue<Runnable> {

    @Override
    protected void prepareOffer(final Runnable e) {
      ScriptSaveTask scriptSaveTask = (ScriptSaveTask) e;
      Iterator<Runnable> iterator = this.iterator();
      while (iterator.hasNext()) {
        if (scriptSaveTask.compareTo((ScriptSaveTask) iterator.next()) == 0) {
          iterator.remove();
          break;
        }
      }
    }

  }

}

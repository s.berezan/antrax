/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.dao.IAlarisSmsDAO;
import com.flamesgroup.antrax.storage.dao.IReceivedSmsDAO;
import com.flamesgroup.antrax.storage.dao.ISentSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SmsManager implements ISmsManager {

  private static final Logger logger = LoggerFactory.getLogger(SmsManager.class);

  private final IReceivedSmsDAO receivedSmsDAO;
  private final ISentSmsDAO sentSmsDAO;
  private final IAlarisSmsDAO alarisSmsDAO;

  public SmsManager(final IReceivedSmsDAO receivedSmsDAO, final ISentSmsDAO sentSmsDAO, final IAlarisSmsDAO alarisSmsDAO) {
    this.receivedSmsDAO = receivedSmsDAO;
    this.sentSmsDAO = sentSmsDAO;
    this.alarisSmsDAO = alarisSmsDAO;
  }

  @Override
  public void saveReceivedSms(final ICCID iccid, final SMSMessageInbound smsMessageInbound) throws RemoteException {
    receivedSmsDAO.saveReceivedSms(iccid, smsMessageInbound);
  }

  @Override
  public UUID updateReceivedSmsIdForSms(final ICCID iccid, final String senderPhoneNumber, final int referenceNumber) throws RemoteException {
    return receivedSmsDAO.updateSmsIdForSms(iccid, senderPhoneNumber, referenceNumber);
  }

  @Override
  public boolean isReceivedAllMultiPartSms(final ICCID iccid, final String senderPhoneNumber, final int referenceNumber, final int countOfSmsParts)
      throws RemoteException {
    return countOfSmsParts == receivedSmsDAO.getCountOfStoredSms(iccid, senderPhoneNumber, referenceNumber);
  }

  @Override
  public Pair<String, Date> getFullReceivedSmsAndDate(final UUID smsId) throws RemoteException {
    return smsId == null ? null : receivedSmsDAO.getFullSmsBySimId(smsId);
  }

  @Override
  public void saveMultiPartOutboundSms(final ICCID iccid, final UUID smsId, final List<SMSMessageOutbound> smsMessageOutbounds) throws RemoteException {
    sentSmsDAO.saveMultiPartOutboundSms(iccid, smsId, smsMessageOutbounds);
  }

  @Override
  public Pair<UUID, SMSMessageOutbound> saveSmsStatusReport(final ICCID iccid, final SMSStatusReport smsStatusReport) throws RemoteException {
    Pair<UUID, SMSMessageOutbound> sms = sentSmsDAO.saveSmsStatusReport(iccid, smsStatusReport);
    if (sms == null) {
      return null;
    }
    UUID smsId = sms.first();
    SMSMessageOutbound smsMessageOutbound = sms.second();
    AlarisSms alarisSms = alarisSmsDAO.getBySmsIdAndPartNumber(smsId, smsMessageOutbound.getNumberOfSmsPart());
    if (alarisSms == null) {
      return sms;
    }
    alarisSms.setLastUpdateTime(new Date());
    switch (smsStatusReport.getStatus()) {
      case UNKNOWN:
        alarisSms.setStatus(AlarisSms.Status.UNKNOWN);
        break;
      case INPROGRESS:
      case KEEPTRYING:
        alarisSms.setStatus(AlarisSms.Status.ACCEPTD);
        break;
      case DELIVERED:
        alarisSms.setStatus(AlarisSms.Status.DELIVRD);
        break;
      case ABORTED:
        alarisSms.setStatus(AlarisSms.Status.REJECTD);
        break;
      default:
        alarisSms.setStatus(AlarisSms.Status.UNKNOWN);
    }
    try {
      alarisSmsDAO.updateSms(alarisSms);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't update alarisSms by id: [{}]", this, smsId, e);
    }
    return sms;
  }

}

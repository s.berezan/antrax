/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.automation.meta.NotFoundScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.io.Serializable;

public class SimGroupScriptModificationConflictImpl implements SimGroupScriptModificationConflict, Serializable {

  private static final long serialVersionUID = -4852085921462729232L;

  private final SIMGroup oldGroup;
  private final SIMGroup newGroup;

  public SimGroupScriptModificationConflictImpl(final SIMGroup oldGroup, final SIMGroup newGroup) {
    this.oldGroup = oldGroup;
    this.newGroup = newGroup;
  }

  @Override
  public SIMGroup getNewGroup() {
    return newGroup;
  }

  @Override
  public SIMGroup getOldGroup() {
    return oldGroup;
  }

  /**
   * Tests whether group was fully resolved. Such resolve may be done
   * automatically or manually by user
   *
   * @return true if conflict is already resolved
   */
  @Override
  public boolean isFullyResolved() {
    return getUnresolvedInfo() == null;
  }

  @Override
  public String getUnresolvedInfo() {
    return getUnresolvedInformation();
  }

  public String getUnresolvedInformation() {
    String unresolvedInformation;
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getActionProviderScripts(), newGroup.getActionProviderScripts())) != null) {
      return "Action Provider Scripts: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getBusinessActivityScripts(), newGroup.getBusinessActivityScripts())) != null) {
      return "Business Activity Scripts: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getActivityPeriodScript(), newGroup.getActivityPeriodScript())) != null) {
      return "Activity Period Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getFactorScript(), newGroup.getFactorScript())) != null) {
      return "SIM Server Factor Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getGWSelectorScript(), newGroup.getGWSelectorScript())) != null) {
      return "Gateway Selector Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getIncomingCallManagementScript(), newGroup.getIncomingCallManagementScript())) != null) {
      return "Incoming Call Management Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getSessionScript(), newGroup.getSessionScript())) != null) {
      return "Session Period Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getVSFactorScript(), newGroup.getVSFactorScript())) != null) {
      return "Voice Server Factor Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getIMEIGeneratorScript(), newGroup.getIMEIGeneratorScript())) != null) {
      return "IMEI Generator Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getCallFilterScripts(), newGroup.getCallFilterScripts())) != null) {
      return "Call Filter Script: " + unresolvedInformation;
    }
    if ((unresolvedInformation = getUnresolvedScriptWithParam(oldGroup.getSmsFilterScripts(), newGroup.getSmsFilterScripts())) != null) {
      return "SMS Filter Script: " + unresolvedInformation;
    }
    return null;
  }

  private String getUnresolvedScriptWithParam(final ScriptInstance... newScriptInstance) {
    if (newScriptInstance == null) {
      return null;
    }

    for (ScriptInstance s : newScriptInstance) {
      if (s.getDefinition() instanceof NotFoundScriptDefinition) { // was removed or changed name in new version of scripts
        return "Script [" + s.getDefinition().getName() + "] was removed or changed name";
      }

      for (ScriptParameter p : s.getParameters()) {
        if (!p.hasValue()) {
          return s.getDefinition().getName() + "] Parameter [" + p.getDefinition().getName() + "]";
        }
      }
    }
    return null;
  }

  private String getUnresolvedScriptWithParam(final ScriptInstance[] oldScriptInstance, final ScriptInstance[] newScriptInstance) {
    if (oldScriptInstance == null || oldScriptInstance.length == 0) {
      return null;
    } else {
      return getUnresolvedScriptWithParam(newScriptInstance);
    }
  }

  private String getUnresolvedScriptWithParam(final ScriptInstance oldScriptInstance, final ScriptInstance newScriptInstance) {
    if (oldScriptInstance == null) {
      return null;
    } else {
      return getUnresolvedScriptWithParam(newScriptInstance);
    }
  }

  @Override
  public String toString() {
    if (!isFullyResolved()) {
      return "SIM Group [" + oldGroup.getName() + "] Script [" + getUnresolvedInformation();
    }
    return "SimGroupScriptConflict " + oldGroup.getName();
  }

}

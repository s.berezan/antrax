/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import static org.slf4j.LoggerFactory.getLogger;

import com.flamesgroup.antrax.control.communication.CellInfoStatistic;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.IGsmViewDAO;
import com.flamesgroup.antrax.storage.dao.INetworkSurveyDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GsmViewManager implements IGsmViewManager {

  private static final Logger logger = getLogger(GsmViewManager.class);

  private final INetworkSurveyDAO networkSurveyDAO;
  private final IConfigViewDAO configViewDAO;
  private final IGsmViewDAO gsmViewDAO;

  private final Map<String, Boolean> serverUpdateCellAdjustments = new ConcurrentHashMap<>();
  private final Map<String, Boolean> serverUpdateImsiCatcher = new ConcurrentHashMap<>();

  private final Map<String, List<GsmView>> serverGsmViews = new ConcurrentHashMap<>();
  private final Map<String, Map<CellKey, CellInfoStatistic>> prevStatistic = new ConcurrentHashMap<>();

  private final BlockingQueue<Pair<String, IVoiceServerStatus>> voiceServerStatistics = new LinkedBlockingDeque<>();

  public GsmViewManager(final INetworkSurveyDAO networkSurveyDAO, final IConfigViewDAO configViewDAO, final IGsmViewDAO gsmViewDAO) {
    this.networkSurveyDAO = networkSurveyDAO;
    this.configViewDAO = configViewDAO;
    this.gsmViewDAO = gsmViewDAO;
    new GsmViewService().start();
  }

  @Override
  public List<GsmView> listGsmView(final String voiceServerName) throws RemoteException {
    List<GsmView> gsmView = serverGsmViews.get(voiceServerName);
    if (gsmView == null) {
      IServerData serverData = configViewDAO.getServerByName(voiceServerName);
      gsmView = gsmViewDAO.listGsmView(serverData.getID());
      serverGsmViews.put(voiceServerName, new CopyOnWriteArrayList<>(gsmView));
    }
    return gsmView;
  }

  @Override
  public void updateGsmView(final String serverName, final List<GsmView> gsmViews) throws RemoteException {
    if (isUpdateImsiCatcher(serverName)) {
      return;
    }
    IServerData serverData = configViewDAO.getServerByName(serverName);
    gsmViews.forEach(gsmView -> gsmView.setServerId(serverData.getID()));
    List<GsmView> serverGsmView = serverGsmViews.get(serverName);
    if (serverGsmView.isEmpty()) {
      serverGsmViews.put(serverName, new CopyOnWriteArrayList<>(gsmViews));
    } else {
      Map<CellKey, GsmView> gsmViewMap = gsmViews.stream().collect(Collectors.toMap(GsmView::getCellKey, Function.identity()));
      for (GsmView gsmView : serverGsmView) {
        GsmView curGsmView = gsmViewMap.remove(gsmView.getCellKey());
        if (curGsmView != null) {
          gsmView.setNetworkSurveyNumberOccurrences(curGsmView.getNetworkSurveyNumberOccurrences())
              .setServingNumberOccurrences(curGsmView.getServingNumberOccurrences())
              .setNeighborsNumberOccurrences(curGsmView.getNeighborsNumberOccurrences())
              .setLastMcc(curGsmView.getLastMcc())
              .setLastMnc(curGsmView.getLastMnc())
              .setNetworkSurveyLastRxLev(curGsmView.getNetworkSurveyLastRxLev())
              .setCellInfoLastRxLev(curGsmView.getCellInfoLastRxLev())
              .setLastAppearance(curGsmView.getLastAppearance());
        }
      }
      if (!gsmViewMap.isEmpty()) {
        serverGsmView.addAll(gsmViewMap.values());
      }
    }
  }

  @Override
  public void updateRuntimeGsmView(final String serverName, final List<GsmView> gsmViews) throws RemoteException {
    try {
      gsmViewDAO.upsertManualGsmView(gsmViews);
      serverGsmViews.put(serverName, new CopyOnWriteArrayList<>(gsmViews));
    } catch (DataModificationException e) {
      logger.error("[{}] - can't update gsmViews [{}]", this, gsmViews, e);
    }
  }

  @Override
  public void clearGsmView(final String server) throws RemoteException {
    serverGsmViews.get(server).clear();
  }

  @Override
  public Date getStartNetworkSurvey(final String voiceServerName) throws RemoteException {
    IServerData serverData = configViewDAO.getServerByName(voiceServerName);
    return networkSurveyDAO.getStartNetworkSurvey(serverData.getID());
  }

  @Override
  public Boolean isUpdateCellAdjustments(final String server) throws RemoteException {
    return serverUpdateCellAdjustments.getOrDefault(server, false);
  }

  @Override
  public void setServerForUpdateCellAdjustments(final String server, final Boolean update) throws RemoteException {
    serverUpdateCellAdjustments.put(server, update);
  }

  @Override
  public Boolean isUpdateImsiCatcher(final String server) throws RemoteException {
    return serverUpdateImsiCatcher.getOrDefault(server, false);
  }

  @Override
  public void setServerForUpdateImsiCatcher(final String server, final Boolean update) throws RemoteException {
    serverUpdateImsiCatcher.put(server, update);
  }

  @Override
  public CellEqualizerAlgorithm getCellEqualizerAlgorithm(final String server) throws RemoteException {
    IServerData serverData = configViewDAO.getServerByName(server);
    return gsmViewDAO.getCellEqualizerConfiguration(serverData.getID());
  }

  @Override
  public void ping(final Server server, final IVoiceServerStatus voiceServerStatus) {
    voiceServerStatistics.offer(new Pair<>(server.getName(), voiceServerStatus));
  }

  @Override
  public void resetGsmViewStatistic(final List<GsmView> gsmViews, final String server) throws RemoteException {
    List<GsmView> serverGsmView = serverGsmViews.get(server);
    List<CellKey> cellKeys = gsmViews.stream().map(GsmView::getCellKey).collect(Collectors.toList());
    serverGsmView.stream().filter(view -> cellKeys.contains(view.getCellKey())).forEach(view -> {
      view.setNetworkSurveyNumberOccurrences(0);
      view.setServingNumberOccurrences(0);
      view.setNeighborsNumberOccurrences(0);
      view.setSuccessfulCallsCount(0);
      view.setTotalCallsCount(0);
      view.setCallsDuration(0);
      view.setOutgoingSmsCount(0);
      view.setIncomingSmsCount(0);
      view.setUssdCount(0);
      view.setAmountPdd(0);
    });
    serverGsmViews.put(server, new CopyOnWriteArrayList<>(serverGsmView));
    try {
      gsmViewDAO.upsertAutomaticGsmView(serverGsmView);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't update gsmView for: [{}]", this, server, e);
    }
  }

  private class GsmViewService extends Thread {

    private final long SYNC_WITH_DB_PERIOD = TimeUnit.MINUTES.toMillis(1);

    private long lastSyncWithDb;

    public GsmViewService() {
      setName("GsmViewService");
      setDaemon(true);
    }

    @Override
    public void run() {
      while (true) {
        Pair<String, IVoiceServerStatus> voiceServerStatistic;
        try {
          voiceServerStatistic = voiceServerStatistics.take();
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        }

        processVoiceServerStatistic(voiceServerStatistic.first(), voiceServerStatistic.second());

        if (System.currentTimeMillis() >= (lastSyncWithDb + SYNC_WITH_DB_PERIOD)) {
          for (Map.Entry<String, List<GsmView>> serverGsmView : serverGsmViews.entrySet()) {
            try {
              gsmViewDAO.upsertAutomaticGsmView(serverGsmView.getValue());
            } catch (DataModificationException e) {
              logger.warn("[{}] - can't update gsmView for: [{}]", this, serverGsmView.getKey(), e);
            }
          }
          lastSyncWithDb = System.currentTimeMillis();
        }
      }
    }

    private void processVoiceServerStatistic(final String server, final IVoiceServerStatus voiceServerStatus) {
      long startTime = System.currentTimeMillis();
      List<Map<CellKey, CellInfoStatistic>> collect = voiceServerStatus.getChannelsInfo().stream().map(IVoiceServerChannelsInfo::getCellStatistic)
          .filter(map -> !map.isEmpty())
          .collect(Collectors.toList());
      logger.trace("[{}] - collect: {}", this, collect);
      // Join all channels statistic to single
      Map<CellKey, CellInfoStatistic> cellKeyCellInfoStatistic = new HashMap<>();
      for (Map<CellKey, CellInfoStatistic> cellKeyCellInfoStatisticMap : collect) {
        for (Map.Entry<CellKey, CellInfoStatistic> cellKeyCellInfoStatisticEntry : cellKeyCellInfoStatisticMap.entrySet()) {
          cellKeyCellInfoStatistic.compute(cellKeyCellInfoStatisticEntry.getKey(), (cellKey, cellInfoStatistic) -> {
            CellInfoStatistic newCellInfoStatistic = cellKeyCellInfoStatisticEntry.getValue();
            if (cellInfoStatistic == null) {
              return newCellInfoStatistic;
            }

            return new CellInfoStatistic(cellInfoStatistic.getSuccessfulCallsCount() + newCellInfoStatistic.getSuccessfulCallsCount(),
                cellInfoStatistic.getTotalCallsCount() + newCellInfoStatistic.getTotalCallsCount(),
                cellInfoStatistic.getCallsDuration() + newCellInfoStatistic.getCallsDuration(),
                cellInfoStatistic.getSuccessOutgoingSmsCount() + newCellInfoStatistic.getSuccessOutgoingSmsCount(),
                cellInfoStatistic.getIncomingSmsCount() + newCellInfoStatistic.getIncomingSmsCount(),
                cellInfoStatistic.getUssdCount() + newCellInfoStatistic.getUssdCount(),
                cellInfoStatistic.getAmountPdd() + newCellInfoStatistic.getAmountPdd());
          });
        }
      }
      logger.trace("[{}] - cellKeyCellInfoStatistic: {}", this, cellKeyCellInfoStatistic);

      // Create diff with previous CellInfoStatistic
      Map<CellKey, CellInfoStatistic> prevCellKeyCellInfoStatistic = prevStatistic.get(server);
      Map<CellKey, CellInfoStatistic> diffKeyCellInfoStatistic = new HashMap<>();
      if (prevCellKeyCellInfoStatistic == null) {
        diffKeyCellInfoStatistic.putAll(cellKeyCellInfoStatistic);
      } else {
        for (Map.Entry<CellKey, CellInfoStatistic> cellKeyCellInfoStatisticEntry : cellKeyCellInfoStatistic.entrySet()) {
          // server restarted and statistic reset
          CellInfoStatistic prevCellInfoStatistic = prevCellKeyCellInfoStatistic.get(cellKeyCellInfoStatisticEntry.getKey());
          CellInfoStatistic newCellInfoStatistic = cellKeyCellInfoStatisticEntry.getValue();
          if (prevCellInfoStatistic != null && newCellInfoStatistic.getTotalCallsCount() < prevCellInfoStatistic.getTotalCallsCount()) {
            diffKeyCellInfoStatistic.putAll(cellKeyCellInfoStatistic);
            break;
          } else if (prevCellInfoStatistic == null) {
            diffKeyCellInfoStatistic.put(cellKeyCellInfoStatisticEntry.getKey(), newCellInfoStatistic);
          } else {
            CellInfoStatistic cellInfoStatistic = new CellInfoStatistic(newCellInfoStatistic.getSuccessfulCallsCount() - prevCellInfoStatistic.getSuccessfulCallsCount(),
                newCellInfoStatistic.getTotalCallsCount() - prevCellInfoStatistic.getTotalCallsCount(),
                newCellInfoStatistic.getCallsDuration() - prevCellInfoStatistic.getCallsDuration(),
                newCellInfoStatistic.getSuccessOutgoingSmsCount() - prevCellInfoStatistic.getSuccessOutgoingSmsCount(),
                newCellInfoStatistic.getIncomingSmsCount() - prevCellInfoStatistic.getIncomingSmsCount(),
                newCellInfoStatistic.getUssdCount() - prevCellInfoStatistic.getUssdCount(),
                newCellInfoStatistic.getAmountPdd() - prevCellInfoStatistic.getAmountPdd());
            diffKeyCellInfoStatistic.put(cellKeyCellInfoStatisticEntry.getKey(), cellInfoStatistic);
          }
        }
      }

      logger.trace("[{}] - diffKeyCellInfoStatistic : {}", this, diffKeyCellInfoStatistic);

      prevStatistic.put(server, cellKeyCellInfoStatistic);

      // Update runtime GsmView
      List<GsmView> gsmViews = serverGsmViews.get(server);
      for (GsmView gsmView : gsmViews) {
        CellInfoStatistic cellInfoStatistic = diffKeyCellInfoStatistic.get(gsmView.getCellKey());
        if (cellInfoStatistic == null) {
          continue;
        }
        gsmView.setSuccessfulCallsCount(gsmView.getSuccessfulCallsCount() + cellInfoStatistic.getSuccessfulCallsCount());
        gsmView.setTotalCallsCount(gsmView.getTotalCallsCount() + cellInfoStatistic.getTotalCallsCount());
        gsmView.setCallsDuration(gsmView.getCallsDuration() + cellInfoStatistic.getCallsDuration());
        gsmView.setOutgoingSmsCount(gsmView.getOutgoingSmsCount() + cellInfoStatistic.getSuccessOutgoingSmsCount());
        gsmView.setIncomingSmsCount(gsmView.getIncomingSmsCount() + cellInfoStatistic.getIncomingSmsCount());
        gsmView.setUssdCount(gsmView.getUssdCount() + cellInfoStatistic.getUssdCount());
        gsmView.setAmountPdd(gsmView.getAmountPdd() + cellInfoStatistic.getAmountPdd());
      }

      logger.trace("[{}] - gsmViews: {}", this, gsmViews);

      long endTime = System.currentTimeMillis();
      logger.trace("[{}] - gsmView merging time: [{}], for server: [{}]", this, endTime - startTime, server);
    }

  }

}

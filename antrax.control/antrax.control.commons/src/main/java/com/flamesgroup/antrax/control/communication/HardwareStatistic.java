/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceUID;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class HardwareStatistic implements Serializable {

  private static final long serialVersionUID = 5615943727607881776L;

  private int gsmCount;
  private int gsmChannels;
  private int simChannels;

  private final Map<DeviceUID, AtomicInteger> gsmDevices = new HashMap<>();

  public int getGsmCount() {
    return gsmCount;
  }

  public int getGsmChannels() {
    return gsmChannels;
  }

  public int getSimChannels() {
    return simChannels;
  }

  public void removeSimChannel() {
    simChannels--;
  }

  public void addSimChannel(final ChannelUID simChannel) {
    simChannels++;
  }

  public synchronized void addGsmUnit(final ChannelUID gsmChannel) {
    gsmChannels++;
    DeviceUID device = gsmChannel.getDeviceUID();
    if (gsmDevices.containsKey(device)) {
      gsmDevices.get(device).incrementAndGet();
    } else {
      gsmDevices.put(device, new AtomicInteger(1));
    }
    gsmCount = gsmDevices.size();
  }

  public void removeGsmUnit(final ChannelUID gsmChannel) {
    gsmChannels--;
    DeviceUID device = gsmChannel.getDeviceUID();
    if (gsmDevices.containsKey(device)) {
      AtomicInteger count = gsmDevices.get(device);
      if (count.decrementAndGet() == 0) {
        gsmDevices.remove(device);
      }
    }
    gsmCount = gsmDevices.size();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.voiceserver;

import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerLongInformation;
import com.flamesgroup.antrax.control.communication.IVoiceServerShortInfo;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformationImpl;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SimChannelInformationImpl;
import com.flamesgroup.antrax.control.communication.VoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.VoiceServerLongInformation;
import com.flamesgroup.antrax.control.communication.VoiceServerServerShortInfo;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ActivityLogger implements IActivityLogger {

  private static final Logger logger = LoggerFactory.getLogger(ActivityLogger.class);

  // ChannelsRuntime
  private final Map<ChannelUID, VoiceServerChannelsInfo> voiceServerChannels = new HashMap<>();
  private final Map<ChannelUID, SimChannelInformationImpl> simServerChannels = new HashMap<>();
  private final Map<ChannelUID, VoiceServerChannelsInfo> createdServerChannels = new HashMap<>();
  private final Map<CallState.State, EnumSet<CallState.State>> channelCallStates = new HashMap<>();

  // ShortServerRuntime
  private final VoiceServerServerShortInfo shortServerInfo = new VoiceServerServerShortInfo();

  // LongServerRuntime
  private final VoiceServerLongInformation longServerInfo = new VoiceServerLongInformation();

  private final Map<ChannelUID, VoiceServerChannelsInfo> lateReleasedCallChannels = new ConcurrentHashMap<>();

  public ActivityLogger() {
    // ChannelsRuntime
    channelCallStates.put(CallState.State.IDLE, EnumSet.of(CallState.State.DIALING));
    channelCallStates.put(CallState.State.DIALING, EnumSet.of(CallState.State.IDLE, CallState.State.ALERTING, CallState.State.ACTIVE));
    channelCallStates.put(CallState.State.ALERTING, EnumSet.of(CallState.State.IDLE, CallState.State.ACTIVE));
    channelCallStates.put(CallState.State.ACTIVE, EnumSet.of(CallState.State.IDLE));

    // LongServerRuntime
    longServerInfo.setVersion("Unsupported");
  }

  @Override
  public List<IVoiceServerChannelsInfo> getVoiceServerChannelsInfos() {
    return new ArrayList<>(voiceServerChannels.values());
  }

  @Override
  public IVoiceServerChannelsInfo getVoiceServerChannelsInfo(final ChannelUID gsmUnit) {
    return voiceServerChannels.get(gsmUnit);
  }

  @Override
  public IVoiceServerShortInfo getShortServerInfo() {
    return shortServerInfo;
  }

  @Override
  public IVoiceServerLongInformation getLongServerInfo() {
    return longServerInfo;
  }

  @Override
  public void logDeclaredGsmUnit(final GSMChannel gsmChannel) {
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = new MobileGatewayChannelInformationImpl(gsmChannel.getChannelUID());
    mobileGatewayChannelInfo.setGsmChannel(gsmChannel);
    VoiceServerChannelsInfo prev = voiceServerChannels.put(gsmChannel.getChannelUID(), new VoiceServerChannelsInfo(mobileGatewayChannelInfo));
    if (prev != null) {
      logger.warn("[{}] - found gsm unit {} before releasing it: {}", this, gsmChannel.getChannelUID(), prev);
    }
  }

  @Override
  public void logActiveGsmUnit(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    updateChannelConfig(gsmUnit, gsmChannel, channelConfig);

    shortServerInfo.incFreeGSMChannels();

    longServerInfo.getChannelsStatistic().incNotConnectedChannelsCount();
    longServerInfo.getHardwareStatistic().addGsmUnit(gsmUnit);
  }

  @Override
  public void updateChannelConfig(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    VoiceServerChannelsInfo channel = voiceServerChannels.get(gsmUnit);
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
    mobileGatewayChannelInfo.setLive(true);
    mobileGatewayChannelInfo.setGsmChannel(gsmChannel);
    mobileGatewayChannelInfo.setChannelConfig(channelConfig);
  }

  @Override
  public void logGsmUnitChangedGroup(final ChannelUID gsmUnit, final GSMGroup group) {
    VoiceServerChannelsInfo channel = voiceServerChannels.get(gsmUnit);
    if (channel != null) {
      MobileGatewayChannelInformationImpl info = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
      if (info != null) {
        info.getGSMChannel().setGsmGroup(group);
      }
    }
  }

  @Override
  public void logGsmUnitLock(final ChannelUID gsmUnit, final boolean lock, final String lockReason) {
    VoiceServerChannelsInfo channel = voiceServerChannels.get(gsmUnit);
    if (channel == null) {
      return;
    }
    GSMChannel getGsmChannel = channel.getMobileGatewayChannelInfo().getGSMChannel();
    getGsmChannel.setLock(lock).setLockReason(lockReason).setLockTime(lock ? System.currentTimeMillis() : 0);
  }

  @Override
  public void logGSMUnitReleased(final ChannelUID gsmUnit) {
    logger.debug("[{}] - gsm unit released: {}", this, gsmUnit);
    VoiceServerChannelsInfo channel = voiceServerChannels.get(gsmUnit);
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
    mobileGatewayChannelInfo.setLive(false);
    mobileGatewayChannelInfo.setChannelConfig(null);

    shortServerInfo.decFreeGSMChannels();
    if (shortServerInfo.getFreeGSMChannels() < 0) {
      logger.warn("[{}] - free gsm unit counter is negative", this);
    }

    longServerInfo.getChannelsStatistic().decNotConnectedChannelsCount();
    longServerInfo.getHardwareStatistic().removeGsmUnit(gsmUnit);
  }

  @Override
  public void logSIMTakenFromSimServer(final ChannelUID simUnit, final ICCID uid) {
    simServerChannels.put(simUnit, new SimChannelInformationImpl(simUnit, uid));

    longServerInfo.getHardwareStatistic().addSimChannel(simUnit);
  }

  @Override
  public void logSIMReturnedToSimServer(final ChannelUID simUnit) {
    simServerChannels.remove(simUnit);

    longServerInfo.getHardwareStatistic().removeSimChannel();
  }

  @Override
  public void logCallChannelCreated(final ChannelUID gsmUnit, final ChannelUID simUnit) {
    logger.debug("[{}] - call channel created with [{}] and [{}]", this, gsmUnit, simUnit);
    VoiceServerChannelsInfo channelsInfo = voiceServerChannels.get(gsmUnit);
    SimChannelInformationImpl cemInfo = simServerChannels.get(simUnit);
    if (channelsInfo == null) {
      logger.warn("[{}] - channel <{}:{}> created, but appropriate gsm unit was not found", this, gsmUnit, simUnit);
    } else if (cemInfo == null) {
      logger.warn("[{}] - channel <{}:{}> created, but appropriate sim unit was not taken from sim server", this, gsmUnit, simUnit);
    } else {
      channelsInfo.setSimChannelInfo(cemInfo);
      // FIXME: resolve getting release before created
      VoiceServerChannelsInfo previousChannelInfo = createdServerChannels.put(simUnit, channelsInfo);
      if (previousChannelInfo != null) {
        logger.info("[{}] - get created call channel before released for {}", this, simUnit);
        lateReleasedCallChannels.put(simUnit, previousChannelInfo);
      }
    }

    shortServerInfo.decFreeGSMChannels();
    if (shortServerInfo.getFreeGSMChannels() < 0) {
      logger.warn("[{}] - free gsm unit counter is negative", this);
    }
  }

  @Override
  public void logCallChannelStartActivity(final ChannelUID simUnit) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setRegistered(true);
    }
  }

  @Override
  public void logCallChannelReleased(final ChannelUID simUnit) {
    // FIXME: resolve getting release before created
    VoiceServerChannelsInfo lateReleasedCallChannel = lateReleasedCallChannels.remove(simUnit);
    if (lateReleasedCallChannel != null) {
      logger.info("[{}] - get late released call channel after created for {}", this, simUnit);
      return;
    }

    logger.debug("[{}] - call channel with sim unit {} released", this, simUnit);
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.remove(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setSimChannelInfo(null);
      channelsInfo.reset();
    } else {
      logger.warn("[{}] - there's no channel with sim unit {} for release", this, simUnit);
    }

    shortServerInfo.incFreeGSMChannels();
  }

  @Override
  public void logCallSetup(final ChannelUID simUnit, final PhoneNumber phoneNumber) {
    VoiceServerChannelsInfo channel = createdServerChannels.get(simUnit);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(phoneNumber);
    }
  }

  @Override
  public void logCallStarted(final ChannelUID simUnit, final PhoneNumber phoneNumber) {
    VoiceServerChannelsInfo channel = createdServerChannels.get(simUnit);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(phoneNumber);
    }

    shortServerInfo.incOutgoingCalls();
  }

  @Override
  public void logCallEnded(final ChannelUID simUnit, final long duration) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incTotalCallsCount();
      if (channelsInfo.getCallState().getState() == CallState.State.ACTIVE) {
        channelsInfo.incSuccessfulCallsCount();
        channelsInfo.addCallsDuration(duration);
      }
      logCallChangeState(simUnit, CallState.State.IDLE);
      channelsInfo.getCallState().setPhoneNumber(null);
    }

    shortServerInfo.decOutgoingCalls();

    longServerInfo.handleCallEnded(simUnit);
  }

  @Override
  public void logCallRelease(final ChannelUID simUnit) {
    VoiceServerChannelsInfo channel = createdServerChannels.get(simUnit);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(null);
    }
  }

  @Override
  public void logCallChangeState(final ChannelUID simUnit, final CallState.State callState) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null && channelCallStates.get(channelsInfo.getCallState().getState()).contains(callState)) {
      channelsInfo.getCallState().changeState(callState);
    }

    longServerInfo.handleCallChangeState(simUnit, callState);
  }

  @Override
  public void logCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) {
    logger.debug("[{}] - callChannelChangedState state={} adv.info={} UID={}", this, state, advInfo, simUnit);
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelState(new CallChannelState(state, periodPrediction));
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }

    longServerInfo.handleCallChannelChangedState(simUnit, state);
  }

  @Override
  public void logCallChannelChangedAdvInfo(final ChannelUID simUnit, final String advInfo) {
    logger.debug("[{}] - callChannelChangedAdvInfo adv.info={} UID={}", this, advInfo, simUnit);
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }
  }

  @Override
  public void logGsmUnitChangedState(final ChannelUID gsmUnit, final CallChannelState.State state, final String advInfo,
      final long periodPrediction) {
    VoiceServerChannelsInfo channelsInfo = voiceServerChannels.get(gsmUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelState(state == null ? null : new CallChannelState(state, periodPrediction));
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }
  }

  @Override
  public void logSIMChangedEnableStatus(final ChannelUID simUnit, final boolean enable) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setEnabled(enable);
    }
  }

  @Override
  public void logSIMChangedGroup(final ChannelUID simUnit, final String group) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setGroup(group);
    }
  }

  @Override
  public void logCallChannelSendUSSD(final ChannelUID simUnit, final String ussd, final String response) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setLastUSSDResponse(response);
    }
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incUssdCount();
    }
  }

  @Override
  public void logCallChannelSuccessSentSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incSuccessOutgoingSmsCount(parts);
      channelsInfo.incTotalOutgoingSmsCount(parts);
    }
  }

  @Override
  public void logCallChannelFailSentSMS(final ChannelUID simUnit, final String number, final String text, final int totalSmsParts, final int successSendSmsParts) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incTotalOutgoingSmsCount(totalSmsParts);
      channelsInfo.incSuccessOutgoingSmsCount(successSendSmsParts);
    }
  }

  @Override
  public void logCallChannelReceivedSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incIncomingTotalSmsCount(parts);
    }
  }

  @Override
  public void changeServerStatus(final ServerStatus status) {
    shortServerInfo.setStatus(status);

    longServerInfo.changeServerStatus(status);
  }

  private SimChannelInformationImpl findSimChannelInfo(final ChannelUID simUnit) {
    return simServerChannels.get(simUnit);
  }

  @Override
  public void logSignalQualityChange(final ChannelUID gsmUnit, final int signalStrength, final int bitErrorRate) {
    VoiceServerChannelsInfo channelInfo = voiceServerChannels.get(gsmUnit);
    if (channelInfo != null) {
      MobileGatewayChannelInformationImpl info = (MobileGatewayChannelInformationImpl) channelInfo.getMobileGatewayChannelInfo();
      if (info != null) {
        info.setSignalStrength(signalStrength);
        info.setBitErrorRate(bitErrorRate);
      }
    }
  }

  @Override
  public void logGSMNetworkInfoChange(final ChannelUID gsmUnit, final GSMNetworkInfo gsmNetworkInfo) {
    VoiceServerChannelsInfo channelInfo = voiceServerChannels.get(gsmUnit);
    if (channelInfo != null) {
      channelInfo.setGSMNetworkInfoChange(gsmNetworkInfo);
    }
  }

  @Override
  public void logPdd(final ChannelUID simUnit, final long pdd) {
    VoiceServerChannelsInfo channelsInfo = createdServerChannels.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incPdd(pdd);
    }
  }

  @Override
  public void resetStatistic() {
    voiceServerChannels.values().stream().map(VoiceServerChannelsInfo::getMobileGatewayChannelInfo).forEach(MobileGatewayChannelInformation::reset);
  }

  @Override
  public void logGsmUnitLockToArfcn(final ChannelUID channel, final Integer arfcn) {
    VoiceServerChannelsInfo voiceServerChannelsInfo = voiceServerChannels.get(channel);
    voiceServerChannelsInfo.getMobileGatewayChannelInfo().getGSMChannel().setLockArfcn(arfcn);
  }

}

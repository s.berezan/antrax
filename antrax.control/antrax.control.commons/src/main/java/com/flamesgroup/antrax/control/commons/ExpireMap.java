/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.commons;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ExpireMap<K, V> extends ConcurrentHashMap<K, V> {

  private static final long serialVersionUID = 4129291010459653139L;

  private final IExpireHandler<K> expireHandler;

  private final Map<K, Long> expireTimeouts = new ConcurrentHashMap<>();
  private final Map<K, Long> expireKeys = new ConcurrentHashMap<>();

  public ExpireMap(final IExpireHandler<K> expireHandler) {
    this.expireHandler = expireHandler;
  }

  public void putExpireTimeout(final K key, final long timeout) {
    expireTimeouts.put(key, timeout);
  }

  public Long removeExpireTimeout(final K key) {
    return expireTimeouts.remove(key);
  }

  public void updateExpireKey(final K key) {
    expireKeys.put(key, System.currentTimeMillis());
  }

  public int clearExpireKeys() {
    expireKeys.keySet().retainAll(keySet());

    Set<K> expired = expireKeys.entrySet().stream().filter(this::filterExpired).map(Entry::getKey).collect(Collectors.toSet());
    expired.forEach(k -> {
      expireHandler.handleExpire(k);
      expireKeys.remove(k);
    });

    return expired.size();
  }

  private boolean filterExpired(final Entry<K, Long> entry) {
    return System.currentTimeMillis() - entry.getValue() > expireTimeouts.getOrDefault(entry.getKey(), 0L);
  }

  public interface IExpireHandler<E> extends Serializable {

    void handleExpire(E expired);

  }

}

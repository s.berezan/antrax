/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;

public class CallsInfo implements Serializable {

  private static final long serialVersionUID = 3258176256253607979L;

  private int count;
  private long totalDuration;

  private final PeriodExpirationTrigger trigger;

  public CallsInfo(final PeriodExpirationTrigger trigger) {
    this.trigger = trigger;
    trigger.handleNewPeriod();
  }

  public int getCount() {
    checkExpiration();
    return count;
  }

  private void checkExpiration() {
    if (trigger.isPeriodExpired()) {
      totalDuration = 0;
      count = 0;
      trigger.handleNewPeriod();
    }
  }

  public void incCount() {
    checkExpiration();
    this.count++;
  }

  public long getDuration() {
    checkExpiration();
    return totalDuration;
  }

  public void incTotalDuration(final long duration) {
    checkExpiration();
    this.totalDuration += duration;
  }

}

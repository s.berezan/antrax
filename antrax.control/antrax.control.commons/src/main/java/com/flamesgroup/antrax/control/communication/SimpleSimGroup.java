/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.io.Serializable;

public class SimpleSimGroup implements Serializable {

  private static final long serialVersionUID = 1496038703118882011L;

  private final long id;
  private final long revision;
  private final String name;
  private final boolean canBeAppointed;

  public SimpleSimGroup(final SIMGroup simGroup) {
    this(simGroup.getID(), simGroup.getRevision(), simGroup.getName(), simGroup.canBeAppointed());
  }

  public SimpleSimGroup(final long id, final long revision, final String name, final boolean canBeAppointed) {
    this.id = id;
    this.revision = revision;
    this.name = name;
    this.canBeAppointed = canBeAppointed;
  }

  public long getId() {
    return id;
  }

  public long getRevision() {
    return revision;
  }

  public String getName() {
    return name;
  }

  public boolean canBeAppointed() {
    return canBeAppointed;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SimpleSimGroup)) {
      return false;
    }
    final SimpleSimGroup that = (SimpleSimGroup) object;

    return id == that.id;
  }

  @Override
  public String toString() {
    return name;
  }

}

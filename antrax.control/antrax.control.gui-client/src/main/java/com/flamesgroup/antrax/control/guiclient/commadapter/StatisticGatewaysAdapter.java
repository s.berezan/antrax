/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.guiclient.domain.ServerChannelRowKey;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.ChannelUID;

public class StatisticGatewaysAdapter {

  private final IServerData voiceServer;
  private final MobileGatewayChannelInformation[] channels;

  private int serverTotalCallsCount;
  private int serverSuccessfulCallsCount;
  private long serverCallsDuration;
  private int serverTotalSmsCount;
  private int successOutgoingSmsCount;
  private int serverIncomingTotalSmsCount;

  public StatisticGatewaysAdapter(final IServerData voiceServer, final MobileGatewayChannelInformation[] channels) {
    this.voiceServer = voiceServer;
    this.channels = channels;

    for (MobileGatewayChannelInformation channel : channels) {
      serverTotalCallsCount += channel.getTotalCallsCount();
      serverSuccessfulCallsCount += channel.getSuccessfulCallsCount();
      serverCallsDuration += channel.getCallsDuration();
      serverTotalSmsCount += channel.getTotalOutgoingSmsCount();
      successOutgoingSmsCount += channel.getSuccessOutgoingSmsCount();
      serverIncomingTotalSmsCount += channel.getIncomingTotalSmsCount();
    }
  }

  private int calculateASR(final int successfulCallsCount, final int totalCallsCount) {
    return (totalCallsCount == 0) ? 0 : 100 * successfulCallsCount / totalCallsCount;
  }

  public long calculateACD(final long callsDuration, final int successfulCallsCount) {
    return (successfulCallsCount == 0) ? 0 : (callsDuration / successfulCallsCount);
  }

  public int getChannelsCount() {
    return channels.length;
  }

  public int getServerASR() {
    return calculateASR(serverSuccessfulCallsCount, serverTotalCallsCount);
  }

  public long getServerACD() {
    return calculateACD(serverCallsDuration, serverSuccessfulCallsCount);
  }

  public int getServerTotalCalls() {
    return serverTotalCallsCount;
  }

  public int getServerSuccessCalls() {
    return serverSuccessfulCallsCount;
  }

  public long getServerCallsDuration() {
    return serverCallsDuration;
  }

  public int getServerTotalSmses() {
    return serverTotalSmsCount;
  }

  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  public int getServerSMSASR() {
    return calculateASR(successOutgoingSmsCount, serverTotalSmsCount);
  }

  public int getServerIncomingTotalSmses() {
    return serverIncomingTotalSmsCount;
  }

  public IServerData getServer() {
    return voiceServer;
  }

  public ChannelUID getMobileChannelUID(final int channelIndex) {
    return channels[channelIndex].getGSMChannelUID();
  }

  public int getChannelASR(final int channelIndex) {
    MobileGatewayChannelInformation channel = channels[channelIndex];
    return calculateASR(channel.getSuccessfulCallsCount(), channel.getTotalCallsCount());
  }

  public long getChannelACD(final int channelIndex) {
    MobileGatewayChannelInformation channel = channels[channelIndex];
    return calculateACD(channel.getCallsDuration(), channel.getSuccessfulCallsCount());
  }

  public int getChannelTotalCalls(final int channelIndex) {
    return channels[channelIndex].getTotalCallsCount();
  }

  public int getChannelSuccessCalls(final int channelIndex) {
    return channels[channelIndex].getSuccessfulCallsCount();
  }

  public long getChannelCallsDuration(final int channelIndex) {
    return channels[channelIndex].getCallsDuration();
  }

  public int getChannelTotalSmses(final int channelIndex) {
    return channels[channelIndex].getTotalOutgoingSmsCount();
  }

  public int getSuccessOutgoingSmsCount(final int channelIndex) {
    return channels[channelIndex].getSuccessOutgoingSmsCount();
  }

  public int getASRSms(final int channelIndex) {
    MobileGatewayChannelInformation channel = channels[channelIndex];
    return calculateASR(channel.getSuccessOutgoingSmsCount(), channel.getTotalOutgoingSmsCount());
  }

  public long getChannelIncomingTotalSmses(final int channelIndex) {
    return channels[channelIndex].getIncomingTotalSmsCount();
  }

  public ServerChannelRowKey getKey(final int channelIndex) {
    if (channelIndex < 0 || channelIndex >= channels.length) {
      return new ServerChannelRowKey(voiceServer, null);
    } else {
      return new ServerChannelRowKey(voiceServer, channels[channelIndex].getGSMChannelUID());
    }
  }

}

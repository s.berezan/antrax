/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.forms;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.field.IPTextField;

import java.awt.*;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;

public abstract class BaseForm<T> extends JPanel {

  private static final long serialVersionUID = 210248570884012161L;

  private boolean editable;

  public BaseForm() {
    FormBuilder builder = createBuilder();
    GroupLayout layout = new GroupLayout(this);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);

    GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
    ParallelGroup labelsHGroup = layout.createParallelGroup();
    ParallelGroup editorsHGroup = layout.createParallelGroup();
    hGroup.addGroup(labelsHGroup);
    hGroup.addGroup(editorsHGroup);

    GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();

    for (FormField<?> field : builder.getFields()) {
      JLabel label = createLabel(field.getName());
      labelsHGroup.addComponent(label);
      editorsHGroup.addComponent(field.getComponent());

      vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
      /*    */.addComponent(label)
      /*    */.addComponent(field.getComponent()));
    }

    layout.setHorizontalGroup(hGroup);
    layout.setVerticalGroup(vGroup);
    setLayout(layout);
  }

  private JLabel createLabel(final String text) {
    return new JEditLabel(text);
  }

  public void setEditable(final boolean editable) {
    this.editable = editable;

    for (Component cmp : getComponents()) {
      if (cmp instanceof JLabel) {
        ((JLabel) cmp).setEnabled(editable);
      } else if (cmp instanceof JTextField) {
        ((JTextField) cmp).setEditable(editable);
      } else if (cmp instanceof IPTextField) {
        ((IPTextField) cmp).setEditable(editable);
      }
    }
  }

  public boolean isEditable() {
    return editable;
  }

  public abstract void clear();

  public abstract T getValue();

  public abstract void setValue(T value);

  protected abstract FormBuilder createBuilder();

  protected FormField<Integer> createPortField(final FormBuilder builder, final String name) {
    FormField<Integer> field = builder.addField(name, Integer.class, 1, 65535);
    field.getComponent().setMaximumSize(new Dimension(70, 0));
    return field;
  }

  /**
   * Fields builder
   */
  public static class FormBuilder {

    private int ids = 0;
    private final List<FormField<?>> fields = new LinkedList<>();

    public List<FormField<?>> getFields() {
      return fields;
    }

    @SuppressWarnings("unchecked")
    public <T> FormField<T> addField(final String name, final Class<T> clazz) {
      if (clazz == null) {
        return null;
      }

      FormField<T> field = null;
      if (Integer.class.equals(clazz)) {
        field = (FormField<T>) new IntegerFormField(ids++, name);
      } else if (InetAddress.class.equals(clazz)) {
        field = (FormField<T>) new IPAddressFormField(ids++, name);
      }

      if (field != null) {
        fields.add(field);
      }

      return field;
    }

    @SuppressWarnings("unchecked")
    public <T> FormField<T> addField(final String name, final Class<T> clazz, final T minValue, final T maxValue) {
      if (clazz == null) {
        return null;
      }

      FormField<T> field = null;
      if (Integer.class.equals(clazz)) {
        Integer min = (Integer) minValue;
        Integer max = (Integer) maxValue;
        field = (FormField<T>) new IntegerFormField(ids++, name, min, max);
      }

      if (field != null) {
        fields.add(field);
      }

      return field;
    }

  }
}

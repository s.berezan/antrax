/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.indicators;

import com.flamesgroup.antrax.control.guiclient.widgets.indicators.JIndicatorBar;
import com.flamesgroup.antrax.control.guiclient.widgets.indicators.JIntIndicator;

import javax.swing.*;

public class DefaultIndicators {

  private final JIndicatorBar bar;

  private final JIntIndicator totalIndicator;
  private final JIntIndicator filteredIndicator;
  private final JIntIndicator selectedIndicator;

  public DefaultIndicators() {
    String formatStr = "<html><b color='#222222'>%s</b></html>";
    totalIndicator = new JIntIndicator(String.format(formatStr, "Total:"), 0);
    filteredIndicator = new JIntIndicator(String.format(formatStr, "Filtered:"), 0);
    selectedIndicator = new JIntIndicator(String.format(formatStr, "Selected:"), 0);

    bar = new JIndicatorBar();
    bar.addIndicator(totalIndicator);
    bar.addIndicator(filteredIndicator);
    bar.addIndicator(selectedIndicator);

    setTotal(0);
    setFiltered(0);
    setSelected(0);
  }

  private void refreshVisibility() {
    boolean showFilters = !filteredIndicator.getValue().equals(totalIndicator.getValue());
    filteredIndicator.setVisible(showFilters);
  }

  public void setTotal(final int total) {
    totalIndicator.setValue(total);
    refreshVisibility();
  }

  public void setFiltered(final int filtered) {
    filteredIndicator.setValue(filtered);
    refreshVisibility();
  }

  public void setSelected(final int selected) {
    selectedIndicator.setValue(selected);
    selectedIndicator.setVisible(selected > 0);
  }

  public JComponent getComponent() {
    return bar.getComponent();
  }
}

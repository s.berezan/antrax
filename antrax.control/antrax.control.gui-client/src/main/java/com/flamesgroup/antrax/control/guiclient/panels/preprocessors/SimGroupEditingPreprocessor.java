/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.preprocessors;

import com.flamesgroup.antrax.control.guiclient.panels.EditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.OperationCanceledException;
import com.flamesgroup.antrax.control.guiclient.panels.SimGroupWrapper;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class SimGroupEditingPreprocessor implements EditingPreprocessor<SimGroupWrapper> {

  private static final Logger logger = LoggerFactory.getLogger(SimGroupEditingPreprocessor.class);

  private final JUpdatableTable<SimGroupWrapper, Long> table;

  public SimGroupEditingPreprocessor(final JUpdatableTable<SimGroupWrapper, Long> table) {
    this.table = table;
  }

  @Override
  public SimGroupWrapper beforeAdd(final SimGroupWrapper elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
    List<String> groupNames = table.getElems().stream().map(e -> e.getSimGroup().getName()).collect(Collectors.toList());
    elem.getSimGroup().setName(checkOnUniqueAndAddStarToEnd(groupNames, "New group"));
    return elem;
  }

  @Override
  public void beforeRemove(final SimGroupWrapper elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
  }

  @Override
  public void beforeUpdate(final SimGroupWrapper elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
    if (table.getElems().stream().map(e -> e.getSimGroup().getName()).filter(e -> e.equals(elem.getSimGroup().getName())).count() > 1) {
      logger.info("[{}] - contains duplicate: [{}]", this, elem.getSimGroup().getName());
      MessageUtils.showWarn(invoker.getParent(), "Warn", "Please use unique name for GSM group");
      throw new OperationCanceledException();
    }
  }

  private String checkOnUniqueAndAddStarToEnd(final List<String> groupNames, final String newGroupName) {
    for (String groupName : groupNames) {
      if (newGroupName.equals(groupName)) {
        return checkOnUniqueAndAddStarToEnd(groupNames, newGroupName + "*");
      }
    }
    return newGroupName;
  }
}

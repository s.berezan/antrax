/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.configuration.diff.DiffUtil;
import com.flamesgroup.antrax.configuration.diff.ListContainer;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.panels.SimChannelsTable;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.editor.ExtendedComboBox;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.*;

public class SimChannelsPanelRefresher extends ExecutiveRefresher {

  private volatile boolean showEmptyHolders;
  private volatile boolean showUnliveHolders;

  private final String server;
  private final SimChannelsTable table;
  private final JComboBox simGroupComboBox;
  private volatile List<SimViewData> simData;
  private volatile long simGroupsRevision = -1;
  private volatile SimpleSimGroup[] simGroups;
  private Map<String, List<SimViewData>> prevSimViewDataAll = new HashMap<>();

  public SimChannelsPanelRefresher(final String server, final RefresherThread refresherThread, final TransactionManager transactionManager, final SimChannelsTable table,
      final ExtendedComboBox simGroupComboBox) {
    super("SimChannelsPanelRefresher", refresherThread, transactionManager, table);
    this.server = server;
    this.table = table;
    this.simGroupComboBox = simGroupComboBox;

    addPermanentExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
        updateSimGroups();
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        readSimGroups(beansPool, transactionManager);
      }

      @Override
      public String describeExecution() {
        return "read sim groups";
      }
    });

    addPermanentExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
        updateSimData();
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        readSimData(beansPool);
      }

      @Override
      public String describeExecution() {
        return "read sim runtime data";
      }
    });
  }

  public void clearPrevSimViewDataAll() {
    prevSimViewDataAll.clear();
  }

  public void setShowEmptyHolders(final boolean selected) {
    this.showEmptyHolders = selected;
  }

  public void setShowUnliveHolders(final boolean selected) {
    this.showUnliveHolders = selected;
  }

  private void readSimGroups(final BeansPool beansPool, final TransactionManager transactionManager) throws StorageException, DataSelectionException {
    if (simGroupsRevision == Long.MAX_VALUE) {
      return; // We had NotPermittedException, will not waste user's internet on the same exception
    }
    try {
      long rev = beansPool.getConfigBean().getSIMGroupsRevision(MainApp.clientUID, transactionManager.getGuaranteedTransactionId());
      if (rev != simGroupsRevision) {
        simGroupsRevision = rev;
        simGroups = beansPool.getConfigBean().listSimpleSIMGroups(MainApp.clientUID);
      }
    } catch (NotPermittedException ignored) {
      simGroupsRevision = Long.MAX_VALUE;
    }
  }

  private void readSimData(final BeansPool beansPool) throws StorageException {
    List<String> simServers;
    try {
      if (server == null) {
        simServers = beansPool.getControlBean().listSimServers().stream().map(IServerData::getName).collect(Collectors.toList());
      } else {
        simServers = Collections.singletonList(server);
      }

      List<SimViewData> resultSimViewData = new ArrayList<>();
      for (String simServer : simServers) {
        List<SimViewData> prevSimViewData = prevSimViewDataAll.getOrDefault(simServer, new ArrayList<>());
        if (prevSimViewData.isEmpty()) {
          prevSimViewData = beansPool.getControlBean().getSimRuntimeData(simServer);
        } else {
          List<GraphComparator.Delta> simViewDataDiff = beansPool.getControlBean().getSimRuntimeDataDiff(simServer);
          GraphComparator.applyDelta(new ListContainer(0, prevSimViewData), simViewDataDiff, DiffUtil.getIdFetcher(), GraphComparator.getJavaDeltaProcessor());
        }
        prevSimViewDataAll.put(simServer, prevSimViewData);
        resultSimViewData.addAll(prevSimViewData.stream().filter(d -> (d.isLive() || showUnliveHolders) && (!d.isEmpty() || showEmptyHolders)).collect(Collectors.toList()));
      }

      this.simData = resultSimViewData;
    } catch (NotPermittedException e) {
      System.out.println("fail on get simData");
      e.printStackTrace();
    }
  }

  public void updateSimGroups() {
    if (simGroups == null) {
      return;
    }
    if (!simGroupComboBox.isFocusOwner()) {
      simGroupComboBox.removeAllItems();
      simGroupComboBox.addItem(null);
      for (SimpleSimGroup g : simGroups) {
        if (g.canBeAppointed()) {
          simGroupComboBox.addItem(g);
        }
      }
      simGroups = null;
    }
  }

  public void updateSimData() {
    if (simData == null) {
      table.clearData();
    } else {
      table.setData(simData.toArray(new SimViewData[simData.size()]));
    }
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

}

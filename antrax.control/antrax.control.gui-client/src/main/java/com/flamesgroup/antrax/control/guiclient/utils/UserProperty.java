/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.guiclient.MainApp;

/**
 * Property stored in registry for each user. Any time it is changed it's value
 * updated. After application restart value will be resored from registry.
 * <p>
 * Use: Call {@link #syncWithRegistry(BeansPool)} in the refresher. Call
 * {@link #getValue()} any time you need a value. Call {@link #setValue(String)}
 * any time when value is changed (it will be stored automatically on the first
 * call to {@link #syncWithRegistry(BeansPool)}
 */
public class UserProperty {
  private final String defaultValue;
  private final String registryKey;
  private volatile String value;
  private volatile String prevValue;

  public UserProperty(final String defaultValue, final String registryKey) {
    this.defaultValue = defaultValue;
    this.registryKey = String.format("antrax-gui.%s.of.%s", registryKey, MainApp.userName);
  }

  public synchronized String getValue() {
    if (value == null) {
      value = defaultValue;
    }
    return value;
  }

  public synchronized void setValue(final String value) {
    this.value = value;
  }

  public synchronized void syncWithRegistry(final BeansPool beansPool) {
    if (value == null || prevValue == null) {
      RegistryEntry[] entries = beansPool.getRegistryAccess().listEntries(registryKey, 1);
      if (entries.length > 0) {
        value = entries[0].getValue();
      } else {
        value = defaultValue;
      }
      prevValue = value;
    }

    if (prevValue == null || !prevValue.equals(value)) {
      beansPool.getRegistryAccess().add(registryKey, value, 1);
      prevValue = value;
    }
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import static org.slf4j.LoggerFactory.getLogger;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ConfigurationBean;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public final class TransactionManagerImpl implements TransactionManager {

  private static final Logger logger = getLogger(TransactionManagerImpl.class);

  private static final long TRANSACTION_LIVETIME = 15000;

  private final List<TransactionListener> listeners = new LinkedList<>();
  private RefresherThread refresherThread;
  private Integer editTransactionId;
  private volatile Thread retainThread;
  private ConfigurationBean configBean;

  public void postInitialize(final RefresherThread refresherThread, final ConfigurationBean configBean) {
    setRefresherThread(refresherThread);
    this.configBean = configBean;
  }

  public synchronized RefresherThread getRefresherThread() {
    return refresherThread;
  }

  public synchronized void setRefresherThread(final RefresherThread refresherThread) {
    this.refresherThread = refresherThread;
  }

  public synchronized void destroy(final ConfigurationBean configBean) {
    if (editTransactionId != null) {
      try {
        rollbackEditTransaction(configBean);
      } catch (NotPermittedException e) {
        // ignore
      } catch (TransactionException e) {
        // ignore
      } catch (RemoteAccessException e) {
        // ignore
      }
    }
  }

  @Override
  public void addListener(final TransactionListener l) {
    listeners.add(l);
  }

  @Override
  public void removeListener(final TransactionListener l) {
    listeners.remove(l);
  }

  @Override
  public synchronized boolean isEditTransactionStarted() {
    return (editTransactionId != null);
  }

  @Override
  public synchronized Integer getEditTransactionId() {
    return editTransactionId;
  }

  @Override
  public synchronized int getGuaranteedTransactionId() {
    return (getEditTransactionId() != null) ? getEditTransactionId() : -1;
  }

  @Override
  public void startEditTransaction(final TransactionCallbackHandler handler) {
    execTransactionAction(TransactionState.START, handler);
  }

  @Override
  public void commitEditTransaction(final TransactionCallbackHandler handler) {
    execTransactionAction(TransactionState.COMMIT, handler);
  }

  @Override
  public void rollbackEditTransaction(final TransactionCallbackHandler handler) {
    execTransactionAction(TransactionState.ROLLBACK, handler);
  }

  private void execTransactionAction(final TransactionState state, final TransactionCallbackHandler handler) {
    final TransactionRefresher transactionAction = new TransactionRefresher(state);

    ActionCallbackHandler<Object> actionHandler = new ActionCallbackHandler<Object>() {
      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        disposeRefresher(transactionAction);

        handler.onTransactionActionFailure(((TransactionRefresher) refresher).getTransactionStage(), caught);
        fireTransactionStageChanged(TransactionState.ROLLBACK);
      }

      private void disposeRefresher(final TransactionRefresher transactionAction) {
        refresherThread.removeRefresher(transactionAction);
        transactionAction.interrupt();
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final Object result) {
        disposeRefresher(transactionAction);

        handler.onTransactionActionSuccess(((TransactionRefresher) refresher).getTransactionStage());
        fireTransactionStageChanged(state);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }
    };

    transactionAction.setActionHandler(actionHandler);
    refresherThread.addRefresher(transactionAction);
    refresherThread.forceRefresh();
  }

  private void fireTransactionStageChanged(final TransactionState newStage) {
    for (TransactionListener l : listeners) {
      l.onTransactionStateChanged(newStage);
    }
  }

  private void initEditTransactionData(final int id) {
    editTransactionId = id;
    retainThread = new Thread(new TransactionRetainThread(), "TransactionRetainThread");
    retainThread.start();
  }

  private Thread destroyEditTransactionData() {
    editTransactionId = null;
    Thread rt = retainThread;
    retainThread = null;
    return rt;
  }

  private void stopRetainThread(final Thread rt) {
    if (rt != null) {
      synchronized (rt) {
        rt.notifyAll();
      }
      rt.interrupt();
    }
  }

  private void startEditTransaction(final ConfigurationBean cfgBean) throws TransactionException, NotPermittedException {
    synchronized (TransactionManagerImpl.this) {
      if (getRefresherThread() == null) {
        throw new IllegalStateException("Init " + TransactionManager.class.getSimpleName() + " before use any method");
      }

      if (getEditTransactionId() != null) {
        throw new TransactionException("Edit transaction already started");
      }

      int id = cfgBean.startEditTransaction(MainApp.clientUID);
      initEditTransactionData(id);
    }
  }

  private void commitEditTransaction(final TransactionRefresher transactionRefresher, final BeansPool beansPool, final ActionCallbackHandler<Object> handler) throws Exception {
    Thread rt = null;
    try {
      synchronized (this) {
        if (getRefresherThread() == null) {
          throw new IllegalStateException("Init " + TransactionManager.class.getSimpleName() + " before use any method");
        }

        Integer id = getEditTransactionId();
        if (id == null) {
          throw new TransactionException("Start edit transaction before commit");
        }
        List<Throwable> failedExceptions = new ArrayList<>();
        for (TransactionListener l : listeners) {
          failedExceptions.addAll(l.handleTransactionGoingToBeCommitted(id, beansPool));
        }
        if (!failedExceptions.isEmpty()) {
          failedExceptions.forEach(Throwable::printStackTrace);
          handler.onActionRefresherFailure(transactionRefresher, failedExceptions);
        }
        beansPool.getConfigBean().commitEditTransaction(MainApp.clientUID, id);
        rt = destroyEditTransactionData();
      }
    } finally {
      stopRetainThread(rt);
    }
  }

  public void rollbackEditTransaction(final ConfigurationBean cfgBean) throws NotPermittedException, TransactionException {
    Thread rt = null;
    try {
      synchronized (this) {
        if (getRefresherThread() == null) {
          throw new IllegalStateException("Start " + TransactionManager.class.getSimpleName() + " before use any method");
        }

        Integer id = getEditTransactionId();
        if (id == null) {
          throw new TransactionException("Start edit transaction before rollback");
        }

        try {
          cfgBean.rollbackEditTransaction(MainApp.clientUID, id);
        } finally {
          rt = destroyEditTransactionData();
        }
      }
    } finally {
      stopRetainThread(rt);
    }
  }

  /**
   * Refresher for asynchronous transaction actions
   */
  private class TransactionRefresher extends Refresher {
    private final TransactionState stage;
    private BeansPool beansPool;
    private ActionCallbackHandler<Object> handler;

    @Override
    public void updateGuiElements() {
      try {
        switch (stage) {
          case START:
            startEditTransaction(beansPool.getConfigBean());
            break;

          case ROLLBACK:
            rollbackEditTransaction(beansPool.getConfigBean());
            break;

          case COMMIT:
            commitEditTransaction(this, beansPool, handler);
            break;
        }
        handler.onRefreshUI(this);
        handler.onActionRefresherSuccess(this, null);
      } catch (Exception e) {
        logger.error("[{}] - when update Gui Elements", this, e);
        handler.onActionRefresherFailure(this, Collections.singletonList(e));
      }
    }

    public void setActionHandler(final ActionCallbackHandler<Object> actionHandler) {
      this.handler = actionHandler;
    }

    @Override
    protected void refreshInformation(final BeansPool beansPool) throws Exception {
      this.beansPool = beansPool;
    }

    public TransactionRefresher(final TransactionState stage) {
      super("TransactionRefresher");
      this.stage = stage;
    }

    public TransactionState getTransactionStage() {
      return stage;
    }

    @Override
    public void handleEnabled(final BeansPool beansPool) {
    }

    @Override
    public void handleDisabled(final BeansPool beansPool) {
    }
  }

  /**
   * Thread for retain transaction
   */
  private class TransactionRetainThread implements Runnable, ActionCallbackHandler<Object> {

    @Override
    public void run() {
      Thread curThread = Thread.currentThread();
      while (curThread == retainThread) {
        try {
          synchronized (this) {
            this.wait(TRANSACTION_LIVETIME);
          }
        } catch (InterruptedException ignored) {
          break;
        }

        final Integer id = getEditTransactionId();
        if (id == null || curThread != retainThread) {
          break;
        }

        if (configBean == null) {
          retainThread = null;
          break;
        }

        try {
          configBean.retainEditTransaction(MainApp.clientUID, id.intValue());
        } catch (RemoteAccessException | TransactionException | NotPermittedException e) {
          e.printStackTrace();
          onActionRefresherFailure(null, Collections.singletonList(e));
        }
      }
    }

    public void notifyRetain() {
      synchronized (this) {
        this.notifyAll();
      }
    }

    @Override
    public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
      retainThread = null;
      notifyRetain();
    }

    @Override
    public void onActionRefresherSuccess(final Refresher refresher, final Object result) {
      notifyRetain();
    }

    @Override
    public void onRefreshUI(final Refresher refresher) {
    }
  }

}

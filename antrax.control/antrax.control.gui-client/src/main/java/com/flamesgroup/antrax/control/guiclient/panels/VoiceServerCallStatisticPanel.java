/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.widgets.PhoneNumberField;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.PercentTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.VoiceServerCallStatisticTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.VoiceServerCallStatistic;

import java.awt.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;

public class VoiceServerCallStatisticPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 6846205403072147719L;

  private static final VoiceServerCallStatistic[] EMPTY_VOICE_SERVER_CALL_STATISTIC = new VoiceServerCallStatistic[0];

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final JUpdatableTable<VoiceServerCallStatistic, Integer> table = new JUpdatableTable<>(new VoiceServerCallStatisticTableBuilder());

  private final JTimePeriodSelector timePeriodSelector = new JTimePeriodSelector();

  private final PhoneNumberField prefixField = new PhoneNumberField();
  private final JComboBox<String> serverField = new JComboBox<>();

  private final String[] columnToolTips = {
      VoiceServerCallStatisticTableTooltips.getVoiceServerTooltip(),
      VoiceServerCallStatisticTableTooltips.getPrefixTooltip(),
      VoiceServerCallStatisticTableTooltips.getTotalTooltip(),
      VoiceServerCallStatisticTableTooltips.getSuccessfulTooltip(),
      VoiceServerCallStatisticTableTooltips.getDurationTooltip(),
      VoiceServerCallStatisticTableTooltips.getAsrTooltip(),
      VoiceServerCallStatisticTableTooltips.getAcdTooltip()
  };

  public VoiceServerCallStatisticPanel() {
    setLayout(new BorderLayout());

    table.setColumnToolTips(columnToolTips);

    table.setName(getClass().getSimpleName() + "_VoiceServerCallStatistic");
    table.getUpdatableTableProperties().restoreProperties();
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    timePeriodSelector.setOpaque(false);
    serverField.setEditable(true);

    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(table);
    JReflectiveBar searchPanel = new JReflectiveBar();
    searchPanel.addToRight(searchField);

    JExportCsvButton exportButton = new JExportCsvButton(table, "voiceServerCallStatistic");
    exportButton.setText("Export");

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(exportButton);
    bar.addToRight(searchField);

    add(bar, BorderLayout.PAGE_START);

    GridBagLayout callReportQueryLayout = new GridBagLayout();
    callReportQueryLayout.columnWidths = new int[] {0, 55, 35, 0, 125, 0, 0, 0, 0};
    callReportQueryLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0};
    callReportQueryLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
    callReportQueryLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

    JPanel callReportQueryPanel = new JPanel(callReportQueryLayout);

    callReportQueryPanel.add(timePeriodSelector, new GridBagConstraints(0, 0, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 0, 5, 5), 0, 0));

    callReportQueryPanel.add(new JEditLabel("Prefix"), new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
    callReportQueryPanel.add(prefixField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    callReportQueryPanel.add(new JEditLabel("Voice server"), new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    callReportQueryPanel.add(serverField, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    JButton findButton = new JButton("Find");
    callReportQueryPanel.add(findButton, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));

    JScrollPane callReportTablePanel = new JScrollPane(table);

    JPanel callReportPanel = new JPanel();
    callReportPanel.setLayout(new BorderLayout());

    callReportPanel.add(callReportQueryPanel, BorderLayout.NORTH);
    callReportPanel.add(callReportTablePanel, BorderLayout.CENTER);

    add(callReportPanel);

    findButton.addActionListener(e -> {
      refresherThread.get().addRefresher(new ActionRefresher<VoiceServerCallStatistic[]>("find voice server call statistics", refresherThread.get(), getHandler()) {

        @Override
        protected VoiceServerCallStatistic[] performAction(final BeansPool beansPool) throws Exception {
          Date fromDate = timePeriodSelector.getFromDate();
          Date toDate = timePeriodSelector.getToDate();

          String prefix = prefixField.getContent();
          String server = (String) serverField.getEditor().getItem();

          List<VoiceServerCallStatistic> voiceServerCallStatistics = beansPool.getControlBean().listVoiceServerCallStatistic(fromDate, toDate, prefix, server);
          return voiceServerCallStatistics.toArray(new VoiceServerCallStatistic[voiceServerCallStatistics.size()]);
        }

      });

    });
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refreshVoiceServers();
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
  }

  @Override
  public void release() {
    table.getUpdatableTableProperties().saveProperties();
  }

  private void setVoiceServers(final String[] voiceServers) {
    Object oldValue = serverField.getEditor().getItem();
    serverField.removeAllItems();
    if (voiceServers != null) {
      for (String voiceServer : voiceServers) {
        serverField.addItem(voiceServer);
      }
    }
    serverField.getEditor().setItem(oldValue);
  }

  private void refreshVoiceServers() {
    ActionCallbackHandler<String[]> handler = new ActionCallbackHandler<String[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        setVoiceServers(null);
        MessageUtils.showError(VoiceServerCallStatisticPanel.this, "Error", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final String[] result) {
        setVoiceServers(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    new ActionRefresher<String[]>("ReadVoiceServersRefresher", refresherThread.get(), handler) {
      @Override
      protected String[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getControlBean().listVoiceServers().stream().map(IServerData::getName).toArray(String[]::new);
      }
    }.execute();

  }

  private class VoiceServerCallStatisticTableBuilder implements TableBuilder<VoiceServerCallStatistic, Integer> {

    @Override
    public Integer getUniqueKey(final VoiceServerCallStatistic src) {
      return src.hashCode();
    }

    @Override
    public void buildRow(final VoiceServerCallStatistic src, final ColumnWriter<VoiceServerCallStatistic> dest) {
      dest.writeColumn(src.getServer());
      dest.writeColumn(src.getPrefix());
      dest.writeColumn(src.getTotal());
      dest.writeColumn(src.getSuccessful());
      dest.writeColumn(src.getDuration());
      dest.writeColumn(calculateAsr(src));
      dest.writeColumn(calculateAcd(src));
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      columns.addColumn("Voice server", String.class);
      columns.addColumn("Prefix", String.class);
      columns.addColumn("Total", Integer.class);
      columns.addColumn("Successful", Integer.class);
      columns.addColumn("Duration", Long.class).setRenderer(new TimeoutTCRenderer());
      columns.addColumn("ASR", Integer.class).setRenderer(new PercentTCRenderer());
      columns.addColumn("ACD", Long.class).setRenderer(new TimeoutTCRenderer());
    }

  }

  private int calculateAsr(final VoiceServerCallStatistic src) {
    if (src.getTotal() > 0) {
      return 100 * src.getSuccessful() / src.getTotal();
    } else {
      return 0;
    }
  }

  private long calculateAcd(final VoiceServerCallStatistic src) {
    if (src.getSuccessful() > 0) {
      return src.getDuration() / src.getSuccessful();
    } else {
      return 0;
    }
  }

  private ActionCallbackHandler<VoiceServerCallStatistic[]> getHandler() {
    return new ActionCallbackHandler<VoiceServerCallStatistic[]>() {

      private VoiceServerCallStatistic[] voiceServerCallStatistics;

      @Override
      public void onRefreshUI(final Refresher refresher) {
        table.setData(voiceServerCallStatistics);
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final VoiceServerCallStatistic[] result) {
        voiceServerCallStatistics = result;
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(VoiceServerCallStatisticPanel.this, "Error", caught.get(0));
        voiceServerCallStatistics = EMPTY_VOICE_SERVER_CALL_STATISTIC;
      }

    };

  }

}

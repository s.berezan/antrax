/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.indicators.DefaultIndicators;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.UserProperty;
import com.flamesgroup.antrax.control.swingwidgets.ExtFileFilter;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.CDRTableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.ElementSelectionListener;
import com.flamesgroup.antrax.control.swingwidgets.table.JCallPathTableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class CDRPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 84259652050517376L;
  private static final CDR[] EMPTY_CDR_LIST = new CDR[0];

  private final JTimePeriodSelector timePeriodSelector = new JTimePeriodSelector();

  private final String[] EMPTY_GROUP_NAMES = new String[0];
  private final CDRQueryBox queryBox = new CDRQueryBox(this, timePeriodSelector);
  private final JUpdatableTable<CDR, Long> table = new JUpdatableTable<>(new CDRTableBuilder());
  private final JUpdatableTable<CallPath, String> callPathTable = new JUpdatableTable<>(new JCallPathTableBuilder(timePeriodSelector));
  private RefresherThread refresherThread;
  private final DefaultIndicators indicators = new DefaultIndicators();
  private final AtomicReference<UserProperty> sourceDirectory = new AtomicReference<>(new UserProperty(System.getProperty("user.home"), "scripts.source-dir"));

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
  }

  @Override
  public void release() {
    queryBox.release();
    table.getUpdatableTableProperties().saveProperties();
    callPathTable.getUpdatableTableProperties().saveProperties();
  }

  public CDRPanel() {
    createGUI();
    setupListeners();

    setTransferHandler(queryBox.getTransferHandler());
    table.setName(getClass().getSimpleName() + "_CDR");
    callPathTable.setName(getClass().getSimpleName() + "_CallPath");
    table.getUpdatableTableProperties().restoreProperties();
    callPathTable.getUpdatableTableProperties().restoreProperties();
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refreshSIMGroups();
      refreshGSMGroups();
    }
  }

  private void createGUI() {
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(table, "main table");
    searchField.registerSearchItem(callPathTable, "call path table");
    JButton clearQueriesBut = new JReflectiveButton.JReflectiveButtonBuilder().setText("Clear queries").build();
    JButton clearFilterButtons = new JReflectiveButton.JReflectiveButtonBuilder().setText("Clear filters").build();
    JButton exportAllButtons = new JReflectiveButton.JReflectiveButtonBuilder().setIcon(IconPool.getShared("/img/doc_csv.png")).setToolTipText("Export to CSV all found results")
        .addActionListener(createActionListenerExportFile()).setText("Export").build();

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(exportAllButtons);
    bar.addToLeft(clearQueriesBut);
    bar.addToLeft(clearFilterButtons);
    bar.addToRight(searchField);

    JPanel callPathTablePanel = new JPanel(new BorderLayout());
    JReflectiveBar nameBar = new JReflectiveBar();
    nameBar.addToLeft(new JLabel("Call path info"));
    callPathTablePanel.add(nameBar, BorderLayout.NORTH);
    callPathTablePanel.add(new JScrollPane(callPathTable), BorderLayout.CENTER);
    callPathTablePanel.add(indicators.getComponent(), BorderLayout.SOUTH);
    clearFilterButtons.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        queryBox.clearFilters();
      }
    });
    clearQueriesBut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        table.clearData();
        callPathTable.clearData();
      }
    });
    JPanel mainPanel = new JPanel(new BorderLayout());
    mainPanel.add(bar, BorderLayout.PAGE_START);
    mainPanel.add(queryBox, BorderLayout.CENTER);

    mainPanel.setOpaque(true);

    JSplitPane splitSubPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(table), callPathTablePanel);
    splitSubPane.setDividerLocation(250);

    JSplitPane p = new JSplitPane(JSplitPane.VERTICAL_SPLIT, mainPanel, splitSubPane);
    GroupLayout l = new GroupLayout(this);
    setLayout(l);
    l.setAutoCreateContainerGaps(false);
    l.setAutoCreateGaps(true);
    l.setHorizontalGroup(l.createSequentialGroup().addComponent(p/* splitPane */));
    l.setVerticalGroup(l.createSequentialGroup().addComponent(p/* splitPane */));

  }

  private void setupListeners() {
    table.addElementSelectionListener(new ElementSelectionListener<CDR>() {
      @Override
      public void handleSelectionChanged(final List<CDR> selectedElem, final List<CDR> prevSelection) {
        if (selectedElem == null) {
          return;
        }

        if (selectedElem.isEmpty()) {
          callPathTable.clearData();
          return;
        }
        List<CallPath> callPath = selectedElem.get(0).getCallPath();
        callPathTable.setData(callPath.toArray(new CallPath[callPath.size()]));
      }
    });

    queryBox.addQueryListener(new CDRQueryBox.CDRQueryActionListener() {
      @Override
      public boolean handleQueryAction(final CDRQueryBox.CDRQuery query, final boolean appendResult, final boolean waitData) {
        return executeQuery(query, appendResult, waitData);
      }
    });
    callPathTable.addElementSelectionListener(new ElementSelectionListener<CallPath>() {

      @Override
      public void handleSelectionChanged(final List<CallPath> currSelection, final List<CallPath> prevSelection) {
        indicators.setSelected(currSelection.size());
      }

    });

    callPathTable.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent e) {
        indicators.setTotal(callPathTable.getModel().getRowCount());
      }
    });

    callPathTable.getRowSorter().addRowSorterListener(new RowSorterListener() {

      @Override
      public void sorterChanged(final RowSorterEvent e) {
        indicators.setFiltered(callPathTable.getRowSorter().getViewRowCount());

      }

    });

  }

  private boolean executeQuery(final CDRQueryBox.CDRQuery query, final boolean appendResult, final boolean waitData) {
    if (!appendResult) {
      table.clearData();
    }

    final AtomicReference<CDR[]> retval = new AtomicReference<>(null);
    ActionCallbackHandler<CDR[]> handler = new ActionCallbackHandler<CDR[]>() {
      private CDR[] cdrs;

      @Override
      public void onRefreshUI(final Refresher refresher) {
        table.setData(cdrs);
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final CDR[] result) {
        cdrs = result;
        retval.set(result);
        synchronized (retval) {
          retval.notifyAll();
        }
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(CDRPanel.this, "Error", caught.get(0));
        cdrs = EMPTY_CDR_LIST;
        synchronized (retval) {
          retval.notifyAll();
        }
      }
    };

    new ActionRefresher<CDR[]>("CDRQuery", refresherThread, handler) {
      @Override
      protected CDR[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getControlBean()
            .listCDRs(query.getFromDate(), query.getToDate(), query.getGSMGroup(), query.getSIMGroup(), query.getCallerPhoneNumber(), query.getCalledPhoneNumber(), query.getSimUID(),
                query.getPageSize(), query.getOffset());
      }
    }.execute();

    if (waitData) {
      if (retval.get() == null) {
        synchronized (retval) {
          try {
            retval.wait();
          } catch (InterruptedException ignored) {
          }
        }
      }
      return (retval.get() != null && retval.get().length > 0);
    } else {
      return false;
    }
  }

  private ActionCallbackHandler<CDR[]> getHandler() {
    return new ActionCallbackHandler<CDR[]>() {

      private CDR[] cdrs;

      @Override
      public void onRefreshUI(final Refresher refresher) {
        table.setData(cdrs);
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final CDR[] result) {
        cdrs = result;
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(CDRPanel.this, "Error", caught.get(0));
        cdrs = EMPTY_CDR_LIST;
      }
    };
  }

  public void showCDR(final long cdrID) {
    table.clearData();
    new ActionRefresher<CDR[]>("CDRQueryById", refresherThread, getHandler()) {

      @Override
      protected CDR[] performAction(final BeansPool beansPool) throws Exception {
        CDR cdr = beansPool.getControlBean().getCDR(cdrID);
        return (cdr == null) ? EMPTY_CDR_LIST : new CDR[] {cdr};
      }
    }.execute();
  }

  private void refreshSIMGroups() {
    // Read SIM group names
    ActionCallbackHandler<String[]> handler = new ActionCallbackHandler<String[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        queryBox.setSimGroups(EMPTY_GROUP_NAMES);
        MessageUtils.showError(CDRPanel.this, "Error", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final String[] result) {
        queryBox.setSimGroups(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    new ActionRefresher<String[]>("ReadSimGroupsRefresher", refresherThread, handler) {
      @Override
      protected String[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getConfigBean().listSimGroupNames(MainApp.clientUID);
      }
    }.execute();
  }

  private void refreshGSMGroups() {
    // Read GSM group names
    ActionCallbackHandler<String[]> handler = new ActionCallbackHandler<String[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        queryBox.setGsmGroups(EMPTY_GROUP_NAMES);
        MessageUtils.showError(CDRPanel.this, "Error", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final String[] result) {
        queryBox.setGsmGroups(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    new ActionRefresher<String[]>("ReadGsmGroupsRefresher", refresherThread, handler) {
      @Override
      protected String[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getConfigBean().listGsmGroupNames(MainApp.clientUID);
      }
    }.execute();
  }

  private ActionListener createActionListenerExportFile() {
    return e -> {

      File file = selectFile("Select file for export");

      if (file == null) {
        return;
      }

      ExecutiveRefresher refresher = new ExecutiveRefresher("ExportToFile", refresherThread, null, this);
      refresher.addExecution(new RefresherExecution() {

        private final Integer EXPORT_PAGE_SIZE = 1000;
        private final String DELIMITER = ",";
        private final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        private final Format durationFormatter = new SimpleDateFormat("mm:ss");
        private final Calendar cal = Calendar.getInstance();

        @Override
        public String describeExecution() {
          return "Export all CDR";
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          WaitingDialog wd = new WaitingDialog(IconPool.getShared("/img/doc_csv.png"), MainApp.getAppName(), "Export all CDR");
          wd.show(CDRPanel.this, () -> {
            StringBuilder exportedDataAtCSV = new StringBuilder();
            exportedDataAtCSV.append("server").append(DELIMITER);
            exportedDataAtCSV.append("caller number").append(DELIMITER);
            exportedDataAtCSV.append("called number").append(DELIMITER);
            exportedDataAtCSV.append("setup").append(DELIMITER);
            exportedDataAtCSV.append("start").append(DELIMITER);
            exportedDataAtCSV.append("stop").append(DELIMITER);
            exportedDataAtCSV.append("duration").append(DELIMITER);
            exportedDataAtCSV.append("pdd").append(DELIMITER);
            exportedDataAtCSV.append("ring time").append(DELIMITER);
            exportedDataAtCSV.append("call type").append(DELIMITER);
            exportedDataAtCSV.append("drop reason").append(DELIMITER);
            exportedDataAtCSV.append("drop code");
            exportedDataAtCSV.append("\n");

            CDRQueryBox.CDRQuery cdrQuery = queryBox.collectQuery();
            int offset = 0;
            try {
              while (true) {
                CDR[] cdrs = beansPool.getControlBean()
                    .listCDRs(cdrQuery.getFromDate(), cdrQuery.getToDate(), cdrQuery.getGSMGroup(), cdrQuery.getSIMGroup(),
                        cdrQuery.getCallerPhoneNumber(), cdrQuery.getCalledPhoneNumber(), cdrQuery.getSimUID(), EXPORT_PAGE_SIZE, offset);
                if (cdrs.length == 0) {
                  break;
                }
                offset += EXPORT_PAGE_SIZE;
                for (CDR cdr : cdrs) {
                  exportedDataAtCSV.append(cdr.getVoiceServerName()).append(DELIMITER);
                  exportedDataAtCSV.append(cdr.getCallerPhoneNumber() == null ? "" : cdr.getCallerPhoneNumber().getValue()).append(DELIMITER);
                  exportedDataAtCSV.append(cdr.getCalledPhoneNumber() == null ? "" : cdr.getCalledPhoneNumber().getValue()).append(DELIMITER);
                  exportedDataAtCSV.append(calculateTime(cdr.getSetupTime())).append(DELIMITER);
                  exportedDataAtCSV.append(calculateTime(cdr.getStartTime())).append(DELIMITER);
                  exportedDataAtCSV.append(calculateTime(cdr.getStopTime())).append(DELIMITER);
                  exportedDataAtCSV.append(TimeUtils.writeTime(cdr.getDuration())).append(DELIMITER);
                  exportedDataAtCSV.append(TimeUtils.writeTime(cdr.getPDD())).append(DELIMITER);
                  exportedDataAtCSV.append(TimeUtils.writeTime(cdr.getRingTime())).append(DELIMITER);
                  exportedDataAtCSV.append(cdr.getCallType() == null ? "" : cdr.getCallType()).append(DELIMITER);
                  exportedDataAtCSV.append(cdr.getDropReason() == null ? "" : cdr.getDropReason()).append(DELIMITER);
                  exportedDataAtCSV.append(cdr.getDropCode());
                  exportedDataAtCSV.append("\n");
                }
              }
              try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
                writer.write(exportedDataAtCSV.toString());
              }
              MessageUtils.showInfo(CDRPanel.this, MainApp.getAppName(), "Export completed");
            } catch (Exception e) {
              e.printStackTrace();
              MessageUtils.showError(CDRPanel.this, "Error while, export CDR ", e);
            } finally {
              refresherThread.removeRefresher(refresher);
            }
          });
        }

        @Override
        public void updateGUIComponent() {
        }

        private String calculateTime(final long time) {
          if (time == 0) {
            return " - no - ";
          } else {
            cal.setTimeInMillis(time);
            return formatter.format(cal.getTime());
          }
        }

      });
      refresherThread.addRefresher(refresher);
      refresherThread.forceRefresh();
    };
  }

  private File selectFile(final String title) {
    JFileChooser chooser = new JFileChooser();
    chooser.setDialogTitle(title);
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setAcceptAllFileFilterUsed(false);
    chooser.setFileFilter(new ExtFileFilter(".csv", "*.csv files"));
    chooser.setMultiSelectionEnabled(false);
    chooser.setSelectedFile(new File(generateFileName()));

    if (chooser.showSaveDialog(CDRPanel.this) == JFileChooser.APPROVE_OPTION) {
      sourceDirectory.get().setValue(chooser.getCurrentDirectory().getAbsolutePath());
      return chooser.getSelectedFile();
    } else {
      return null;
    }
  }

  private String generateFileName() {
    Format formatter = new SimpleDateFormat("dd.MM.yy_HH.mm.ss");
    String strDate = formatter.format(new Date());
    String filename = "cdr-" + strDate + ".csv";
    filename = filename.replaceAll(" ", "_");
    return filename;
  }

  public void dropSimChannel(final SimViewData simViewData) {
    Calendar from = Calendar.getInstance();
    from.set(Calendar.HOUR_OF_DAY, 0);
    from.set(Calendar.MINUTE, 0);
    from.set(Calendar.SECOND, 0);
    from.set(Calendar.MILLISECOND, 0);

    Calendar to = Calendar.getInstance();
    to.set(Calendar.HOUR_OF_DAY, 23);
    to.set(Calendar.MINUTE, 59);
    to.set(Calendar.SECOND, 59);
    to.set(Calendar.MILLISECOND, 0);
    queryBox.generateQuery(simViewData.getUid(), from.getTime(), to.getTime(), simViewData.getSimGroupName());
  }

}

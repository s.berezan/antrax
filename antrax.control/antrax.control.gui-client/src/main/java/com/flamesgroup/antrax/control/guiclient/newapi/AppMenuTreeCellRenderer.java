/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.swingwidgets.ColorUtils;

import java.awt.*;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

public class AppMenuTreeCellRenderer implements TreeCellRenderer {
  private final DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
  private static final String INFO_COLOR = "blue";
  private static final String INFO_COLOR_SEL = "#AFDCEC";
  private static final String NOTICE_COLOR = "#696969";
  private static final String NOTICE_COLOR_SEL = "#B4B4B4";
  private static final String WARNING_COLOR = "#F88017";
  private static final String WARNING_COLOR_SEL = "#F4A913";
  private static final String ERROR_COLOR = "red";
  private static final String ERROR_COLOR_SEL = "ff0000";

  public AppMenuTreeCellRenderer() {
    defaultRenderer.setBackgroundNonSelectionColor(ColorUtils.darker(defaultRenderer.getBackgroundNonSelectionColor()));
    defaultRenderer.setBackgroundSelectionColor(ColorUtils.darker(defaultRenderer.getBackgroundSelectionColor()));
  }

  @Override
  public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean selected, final boolean expanded, final boolean leaf, final int row, final boolean hasFocus) {
    return getTreeCellRendererComponent(tree, (AppMenuNode) value, selected, expanded, leaf, row, hasFocus);
  }

  public Component getTreeCellRendererComponent(final JTree tree, final AppMenuNode value, final boolean selected, final boolean expanded, final boolean leaf, final int row, final boolean hasFocus) {
    defaultRenderer.setLeafIcon(value.getIcon());
    defaultRenderer.setOpenIcon(value.getIcon());
    defaultRenderer.setClosedIcon(value.getIcon());
    defaultRenderer.setToolTipText(getTooltip(value));
    return defaultRenderer.getTreeCellRendererComponent(tree, buildNodeValue(value, selected), selected, expanded, leaf, row, hasFocus);
  }

  private String getTooltip(final AppMenuNode value) {
    if (value == null || value.getBadge() == null) {
      return null;
    }
    Badge badge = value.getBadge();
    if (badge.getInfoHelp() == null && badge.getNoticeHelp() == null && badge.getErrorHelp() == null && badge.getWarningHelp() == null) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    builder.append("<html>");
    builder.append("<body>");
    if (badge.getInfoHelp() != null) {
      builder.append("<b><font color=").append(INFO_COLOR).append(">").append("Information: ").append("</font></b>");
      builder.append(badge.getInfoHelp().replaceAll("\n", "<br></br>"));
      builder.append("<br></br>");
    }
    if (badge.getNoticeHelp() != null) {
      builder.append("<b><font color=").append(NOTICE_COLOR).append(">").append("Notice: ").append("</font></b>");
      builder.append(badge.getNoticeHelp().replaceAll("\n", "<br></br>"));
      builder.append("<br></br>");
    }
    if (badge.getWarningHelp() != null) {
      builder.append("<b><font color=").append(WARNING_COLOR).append(">").append("Warning: ").append("</font></b>");
      builder.append(badge.getWarningHelp().replaceAll("\n", "<br></br>"));
      builder.append("<br></br>");
    }
    if (badge.getErrorHelp() != null) {
      builder.append("<b><font color=").append(ERROR_COLOR).append(">").append("Error: ").append("</font></b>");
      builder.append(badge.getErrorHelp().replaceAll("\n", "<br></br>"));
    }
    builder.append("</body>");
    builder.append("</html>");
    return builder.toString();
  }

  private String buildNodeValue(final AppMenuNode value, final boolean selected) {
    if (value == null) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    builder.append("<html>");
    builder.append("<body>");
    builder.append(value.getText());
    if (value.getBadge() != null) {
      appendBadge(builder, value.getBadge().getInfo(), selected ? INFO_COLOR_SEL : INFO_COLOR);
      appendBadge(builder, value.getBadge().getNotice(), selected ? NOTICE_COLOR_SEL : NOTICE_COLOR);
      appendBadge(builder, value.getBadge().getWarning(), selected ? WARNING_COLOR_SEL : WARNING_COLOR);
      appendBadge(builder, value.getBadge().getError(), selected ? ERROR_COLOR_SEL : ERROR_COLOR);
    }

    builder.append("</body>");
    builder.append("</html");
    return builder.toString();
  }

  private void appendBadge(final StringBuilder builder, final String text, final String color) {
    if (text == null) {
      return;
    }
    builder.append(" <font ");
    builder.append(" color=");
    builder.append(color);
    builder.append(">(");
    builder.append(text);
    builder.append(")</font> ");
  }
}

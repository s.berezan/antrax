/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.panels.ivr.IvrTemplatesRefresher;
import com.flamesgroup.antrax.control.guiclient.panels.ivr.IvrTemplatesServerTable;
import com.flamesgroup.antrax.control.guiclient.panels.ivr.IvrTemplatesSimGroupTable;
import com.flamesgroup.antrax.control.guiclient.panels.ivr.IvrTemplatesTable;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Comparator;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;

public class IvrTemplatesPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 4415824450007347677L;

  private final Logger logger = LoggerFactory.getLogger(IvrTemplatesPanel.class);

  private final IvrTemplatesTable ivrTemplatesTable = new IvrTemplatesTable();
  private final IvrTemplatesServerTable serverTable = new IvrTemplatesServerTable();
  private final IvrTemplatesSimGroupTable simGroupTable = new IvrTemplatesSimGroupTable();
  private RefresherThread refresherThread;
  private IvrTemplatesRefresher refresher;

  public IvrTemplatesPanel() {

    JPanel simGroupPanel = new JPanel();
    simGroupPanel.setLayout(new BorderLayout());
    simGroupPanel.add(createSimGroupToolbar(), BorderLayout.PAGE_START);
    simGroupPanel.add(new JScrollPane(simGroupTable), BorderLayout.CENTER);

    JSplitPane subSplitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(serverTable), simGroupPanel);
    subSplitPanel.setDividerLocation(150);

    JPanel templatesPanel = new JPanel();
    templatesPanel.setLayout(new BorderLayout());
    templatesPanel.add(createTemplatesToolbar(), BorderLayout.PAGE_START);
    templatesPanel.add(new JScrollPane(ivrTemplatesTable), BorderLayout.CENTER);

    JSplitPane splitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, subSplitPanel, templatesPanel);
    splitPanel.setDividerLocation(300);

    setLayout(new BorderLayout());

    add(splitPanel, BorderLayout.CENTER);
    add(createToolbar(), BorderLayout.PAGE_START);

    initializeListeners();
    ivrTemplatesTable.setName(getClass().getSimpleName() + "_ivrTemplatesTable");
    serverTable.setName(getClass().getSimpleName() + "_serverTable");
    simGroupTable.setName(getClass().getSimpleName() + "_simGroupTable");
    ivrTemplatesTable.getUpdatableTableProperties().restoreProperties();
    serverTable.getUpdatableTableProperties().restoreProperties();
    simGroupTable.getUpdatableTableProperties().restoreProperties();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {

  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
    this.refresher = new IvrTemplatesRefresher(serverTable, simGroupTable, ivrTemplatesTable, refresherThread, transactionManager, this);
    this.refresher.init();
  }

  @Override
  public void release() {
    ivrTemplatesTable.getUpdatableTableProperties().saveProperties();
    serverTable.getUpdatableTableProperties().saveProperties();
    simGroupTable.getUpdatableTableProperties().saveProperties();
    if (refresher != null) {
      this.refresher.interrupt();
    }
  }

  private void initializeListeners() {
    logger.debug("[{}] - setup listeners", this);
    serverTable.getSelectionModel().addListSelectionListener(this::valueChanged);
    simGroupTable.getSelectionModel().addListSelectionListener(this::valueChanged);
  }

  private Component createToolbar() {
    JContextSearch cs = new JContextSearch();
    cs.registerSearchItem(ivrTemplatesTable, "ivrTemplatesTable");
    cs.registerSearchItem(serverTable, "serverTable");
    cs.registerSearchItem(simGroupTable, "simGroupTable");

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToRight(cs);
    return bar;
  }

  private void valueChanged(final ListSelectionEvent e) {
    if (refresher.isUpdating()) {
      return;
    }
    if (e.getValueIsAdjusting()) {
      return;
    }
    simGroupTable.setEnabled(false);
    ivrTemplatesTable.setEnabled(false);

    refresherThread.forceRefresh();
  }

  private Component createSimGroupToolbar() {
    JReflectiveBar jReflectiveBar = new JReflectiveBar();
    jReflectiveBar.addToLeft(createNewSimGroupButton());
    jReflectiveBar.addToLeft(removeSimGroupButton());
    return jReflectiveBar;
  }

  private Component createNewSimGroupButton() {
    return createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"), "Create a directory for store IVR templates of Sim group", e -> {
      if (serverTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please a select server");
        return;
      }

      Object[] allSimGroups = refresher.getAllSimGroups();

      String newSimGroup = (String) JOptionPane.showInputDialog(
          this,
          "Please choose Sim group",
          "Create a directory for store IVR templates of Sim group",
          JOptionPane.PLAIN_MESSAGE,
          null,
          allSimGroups,
          null);

      if ((newSimGroup != null) && (newSimGroup.length() > 0)) {
        if (simGroupTable.getElems().contains(newSimGroup)) {
          MessageUtils.showInfo(this, "Duplicate", "SIM group with name: " + newSimGroup + " already exists");
          return;
        }
        refresher.createSimGroup(newSimGroup);
      }
    });
  }

  private Component removeSimGroupButton() {
    return createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"), "Remove current directory of SIM group", e -> {
      if (serverTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select a server");
        return;
      }

      if (simGroupTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select at least one SIM group");
        return;
      }

      int option = JOptionPane.showConfirmDialog(
          this,
          "All selected SIM groups and templates in it will be deleted",
          "Remove selected SIM group(s)",
          JOptionPane.YES_NO_OPTION);
      if (option == JOptionPane.YES_OPTION) {
        refresher.removeSimGroup(simGroupTable.getSelectedElems());
      }

    });
  }

  private Component createTemplatesToolbar() {
    JReflectiveBar jReflectiveBar = new JReflectiveBar();
    jReflectiveBar.addToLeft(createNewTemplateButton());
    jReflectiveBar.addToLeft(removeTemplateButton());
    return jReflectiveBar;
  }

  private Component createNewTemplateButton() {
    return createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"), "Create new IVR template", e -> {
      if (serverTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select a server");
        return;
      }

      if (simGroupTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select a SIM group");
        return;
      }
      JFileChooser fileChooser = new JFileChooser();
      FileNameExtensionFilter filter = new FileNameExtensionFilter("sln file", "sln");
      fileChooser.setFileFilter(filter);
      File selectedFile;
      if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
        selectedFile = fileChooser.getSelectedFile();
        Object[] possibilities = refresher.getCallDropReasons().entrySet().stream().sorted(Comparator.comparingInt(Map.Entry::getValue)).map(c -> new DropCode(c.getKey(), c.getValue())).toArray();
        DropCode s = (DropCode) JOptionPane.showInputDialog(
            this,
            "Please select drop code for ivr detect",
            "Drop code",
            JOptionPane.PLAIN_MESSAGE,
            null,
            possibilities,
            null);

        if (s != null) {
          String fileName = selectedFile.getName();
          fileName = fileName.substring(0, fileName.length() - 4);
          fileName = fileName.replaceAll("[^a-zA-Z0-9_]", "");
          fileName = fileName + "#" + s.getCode() + ".sln";
          refresher.uploadFileToVs(selectedFile, fileName);
        }
      }
    });
  }

  private Component removeTemplateButton() {
    return createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"), "Remove current IVR template", e -> {
      if (serverTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select a server");
        return;
      }

      if (simGroupTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select a SIM group");
        return;
      }

      if (ivrTemplatesTable.getSelectedElems().isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select at least one IVR template");
        return;
      }

      int option = JOptionPane.showConfirmDialog(
          this,
          "All selected IVR templates will be deleted",
          "Remove selected IVR template(s)",
          JOptionPane.YES_NO_OPTION);
      if (option == JOptionPane.YES_OPTION) {
        refresher.removeIvrTemplatesFromVs(ivrTemplatesTable.getSelectedElems());
      }
    });
  }

  private JButton createToolbarButton(final Icon icon, final String title, final ActionListener l) {
    JButton button = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
    button.setToolTipText(title);
    button.addActionListener(l);
    button.setFocusable(false);
    return button;
  }

  private class DropCode {
    private final String name;
    private final int code;

    public DropCode(final String name, final int code) {
      this.name = name;
      this.code = code;
    }

    public String getName() {
      return name;
    }

    public int getCode() {
      return code;
    }

    @Override
    public String toString() {
      return code + " " + name;
    }
  }

}

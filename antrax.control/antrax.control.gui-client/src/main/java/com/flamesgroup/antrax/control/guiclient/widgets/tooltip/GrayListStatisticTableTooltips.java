/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

public class GrayListStatisticTableTooltips {

  private GrayListStatisticTableTooltips() {
  }

  public static String getNumberTooltip() {
    return new TooltipHeaderBuilder("Gray number").build();
  }

  public static String getStatusTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("The reason for which the number is in the table");
    TooltipHeaderBuilder.TextTooltipHeader textTable = builder.createTextTable();
    textTable.addValue(VoipAntiSpamListNumbersStatus.ROUTING.name(), "Added by routing filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.ACD.name(), "Added manually by acd filter configs");
    return builder.build();
  }

  public static String getBlockTimeTooltip() {
    return new TooltipHeaderBuilder("Time of blocking number").build();
  }

  public static String getBlockCountTooltip() {
    return new TooltipHeaderBuilder("Count of blocking number before move to black list").build();
  }

  public static String getBlockTimeLeftTooltip() {
    return new TooltipHeaderBuilder("Time after which blocking of the number will expire").build();
  }

  public static String getRoutingRequestCountTooltip() {
    return new TooltipHeaderBuilder("Count of requests on routing for number").build();
  }

}

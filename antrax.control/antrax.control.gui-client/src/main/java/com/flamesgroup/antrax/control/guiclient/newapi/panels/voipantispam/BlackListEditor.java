/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.BlackListConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.BlackListConfig;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class BlackListEditor extends JPanel {

  private static final long serialVersionUID = 6770256474741020611L;

  private final BlackListTable table = new BlackListTable();

  private Component addButton;
  private Component removeButton;
  private Component editButton;

  private final String[] columnToolTips = {
      BlackListConfigTableTooltips.getPeriodTooltip(),
      BlackListConfigTableTooltips.getMaxRoutingRequestPerPeriodTooltip(),
  };

  public BlackListEditor() {
    super(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    add(new JScrollPane(table), BorderLayout.CENTER);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      BlackListConfig blackListConfig = BlackListDialog.createNewBlackListConfig(retval);
      if (blackListConfig != null) {
        table.insertElem(blackListConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      BlackListConfig blackListConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (blackListConfig == null) {
        return;
      }
      BlackListConfig editBlackListConfig = BlackListDialog.createEditBlackListConfig(retval, blackListConfig);
      table.updateElemAt(editBlackListConfig, selectedRow);
    });
    return retval;
  }

  public java.util.List<BlackListConfig> getBlackListConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setBlackListConfigs(final java.util.List<BlackListConfig> blackListConfigs) {
    table.setData(blackListConfigs.toArray(new BlackListConfig[blackListConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class BlackListTable extends JUpdatableTable<BlackListConfig, Integer> {

    private static final long serialVersionUID = 1394243525062439643L;

    public BlackListTable() {
      super(new TableBuilder<BlackListConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final BlackListConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final BlackListConfig src, final ColumnWriter<BlackListConfig> dest) {
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxRoutingRequestPerPeriod());
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max routing request per period", Integer.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = -423779679608563529L;

        public String getToolTipText(final MouseEvent e) {
          java.awt.Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.VoipAlertingAnalyzeConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class VoipAlertingEditor extends JPanel {

  private static final long serialVersionUID = 9173893203772410071L;

  private final JCheckBox enable = new JCheckBox("Enable");
  private final VoipAlertingTable table = new VoipAlertingTable();
  private final JScrollPane tablePane = new JScrollPane(table);

  private Component addButton;
  private Component removeButton;
  private Component editButton;

  private final String[] columnToolTips = {
      VoipAlertingAnalyzeConfigTableTooltips.getVoipAlertingTimeTooltip(),
      VoipAlertingAnalyzeConfigTableTooltips.getPeriodTooltip(),
      VoipAlertingAnalyzeConfigTableTooltips.getMaxRoutingRequestPerPeriodTooltip()
  };

  public VoipAlertingEditor() {
    super(new BorderLayout());

    JReflectiveBar bar = createToolbar();
    add(bar, BorderLayout.NORTH);

    setEditorVisible(false);
    enable.addItemListener(e -> setEditorVisible(e.getStateChange() == ItemEvent.SELECTED));
    add(tablePane, BorderLayout.CENTER);
  }

  private void setEditorVisible(final boolean visible) {
    addButton.setVisible(visible);
    removeButton.setVisible(visible);
    editButton.setVisible(visible);
    tablePane.setVisible(visible);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());

    retval.addToRight(enable);
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      VoipAlertingConfig voipAlertingConfig = VoipAlertingDialog.createNewVoipAlertingConfig(retval);
      if (voipAlertingConfig != null) {
        table.insertElem(voipAlertingConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      VoipAlertingConfig voipAlertingConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (voipAlertingConfig == null) {
        return;
      }
      VoipAlertingConfig editVoipAlertingConfig = VoipAlertingDialog.createEditVoipAlertingConfig(retval, voipAlertingConfig);
      table.updateElemAt(editVoipAlertingConfig, selectedRow);
    });
    return retval;
  }

  public boolean isVoipAlertingEnabled() {
    return enable.isSelected();
  }

  public void setVoipAlertingEnabled(final boolean enabled) {
    setEditorVisible(enabled);
    enable.setSelected(enabled);
  }

  public java.util.List<VoipAlertingConfig> getVoipAlertingConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setVoipAlertingConfigs(final java.util.List<VoipAlertingConfig> voipAlertingConfigs) {
    table.setData(voipAlertingConfigs.toArray(new VoipAlertingConfig[voipAlertingConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    enable.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class VoipAlertingTable extends JUpdatableTable<VoipAlertingConfig, Integer> {

    private static final long serialVersionUID = -4155647707894299462L;

    public VoipAlertingTable() {
      super(new TableBuilder<VoipAlertingConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final VoipAlertingConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final VoipAlertingConfig src, final ColumnWriter<VoipAlertingConfig> dest) {
          dest.writeColumn(src.getAlertingTime());
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxRoutingRequestPerPeriod());
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("VOIP Alerting time", Integer.class);
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max routing request per period", Integer.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = -1895253457387071357L;

        public String getToolTipText(final MouseEvent e) {
          java.awt.Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }

  }

}

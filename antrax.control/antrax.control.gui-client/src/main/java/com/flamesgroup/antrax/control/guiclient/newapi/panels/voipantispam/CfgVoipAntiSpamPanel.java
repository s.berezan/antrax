/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class CfgVoipAntiSpamPanel extends JPanel implements AppPanel, KeyListener {

  private static final long serialVersionUID = 2911791045630652815L;

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final AtomicReference<TransactionManager> transactionManager = new AtomicReference<>();
  private final AtomicReference<Refresher> refresher = new AtomicReference<>();

  private final JCheckBox enableCheckBox = new JCheckBox("Enable Voip Anti Spam");

  private final GrayListEditor grayListEditor = new GrayListEditor();
  private final BlackListEditor blackListEditor = new BlackListEditor();

  private final GsmAlertingEditor gsmAlertingEditor = new GsmAlertingEditor();
  private final VoipAlertingEditor voipAlertingEditor = new VoipAlertingEditor();
  private final FasEditor fasEditor = new FasEditor();

  private VoipAntiSpamConfiguration voipAntiSpamConfiguration;

  public CfgVoipAntiSpamPanel() {
    initConfigComponents();
  }

  private void initConfigComponents() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    grayListEditor.setBorder(BorderFactory.createTitledBorder("Gray List"));
    blackListEditor.setBorder(BorderFactory.createTitledBorder("Black List"));

    gsmAlertingEditor.setBorder(BorderFactory.createTitledBorder("GSM Alerting Analyze"));
    voipAlertingEditor.setBorder(BorderFactory.createTitledBorder("VOIP Alerting Analyze"));
    fasEditor.setBorder(BorderFactory.createTitledBorder("FAS Analyze"));

    enableCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    grayListEditor.setAlignmentX(Component.LEFT_ALIGNMENT);
    blackListEditor.setAlignmentX(Component.LEFT_ALIGNMENT);
    gsmAlertingEditor.setAlignmentX(Component.LEFT_ALIGNMENT);
    voipAlertingEditor.setAlignmentX(Component.LEFT_ALIGNMENT);
    fasEditor.setAlignmentX(Component.LEFT_ALIGNMENT);

    enableCheckBox.setBorder(new EmptyBorder(10, 5, 10, 0));

    add(enableCheckBox);
    add(grayListEditor);
    add(blackListEditor);
    add(gsmAlertingEditor);
    add(voipAlertingEditor);
    add(fasEditor);
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
    this.refresher.set(createRefresher());
    this.transactionManager.set(transactionManager);
    this.transactionManager.get().addListener(new TransactionListener() {

      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
        refresherThread.get().forceRefresh();
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        voipAntiSpamConfiguration.setEnable(enableCheckBox.getModel().isSelected());

        voipAntiSpamConfiguration.setGrayListConfigs(grayListEditor.getGrayListConfigs());
        voipAntiSpamConfiguration.setBlackListConfigs(blackListEditor.getBlackListConfigs());

        voipAntiSpamConfiguration.setGsmAlertingAnalyze(gsmAlertingEditor.isGsmAlertingEnabled());
        voipAntiSpamConfiguration.setGsmAlertingConfigs(gsmAlertingEditor.getGsmAlertingConfigs());
        voipAntiSpamConfiguration.setVoipAlertingAnalyze(voipAlertingEditor.isVoipAlertingEnabled());
        voipAntiSpamConfiguration.setVoipAlertingConfigs(voipAlertingEditor.getVoipAlertingConfigs());
        voipAntiSpamConfiguration.setFasAnalyze(fasEditor.isFasEnabled());
        voipAntiSpamConfiguration.setFasConfigs(fasEditor.getFasConfigs());

        List<Throwable> throwables = new ArrayList<>();
        try {
          beansPool.getConfigBean().saveVoipAntiSpamConfiguration(MainApp.clientUID, transactionId, voipAntiSpamConfiguration);
        } catch (Exception e) {
          throwables.add(e);
        }
        return throwables;
      }
    });
  }

  @Override
  public void release() {
    if (refresher.get() != null) {
      this.refresher.get().interrupt();
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
    enableCheckBox.setEnabled(editable);

    grayListEditor.setEnabled(editable);
    blackListEditor.setEnabled(editable);

    gsmAlertingEditor.setEnabled(editable);
    voipAlertingEditor.setEnabled(editable);
    fasEditor.setEnabled(editable);
  }


  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.get().addRefresher(refresher.get());
    } else {
      refresherThread.get().removeRefresher(refresher.get());
    }
  }

  private Refresher createRefresher() {
    return new Refresher("VoipAntiSpamConfigurationPanelRefresher") {

      @Override
      protected void refreshInformation(final BeansPool beansPool) throws Exception {
        if (voipAntiSpamConfiguration == null || !transactionManager.get().isEditTransactionStarted()) {
          voipAntiSpamConfiguration = beansPool.getConfigBean().getVoipAntiSpamConfiguration(MainApp.clientUID);
          if (voipAntiSpamConfiguration == null) {
            voipAntiSpamConfiguration = new VoipAntiSpamConfiguration();
          }
          refreshEditors();
        }
      }

      @Override
      public void updateGuiElements() {
      }

      @Override
      public void handleEnabled(final BeansPool beansPool) {
      }

      @Override
      public void handleDisabled(final BeansPool beansPool) {
      }
    };
  }

  @Override
  public void keyTyped(final KeyEvent e) {
    char character = e.getKeyChar();
    if (((character < '0') || (character > '9'))
        && (character != '\b')) {
      e.consume();
    }
  }

  @Override
  public void keyPressed(final KeyEvent e) {
  }

  @Override
  public void keyReleased(final KeyEvent e) {
  }

  private void refreshEditors() {
    if (voipAntiSpamConfiguration != null) {
      enableCheckBox.setSelected(voipAntiSpamConfiguration.isEnable());

      grayListEditor.setGrayListConfigs(voipAntiSpamConfiguration.getGrayListConfigs());
      blackListEditor.setBlackListConfigs(voipAntiSpamConfiguration.getBlackListConfigs());

      gsmAlertingEditor.setGsmAlertingEnabled(voipAntiSpamConfiguration.isGsmAlertingAnalyze());
      gsmAlertingEditor.setGsmAlertingConfigs(voipAntiSpamConfiguration.getGsmAlertingConfigs());
      voipAlertingEditor.setVoipAlertingEnabled(voipAntiSpamConfiguration.isVoipAlertingAnalyze());
      voipAlertingEditor.setVoipAlertingConfigs(voipAntiSpamConfiguration.getVoipAlertingConfigs());
      fasEditor.setFasEnabled(voipAntiSpamConfiguration.isFasAnalyze());
      fasEditor.setFasConfigs(voipAntiSpamConfiguration.getFasConfigs());
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.forms;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;

import java.awt.*;

public class IntegerFormField extends FormField<Integer> {

  private JEditIntField component;
  private final int minValue;
  private final int maxValue;

  public IntegerFormField(final int id, final String name) {
    this(id, name, Integer.MIN_VALUE, Integer.MAX_VALUE);
  }

  public IntegerFormField(final int id, final String name, final int minValue, final int maxValue) {
    super(id, name);
    this.minValue = minValue;
    this.maxValue = maxValue;
  }

  @Override
  public Component getComponent() {
    if (component == null) {
      component = new JEditIntField(minValue, maxValue);
    }
    return component;
  }

  protected JEditIntField getFieldComponent() {
    return (JEditIntField) getComponent();
  }

  @Override
  public Integer getValue() {
    return getFieldComponent().getIntValue();
  }

  @Override
  public void setValue(final Integer value) {
    getFieldComponent().setValue(value);
  }

  @Override
  public void clear() {
    getFieldComponent().setText("");
  }

}

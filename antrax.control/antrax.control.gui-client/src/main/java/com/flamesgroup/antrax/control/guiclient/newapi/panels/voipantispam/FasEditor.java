/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.FasAnalyzeConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.FasConfig;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class FasEditor extends JPanel {

  private static final long serialVersionUID = -5714193512328327620L;

  private final JCheckBox enable = new JCheckBox("Enable");
  private final FasTable table = new FasTable();
  private final JScrollPane tablePane = new JScrollPane(table);

  private Component addButton;
  private Component removeButton;
  private Component editButton;

  private final String[] columnToolTips = {
      FasAnalyzeConfigTableTooltips.getPeriodTooltip(),
      FasAnalyzeConfigTableTooltips.getMaxRoutingRequestPerPeriodTooltip()
  };

  public FasEditor() {
    super(new BorderLayout());

    JReflectiveBar bar = createToolbar();
    add(bar, BorderLayout.NORTH);

    setEditorVisible(false);
    enable.addItemListener(e -> setEditorVisible(e.getStateChange() == ItemEvent.SELECTED));
    add(tablePane, BorderLayout.CENTER);
  }

  private void setEditorVisible(final boolean visible) {
    addButton.setVisible(visible);
    removeButton.setVisible(visible);
    editButton.setVisible(visible);
    tablePane.setVisible(visible);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());

    retval.addToRight(enable);
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      FasConfig fasConfig = FasDialog.createNewFasConfig(retval);
      if (fasConfig != null) {
        table.insertElem(fasConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      FasConfig fasConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (fasConfig == null) {
        return;
      }
      FasConfig editFasConfig = FasDialog.createEditFasConfig(retval, fasConfig);
      table.updateElemAt(editFasConfig, selectedRow);
    });
    return retval;
  }

  public boolean isFasEnabled() {
    return enable.isSelected();
  }

  public void setFasEnabled(final boolean enabled) {
    setEditorVisible(enabled);
    enable.setSelected(enabled);
  }

  public java.util.List<FasConfig> getFasConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setFasConfigs(final java.util.List<FasConfig> fasConfigs) {
    table.setData(fasConfigs.toArray(new FasConfig[fasConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    enable.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class FasTable extends JUpdatableTable<FasConfig, Integer> {

    private static final long serialVersionUID = -4155647707894299462L;

    public FasTable() {
      super(new TableBuilder<FasConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final FasConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final FasConfig src, final ColumnWriter<FasConfig> dest) {
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxRoutingRequestPerPeriod());
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max routing request per period", Integer.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = 5347856001345543454L;

        public String getToolTipText(final MouseEvent e) {
          java.awt.Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.auth;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class ConnectionLostDialog extends JDialog {
  private static final long serialVersionUID = 5460036241421555958L;
  private JProgressBar progressBar;

  private boolean checkNow = false;

  private final Action checkNowAction = new AbstractAction("Check Now") {

    private static final long serialVersionUID = -8822797320386389147L;

    @Override
    public void actionPerformed(final ActionEvent e) {
      checkNow = true;
      setVisible(false);
    }

  };

  private final Action quitAction = new AbstractAction("Quit") {

    private static final long serialVersionUID = -8296129923860508280L;

    @Override
    public void actionPerformed(final ActionEvent e) {
      checkNow = false;
      setVisible(false);
    }
  };

  public ConnectionLostDialog(final int iterationsCount) {
    setTitle("Antrax client: network problems");
    getContentPane().add(createCenterPanel(), BorderLayout.CENTER);
    getContentPane().add(createBottomPanel(iterationsCount), BorderLayout.SOUTH);
    setResizable(false);
    pack();
    setLocationRelativeTo(null);

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(quitAction, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

  private Component createCenterPanel() {
    JPanel retval = new JPanel();
    retval.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    JLabel label = new JLabel();
    label.setText("<html><body>Could not connect to the server.<br>Check your internet connection and ensure that server is started.");
    retval.add(label);
    return retval;
  }

  private Component createBottomPanel(final int iterationsCount) {
    JPanel retval = new JPanel();
    retval.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    retval.setLayout(new BoxLayout(retval, BoxLayout.X_AXIS));
    retval.add(Box.createHorizontalStrut(5));
    retval.add(new JLabel("Next Attempt:"));
    retval.add(Box.createHorizontalStrut(5));
    retval.add(progressBar = new JProgressBar(0, iterationsCount));
    retval.add(Box.createHorizontalStrut(5));
    retval.add(new JButton(checkNowAction));
    retval.add(Box.createHorizontalStrut(5));
    retval.add(new JButton(quitAction));
    return retval;
  }

  public boolean isCheckNow() {
    return checkNow;
  }

  public void setProgress(final int progress) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        progressBar.setValue(progress);
      }
    });
  }

  public static boolean showDialogFor(final int timeMs) throws InterruptedException, InvocationTargetException {
    int sleep = 50;
    final ConnectionLostDialog dialog = new ConnectionLostDialog(timeMs / sleep);

    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        dialog.setVisible(true);
      }
    });

    for (int i = 0; i < timeMs / sleep; ++i) {
      Thread.sleep(sleep);
      if (!dialog.isVisible()) {
        dialog.dispose();
        if (!dialog.isCheckNow()) {
          return false;
        }
        break;
      }
      dialog.setProgress(i);
    }
    dialog.setVisible(false);
    dialog.dispose();
    return true;
  }
}

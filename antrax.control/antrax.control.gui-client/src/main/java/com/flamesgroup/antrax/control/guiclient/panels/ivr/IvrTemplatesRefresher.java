/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flamesgroup.antrax.control.guiclient.panels.ivr;

import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.IvrTemplateWrapper;

import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class IvrTemplatesRefresher extends ExecutiveRefresher {

  private final IvrTemplatesServerTable ivrTemplatesServerTable;
  private final IvrTemplatesSimGroupTable ivrTemplatesSimGroupTable;
  private final IvrTemplatesTable ivrTemplatesTable;

  private final Set<IServerData> servers = new HashSet<>();
  private List<String> simGroups = new ArrayList<>();
  private String[] allSimGroups = new String[0];
  private List<IvrTemplateWrapper> ivrTemplates = new ArrayList<>();
  private Map<String, Integer> callDropReasons = new HashMap<>();
  private IServerData selectedServer;
  private String selectedSimGroup;
  private volatile boolean updating;

  public IvrTemplatesRefresher(final IvrTemplatesServerTable ivrTemplatesServerTable, final IvrTemplatesSimGroupTable ivrTemplatesSimGroupTable,
      final IvrTemplatesTable ivrTemplatesTable, final RefresherThread refresherThread,
      final TransactionManager transactionManager,
      final Component invoker) {
    super("IvrTemplatesRefresher", refresherThread, transactionManager, invoker);
    this.ivrTemplatesServerTable = ivrTemplatesServerTable;
    this.ivrTemplatesSimGroupTable = ivrTemplatesSimGroupTable;
    this.ivrTemplatesTable = ivrTemplatesTable;
  }

  public void init() {
    addPermanentExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Refresh IVR templates";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        List<String> startedServers = beansPool.getControlBean().listVoiceServerStatus().entrySet().stream()
            .filter(s -> s.getValue().getShortInfo().getStatus() == ServerStatus.STARTED)
            .map(s -> s.getKey().getName())
            .collect(Collectors.toList());
        servers.clear();
        simGroups.clear();
        servers.addAll(beansPool.getConfigBean().listServers(MainApp.clientUID).stream()
            .filter(s -> s.isVoiceServerEnabled() && startedServers.contains(s.getName()))
            .collect(Collectors.toSet()));
        if (!ivrTemplatesServerTable.getSelectedElems().isEmpty()) {
          selectedServer = ivrTemplatesServerTable.getSelectedElem();
          simGroups = beansPool.getControlBean().getIvrTemplatesSimGroups(selectedServer);
          allSimGroups = beansPool.getConfigBean().listSimGroupNames(MainApp.clientUID);
          callDropReasons = beansPool.getControlBean().getCallDropReasons(selectedServer);
        }
        if (!ivrTemplatesSimGroupTable.getSelectedElems().isEmpty()) {
          selectedSimGroup = ivrTemplatesSimGroupTable.getSelectedElem();
          ivrTemplates = beansPool.getControlBean().getIvrTemplates(selectedServer, selectedSimGroup);
        }
      }

      @Override
      public void updateGUIComponent() {
        updating = true;
        ivrTemplatesServerTable.setData(servers.toArray(new IServerData[servers.size()]));
        ivrTemplatesSimGroupTable.setData(simGroups.toArray(new String[simGroups.size()]));
        ivrTemplatesTable.setData(ivrTemplates.toArray(new IvrTemplateWrapper[ivrTemplates.size()]));

        ivrTemplatesSimGroupTable.setEnabled(true);
        ivrTemplatesTable.setEnabled(true);

        updating = false;
      }
    });
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }

  public boolean isUpdating() {
    return updating;
  }

  public void createSimGroup(final String simGroup) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Create new SIM group";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        beansPool.getControlBean().createIvrTemplateSimGroup(selectedServer, simGroup);
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }

  public void removeSimGroup(final List<String> selectedSimGroups) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Remove selected SIM group(s)";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        beansPool.getControlBean().removeIvrTemplateSimGroups(selectedServer, selectedSimGroups);
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }

  public Map<String, Integer> getCallDropReasons() {
    return callDropReasons;
  }

  public String[] getAllSimGroups() {
    return allSimGroups;
  }

  public void uploadFileToVs(final File selectedFile, final String fileName) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Upload file";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        byte[] bytes = Files.readAllBytes(selectedFile.toPath());
        beansPool.getControlBean().uploadIvrTemplate(selectedServer, selectedSimGroup, bytes, fileName);
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }

  public void removeIvrTemplatesFromVs(final List<IvrTemplateWrapper> selectedIvrTemplates) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Remove selected IVR templates(s)";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        beansPool.getControlBean().removeIvrTemplate(selectedServer, selectedSimGroup, selectedIvrTemplates);
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }
}

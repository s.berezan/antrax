/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptCommonsSource;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ScriptCommonsCreateDialog extends JDialog {

  private static final long serialVersionUID = 8414983372447419493L;
  private int componentsIndex;
  private final JTextField name;
  private final JComboBox type;
  private final ScriptParamEditor value;
  private boolean approved;
  private final ScriptCommonsSource commonsSource;

  public ScriptCommonsCreateDialog(final Window windowAncestor, final ScriptCommonsSource commonsSource, final PropertyEditor<?>... editors) {
    super(windowAncestor);
    this.commonsSource = commonsSource;
    GridBagLayout layout = new GridBagLayout();
    getContentPane().setLayout(layout);
    layout.rowWeights = new double[] {0, 0, 0, 1};

    add(new JLabel("Name:"), name = new JTextField());
    add(new JLabel("Type:"), type = new JComboBox(createComboBoxModel(editors)));
    add(new JLabel("Value:"), value = new ScriptParamEditor());
    add(createButtonsPanel(), new GridBagConstraints(0, componentsIndex, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

    type.setRenderer(createTypeRenderer());
    type.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        initializeEditor(value, type);
        pack();
        repaint();
      }
    });
    initializeEditor(value, type);
    setMinimumSize(new Dimension(300, 150));
    pack();
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    JButton okBtn = createOkBtn();
    JButton cancelBtn = createCancelBtn();
    retval.add(okBtn);
    retval.add(cancelBtn);
    layout.putConstraint(SpringLayout.EAST, okBtn, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, cancelBtn, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, okBtn, 0, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, cancelBtn, 0, SpringLayout.SOUTH, retval);

    retval.setPreferredSize(okBtn.getPreferredSize());
    return retval;
  }

  private JButton createCancelBtn() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        setVisible(false);
      }
    });
    return retval;
  }

  private JButton createOkBtn() {
    final JButton retval = new JButton("OK");
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        try {
          if (!isInputValid()) {
            MessageUtils.showError(retval, "Input is not valid", "Input is not full, please fill all fields");
            return;
          }
          if (commonsSource.getCommonsByName(name.getText()) != null) {
            MessageUtils.showError(retval, "Duplicate name", "This name is already used");
            return;
          }
          approved = true;
          setVisible(false);
        } catch (Exception exc) {
          MessageUtils.showError(retval, "Failed to apply value", exc.getMessage(), exc);
        }
      }
    });
    return retval;
  }

  private boolean isInputValid() {
    return !name.getText().isEmpty() && type.getSelectedItem() != null && value.getValue() != null;
  }

  public ScriptCommons getScriptCommon() {
    if (type.getSelectedItem() == null) {
      return null;
    }
    if (!approved) {
      return null;
    }
    ScriptCommons retval = new ScriptCommons();
    retval.setName(name.getText());
    retval.setValue(value.getValue());
    return retval;
  }

  @SuppressWarnings("unchecked")
  private void initializeEditor(final ScriptParamEditor value, final JComboBox type) {
    try {
      value.setScriptEditor((PropertyEditor<Object>) type.getSelectedItem());
    } catch (Exception e) {
      MessageUtils.showError(type, "Failed to select type " + type.getSelectedItem(), e.getMessage(), e);
    }
  }

  private ListCellRenderer createTypeRenderer() {
    return new DefaultListCellRenderer() {
      private static final long serialVersionUID = 6030931447226231668L;

      @Override
      public Component getListCellRendererComponent(final JList list, Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value instanceof PropertyEditor<?>) {
          value = ((PropertyEditor<?>) value).getType().getSimpleName();
        }
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      }
    };
  }

  private ComboBoxModel createComboBoxModel(final PropertyEditor<?>... propertyEditors) {
    DefaultComboBoxModel retval = new DefaultComboBoxModel();
    for (PropertyEditor<?> pe : propertyEditors) {
      retval.addElement(pe);
    }
    return retval;
  }

  private void add(final JLabel lbl, final Component editor) {
    getContentPane().add(lbl, new GridBagConstraints(0, componentsIndex, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    getContentPane().add(editor, new GridBagConstraints(1, componentsIndex, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    componentsIndex++;
  }

  public static ScriptCommons askForCommon(final Component parent, final ScriptCommonsSource commonsSource, final PropertyEditor<?>... propertyEditors) {
    ScriptCommonsCreateDialog dialog = new ScriptCommonsCreateDialog(SwingUtilities.getWindowAncestor(parent), commonsSource, propertyEditors);
    dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setLocationRelativeTo(parent);
    dialog.setVisible(true);
    dialog.dispose();

    return dialog.getScriptCommon();
  }

}

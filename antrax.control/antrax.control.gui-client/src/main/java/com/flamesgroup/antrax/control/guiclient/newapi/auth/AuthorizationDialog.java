/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.auth;

import com.flamesgroup.antrax.control.authorization.AuthorizationFailedException;
import com.flamesgroup.antrax.control.authorization.AuthorizationHelper;
import com.flamesgroup.antrax.control.communication.AuthorizationBean;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.layouts.FormLayout;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import javax.swing.*;

public class AuthorizationDialog extends JFrame {
  private static final long serialVersionUID = -2200324808208352228L;

  private final JTextField txtUser = new JTextField(25);
  private final JPasswordField txtPass = new JPasswordField(25);
  private volatile boolean authorized;

  private final AuthorizationBean authorizationBean;

  private final Action loginAction = new AbstractAction("Login") {

    private static final long serialVersionUID = -3250404655198271390L;

    @Override
    public void actionPerformed(final ActionEvent e) {
      WaitingDialog dialog = new WaitingDialog(null, "Authorizing", "Please wait until being authorized");
      dialog.show((Component) e.getSource(), createAuthorizationTask((Component) e.getSource()));
    }
  };

  private final Action cancelAction = new AbstractAction("Cancel") {

    private static final long serialVersionUID = -4853584829358456591L;

    @Override
    public void actionPerformed(final ActionEvent e) {
      setVisible(false);
      dispose();
    }
  };

  private AuthorizationDialog(final AuthorizationBean authorizationBean) {
    this.authorizationBean = authorizationBean;
    setTitle("Antrax Login");
    getContentPane().add(createEditPanel(), BorderLayout.CENTER);
    getContentPane().add(createOkCancelPanel(), BorderLayout.SOUTH);
    setResizable(false);
    pack();

    txtPass.setAction(loginAction);
    txtUser.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        txtPass.requestFocusInWindow();
      }
    });
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelAction, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    addWindowListener(new WindowAdapter() {
      @Override
      public void windowOpened(final WindowEvent e) {
        if (System.getProperty("debugging-user") != null && System.getProperty("debugging-pass") != null) {
          txtUser.setText(System.getProperty("debugging-user"));
          txtPass.setText(System.getProperty("debugging-pass"));
        }
      }
    });
  }

  protected Runnable createAuthorizationTask(final Component source) {
    return new Runnable() {

      @Override
      public void run() {
        try {
          authorize(txtUser.getText(), txtPass.getPassword(), authorizationBean);
          setVisible(false);
          dispose();
        } catch (AuthorizationFailedException ignored) {
          JOptionPane.showMessageDialog(null, "Check user and password", "Failed to authorize", JOptionPane.ERROR_MESSAGE);
          txtPass.setSelectionStart(0);
          txtPass.setSelectionEnd(txtPass.getPassword().length);
          txtPass.requestFocus();
        } catch (Exception e) {
          JOptionPane.showMessageDialog(null, e.getMessage(), "Failed to authorize", JOptionPane.ERROR_MESSAGE);
          txtPass.setSelectionStart(0);
          txtPass.setSelectionEnd(txtPass.getPassword().length);
          txtPass.requestFocus();
        }
      }
    };
  }

  private JPanel createOkCancelPanel() {
    JPanel retval = new JPanel(new GridLayout());
    retval.add(Box.createGlue());
    retval.add(new JButton(loginAction));
    retval.add(new JButton(cancelAction));
    retval.add(Box.createGlue());
    return retval;
  }

  private JPanel createEditPanel() {
    JPanel panel = new JPanel();
    FormLayout formLayout = new FormLayout(panel);
    formLayout.addField("User:", txtUser);
    formLayout.addField("Password:", txtPass);
    return panel;
  }

  private void authorize(final String name, final char[] password, final AuthorizationBean authorizationBean) throws Exception {
    String passwd = AuthorizationHelper.digest(password);
    Arrays.fill(password, (char) 0); // For security reason password should be cleaned up as quick as possible
    AuthorizationHelper.authorize(MainApp.clientUID, name, passwd, authorizationBean);
    authorized = true;
    MainApp.userName = name;
  }

  private boolean isAuthorized() {
    return authorized;
  }

  public static boolean authorize(final AuthorizationBean authorizationBean, final String serverURL) throws InterruptedException, InvocationTargetException {
    final AuthorizationDialog dialog = new AuthorizationDialog(authorizationBean);
    dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    dialog.setTitle(dialog.getTitle() + " at " + serverURL);
    dialog.setLocationRelativeTo(null);

    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {

        dialog.setVisible(true);
      }
    });
    while (dialog.isVisible()) {
      Thread.sleep(150);
    }
    return dialog.isAuthorized();
  }

  public static void main(final String[] args) {
    final AuthorizationDialog dialog = new AuthorizationDialog(null);
    dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        dialog.setVisible(true);

      }
    });
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

public final class SystemHelper {

  // Suppress default constructor, ensuring non-instantiability
  private SystemHelper() {
    throw new AssertionError();
  }

  public static final String OS_NAME = System.getProperty("os.name");
  public static final String OS_ARCH = System.getProperty("os.arch");
  public static final String OS_VERSION = System.getProperty("os.version");

  private static Boolean isWindows;

  public static boolean isWindows() {
    if (isWindows == null) {
      isWindows = OS_NAME.toLowerCase().startsWith("win");
    }
    return isWindows;
  }

  public static final boolean DEBUG_MODE = false;

}

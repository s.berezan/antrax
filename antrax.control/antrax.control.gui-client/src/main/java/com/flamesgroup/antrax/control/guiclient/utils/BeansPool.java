/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import static com.flamesgroup.rmi.RemoteProxyFactory.create;

import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.commons.CommonConstants;
import com.flamesgroup.antrax.control.communication.ActivationBean;
import com.flamesgroup.antrax.control.communication.ConfigurationBean;
import com.flamesgroup.antrax.control.communication.ControlBean;
import com.flamesgroup.antrax.control.communication.IGsmViewBean;
import com.flamesgroup.antrax.control.communication.IPrefixListBean;
import com.flamesgroup.antrax.control.communication.IVoipAntiSpamStatisticBean;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.voiceserv.utils.delivery.DeliveryConfig;
import com.flamesgroup.antrax.voiceserv.utils.delivery.GuaranteedDeliveryProxyServer;
import com.flamesgroup.rmi.RemoteAccessor;

import java.lang.reflect.Method;

public class BeansPool {

  private String serverURL;
  private final ControlBeanAccess guiBeanAccess;
  private final ControlBean controlBean;
  private final ConfigurationBean cfgBean;
  private final ActivationBean activationBean;
  private final RegistryAccessBean registryAccessBean;
  private final RegistryAccess registryAccess;
  private final IVoipAntiSpamStatisticBean voipAntiSpamStatisticBean;
  private final IGsmViewBean gsmViewBean;
  private final IPrefixListBean prefixListBean;
  private final GuaranteedDeliveryProxyServer registryServer = new GuaranteedDeliveryProxyServer("RegistryDelivery", NoSuchFieldError.class);

  public BeansPool(final String serverURL) {
    assert serverURL != null : "serverURL can't be null for beans pool";
    this.serverURL = serverURL;

    cfgBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_CONFIGURATION_BEAN), ConfigurationBean.class).build());
    controlBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_CONTROL_BEAN), ControlBean.class).build());
    guiBeanAccess = new ControlBeanAccess(controlBean);
    activationBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_ACTIVATION_BEAN), ActivationBean.class).build());
    registryAccessBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_REGISTRY_ACCESS_BEAN), RegistryAccessBean.class).build());
    voipAntiSpamStatisticBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_VOIP_ANTI_SPAM_STATISTIC_BEAN), IVoipAntiSpamStatisticBean.class).build());
    gsmViewBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_GSM_VIEW_BEAN), IGsmViewBean.class).build());
    prefixListBean = create(new RemoteAccessor.Builder<>(buildURL(CommonConstants.RMI_PREFIX_LIST_BEAN), IPrefixListBean.class).build());
    registryServer.start();

    registryAccess = registryServer.proxy(new RegistryAccessAdapter(registryAccessBean, MainApp.clientUID), RegistryAccess.class, new DeliveryConfig() {

      @Override
      public Object defaultValue(final Method method) {
        return null;
      }

      @Override
      public boolean isAsync(final Method method) {
        return false;
      }

      @Override
      public boolean isExceptionless(final Method method) {
        return false;
      }

      @Override
      public boolean isGuaranteed(final Method method) {
        return true;
      }

    });
  }

  public BeansPool(final ConfigurationBean cfgBean, final ControlBean antraxBean, final ActivationBean activationBean, final RegistryAccessBean registryAccess,
      final IVoipAntiSpamStatisticBean voipAntiSpamStatisticBean,
      final IGsmViewBean gsmViewBean, final IPrefixListBean prefixListBean) {
    this.cfgBean = cfgBean;
    this.controlBean = antraxBean;
    this.guiBeanAccess = new ControlBeanAccess(antraxBean);
    this.activationBean = activationBean;
    this.registryAccessBean = registryAccess;
    this.voipAntiSpamStatisticBean = voipAntiSpamStatisticBean;
    this.gsmViewBean = gsmViewBean;
    this.prefixListBean = prefixListBean;
    this.registryAccess = new RegistryAccessAdapter(registryAccessBean, MainApp.clientUID);
  }

  private String buildURL(final String beanName) {
    return String.format("%s/%s", getServerURL(), beanName);
  }

  public String getServerURL() {
    return this.serverURL;
  }

  public ControlBeanAccess getControlBean() {
    return guiBeanAccess;
  }

  public ConfigurationBean getConfigBean() {
    return cfgBean;
  }

  public ActivationBean getActivationBean() {
    return activationBean;
  }

  public RegistryAccess getRegistryAccess() {
    return registryAccess;
  }

  public RegistryAccessBean getRegistryAccessBean() {
    return registryAccessBean;
  }

  public IVoipAntiSpamStatisticBean getVoipAntiSpamStatisticBean() {
    return voipAntiSpamStatisticBean;
  }

  public IGsmViewBean getGsmViewBean() {
    return gsmViewBean;
  }

  public IPrefixListBean getPrefixListBean() {
    return prefixListBean;
  }

  public void release() {
    registryServer.terminate();
  }

}

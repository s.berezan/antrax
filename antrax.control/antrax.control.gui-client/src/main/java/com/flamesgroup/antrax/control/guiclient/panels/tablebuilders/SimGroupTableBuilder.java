/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.tablebuilders;

import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.control.guiclient.panels.SimGroupWrapper;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumn;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import javax.swing.*;

public class SimGroupTableBuilder implements TableBuilder<SimGroupWrapper, Long> {

  @Override
  public Long getUniqueKey(final SimGroupWrapper group) {
    return group.getSimGroup().getID();
  }

  @Override
  public void buildRow(final SimGroupWrapper wrapper, final ColumnWriter<SimGroupWrapper> dest) {
    SIMGroup group = wrapper.getSimGroup();
    dest.writeColumn(group.getName(), group, group.getName());
    dest.writeColumn(group.getIdleAfterRegistration());
    dest.writeColumn(group.getIdleAfterSuccessfullCall());
    dest.writeColumn(group.getIdleAfterZeroCall());
    dest.writeColumn(group.getIdleBeforeUnregistration());
    dest.writeColumn(group.getIdleBeforeSelfCall());
    dest.writeColumn(group.getSelfDialFactor());
    dest.writeColumn(group.getSelfDialChance());
    dest.writeColumn(group.getIdleAfterSelfCallTimeout());
    dest.writeColumn(group.getDescription());
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    UpdateTableColumn columnName = columns.addColumn("Name", String.class)
    /*  */.setPreferredWidth(150);
    columns.addColumn("IdleAfterRegistration", Timeout.class);
    columns.addColumn("IdleAfterSuccessfulCall", Timeout.class);
    columns.addColumn("IdleAfterZeroCall", Timeout.class);
    columns.addColumn("IdleBeforeUnregistration", Timeout.class);
    columns.addColumn("IdleBeforeSelfCall", Timeout.class);
    columns.addColumn("SelfDialFactor", Integer.class);
    columns.addColumn("SelfDialChance", Integer.class);
    columns.addColumn("IdleAfterSelfCallTimeout", Timeout.class);
    columns.addColumn("Description", String.class);

    columns.addColumnSortSequence(columnName, SortOrder.ASCENDING);
  }

}

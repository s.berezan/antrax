/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class GsmAlertingAnalyzeConfigTableTooltips {

  private GsmAlertingAnalyzeConfigTableTooltips() {
  }

  public static String getGsmAlertingTimeTooltip() {
    return new TooltipHeaderBuilder("Time of GSM alerting, in seconds. Time between dialing and answer").build();
  }

  public static String getPeriodTooltip() {
    return new TooltipHeaderBuilder("The period of time in which counting requests are going, in minutes").build();
  }

  public static String getMaxRoutingRequestPerPeriodTooltip() {
    return new TooltipHeaderBuilder("A maximum number of requests on routing during period").build();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class GrayListConfigTableTooltips {

  private GrayListConfigTableTooltips() {
  }

  public static String getPeriodTooltip() {
    return new TooltipHeaderBuilder("The period of time in which counting requests are going, in minutes").build();
  }

  public static String getMaxRoutingRequestPerPeriod() {
    return new TooltipHeaderBuilder("Ultimate (maximum) number of requests on routing for setup time period").build();
  }

  public static String getBlockPeriodTooltip() {
    return new TooltipHeaderBuilder("The period of time for number which is blocked in gray list, in minutes").build();
  }

  public static String getMaxBlockCountBeforeMoveToBlackListTooltip() {
    return new TooltipHeaderBuilder("Ultimate (maximum) number of requests on routing for setup time period").build();
  }

  public static String getAcdPeriodTooltip() {
    return new TooltipHeaderBuilder("The period of time in which counting calls with short acd - smaller than established, in minutes").build();
  }

  public static String getMaxMinAcdCallPerPeriodTooltip() {
    return new TooltipHeaderBuilder("The number of calls with short acd which lead to the moving in black list after it").build();
  }

  public static String getMinAcdTooltip() {
    return new TooltipHeaderBuilder("Minimum average call duration for normal call, in seconds").build();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

public class WhiteListStatisticTableTooltips {

  private WhiteListStatisticTableTooltips() {
  }

  public static String getNumberTooltip() {
    return new TooltipHeaderBuilder("White number").build();
  }

  public static String getStatusTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("The reason for which the number is in the table");
    TooltipHeaderBuilder.TextTooltipHeader textTable = builder.createTextTable();
    textTable.addValue(VoipAntiSpamListNumbersStatus.AUTO.name(), "Added by analyzing cdr")
        .addValue(VoipAntiSpamListNumbersStatus.MANUAL.name(), "Added manually by user");
    return builder.build();
  }

  public static String getAddTimeTooltip() {
    return new TooltipHeaderBuilder("Time of adding number to table").build();
  }

  public static String getRoutingRequestCountTooltip() {
    return new TooltipHeaderBuilder("Count of requests on routing for number").build();
  }

}

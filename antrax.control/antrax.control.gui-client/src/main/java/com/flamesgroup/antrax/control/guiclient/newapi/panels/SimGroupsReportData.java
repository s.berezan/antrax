/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// FIXME: replace public attributes to getters
public class SimGroupsReportData {
  public String groupName;
  public int locked;
  public int unlocked;
  public int total;
  public int totalCallsCount;
  public int successfulCallsCount;
  public long callsDuration;
  public int totalIncomingCallsCount;
  public int successfulIncomingCallsCount;
  public int successOutgoingSmsCount;
  public int totalOutgoingSmsCount;
  public int totalIncomingSmsCount;
  public int totalSmsStatuses;

  public long getAverageCallDuration() {
    if (successfulCallsCount == 0) {
      return 0;
    }
    return callsDuration / successfulCallsCount;
  }

  private void visitRuntime(final SimViewData simViewData) {
    if (simViewData.isLocked()) {
      locked++;
    } else {
      unlocked++;
    }
    total++;
    totalCallsCount += simViewData.getTotalCallsCount();
    successfulCallsCount += simViewData.getSuccessfulCallsCount();
    callsDuration += simViewData.getCallDuration();
    totalIncomingCallsCount += simViewData.getIncomingTotalCallsCount();
    successfulIncomingCallsCount = simViewData.getIncomingSuccessfulCallsCount();
    successOutgoingSmsCount += simViewData.getSuccessOutgoingSmsCount();
    totalOutgoingSmsCount += simViewData.getTotalOutgoingSmsCount();
    totalIncomingSmsCount += simViewData.getIncomingSMSCount();
    totalSmsStatuses += simViewData.getCountSmsStatuses();
  }

  public static SimGroupsReportData[] collect(final List<SimViewData> simViewData, final SimpleSimGroup[] simGroups) {
    Map<String, SimGroupsReportData> groupsMap = new HashMap<>();
    for (SimpleSimGroup group : simGroups) {
      ensureReportData(group.getName(), groupsMap);
    }

    simViewData.stream().filter(data -> data.getSimGroupName() != null).forEach(data -> visitRuntime(data, ensureReportData(data.getSimGroupName(), groupsMap)));
    return groupsMap.values().toArray(new SimGroupsReportData[groupsMap.size()]);
  }

  private static void visitRuntime(final SimViewData data, final SimGroupsReportData report) {
    report.visitRuntime(data);
  }

  private static SimGroupsReportData ensureReportData(final String name, final Map<String, SimGroupsReportData> groupsMap) {
    SimGroupsReportData data = groupsMap.get(name);
    if (data == null) {
      groupsMap.put(name, data = new SimGroupsReportData());
      data.groupName = name;
    }
    return data;
  }
}

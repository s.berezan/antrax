/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.control.communication.GsmGroupEditInfo;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.Arrays;

public class GsmGroupEditAdapter {

  private GsmGroupEditInfo gsmGroupInfo;
  private final SIMGroup[] simGroups;

  public GsmGroupEditAdapter(final GsmGroupEditInfo gsmGroupInfo, final SIMGroup[] simGroups) {
    this.gsmGroupInfo = gsmGroupInfo;
    this.simGroups = simGroups;
  }

  public GsmGroupEditInfo getGsmGroupInfo() {
    return gsmGroupInfo;
  }

  public GSMGroup getGsmGroup() {
    return gsmGroupInfo.getGsmGroup();
  }

  public void setGsmGroupInfo(final GsmGroupEditInfo gsmGroupInfo) {
    this.gsmGroupInfo = gsmGroupInfo;
  }

  public SIMGroup[] getSimGroups() {
    return simGroups;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[gsmGroupInfo:" + gsmGroupInfo +
        " simGroups:" + Arrays.toString(simGroups) +
        ']';
  }
}

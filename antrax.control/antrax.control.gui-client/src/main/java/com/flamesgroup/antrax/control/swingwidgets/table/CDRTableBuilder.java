/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CDRTableBuilder implements TableBuilder<CDR, Long> {

  final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
  final Calendar cal = Calendar.getInstance();

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    columns.addColumn("server", String.class);
    columns.addColumn("caller number", String.class);
    columns.addColumn("called number", String.class);
    columns.addColumn("setup", String.class);
    columns.addColumn("start", String.class);
    columns.addColumn("stop", String.class);
    columns.addColumn("duration", String.class);
    columns.addColumn("pdd", String.class);
    columns.addColumn("ring time", String.class);
    columns.addColumn("call type", CallType.class);
    columns.addColumn("drop reason", CdrDropReason.class);
    columns.addColumn("drop code", Short.class);
  }

  @Override
  public void buildRow(final CDR src, final ColumnWriter<CDR> dest) {
    dest.writeColumn(src.getVoiceServerName());
    dest.writeColumn(src.getCallerPhoneNumber() == null ? "" : src.getCallerPhoneNumber().getValue());
    dest.writeColumn(src.getCalledPhoneNumber() == null ? "" : src.getCalledPhoneNumber().getValue());
    dest.writeColumn(calculateTime(src.getSetupTime()));
    dest.writeColumn(calculateTime(src.getStartTime()));
    dest.writeColumn(calculateTime(src.getStopTime()));
    dest.writeColumn(TimeUtils.writeTime(src.getDuration()));
    dest.writeColumn(TimeUtils.writeTime(src.getPDD()));
    dest.writeColumn(TimeUtils.writeTime(src.getRingTime()));
    dest.writeColumn(src.getCallType());
    dest.writeColumn(src.getDropReason());
    dest.writeColumn(src.getDropCode());
  }

  private String calculateTime(final long time) {
    if (time == 0) {
      return " - no - ";
    } else {
      cal.setTimeInMillis(time);
      return formatter.format(cal.getTime());
    }
  }

  @Override
  public Long getUniqueKey(final CDR src) {
    return src.getID();
  }

}

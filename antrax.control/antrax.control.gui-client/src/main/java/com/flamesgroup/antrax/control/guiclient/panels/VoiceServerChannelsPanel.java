/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.guiclient.commadapter.VoiceServerChannelAdapter;
import com.flamesgroup.antrax.control.guiclient.indicators.VoiceServerChannelIndicators;
import com.flamesgroup.antrax.control.guiclient.refreshers.VoiceServerChannelsRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.ExtendedComboBox;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.device.DeviceType;

import java.awt.*;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;

public class VoiceServerChannelsPanel extends JPanel {

  private static final long serialVersionUID = -550321748191186931L;

  private VoiceServerChannelsRefresher refresher;
  private static final VoiceServerChannelsTable channelsTable = new VoiceServerChannelsTable();
  private static final JScrollPane tableScrollPane = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(channelsTable);
  private static final AtomicBoolean loadProperties = new AtomicBoolean();
  private static final AtomicBoolean saveProperties = new AtomicBoolean();
  private static final JCheckBox showUnavailableChannels = new JCheckBox("Show unavailable channels");

  private final GuiProperties guiProperties;
  private final JReflectiveBar bottomToolbar = new JReflectiveBar();
  private JButton buttonRefresh;
  private JButton buttonExportToCsv;

  private JButton buttonFireEvent;
  private JButton enableButton;
  private JButton disableButton;
  private JButton buttonLock;
  private JButton buttonUnlock;
  private JButton buttonLockArfcn;
  private JButton buttonUnLockArfcn;
  private JButton buttonSendUSSD;
  private JButton buttonSendSMS;
  private JButton buttonSendDTMF;
  private JButton buttonNetworkSurvey;

  private FireEventDialog fireEventDialog;
  private SendUSSDDialog sendUSSDDialog;
  private SendSMSDialog sendSMSDialog;
  private SendDTMFDialog sendDTMFDialog;

  private final VoiceServerChannelIndicators indicators;

  private final String server;

  private final JContextSearch contextSearch = new JContextSearch();

  private RefresherThread refresherThread;

  private ExtendedComboBox gsmGroupComboBox;
  private ExtendedComboBox simGroupComboBox;

  public VoiceServerChannelsPanel(final String server) {
    super(new BorderLayout());
    this.server = server;

    this.indicators = new VoiceServerChannelIndicators();

    bottomToolbar.addToLeft(new JLabel("GSM group:"));
    bottomToolbar.addToLeft(getGSMGroupComboBox());
    bottomToolbar.addToLeft(new JLabel("SIM group:"));
    bottomToolbar.addToLeft(getSimGroupComboBox());

    Box box = new Box(BoxLayout.Y_AXIS);
    box.add(createTopToolbar());
    box.add(bottomToolbar);
    add(box, BorderLayout.NORTH);
    add(createCountersPanel(), BorderLayout.SOUTH);
    showUnavailableChannels.addActionListener(e -> {
      if (refresher == null) {
        return;
      }
      refresher.setShowUnliveChannels(showUnavailableChannels.isSelected());
      refresherThread.forceRefresh();
    });
    setTransferHandler(new VoiceServerChannelsTransferHandler(contextSearch));
    guiProperties = new GuiProperties(VoiceServerChannelsPanel.class.getSimpleName()) {
      private static final String SHOW_UNAVAILABLE_CHANNELS = "showUnavailableChannels";

      @Override
      protected void restoreOutProperties() {
        String showUnLiveHoldersString = properties.getProperty(SHOW_UNAVAILABLE_CHANNELS, "false");
        showUnavailableChannels.setSelected(Boolean.valueOf(showUnLiveHoldersString));
      }

      @Override
      protected void saveOutProperties() {
        properties.setProperty(SHOW_UNAVAILABLE_CHANNELS, String.valueOf(showUnavailableChannels.isSelected()));
      }
    };
    if (!loadProperties.getAndSet(true)) {
      channelsTable.getUpdatableTableProperties().restoreProperties();
      guiProperties.restoreProperties();
    }
  }

  public void setEditable(final boolean editable) {
  }

  public void postInitialize(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    this.refresherThread = refresherThread;
    this.refresher = new VoiceServerChannelsRefresher(refresherThread, transactionManager, this);
    this.refresher.initializePermanentRefresher(buttonRefresh, channelsTable, indicators, server);
    this.refresher.initializeGsmGroupsPermanentRefresher(gsmGroupComboBox, simGroupComboBox);
  }

  public void setActive(final boolean active) {
    if (active) {
      add(tableScrollPane, BorderLayout.CENTER);
      refresher.setShowUnliveChannels(showUnavailableChannels.isSelected());
      refresher.setVoiceServerChannelAdaptersData(buttonRefresh, channelsTable, indicators);
      refresher.setGSMGroupData(gsmGroupComboBox);
      refresher.setSimGroupData(simGroupComboBox);
      bottomToolbar.addToLeft(showUnavailableChannels);
      contextSearch.applyFilter();
      refresherThread.addRefresher(refresher);
    } else {
      remove(tableScrollPane);
      bottomToolbar.remove(showUnavailableChannels);
      refresherThread.removeRefresher(refresher);
    }
  }

  public void release() {
    if (!saveProperties.getAndSet(true)) {
      channelsTable.getUpdatableTableProperties().saveProperties();
      guiProperties.saveProperties();
    }
    if (refresher != null) {
      refresher.release();
    }
  }

  private JComponent createTopToolbar() {
    contextSearch.registerSearchItem(channelsTable);

    channelsTable.getRowSorter().addRowSorterListener(new RowSorterListener() {

      @Override
      public void sorterChanged(final RowSorterEvent e) {
        int filtered = (channelsTable.getRowSorter() != null) ? channelsTable.getRowSorter().getViewRowCount() : 0;
        indicators.setFiltered(filtered);
      }

    });

    channelsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        int selectedRowCount = channelsTable.getSelectedRowCount();
        indicators.setSelected(selectedRowCount);

        int selectedActiveRowCount = (int) channelsTable.getSelectedElems().stream().filter(a -> a.getSimUID() != null).count();
        boolean selectedActiveRows = selectedActiveRowCount > 0;
        enableButton.setEnabled(selectedActiveRows);
        disableButton.setEnabled(selectedActiveRows);
        buttonFireEvent.setEnabled(selectedActiveRows);
        buttonSendSMS.setEnabled(selectedActiveRows);
        buttonSendUSSD.setEnabled(selectedActiveRows);
        buttonSendDTMF.setEnabled(selectedActiveRows);
        simGroupComboBox.setEnabled(selectedActiveRows);

        boolean selectedRows = selectedRowCount > 0;
        buttonLock.setEnabled(selectedRows);
        buttonUnlock.setEnabled(selectedRows);
        buttonNetworkSurvey.setEnabled(selectedRows);
        buttonLockArfcn.setEnabled(selectedRows);
        buttonUnLockArfcn.setEnabled(selectedRows);
      }
    });

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(getRefreshButton());
    bar.addToLeft(getExportToCsvButton());
    enableButton = createEnableButton(true, "Enable selected sim cards", "/img/simstate/simstate_enable.png");
    bar.addToLeft(enableButton);
    disableButton = createEnableButton(false, "Disable selected sim cards", "/img/simstate/simstate_disable.png");
    bar.addToLeft(disableButton);
    bar.addToLeft(getFireEventButton());
    bar.addToLeft(getLockButton());
    bar.addToLeft(getUnlockButton());
    bar.addToLeft(getNetworkSurveyButton());
    bar.addToLeft(getLockArfcnButton());
    bar.addToLeft(getUnLockArfcnButton());
    bar.addToRight(getSendSMSButton());
    bar.addToRight(getSendUSSDButton());
    bar.addToRight(getSendDTMFButton());
    bar.addToRight(new JLabel("  "));
    bar.addToRight(contextSearch);
    return bar;
  }

  private JButton getExportToCsvButton() {
    if (buttonExportToCsv == null) {
      buttonExportToCsv = new JExportCsvButton(channelsTable, "voiceChannels");
    }
    return buttonExportToCsv;
  }

  private JButton getRefreshButton() {
    if (buttonRefresh == null) {
      buttonRefresh = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/reload-alt.png")).setToolTipText(
          "Refresh").addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          buttonRefresh.setEnabled(false);
          refresherThread.forceRefresh();
        }
      }).build();
    }
    return buttonRefresh;
  }

  private FireEventDialog getFireEventDialog() {
    if (null == fireEventDialog) {
      fireEventDialog = new FireEventDialog(SwingUtilities.getWindowAncestor(VoiceServerChannelsPanel.this));
      fireEventDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      fireEventDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      fireEventDialog.setLocationRelativeTo(VoiceServerChannelsPanel.this);
    }
    fireEventDialog.setTitle("Generate event to test SIM group scripts");
    //fireEventDialog.clear();

    return fireEventDialog;
  }

  private SendUSSDDialog getSendUSSDDialog() {
    if (null == sendUSSDDialog) {
      sendUSSDDialog = new SendUSSDDialog(SwingUtilities.getWindowAncestor(VoiceServerChannelsPanel.this));
      sendUSSDDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      sendUSSDDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      sendUSSDDialog.setLocationRelativeTo(VoiceServerChannelsPanel.this);
      sendUSSDDialog.addCloseDialogListener(() -> {
        final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
        if (!selected.isEmpty()) {
          refresher.endUSSDDialog(selected);
        }
      });
    }
    int selectedRows = channelsTable.getSelectedRowCount();
    sendUSSDDialog.setTitle(String.format("Send USSD (for %s)", (selectedRows == channelsTable.getRowCount()) ? "all" : selectedRows));
    sendUSSDDialog.clear();

    return sendUSSDDialog;
  }

  private SendSMSDialog getSendSMSDialog() {
    if (null == sendSMSDialog) {
      sendSMSDialog = new SendSMSDialog(SwingUtilities.getWindowAncestor(VoiceServerChannelsPanel.this));
      sendSMSDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      sendSMSDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      sendSMSDialog.setLocationRelativeTo(VoiceServerChannelsPanel.this);
    }

    int selectedRows = channelsTable.getSelectedRowCount();
    sendSMSDialog.setTitle(String.format("Send SMS (from %s)", (selectedRows == channelsTable.getRowCount()) ? "all" : selectedRows));
    sendSMSDialog.clear();

    return sendSMSDialog;
  }

  private SendDTMFDialog getSendDTMFDialog() {
    if (null == sendDTMFDialog) {
      sendDTMFDialog = new SendDTMFDialog(SwingUtilities.getWindowAncestor(VoiceServerChannelsPanel.this), l -> {
        final String dtmfText = sendDTMFDialog.getDTMFText();
        if (dtmfText == null || dtmfText.trim().isEmpty()) {
          return;
        }

        final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
        if (!selected.isEmpty()) {
          refresher.sendDTMF(VoiceServerChannelsPanel.this, dtmfText, selected);
        }
      });
      sendDTMFDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      sendDTMFDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      sendDTMFDialog.setLocationRelativeTo(VoiceServerChannelsPanel.this);
    }
    int selectedRows = channelsTable.getSelectedRowCount();
    sendDTMFDialog.setTitle(String.format("Send DTMF (for %s)", (selectedRows == channelsTable.getRowCount()) ? "all" : selectedRows));
    sendDTMFDialog.clear();

    return sendDTMFDialog;
  }

  private JButton getFireEventButton() {
    if (null == buttonFireEvent) {
      buttonFireEvent = new JReflectiveButton.JReflectiveButtonBuilder()
              /* */.setIcon(IconPool.getShared("/img/test_script.png"))
              /* */.setToolTipText("Generate event to test SIM group scripts")
              /* */.build();

      buttonFireEvent.setEnabled(false);

      buttonFireEvent.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          FireEventDialog dialog = getFireEventDialog();
          if (dialog.showDialog()) {
            final String eventText = dialog.getEventText();
            if (eventText == null || eventText.trim().isEmpty()) {
              return;
            }

            final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
            if (!selected.isEmpty()) {
              refresher.fireEvent(VoiceServerChannelsPanel.this, eventText, selected);
            }
          }
        }
      });
    }

    return buttonFireEvent;
  }

  private JButton getLockButton() {
    if (null == buttonLock) {
      buttonLock = createLockButton(true, "Lock selected gsm channels", "Enter lock gsm channel reason", IconPool.getShared("/img/buttons/button_lock.png"));
    }
    return buttonLock;
  }

  private JButton getNetworkSurveyButton() {
    if (null == buttonNetworkSurvey) {
      buttonNetworkSurvey = createNetworkSurveyButton("Execute Network Survey gsm channels", IconPool.getShared("/img/transmit.png"));
    }
    return buttonNetworkSurvey;
  }

  private JButton getUnlockButton() {
    if (null == buttonUnlock) {
      buttonUnlock = createLockButton(false, "Unlock selected gsm channels", "Enter unlock gsm channel reason", IconPool.getShared("/img/buttons/button_unlock.png"));
    }
    return buttonUnlock;
  }

  private JButton getLockArfcnButton() {
    if (null == buttonLockArfcn) {
      buttonLockArfcn = createLockArfcnButton("Lock selected gsm channels to arfcn", "Enter lock arfcn to gsm channel", IconPool.getShared("/img/lock_arfcn.png"));
    }
    return buttonLockArfcn;
  }

  private JButton getUnLockArfcnButton() {
    if (null == buttonUnLockArfcn) {
      buttonUnLockArfcn = createUnLockArfcnButton("Set default value for arfcn", IconPool.getShared("/img/unlock_arfcn.png"));
    }
    return buttonUnLockArfcn;
  }

  private JButton createLockButton(final boolean lock, final String dialogText, final String tooltip, final Icon icon) {
    final JButton buttonLock = new JReflectiveButton.JReflectiveButtonBuilder().setToolTipText(tooltip).setIcon(icon).build();
    buttonLock.setEnabled(false);

    buttonLock.addActionListener(e -> {
      final String lockReason = JOptionPane.showInputDialog(buttonLock, dialogText, "Manually by user");
      if (lockReason == null) {
        return;
      }

      buttonLock.setEnabled(false);
      final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
      if (!selected.isEmpty()) {
        refresher.lock(VoiceServerChannelsPanel.this, lock, lockReason, selected);
      }
    });

    return buttonLock;
  }

  private JButton createLockArfcnButton(final String dialogText, final String tooltip, final Icon icon) {
    final JButton buttonLock = new JReflectiveButton.JReflectiveButtonBuilder().setToolTipText(tooltip).setIcon(icon).build();
    buttonLock.setEnabled(false);

    buttonLock.addActionListener(e -> {
      String res = JOptionPane.showInputDialog(buttonLock, dialogText, "0");
      if (res == null) {
        return;
      }
      final int lockArfcn = Integer.valueOf(res);

      buttonLock.setEnabled(false);
      final List<VoiceServerChannelAdapter> selected = getNewDevices(channelsTable.getSelectedElems());
      if (!selected.isEmpty()) {
        refresher.lockArfcn(VoiceServerChannelsPanel.this, lockArfcn, selected);
      }
    });

    return buttonLock;
  }

  private JButton createUnLockArfcnButton(final String tooltip, final Icon icon) {
    final JButton buttonLock = new JReflectiveButton.JReflectiveButtonBuilder().setToolTipText(tooltip).setIcon(icon).build();
    buttonLock.setEnabled(false);

    buttonLock.addActionListener(e -> {
      buttonLock.setEnabled(false);
      final List<VoiceServerChannelAdapter> selected = getNewDevices(channelsTable.getSelectedElems());
      if (!selected.isEmpty()) {
        refresher.unLockArfcn(VoiceServerChannelsPanel.this, selected);
      }
    });

    return buttonLock;
  }

  private List<VoiceServerChannelAdapter> getNewDevices(final List<VoiceServerChannelAdapter> selected) {
    return selected.stream()
        .filter(vsc -> vsc.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMB3) ||
            vsc.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMBOX4) ||
            vsc.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMBOX8))
        .collect(Collectors.toList());
  }

  private JButton createNetworkSurveyButton(final String tooltip, final Icon icon) {
    final JButton buttonNetworkSurvey = new JReflectiveButton.JReflectiveButtonBuilder().setToolTipText(tooltip).setIcon(icon).build();
    buttonNetworkSurvey.setEnabled(false);

    buttonNetworkSurvey.addActionListener(e -> {
      buttonNetworkSurvey.setEnabled(false);
      final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems().stream()
          .filter(vca -> vca.getCallChannelState() == null &&
              (vca.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMB3)
                  || vca.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMBOX4)
                  || vca.getMobileGatewayChannelUID().getDeviceUID().getDeviceType().equals(DeviceType.GSMBOX8))
          )
          .collect(Collectors.toList());
      if (!selected.isEmpty()) {
        refresher.executeNetworkSurvey(VoiceServerChannelsPanel.this, selected);
      }
    });

    return buttonNetworkSurvey;
  }

  private JButton getSendUSSDButton() {
    if (null == buttonSendUSSD) {
      buttonSendUSSD = new JReflectiveButton.JReflectiveButtonBuilder().setText("USSD").setToolTipText("Send USSD").build();
      buttonSendUSSD.setEnabled(false);

      buttonSendUSSD.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          executeUSSD(null);
        }
      });
    }

    return buttonSendUSSD;
  }

  public void executeUSSD(final String response) {
    SendUSSDDialog dialog = getSendUSSDDialog();
    if (dialog.showDialog(response)) {
      final String ussdText = dialog.getUSSDText();
      if (ussdText == null || ussdText.trim().isEmpty()) {
        return;
      }

      final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
      if (!selected.isEmpty()) {
        refresher.sendUSSD(VoiceServerChannelsPanel.this, ussdText, selected, dialog.isGetResponse());
      }
    }
  }

  private JButton getSendSMSButton() {
    if (null == buttonSendSMS) {
      buttonSendSMS = new JReflectiveButton.JReflectiveButtonBuilder().setText("SMS").setToolTipText("Send SMS").build();
      buttonSendSMS.setEnabled(false);

      buttonSendSMS.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          SendSMSDialog dialog = getSendSMSDialog();
          if (dialog.showDialog()) {
            final List<String> numbers = dialog.getNumbers();
            String smsText = dialog.getSMSText();

            if (null == smsText) {
              smsText = "";
            }

            final List<VoiceServerChannelAdapter> selected = channelsTable.getSelectedElems();
            if (!selected.isEmpty()) {
              refresher.sendSMS(VoiceServerChannelsPanel.this, numbers, smsText, selected);
            }
          }
        }
      });
    }

    return buttonSendSMS;
  }

  private JButton getSendDTMFButton() {
    if (null == buttonSendDTMF) {
      buttonSendDTMF = new JReflectiveButton.JReflectiveButtonBuilder().setText("DTMF").setToolTipText("Send DTMF").build();
      buttonSendDTMF.setEnabled(false);

      buttonSendDTMF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          SendDTMFDialog dialog = getSendDTMFDialog();
          dialog.setVisible(true);
        }
      });
    }

    return buttonSendDTMF;
  }

  private JButton createEnableButton(final boolean enable, final String tooltip, final String iconPath) {
    final JButton retval = new JButton();
    retval.setToolTipText(tooltip);
    retval.setIcon(IconPool.getShared(iconPath));
    retval.addActionListener(e -> {
      retval.setEnabled(false);
      refresher.enableSimUnit(channelsTable.getSelectedElems(), enable);
    });
    return retval;
  }

  private ExtendedComboBox getGSMGroupComboBox() {
    if (gsmGroupComboBox == null) {
      gsmGroupComboBox = new ExtendedComboBox();

      gsmGroupComboBox.setPreferredSize(new Dimension(200, gsmGroupComboBox.getPreferredSize().height));
      gsmGroupComboBox.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          final List<VoiceServerChannelAdapter> selectedElems = channelsTable.getSelectedElems();
          if (selectedElems.isEmpty()) {
            return;
          }

          Object selectedItem = gsmGroupComboBox.getSelectedItem();
          refresher.updateGSMGroup(gsmGroupComboBox, selectedElems, selectedItem == null ? new GsmNoGroup() : (GSMGroup) selectedItem);
        }
      });

      channelsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

        @Override
        public void valueChanged(final ListSelectionEvent e) {
          if (gsmGroupComboBox.isFocusOwner()) {
            return;
          }

          GSMGroup gsmGroup = channelsTable.getSelectedElems().stream().map(VoiceServerChannelAdapter::getGSMGroup).findFirst().orElse(null);
          if (gsmGroup == null || gsmGroup instanceof GsmNoGroup) {
            gsmGroupComboBox.setSynteticSelectedItem(null);
            return;
          }

          for (int i = 0; i < gsmGroupComboBox.getItemCount(); ++i) {
            GSMGroup group = (GSMGroup) gsmGroupComboBox.getItemAt(i);
            if (group == null) {
              group = new GsmNoGroup();
            }

            if (group.getName().equals(gsmGroup.getName())) {
              gsmGroupComboBox.setSynteticSelectedIndex(i);
              break;
            }
          }
        }
      });
    }
    return gsmGroupComboBox;
  }

  private ExtendedComboBox getSimGroupComboBox() {
    if (simGroupComboBox == null) {
      simGroupComboBox = new ExtendedComboBox();

      simGroupComboBox.setPreferredSize(new Dimension(200, simGroupComboBox.getPreferredSize().height));
      simGroupComboBox.addActionListener(e -> {
        final List<VoiceServerChannelAdapter> selectedElems = channelsTable.getSelectedElems();
        if (selectedElems.isEmpty()) {
          return;
        }

        Object selectedItem = simGroupComboBox.getSelectedItem();
        refresher.updateSimGroupGroup(selectedElems, selectedItem == null ? new SimpleSimGroup(new SimNoGroup()) : (SimpleSimGroup) selectedItem);
      });

      channelsTable.getSelectionModel().addListSelectionListener(e -> {
        if (simGroupComboBox.isFocusOwner()) {
          return;
        }

        String SimGroupName = channelsTable.getSelectedElems().stream().filter(d -> d.getSIMGroupName() != null).map(VoiceServerChannelAdapter::getSIMGroupName).findFirst().orElse(null);
        if (SimGroupName == null) {
          simGroupComboBox.setSynteticSelectedItem(null);
          return;
        }

        for (int i = 0; i < simGroupComboBox.getItemCount(); ++i) {
          SimpleSimGroup group = (SimpleSimGroup) simGroupComboBox.getItemAt(i);
          if (group == null) {
            group = new SimpleSimGroup(new SimNoGroup());
          }

          if (group.getName().equals(SimGroupName)) {
            simGroupComboBox.setSynteticSelectedIndex(i);
            break;
          }
        }
      });
    }
    return simGroupComboBox;
  }

  private JComponent createCountersPanel() {
    JPanel panel = new JPanel(new BorderLayout());
    panel.add(indicators.getComponent(), BorderLayout.EAST);
    return panel;
  }

}

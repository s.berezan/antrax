/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flamesgroup.antrax.control.guiclient.panels.audiolist;

import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.AudioFileTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.IServerData;

public class DownloadAudioServerTable extends JUpdatableTable<IServerData, String> {

  private static final long serialVersionUID = 5542324858335143647L;

  private final String[] columnToolTips = {
      AudioFileTableTooltips.getServerTooltip()
  };

  public DownloadAudioServerTable() {
    super(new TableBuilder<IServerData, String>() {

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("server", String.class);
      }

      @Override
      public void buildRow(final IServerData src, final ColumnWriter<IServerData> dest) {
        dest.writeColumn(src.getName());
      }

      @Override
      public String getUniqueKey(final IServerData src) {
        return src.getName();
      }
    });
    setColumnToolTips(columnToolTips);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.ElementSelectionListener;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ScriptConflictsResolverPanel extends JPanel {

  private static final long serialVersionUID = -5582485663087161000L;

  private JUpdatableTable<ScriptConflict, Long> table;
  private ScriptsConflictPanel scriptsConflictsPanel;

  public ScriptConflictsResolverPanel() {
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    splitPane.setLeftComponent(new JScrollPane(getTable()));
    splitPane.setRightComponent(createScriptBox(new JScrollPane(getScriptsPanel())));
    splitPane.setDividerLocation(200);
    setLayout(new BorderLayout());
    add(splitPane, BorderLayout.CENTER);
    add(createButtonsPanel(), BorderLayout.SOUTH);
  }

  private Component createScriptBox(final JScrollPane jScrollPane) {
    JPanel panel = new JPanel(new BorderLayout());
    JPanel titlePanel = new JPanel();
    titlePanel.setLayout(new GridLayout(1, 2));
    titlePanel.add(new JLabel("Currently Systems Scripts", JLabel.CENTER));
    titlePanel.add(new JLabel("Future Scripts (Requites Updates)", JLabel.CENTER));
    panel.add(titlePanel, BorderLayout.NORTH);
    panel.add(jScrollPane, BorderLayout.CENTER);
    return panel;
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    retval.add(createNextButton());
    return retval;
  }

  private JButton createNextButton() {
    JButton retval = new JButton("Next");
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        if (getTable().getRowCount() == 0) {
          return;
        }
        int row = getTable().getSelectedRow();
        int nextRow = nextRow(row);
        while (getTable().getElemAt(nextRow).isFullyResolved() && nextRow != row) {
          nextRow = nextRow(nextRow);
        }
        if (nextRow == row) {
          nextRow = nextRow(row);
        }
        getTable().selectRow(nextRow);

      }

      private int nextRow(final int row) {
        if (row + 1 >= getTable().getRowCount()) {
          return 0;
        }
        return row + 1;
      }
    });
    return retval;
  }

  private ScriptsConflictPanel getScriptsPanel() {
    if (scriptsConflictsPanel == null) {
      scriptsConflictsPanel = new ScriptsConflictPanel();
    }
    return scriptsConflictsPanel;
  }

  private JUpdatableTable<ScriptConflict, Long> getTable() {
    if (table == null) {
      table = createTable();
    }
    return table;
  }

  private JUpdatableTable<ScriptConflict, Long> createTable() {
    JUpdatableTable<ScriptConflict, Long> retval = new JUpdatableTable<>(createTableBuilder());
    retval.addElementSelectionListener(new ElementSelectionListener<ScriptConflict>() {
      public boolean synteticChange;

      @Override
      public void handleSelectionChanged(final List<ScriptConflict> currSelection, final List<ScriptConflict> prevSelection) {
        if (synteticChange) {
          return;
        }
        synteticChange = true;
        getTable().setData(getResolvedConflicts());
        synteticChange = false;
        if (!currSelection.isEmpty()) {
          getScriptsPanel().setConflict(currSelection.get(0));
        } else {
          getScriptsPanel().setConflict(null);
        }

      }

    });
    return retval;
  }

  private TableBuilder<ScriptConflict, Long> createTableBuilder() {
    return new TableBuilder<ScriptConflict, Long>() {

      @Override
      public Long getUniqueKey(final ScriptConflict src) {
        if (src instanceof SimGroupScriptModificationConflict) {
          return ((SimGroupScriptModificationConflict) src).getOldGroup().getID();
        }
        throw new UnsupportedOperationException("Unsupported conflict type: " + src);

      }

      @Override
      public void buildRow(final ScriptConflict src, final ColumnWriter<ScriptConflict> dest) {
        if (src instanceof SimGroupScriptModificationConflict) {
          writeSimConfict((SimGroupScriptModificationConflict) src, dest);
        } else {
          throw new UnsupportedOperationException("Unsupported conflict type: " + src);
        }
      }

      private void writeSimConfict(final SimGroupScriptModificationConflict src, final ColumnWriter<ScriptConflict> dest) {
        dest.writeColumn(new Pair<>(src.getOldGroup().getName(), src.getOldGroup().getDescription()));
        dest.writeColumn(src.isFullyResolved());
        dest.writeColumn(src.getUnresolvedInfo());
      }

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("Name", String.class).setRenderer(new DefaultTableCellRenderer() {
          @Override
          public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (value instanceof Pair) {
              String name = ((Pair) value).first().toString();
              String description = ((Pair) value).second().toString();

              setText(name);
              if (description.isEmpty()) {
                setToolTipText(null);
              } else {
                setToolTipText(description);
              }
            } else {
              setText(value.toString());
              setToolTipText(null);
            }
            return cmp;
          }
        });

        columns.addColumn("Resolved", Boolean.class).setMaxWidth(100);
        columns.addColumn("Conflict", String.class);
      }
    };
  }

  public void setConflicts(final ScriptConflict... conflicts) {
    table.setData(conflicts);
  }

  public void initialize(final AntraxPluginsStore leftPS, final AntraxPluginsStore rightPS, final RegistryAccess registry) {
    getScriptsPanel().initialize(leftPS, rightPS, registry);
  }

  public ScriptConflict[] getResolvedConflicts() {
    getScriptsPanel().applyConflict();
    Collection<ScriptConflict> retval = getTable().getElems();
    return retval.toArray(new ScriptConflict[retval.size()]);
  }

  public boolean isAllConflictsResolved() {
    return getUnresolvedConflict() == null;
  }

  public ScriptConflict getUnresolvedConflict() {
    for (ScriptConflict c : getResolvedConflicts()) {
      if (!c.isFullyResolved()) {
        return c;
      }
    }
    return null;
  }
}

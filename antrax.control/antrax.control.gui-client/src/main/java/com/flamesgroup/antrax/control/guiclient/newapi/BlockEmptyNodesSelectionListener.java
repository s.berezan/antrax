/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import java.util.List;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

final class BlockEmptyNodesSelectionListener implements TreeSelectionListener {

  private final List<MenuSelectionListener> listeners;

  BlockEmptyNodesSelectionListener(final List<MenuSelectionListener> listeners) {
    this.listeners = listeners;
  }

  @Override
  public void valueChanged(final TreeSelectionEvent e) {
    AppPanel panel = getPanel(e.getPath());
    if (panel != null) {
      for (MenuSelectionListener l : listeners) {
        l.handleMenuSelected(panel);
      }
    } else {
      JTree tree = (JTree) e.getSource();
      tree.setSelectionPath(guessNextPath(tree, e.getOldLeadSelectionPath(), e.getNewLeadSelectionPath()));
    }

  }

  private TreePath guessNextPath(final JTree tree, final TreePath prevPath, final TreePath currPath) {
    if (prevPath == null) {
      return null;
    }
    int prevRow = tree.getRowForPath(prevPath);
    int currRow = tree.getRowForPath(currPath);
    if (Math.abs(prevRow - currRow) == 1) {
      if (prevRow - currRow > 0) {
        return ensureBestPathReturned(moveUp(tree, currRow), prevPath);
      } else {
        return ensureBestPathReturned(moveDown(tree, currRow), prevPath);
      }
    }
    return ensureBestPathReturned(null, prevPath);
  }

  private TreePath moveDown(final JTree tree, int currRow) {
    while (currRow++ < tree.getRowCount()) {
      TreePath path = tree.getPathForRow(currRow);
      if (!tree.isVisible(path)) {
        continue;
      }
      if (getPanel(path) != null) {
        return path;
      }
    }
    return null;
  }

  private TreePath moveUp(final JTree tree, int currRow) {
    while (currRow-- > 0) {
      TreePath path = tree.getPathForRow(currRow);
      if (!tree.isVisible(path)) {
        continue;
      }
      if (getPanel(path) != null) {
        return path;
      }
    }
    return null;
  }

  private TreePath ensureBestPathReturned(final TreePath path, final TreePath prevPath) {
    if (getPanel(path) != null) {
      return path;
    }
    if (getPanel(prevPath) != null) {
      return prevPath;
    }
    return null;
  }

  private AppPanel getPanel(final TreePath treePath) {
    if (treePath == null) {
      return null;
    }
    return ((AppMenuNode) treePath.getLastPathComponent()).getPanel();
  }
}

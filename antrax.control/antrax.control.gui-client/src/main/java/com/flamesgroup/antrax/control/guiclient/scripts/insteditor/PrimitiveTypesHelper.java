/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import java.util.HashMap;
import java.util.Map;

public class PrimitiveTypesHelper {
  private static final Map<String, Class<?>> primitives;

  static {
    primitives = new HashMap<>();
    primitives.put("int", Integer.class);
    primitives.put("boolean", Boolean.class);
    primitives.put("byte", Byte.class);
    primitives.put("long", Long.class);
    primitives.put("short", Short.class);
    primitives.put("float", Float.class);
    primitives.put("double", Double.class);
  }

  public static String primitiveToClass(final String type) {
    if (primitives.containsKey(type)) {
      return primitives.get(type).getCanonicalName();
    }
    return type;
  }
}

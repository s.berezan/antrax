/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.guiclient.widgets.PhoneNumberField;
import com.flamesgroup.antrax.control.guiclient.widgets.SimUIDField;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditPanel;
import com.flamesgroup.antrax.control.swingwidgets.pagging.JPaggingBar;
import com.flamesgroup.antrax.control.swingwidgets.pagging.JPaggingBar.PaggingHandler;
import com.flamesgroup.antrax.control.transfer.AntraxTransferHandler;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.unit.ICCID;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class CDRQueryBox extends JEditPanel {

  public class CDRQueryBoxTransferHandler extends AntraxTransferHandler {
    private static final long serialVersionUID = -3812871749870244471L;

    public void importObject(final GSMGroup gsmGroup) {
      gsmGroupFilter.setSelectedItem(gsmGroup.getName());
    }

    public void importObject(final SIMGroup simGroup) {
      simGroupFilter.setSelectedItem(simGroup.getName());
    }

    public void importObject(final ICCID uid) {
      simUID.setText(uid.getValue());
    }

    public void importObject(final SimViewData simViewData) {
      cdrPanel.dropSimChannel(simViewData);
    }

    public void importObject(final CDR cdr) {
      cdrPanel.showCDR(cdr.getID());
    }
  }

  private static final long serialVersionUID = -1689708163903644819L;

  public static class CDRQuery {
    private String gsmGroup;
    private String simGroup;
    private Date fromDate;
    private Date toDate;
    private int offset;
    private int pageSize;
    private String callerPhoneNumber;
    private String calledPhoneNumber;
    private ICCID simUID;

    public ICCID getSimUID() {
      return simUID;
    }

    public String getGSMGroup() {
      return gsmGroup;
    }

    public String getSIMGroup() {
      return simGroup;
    }

    public Date getFromDate() {
      return fromDate;
    }

    public Date getToDate() {
      return toDate;
    }

    public int getOffset() {
      return offset;
    }

    public void setOffset(final int offset) {
      this.offset = offset;
    }

    public String getCallerPhoneNumber() {
      return callerPhoneNumber;
    }

    public String getCalledPhoneNumber() {
      return calledPhoneNumber;
    }

    public int getPageSize() {
      return pageSize;
    }

    public void setPageSize(final int pageSize) {
      this.pageSize = pageSize;
    }
  }

  public interface CDRQueryActionListener {
    boolean handleQueryAction(CDRQuery query, boolean appendResult, boolean waitData);
  }

  private final JEditLabel lblCallerPhoneNumber = new JEditLabel("Caller number");
  private final PhoneNumberField callerPhoneNumberFilter = new PhoneNumberField();
  private final JEditLabel lblCalledPhoneNumber = new JEditLabel("Called number");
  private final PhoneNumberField calledPhoneNumberFilter = new PhoneNumberField();
  private final JEditLabel lblGsmGroup = new JEditLabel("GSM group");
  private final JComboBox gsmGroupFilter = new JComboBox();
  private final JEditLabel lblSimGroup = new JEditLabel("SIM group");
  private final JComboBox simGroupFilter = new JComboBox();
  private final JButton btnListPlus = new JButton("List+");
  private final JButton btnList = new JButton("Find");
  private final JPaggingBar<CDRQuery> paggingBar;
  private final SimUIDField simUID = new SimUIDField();
  private final JEditLabel lblSimUID = new JEditLabel("SIM UID");
  private final List<CDRQueryActionListener> listeners = new LinkedList<>();

  private final CDRPanel cdrPanel;
  private final JTimePeriodSelector timePeriodSelector;
  private final GuiProperties guiProperties;
  private int defaultPageSize = 100;

  public CDRQueryBox(final CDRPanel cdrPanel, final JTimePeriodSelector timePeriodSelector) {
    this.cdrPanel = cdrPanel;
    this.timePeriodSelector = timePeriodSelector;

    timePeriodSelector.setOpaque(false);
    simGroupFilter.setEditable(true);
    gsmGroupFilter.setEditable(true);
    guiProperties = new GuiProperties(cdrPanel.getClass().getSimpleName() + "_" + getClass().getSimpleName()) {

      private static final String DEFAULT_PAGE_SIZE = "defaultPageSize";

      @Override
      protected void restoreOutProperties() {
        String defaultPageSizeString = properties.getProperty(DEFAULT_PAGE_SIZE, "100");
        defaultPageSize = Integer.valueOf(defaultPageSizeString);
      }

      @Override
      protected void saveOutProperties() {
        properties.setProperty(DEFAULT_PAGE_SIZE, String.valueOf(paggingBar.getPageSize()));
      }
    };
    guiProperties.restoreProperties();

    paggingBar = new JPaggingBar<>(new PaggingHandler<CDRQuery>() {

      @Override
      public boolean onPageChanged(final CDRQuery queryData, final int pageSize, final int offset) {
        queryData.setPageSize(pageSize);
        queryData.setOffset(offset);
        return fireQuery(queryData, false, true);
      }

    }, defaultPageSize);
    paggingBar.getComponent().setOpaque(false);

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup().addGroup(
        layout.createSequentialGroup().addContainerGap().addGroup(
            layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(timePeriodSelector, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(GroupLayout.Alignment.LEADING,
                    layout.createSequentialGroup().addGroup(layout.createParallelGroup().addComponent(lblGsmGroup).addComponent(lblSimGroup)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup().addComponent(simGroupFilter, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup().addComponent(gsmGroupFilter, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup().addComponent(lblCallerPhoneNumber).addComponent(lblCalledPhoneNumber))
                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(callerPhoneNumberFilter, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblSimUID).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 10, 55)
                                    .addComponent(simUID, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnList))
                                .addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(calledPhoneNumberFilter, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)))))
                .addComponent(paggingBar.getComponent(), GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)).addGap(8, 8, 8)));
    layout.setVerticalGroup(layout.createParallelGroup().addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(timePeriodSelector, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
            layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(lblSimGroup)
                .addComponent(simGroupFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblCallerPhoneNumber)
                .addComponent(callerPhoneNumberFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblSimUID).addComponent(simUID)
                .addComponent(btnList)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
            layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(lblGsmGroup)
                .addComponent(gsmGroupFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblCalledPhoneNumber)
                .addComponent(calledPhoneNumberFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(paggingBar.getComponent(), GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addContainerGap(154, Short.MAX_VALUE)));

    btnList.putClientProperty("JButton.buttonType", "textured");
    btnListPlus.putClientProperty("JButton.buttonType", "textured");

    setupListeners();

    setTransferHandler(new CDRQueryBoxTransferHandler());
  }

  public void release() {
    guiProperties.saveProperties();
  }

  private void setupListeners() {
    btnList.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        paggingBar.applyQuery(collectQuery());
      }
    });

    btnListPlus.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        fireQuery(collectQuery(), true, false);
      }
    });
  }

  private String getFilterValue(final JComboBox cbox) {
    Object item = cbox.getEditor().getItem();
    String value = (item == null) ? null : item.toString();
    return (value == null || value.isEmpty()) ? null : value;
  }

  protected CDRQuery collectQuery() {
    String callerNumber = callerPhoneNumberFilter.getContent();
    String calledNumber = calledPhoneNumberFilter.getContent();
    String simUid = simUID.getContent();
    CDRQuery q = new CDRQuery();
    q.fromDate = timePeriodSelector.getFromDate();
    q.toDate = timePeriodSelector.getToDate();
    q.simGroup = getFilterValue(simGroupFilter);
    q.gsmGroup = getFilterValue(gsmGroupFilter);
    q.callerPhoneNumber = callerNumber.isEmpty() ? null : callerNumber;
    q.calledPhoneNumber = calledNumber.isEmpty() ? null : calledNumber;
    q.simUID = simUid.isEmpty() ? null : new ICCID(simUid);
    return q;
  }

  public void clearFilters() {
    callerPhoneNumberFilter.setContent(null);
    calledPhoneNumberFilter.setContent(null);
    simUID.setContent(null);
    timePeriodSelector.setFromDate(null);
    timePeriodSelector.setToDate(null);
    simGroupFilter.getEditor().setItem(null);
    gsmGroupFilter.getEditor().setItem(null);

  }

  public void generateQuery(final ICCID simUid, final Date fromDate, final Date toDate, final String simGroupName) {
    CDRQuery query = new CDRQuery();
    query.simUID = simUid;
    if (simUid != null) {
      this.simUID.setText(simUid.getValue());
    }
    query.fromDate = fromDate;
    query.toDate = toDate;
    query.pageSize = 100;
    query.gsmGroup = null;
    query.offset = 0;
    if (simGroupName != null) {
      query.simGroup = simGroupName;
      simGroupFilter.getEditor().setItem(simGroupName);
    } else {
      query.simGroup = null;
    }
    fireQuery(query, true, true);
  }

  protected boolean fireQuery(final CDRQuery q, final boolean appendResult, final boolean waitData) {
    boolean result = false;
    for (CDRQueryActionListener l : listeners) {
      if (l.handleQueryAction(q, appendResult, waitData)) {
        result = true;
      }
    }

    return result;
  }

  public void addQueryListener(final CDRQueryActionListener l) {
    listeners.add(l);
  }

  public void setComboBoxValue(final JComboBox cbox, final String[] values) {
    Object oldValue = cbox.getEditor().getItem();
    cbox.removeAllItems();
    if (values != null) {
      for (String groupName : values) {
        cbox.addItem(groupName);
      }
    }
    cbox.getEditor().setItem(oldValue);
  }

  public void setSimGroups(final String[] simGroupNames) {
    setComboBoxValue(simGroupFilter, simGroupNames);
  }

  public void setGsmGroups(final String[] gsmGroupNames) {
    setComboBoxValue(gsmGroupFilter, gsmGroupNames);
  }

}

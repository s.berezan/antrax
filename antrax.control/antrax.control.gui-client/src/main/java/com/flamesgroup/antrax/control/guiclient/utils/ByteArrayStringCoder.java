/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

public class ByteArrayStringCoder {

  private static final String SEPARATOR = " ";

  private static String byteToHexStr(final byte b) {
    String sign = (b < 0 ? "-" : "") + "0x";
    String snum = Integer.toString(b, 16);
    if (b < 0) {
      snum = snum.substring(1);
    }
    return sign + "00".substring(0, 2 - snum.length()) + snum;
  }

  public static String encode(final byte[] data) {
    StringBuffer buffer = new StringBuffer();
    for (byte num : data) {
      buffer.append(byteToHexStr(num));
      buffer.append(SEPARATOR);
    }
    return buffer.toString().trim();
  }

  public static byte[] decode(final String str) throws NumberFormatException {
    String[] parts = str.split(SEPARATOR);
    byte[] arr = new byte[parts.length];
    for (int i = 0; i < parts.length; i++) {
      arr[i] = Byte.decode(parts[i]);
    }
    return arr;
  }

}

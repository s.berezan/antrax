/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class VoiceServerCallStatisticTableTooltips {

  private VoiceServerCallStatisticTableTooltips() {
  }

  public static String getVoiceServerTooltip() {
    return new TooltipHeaderBuilder("Name of voice server").build();
  }

  public static String getPrefixTooltip() {
    return new TooltipHeaderBuilder("Prefix of call numbers").build();
  }

  public static String getTotalTooltip() {
    return new TooltipHeaderBuilder("Total calls per certain time").build();
  }

  public static String getSuccessfulTooltip() {
    return new TooltipHeaderBuilder("Successful calls per certain time").build();
  }

  public static String getDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration per certain time").build();
  }

  public static String getAsrTooltip() {
    return new TooltipHeaderBuilder("Answer seizure ratio per certain time").build();
  }

  public static String getAcdTooltip() {
    return new TooltipHeaderBuilder("Average call duration per certain time").build();
  }

}

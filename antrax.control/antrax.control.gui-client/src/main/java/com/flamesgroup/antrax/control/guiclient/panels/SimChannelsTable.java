/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.guiclient.panels.tablebuilders.SimChannelKey;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.ChannelConfigCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.ChannelStatusCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.EnableCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.LockCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.PairChannelUIDCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.PercentCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.TariffPlanExpireExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.TimePeriodCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.UserMessageExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.BalanceTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelConfigRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelStatusRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelUidTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.EnableTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.GroupNameTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.HintTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.LockTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.PercentTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TariffPlanExpireRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutDateHintTableCellRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.UserMessageRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.SimChannelsTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.ContextSearchRowFilter;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.util.Comparator;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class SimChannelsTable extends JUpdatableTable<SimViewData, SimChannelKey> {

  private static final long serialVersionUID = 8968228423713663164L;

  private final String[] columnToolTips = {
      SimChannelsTableTooltips.getGatewayTooltip(),
      SimChannelsTableTooltips.getPreviousGatewayTooltip(),
      SimChannelsTableTooltips.getSimHolderTooltip(),
      SimChannelsTableTooltips.getMobileChannelTooltip(),
      SimChannelsTableTooltips.getSimCardStateTooltip(),
      SimChannelsTableTooltips.getSimCardLockStateTooltip(),
      SimChannelsTableTooltips.getLockTimeoutTooltip(),
      SimChannelsTableTooltips.getLockReasonTooltip(),
      SimChannelsTableTooltips.getSimGroupTooltip(),
      SimChannelsTableTooltips.getPhoneNumberTooltip(),
      SimChannelsTableTooltips.getStatusTooltip(),
      SimChannelsTableTooltips.getStatusTimeoutTooltip(),
      SimChannelsTableTooltips.getAsrTooltip(),
      SimChannelsTableTooltips.getAcdTooltip(),
      SimChannelsTableTooltips.getSuccessfulCallsTooltip(),
      SimChannelsTableTooltips.getTotalCallsTooltip(),
      SimChannelsTableTooltips.getCallsDurationTooltip(),
      SimChannelsTableTooltips.getIncomingSuccessfulCallsTooltip(),
      SimChannelsTableTooltips.getIncomingTotalCallsTooltip(),
      SimChannelsTableTooltips.getIncomingCallsDurationTooltip(),
      SimChannelsTableTooltips.getLastCallDurationTooltip(),
      SimChannelsTableTooltips.getSuccessSmsTooltip(),
      SimChannelsTableTooltips.getTotalSmsTooltip(),
      SimChannelsTableTooltips.getSmsAsrTooltip(),
      SimChannelsTableTooltips.getIncomingTotalSmsTooltip(),
      SimChannelsTableTooltips.getTotalSmsStatusesTooltip(),
      SimChannelsTableTooltips.getIccidTooltip(),
      SimChannelsTableTooltips.getImeiTooltip(),
      SimChannelsTableTooltips.getBalanceTooltip(),
      SimChannelsTableTooltips.getLastReconfigurationTimeTooltip(),
      SimChannelsTableTooltips.getAllowedInternetTooltip(),
      SimChannelsTableTooltips.getNoteTooltip(),
      SimChannelsTableTooltips.getUserMessageTooltip(),
      SimChannelsTableTooltips.getTariffPlanExpiresTooltip(),
      SimChannelsTableTooltips.getChannelConnectionStateTooltip(),
      SimChannelsTableTooltips.getChannelLicenseStateTooltip()
  };

  public SimChannelsTable() {
    super(new SimChannelsTableBuilder());
    setName("SIM channels");
    setDragEnabled(true);
    setRowSelectionAllowed(true);
    setRowFilter(createRowFilter());
    setColumnToolTips(columnToolTips);
  }

  private static class SimChannelsTableBuilder implements TableBuilder<SimViewData, SimChannelKey> {

    @Override
    public SimChannelKey getUniqueKey(final SimViewData src) {
      return new SimChannelKey(src.getCurrentGateway(), src.getSimHolder(), src.getGsmHolder());
    }

    @Override
    public void buildRow(final SimViewData src, final ColumnWriter<SimViewData> dest) {
      long currentTimeMillis = src.getServerTimeMillis();

      dest.writeColumn(src.getCurrentGateway());
      dest.writeColumn(src.getPreviousGateway());
      dest.writeColumn(new Pair<>(src.getSimHolder(), !src.isLive() | src.isEmpty()), src);
      dest.writeColumn(src.getGsmHolder());
      dest.writeColumn(src.isEnabled());
      dest.writeColumn(src.isLocked());
      dest.writeColumn(src.getLockTime() > 0 ? currentTimeMillis - src.getLockTime() : 0);
      dest.writeColumn(src.getLockReason());
      dest.writeColumn(src.getSimGroupName());
      if (src.getPhoneNumber() != null && !src.getPhoneNumber().isPrivate()) {
        dest.writeColumn(src.getPhoneNumber().getValue());
      } else {
        dest.writeColumn(null);
      }
      dest.writeColumn(src.getStatus());
      dest.writeColumn(src.getStatusTime() > 0 ? currentTimeMillis - src.getStatusTime() : 0);
      dest.writeColumn(calculateASR(src.getSuccessfulCallsCount(), src.getTotalCallsCount()));
      dest.writeColumn(calculateACD(src.getCallDuration(), src.getSuccessfulCallsCount()));
      dest.writeColumn(src.getSuccessfulCallsCount());
      dest.writeColumn(src.getTotalCallsCount());
      dest.writeColumn(src.getCallDuration());
      dest.writeColumn(src.getIncomingSuccessfulCallsCount());
      dest.writeColumn(src.getIncomingTotalCallsCount());
      dest.writeColumn(src.getIncomingCallDuration());
      dest.writeColumn(src.getLastCallDuration());
      dest.writeColumn(src.getSuccessOutgoingSmsCount());
      dest.writeColumn(src.getTotalOutgoingSmsCount());
      dest.writeColumn(calculateASR(src.getSuccessOutgoingSmsCount(), src.getTotalOutgoingSmsCount()));
      dest.writeColumn(src.getIncomingSMSCount());
      dest.writeColumn(src.getCountSmsStatuses());
      dest.writeColumn(src.getUid() == null ? null : src.getUid().getValue());
      dest.writeColumn(src.getImei() == null ? null : src.getImei().getValue());
      dest.writeColumn(src.getBalance());
      dest.writeColumn(src.getReconfigureTime() > 0 ? currentTimeMillis - src.getReconfigureTime() : 0);
      dest.writeColumn(src.isAllowInternet() ? "Allowed" : "Disallowed");
      dest.writeColumn(src.getNote());
      SimRuntimeUserMessageData userMessages = new SimRuntimeUserMessageData(
          src.getUserMessage1(), currentTimeMillis - src.getUserMessageTime1(),
          src.getUserMessage2(), currentTimeMillis - src.getUserMessageTime2(),
          src.getUserMessage3(), currentTimeMillis - src.getUserMessageTime3());
      dest.writeColumn(userMessages);
      dest.writeColumn(new Pair<>(src.getTariffPlanEndDate(), src.getServerTimeMillis()));
      dest.writeColumn(new Pair<>(src.isLive(), src.isEmpty()));
      dest.writeColumn(new Pair<>(src.getChannelConfig(), src.getServerTimeMillis()));
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      // Renderer's
      DefaultTableCellRenderer alignCenterRenderer = new DefaultTableCellRenderer();
      alignCenterRenderer.setHorizontalAlignment(SwingConstants.CENTER);

      ChannelUidTCRenderer channelUidRenderer = new ChannelUidTCRenderer();
      ChannelStatusRenderer channelStatusRenderer = new ChannelStatusRenderer();
      ChannelConfigRenderer channelConfigRenderer = new ChannelConfigRenderer();

      // Comparators
      Comparator simHolderComparator = new Comparator<Pair<ChannelUID, Boolean>>() {
        @Override
        public int compare(final Pair<ChannelUID, Boolean> o1, final Pair<ChannelUID, Boolean> o2) {
          return Comparator.nullsLast(ChannelUID::compareTo).compare(o1.first(), o2.first());
        }
      };
      Comparator userMessageComparator = Comparator.nullsLast(new Comparator<SimRuntimeUserMessageData>() {
        @Override
        public int compare(final SimRuntimeUserMessageData o1, final SimRuntimeUserMessageData o2) {
          return Comparator.nullsLast(String::compareTo).compare(o1.toString(), o2.toString());
        }
      });
      Comparator tariffPlanComparator = new Comparator<Pair<Long, Long>>() {
        @Override
        public int compare(final Pair<Long, Long> o1, final Pair<Long, Long> o2) {
          return Long.compare(o1.first(), o2.first());
        }
      };

      // Columns
      columns.addColumn("Gateway", String.class)
          /*  */.setPreferredWidth(120);

      columns.addColumn("Prev gateway", String.class)
          /*  */.setPreferredWidth(120);

      columns.addColumn("SIM holder", ChannelUID.class)
          /*  */.setRenderer(channelUidRenderer)
          /*  */.setCellExporter(new PairChannelUIDCellExporter())
          /*  */.setComparator(simHolderComparator)
          /*  */.setMinWidth(110)
          /*  */.setPreferredWidth(110);

      columns.addColumn("Mobile chan.", ChannelUID.class)
          /*  */.setRenderer(channelUidRenderer)
          /*  */.setMinWidth(110)
          /*  */.setPreferredWidth(110);

      columns.addColumn("E", Boolean.class)
          /*   */.setMaxWidth(30)
          /*   */.setPreferredWidth(30)
          /*   */.setRenderer(new EnableTCRenderer())
          /*   */.setCellExporter(new EnableCellExporter());

      columns.addColumn("L", Boolean.class)
          /*   */.setMaxWidth(30)
          /*   */.setPreferredWidth(30)
          /*   */.setRenderer(new LockTCRenderer())
          /*   */.setCellExporter(new LockCellExporter());

      columns.addColumn("Lock timeout", Long.class)
          /*  */.setRenderer(new TimeoutDateHintTableCellRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter())
          /*  */.setPreferredWidth(85);

      columns.addColumn("(Un)Lock reason", String.class)
          /*  */.setRenderer(new HintTCRenderer())
          /*  */.setPreferredWidth(170);

      columns.addColumn("SIM group", String.class)
          /*  */.setRenderer(new GroupNameTCRenderer())
          /*  */.setPreferredWidth(130);

      columns.addColumn("Phone Number", String.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(100);

      columns.addColumn("Status", String.class)
          /*  */.setPreferredWidth(150);

      columns.addColumn("Status timeout", Long.class)
          /*  */.setRenderer(new TimeoutDateHintTableCellRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter())
          /*  */.setPreferredWidth(100);

      columns.addColumn("ASR", Integer.class)
          /*  */.setRenderer(new PercentTCRenderer())
          /*  */.setCellExporter(new PercentCellExporter())
          /*  */.setPreferredWidth(50);

      columns.addColumn("ACD", Long.class)
          /*  */.setRenderer(new TimeoutTCRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter())
          /*  */.setPreferredWidth(50);

      columns.addColumn("Successful", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(68);

      columns.addColumn("Total", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Calls dur.", Long.class)
          /*  */.setRenderer(new TimeoutTCRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter());

      columns.addColumn("Inc. Success", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Inc. Total", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Inc. Calls dur.", Long.class)
          /*  */.setRenderer(new TimeoutTCRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter());

      columns.addColumn("Last call dur.", Long.class)
          /*  */.setRenderer(new TimeoutTCRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter());

      columns.addColumn("Success SMS", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Total SMS", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("SMS ASR", Integer.class)
          /*  */.setRenderer(new PercentTCRenderer())
          /*  */.setCellExporter(new PercentCellExporter())
          /*  */.setPreferredWidth(50);

      columns.addColumn("Inc. Total SMS", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Total SMS statuses", Integer.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(60);

      columns.addColumn("ICCID", String.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setPreferredWidth(180);

      columns.addColumn("IMEI", String.class)
          /*  */.setRenderer(alignCenterRenderer)
          /*  */.setMinWidth(145)
          /*  */.setPreferredWidth(145);

      columns.addColumn("Balance", Double.class)
          /*  */.setRenderer(new BalanceTCRenderer())
          /*  */.setMinWidth(60)
          /*  */.setPreferredWidth(60);

      columns.addColumn("Last reconf. time", Long.class)
          /*  */.setRenderer(new TimeoutDateHintTableCellRenderer())
          /*  */.setCellExporter(new TimePeriodCellExporter())
          /*  */.setMinWidth(100)
          /*  */.setPreferredWidth(100);

      columns.addColumn("Allowed internet", String.class)
          /*  */.setPreferredWidth(170);
      
      columns.addColumn("Note", String.class)
          /*  */.setPreferredWidth(170);

      columns.addColumn("User message", SimRuntimeUserMessageData.class)
          /*  */.setRenderer(new UserMessageRenderer())
          /*  */.setPreferredWidth(170)
          /*  */.setCellExporter(new UserMessageExporter())
          /*  */.setComparator(userMessageComparator);

      columns.addColumn("Tariff plan expires", Long.class)
          /*  */.setRenderer(new TariffPlanExpireRenderer())
          /*  */.setCellExporter(new TariffPlanExpireExporter())
          /*  */.setMinWidth(150)
          /*  */.setPreferredWidth(150)
          /*  */.setComparator(tariffPlanComparator);

      columns.addColumn(null, Image.class)
          /*  */.setMaxWidth(30)
          /*  */.setPreferredWidth(30)
          /*  */.setRenderer(channelStatusRenderer)
          /*  */.setCellExporter(new ChannelStatusCellExporter());

      columns.addColumn(null, ChannelConfig.class)
          /*  */.setMaxWidth(30)
          /*  */.setMinWidth(30)
          /*  */.setRenderer(channelConfigRenderer)
          /*  */.setCellExporter(new ChannelConfigCellExporter());
    }

    private int calculateASR(final int successfulCallsCount, final int totalCallsCount) {
      return (totalCallsCount == 0) ? 0 : 100 * successfulCallsCount / totalCallsCount;
    }

    private long calculateACD(final long callsDuration, final int successfulCallsCount) {
      return (successfulCallsCount == 0) ? 0 : (callsDuration / successfulCallsCount);
    }
  }

  private ContextSearchRowFilter createRowFilter() {
    return new ContextSearchRowFilter() {

      @Override
      protected String getEntryValue(final Entry<? extends Object, ? extends Object> entry, final int column) {
        String strValue;
        switch (column) {
          case 2:
            ChannelUID simChannel = (ChannelUID) ((Pair) entry.getValue(column)).first();
            strValue = simChannel.getAliasCanonicalName();
            break;
          case 3:
            ChannelUID gsmChannel = (ChannelUID) entry.getValue(column);
            strValue = gsmChannel == null ? "" : gsmChannel.getAliasCanonicalName();
            break;
          default:
            strValue = super.getEntryValue(entry, column);
            break;
        }
        return strValue;
      }
    };
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.CDRPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.CfgPrefixListPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.CfgVoipAntiSpamPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.DownloadAudioPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.GSMGroupsPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.GsmViewPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.IvrTemplatesPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RegistryPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ScriptsPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SessionsPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SimGroupsPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SimGroupsReportPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SimHistoryPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SimServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SmsHistoryPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.StatisticSmsPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.StatisticVoiceServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.StatisticVoipAntiSpamPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.VoiceServerCallStatisticProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.VoiceServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;

public class AppMenuBuilder {
  private static final String N_A = "N/A";

  public void buildMenu(final AppMenuNode menuNode, final BeansPool beansPool) throws Exception {
    addVoiceServersNode(menuNode, beansPool);
    addSimServersNode(menuNode, beansPool);
    addSessionsNode(menuNode, beansPool);
    addStatisticNode(menuNode, beansPool);
    addGsmViewNode(menuNode, beansPool);
    addReportsNode(menuNode, beansPool);
    addConfigurationNode(menuNode, beansPool);
    addUtilityNode(menuNode, beansPool);
  }

  private void addUtilityNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    AppMenuNode utilityNode = new AppMenuNode("UTILITY", null, null, null);
    addRegistryNode(utilityNode, beansPool);
    addDownloadAudioNode(utilityNode, beansPool);
    addIvrTamplateNode(utilityNode, beansPool);
    if (utilityNode.getChildCount() > 0) {
      menuNode.addNode(utilityNode);
    }
  }

  private void addRegistryNode(final AppMenuNode utilityNode, final BeansPool beansPool) throws NotPermittedException {
    RegistryPanelProvider panelProvider = new RegistryPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      utilityNode.addNode(new AppMenuNode("Registry", IconPool.getShared("/img/registry.png"), null, panelProvider));
    }
  }

  private void addDownloadAudioNode(final AppMenuNode utilityNode, final BeansPool beansPool) throws NotPermittedException {
    DownloadAudioPanelProvider panelProvider = new DownloadAudioPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      utilityNode.addNode(new AppMenuNode("Download audio", IconPool.getShared("/img/down.png"), null, panelProvider));
    }
  }

  private void addIvrTamplateNode(final AppMenuNode utilityNode, final BeansPool beansPool) throws NotPermittedException {
    IvrTemplatesPanelProvider panelProvider = new IvrTemplatesPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      utilityNode.addNode(new AppMenuNode("IVR templates", IconPool.getShared("/img/ivr.png"),
          null, panelProvider));
    }
  }

  private void addReportsNode(final AppMenuNode menuNode, final BeansPool beansPool) throws Exception {
    AppMenuNode reportsNode = new AppMenuNode("REPORTS", null, null, null);
    addCDRNode(reportsNode, beansPool);
    addSimHistoryNode(reportsNode, beansPool);
    addSMSNode(reportsNode, beansPool);
    addCallHistoryNode(reportsNode, beansPool);
    addSimGroupsReportNode(reportsNode, beansPool);
    if (reportsNode.getChildCount() > 0) {
      menuNode.addNode(reportsNode);
    }
  }

  private void addSimGroupsReportNode(final AppMenuNode reportsNode, final BeansPool beansPool) throws Exception {
    SimGroupsReportPanelProvider panelProvider = new SimGroupsReportPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      reportsNode.addNode(new AppMenuNode("Sim Groups Report", IconPool.getShared("/img/report/sim_history.png"), null, panelProvider));
    }
  }

  private void addCDRNode(final AppMenuNode reportsNode, final BeansPool beansPool) throws NotPermittedException {
    CDRPanelProvider panelProvider = new CDRPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      reportsNode.addNode(new AppMenuNode("CDR", IconPool.getShared("/img/report/sim_history.png"), null, panelProvider));
    }
  }

  private void addSMSNode(final AppMenuNode reportsNode, final BeansPool beansPool) throws NotPermittedException {
    SmsHistoryPanelProvider panelProvider = new SmsHistoryPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      reportsNode.addNode(new AppMenuNode("SMS History", IconPool.getShared("/img/report/sim_history.png"), null, panelProvider));
    }
  }

  private void addCallHistoryNode(final AppMenuNode reportsNode, final BeansPool beansPool) throws NotPermittedException {
    VoiceServerCallStatisticProvider panelProvider = new VoiceServerCallStatisticProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      reportsNode.addNode(new AppMenuNode("Call History", IconPool.getShared("/img/report/sim_history.png"), null, panelProvider));
    }
  }

  private void addSimHistoryNode(final AppMenuNode reportsNode, final BeansPool beansPool) throws NotPermittedException {
    SimHistoryPanelProvider panelProvider = new SimHistoryPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      reportsNode.addNode(new AppMenuNode("Sim History", IconPool.getShared("/img/report/sim_history.png"), null, panelProvider));
    }
  }

  private void addStatisticNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    AppMenuNode statisticNode = new AppMenuNode("STATISTIC", null, null, null);
    addStatisticVoiceServersNode(statisticNode, beansPool);
    addStatisticVoipAntiSpamNode(statisticNode, beansPool);
    addStatisticSMSNode(statisticNode, beansPool);
    if (statisticNode.getChildCount() > 0) {
      menuNode.addNode(statisticNode);
    }
  }

  private void addStatisticVoiceServersNode(final AppMenuNode statisticNode, final BeansPool beansPool) throws NotPermittedException {
    StatisticVoiceServersPanelProvider panelProvider = new StatisticVoiceServersPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      statisticNode.addNode(new AppMenuNode("Voice Servers", IconPool.shared().get("/img/statistic/voice_server_statistic.png"), null, panelProvider));
    }
  }

  private void addStatisticVoipAntiSpamNode(final AppMenuNode statisticNode, final BeansPool beansPool) throws NotPermittedException {
    StatisticVoipAntiSpamPanelProvider panelProvider = new StatisticVoipAntiSpamPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      statisticNode.addNode(new AppMenuNode("VoIP AntiSpam", IconPool.getShared("/img/antispam.png"), null, panelProvider));
    }
  }

  private void addStatisticSMSNode(final AppMenuNode statisticNode, final BeansPool beansPool) throws NotPermittedException {
    StatisticSmsPanelProvider panelProvider = new StatisticSmsPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      statisticNode.addNode(new AppMenuNode("SMS", IconPool.getShared("/img/message.png"), null, panelProvider));
    }
  }

  private void addGsmViewNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    GsmViewPanelProvider panelProvider = new GsmViewPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("GSM-view", null, null, panelProvider));
    }
  }

  private void addSimServersNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    SimServersPanelProvider panelProvider = new SimServersPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("SIM SERVERS", null, createSimServersBadge(), panelProvider));
    }
  }

  private void addVoiceServersNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    VoiceServersPanelProvider panelProvider = new VoiceServersPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("VOICE SERVERS", null, createVoiceServersBadge(), panelProvider));
    }
  }

  private void addSessionsNode(final AppMenuNode menuNode, final BeansPool beansPool) throws Exception {
    SessionsPanelProvider panelProvider = new SessionsPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("SESSIONS", null, createSessionsBadge(), panelProvider));
    }
  }

  private Badge createVoiceServersBadge() {
    Badge badge = new Badge();
    badge.setNotice(N_A);
    badge.setNoticeHelp("Count of stopped voice servers");
    return badge;
  }

  private Badge createSimServersBadge() {
    Badge badge = new Badge();
    badge.setNotice(N_A);
    badge.setNoticeHelp("Count of stopped sim servers");
    return badge;
  }

  private Badge createSessionsBadge() {
    Badge badge = new Badge();
    badge.setInfo(N_A);
    badge.setInfoHelp("Number of users currently connected to the server");
    return badge;
  }

  private void addConfigurationNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    AppMenuNode reportsNode = new AppMenuNode("CONFIGURATION", null, null, null);
    addServersNode(reportsNode, beansPool);
    addGsmGroupsNode(reportsNode, beansPool);
    addSimGroupsNode(reportsNode, beansPool);
    addScriptsNode(reportsNode, beansPool);
    addVoipAntiSpamNode(reportsNode, beansPool);
    addPrefixListNode(reportsNode, beansPool);
    if (reportsNode.getChildCount() > 0) {
      menuNode.addNode(reportsNode);
    }
  }

  private void addServersNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    ServersPanelProvider panelProvider = new ServersPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("Servers", IconPool.getShared("/img/black_server.png"), null, panelProvider));
    }
  }

  private void addScriptsNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    ScriptsPanelProvider panelProvider = new ScriptsPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("Scripts", IconPool.getShared("/img/configuration/script.png"), null, panelProvider));
    }
  }

  private void addVoipAntiSpamNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    CfgVoipAntiSpamPanelProvider panelProvider = new CfgVoipAntiSpamPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("VoIP AntiSpam", IconPool.getShared("/img/antispam.png"), null, panelProvider));
    }
  }

  private void addPrefixListNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    CfgPrefixListPanelProvider panelProvider = new CfgPrefixListPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("Prefix mark lists", IconPool.getShared("/img/configuration/activation-cfg.png"), null, panelProvider));
    }
  }

  private void addSimGroupsNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    SimGroupsPanelProvider panelProvider = new SimGroupsPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("SIM groups", IconPool.getShared("/img/configuration/simgroups.png"), null, panelProvider));
    }
  }

  private void addGsmGroupsNode(final AppMenuNode menuNode, final BeansPool beansPool) throws NotPermittedException {
    GSMGroupsPanelProvider panelProvider = new GSMGroupsPanelProvider();
    if (panelProvider.checkPermissions(beansPool)) {
      menuNode.addNode(new AppMenuNode("GSM groups", IconPool.getShared("/img/transmit.png"), null, panelProvider));
    }
  }

}

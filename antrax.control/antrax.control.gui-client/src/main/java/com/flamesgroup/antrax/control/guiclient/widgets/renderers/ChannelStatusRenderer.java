/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.commons.Pair;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ChannelStatusRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 2024638694941577376L;

  private static final Icon liveIcon = IconPool.getShared("/img/channels/live.png");
  private static final String liveToolTip = "Available channel";
  private static final Icon unLiveIcon = IconPool.getShared("/img/channels/unlive.png");
  private static final String unLiveToolTip = "Unavailable channel";
  private static final Icon simNotFound = IconPool.getShared("/img/channels/ip_config_parameters_invalid.png");
  private static final String simNotFoundToolTip = "Sim not found";

  public ChannelStatusRenderer() {
    this.setHorizontalAlignment(SwingConstants.CENTER);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    setIcon(null);
    Object live = getValue(value).first();
    Object empty = getValue(value).second();
    if (live instanceof Boolean && empty instanceof Boolean) {
      setText(null);

      Boolean liveBool = (Boolean) live;
      Boolean emptyBool = (Boolean) empty;
      if (!emptyBool && liveBool) {
        setIcon(liveIcon);
        setToolTipText(liveToolTip);
      } else if (emptyBool) {
        setIcon(simNotFound);
        setToolTipText(simNotFoundToolTip);
      } else {
        setIcon(unLiveIcon);
        setToolTipText(unLiveToolTip);
      }
    } else {
      setText(value.toString());
    }

    return component;
  }

  private Pair<?, ?> getValue(final Object value) {
    if (value instanceof Pair<?, ?>) {
      return (Pair<?, ?>) value;
    } else {
      return null;
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class AppNodesModel implements TreeModel {
  private final AppMenuNode root;
  private final List<TreeModelListener> listeners = new LinkedList<>();

  public AppNodesModel(final AppMenuNode root) {
    this.root = root;
    this.root.setTreeModelListener(new TreeModelListener() {

      @Override
      public void treeNodesChanged(final TreeModelEvent e) {
        for (TreeModelListener l : listeners) {
          l.treeNodesChanged(e);
        }
      }

      @Override
      public void treeNodesInserted(final TreeModelEvent e) {
        for (TreeModelListener l : listeners) {
          l.treeNodesInserted(e);
        }
      }

      @Override
      public void treeNodesRemoved(final TreeModelEvent e) {
        for (TreeModelListener l : listeners) {
          l.treeNodesRemoved(e);
        }
      }

      @Override
      public void treeStructureChanged(final TreeModelEvent e) {
        for (TreeModelListener l : listeners) {
          l.treeStructureChanged(e);
        }
      }
    });
  }

  @Override
  public Object getRoot() {
    return root;
  }

  @Override
  public Object getChild(final Object parent, final int index) {
    AppMenuNode node = (AppMenuNode) parent;
    return node.getChild(index);
  }

  @Override
  public int getChildCount(final Object parent) {
    AppMenuNode node = (AppMenuNode) parent;
    return node.getChildCount();
  }

  @Override
  public boolean isLeaf(final Object node) {
    return getChildCount(node) == 0;
  }

  @Override
  public void valueForPathChanged(final TreePath path, final Object newValue) {
    throw new UnsupportedOperationException("Not implemented yet");
  }

  @Override
  public int getIndexOfChild(final Object parent, final Object child) {
    AppMenuNode node = (AppMenuNode) parent;
    return node.getIndexOf((AppMenuNode) child);
  }

  @Override
  public void addTreeModelListener(final TreeModelListener l) {
    listeners.add(l);
  }

  @Override
  public void removeTreeModelListener(final TreeModelListener l) {
    listeners.remove(l);
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.control.guiclient.utils.UIConstants;
import com.flamesgroup.antrax.control.swingwidgets.editor.JCodeEditor;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;

import java.awt.*;

import javax.swing.*;

public class CodeViewer extends JPanel {

  private static final long serialVersionUID = -6166830922649338182L;

  private final JLabel titleLabel;
  private final JCodeEditor codeEditor;

  public CodeViewer() {
    titleLabel = new JLabel();

    codeEditor = new JCodeEditor();
    codeEditor.setEditable(false);

    JScrollPane editorScrollPane = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(codeEditor);
    editorScrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
    setBorder(BorderFactory.createLineBorder(UIConstants.BORDER_COLOR));

    JPanel header = new JPanel();
    header.add(titleLabel);
    header.setMinimumSize(new Dimension(0, 35));
    header.setPreferredSize(new Dimension(0, 35));
    header.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIConstants.BORDER_COLOR));

    setLayout(new BorderLayout());
    add(header, BorderLayout.NORTH);
    add(editorScrollPane, BorderLayout.CENTER);
  }

  public void setTitle(final String title) {
    titleLabel.setText(title);
  }

  public void clear() {
    setTitle("");
    codeEditor.setText("");
  }

  public void setCode(final String code) {
    codeEditor.setText(code);
    codeEditor.setCaretPosition(0);
  }

}

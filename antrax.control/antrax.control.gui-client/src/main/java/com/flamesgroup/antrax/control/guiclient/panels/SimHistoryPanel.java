/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.SimHistoryTableBuilder;
import com.flamesgroup.antrax.control.guiclient.commadapter.VoiceServerChannelAdapter;
import com.flamesgroup.antrax.control.guiclient.domain.SimEventRecRow;
import com.flamesgroup.antrax.control.guiclient.indicators.DefaultIndicators;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.panels.transfer.CallPathTransfer;
import com.flamesgroup.antrax.control.guiclient.panels.transfer.SmsPanelTransfer;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.CallableRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherUtils;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.WaitingReference;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ElementSelectionListener;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.transfer.AntraxTransferHandler;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class SimHistoryPanel extends JPanel implements AppPanel {

  private static final Logger logger = LoggerFactory.getLogger(SimHistoryPanel.class);

  public class SimHistoryPanelTransferHandler extends AntraxTransferHandler {

    private static final long serialVersionUID = 2976696116372063102L;

    public void importObject(final SimViewData simViewData) {
      dropSimChannel(simViewData.getUid(), simViewData.getSimHolder(), simViewData.getGsmHolder(), simViewData.getSimGroupName(),
          simViewData.getPhoneNumber());
    }

    public void importObject(final VoiceServerChannelAdapter adapter) {
      dropSimChannel(adapter.getSimUID(), adapter.getSimChannelUID(), adapter.getMobileGatewayChannelUID(), adapter.getSIMGroupName(), null);
    }

    public void importObject(final SmsPanelTransfer smsPanelTransfer) {
      dropSimChannel(smsPanelTransfer.getIccid(), smsPanelTransfer.getFromData(), smsPanelTransfer.getToDate());
    }

    public void importObject(final CallPathTransfer callPathTransfer) {
      CallPath callPath = callPathTransfer.getCallPath();
      dropSimChannel(callPath.getSimUID(), callPathTransfer.getFromData(), callPathTransfer.getToDate(), callPath.getSimChannelUID(), callPath.getGsmChannelUID(), callPath.getSimGroupName(), null);
    }

  }

  private static final long serialVersionUID = 4360121403949311908L;

  private RefresherThread refresherThread;
  private SimHistoryQueryBox queryBox;
  private final JUpdatableTable<SimEventRecRow, Long> table = new JUpdatableTable<>(new SimHistoryTableBuilder());
  private final DefaultIndicators indicators = new DefaultIndicators();

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void release() {
    table.getUpdatableTableProperties().saveProperties();
    queryBox.release();
  }

  public SimHistoryPanel() {
    createGUIElements();
    setupListeners();

    setTransferHandler(new SimHistoryPanelTransferHandler());
    table.setName(getClass().getSimpleName());
    table.getUpdatableTableProperties().restoreProperties();
  }

  private void createGUIElements() {
    queryBox = new SimHistoryQueryBox();
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, queryBox, new JScrollPane(table));
    JReflectiveBar bar = new JReflectiveBar();
    JExportCsvButton jExportCsvButton = new JExportCsvButton(table, "simHistory");
    jExportCsvButton.setText("Export");
    JButton clearFilterButtons = new JReflectiveButton.JReflectiveButtonBuilder().setText("Clear filters").build();
    clearFilterButtons.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        queryBox.clearFilters();
      }
    });
    JButton clearQueriesBut = new JReflectiveButton.JReflectiveButtonBuilder().setText("Clear queries").build();
    clearQueriesBut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        table.clearData();
        queryBox.clearQueries();
      }
    });
    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(table, "sim history");
    searchField.registerSearchItem(queryBox.table, "sim table");
    searchField.setItemFilterEnabled(queryBox.table, false);
    searchField.rebuildMenu();
    bar.addToLeft(jExportCsvButton);
    bar.addToLeft(clearFilterButtons);
    bar.addToLeft(clearQueriesBut);

    bar.addToRight(searchField);

    JPanel content = new JPanel(new BorderLayout());
    content.add(bar, BorderLayout.NORTH);
    content.add(splitPane, BorderLayout.CENTER);

    setLayout(new BorderLayout());
    add(content, BorderLayout.CENTER);
    add(indicators.getComponent(), BorderLayout.SOUTH);
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refreshSIMGroups();
    }
  }

  private boolean loadAllHistory(final List<SimEventRecRow> rows, final SimEventRecRow row, final SimHistoryQueryBox.QueryInfo queryInfo) {
    if (queryInfo == null) {
      return false;
    }

    WaitingReference<Boolean> ref = RefresherUtils.executeWaitableAction(refresherThread, new CallableRefresher<Boolean>() {

      @Override
      public Boolean performAction(final BeansPool pool) throws Exception {
        return loadHistory(pool, rows, row, queryInfo);
      }
    });

    if (ref.get() == null && ref.getErrorCaught() != null) {
      MessageUtils.showError(SimHistoryPanel.this, "Failed to read SIM history", null, ref.getErrorCaught());
      return false;
    }

    return true;
  }

  private boolean loadHistory(final BeansPool pool, final List<SimEventRecRow> rows, final SimEventRecRow row, final SimHistoryQueryBox.QueryInfo queryInfo) throws Exception {
    SimEventRecRow[] recNodes = loadChilds(pool, row, queryInfo);
    if (recNodes != null) {
      for (SimEventRecRow rec : recNodes) {
        rows.add(rec);
      }
    }

    for (SimEventRecRow rec : rows) {
      if (!loadAllHistory(rows, rec, queryBox.getQueryInfo(rec))) {
        return false;
      }
    }

    return true;
  }

  private void refreshSIMGroups() {
    // Read SIM group names
    ActionCallbackHandler<String[]> handler = new ActionCallbackHandler<String[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        queryBox.setSimGroupNames(null);
        MessageUtils.showError(SimHistoryPanel.this, "Error", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final String[] result) {
        queryBox.setSimGroupNames(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    new ActionRefresher<String[]>("ReadSimGroupsRefresher", refresherThread, handler) {
      @Override
      protected String[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getConfigBean().listSimGroupNames(MainApp.clientUID);
      }
    }.execute();
  }

  private void dropSimChannel(final ICCID simUID, final ChannelUID simChannelUID, final ChannelUID gsmChannelUID, final String simGroupName, final PhoneNumber phoneNumber) {
    dropSimChannel(simUID, getFromDate(), getToDate(), simChannelUID, gsmChannelUID, simGroupName, phoneNumber);
  }
  private void dropSimChannel(final ICCID simUID, final Date fromDate, final Date toDate, final ChannelUID simChannelUID, final ChannelUID gsmChannelUID, final String simGroupName, final PhoneNumber phoneNumber) {
    queryBox.generateQuery(simUID, fromDate, toDate, simChannelUID, gsmChannelUID, simGroupName, phoneNumber, null, null);
  }
  private void dropSimChannel(final ICCID iccid, final Date fromDate, final Date toDate) {
    queryBox.generateQuery(iccid, fromDate, toDate, null, null, null, null, null, null);
  }

  private Date getFromDate() {
    Calendar from = Calendar.getInstance();
    from.set(Calendar.HOUR_OF_DAY, 0);
    from.set(Calendar.MINUTE, 0);
    from.set(Calendar.SECOND, 0);
    from.set(Calendar.MILLISECOND, 0);
    return from.getTime();
  }

  private Date getToDate() {
    Calendar to = Calendar.getInstance();
    to.set(Calendar.HOUR_OF_DAY, 23);
    to.set(Calendar.MINUTE, 59);
    to.set(Calendar.SECOND, 59);
    to.set(Calendar.MILLISECOND, 0);
    return to.getTime();
  }

  @SuppressWarnings("unused")
  private SimEventRecRow[] loadSimEvents(final List<SimEventRecRow> parent) {
    final SimHistoryQueryBox.QueryInfo queryInfo = queryBox.getQueryInfo(parent.get(0));
    if (queryInfo == null) {
      return null;
    }

    final AtomicReference<SimEventRecRow[]> retval = new AtomicReference<>(null);

    ActionCallbackHandler<SimEventRecRow[]> handler = new ActionCallbackHandler<SimEventRecRow[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        synchronized (retval) {
          retval.notifyAll();
        }
        MessageUtils.showError(SimHistoryPanel.this, "Failed to read SIM history", null, caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final SimEventRecRow[] result) {
        synchronized (retval) {
          retval.set(result);
          retval.notifyAll();
        }
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    ActionRefresher<SimEventRecRow[]> refresher = createSimHistoryRefresher(parent, queryInfo, handler);
    refresher.execute();

    synchronized (retval) {
      if (retval.get() == null) {
        try {
          retval.wait();
        } catch (InterruptedException ignored) {
          Thread.currentThread().interrupt();
        }
      }
    }

    return retval.get();
  }

  private void setupListeners() {
    queryBox.addQueryListener(new SimHistoryQueryBox.SimHistoryQueryActionListener() {
      @Override
      public void handleQueryAction(final SimHistoryQueryBox.SimHistoryQuery query, final boolean appendResult) {
        executeQuery(query, appendResult);
      }

      @Override
      public void handleRootHistoryAction(final SimHistoryQueryBox.QueryInfo queryInfo) {
        executeRootHistoryAction(queryInfo.getRows(), queryInfo);
      }

      @Override
      public void showHistory(final List<SimEventRecRow> simEventRecRowList) {
        SimEventRecRow[] simEventRecRows = new SimEventRecRow[simEventRecRowList.size()];
        simEventRecRowList.toArray(simEventRecRows);
        table.setData(simEventRecRows);
      }

    });

    table.addElementSelectionListener(new ElementSelectionListener<SimEventRecRow>() {

      @Override
      public void handleSelectionChanged(final List<SimEventRecRow> currSelection, final List<SimEventRecRow> prevSelection) {
        indicators.setSelected(currSelection.size());
      }

    });

    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent e) {
        indicators.setTotal(table.getModel().getRowCount());
      }
    });

    table.getRowSorter().addRowSorterListener(new RowSorterListener() {

      @Override
      public void sorterChanged(final RowSorterEvent e) {
        indicators.setFiltered(table.getRowSorter().getViewRowCount());

      }

    });
    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(final MouseEvent me) {
        JTable table = (JTable) me.getSource();
        Point p = me.getPoint();
        int row = table.rowAtPoint(p);
        if (me.getClickCount() == 2) {
          if (table.getModel().getRowCount() > 0) {
            JTextArea textArea = new JTextArea();
            textArea.setText(table.getValueAt(row, 3).toString());
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            JScrollPane areaScrollPane = new JScrollPane(textArea);
            areaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            areaScrollPane.setPreferredSize(new Dimension(400, 100));

            areaScrollPane.addHierarchyListener(e -> {
              Window window = SwingUtilities.getWindowAncestor(areaScrollPane);
              if (window instanceof Dialog) {
                Dialog dialog = (Dialog) window;
                if (!dialog.isResizable()) {
                  dialog.setResizable(true);
                }
              }
            });

            JOptionPane.showMessageDialog(getRootPane(), areaScrollPane, "Full description", JOptionPane.PLAIN_MESSAGE);
          }
        }
      }
    });
  }

  private SimEventRecRow[] loadChilds(final BeansPool beansPool, final SimEventRecRow row, final SimHistoryQueryBox.QueryInfo queryInfo) throws Exception {
    SimHistoryQueryBox.SimHistoryQuery query = queryInfo.getQuery();
    SIMEventRec[] records = beansPool.getControlBean().listSIMEvents(query.getSimUID(), query.getFromDate(), query.getToDate(), -1);
    SimEventRecRow[] recNodes = new SimEventRecRow[records.length];
    for (int i = 0; i < records.length; i++) {
      recNodes[i] = new SimEventRecRow(records[i]);
      switch (records[i].getEvent()) {
        case INCOMING_CALL_ENDED:
        case OUTGOING_CALL_ENDED: {
          Long id = Long.valueOf(records[i].getArgs()[0]);
          CDR cdr = beansPool.getControlBean().getCDR(id);
          recNodes[i].setCDR(cdr);
        }
        break;
      }
    }
    return recNodes;
  }

  private ActionRefresher<SimEventRecRow[]> createSimHistoryRefresher(final List<SimEventRecRow> parentNode, final SimHistoryQueryBox.QueryInfo queryInfo,
      final ActionCallbackHandler<SimEventRecRow[]> handler) {

    return new ActionRefresher<SimEventRecRow[]>("SimHistoryRefresher", refresherThread, handler) {
      @Override
      protected SimEventRecRow[] performAction(final BeansPool beansPool) throws Exception {
        SimEventRecRow[] childs = loadChilds(beansPool, null, queryInfo);

        List<SimEventRecRow> simEventRecList = new LinkedList<>();

        // + plus three levels down
        for (SimEventRecRow rec : childs) {
          simEventRecList.add(rec);
          if (rec.getRecord().getEvent().hasChildren()) {
            SimEventRecRow[] subChilds = loadChilds(beansPool, rec, queryInfo);
            for (SimEventRecRow child : subChilds) {
              simEventRecList.add(child);
              SimEventRecRow[] subChilds2 = loadChilds(beansPool, child, queryInfo);
              for (SimEventRecRow child2 : subChilds2) {
                simEventRecList.add(child2);
                SimEventRecRow[] subChilds3 = loadChilds(beansPool, child2, queryInfo);
                for (SimEventRecRow child3 : subChilds3) {
                  simEventRecList.add(child3);
                }
              }
            }
            rec.setExpanded(true);
          }
        }
        SimEventRecRow[] result = new SimEventRecRow[simEventRecList.size()];
        simEventRecList.toArray(result);
        return result;
      }
    };
  }

  private void executeRootHistoryAction(final List<SimEventRecRow> parentNode, final SimHistoryQueryBox.QueryInfo queryInfo) {
    ActionRefresher<SimEventRecRow[]> refresher = createSimHistoryRefresher(parentNode, queryInfo, getRootSimHistoryHandler(queryInfo.getRows()));
    refresher.execute();
  }

  private ActionCallbackHandler<SimEventRecRow[]> getRootSimHistoryHandler(final List<SimEventRecRow> recordRows) {
    return new ActionCallbackHandler<SimEventRecRow[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(SimHistoryPanel.this, "Failed to read SIM history", null, caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final SimEventRecRow[] result) {
        for (SimEventRecRow row : result) {
          recordRows.add(row);
        }

        // floating bug: method setData can produce IllegalArgumentException, so try set data more than once
        for (int i = 0; i < 3; i++) {
          try {
            table.setData(result);
            break;
          } catch (Exception e) {
            if (i == 2) {
              logger.error("[{}] - can't set sim history data", this, e);
            }
          }
        }
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };
  }

  private void executeQuery(final SimHistoryQueryBox.SimHistoryQuery query, final boolean appendResult) {
    ActionRefresher<ICCID[]> refresher = new ActionRefresher<ICCID[]>("SimHistoryQuery", refresherThread, getHandler(query, appendResult)) {
      @Override
      protected ICCID[] performAction(final BeansPool beansPool) throws Exception {
        if (query.getSimUID() != null) {
          return new ICCID[] {query.getSimUID()};
        } else {
          Boolean locked = query.isActive() == null ? null : !query.isActive();

          SimSearchingParams params = new SimSearchingParams();
          params.setEnabled(query.isEnabled());
          params.setLocked(locked);
          params.setPhoneNumber(query.getPhoneNumber());
          params.setSimGroupName(query.getSimGroupName());

          return beansPool.getControlBean().findSIMList(params);
        }
      }
    };
    refresher.execute();
  }

  private ActionCallbackHandler<ICCID[]> getHandler(final SimHistoryQueryBox.SimHistoryQuery query, final boolean appendResult) {
    return new ActionCallbackHandler<ICCID[]>() {

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final ICCID[] uids) {
        List<SimHistoryQueryBox.QueryInfo> infos = new LinkedList<>();
        for (ICCID simUID : uids) {
          SimHistoryQueryBox.SimHistoryQuery q = new SimHistoryQueryBox.SimHistoryQuery(query);
          q.setSimUID(simUID);
          infos.add(new SimHistoryQueryBox.QueryInfo(q));
        }

        if (!appendResult) {
          table.clearData();
        }
        queryBox.addQueryResult(infos, appendResult);
        queryBox.setAllowQuery(true);
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(SimHistoryPanel.this, "Failed to find SIM", null, caught.get(0));
        queryBox.setAllowQuery(true);
      }
    };
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        final JFrame frame = new JFrame();
        SimHistoryPanel contentPane = new SimHistoryPanel();
        contentPane.createGUIElements();

        frame.setContentPane(contentPane);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
      }

    });

  }

}

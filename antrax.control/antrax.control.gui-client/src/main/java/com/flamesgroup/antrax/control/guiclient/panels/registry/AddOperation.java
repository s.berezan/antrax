/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;

class AddOperation implements Operation {

  private final TemporaryRegistryEntry entry;

  public AddOperation(final TemporaryRegistryEntry entry) {
    this.entry = entry;
  }

  @Override
  public void execute(final RegistryAccessBean registryAccess) throws TransactionException {
    registryAccess.add(MainApp.clientUID, entry.getPath(), entry.getValue(), Integer.MAX_VALUE);
  }

  public TemporaryRegistryEntry getEntry() {
    return entry;
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.preprocessors;

import com.flamesgroup.antrax.control.guiclient.panels.AddServerPreprocessingDialog;
import com.flamesgroup.antrax.control.guiclient.panels.EditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.OperationCanceledException;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.storage.commons.IServerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

public class ServerEditingPreprocessor implements EditingPreprocessor<IServerData> {
  private static final Logger logger = LoggerFactory.getLogger(ServerEditingPreprocessor.class);

  private final JUpdatableTable<IServerData, Long> table;

  public ServerEditingPreprocessor(final JUpdatableTable<IServerData, Long> table) {
    this.table = table;
  }

  @Override
  public void beforeUpdate(final IServerData elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
    if (table.getElems().stream().map(IServerData::getName).filter(e -> e.equals(elem.getName())).count() > 1) {
      logger.info("[{}] - contains duplicate: [{}]", this, elem.getName());
      MessageUtils.showWarn(invoker.getParent(), "Warn", "Please use unique name for Server");
      throw new OperationCanceledException();
    }
  }

  @Override
  public IServerData beforeAdd(IServerData server, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {

    AddServerPreprocessingDialog dlg = new AddServerPreprocessingDialog(invoker);
    server = dlg.getServerCreationData(server);
    if (server == null) {
      throw new OperationCanceledException();
    }
    return server;
  }

  @Override
  public void beforeRemove(final IServerData elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {

  }

}

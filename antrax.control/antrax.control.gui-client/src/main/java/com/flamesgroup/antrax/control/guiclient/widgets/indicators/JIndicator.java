/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.indicators;

import java.awt.*;
import java.text.Format;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public abstract class JIndicator<T> {

  private Format valueFormatter;

  private final JPanel bar;
  private final JLabel captionLabel;
  private final JLabel valueLabel;

  private final List<IndicatorVisibleListener> listeners = new LinkedList<>();

  public JIndicator(final T initialValue) {
    this(null, initialValue);
  }

  public JIndicator(final String caption, final T initialValue) {
    captionLabel = new JLabel(caption);
    bar = new JPanel();
    if (caption != null && !caption.isEmpty()) {
      bar.add(captionLabel);
    }
    bar.add(valueLabel = new JLabel());
    setValue(initialValue);
  }

  public JComponent getComponent() {
    return bar;
  }

  protected void updateUI() {
    T value = getValue();
    String labelText = "";
    if (value != null) {
      if (valueFormatter != null) {
        labelText = valueFormatter.format(value);
      } else {
        labelText = String.valueOf(value);
      }
    }
    labelText = beforeRenderValue(labelText);
    valueLabel.setText(labelText);
  }

  protected String beforeRenderValue(final String labelText) {
    return labelText;
  }

  public void setValueFormater(final Format format) {
    valueFormatter = format;
  }

  public void setCaption(final String caption) {
    String prevCaption = captionLabel.getText();
    if (prevCaption == null || prevCaption.isEmpty()) {
      bar.remove(valueLabel);
      bar.add(captionLabel);
      bar.add(valueLabel);
    } else {
      bar.remove(captionLabel);
      bar.add(valueLabel);
    }
    captionLabel.setText(caption);
  }

  public void setIcon(final Icon icon) {
    valueLabel.setIcon(icon);
  }

  public void setColor(final Color color) {
    valueLabel.setForeground(color);
  }

  public void setToolTipText(final String text) {
    valueLabel.setToolTipText(text);
  }

  public void setVisible(final boolean visible) {
    boolean oldValue = isVisible();
    getComponent().setVisible(visible);
    fireVisibleChanged(oldValue, visible);
  }

  public boolean isVisible() {
    return getComponent().isVisible();
  }

  public abstract T getValue();

  public abstract void setValue(T value);

  public void addVisibleListener(final IndicatorVisibleListener l) {
    listeners.add(l);
  }

  public void removeVisibleListener(final IndicatorVisibleListener l) {
    listeners.remove(l);
  }

  private void fireVisibleChanged(final boolean oldValue, final boolean newValue) {
    for (IndicatorVisibleListener l : listeners) {
      l.onVisibleChanged(this, oldValue, newValue);
    }
  }

  public interface IndicatorVisibleListener {
    void onVisibleChanged(JIndicator<?> indicator, boolean oldValue, boolean newValue);
  }

}

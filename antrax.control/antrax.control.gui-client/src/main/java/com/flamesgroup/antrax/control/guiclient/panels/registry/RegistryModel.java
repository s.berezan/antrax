/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

public class RegistryModel {
  private final HashSet<String> paths = new LinkedHashSet<>();

  private final RegistryPathTable pathsList;
  private final RegistryValuesTable valuesList;

  private final List<Operation> outgoingOperations = new LinkedList<>();

  public RegistryModel(final RegistryPathTable pathsList, final RegistryValuesTable valuesList) {
    this.pathsList = pathsList;
    this.valuesList = valuesList;
  }

  public synchronized int addEntry(final String name, final String value) {
    int retval = -1;
    if (paths.add(name)) {
      retval = pathsList.insertElem(name);
    }
    TemporaryRegistryEntry entry = new TemporaryRegistryEntry(name, value, new Date());
    valuesList.insertElem(entry);
    addOperation(new AddOperation(entry));
    return retval;
  }

  public synchronized void moveEntry(final RegistryEntry entry, final String path) {
    if (path.equals(entry.getPath())) {
      return;
    }
    addOperation(new MoveOperation(entry, path));
    valuesList.removeElem(entry);
    if (paths.add(path)) {
      pathsList.insertElem(path);
    }
  }

  public synchronized void setEntries(final RegistryEntry[] entries) {
    valuesList.setData(entries);
  }

  public synchronized void setPaths(final String[] paths) {
    this.paths.clear();
    this.paths.addAll(Arrays.asList(paths));
    pathsList.setData(paths);
  }

  public synchronized void removeAllUnderPath(final String path) {
    addOperation(new RemovePathOperation(path));
    pathsList.removeElem(path);
    valuesList.clearData();
  }

  private void addOperation(final Operation operation) {
    outgoingOperations.add(operation);
  }

  public synchronized void removeValue(final RegistryEntry entry) {
    addOperation(new RemoveEntry(entry));
    valuesList.removeElem(entry);
  }

  public synchronized void applyOutgoingOperations(final RegistryAccessBean registryAccess) throws Exception {
    for (Operation op : outgoingOperations) {
      op.execute(registryAccess);
    }
    outgoingOperations.clear();
  }

  public void showUpdatingMessage() {
    valuesList.clearData();
    valuesList.insertElem(new TemporaryRegistryEntry("", "loading...", new Date(0)));
  }

  public String[] getPaths() {
    return paths.toArray(new String[paths.size()]);
  }
}

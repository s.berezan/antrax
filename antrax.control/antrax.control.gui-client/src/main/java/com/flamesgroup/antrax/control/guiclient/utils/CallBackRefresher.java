/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.rmi.exception.RemoteAccessException;

import java.util.Collections;

public abstract class CallBackRefresher<R> extends Refresher {

  private final ActionCallbackHandler<R> actionHandler;

  public CallBackRefresher(final String name, final ActionCallbackHandler<R> actionHandler) {
    super(name);

    if (actionHandler == null) {
      throw new IllegalStateException("Refresher '" + name + "' action handler is null");
    }

    this.actionHandler = actionHandler;
  }

  @Override
  protected void refreshInformation(final BeansPool beansPool) throws RemoteAccessException {
    Exception caught = null;
    R result = null;

    try {
      result = performAction(beansPool);
    } catch (RemoteAccessException e) {
      if (isConnectionError(e)) {
        throw e;
      } else {
        caught = e;
      }
    } catch (Exception e) {
      caught = e;
    }

    if (actionHandler == null) {
      if (caught != null) {
        throw new RuntimeException(caught);
      }
    } else {
      if (caught == null) {
        actionHandler.onActionRefresherSuccess(this, result);
      } else {
        caught.printStackTrace();
        actionHandler.onActionRefresherFailure(this, Collections.singletonList(caught));
      }
    }
  }

  protected boolean isConnectionError(final Throwable e) {
    Throwable root = e;
    while (root.getCause() != null) {
      root = root.getCause();
    }
    return (root instanceof java.net.SocketException) || (root instanceof java.net.UnknownHostException);

  }

  @Override
  public void updateGuiElements() {
    if (actionHandler != null) {
      actionHandler.onRefreshUI(CallBackRefresher.this);
    }
  }

  protected abstract R performAction(BeansPool beansPool) throws Exception;

}

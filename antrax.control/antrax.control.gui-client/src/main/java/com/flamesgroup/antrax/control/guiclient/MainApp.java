/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.commons.CommonConstants;
import com.flamesgroup.antrax.control.communication.AuthorizationBean;
import com.flamesgroup.antrax.control.guiclient.newapi.MainWindow;
import com.flamesgroup.antrax.control.guiclient.newapi.auth.AuthorizationDialog;
import com.flamesgroup.antrax.control.guiclient.newapi.auth.ConnectionLostDialog;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefreshFailedListener;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManagerImpl;
import com.flamesgroup.rmi.RemoteAccessor;
import com.flamesgroup.rmi.RemoteProxyFactory;
import com.flamesgroup.rmi.exception.RemoteLookupFailureException;
import com.flamesgroup.utils.BuildInfo;
import com.flamesgroup.utils.BuildInfoUtils;

import java.awt.*;
import java.io.InvalidClassException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

public class MainApp {

  public static ClientUID clientUID = new ClientUID();
  public static String userName;

  public static String getAppName() {
    return "Antrax manager";
  }

  private static void applyLookAndFeel() {
    try {
      //      UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
      if (!setMacOSLaF() && !setNimbusLaF()) {
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
      }
      ToolTipManager.sharedInstance().setDismissDelay(100000);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static boolean setMacOSLaF() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
      if ("Mac OS X".equals(info.getName())) {
        UIManager.setLookAndFeel(info.getClassName());
        return true;
      }
    }
    return false;
  }

  private static boolean setNimbusLaF() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
      if ("Nimbus".equals(info.getName())) {
        UIManager.setLookAndFeel(info.getClassName());
        return true;
      }
    }
    return false;
  }

  protected static void startMainLoop() throws Throwable {
    String serverURL = System.getProperty("jnlp.com.flamesgroup.antrax.control.server.url");
    BuildInfo buildInfo = BuildInfoUtils.readBuildInfo(MainApp.class);
    System.out.println(buildInfo);
    AuthorizationBean authorizationBean = connectToAuthorizationBean();
    BeansPool beansPool = new BeansPool(serverURL);
    TransactionManagerImpl transactionManager = new TransactionManagerImpl();

    try {
      while (true) {
        transactionManager.destroy(beansPool.getConfigBean());
        if (!ensureConnectionAndClientVersion(authorizationBean)) {
          break;
        }
        if (!authorize(serverURL, beansPool, authorizationBean)) {
          break;
        }
        RefresherThread refresherThread = new RefresherThread(beansPool, CommonConstants.REFRESH_TIMEOUT);
        final MainWindow mw = new MainWindow(transactionManager);
        mw.setTitle(mw.getTitle() + (buildInfo == null ? "" : String.format(" %s", buildInfo.getProjectVersion())) + " at " + serverURL);
        refresherThread.addRefreshFailedListener(createRefreshFailedListener(mw));
        refresherThread.addRefresher(createSessionCheckRefresher(serverURL, authorizationBean));
        mw.setRefresherThread(refresherThread);
        refresherThread.start();

        transactionManager.postInitialize(refresherThread, beansPool.getConfigBean());

        try {
          showWindowAndWait(mw);
        } catch (Throwable e) {
          StringWriter exceptionStackTraceWriter = new StringWriter();
          e.printStackTrace(new PrintWriter(exceptionStackTraceWriter));
          String exceptionStackTrace = exceptionStackTraceWriter.toString();
          if (!reportError(mw, serverURL, exceptionStackTrace)) {
            JOptionPane.showMessageDialog(null, e.getMessage() + "\n" + exceptionStackTrace, "Antrax running failed", JOptionPane.ERROR_MESSAGE);
          }
          throw e;
        }

        refresherThread.stopRefresher();
        refresherThread.join();
        mw.dispose();

        if (mw.getCaughtError() == null) {
          break;
        }

        if (mw.getCaughtError() instanceof SessionClosedException) {
          break;
        }
        if (isConnectionError(mw.getCaughtError())) {
          continue;
        }

        {
          StringWriter exceptionStackTraceWriter = new StringWriter();
          mw.getCaughtError().printStackTrace(new PrintWriter(exceptionStackTraceWriter));
          String exceptionStackTrace = exceptionStackTraceWriter.toString();
          if (!reportError(mw, serverURL, exceptionStackTrace)) {
            mw.getCaughtError().printStackTrace();
          }
        }
      }
    } finally {
      transactionManager.destroy(beansPool.getConfigBean());
      beansPool.release();
    }
  }

  private static Refresher createSessionCheckRefresher(final String serverURL, final AuthorizationBean authorizationBean) {
    return new Refresher("SessionCheckRefresher") {

      @Override
      public void updateGuiElements() {
      }

      @Override
      protected void refreshInformation(final BeansPool beansPool) throws Exception {
        try {
          beansPool.getControlBean().checkSession();
        } catch (NotPermittedException ignored) {
          if (!authorize(serverURL, beansPool, authorizationBean)) {
            throw new SessionClosedException();
          }

        }
      }

      @Override
      public void handleEnabled(final BeansPool beansPool) {
      }

      @Override
      public void handleDisabled(final BeansPool beansPool) {
      }
    };
  }

  private static boolean authorize(final String serverURL, final BeansPool beansPool, final AuthorizationBean authorizationBean) throws InterruptedException, InvocationTargetException {
    try {
      beansPool.getControlBean().checkSession();
      return true;
    } catch (Throwable ignored) {
      return AuthorizationDialog.authorize(authorizationBean, serverURL);
    }
  }

  private static void showWindowAndWait(final MainWindow mw) throws InterruptedException, InvocationTargetException {
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        mw.setVisible(true);
      }
    });

    while (mw.isVisible()) {
      Thread.sleep(500);
    }
  }

  private static RefreshFailedListener createRefreshFailedListener(final MainWindow mw) {
    return new RefreshFailedListener() {

      @Override
      public void handleRefreshFailed(final Throwable e) {
        if (e instanceof NotPermittedException) {
          return;
        }
        mw.setCaughtError(e);
        mw.setVisible(false);
      }
    };
  }

  private static boolean reportError(final MainWindow mainWindow, final String serverUrl, final String exceptionStackTrace) {
    if (Desktop.isDesktopSupported()) {
      String errorReport = String.format("ClientUID=%s\nServer URL=%s\n%s", MainApp.clientUID, serverUrl, exceptionStackTrace);
      ErrorReportDialog errorReportDialog = new ErrorReportDialog(errorReport);
      errorReportDialog.setLocationRelativeTo(mainWindow);
      errorReportDialog.setVisible(true);
      return errorReportDialog.getResult();
    } else {
      return false;
    }
  }

  private static boolean ensureConnectionAndClientVersion(final AuthorizationBean authorizationBean) throws Throwable {
    while (!checkConnectionAndClientVersion(authorizationBean)) {
      clientUID = new ClientUID();
      if (!ConnectionLostDialog.showDialogFor(5000)) {
        return false;
      }
    }
    return true;
  }

  private static boolean checkConnectionAndClientVersion(final AuthorizationBean authorizationBean) throws Throwable {
    try {
      authorizationBean.getVersionCheckData();
    } catch (Throwable e) {
      if (isConnectionError(e)) {
        return false;
      } else if (isVersionError(e)) {
        if (System.getProperty("debugging") == null) {
          throw new Exception("This program is outdated and is not compatible with server.\nPlease visit the web page of your server to download latest version.");
        }
      } else {
        throw e;
      }
    }
    return true;
  }

  private static boolean isConnectionError(final Throwable e) {
    Throwable root = e;
    while (root != null) {
      if (root instanceof SocketException || root instanceof UnknownHostException || root instanceof SocketTimeoutException || root instanceof RemoteLookupFailureException
          || root instanceof java.net.ConnectException || root instanceof NoRouteToHostException) {
        return true;
      }
      root = root.getCause();
    }

    return false;
  }

  private static boolean isVersionError(Throwable e) {
    while (e != null) {
      if (e instanceof InvalidClassException) {
        return true;
      }
      e = e.getCause();
    }
    return false;
  }

  private static AuthorizationBean connectToAuthorizationBean() {
    String url = System.getProperty("jnlp.com.flamesgroup.antrax.control.server.url") + "/" + CommonConstants.RMI_AUTHORIZATION_BEAN;
    return RemoteProxyFactory.create(new RemoteAccessor.Builder<>(url, AuthorizationBean.class).build());
  }

  public static void main(final String[] args) throws Exception {
    System.setSecurityManager(null);
    java.util.Locale.setDefault(java.util.Locale.US);
    applyLookAndFeel();
    try {
      startMainLoop();
      System.exit(0);
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(1);
    }
  }
}

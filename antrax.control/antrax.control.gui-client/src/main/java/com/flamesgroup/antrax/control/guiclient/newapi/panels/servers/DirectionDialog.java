/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.commons.Direction;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashSet;

import javax.swing.*;

public final class DirectionDialog extends JDialog {

  private static final long serialVersionUID = -1152622309011901093L;

  private final JTextField address;
  private final JEditIntField priority;
  private final JEditIntField capacity;

  private int componentsIndex;
  private boolean approved;
  private final HashSet<String> directions;
  private final JButton cancelButton;
  private final JButton okButton;

  private DirectionDialog(final Window windowAncestor, final Direction editDirection) {
    this(windowAncestor, new HashSet<>());

    address.setText(editDirection.getAddress());
    priority.setValue(editDirection.getPriority());
    capacity.setValue(editDirection.getCapacity());
  }

  private DirectionDialog(final Window windowAncestor, final HashSet<String> directions) {
    super(windowAncestor);
    cancelButton = createCancelBtn();
    okButton = createOkBtn();

    this.directions = directions;

    GridBagLayout layout = new GridBagLayout();
    getContentPane().setLayout(layout);
    layout.rowWeights = new double[] {0, 0, 0, 1};

    add(new JLabel("Address: ", JLabel.RIGHT), address = new JTextField());
    add(new JLabel("Priority: ", JLabel.RIGHT), priority = new JEditIntField(0, 100));
    add(new JLabel("Capacity: ", JLabel.RIGHT), capacity = new JEditIntField(0, 300));
    add(createButtonsPanel(), new GridBagConstraints(0, componentsIndex, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

    setTitle("Direction options");
    setMinimumSize(new Dimension(300, 150));
    setResizable(false);
    pack();

    ActionListener cancelListener = e -> cancelButton.doClick();
    ActionListener okListener = e -> okButton.doClick();

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    address.addActionListener(okListener);
    priority.addActionListener(okListener);
    capacity.addActionListener(okListener);
  }

  private void add(final JLabel lbl, final Component editor) {
    getContentPane().add(lbl, new GridBagConstraints(0, componentsIndex, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    getContentPane().add(editor, new GridBagConstraints(1, componentsIndex, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    componentsIndex++;
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    retval.add(okButton);
    retval.add(cancelButton);
    layout.putConstraint(SpringLayout.EAST, okButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, cancelButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, okButton, 0, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, cancelButton, 0, SpringLayout.SOUTH, retval);

    retval.setPreferredSize(okButton.getPreferredSize());
    return retval;
  }

  private JButton createCancelBtn() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(e -> setVisible(false));
    return retval;
  }

  private JButton createOkBtn() {
    final JButton retval = new JButton("OK");
    retval.addActionListener(e -> {
      try {
        if (address.getText().isEmpty()) {
          MessageUtils.showError(retval, "Input is not valid", "Input is not full, please fill all fields");
        } else if (directions.contains(address.getText())) {
          MessageUtils.showError(retval, "Duplicate direction address", "This name is already used");
        } else {
          approved = true;
          setVisible(false);
        }
      } catch (Exception exc) {
        MessageUtils.showError(retval, "Failed to apply value", exc.getMessage(), exc);
      }
    });
    return retval;
  }

  private Direction getDirection() {
    if (approved) {
      return new Direction(address.getText(), priority.getIntValue(), capacity.getIntValue());
    } else {
      return null;
    }
  }

  private static Direction getDirection(final DirectionDialog dialog) {
    dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setLocationRelativeTo(dialog.getOwner());
    dialog.setVisible(true);
    dialog.dispose();

    return dialog.getDirection();
  }

  public static Direction createNewDirection(final Component parent, final HashSet<String> directions) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getDirection(new DirectionDialog(windowAncestor, directions));
  }

  public static Direction createEditDirection(final Component parent, final Direction direction) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getDirection(new DirectionDialog(windowAncestor, direction));
  }

}

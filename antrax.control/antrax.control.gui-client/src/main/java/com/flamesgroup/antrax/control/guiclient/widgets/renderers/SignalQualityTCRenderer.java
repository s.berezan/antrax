/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.control.guiclient.domain.SignalQuality;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.commons.OperatorSelectionMode;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus;

import java.awt.*;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Signal quality table cell renderer.
 */
public class SignalQualityTCRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 8241537131801221136L;

  private static final int INTERVALS_COUNT = 5;

  private final Map<Integer, Icon> iconCache = new HashMap<>();

  public SignalQualityTCRenderer() {
    super();
    setHorizontalAlignment(SwingConstants.CENTER);
  }

  /**
   * @param dBm signal strength in dBm
   * @return interval number in range [1; INTERVALS_COUNT]
   */
  private int getIntervalNumber(final int dBm) {
    int intervalVal = (SignalQuality.MAX_SIGNAL_STRENGTH - SignalQuality.MIN_SIGNAL_STRENGTH) / (INTERVALS_COUNT - 1);
    double intervalNum = (double) (dBm - SignalQuality.MIN_SIGNAL_STRENGTH) / intervalVal;
    int result = new BigDecimal(intervalNum).setScale(0, BigDecimal.ROUND_CEILING).intValue();
    if (result > INTERVALS_COUNT) {
      result = INTERVALS_COUNT;
    }
    return result;
  }

  private Icon getIcon(final int interval) {
    Icon icon = iconCache.get(interval);
    if (icon == null) {
      String iconPath = MessageFormat.format("/img/signal/signal_strength_{0}.png", String.valueOf(interval));
      icon = IconPool.getShared(iconPath);
      iconCache.put(interval, icon);
    }
    return icon;
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    if (value instanceof Pair<?, ?>) {
      SignalQuality signalQuality = (SignalQuality) ((Pair<?, ?>) value).first();
      GSMNetworkInfo gsmNetworkInfo = (GSMNetworkInfo) ((Pair<?, ?>) value).second();
      int iconIndex;
      StringBuilder dBmStr = new StringBuilder();
      StringBuilder berStr = new StringBuilder();
      if (signalQuality.isSignalStrengthDetectable()) {
        int dBm = signalQuality.getSignalStrength();
        iconIndex = (dBm <= SignalQuality.MIN_SIGNAL_STRENGTH) ? 0 : getIntervalNumber(dBm);
        if (dBm <= SignalQuality.MIN_SIGNAL_STRENGTH) {
          dBmStr.append("≤ ");
        } else if (dBm >= SignalQuality.MAX_SIGNAL_STRENGTH) {
          dBmStr.append("≥ ");
        }
        dBmStr.append(String.valueOf(dBm)).append(" dBm");
      } else {
        iconIndex = 0;
        dBmStr.append("<b>?</b>");
      }

      if (signalQuality.isBERDetectable()) {
        if (signalQuality.getLowBERPercent() == 0) {
          berStr.append(MessageFormat.format("&lt; {0} %", signalQuality.getHighBERPercent()));
        } else {
          berStr.append(MessageFormat.format("{0}-{1} %", signalQuality.getLowBERPercent(), signalQuality.getHighBERPercent()));
        }
      } else {
        berStr.append("<b>?</b>");
      }

      setIcon(getIcon(iconIndex));
      setToolTipText("<html><sup><strong>" + dBmStr + ", " + berStr + "</strong><br>" + gsmNetworkInfoHtmlFormat(gsmNetworkInfo) + "</sup></html>");
      setText("");
    } else if (value == null) {
      setIcon(null);
      setToolTipText(null);
      setText("");
    } else {
      setText(value.toString());
      setIcon(null);
      setToolTipText(null);
    }

    return cmp;
  }

  private String gsmNetworkInfoHtmlFormat(final GSMNetworkInfo gsmNetworkInfo) {
    if (gsmNetworkInfo == null) {
      return "";
    }
    StringBuilder neighbourCellsStr = new StringBuilder();
    neighbourCellsStr.append("Reg mode: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(convertOperatorSelectionMode(gsmNetworkInfo.getOperatorSelectionMode()));
    neighbourCellsStr.append("</i></b><br>");
    neighbourCellsStr.append("Reg status: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(gsmNetworkInfo.getRegStatus()); //TODO: display string description of status
    neighbourCellsStr.append("</i></b>");

    if (NetworkRegistrationStatus.REGISTERED_TO_HOME_NETWORK == gsmNetworkInfo.getRegStatus() ||
        NetworkRegistrationStatus.REGISTERED_ROAMING == gsmNetworkInfo.getRegStatus()) {
      neighbourCellsStr.append("<br>");
      neighbourCellsStr.append("Operator: ");
      neighbourCellsStr.append("<b><i>");
      if (gsmNetworkInfo.getOperator() == null) {
        neighbourCellsStr.append("unknown");
      } else {
        neighbourCellsStr.append(gsmNetworkInfo.getOperator());
      }
      neighbourCellsStr.append("</i></b><br>");

      CellInfo servingCell = gsmNetworkInfo.getCellInfo()[0];
      CellInfo[] neighbourCells = new CellInfo[gsmNetworkInfo.getCellInfo().length - 1];
      System.arraycopy(gsmNetworkInfo.getCellInfo(), 1, neighbourCells, 0, neighbourCells.length);

      neighbourCellsStr.append("<u>");
      appendStringBuilderWithGSMInfo(neighbourCellsStr, servingCell);
      neighbourCellsStr.append("</u>");
      for (CellInfo cell : neighbourCells) {
        neighbourCellsStr.append("<br>");
        appendStringBuilderWithGSMInfo(neighbourCellsStr, cell);
      }
    }

    return neighbourCellsStr.toString();
  }

  private void appendStringBuilderWithGSMInfo(final StringBuilder neighbourCellsStr, final CellInfo servingCell) {
    neighbourCellsStr.append(" LAC: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(servingCell.getLac());
    neighbourCellsStr.append("</i></b>");
    neighbourCellsStr.append(" cellID: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(servingCell.getCellId());
    neighbourCellsStr.append("</i></b>");
    neighbourCellsStr.append(" BSIC: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(servingCell.getBsic());
    neighbourCellsStr.append("</i></b>");
    neighbourCellsStr.append(" ARFCN: ");
    neighbourCellsStr.append("<b><i>");
    neighbourCellsStr.append(servingCell.getArfcn());
    neighbourCellsStr.append("</i></b>");
  }

  private String convertOperatorSelectionMode(final OperatorSelectionMode operatorSelectionMode) {
    switch (operatorSelectionMode) {
      case AUTOMATIC:
        return "automatic";
      case MANUAL:
        return "manual";
      default:
        return "";
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

public class RegistryAccessAdapter implements RegistryAccess {

  private final RegistryAccessBean bean;
  private final ClientUID clientUID;

  public RegistryAccessAdapter(final RegistryAccessBean bean, final ClientUID clientUID) {
    this.bean = bean;
    this.clientUID = clientUID;
  }

  @Override
  public void add(final String path, final String value, final int maxSize) {
    try {
      bean.add(clientUID, path, value, maxSize);
    } catch (TransactionException ignored) {
    }
  }

  @Override
  public String[] listAllPaths() {
    try {
      return bean.listAllPaths(clientUID);
    } catch (TransactionException ignored) {
      return new String[0];
    }
  }

  @Override
  public RegistryEntry[] listEntries(final String path, final int maxSize) {
    try {
      return bean.listEntries(clientUID, path, maxSize);
    } catch (Exception ignored) {
      return new RegistryEntry[0];
    }
  }

  @Override
  public RegistryEntry move(final RegistryEntry entry, final String path) throws UnsyncRegistryEntryException, DataModificationException {
    try {
      return bean.move(clientUID, entry, path);
    } catch (Exception ignored) {
      return entry;
    }
  }

  @Override
  public void remove(final RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException {
    try {
      bean.remove(clientUID, entry);
    } catch (Exception ignored) {

    }
  }

  @Override
  public void removePath(final String path) {
    try {
      bean.removePath(clientUID, path);
    } catch (Exception ignored) {

    }
  }

}

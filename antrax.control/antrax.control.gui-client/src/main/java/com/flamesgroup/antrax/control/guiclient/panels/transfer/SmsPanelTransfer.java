/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.transfer;

import com.flamesgroup.unit.ICCID;

import java.io.Serializable;
import java.util.Date;

public class SmsPanelTransfer implements Serializable {

  private static final long serialVersionUID = -8089483835395622875L;

  private final ICCID iccid;
  private final Date fromData;
  private final Date toDate;

  public SmsPanelTransfer(final ICCID iccid, final Date fromData, final Date toDate) {
    this.iccid = iccid;
    this.fromData = fromData;
    this.toDate = toDate;
  }

  public ICCID getIccid() {
    return iccid;
  }

  public Date getFromData() {
    return fromData;
  }

  public Date getToDate() {
    return toDate;
  }

}

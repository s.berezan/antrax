/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.SimChannelInformation;
import com.flamesgroup.antrax.control.guiclient.domain.SignalQuality;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

public class VoiceServerChannelAdapter implements Comparable<VoiceServerChannelAdapter> {
  private final IServerData voiceServer;
  private final IVoiceServerChannelsInfo delegate;
  private final SignalQuality signalQuality;
  private final ICCID simUID;
  private final GSMNetworkInfo gsmNetworkInfo;

  public VoiceServerChannelAdapter(final IServerData voiceServer, final IVoiceServerChannelsInfo info) {
    super();
    this.voiceServer = voiceServer;
    this.delegate = info;

    MobileGatewayChannelInformation mi = delegate.getMobileGatewayChannelInfo();
    this.signalQuality = (mi == null) ? null : new SignalQuality(mi.getSignalStrength() == 0 ? SignalQuality.MIN_SIGNAL_STRENGTH : mi.getSignalStrength(), mi.getBitErrorRate());
    this.gsmNetworkInfo = (mi == null) ? null : mi.getGsmNetworkInfo();

    SimChannelInformation simChaninfo = delegate.getSimChannelInfo();
    this.simUID = (simChaninfo == null) ? null : simChaninfo.getSimUID();
  }

  public GSMNetworkInfo getGSMNetworkInfo() {
    return gsmNetworkInfo;
  }

  public ICCID getSimUID() {
    return simUID;
  }

  public IServerData getVoiceServer() {
    return voiceServer;
  }

  public String getStateAdvInfo() {
    return delegate.getStateAdvInfo();
  }

  public ChannelUID getMobileGatewayChannelUID() {
    MobileGatewayChannelInformation info = delegate.getMobileGatewayChannelInfo();
    return (info == null) ? null : info.getGSMChannelUID();
  }

  public Boolean isEnabled() {
    SimChannelInformation info = delegate.getSimChannelInfo();
    return (info == null) ? null : info.isEnabled();
  }

  public ChannelUID getSimChannelUID() {
    SimChannelInformation info = delegate.getSimChannelInfo();
    return (info == null) ? null : info.getSimChannelUID();
  }

  public String getLastUSSD() {
    SimChannelInformation info = delegate.getSimChannelInfo();
    return (info == null) ? "" : info.getLastUSSDResponse();
  }

  public CallChannelState getCallChannelState() {
    return delegate.getChannelState();
  }

  public CallState getCallState() {
    return delegate.getCallState();
  }

  public long getLastUSSDTimeout() {
    SimChannelInformation info = delegate.getSimChannelInfo();
    return (info == null) ? 0L : info.getLastUSSDTimeout();
  }

  public int getSuccessfulCallsCount() {
    return delegate.getSuccessfulCallsCount();
  }

  public int getTotalCallsCount() {
    return delegate.getTotalCallsCount();
  }

  public int getSuccessOutgoingSmsCount() {
    return delegate.getSuccessOutgoingSmsCount();
  }

  public int getTotalOutgoingSmsCount() {
    return delegate.getTotalOutgoingSmsCount();
  }

  public String getSIMGroupName() {
    SimChannelInformation info = delegate.getSimChannelInfo();
    return info == null ? null : info.getGroup();
  }

  public String getGSMGroupName() {
    GSMGroup gsmGroup = delegate.getMobileGatewayChannelInfo().getGSMChannel().getGSMGroup();
    if (gsmGroup instanceof GsmNoGroup) {
      return null;
    } else {
      return gsmGroup.getName();
    }
  }

  public GSMGroup getGSMGroup() {
    return delegate.getMobileGatewayChannelInfo().getGSMChannel().getGSMGroup();
  }

  public SignalQuality getSignalQuality() {
    return signalQuality;
  }

  public long getCallsDuration() {
    return delegate.getCallsDuration();
  }

  @Override
  public int compareTo(final VoiceServerChannelAdapter o) {
    return getMobileGatewayChannelUID().compareTo(o.getMobileGatewayChannelUID());
  }

  public PhoneNumber getPhoneNumber() {
    return (getCallState() == null) ? null : getCallState().getPhoneNumber();
  }

  private boolean equalsObjects(final Object o1, final Object o2) {
    return (o1 == null) ? (o2 == null) : o1.equals(o2);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj instanceof VoiceServerChannelAdapter) {
      VoiceServerChannelAdapter other = (VoiceServerChannelAdapter) obj;
      return equalsObjects(this.voiceServer.getName(), other.voiceServer.getName()) && equalsObjects(this.getMobileGatewayChannelUID(), other.getMobileGatewayChannelUID()) && equalsObjects(
          this.getSimChannelUID(), other.getSimChannelUID());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return 19 + delegate.hashCode();
  }

  @Override
  public String toString() {
    return String.format("%s <B-%s : C-%s>", voiceServer.getName(), getMobileGatewayChannelUID(), getSimChannelUID());
  }

  public String getID() {
    return toString();
  }

  public boolean isReady() {
    CallChannelState callChannelState = getCallChannelState();
    return (callChannelState != null) && callChannelState.getState() == CallChannelState.State.READY_TO_CALL;
  }

  public boolean isCalling() {
    CallChannelState callChannelState = getCallChannelState();
    return (callChannelState != null) && callChannelState.getState() == CallChannelState.State.OUTGOING_CALL;
  }

  public boolean isLive() {
    return delegate.getMobileGatewayChannelInfo().isLive();
  }

  public ChannelConfig getChannelConfig() {
    return delegate.getMobileGatewayChannelInfo().getChannelConfig();
  }

  public GSMChannel getGSMChannel() {
    return delegate.getMobileGatewayChannelInfo().getGSMChannel();
  }

  public long getServerTimeMillis() {
    return delegate.getServerTimeMillis();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.control.guiclient.scripts.panels.ScriptCommonsCreateDialog;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableCellEditor;

public class ScriptCommonsCellEditor extends AbstractCellEditor implements TableCellEditor {

  private static final long serialVersionUID = 4873569300243447101L;

  private ScriptCommonsSource scriptCommonsSource;

  private final JComboBox combobox = new JComboBox();

  private ScriptCommons currentValue;

  private String type;

  private PropertyEditorsSource propertyEditorsSource;

  private ScriptParameter currentParam;

  private AntraxPluginsStore pluginsStore;

  private JTable table;

  public ScriptCommonsCellEditor() {
    combobox.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        switch (combobox.getSelectedIndex()) {
          case 0:
            directValue();
            fireEditingStopped();
            break;
          case 1:
            addNewReference();
            break;
          default:
            currentValue = scriptCommonsSource.getCommonsByName((String) combobox.getSelectedItem());
            fireEditingStopped();
        }
      }

      private void addNewReference() {
        Object value;
        if (currentParam.getValue(pluginsStore.getClassLoader()) instanceof SharedReference) {
          value = currentValue.getValue(pluginsStore.getClassLoader());
        } else {
          value = currentParam.getValue(pluginsStore.getClassLoader());
        }
        ScriptCommons common = ScriptCommonsCreateDialog.askForCommon(table, scriptCommonsSource, propertyEditorsSource.getEditorFor(value, type));
        if (common != null) {
          scriptCommonsSource.addScriptCommon(common);
          currentValue = common;
          fireEditingStopped();

        } else {
          fireEditingCanceled();
        }
      }

      private void directValue() {
        currentValue = null;
      }
    });
  }

  public void setScriptCommonsSource(final ScriptCommonsSource source) {
    assert source != null;
    this.scriptCommonsSource = source;
  }

  public void setPropertyEditorsSource(final PropertyEditorsSource source) {
    assert source != null;
    this.propertyEditorsSource = source;
  }

  public void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
    this.pluginsStore = pluginsStore;
  }

  public void setType(final String type) {
    this.type = type;
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    model.addElement("-");
    model.addElement("<new>");
    for (ScriptCommons sc : scriptCommonsSource.listAcceptableCommons(type)) {
      model.addElement(sc.getName());
    }
    combobox.setModel(model);
  }

  @Override
  public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
    this.table = table;
    currentParam = ((ScriptParamsTable) table).getElemAt(row).getParam();
    currentValue = (ScriptCommons) value;
    if (currentValue != null) {
      combobox.setSelectedItem(currentValue.getName());
    } else {
      combobox.setSelectedIndex(-1);
    }
    return combobox;
  }

  @Override
  public Object getCellEditorValue() {
    return currentValue;
  }

}

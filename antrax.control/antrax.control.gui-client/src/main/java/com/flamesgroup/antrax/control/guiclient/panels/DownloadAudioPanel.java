/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.panels.audiolist.DownloadAudioRefresher;
import com.flamesgroup.antrax.control.guiclient.panels.audiolist.DownloadAudioServerTable;
import com.flamesgroup.antrax.control.guiclient.panels.audiolist.DownloadAudioTable;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.storage.commons.IServerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class DownloadAudioPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = -1499000675681762097L;

  private final Logger logger = LoggerFactory.getLogger(DownloadAudioPanel.class);

  private final DownloadAudioTable downloadAudioTable = new DownloadAudioTable();
  private final DownloadAudioServerTable downloadAudioServerTable = new DownloadAudioServerTable();
  private RefresherThread refresherThread;
  private DownloadAudioRefresher refresher;

  public DownloadAudioPanel() {

    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(downloadAudioServerTable), new JScrollPane(downloadAudioTable));
    splitPane.setDividerLocation(170);

    setLayout(new BorderLayout());

    add(splitPane, BorderLayout.CENTER);
    add(createToolbar(), BorderLayout.PAGE_START);

    initializeListeners();
    downloadAudioTable.setName(getClass().getSimpleName() + "_downloadAudioTable");
    downloadAudioServerTable.setName(getClass().getSimpleName() + "_downloadAudioServerTable");
    downloadAudioTable.getUpdatableTableProperties().restoreProperties();
    downloadAudioServerTable.getUpdatableTableProperties().restoreProperties();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {

  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
    this.refresher = new DownloadAudioRefresher(downloadAudioServerTable, downloadAudioTable,refresherThread, transactionManager, this);
    this.refresher.init();
  }


  @Override
  public void release() {
    downloadAudioTable.getUpdatableTableProperties().saveProperties();
    downloadAudioServerTable.getUpdatableTableProperties().saveProperties();
    if (refresher != null) {
      this.refresher.interrupt();
    }
  }

  private void initializeListeners() {
    logger.debug("[{}] - setup listeners", this);
    downloadAudioServerTable.getSelectionModel().addListSelectionListener(e -> {
      if (refresher.isUpdating()) {
        return;
      }
      if (e.getValueIsAdjusting()) {
        return;
      }
      downloadAudioTable.setEnabled(false);

      refresherThread.forceRefresh();
    });
  }

  private Component createToolbar() {
    JContextSearch cs = new JContextSearch();
    cs.registerSearchItem(downloadAudioTable, "downloadAudioTable");
    cs.registerSearchItem(downloadAudioServerTable, "downloadAudioServerTable");

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(createDownloadButton());
    bar.addToRight(cs);
    return bar;
  }

  private JComponent createDownloadButton() {
    final JButton retval = createToolbarButton(IconPool.getShared("/img/down.png"));
    retval.setToolTipText("Download selected file(s)");

    retval.addActionListener(e -> {
      IServerData selectedServer = downloadAudioServerTable.getSelectedElem();
      java.util.List<AudioFile> selectedAudioFile = downloadAudioTable.getSelectedElems();
      if (selectedAudioFile.isEmpty()) {
        MessageUtils.showInfo(this, "Info", "Please select at least one file");
        return;
      }
      JFileChooser fileChooser = new JFileChooser();
      FileNameExtensionFilter filter = new FileNameExtensionFilter("ZIP archive", "zip");
      fileChooser.setFileFilter(filter);
      fileChooser.setSelectedFile(new File("AudioFile_from_" + selectedServer.getName() + ".zip"));
      File selectedFile = null;
      if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
        selectedFile = fileChooser.getSelectedFile();
      }
      if (selectedFile != null) {
        refresher.downloadAudioFiles(selectedFile, selectedServer, selectedAudioFile);
      }
    });
    return retval;
  }

  private JButton createToolbarButton(final Icon icon) {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
    retval.setFocusable(false);
    return retval;
  }

}

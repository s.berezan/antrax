/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class SimGroupReportTableTooltips {

  private SimGroupReportTableTooltips() {
  }

  public static String getGroupTooltip() {
    return new TooltipHeaderBuilder("SIM group name").build();
  }

  public static String getTotalTooltip() {
    return new TooltipHeaderBuilder("Count of SIM cards in the group").build();
  }

  public static String getLockedTooltip() {
    return new TooltipHeaderBuilder("Count of locked SIM cards in the group").build();
  }

  public static String getUnlockedTooltip() {
    return new TooltipHeaderBuilder("Count of unlocked SIM cards in the group").build();
  }

  public static String getTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total calls of SIM cards in the group").build();
  }

  public static String getSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful calls of SIM cards in the group").build();
  }

  public static String getCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration of SIM cards in the group").build();
  }

  public static String getAcdTooltip() {
    return new TooltipHeaderBuilder("Average call duration of SIM cards in the group").build();
  }

  public static String getIncomingTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total incoming calls of SIM cards in the group").build();
  }

  public static String getIncomingSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful incoming calls of SIM cards in the group").build();
  }

  public static String getSuccessSmsTooltip() {
    return new TooltipHeaderBuilder("Successful SMS count of SIM cards in the group").build();
  }

  public static String getTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total SMS count of SIM cards in the group").build();
  }

  public static String getIncomingTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total incoming SMS count of SIM cards in the group").build();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.commons.TimePeriodWriter;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.commons.Pair;

import java.util.Comparator;

public class CallChannelStateTimeComparator implements Comparator<Pair<CallChannelState, Long>> {

  @Override
  public int compare(final Pair<CallChannelState, Long> o1, final Pair<CallChannelState, Long> o2) {
    if (o1.first() == null) {
      return o2.first() == null ? 0 : 1;
    } else if (o2.first() == null) {
      return -1;
    } else {
      long dif = (calculateStateTime(o1) - calculateStateTime(o2));
      return (dif > 0) ? +1 : ((dif < 0) ? -1 : 0);
    }
  }

  private long calculateStateTime(final Pair<CallChannelState, Long> state) {
    return (Long) state.first().writeTime(new TimePeriodWriter() {

      @Override
      public Object writeLeftTime(final long time) {
        return -time;
      }

      @Override
      public Object writePassedTime(final long time) {
        return time;
      }

    }, state.second());
  }

}

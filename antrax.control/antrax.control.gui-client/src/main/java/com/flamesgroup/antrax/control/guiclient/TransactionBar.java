/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public final class TransactionBar extends JReflectiveBar implements TransactionCallbackHandler, TransactionListener {

  private static final long serialVersionUID = 5075882030065572085L;

  private final TransactionManager transactionManager;
  private final Frame rootFrame;

  private final JButton buttonStart;
  private final JButton buttonCommit;
  private final JButton buttonRollback;

  private JDialog waitingDialog;

  public TransactionBar(final TransactionManager transactionManager, final Frame rootFrame) {
    this.transactionManager = transactionManager;
    this.rootFrame = rootFrame;
    int padding = 2;
    buttonStart = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.png")).setToolTipText("Edit config").setPadding(padding)
        .addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            execTransactionStage(TransactionState.START);
          }
        }).build();

    buttonCommit = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/apply.png")).setToolTipText("Save config").setPadding(padding)
        .addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            execTransactionStage(TransactionState.COMMIT);
          }
        }).build();

    buttonRollback = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-undo.png")).setToolTipText("Cancel edit").setPadding(padding)
        .addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            execTransactionStage(TransactionState.ROLLBACK);
          }
        }).build();

    enableButtons(TransactionState.COMMIT);
    addToLeft(buttonStart);
    addToLeft(buttonCommit);
    addToLeft(buttonRollback);
    transactionManager.addListener(this);
  }

  public AbstractButton getStartButton() {
    return buttonStart;
  }

  public AbstractButton getCommitButton() {
    return buttonCommit;
  }

  public AbstractButton getRollbackButton() {
    return buttonRollback;
  }

  private void enableButtons(final TransactionState stage) {
    boolean started = false;
    switch (stage) {
      case START:
        started = true;
        break;

      case COMMIT:
      case ROLLBACK:
        started = false;
        break;
    }
    buttonStart.setEnabled(!started);
    buttonCommit.setEnabled(started);
    buttonRollback.setEnabled(started);
  }

  private void enableButtons(final boolean enable) {
    buttonStart.setEnabled(enable);
    buttonCommit.setEnabled(enable);
    buttonRollback.setEnabled(enable);
  }

  private void execTransactionStage(final TransactionState stage) {
    enableButtons(false);
    switch (stage) {
      case START:
        transactionManager.startEditTransaction(this);
        break;

      case COMMIT:
        transactionManager.commitEditTransaction(this);
        waitingDialog = createWaitingDialog();
        waitingDialog.setVisible(true);
        break;

      case ROLLBACK:
        transactionManager.rollbackEditTransaction(this);
        break;
    }
  }

  @Override
  public void onTransactionActionFailure(final TransactionState stage, final List<Throwable> caught) {
    if (waitingDialog != null) {
      waitingDialog.dispose();
      waitingDialog = null;
    }
    StringBuilder msg = new StringBuilder();
    for (Throwable e : caught) {
      msg.append(e.getLocalizedMessage()).append(System.lineSeparator());
    }
    MessageUtils.showError(buttonStart.getRootPane(), "Transaction error", msg.toString());
    enableButtons(TransactionState.ROLLBACK);
  }

  @Override
  public void onTransactionActionSuccess(final TransactionState stage) {
    if (waitingDialog != null) {
      waitingDialog.dispose();
      waitingDialog = null;
    }
    enableButtons(stage);
  }

  @Override
  public void onTransactionStateChanged(final TransactionState newState) {
    enableButtons(newState);
  }

  @Override
  public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
    return Collections.emptyList();
  }

  private JDialog createWaitingDialog() {
    JDialog dialogLocal = new JDialog(rootFrame, true);
    JPanel panelOption = new JPanel();
    panelOption.setBorder(new EmptyBorder(5, 5, 5, 5));
    panelOption.setLayout(new BoxLayout(panelOption, BoxLayout.Y_AXIS));
    JLabel jLabel = new JLabel("Please waiting. Saving changes.");
    panelOption.add(jLabel);
    JProgressBar progressBar = new JProgressBar(0, 100);
    progressBar.setIndeterminate(true);
    panelOption.add(progressBar);
    dialogLocal.add(panelOption);
    dialogLocal.setResizable(false);
    dialogLocal.setLocationRelativeTo(rootFrame);
    dialogLocal.setAlwaysOnTop(true);
    dialogLocal.pack();

    dialogLocal.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    return dialogLocal;
  }
}

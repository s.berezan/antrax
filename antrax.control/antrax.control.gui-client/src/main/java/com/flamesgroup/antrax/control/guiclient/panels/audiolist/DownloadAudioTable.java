/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.audiolist;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.AudioFileTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

public class DownloadAudioTable extends JUpdatableTable<AudioFile, String> {

  private static final long serialVersionUID = -8621675776929229714L;

  private final String[] columnToolTips = {
      AudioFileTableTooltips.getAudioFileTooltip(),
      AudioFileTableTooltips.getCreateTimeTooltip(),
      AudioFileTableTooltips.getSizeTooltip()
  };

  public DownloadAudioTable() {
    super(new TableBuilder<AudioFile, String>() {

      private final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("audio file", String.class);
        columns.addColumn("create time", String.class);
        columns.addColumn("size in Mbyte", String.class);
      }

      @Override
      public void buildRow(final AudioFile src, final ColumnWriter<AudioFile> dest) {
        dest.writeColumn(src.getName());
        dest.writeColumn(calculateTime(src.getCreateDate()));
        dest.writeColumn(new DecimalFormat("#.###").format(src.getSize()/(double)1024/1024));
      }

      @Override
      public String getUniqueKey(final AudioFile src) {
        return src.getName();
      }

      private String calculateTime(final long time) {
        if (time == 0) {
          return " - no - ";
        } else {
          return formatter.format(time);
        }
      }

    });
    setColumnToolTips(columnToolTips);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class SendUSSDDialog extends JDialog {

  private static final long serialVersionUID = -4758733957746906568L;

  private final JPanel panelResponse = new JPanel();
  private final JTextArea responseTextArea = new JTextArea();

  private final JTextField txtUSSD = new JTextField(25);

  private final JCheckBox responseCheckBox = new JCheckBox();

  private final JButton btnOk = new JButton("Send");
  private final JButton btnCancel = new JButton("Cancel");

  private boolean applied = false;

  private final java.util.List<CloseDialogListener> closeDialogListeners = new ArrayList<>();


  public JButton getOkBtn() {
    return btnOk;
  }

  public void addCloseDialogListener(final CloseDialogListener closeDialogListener) {
    closeDialogListeners.add(closeDialogListener);
  }

  public SendUSSDDialog(final Window window) {
    super(window);
    setLayout(new BoxLayout(getContentPane(), 3));
    setContentPane(createPanel());
    initializeEvents();
    pack();
    setLocationRelativeTo(getOwner());
    setResizable(false);
  }

  private JPanel createPanel() {
    JPanel retval = new JPanel();
    resizeToWider(btnOk, btnCancel, btnOk.getPreferredSize().height);
    btnOk.setEnabled(false);

    JLabel labelResponse = new JLabel();
    JScrollPane scrollPaneResponse = new JScrollPane();
    JPanel panelUSSD = new JPanel();
    JLabel labelUSSD = new JLabel();
    JPanel panelCheckBox = new JPanel();
    JPanel panelControl = new JPanel();

    //======== retval ========
    retval.setBorder(new EmptyBorder(5, 5, 10, 5));
    retval.setLayout(new BoxLayout(retval, BoxLayout.Y_AXIS));

    //======== panelResponse ========
    {
      panelResponse.setBorder(new EmptyBorder(5, 5, 5, 5));
      panelResponse.setLayout(new BorderLayout());

      //---- labelResponse ----
      labelResponse.setText("Response:");
      labelResponse.setHorizontalAlignment(SwingConstants.LEFT);
      panelResponse.add(labelResponse, BorderLayout.NORTH);

      //======== scrollPaneResponse ========
      {
        //---- textAreaResponse ----
        responseTextArea.setRows(10);
        responseTextArea.setColumns(25);
        responseTextArea.setEditable(false);
        responseTextArea.setLineWrap(true);
        responseTextArea.setWrapStyleWord(true);
        scrollPaneResponse.setViewportView(responseTextArea);
      }
      panelResponse.add(scrollPaneResponse, BorderLayout.CENTER);
      panelResponse.setVisible(false);
    }
    retval.add(panelResponse);

    //======== panelUSSD ========
    {
      panelUSSD.setBorder(new EmptyBorder(5, 5, 5, 5));
      panelUSSD.setLayout(new BorderLayout());

      //---- labelUSSD ----
      labelUSSD.setText("USSD: ");
      panelUSSD.add(labelUSSD, BorderLayout.NORTH);
      panelUSSD.add(txtUSSD, BorderLayout.CENTER);
    }
    retval.add(panelUSSD);

    //======== panelCheckBox ========
    {
      panelCheckBox.setBorder(null);
      panelCheckBox.setLayout(new FlowLayout(FlowLayout.LEFT));

      //---- checkBox1 ----
      responseCheckBox.setText("Get response");
      responseCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
      panelCheckBox.add(responseCheckBox);
    }
    retval.add(panelCheckBox);

    //======== panelControl ========
    {
      panelControl.setLayout(new BoxLayout(panelControl, BoxLayout.X_AXIS));

      //---- btnOk ----
      panelControl.add(btnOk);
      //---- btnCancel ----
      panelControl.add(btnCancel);
    }
    retval.add(panelControl);

    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    retval.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  private void initializeEvents() {
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        applied = false;
        txtUSSD.requestFocus();
      }
    });

    DocumentListener documentListener = new DocumentListener() {
      private void onChange() {
        if (txtUSSD.getText().length() > 0) {
          btnOk.setEnabled(true);
        } else {
          btnOk.setEnabled(false);
        }
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        onChange();
      }
    };
    txtUSSD.getDocument().addDocumentListener(documentListener);

    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnOk.doClick();
      }
    };
    txtUSSD.addActionListener(actionListener);



    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        applied = e.getSource() == btnOk;
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }
      }
    };
    btnCancel.addActionListener(listener);
    btnCancel.addActionListener(e -> closeDialogListeners.forEach(CloseDialogListener::close));
    btnOk.addActionListener(listener);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        closeDialogListeners.forEach(CloseDialogListener::close);
      }
    });
  }

  public String getUSSDText() {
    return txtUSSD.getText();
  }

  public void setUSSDText(final String USSD) {
    txtUSSD.setText(USSD);
  }

  public void clear() {
    txtUSSD.setText("");
  }

  public boolean showDialog(final String response) {
    if (response == null) {
      panelResponse.setVisible(false);
      setSize(400, 150);
    } else {
      panelResponse.setVisible(true);
      responseTextArea.setText(response);
      responseTextArea.setCaretPosition(0);
      setSize(400, 300);
    }
    setVisible(true);
    return applied;
  }

  public boolean isGetResponse() {
    return responseCheckBox.isSelected();
  }

  public interface CloseDialogListener {

    void close();

  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        boolean res;

        SendUSSDDialog dialog = new SendUSSDDialog(null);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
        res = dialog.showDialog(null);
        System.out.println(res);
      }
    });
  }

}

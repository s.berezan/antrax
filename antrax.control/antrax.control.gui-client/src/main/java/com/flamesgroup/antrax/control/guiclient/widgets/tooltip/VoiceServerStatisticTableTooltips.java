/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class VoiceServerStatisticTableTooltips {

  private VoiceServerStatisticTableTooltips() {
  }

  public static String getVoiceServerTooltip() {
    return null;
  }

  public static String getMobileChannelTooltip() {
    return new TooltipHeaderBuilder("GSM channel").addText("Number of device and GSM module").build();
  }

  public static String getAsrTooltip() {
    return new TooltipHeaderBuilder("Answer seizure ratio since the launch of the voice server").build();
  }

  public static String getAcdTooltip() {
    return new TooltipHeaderBuilder("Average call duration since the launch of the voice server").build();
  }

  public static String getSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful calls since the launch of the voice server").build();
  }

  public static String getTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total calls since the launch of the voice server").build();
  }

  public static String getCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration since the launch of the voice server").build();
  }

  public static String getSuccessSmsTooltip() {
    return new TooltipHeaderBuilder("Successful SMS count since the launch of the voice server").build();
  }

  public static String getTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total SMS count since the launch of the voice server").build();
  }

  public static String getSmsAsrTooltip() {
    return new TooltipHeaderBuilder("Successful sending seizure ratio of SMS since the launch of the voice server").build();
  }

  public static String getIncomingTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total incoming SMS count since the launch of the voice server").build();
  }

}

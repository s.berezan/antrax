/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class CreateRegistryEntryDialog extends JDialog {
  private static final long serialVersionUID = 3326381184126630109L;

  private final JTextField txtPath = new JTextField(35);
  private final JTextField txtValue = new JTextField(35);

  private final JButton btnOk = new JButton("Add");
  private final JButton btnCancel = new JButton("Cancel");

  private boolean applied = false;

  public JButton getOkBtn() {
    return btnOk;
  }

  public CreateRegistryEntryDialog(final Window window) {
    super(window);
    setLayout(new BoxLayout(getContentPane(), 3));
    setContentPane(createPanel());
    initializeEvents();
    setPreferredSize(new Dimension(490, 130));
    pack();
    setResizable(false);
  }

  private JPanel createPanel() {
    JLabel lblPath = new JLabel("Path:", JLabel.TRAILING);
    JLabel lblValue = new JLabel("Value:", JLabel.TRAILING);

    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    lblPath.setLabelFor(txtPath);
    lblValue.setLabelFor(txtValue);

    resizeToWider(lblPath, lblValue, txtPath.getPreferredSize().height);
    resizeToWider(btnOk, btnCancel, btnOk.getPreferredSize().height);

    layout.putConstraint(SpringLayout.NORTH, lblPath, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.NORTH, txtPath, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.NORTH, lblValue, 5, SpringLayout.SOUTH, txtPath);
    layout.putConstraint(SpringLayout.NORTH, txtValue, 5, SpringLayout.SOUTH, txtPath);

    layout.putConstraint(SpringLayout.WEST, lblPath, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, lblValue, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, txtPath, 5, SpringLayout.EAST, lblPath);
    layout.putConstraint(SpringLayout.WEST, txtValue, 5, SpringLayout.EAST, lblPath);

    layout.putConstraint(SpringLayout.SOUTH, btnOk, -5, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.EAST, btnOk, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, btnCancel, -5, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.WEST, btnCancel, 5, SpringLayout.HORIZONTAL_CENTER, retval);

    retval.add(lblPath);
    retval.add(txtPath);
    retval.add(lblValue);
    retval.add(txtValue);
    retval.add(btnOk);
    retval.add(btnCancel);

    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    retval.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  private void initializeEvents() {
    FocusAdapter focusAdapter = new FocusAdapter() {
      @Override
      public void focusGained(final FocusEvent e) {
        ((JTextField) e.getComponent()).selectAll();
      }
    };
    txtPath.addFocusListener(focusAdapter);
    txtValue.addFocusListener(focusAdapter);

    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        applied = false;
        if (getPath().isEmpty()) {
          txtPath.requestFocus();
        } else {
          txtValue.requestFocus();
        }
      }
    });
    btnOk.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        txtValue.selectAll();
      }
    });
    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnOk.doClick();
      }
    };

    txtPath.addActionListener(actionListener);
    txtValue.addActionListener(actionListener);

    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }
      }
    };
    btnCancel.addActionListener(listener);
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  public void setPath(final String path) {
    txtPath.setText(path);
  }

  public String getPath() {
    return txtPath.getText();
  }

  public String getValue() {
    return txtValue.getText();
  }

  public boolean showDialog() {
    setVisible(true);
    return applied;
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        CreateRegistryEntryDialog dialog = new CreateRegistryEntryDialog(null);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);
      }
    });
  }

}

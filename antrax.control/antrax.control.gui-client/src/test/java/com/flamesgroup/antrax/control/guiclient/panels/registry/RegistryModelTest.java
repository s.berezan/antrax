/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.communication.RegistryAccessBean;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RegistryModelTest {

  @Mocked
  private RegistryAccessBean mock;

  @Test
  public void testAddMove() throws Exception {
    RegistryPathTable paths = new RegistryPathTable();
    RegistryValuesTable values = new RegistryValuesTable();

    RegistryModel model = new RegistryModel(paths, values);
    model.addEntry("test", "value");
    final RegistryEntry entry = values.getElemAt(0);
    model.moveEntry(entry, "newValue");

    new Expectations() {{
      mock.add(MainApp.clientUID, "test", "value", Integer.MAX_VALUE);
      mock.move(MainApp.clientUID, entry, "newValue");
    }};

    model.applyOutgoingOperations(mock);
  }

  @Test
  public void testAddRemove() throws Exception {
    RegistryPathTable paths = new RegistryPathTable();
    RegistryValuesTable values = new RegistryValuesTable();

    RegistryModel model = new RegistryModel(paths, values);
    model.addEntry("test", "value");
    RegistryEntry entry = values.getElemAt(0);
    model.removeValue(entry);

    new Expectations() {{
      mock.add(MainApp.clientUID, "test", "value", Integer.MAX_VALUE);
      mock.remove(MainApp.clientUID, entry);
    }};

    model.applyOutgoingOperations(mock);
  }

}

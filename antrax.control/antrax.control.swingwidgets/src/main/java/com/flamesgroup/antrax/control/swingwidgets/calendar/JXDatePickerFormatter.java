package com.flamesgroup.antrax.control.swingwidgets.calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.*;

public class JXDatePickerFormatter extends
    JFormattedTextField.AbstractFormatter {
  private static final long serialVersionUID = 1279422186258308763L;
  private DateFormat formats[] = null;

  public JXDatePickerFormatter() {
    formats = new DateFormat[3];
    formats[0] = new SimpleDateFormat(UIManager.getString("JXDatePicker.longFormat"));
    formats[1] = new SimpleDateFormat(UIManager.getString("JXDatePicker.mediumFormat"));
    formats[2] = new SimpleDateFormat(UIManager.getString("JXDatePicker.shortFormat"));
  }

  public JXDatePickerFormatter(final DateFormat[] formats) {
    this.formats = formats;
  }

  public DateFormat[] getFormats() {
    return formats;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object stringToValue(final String text) throws ParseException {
    Object result = null;
    ParseException pex = null;

    if (text == null || text.trim().length() == 0) {
      return null;
    }

    // If the current formatter did not work loop through the other
    // formatters and see if any of them can parse the string passed
    // in.
    for (DateFormat _format : formats) {
      try {
        result = (_format).parse(text);
        pex = null;
        break;
      } catch (ParseException ex) {
        pex = ex;
      }
    }

    if (pex != null) {
      throw pex;
    }

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String valueToString(final Object value) throws ParseException {
    if (value != null) {
      return formats[0].format(value);
    }
    return null;
  }

}

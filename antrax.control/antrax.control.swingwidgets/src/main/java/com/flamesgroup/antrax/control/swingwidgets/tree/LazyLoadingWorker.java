package com.flamesgroup.antrax.control.swingwidgets.tree;

import java.util.concurrent.ExecutionException;

import javax.swing.*;
import javax.swing.tree.MutableTreeNode;

public class LazyLoadingWorker extends SwingWorker<MutableTreeNode[], Object> {

  private final BackgroundWorker<MutableTreeNode[]> uiWorker;

  public LazyLoadingWorker(final BackgroundWorker<MutableTreeNode[]> nodeLoadingWorker) {
    this.uiWorker = nodeLoadingWorker;
  }

  @Override
  protected void done() {
    try {
      uiWorker.done(get());
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected MutableTreeNode[] doInBackground() throws Exception {
    return uiWorker.doInBackground();
  }

}

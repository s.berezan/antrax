package com.flamesgroup.antrax.control.swingwidgets.list;

import java.awt.*;

import javax.swing.*;

public class JCheckBoxList {

  private static final long serialVersionUID = 465888679249928022L;

  private final JList list;

  public JCheckBoxList() {
    this(null);
  }

  public JCheckBoxList(final Object[] items) {
    list = new JList(new DefaultCheckBoxListModel());
    list.setFocusable(true);
    list.setRequestFocusEnabled(true);
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setCellRenderer(new CheckListCellRenderer());

    CheckBoxListKeyMouseListener l = new CheckBoxListKeyMouseListener(this);
    list.addMouseListener(l);
    list.addKeyListener(l);

    if (items != null) {
      for (Object item : items) {
        getModel().addItem(item);
      }
    }
  }

  public CheckBoxListModel getModel() {
    return (CheckBoxListModel) list.getModel();
  }

  public JComponent getComponent() {
    return list;
  }

  public boolean isItemChecked(final Object item) {
    return getModel().isChecked(item);
  }

  public void setChecked(final Object item, final boolean checked) {
    getModel().setChecked(item, checked);
  }

  public int getSelectedIndex() {
    return list.getSelectedIndex();
  }

  public int locationToIndex(final Point location) {
    return list.locationToIndex(location);
  }

  public void addItem(final Object item) {
    getModel().addItem(item);
  }

  public void insertItemAt(final Object item, final int index) {
    getModel().insertItemAt(item, index);
  }

  public void removeItemAt(final int index) {
    getModel().removeItemAt(index);
  }

  public boolean removeItem(final Object item) {
    return getModel().removeItem(item);
  }

  public void removeAllItems() {
    getModel().removeAllItems();
  }

  public void checkAll(final boolean checked) {
    for (int i = 0; i < getModel().getSize(); i++) {
      getModel().setChecked(getModel().getElementAt(i), checked);
    }
  }

  public Object[] getCheckedItems() {
    return getModel().getCheckedItems();
  }

}

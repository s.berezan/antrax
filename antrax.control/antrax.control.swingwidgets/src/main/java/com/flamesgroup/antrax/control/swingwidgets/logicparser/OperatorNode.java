package com.flamesgroup.antrax.control.swingwidgets.logicparser;

import java.util.List;
import java.util.Stack;

/**
 * A node implementation that represents an operation.
 */
public abstract class OperatorNode implements Node {

  /**
   * The left branch.
   */
  private Node left;
  /**
   * The right branch.
   */
  private Node right;

  /**
   * Lets the node pop its own branch nodes off the front of the specified
   * list. The default pulls two.
   */
  public void popValues(final List<Node> values) throws NoOperandsException {
    if (values.size() < 2) {
      throw new NoOperandsException("No operands for binary operator.");
    }
    right = values.remove(0);
    left = values.remove(0);
  }

  public void popValues(final Stack<Node> values) {
    right = values.pop();
    left = values.pop();
  }

  /**
   * Returns a preference level suitable for comparison to other {@code
   * OperationNode} preference levels.
   */
  public abstract int getPrecedence();

  protected Node getLeft() {
    return left;
  }

  protected Node getRight() {
    return right;
  }

  protected void setLeft(final Node left) {
    this.left = left;
  }

  protected void setRight(final Node right) {
    this.right = right;
  }

}

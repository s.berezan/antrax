package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class NoOperandsException extends Exception {

  private static final long serialVersionUID = -2682633618434723644L;

  public NoOperandsException(final String message) {
    super(message);
  }


}

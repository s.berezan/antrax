package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.swingwidgets.contextsearch.ContextSearchRowFilter;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.SearchFilter;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.SearchItem;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * JTable, which can be updated using bulks of data. This table understands
 * element changes and updates them instead of removing and adding.
 *
 * @param <T> elem type
 * @param <K> key type
 */
public class JUpdatableTable<T, K extends Comparable<K>> extends JMetalTable implements SearchItem {

  public static final int AUTO_WIDTH = -1;

  private static final long serialVersionUID = -4941963014638734457L;
  private final UpdatableTableModel<T, K> model;
  private String searchItemName;
  private final TableRowSorter<UpdatableTableModel<T, K>> sorter;
  private ContextSearchRowFilter rowFilter;
  private UpdatableTableRendererHandler rendererHandler;

  private final List<ElementSelectionListener<T>> elementSelectionListeners = new LinkedList<>();
  private List<T> prevSelection = new ArrayList<>();

  private final TableBuilder<T, K> tableBuilder;

  private boolean updating;

  private T[] data;

  private UpdatableTableFilter<T> filter;

  public JUpdatableTable(final TableBuilder<T, K> tableBuilder) {
    // create model
    UpdateTableColumnModel columnModel = new DefaultUpdateTableColumnModel();
    UpdatableTableModel<T, K> m = new UpdatableTableModel<>(tableBuilder, columnModel);
    setModel(m);
    model = m;
    this.tableBuilder = tableBuilder;
    getSelectionModel().addListSelectionListener(createSelectionListener());

    // create sorter
    sorter = new TableRowSorter<>();
    sorter.setModel(model);

    sorter.setSortsOnUpdates(true);
    setRowSorter(sorter);

    // build columns
    adjustUpdateColumnModel(columnModel);

    setDragEnabled(true);
    setTransferHandler(new JUpdatableTableTransferHandler());
  }

  protected TableBuilder<T, K> getTableBuilder() {
    return tableBuilder;
  }

  private ListSelectionListener createSelectionListener() {
    return new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        if (e.getValueIsAdjusting() || updating) {
          return;
        }
        // Checking whether element selection changed
        List<T> currSelection = collectSelectedElems();
        Set<K> oldKeys = collectKeys(prevSelection);
        Set<K> newKeys = collectKeys(currSelection);
        if (!oldKeys.equals(newKeys)) {
          for (ElementSelectionListener<T> l : elementSelectionListeners) {
            l.handleSelectionChanged(currSelection, prevSelection);
          }
        }
        prevSelection = currSelection;
      }

      private Set<K> collectKeys(final List<T> elems) {
        HashSet<K> retval = new HashSet<>();
        for (T e : elems) {
          retval.add(tableBuilder.getUniqueKey(e));
        }
        return retval;
      }
    };
  }

  @Override
  public void setModel(final TableModel dataModel) {
    if (model != null) {
      throw new UnsupportedOperationException("This table has already the best model ever");
    }
    super.setModel(dataModel);
  }

  public void scrollToRow(final int row) {
    if (row < 0) {
      return;
    }
    Rectangle r = getCellRect(sorter.convertRowIndexToView(row), 0, true);
    scrollRectToVisible(r);
    if (getParent() instanceof JViewport && getParent().getParent() instanceof JScrollPane) {
      JScrollPane scp = (JScrollPane) getParent().getParent();
      scp.scrollRectToVisible(r);
    }
  }

  public T getElemAt(final int viewRow) {
    return model.getElemAt(getModelRow(viewRow));
  }

  public List<Object> getTransferableObjects(final int row) {
    return model.getTransferableObjects(getModelRow(row));
  }

  private int getModelRow(final int viewRow) {
    return (getRowSorter() == null) ? viewRow : (viewRow < 0 ? viewRow : getRowSorter().convertRowIndexToModel(viewRow));
  }

  private int getViewRow(final int modelRow) {
    return (getRowSorter() == null) ? modelRow : (modelRow < 0 ? modelRow : getRowSorter().convertRowIndexToView(modelRow));
  }

  public int getRowOf(final T elem) {
    return model.getRowOf(elem);
  }

  public int getViewRowOf(final T elem) {
    RowSorter<? extends TableModel> rowSorter = getRowSorter();
    int rowOf = getRowOf(elem);
    if (rowSorter != null) {
      rowOf = rowSorter.convertRowIndexToView(rowOf);
    }
    return rowOf;
  }

  /**
   * @param elem
   * @param viewRow row index in view
   */
  public void updateElemAt(final T elem, final int viewRow) {
    updating = true;
    List<T> selected = collectSelectedElems();
    if (getSelectionModel().isSelectedIndex(viewRow)) {
      int si = selected.indexOf(getElemAt(viewRow));
      if (si >= 0) {
        selected.set(si, elem);
      }
    }
    model.updateElemAt(elem, getModelRow(viewRow));
    updating = false;
    restoreSelection(selected, getSelectedRows());
  }

  public void updateElemAt(final int viewRow) {
    updateElemAt(getElemAt(viewRow), viewRow);
  }

  /**
   * @param elem
   * @return view row index
   */
  public int insertElem(final T elem) {
    updating = true;
    List<T> selected = collectSelectedElems();
    int row = model.insertElem(elem);
    updating = false;
    restoreSelection(selected, getSelectedRows());
    return getViewRow(row);
  }

  public void removeElem(final T elem) {
    updating = true;
    List<T> selected = collectSelectedElems();
    model.removeElem(elem);
    updating = false;
    restoreSelection(selected, getSelectedRows());
  }

  public void setData(final T... data) {
    this.data = data;
    updating = true;
    List<T> selected = collectSelectedElems();
    model.setData(filtrate(data));
    updating = false;
    restoreSelection(selected, getSelectedRows());
  }

  @SuppressWarnings("unchecked")
  private T[] filtrate(final T[] data) {
    if (filter == null) {
      return data;
    }
    List<T> retval = new ArrayList<>();

    for (T d : data) {

      if (filter.isAccepted(d)) {
        retval.add(d);
      }
    }
    return retval.toArray((T[]) Array.newInstance(data.getClass().getComponentType(), retval.size()));
  }

  public void setFilter(final UpdatableTableFilter<T> filter) {
    this.filter = filter;
    if (data != null) {
      setData(this.data);
    }
  }

  @SuppressWarnings("unchecked")
  public void clearData() {
    model.setData();
  }

  @Override
  public void clearSelection() {
    clearSelection(false);
  }

  public void clearSelection(final boolean force) {
    super.clearSelection();
    if (force && prevSelection != null) {
      prevSelection.clear();
    }
  }

  @Override
  public String getSearchItemName() {
    return this.searchItemName;
  }

  private ContextSearchRowFilter getRowFilter() {
    if (rowFilter == null) {
      rowFilter = new ContextSearchRowFilter();
    }
    return rowFilter;
  }

  public void setRowFilter(final ContextSearchRowFilter rowFilter) {
    this.rowFilter = rowFilter;
  }

  @Override
  public void applySearchFilter(final SearchFilter searchFilter) {
    RowSorter<? extends TableModel> rs = getRowSorter();
    if (rs != null && rs instanceof TableRowSorter<?>) {
      getRowFilter().setSearchFilter(searchFilter);
      ((TableRowSorter<? extends TableModel>) rs).setRowFilter(getRowFilter());
    }
  }

  private void restoreSelection(final List<T> selected, final int[] oldSelection) {
    getSelectionModel().setValueIsAdjusting(true);
    int oldIndex = 0;
    int currIndex = 0;
    while (currIndex < selected.size()) {
      T curSelectedElem = selected.get(currIndex);
      int row = getViewRow(getRowOf(curSelectedElem));
      int oldRow = oldIndex >= oldSelection.length ? row + 1 : oldSelection[oldIndex];
      if (row < 0) {
        currIndex++;
        continue;
      }
      if (oldRow == row) {
        oldIndex++;
        currIndex++;
        continue;
      }
      if (oldRow > row) {
        getSelectionModel().addSelectionInterval(row, row);
        currIndex++;
        continue;
      }
      if (oldRow < row) {
        getSelectionModel().removeSelectionInterval(oldRow, oldRow);
        oldIndex++;
      }
    }

    while (oldIndex < oldSelection.length) {
      getSelectionModel().removeIndexInterval(oldSelection[oldIndex], oldSelection[oldIndex]);
      oldIndex++;
    }
    getSelectionModel().setValueIsAdjusting(false);
  }

  private List<T> collectSelectedElems() {
    int[] selected = getSelectedRows();
    List<T> retval = new ArrayList<>(selected.length);
    for (int viewRow : selected) {
      int row = getModelRow(viewRow);
      retval.add(model.getElemAt(row));
    }
    return retval;
  }

  public Collection<T> getElems() {
    return model.getElems();
  }

  public List<T> getSelectedElems() {
    int[] selected = getSelectedRows();
    List<T> retval = new ArrayList<>(selected.length);
    for (int viewRow : selected) {
      retval.add(getElemAt(viewRow));
    }
    return retval;
  }

  public T getSelectedElem() {
    int viewRow = getSelectedRow();
    if (viewRow < 0) {
      return null;
    }
    return getElemAt(viewRow);
  }

  public void selectRow(final int viewRow) {
    getSelectionModel().clearSelection();
    getSelectionModel().addSelectionInterval(viewRow, viewRow);
  }

  public void selectElem(final T elem) {
    selectRow(getViewRowOf(elem));
  }

  public void adjustUpdateColumnModel(final UpdateTableColumnModel columnModel) {
    for (int columnIndex = 0; columnIndex < columnModel.size(); columnIndex++) {
      UpdateTableColumn column = columnModel.getColumnAt(columnIndex);
      TableColumn tc = getColumnModel().getColumn(columnIndex);

      if (column.getWidth() != JUpdatableTable.AUTO_WIDTH) {
        tc.setWidth(column.getWidth());
      }

      if (column.getPreferredWidth() != JUpdatableTable.AUTO_WIDTH) {
        tc.setPreferredWidth(column.getPreferredWidth());
      }

      if (column.getMinWidth() != JUpdatableTable.AUTO_WIDTH) {
        tc.setMinWidth(column.getMinWidth());
      }

      if (column.getMaxWidth() != JUpdatableTable.AUTO_WIDTH) {
        tc.setMaxWidth(column.getMaxWidth());
      }

      if (column.getRenderer() != null) {
        tc.setCellRenderer(column.getRenderer());
      }

      if (column.getComparator() != null) {
        sorter.setComparator(columnIndex, column.getComparator());
      }
    }

    applySortOrder(sorter, columnModel);
  }

  private void applySortOrder(final TableRowSorter<?> sorter, final UpdateTableColumnModel columnModel) {
    if (columnModel.getDefaultSortSequence().length == 0) {
      return;
    }

    for (ColumnSortOrder cso : columnModel.getDefaultSortSequence()) {
      int columnIndex = columnModel.columnIndexOf(cso.getColumn());

      int toggleCount = 0;
      switch (cso.getSortOrder()) {
        case ASCENDING:
          toggleCount = 1;
          break;
        case DESCENDING:
          toggleCount = 2;
          break;
      }

      for (int i = 0; i < toggleCount; i++) {
        sorter.toggleSortOrder(columnIndex);
      }
    }
  }

  @Override
  public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
    if (getRendererHandler() != null) {
      Component defaultComponent = super.prepareRenderer(renderer, row, column);
      return getRendererHandler().prepareRenderer(defaultComponent, renderer, row, column);
    } else {
      return super.prepareRenderer(renderer, row, column);
    }
  }

  public UpdatableTableRendererHandler getRendererHandler() {
    return rendererHandler;
  }

  public void setRendererHandler(final UpdatableTableRendererHandler rendererHandler) {
    this.rendererHandler = rendererHandler;
  }

  public void addElementSelectionListener(final ElementSelectionListener<T> listener) {
    this.elementSelectionListeners.add(listener);
  }

  public void removeElementSelectionListener(final ElementSelectionListener<T> listener) {
    this.elementSelectionListeners.remove(listener);
  }

  public static void main(final String[] args) throws InterruptedException, InvocationTargetException {
    final JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final JUpdatableTable<String, String> table = new JUpdatableTable<>(new TableBuilder<String, String>() {

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("str", String.class);
        columns.addColumn("index", Integer.class);
        columns.addColumn("str1", String.class);
      }

      @Override
      public String getUniqueKey(final String src) {
        return src;
      }

      @Override
      public void buildRow(final String src, final ColumnWriter<String> dest) {
        dest.writeColumn(src);
        dest.writeColumn((int) (Math.random() * 100));
      }
    });
    frame.getContentPane().add(new JScrollPane(table));
    table.setName("test");
    table.getUpdatableTableProperties().restoreProperties();
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        table.getUpdatableTableProperties().saveProperties();
        super.windowClosing(e);
      }
    });
    frame.pack();
    Thread updateThread = new Thread() {
      @Override
      public void run() {
        while (true) {
          try {
            Thread.sleep(700);
          } catch (InterruptedException ignored) {
            break;
          }
          table.setData("d", "b", "a", "f", "k", "t", "p", "l", "s", "m", "r");
        }

      }
    };
    updateThread.start();

    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

  @Override
  public void setSearchItemName(final String name) {
    searchItemName = name;
  }

}

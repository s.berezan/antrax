package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Editor for IP address.
 * <p>
 * It validates input and allows only numbers from 0 to 255. After user inputs 3
 * digits it moves to the next atom. After user removes all digits it becomes
 * zero and moves to the previous atom.
 */
public class IPAddressField extends JPanel {
  private static final long serialVersionUID = -5506624366783244579L;

  private final JTextField[] editors = new JTextField[4];

  public IPAddressField() {
    setLayout(new FlowLayout(FlowLayout.LEFT));
    for (int i = 0; i < editors.length; ++i) {
      editors[i] = createEditor();
      add(editors[i]);
      if (i != editors.length - 1) {
        add(new JLabel("."));
      }
    }
    setName(getName());
  }

  @Override
  public void setName(final String name) {
    super.setName(name);
    for (int i = 0; i < editors.length; ++i) {
      editors[i].setName(name + ".ipField" + i);
    }
  }

  private JTextField createEditor() {
    final JTextField retval = new JTextField(2);
    retval.addFocusListener(focusListener);
    retval.setHorizontalAlignment(JTextField.RIGHT);
    retval.getDocument().addDocumentListener(documentListener);
    retval.setText("0");
    return retval;
  }

  public void addActionListener(final ActionListener listener) {
    for (JTextField e : editors) {
      e.addActionListener(listener);
    }
  }

  public String getText() {
    StringBuilder str = new StringBuilder();
    for (int i = 0; i < editors.length; ++i) {
      str.append(editors[i].getText());
      if (i != editors.length - 1) {
        str.append(".");
      }
    }
    return str.toString();
  }

  public void setText(final String address) {
    String[] atoms = address.split("\\.");
    if (atoms.length != editors.length) {
      throw new NumberFormatException();
    }
    for (int i = 0; i < editors.length; ++i) {
      editors[i].setText(atoms[i]);
    }
  }

  public void setIpAddress(final InetAddress address) {
    if (address != null) {
      setText(address.getHostAddress());
    } else {
      setText("0.0.0.0");
    }
  }

  public InetAddress getAddress() {
    try {
      return InetAddress.getByName(getText());
    } catch (UnknownHostException ignored) {
      return null;
    }
  }

  public void setEditable(final boolean value) {
    for (JTextField e : editors) {
      e.setEditable(value);
    }
  }

  private void moveToTheNextEditor() {
    for (int i = 0; i < editors.length - 1; ++i) {
      if (editors[i].hasFocus()) {
        editors[i + 1].requestFocus();
        break;
      }
    }
  }

  private void moveToThePrevEditor() {
    for (int i = 1; i < editors.length; ++i) {
      if (editors[i].hasFocus()) {
        editors[i - 1].requestFocus();
        break;
      }
    }
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    IPAddressField ipAddressField = new IPAddressField();
    ipAddressField.setText("192.168.10.50");
    frame.getContentPane().add(ipAddressField);
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

  private final FocusAdapter focusListener = new FocusAdapter() {
    @Override
    public void focusGained(final FocusEvent e) {
      ((JTextField) e.getComponent()).selectAll();
    }
  };

  private final DocumentListener documentListener = new DocumentListener() {

    private void laterRemoveUpdate(final DocumentEvent e) {
      cleanupNumber(e.getDocument());
      if (e.getDocument().getLength() == 0) {
        try {
          e.getDocument().insertString(0, "0", null);
        } catch (BadLocationException e1) {
          e1.printStackTrace();
        }
        moveToThePrevEditor();
      }
    }

    private void laterInsertUpdate(final DocumentEvent e) {
      cleanupNumber(e.getDocument());
      final int length = e.getDocument().getLength();
      try {
        if (isValid(e.getDocument().getText(0, length))) {
          if (length > 2) {
            moveToTheNextEditor();
          }
        } else {
          e.getDocument().remove(e.getOffset(), e.getLength());
        }
      } catch (BadLocationException e1) {
        e1.printStackTrace();
      }
    }

    private boolean isValid(final String text) {
      try {
        int val = Integer.parseInt(text);
        return val >= 0 && val <= 255;
      } catch (NumberFormatException ignored) {
        return false;
      }
    }

    private void cleanupNumber(final Document document) {
      try {
        String text = document.getText(0, document.getLength());
        int length = 0;
        for (int i = 0; i < text.length() - 1; ++i) {
          if (text.charAt(i) == '0') {
            length++;
          } else {
            break;
          }
        }
        if (length > 0) {
          document.remove(0, length);
        }
      } catch (Exception ignored) {

      }
    }

    @Override
    public void removeUpdate(final DocumentEvent e) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          laterRemoveUpdate(e);
        }
      });
    }

    @Override
    public void insertUpdate(final DocumentEvent e) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          laterInsertUpdate(e);
        }
      });
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
      cleanupNumber(e.getDocument());
    }
  };
}

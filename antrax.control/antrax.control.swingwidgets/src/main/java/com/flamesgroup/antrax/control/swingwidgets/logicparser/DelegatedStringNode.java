package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class DelegatedStringNode extends StringNode {

  public interface EvaluationDelegate {

    boolean evaluate(String value);

  }

  private final EvaluationDelegate delegate;

  public DelegatedStringNode(final String value, final EvaluationDelegate delegate) {
    super(value);
    this.delegate = delegate;
  }

  @Override
  public boolean evaluate() {
    return delegate.evaluate(getValue());
  }

}

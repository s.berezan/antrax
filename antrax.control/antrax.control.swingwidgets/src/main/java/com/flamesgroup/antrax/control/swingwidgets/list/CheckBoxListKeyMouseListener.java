package com.flamesgroup.antrax.control.swingwidgets.list;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CheckBoxListKeyMouseListener extends MouseAdapter implements KeyListener {

  private final JCheckBoxList cboxList;

  public CheckBoxListKeyMouseListener(final JCheckBoxList cboxList) {
    this.cboxList = cboxList;
  }

  @Override
  public void mouseClicked(final MouseEvent e) {
    if (cboxList.getComponent().isEnabled()) {
      int index = cboxList.locationToIndex(e.getPoint());
      doCheck(index);
    }
  }

  @Override
  public void keyPressed(final KeyEvent e) {
    if (cboxList.getComponent().isEnabled() && e.getKeyChar() == ' ') {
      doCheck(cboxList.getSelectedIndex());
    }
  }

  @Override
  public void keyReleased(final KeyEvent e) {
  }

  @Override
  public void keyTyped(final KeyEvent e) {
  }

  protected void doCheck(final int index) {
    if (index < 0) {
      return;
    }

    CheckBoxListModel model = cboxList.getModel();
    Object item = model.getElementAt(index);
    model.invertChecking(item);
    cboxList.getComponent().repaint();
  }

}

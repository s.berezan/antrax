package com.flamesgroup.antrax.control.swingwidgets.table;

import java.util.List;

public interface ElementSelectionListener<T> {
  void handleSelectionChanged(List<T> currSelection, List<T> prevSelection);
}

package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class DefaultStringNodeFactory implements StringNodeFactory {

  @Override
  public StringNode createStringNode(final String str) {
    return new StringNode(str);
  }

}

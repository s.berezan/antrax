package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class LogicExpressionTokenizer {

  private final char[] expression;
  private final int length;
  private int index;
  private String tokenValue = null;

  /**
   * Creates a new parser for the specified logical expression.
   *
   * @param expression logical expression string representation
   */
  public LogicExpressionTokenizer(final String expression) {
    this.expression = expression.trim().toCharArray();
    this.length = this.expression.length;
    this.index = 0;
  }

  /**
   * @return true if there are more tokens
   */
  public boolean hasMoreTokens() {
    return index < length;
  }

  /**
   * Index may be use for error reporting purposes.
   *
   * @return current index
   */
  public int getIndex() {
    return index;
  }

  /**
   * @return the String value of the token if it was type {@code
   * LogicExpressionToken.STRING}. Otherwise null is returned.
   */
  public String getTokenValue() {
    return tokenValue;
  }

  private char getCurrentChar() {
    return expression[index];
  }

  protected boolean isMetaChar(final char c) {
    return Character.isWhitespace(c) || LogicLexeme.isLexeme(c);
  }

  /**
   * @return the next token type and initializes any state variables
   * accordingly.
   */
  public LogicToken nextToken() {
    // Skip any leading white space
    for (; index < length && Character.isWhitespace(getCurrentChar()); index++)
      ;

    // Clear the current token value
    tokenValue = null;

    // End of string ?
    if (index == length) {
      return LogicToken.END;
    }

    int start = index;

    char currentChar = getCurrentChar();
    char nextChar = (char) 0;
    index++;
    if (index < length) {
      nextChar = getCurrentChar();
    }

    // Check for a known token
    LogicToken token = LogicToken.parseChar(currentChar);
    if (token != null) {
      if (LogicToken.EQUAL == LogicToken.parseChar(nextChar)) {
        switch (token) {
          case NOT:
            index++;
            return LogicToken.NOT_EQ;
          case GT:
            index++;
            return LogicToken.GE;
          case LT:
            index++;
            return LogicToken.LE;
        }
      }
      return token;
    }

    // Otherwise it's a string
    int end;

    // If it's a quoted string then end is the next unescaped quote
    if (currentChar == '"' || currentChar == '\'') {
      boolean escaped = false;
      start++;
      for (; index < length; index++) {
        if (getCurrentChar() == '\\' && !escaped) {
          escaped = true;
          continue;
        }

        if (getCurrentChar() == currentChar && !escaped) {
          break;
        }
        escaped = false;
      }
      end = index;

      // Skip the end quote
      index++;
    } else {
      // End is the next whitespace character
      for (; index < length; index++) {
        if (isMetaChar(getCurrentChar())) {
          break;
        }
      }
      end = index;
    }

    // Extract the string from the array
    this.tokenValue = new String(expression, start, end - start);
    return LogicToken.STRING;
  }

}

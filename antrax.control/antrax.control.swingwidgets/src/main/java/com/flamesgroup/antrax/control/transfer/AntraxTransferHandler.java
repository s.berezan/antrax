package com.flamesgroup.antrax.control.transfer;

import java.awt.datatransfer.DataFlavor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

/**
 * Wise transfer handler, which use reflection to simplify programmer work.
 * <p>
 * You need to implement methods
 * <code>public boolean importObject(T type)</code>, where T is any type you
 * support to import.
 * <p>
 * Example:
 * <p>
 * <pre>
 * ...
 * public boolean importObject(SimUnitIdImpl simUnitId) {
 *    ...
 * }
 *
 * public boolean importObject(SimGroupImpl simGroup) {
 *    ...
 * }
 * ...
 * </pre>
 */
public class AntraxTransferHandler extends TransferHandler {
  private static final long serialVersionUID = 4483141810462939700L;

  @Override
  public boolean canImport(final TransferSupport support) {
    for (DataFlavor f : support.getDataFlavors()) {
      if (importers.containsKey(f)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean importData(final TransferSupport support) {
    try {
      for (DataFlavor f : support.getDataFlavors()) {
        if (importers.containsKey(f)) {
          for (Object object : (List<?>) support.getTransferable().getTransferData(f)) {
            importers.get(f).invoke(this, object);
          }
          return true;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private final Map<DataFlavor, Method> importers = new HashMap<>();

  public AntraxTransferHandler() {
    for (Method m : collectImportMethods()) {
      m.setAccessible(true);
      importers.put(dataFlavour(m.getParameterTypes()[0]), m);
    }
  }

  private List<Method> collectImportMethods() {
    List<Method> retval = new LinkedList<>();
    for (Method m : getClass().getMethods()) {
      if (!m.getName().equals("importObject")) {
        continue;
      }
      if (m.getParameterTypes().length != 1) {
        continue;
      }
      retval.add(m);
    }
    return retval;
  }

  private DataFlavor dataFlavour(final Class<?> clazz) {
    return new DataFlavor(clazz, clazz.getCanonicalName());
  }
}

package com.flamesgroup.antrax.control.swingwidgets.table;

import java.awt.*;

import javax.swing.table.TableCellRenderer;

public interface UpdatableTableRendererHandler {

  Component prepareRenderer(Component defaultComponent, TableCellRenderer renderer, int row, int column);

}

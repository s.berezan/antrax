package com.flamesgroup.antrax.control.swingwidgets.table;

import java.util.Comparator;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

public interface UpdateTableColumn {

  String getName();

  Class<?> getType();

  int getWidth();

  int getPreferredWidth();

  int getMinWidth();

  int getMaxWidth();

  Comparator<?> getComparator();

  TableCellRenderer getRenderer();

  CellExporter getCellExporter();

  SortOrder getDefaultSortOrder();


  UpdateTableColumn setName(String name);

  UpdateTableColumn setType(Class<?> type);

  UpdateTableColumn setPreferredWidth(int preferredWidth);

  UpdateTableColumn setMinWidth(int minWidth);

  UpdateTableColumn setMaxWidth(int maxWidth);

  UpdateTableColumn setWidth(int width);

  UpdateTableColumn setComparator(Comparator<?> comparator);

  UpdateTableColumn setRenderer(TableCellRenderer renderer);

  UpdateTableColumn setCellExporter(CellExporter cellExporter);

  UpdateTableColumn setDefaultSortOrder(SortOrder sortOrder);

}

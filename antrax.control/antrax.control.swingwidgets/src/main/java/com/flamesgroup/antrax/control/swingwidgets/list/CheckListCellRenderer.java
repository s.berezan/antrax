package com.flamesgroup.antrax.control.swingwidgets.list;

import java.awt.*;

import javax.swing.*;

public class CheckListCellRenderer extends JCheckBox implements
    ListCellRenderer {

  private static final long serialVersionUID = 5839170937698710774L;

  public CheckListCellRenderer() {
    super();
    setOpaque(true);
  }

  @Override
  public Component getListCellRendererComponent(final JList list, final Object item,
      final int index, final boolean isSelected, final boolean cellHasFocus) {
    CheckBoxListModel model = (CheckBoxListModel) list.getModel();

    setFont(list.getFont());
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
    //setBorder((cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : m_noFocusBorder);

    setText(item.toString());
    setSelected(model.isChecked(item));
    setEnabled(list.isEnabled());
    return this;
  }

}

package com.flamesgroup.antrax.control;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class GuiProperties {

  protected final Gson gson = new Gson();
  protected final Properties properties = new Properties();
  private final String fileName;

  protected GuiProperties(final String fileName) {
    this.fileName = fileName;
  }

  protected abstract void restoreOutProperties();

  protected abstract void saveOutProperties();

  public void restoreProperties() {
    File antraxDirectory = new File(getUserDataDirectory());
    if (!antraxDirectory.exists()) {
      if (!antraxDirectory.mkdirs()) {
        System.out.println("Can't create antrax directory");
        return;
      }
    }
    try {
      FileInputStream out = new FileInputStream(getUserDataDirectory() + fileName);
      properties.load(out);
      out.close();
    } catch (IOException e) {
      System.out.println("Use default: " + e.getMessage());
      return;
    }
    restoreOutProperties();
  }

  public void saveProperties() {
    properties.clear();

    saveOutProperties();
    try {
      FileOutputStream out = new FileOutputStream(getUserDataDirectory() + fileName);
      properties.store(out, null);
      out.close();
    } catch (IOException e) {
      System.out.println("Can't save properties: " + e.getMessage());
    }
  }

  private String getUserDataDirectory() {
    return System.getProperty("user.home") + File.separator + ".antrax-gui" + File.separator;
  }

}

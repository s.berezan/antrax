package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

public interface Searchable {

  void applySearchFilter(SearchFilter searchFilter);

}

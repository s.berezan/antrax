package com.flamesgroup.antrax.control.swingwidgets.table;

import java.util.Comparator;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class UpdateTableColumnImpl implements UpdateTableColumn {

  private String name;
  private Class<?> type = Object.class;
  private int width = JUpdatableTable.AUTO_WIDTH;
  private int minWidth = JUpdatableTable.AUTO_WIDTH;
  private int maxWidth = JUpdatableTable.AUTO_WIDTH;
  private int preferredWidth = JUpdatableTable.AUTO_WIDTH;
  private TableCellRenderer renderer = new DefaultTableCellRenderer();
  private Comparator<?> comparator;
  private SortOrder sortOrder = SortOrder.UNSORTED;
  private CellExporter cellExporter = new DefaultCellExporter();

  public UpdateTableColumnImpl(final String name, final Class<?> type) {
    setName(name);
    setType(type);
  }

  @Override
  public UpdateTableColumn setComparator(final Comparator<?> comparator) {
    this.comparator = comparator;
    return this;
  }

  @Override
  public UpdateTableColumn setPreferredWidth(final int preferredWidth) {
    this.preferredWidth = preferredWidth;
    return this;
  }

  @Override
  public UpdateTableColumn setMaxWidth(final int maxWidth) {
    this.maxWidth = maxWidth;
    return this;
  }

  @Override
  public UpdateTableColumn setMinWidth(final int minWidth) {
    this.minWidth = minWidth;
    return this;
  }

  @Override
  public UpdateTableColumn setName(final String name) {
    this.name = name;
    return this;
  }

  @Override
  public UpdateTableColumn setRenderer(final TableCellRenderer renderer) {
    this.renderer = renderer;
    return this;
  }

  @Override
  public UpdateTableColumn setCellExporter(final CellExporter cellExporter) {
    this.cellExporter = cellExporter;
    return this;
  }

  @Override
  public UpdateTableColumn setType(final Class<?> type) {
    this.type = type;
    return this;
  }

  @Override
  public UpdateTableColumn setWidth(final int width) {
    this.width = width;
    return this;
  }

  @Override
  public Comparator<?> getComparator() {
    return comparator;
  }

  @Override
  public int getMaxWidth() {
    return maxWidth;
  }

  @Override
  public int getMinWidth() {
    return minWidth;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getPreferredWidth() {
    return preferredWidth;
  }

  @Override
  public TableCellRenderer getRenderer() {
    return renderer;
  }

  @Override
  public CellExporter getCellExporter() {
    return cellExporter;
  }

  @Override
  public Class<?> getType() {
    return type;
  }

  @Override
  public int getWidth() {
    return width;
  }

  @Override
  public SortOrder getDefaultSortOrder() {
    return sortOrder;
  }

  @Override
  public UpdateTableColumn setDefaultSortOrder(final SortOrder sortOrder) {
    this.sortOrder = sortOrder;
    return this;
  }

}

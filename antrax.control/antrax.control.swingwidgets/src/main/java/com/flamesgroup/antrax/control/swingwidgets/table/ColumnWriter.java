package com.flamesgroup.antrax.control.swingwidgets.table;

public interface ColumnWriter<T> {
  void writeColumn(Object obj, Object... transferableObjects);
}

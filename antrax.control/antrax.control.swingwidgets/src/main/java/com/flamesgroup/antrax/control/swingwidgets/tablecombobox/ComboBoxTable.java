package com.flamesgroup.antrax.control.swingwidgets.tablecombobox;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class ComboBoxTable extends JTable {

  private static final long serialVersionUID = -3271226883788738716L;

  private final MouseOverHandler mouseOverHandler;
  private boolean mouseOverActive;

  public ComboBoxTable() {
    mouseOverHandler = new MouseOverHandler();
    setMouseOverActive(true);
  }

  public MouseOverHandler getMouseOverHandler() {
    return mouseOverHandler;
  }

  public void setShowHeaders(final boolean show) {
    getTableHeader().setPreferredSize(show ? getPreferredTableHeaderSize() : new Dimension(0, 0));
  }

  public void setMouseOverActive(final boolean flag) {
    mouseOverActive = flag;
    if (flag) {
      addMouseMotionListener(mouseOverHandler);
      addMouseListener(mouseOverHandler);
    } else {
      removeMouseMotionListener(mouseOverHandler);
      removeMouseListener(mouseOverHandler);
    }
  }

  public boolean isMouseOverActive() {
    return mouseOverActive;
  }

  protected Dimension getPreferredTableHeaderSize() {
    JTableHeader jtableheader = getTableHeader();
    TableCellRenderer tablecellrenderer = jtableheader.getDefaultRenderer();
    int i = 0;
    int j = 0;
    int k = 0;
    for (int l = jtableheader.getColumnModel().getColumnCount(); k < l; k++) {
      TableColumn tablecolumn = jtableheader.getColumnModel().getColumn(k);
      TableCellRenderer tableCellRenderer1 = tablecolumn.getHeaderRenderer();
      if (tableCellRenderer1 == null)
        tableCellRenderer1 = tablecellrenderer;
      Dimension dimension = tableCellRenderer1
          .getTableCellRendererComponent(this,
              tablecolumn.getHeaderValue(), false, false, 0, k)
          .getPreferredSize();
      i = Math.max(i, dimension.height);
      j += dimension.width;
    }

    return new Dimension(j, i);
  }

  protected Color getMouseOverBackground() {
    Color color = getSelectionBackground();
    Color color1 = getBackground();
    int i = (color.getRed() + color1.getRed()) / 2;
    int j = (color.getGreen() + color1.getGreen()) / 2;
    int k = (color.getBlue() + color1.getBlue()) / 2;
    return new Color(i, j, k);
  }

  protected boolean isSelectedRow(final int i) {
    int ai[] = getSelectedRows();
    int j = 0;
    for (int k = ai.length; j < k; j++) {
      if (ai[j] == i)
        return true;
    }

    return false;
  }

  protected boolean isSelectedColumn(final int i) {
    int ai[] = getSelectedColumns();
    int j = 0;
    for (int k = ai.length; j < k; j++) {
      if (ai[j] == i)
        return true;
    }

    return false;
  }

  @Override
  public Component prepareRenderer(final TableCellRenderer tableCellRenderer, final int i, final int j) {
    Component component = super.prepareRenderer(tableCellRenderer, i, j);
    if (isMouseOverActive())
      if (getRowSelectionAllowed() && getColumnSelectionAllowed()) {
        if (!isSelectedRow(i) || !isSelectedColumn(j))
          if (i == mouseOverHandler.getRow() && j == mouseOverHandler.getColumn()) {
            component.setBackground(getMouseOverBackground());
            component.setForeground(getSelectionForeground());
          } else {
            component.setBackground(getBackground());
            component.setForeground(getForeground());
          }
      } else if (getRowSelectionAllowed() && !getColumnSelectionAllowed()) {
        if (!isSelectedRow(i))
          if (i == mouseOverHandler.getRow()) {
            component.setBackground(getMouseOverBackground());
            component.setForeground(getSelectionForeground());
          } else {
            component.setBackground(getBackground());
            component.setForeground(getForeground());
          }
      } else if (getColumnSelectionAllowed() && !getRowSelectionAllowed() && !isSelectedColumn(j))
        if (j == mouseOverHandler.getColumn()) {
          component.setBackground(getMouseOverBackground());
          component.setForeground(getSelectionForeground());
        } else {
          component.setBackground(getBackground());
          component.setForeground(getForeground());
        }
    return component;
  }

  protected void resize(final int i, final int j, final int k) {
    calcAndSetPreferredCellSizes(i);
    Dimension dimension = getPreferredSize();
    dimension.width += j;
    int l = Math.min(k, getRowCount());
    int i1 = getRowHeight();
    dimension.height = i1 * l;
    setPreferredScrollableViewportSize(dimension);
  }

  protected void calcAndSetPreferredCellSizes(final int i) {
    TableModel tablemodel = getModel();
    TableCellRenderer tablecellrenderer = getTableHeader().getDefaultRenderer();
    int j = 0;
    for (int k = 0; k < tablemodel.getColumnCount(); k++) {
      TableColumn tablecolumn = getColumnModel().getColumn(k);
      int l;
      int i1 = 0;
      TableCellRenderer tableCellRenderer1 = tablecolumn
          .getHeaderRenderer();
      if (tableCellRenderer1 == null)
        tableCellRenderer1 = tablecellrenderer;
      Component component = tableCellRenderer1
          .getTableCellRendererComponent(this, tablecolumn.getHeaderValue(), false, false, 0, k);
      l = component.getPreferredSize().width;
      TableCellRenderer tableCellRenderer2 = tablecolumn.getCellRenderer();
      if (tableCellRenderer2 == null)
        tableCellRenderer2 = getDefaultRenderer(tablemodel.getColumnClass(k));
      for (int j1 = 0; j1 < tablemodel.getRowCount(); j1++) {
        Component component1 = tableCellRenderer2
            .getTableCellRendererComponent(this, tablemodel.getValueAt(j1, k), false, false, j1, k);
        int l1 = component1.getPreferredSize().width;
        int i2 = component1.getPreferredSize().height;
        if (l1 > i1)
          i1 = l1;
        if (i2 > j)
          j = i2;
      }

      int k1 = Math.max(l, i1);
      k1 += i;
      tablecolumn.setPreferredWidth(k1);
    }

    setRowHeight(j + getRowMargin());
  }

  //
  // MouseOverHandler
  //
  public class MouseOverHandler extends MouseAdapter implements MouseMotionListener {

    private int row;
    private int column;

    public MouseOverHandler() {
      super();
      invalidateIndexes();
    }

    @Override
    public void mouseMoved(final MouseEvent mouseevent) {
      row = rowAtPoint(mouseevent.getPoint());
      column = columnAtPoint(mouseevent.getPoint());
      repaint();
    }

    @Override
    public void mouseExited(final MouseEvent mouseevent) {
      invalidateIndexes();
      repaint();
    }

    @Override
    public void mouseDragged(final MouseEvent mouseevent) {
    }

    public int getRow() {
      return row;
    }

    public int getColumn() {
      return column;
    }

    public void invalidateIndexes() {
      row = -1;
      column = -1;
    }

  }

}

package com.flamesgroup.antrax.control.swingwidgets.table;

import java.awt.*;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.table.JTableHeader;

public class JMetalTable extends JTable {

  private static final long serialVersionUID = -1282083692068746454L;
  private transient JUpdatableTableProperties jUpdatableTableProperties;

  private String[] columnToolTips;

  public JMetalTable() {
    super();
    setUI(new MetalTableUI());
    setShowHorizontalLines(true);
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //    getTableHeader().setDefaultRenderer(new ITunesTableHeaderRenderer());
    //    addHierarchyListener(new HierarchyListener() {
    //
    //      public void hierarchyChanged(HierarchyEvent e) {
    //        Component cmp = e.getChanged();
    //        if (cmp instanceof JScrollPane) {
    //          JScrollPane scp = (JScrollPane)cmp;
    //          scp.setCorner(ScrollPaneConstants.UPPER_TRAILING_CORNER,
    //              new ITunesTableUpperRightCorner());
    //        }
    //      }
    //
    //    });
  }

  @Override
  protected JTableHeader createDefaultTableHeader() {
    return new JTableHeader(columnModel) {
      private static final long serialVersionUID = -6175489835721540919L;

      @Override
      public String getToolTipText(final MouseEvent e) {
        if (columnToolTips.length > 0) {
          Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        } else {
          return super.getToolTipText(e);
        }
      }
    };
  }

  public void setColumnToolTips(final String[] columnToolTips) {
    this.columnToolTips = columnToolTips;
  }


  public JUpdatableTableProperties getUpdatableTableProperties() {
    if (jUpdatableTableProperties == null) {
      jUpdatableTableProperties = new JUpdatableTableProperties(this);
    }
    return jUpdatableTableProperties;
  }

  @Override
  protected void setUI(final ComponentUI newUI) {
    if (newUI instanceof MetalTableUI) {
      super.setUI(newUI);
    }
  }

  @Override
  public boolean getScrollableTracksViewportHeight() {
    return getPreferredSize().height < getParent().getHeight();
  }

  @Override
  public boolean getScrollableTracksViewportWidth() {
    return getPreferredSize().width < getParent().getWidth();
  }

}

package com.flamesgroup.antrax.control.swingwidgets.table;

import javax.swing.*;

public class ColumnSortOrder {

  private final UpdateTableColumn column;
  private final SortOrder sortOrder;

  public ColumnSortOrder(final UpdateTableColumn column, final SortOrder sortOrder) {
    super();
    this.column = column;
    this.sortOrder = sortOrder;
  }

  public UpdateTableColumn getColumn() {
    return column;
  }

  public SortOrder getSortOrder() {
    return sortOrder;
  }

}

package com.flamesgroup.antrax.control.swingwidgets.badge;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.ChangeListener;

public interface BadgeModel {

  void setText(String text);

  void setColor(Color color);

  void setVisible(boolean visible);

  void setIcon(Icon icon);

  Color getColor();

  String getText();

  boolean isVisible();

  Icon getIcon();

  void addChangeListener(ChangeListener l);

  void removeChangeListener(ChangeListener l);

}

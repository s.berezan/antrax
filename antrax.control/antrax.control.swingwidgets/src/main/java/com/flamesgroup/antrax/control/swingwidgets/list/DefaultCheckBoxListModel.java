package com.flamesgroup.antrax.control.swingwidgets.list;

import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class DefaultCheckBoxListModel extends DefaultListModel implements CheckBoxListModel {

  private static final long serialVersionUID = 8297214129332762221L;

  private final List<CheckBoxListItem> cboxItems = new ArrayList<>();

  public DefaultCheckBoxListModel() {
    addListDataListener(new ListDataListener() {

      @Override
      public void contentsChanged(final ListDataEvent e) {
        processChanges(e);
      }

      @Override
      public void intervalAdded(final ListDataEvent e) {
        processChanges(e);
      }

      @Override
      public void intervalRemoved(final ListDataEvent e) {
        processChanges(e);
      }

    });
  }

  private void processChanges(final ListDataEvent e) {
    for (int index = e.getIndex0(); index <= e.getIndex1(); index++) {
      switch (e.getType()) {
        case ListDataEvent.INTERVAL_ADDED:
          CheckBoxListItem item = new CheckBoxListItem(getElementAt(index), false);
          cboxItems.add(index, item);
          break;

        case ListDataEvent.INTERVAL_REMOVED:
          cboxItems.remove(e.getIndex0());
          break;

        case ListDataEvent.CONTENTS_CHANGED:
          cboxItems.get(index).setItem(getElementAt(index));
          break;
      }
    }
  }

  private CheckBoxListItem getCheckBoxItem(final Object item) {
    for (CheckBoxListItem cboxItem : cboxItems) {
      if (cboxItem.getItem().equals(item)) {
        return cboxItem;
      }
    }
    return null;
  }

  @Override
  public void invertChecking(final Object item) {
    CheckBoxListItem cboxItem = getCheckBoxItem(item);
    if (cboxItem != null) {
      cboxItem.invertChecking();
    }
  }

  @Override
  public boolean isChecked(final Object item) {
    CheckBoxListItem cboxItem = getCheckBoxItem(item);
    if (cboxItem != null) {
      return cboxItem.isChecked();
    }
    return false;
  }

  @Override
  public void setChecked(final Object item, final boolean checked) {
    CheckBoxListItem cboxItem = getCheckBoxItem(item);
    if (cboxItem != null) {
      cboxItem.setChecked(checked);
      int index = cboxItems.indexOf(cboxItem);
      fireContentsChanged(this, index, index);
    }
  }

  @Override
  public void addItem(final Object item) {
    super.addElement(item);
  }

  @Override
  public void insertItemAt(final Object item, final int index) {
    super.insertElementAt(item, index);
  }

  @Override
  public void removeAllItems() {
    super.removeAllElements();
  }

  @Override
  public boolean removeItem(final Object item) {
    return super.removeElement(item);
  }

  @Override
  public void removeItemAt(final int index) {
    super.removeElementAt(index);
  }

  @Override
  public Object[] getCheckedItems() {
    List<Object> selectedItems = new ArrayList<>();
    for (CheckBoxListItem cboxItem : cboxItems) {
      if (cboxItem.isChecked()) {
        selectedItems.add(cboxItem.getItem());
      }
    }
    return selectedItems.toArray();
  }


  /**
   * CheckBox list item class
   */
  private static class CheckBoxListItem {

    private Object item;
    private boolean checked;

    public CheckBoxListItem(final Object item, final boolean checked) {
      super();
      this.item = item;
      this.checked = checked;
    }

    public Object getItem() {
      return item;
    }

    public void setItem(final Object item) {
      this.item = item;
    }

    public boolean isChecked() {
      return checked;
    }

    public void setChecked(final boolean checked) {
      this.checked = checked;
    }

    public void invertChecking() {
      this.checked = !this.checked;
    }

  }

}

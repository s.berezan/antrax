package com.flamesgroup.antrax.control.swingwidgets.pagging;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.JBarButton;
import com.flamesgroup.antrax.control.swingwidgets.field.IntegerField;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class JPaginationPanel extends JPanel {

  private static final long serialVersionUID = 7749996935332489467L;

  private JBarButton buttonFirst;
  private JBarButton buttonPrev;
  private JBarButton buttonNext;
  private JBarButton buttonLast;
  private IntegerField fieldCurrentPage;
  private final List<PagerListener> listeners = new LinkedList<>();
  private int pages = 1;
  private int currentPage = 1;

  public JPaginationPanel() {
    super(new FlowLayout());
    initComponent();
  }

  //
  // Internal Interface
  //
  public interface PagerListener {
    void onPageChanged(int newPage);
  }

  public void addListener(final PagerListener l) {
    listeners.add(l);
  }

  public void removeListener(final PagerListener l) {
    listeners.remove(l);
  }

  public JPaginationPanel setPages(final int pages) {
    if (pages != this.pages) {
      currentPage = 1;
    }
    if (pages >= 1) {
      this.pages = pages;
    }
    fireFiltering(currentPage);
    return this;
  }

  public JPaginationPanel setCurrentPage(final int currentPage) {
    if (currentPage > 0 && currentPage <= pages) {
      this.currentPage = currentPage;
    }
    fireFiltering(currentPage);
    return this;
  }

  private void fireFiltering(final int newPage) {
    setEnabledButtons(true);
    currentPage = newPage;
    if (currentPage <= 1) {
      buttonFirst.setEnabled(false);
      buttonPrev.setEnabled(false);
      currentPage = 1;
    }
    if (currentPage >= pages) {
      buttonNext.setEnabled(false);
      buttonLast.setEnabled(false);
      currentPage = pages;
    }
    fieldCurrentPage.setContent(currentPage);

    for (PagerListener l : listeners) {
      l.onPageChanged(currentPage);
    }
  }

  private void initComponent() {
    buttonFirst = new JBarButton();
    buttonFirst.setIcon(IconPool.getShared("page-first.gif"));
    buttonFirst.setToolTipText("First page");
    buttonFirst.addActionListener(e -> fireFiltering(1));
    buttonFirst.setEnabled(false);

    buttonPrev = new JBarButton();
    buttonPrev.setIcon(IconPool.getShared("page-prev.gif"));
    buttonPrev.setToolTipText("Previous page");
    buttonPrev.addActionListener(e -> fireFiltering(currentPage - 1));
    buttonPrev.setEnabled(false);

    buttonNext = new JBarButton();
    buttonNext.setIcon(IconPool.getShared("page-next.gif"));
    buttonNext.setToolTipText("Next page");
    buttonNext.addActionListener(e -> fireFiltering(currentPage + 1));

    fieldCurrentPage = new IntegerField(1, Integer.MAX_VALUE);
    fieldCurrentPage.setContent(currentPage);
    fieldCurrentPage.setEditable(false);

    buttonLast = new JBarButton();
    buttonLast.setIcon(IconPool.getShared("page-last.gif"));
    buttonLast.setToolTipText("Last page");
    buttonLast.addActionListener(e -> fireFiltering(pages));

    add(buttonFirst);
    add(buttonPrev);
    add(fieldCurrentPage);
    add(buttonNext);
    add(buttonLast);
  }

  private void setEnabledButtons(final boolean enabled) {
    buttonPrev.setEnabled(enabled);
    buttonNext.setEnabled(enabled);
    buttonLast.setEnabled(enabled);
    buttonFirst.setEnabled(enabled);
  }

}

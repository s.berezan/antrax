package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import com.flamesgroup.antrax.control.swingwidgets.logicparser.LogicExpressionSyntaxException;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class JContextSearch extends JPanel implements ContextSearcher {

  private static final long serialVersionUID = -4194084250772607444L;

  private static final int DEFAULT_INSTANT_SEARCH_DELAY = 150;

  private int instantSearchDelay = 0;
  private JSearchField searchField;

  private JPopupMenu itemsMenu;
  private ActionListener menuActionListener;
  private final Map<SearchItem, String> itemMap = new HashMap<>();
  private Timer instantSearchTimer;
  private final ContextSearcher csDelegate = new ContextSearcherImpl();
  private final LogicSearchFilter filter = new LogicSearchFilter();

  public JContextSearch() {
    createGUIComponents();
    layoutGUIComponents();
    setupListeners();

    setInstantSearchDelay(DEFAULT_INSTANT_SEARCH_DELAY);
  }

  public void setText(final String text) {
    searchField.setText(text);
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    searchField.setEditable(enabled);
  }

  private JPopupMenu getItemsMenu() {
    if (itemsMenu == null) {
      itemsMenu = new JPopupMenu();
    }
    return itemsMenu;
  }

  private void createGUIComponents() {
    setOpaque(false);
    searchField = new JSearchField(getItemsMenu());
    searchField.setDragEnabled(true);
  }

  private void setupListeners() {
    menuActionListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (e.getSource() instanceof ContextSearchMenuItem) {
          ContextSearchMenuItem mi = (ContextSearchMenuItem) e.getSource();
          SearchItem item = mi.getSearchItem();
          setItemFilterEnabled(item, !isItemFilterEnabled(item));
          applyFilter();
        }
      }
    };

    searchField.addCancelListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onFilterUpdated();
      }
    });

    searchField.addUpdateListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onFilterUpdated();
      }
    });

    addContextSearcherListener(new ContextSearcherListener() {
      @Override
      public void itemRegistrationChanged(final SearchItem item, final boolean isRegistered) {
        rebuildMenu();
      }
    });
  }

  public void rebuildMenu() {
    getItemsMenu().removeAll();

    for (SearchItem item : getSearchItems()) {
      if (item.getSearchItemName() == null) {
        continue;
      }
      ContextSearchMenuItem menuItem = new ContextSearchMenuItem(item, isItemFilterEnabled(item));
      getItemsMenu().add(menuItem).addActionListener(menuActionListener);
    }
  }

  private void layoutGUIComponents() {
    GridBagLayout gbl = new GridBagLayout();
    setLayout(gbl);
    gbl.columnWidths = new int[] {200, 0};
    gbl.rowHeights = new int[] {0, 0, 0, 0};
    gbl.columnWeights = new double[] {1.0, 1.0E-4};
    gbl.rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

    add(searchField, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
  }

  /**
   * Get the instant search delay in milliseconds.
   *
   * @return the instant search delay in milliseconds
   * @see {@link #setInstantSearchDelay(int)}
   */
  public int getInstantSearchDelay() {
    return instantSearchDelay;
  }

  /**
   * Set the instant search delay in milliseconds. In
   * {@link SearchMode#INSTANT}, when the user changes the text, an action
   * event will be fired after the specified instant search delay.
   * <p>
   * It is recommended to use a instant search delay to avoid the firing of
   * unnecessary events. For example when the user replaces the whole text
   * with a different text the search fields underlying {@link Document}
   * typically fires 2 document events. The first one, because the old text is
   * removed and the second one because the new text is inserted. If the
   * instant search delay is 0, this would result in 2 action events being
   * fired. When a instant search delay is used, the first document event
   * typically is ignored, because the second one is fired before the delay is
   * over, which results in a correct behavior because only the last and only
   * relevant event will be delivered.
   *
   * @param instantSearchDelay
   */
  public void setInstantSearchDelay(final int instantSearchDelay) {
    firePropertyChange("instantSearchDelay", this.instantSearchDelay, this.instantSearchDelay = instantSearchDelay);
  }

  /**
   * Called when the search text changes. Starts the search field instant
   * search timer if the instant search delay is greater 0.
   */
  public void onFilterUpdated() {
    getInstantSearchTimer().stop();

    // only use timer when delay greater 0
    if (getInstantSearchDelay() > 0) {
      getInstantSearchTimer().setInitialDelay(getInstantSearchDelay());
      getInstantSearchTimer().start();
    } else {
      applyFilter();
    }
  }

  public void applyFilter() {
    String expression = searchField.getText();
    try {
      filter.setFilterExpression(expression);
    } catch (LogicExpressionSyntaxException e) {
      if (!expression.isEmpty()) {
        searchField.showErrorLine(e.getErrorPosition());
      }
    }
    csDelegate.applySearchFilter(filter);
  }

  /**
   * Returns the {@link Timer} used to delay the firing of action events in
   * instant search mode when the user enters text.
   * <p>
   * This timer calls {@link #applyFilter()}.
   *
   * @return the {@link Timer} used to delay the firing of action events
   */
  public Timer getInstantSearchTimer() {
    if (instantSearchTimer == null) {
      instantSearchTimer = new Timer(0, new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          applyFilter();
        }
      });
      instantSearchTimer.setRepeats(false);
    }
    return instantSearchTimer;
  }

  @Override
  public void addContextSearcherListener(final ContextSearcherListener l) {
    csDelegate.addContextSearcherListener(l);
  }

  @Override
  public void removeContextSearcherListener(final ContextSearcherListener l) {
    csDelegate.removeContextSearcherListener(l);
  }

  @Override
  public Collection<SearchItem> getSearchItems() {
    return csDelegate.getSearchItems();
  }

  @Override
  public void registerSearchItem(final SearchItem item, final String name) {

    csDelegate.registerSearchItem(item, name);
  }

  @Override
  public void registerSearchItem(final SearchItem item) {
    csDelegate.registerSearchItem(item);

  }

  @Override
  public void unregisterSearchItem(final SearchItem item) {
    csDelegate.unregisterSearchItem(item);
  }

  @Override
  public void applySearchFilter(final SearchFilter searchFilter) {
    csDelegate.applySearchFilter(searchFilter);
  }

  @Override
  public boolean isItemFilterEnabled(final SearchItem item) {
    return csDelegate.isItemFilterEnabled(item);
  }

  @Override
  public void setItemFilterEnabled(final SearchItem item, final boolean enabled) {
    csDelegate.setItemFilterEnabled(item, enabled);
  }

}

package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.badge.JBadge;
import com.flamesgroup.antrax.control.swingwidgets.badge.JBadge.JBadgeBuilder;
import com.flamesgroup.antrax.control.swingwidgets.sidebar.SidebarNode.SidebarNodeBuilder;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;

public class TestSidebarLaF {

  private static final SidebarNode[] nodes = new SidebarNode[3];
  private static final Map<SidebarNode, JBadge[]> badgesMap = new HashMap<>();

  private static final JBadge serversBadge = new JBadgeBuilder().build();
  private static SidebarNode serversNode;

  /**
   * 0xd50222
   * 0xfae221
   * 0x57b846
   *
   * @return
   */
  protected static DefaultTreeModel getTestTreeModel() {

    Icon serverIcon = IconPool.shared().get("gateway-started.png");
    Icon modemIcon = IconPool.shared().get("transmit.png");
    Icon simarrayIcon = IconPool.shared().get("bricks.png");
    Icon bekkiIcon = IconPool.shared().get("ipod_cast.png");
    Icon cemIcon = IconPool.shared().get("brick.png");

    SidebarNode root = new SidebarNodeBuilder().setName("AntraxSidebar").build();
    SidebarNode parent;

    parent = new SidebarNodeBuilder().setName("Servers").build();
    serversNode = parent;
    serversBadge.setToolTipText("Bekki tooltip");
    serversBadge.addMouseMotionListener(new MouseMotionListener() {

      @Override
      public void mouseDragged(final MouseEvent e) {
        System.out.println("mouseDragged");
      }

      @Override
      public void mouseMoved(final MouseEvent e) {
        System.out.println("mouseMoved");
      }

    });
    parent.addBadge(serversBadge);
    root.add(parent);

    for (int i = 0; i < nodes.length; i++) {
      nodes[i] = new SidebarNodeBuilder().setName("Antrax-" + i).build();
      nodes[i].setIcon(serverIcon);

      final int COUNT = i + 1;
      JBadge[] badges = new JBadge[COUNT];
      for (int b = 0; b < badges.length; b++) {
        badges[b] = new JBadgeBuilder().setText("0").setColor(new Color(0xd50222)).build();
        nodes[i].addBadge(badges[b]);
      }
      badgesMap.put(nodes[i], badges);
      parent.add(nodes[i]);
    }


    parent = new SidebarNodeBuilder().setName("Hardware").build();
    root.add(parent);
    SidebarNode subRoot = new SidebarNodeBuilder().setName("Mobile modems").build();
    subRoot.setIcon(modemIcon);
    parent.add(subRoot);
    for (int i = 0; i < 4; i++) {
      SidebarNode node = new SidebarNodeBuilder().setName("Bekki-1:" + (i + 1)).build();
      node.setIcon(bekkiIcon);
      subRoot.add(node);
    }

    subRoot = new SidebarNodeBuilder().setName("SIM-arrays").build();
    subRoot.setIcon(simarrayIcon);
    parent.add(subRoot);
    for (int i = 0; i < 4; i++) {
      SidebarNode node = new SidebarNodeBuilder().setName("CEM-" + (i + 1)).build();
      node.setIcon(cemIcon);
      subRoot.add(node);
    }
    return new DefaultTreeModel(root);
  }

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception ignored) {
    }

    JFrame frame = new JFrame("Sidebar look & feel");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final DefaultTreeModel model = getTestTreeModel();
    JSidebar sidebar = new JSidebar(model);
    ToolTipManager.sharedInstance().registerComponent(sidebar);
    JScrollPane pane = new JScrollPane(sidebar);
    frame.getContentPane().add(pane);
    frame.setSize(250, 400);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    new Thread(new Runnable() {
      private int counter = 50;

      @Override
      public void run() {
        while (true) {
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

              serversBadge.setText(String.valueOf(counter));
              model.nodeChanged(serversNode);

              for (int i = 0; i < nodes.length; i++) {
                JBadge[] badges = badgesMap.get(nodes[i]);
                for (int b = 0; b < badges.length; b++) {
                  badges[b].setText(String.valueOf(counter));
                }
                model.nodeChanged(nodes[i]);
              }
              counter++;
            }
          });

          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }

    }).start();
  }

}

package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class TestJTreeLaF {

  protected static DefaultTreeModel getTestTreeModel() {
    DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");

    for (int i = 0; i < 4; i++) {
      DefaultMutableTreeNode subRoot = new DefaultMutableTreeNode("Node " + i);
      root.add(subRoot);

      for (int j = 0; j < 3; j++) {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode("Child " + j);
        subRoot.add(child);
      }
    }
    return new DefaultTreeModel(root);
  }

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      //      JTree tree = new JTree(getTestTreeModel());
      //      tree.setLargeModel(true);
      //      tree.setRootVisible(false);
      //      tree.setShowsRootHandles(true);
      //      tree.putClientProperty(JTree.ROW_HEIGHT_PROPERTY, 20);
      //      tree.setFont(new Font("Helvetica", Font.BOLD, 11));
      //
      //      //tree.setUI(new SidebarUI());
      //      tree.setCellRenderer(new SidebarCellRenderer());

      JSidebar tree = new JSidebar(getTestTreeModel());


      JScrollPane pane = new JScrollPane(tree);

      JFrame frame = new JFrame("JTree look & feel");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.getContentPane().add(pane);
      frame.setSize(250, 400);
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

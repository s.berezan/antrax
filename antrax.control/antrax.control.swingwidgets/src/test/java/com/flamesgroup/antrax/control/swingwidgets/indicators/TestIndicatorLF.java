package com.flamesgroup.antrax.control.swingwidgets.indicators;

import java.awt.*;

import javax.swing.*;

public class TestIndicatorLF {

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      JFrame frame = new JFrame("Sidebar look & feel");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      OvalIndicator indicator = new OvalIndicator();
      indicator.setText("123");
      JPanel panel = new JPanel(new BorderLayout());
      panel.add(BorderLayout.CENTER, indicator);
      frame.getContentPane().add(panel);
      frame.setSize(250, 400);
      frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

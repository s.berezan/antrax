## User interface

### Entry to the system

Access to the system is available only after user authorization. The authorization window is displayed on the picture below.

![login_gui_panel](user-interface.assets/login_gui_panel.png)

### Elements of interface main window system control

GUI can be conditionally divided by zones:

![gui-full](user-interface.assets/gui-full.png)

#### 1. Transaction control buttons

| Button | Action |
| -------- | -------- |
| ![ob_sv_button-edit](user-interface.assets/ob_sv_button-edit.png) | opening of transaction (enter the edit setting system mode) |
| ![ob_sv_apply](user-interface.assets/ob_sv_apply.png) | the application of updates in system settings within given transaction following its closing |
| ![ob_sv_button-undo](user-interface.assets/ob_sv_button-undo.png) |cancellation of updates in system settings within given transaction following its closing |

Each time period the transaction can be opened only for one GUI client.  Trying to open transaction for one GUI client while another client is already in editing mode will result in error massage issued by the system :

![transaction_error](user-interface.assets/transaction_error.png)

#### 2. Configuration sections
Sections for configurations:

**VOICE SERVERS** - control of GSM channels

**SIM SERVERS** - control of SIM cards

**SESSIONS** - session history of Antrax system users

**STATISTIC** - voice channel statistics

  * Voice Servers - statistics on voice channels 
  * VoIP AntiSpam - statistics of traffic filtration

**GSM-view** - Monitoring and management of GSM-covering state 

**REPORTS** -  report on SIM card and SIM group performance

  * CDR - report contains the information about all calls in Antrax system
  * Sim History - report contains the event history of selected SIM card
  * Sim Groups Report - report on each SIM group performance

**CONFIGURATION** - section of server, channel, hardware module setting

  * Servers - server control settings
  * GSM groups - section for GSM group settings
  * SIM groups - section for SIM group settings
  * Scripts - section for uploading the scripts that are responsible for SIM groups control
  * VoIP AntiSpam - section for traffic filtration settings
  * Prefix mark lists - section lists the settings marking mechanism

**UTILITY** - utilities section

  * Registry - section of work with registry

#### 3. Working area
Basic working area - it has its own type for each selected section.

#### 4. Counters
Counters display different figures depending on selected section.

#### 5. Status bar

The status bar displays the status of information updates in working area.

| Indicator | Status |
| -------- | -------- |
| ![timeuot_before_synchronization](user-interface.assets/timeuot_before_synchronization.png) | Status of graphic timer before synchronization (synchronization of GUI client every 5 minutes.) |
| ![synchronization](user-interface.assets/synchronization.png) | Status of graphic timer on active synchronization |

If it seems to you, that information on the screen does not meet the realities,please, pay your attention to status bar. Possibly GUI is not able to receive necessary information because of poor internet connection or troubles with network connection. In this case the status indicator will stand still.

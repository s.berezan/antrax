## UTILITY

## Register 

Register - is a virtual data warehouse system where data is stored in a **path -> value**( one path can contain several values). Main aim of registry is to allow centralized SIM group work with necessary data (for example vouchers, user can add/remove/replace, and scripts can use it for their logic execution). Also, antrax.ui-gui-client personal configurations  for each client  are stored in registry, it allows user to have personal interface configurations after each connection from  every computer.

### Variants of using storage values

Connections path->value can be used for storing any storage data. Today, several methods of utilization can be highlighted:

  - **Storing of personal configurations**. Today, for each user, who has loaded scripts, path to folder from which scripts have been loaded is stored. So if user wants to reload scripts again, he will be positioned by stored script folder.
  - **Storing data for the scripts**. Some of scripts use registry to store necessary data, reconfiguration.
    *  Pay script uses vouchers for balance top up: first it checks values by path vauchers.current_operator.amount.new, in the case if attempt occurred with error, this values will be replaced with vauchers.current_operator.amount.failed1. In case the failure repeats while second topping up , voucher's value will be replaced with path with posfix .failed2 and during the third  failure .failed3 . In the case of the forth unsuccessful attempt voucher's values will be replaced with path with posfix .bad.  If topping up is successful, values of used vouchers will be replaced with path with posfix .used.
    * Scripts USSD, CheckBill use antrax-scripts.payment.ussd path.
    * Pay uses  antrax-scripts.payment.regexes,  in which variants of USSD response are stored.
  - **Storing IMEI** **values** imei.tacfac stores  IMEI values.

### User interface

To move to registry manage panel  you need to choose element **Registry** in left menu of **UTILITY****.** Here is this panel:

![main](utility.assets/main.png)

4 buttons are used to control the data of the registry:

| **button** | **value**|
| -------- | -------- |
| ![add](utility.assets/add.png) | add  **path** -> **value ** |
| ![del](utility.assets/del.png) | remove  **path** -> **value ** |
| ![copy](utility.assets/copy.png) | add the list of current path values to another path |
| ![import_button](utility.assets/import_button.png) | import **path** -> **value list ** |
| ![export_button](utility.assets/export_button.png) | export **path** values to CVS file |

### Adding

In order to add the expression **path** -> **value **:

![adding](utility.assets/adding.png)

Adding of expression **path** -> **value list**:

![importing](utility.assets/importing.png)

As a result, the list of values will be added on testpath . It is convenient to use such adding when considerable amount of data is added, similarly to refill vouchers.

### Removal

To remove the expression **path** -> **value **:

![removing](utility.assets/removing.png)

## Download audio

Download audio allows to select the necessary server and download audio files from it.

![download-audio](utility.assets/download_audio.png)

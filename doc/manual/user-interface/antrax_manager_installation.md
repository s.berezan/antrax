## Antrax Manager installation

### Antrax Manager installation for Linux/Windows

To install Antrax Manager GUI client it is required to click the link of Antrax Manager network setting.

Antrax Manager network setting can be found on address: http://server_ip_or_dns:8000/.
Server_IP_or_DNS - IP address or server domain name, where you will find CS server installed. Furthermore, to start the process of installation you need to push Install button.

![gui_browser](antrax_manager_installation.assets/gui_browser.png)

To run Antrax Manager Java Standard Edition (Java SE)package is required to be installed. In case of error during the installation of Antrax Manager, make sure that you operate the latest version of Java SE. 

You can download and install the latest version of Java SE on official website [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
Java Web Start is used for installation and updating of Antrax Manager.

Before you launch Antrax Manager, Java software can display dialogue box informing that digital signature of application can not be confirmed .

![javaws_security](antrax_manager_installation.assets/javaws_security.png)

To install Antrax Manager, URL http://server_ip_or_dns:8000 is required to be added to the exception in Java (Java Control Panel).

In Linux operating systems Java control panel can be called from console by performing the command:
```
javaws -viewer
```

In Windows OS Java control panel can be found following the path Control panel -> Java

After launching the Java control panel you will see the following:

![javaws_view](antrax_manager_installation.assets/javaws_view.png)

On Java control panel you need to chose **Security** tab and push **Edit Site List** (change the website list).

![javaws_security1](antrax_manager_installation.assets/javaws_security1.png)

To add the site to the exception list you need to push **Add** button. Add URL http://server_ip_or_dns:8000 to the exception list and push **ADD**.

![javaws_security2](antrax_manager_installation.assets/javaws_security2.png)

Afterwards you will see the following:

![javaws_security3](antrax_manager_installation.assets/javaws_security3.png)

Push **Continue**, and leave Java control panel by pushing  **OK**.

Repeatedly push an **«Install»** button in browser , when the dialogue box appears, push **Run** button to install the Antrax Manager. Mark the check box **I accept the risk and want to run this application** and push the **Run** button.

![security_check](antrax_manager_installation.assets/security_check.png)

Furthermore you will see the window, where you will have to insert login and password.

![login_form](antrax_manager_installation.assets/login_form.png)

By default:
```
login - admin
password - qweqwe
```

It is preferable to change the password, having asked tech-support managers about it.

After you insert login and password, you will be able to operate GUI, which looks as follows:

![gui](antrax_manager_installation.assets/gui.png)

### Updates

The updates will be uploaded automatically as soon as they are available. The user will be able to chose, whether to install the update or run already installed version. But it is advisable to remove Antrax Manager after update is issued and install it again.

### Removal

 Antrax Manager can be removed  with the help of Java Control Panel.

In Linux OS Java control panel can be called from console by performing the following command:
```
javaws -viewer
```

In Windows OS Java control panel can be found following the path Control Panel -> Java

After launching Java control panel you will see the following:

![javaws_view](antrax_manager_installation.assets/javaws_view.png)

Now push the  **View** button. You will see all Java apps installed. Choose **AntraxManager@antrax-test** application and push the red **"cross"**.

![javaws_delete](antrax_manager_installation.assets/javaws_delete.png)

### Antrax Manager installation for MacOS

Before installing the GUI-client, you must install Java for your MacOS. Download and install Java. You can download java installation file via the link below:

http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html

After installing Java, you need to start the javaws service, which is responsible for running the GUI client on your PC, to run javaws. You need to run terminal and run the command:

    javaws -viewer

In the new window, you need to follow to the Web Settings tab and select the Exception Site List sub-tab:

![Web Settings](Install-GUI-client-on-MacOS.assets/Mac.png)

In this tab, you need to add a link to your GUI client:
In this case:

    http://presentation:8000

Web page will look like this:

![Web page](Install-GUI-client-on-MacOS.assets/add_link.png)

Then click Add and after OK.
Now download the installation file by clicking on the Install button:

![Download from web](Install-GUI-client-on-MacOS.assets/download_from_web.png)

Folder, select the file antraxmanager.jnlp click on its mouse button, select Open with → Java Web Start:

![Open GUI](Install-GUI-client-on-MacOS.assets/open_gui.png)

After this, the system will start the download and installation of the GUI client and after installation will issue the following message:

![Launch gui](Install-GUI-client-on-MacOS.assets/launch gui.png)

In this window, you need to install a check-box next to «I accept the risk and want to run this application» and after clicking the Run button, the GUI client will then prompt User and Password:

![Enter Password](Install-GUI-client-on-MacOS.assets/enter_pass.png)

By default, User and Password come with these values:

    User - admin
    Password - qweqwe

The window of the launched GUI client:

![Launched GUI](Install-GUI-client-on-MacOS.assets/launched_gui.png)



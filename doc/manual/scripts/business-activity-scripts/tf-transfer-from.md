## TF Transfer from

This script is primary in the group TF scripts group. All scripts with TF prefix can only work in interaction with each other and are not intended to be used separately. This group realizes the possibility of money transfer from the given card to any other card.

The script starts the search for card-receiver in order to transfer money.

![tf_transfer_from](tf-transfer-from.assets/tf_transfer_from.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event which activates the script |
|  action | the name of current action realized in TF Find dst card action script ( do not change this option, if you are unsure of your actions) |
|  lock on fail | block the card in case of error |

 


## limit month call duration

Given script is designed for limitation of SIM card activity for the period of month in the system. Drop call if it exceeds the specified duration

![limit_call_dur_by_month](limit_call_dur_by_month.assets/limit_call_dur_by_month.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event generated when **drop limit** is reached |
|  duration limit | limit call duration per month |
|  first day of the month | set first day of month |
|  tariffing | tariffing per selected principle |
|  tariffing first minute| tariffing only first minute, after tariffing per second|
|  drop limit | quantity of rejected calls on **duration limit** |


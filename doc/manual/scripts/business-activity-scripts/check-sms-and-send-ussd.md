## check SMS and send ussd

This script enables to process the incoming SMS and execute USSD request.

![check-sms-and-send-ussd](check-sms-and-send-ussd.assets/check-sms-and-send-ussd.png)

### Parameters

| name| description|
| -------- | -------- |
| ussd for send code | pattern for send USSD. $1 marked where paste text from SMS |
| code regex | regex for cut some part from SMS for then send it via USSD |
| sms timeout | time for waiting for incoming SMS after receive event |
| event | event for start executing this script |


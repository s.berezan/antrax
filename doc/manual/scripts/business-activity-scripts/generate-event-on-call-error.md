## generate event on call error

Given script is designed for any action performance with regard to particular codes of calls ring off.

![g_e_on_call_err](generate-event-on-call-error.assets/g_e_on_call_err.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event, which is generated on amount of calls stated in **call errors limit**, with a ring off codes, stated in **call error cause** |
|  **call errors limit** | quantity of calls, with particular ring off codes, after which **event** must be performed |
|  **call error cause** | ring off code |

There is a possibility to set up the script with several rings off codes.

![g_e_on_call_err_add](generate-event-on-call-error.assets/g_e_on_call_err_add.png)

## limit call duration

Given script is designed for limiting the duration of one call. When the particular call duration is reached - call rejecting is performed.

![limit_call_dur](limit-call-duration.assets/limit_call_dur.png)

### Parameters

| name| description|
| -------- | -------- |
|  **duration limit** | call duration |


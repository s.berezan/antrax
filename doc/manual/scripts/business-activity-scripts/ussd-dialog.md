## USSD dialogue

Allows to sends USSD request, analyzes USSD response and also sends sample requests, carrying out USSD dialogue.

The main direction of using USSD-dialogue — is a possibility to receive an additional information from applications and applications' management.

![ussd_dialog](ussd-dialog.assets/ussd_dialog.png)

### Parameters

| name| description|
| -------- | -------- |
|  USSD request | number of request |
|  event on fail | event on failure |
|  event on true | event on successful performance |
|  event | event essential for script activation |
|  response patterns list | regular expression to analyze the requests and generate USSD response|

### Configuration of responce patterns list

![ussd_dialog_pattern](ussd-dialog.assets/ussd_dialog_pattern.png)

### Parameters

| name| description|
| -------- | -------- |
|  Pattern | regular expression to check USSD response |
|  Answer | if there is a need to transfer request to continue USSD dialogue |
|  Answer | a request transferred to continue USSD dialogue |

## lock on event

Given event locks the card.

![lock_on_event](lock-on-event.assets/lock_on_event.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event generated for script activation |

 


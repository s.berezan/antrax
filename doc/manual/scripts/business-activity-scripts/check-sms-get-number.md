## check SMS get number

The script checks incoming SMS texts. If the concurrence on the sample is observed, the script chooses the number of SMSes and directs it to the database.
Furthermore, the number appears in card Phone number field on SIM CHANNELS tab.

**SMS** (Short Message Service - The service of short messages) - the technique, which allows to perform the receipt and sending of short text messages. As a rule, they are delivered within several seconds.

![checknumbersms](check-sms-get-number.assets/checknumbersms.png)

### Parameters

| name | description|
| -------- | -------- |
|  event on fail | event on failure of script performance |
|  SMS response pattern | regular expression to receive a number |
|  event on success | event on successful performance |
|  event | initial event essential for script activation |
|  sms timeout | timeout for waiting incoming sms |

 


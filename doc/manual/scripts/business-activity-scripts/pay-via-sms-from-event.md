## pay via SMS from event

Pay phoneNumber via SMS from event, then analyzes incoming SMS and send SMS with code to the same number

![g_e_pay](pay-via-sms-from-event.assets/pay-via-sms-from-event.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | initial event essential to activate the script with phone number for pay and amount for pay |
|  **sms timeout** | time for waiting for incoming SMS after receive event |
|  **incoming sms text regex** | pattern check incoming SMS must be with the group. The first group used for cut code, which will be sent to the same number |
|  **sms number** | number for send SMS |

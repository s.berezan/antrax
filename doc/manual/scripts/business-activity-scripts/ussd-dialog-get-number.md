## USSD dialogue get number

The script performs USSD-request and analyzes USSD-response. In case of pattern match, the script chooses the number  from USSD-response and puts it in database.
Therefore the number appears in Phone number square of the card in SIM CHANNELS tab.

Enables to send USSD messages, processes USSD responses according to pattern, sends requests on stated samples, carrying out  USSD dialogue.

If pattern match occurs, the script selects USSD-response and puts it in database.

![ussd_dialog_get_number](ussd-dialog-get-number.assets/ussd_dialog_get_number.png)

### Parameters

| name| description|
| -------- | -------- |
|  USSD request | number of request |
|  event on fail | event on failure |
|  event on true | event on successful performance |
|  event | event essential for script activation |
|  response patterns list | regular expression to analyze requests and generate USSD responses |

### Настройка responce patterns list

![ussd_dialog_pattern](ussd-dialog-get-number.assets/ussd_dialog_pattern.png)

### Parameters

| name| description|
| -------- | -------- |
|  Pattern | regular expression to check USSD response |
|  Answer | if there is a need to transfer request to continue USSD dialogue |
|  Answer | request transferred to continue USSD dialogue |

## Execute action

Given script is designed for the performance of any script action. The actions are described in Action Provider Scripts.

![execute_action](execute-action.assets/execute_action.jpg)

### Parameters

| name| description|
| -------- | -------- |
|  **event  ** | initial event essential for execute action |
|  **action name** | the name of event |
|  **attempts count** | quantity of attempts on failure |
|  **action timeout** | maximum waitinig time for action performance |

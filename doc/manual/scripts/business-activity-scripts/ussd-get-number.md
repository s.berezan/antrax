## USSD get number

The script performs USSD-request and analyzes USSD-response. In case of pattern match, the script chooses the number  from USSD-response and puts it in database.
Therefore the number appears in Phone number square of the card in SIM CHANNELS tab.

![ussd_get_number](ussd-get-number.assets/ussd_get_number.png)

### Parameters

| name| description|
| -------- | -------- |
|  USSD request | number of request |
|  USSD response pattern | regular expression to check  USSD response and receive number |
|  event on fail | event essential for successful script performance |
|  event | event essential for script activation |
|  USSD replace response pattern | pattern used to change the number inserted in DB |

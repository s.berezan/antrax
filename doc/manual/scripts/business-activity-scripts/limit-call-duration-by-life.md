## limit call duration by life

Given script is designed for limitation of SIM card activity for the period of its life in the system.

![limit_call_dur_by_life](limit-call-duration-by-life.assets/limit_call_dur_by_life.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event generated when **drop limit** is reached |
|  **duration limit** | call duration |
|  **drop limit** | quantity of rejected calls on **duration limit** |
|  **block of seconds** | proximity of call duration |


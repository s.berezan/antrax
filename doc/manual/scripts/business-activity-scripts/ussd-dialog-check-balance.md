## USSD dialog check balance

USSD dialog, waits for last response with balance and generates events according to the value of balance

![ussd_dialog_check_balance](ussd-dialog-check-balance.assets/ussd-dialog-check-balance.png)

### Parameters

| name| description|
| -------- | -------- |
|  event on more | event on amount of money in the balance that more than minimum |
|  event on less | event on amount of money in the balance that less than minimum |
|  USSD request | number of request |
|  event on fail | event on failure |
|  event on true | event on successful performance |
|  event | event essential for script activation |
|  minimum amount of money | required amount of money in the balance |
|  response patterns list | regular expression to analyze the requests and generate USSD response|

### Configuration of responce patterns list

![ussd_dialog_check_balance_pattern](ussd-dialog.assets/ussd_dialog_pattern.png)

### Parameters

| name| description|
| -------- | -------- |
|  Pattern | regular expression to check USSD response |
|  Answer | if there is a need to transfer request to continue USSD dialogue |
|  Answer | a request transferred to continue USSD dialogue |

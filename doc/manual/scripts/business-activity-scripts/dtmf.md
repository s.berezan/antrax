## DTMF

**DTMF** (Dual-Tone Multi-Frequency) -- a signal used to dial the number as well as various interactive systems, for instance, voice response (IVR - Interactive Voice Response).

Basic script direction: navigation in voice menu (IVR). The script calls the particular number, initiates the pressing of number buttons. There is a minimal time interval between "pressings".

![dtmf](dtmf.assets/dtmf.png)

### Parameters

| name| description|
| -------- | -------- |
| event|event essential for script activation|
| attempts count | quantity of repetitions on attempt failure|
| DTMF scriplet | navigation data and number|

 

### DTMF scriplet set-up

![dtmf_scriplet](dtmf.assets/dtmf_scriplet.png)

In fact, this is a line, which can be defined as follows:

number;time;figure;time;figure and so on.

The pattern which goes before the first semicolon  (;) - is a number. Then goes alternation of dial time before pressing the button.\\ The time is stated in milliseconds. There are 1000 milliseconds in one second, therefore time 40000 stated in the sample is equal to 40 seconds.

 


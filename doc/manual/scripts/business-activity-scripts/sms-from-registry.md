## SMS from registry

Sends SMS onto generated number. In contrast to the script SMS, this script allows you to send a message with prepared and saved text from the registry.

**SMS** (Short Message Service) - technology, which allows performing receiving and sending of short messages. Usually, this messages are delivered in several seconds.

You can send messages onto offline or switched off a phone. As soon as addressee becomes available in the network, he will receive the message. You can send a message to an addressee, who is talking at that moment.

![sms_from_registry](sms-from-registry.assets/sms_from_registry.png)

### Parameters

| name| description|
| -------- | -------- |
|  attempts account | number of attempts in case of unsuccessful performance |
|  event on failure | this event will be generated after failed send SMS |
|  event on success | this event will be generated after succeed send SMS |
|  event | event for script activation |
|  skip errors | if set up, then all errors on sending SMS will be ignored |
|  interval between attempts | interval of time between attempts in case of error |
|  number pattern | regular expression, which defines  numbers where sms was sent |
|  lock on fail | need of locking card in case of unsuccessful performance |
|  path in registry | path to registry,where texts of the message are stated | 
|  sms count | amount of sms |
|  interval between sms | interval of time between successfully sent sms |

 


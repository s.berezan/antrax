## TF Start transfer

The script is an intermediate part of a TF group scripts and can not be used separately from the group. Script initiates the process of money transferring.

![tf_start_transfer](tf-start-transfer.assets/tf_start_transfer.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | initial event to activate script (don't change this option if you are unsure of your actions) |
|  action | name of the initial event which is implemented in the TF Transfer action script (don't change this option if you are unsure of your actions) |

 


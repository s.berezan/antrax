## generate event on defined duration calls

Given script is designed for any event performance on obtaining a number of calls with particular duration by the card.

![define_duration_calls](generate-event-on-defined-duration-calls.assets/define_duration_calls.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event which requires launching on detection of call forwarding |
|  **duration bounds** | duration of followed calls |
|  **maximum calls** | a number of calls when reached the **event** is performed |
|  **count sequence calls** | take into account the sequence of calls with particular duration |
|  **use incoming calls** | given checkmark calculates incoming calls with specified duration to the card |
|  **use outgoing calls** | given checkmark calculates outgoing calls with specified duration from the card  |

**If both checkmarks are switched on, the call calculation with the specified duration will be conducted with incoming calls to the card as well as outgoing calls from the card.**


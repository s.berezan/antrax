## generate event on registration

Given script is designed for any event performance after card registration in GSM network.

![g_e_on_reg](generate-event-on-registration.assets/g_e_on_reg.png)

### Parameters

| name | description|
| -------- | -------- |
|  **period** |regularity of **event** performance |
|  **event** | event which requires launching after card registration in GSM network  |
|  **count limit** | amount of **event** performance repetition for **period** |

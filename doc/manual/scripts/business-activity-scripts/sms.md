## SMS

Sends SMS onto generated number.

**SMS** (Short Message Service) - technology, which allows to perform receiving and sending of short messages. Usually, this messages are delivered in several seconds.

You can send messages onto  offline or switched off phone. As soon as addressee becomes available in the network, he will receive the message. You can send a message to an addressee, who is talking at that moment.

![sms](sms.assets/sms.png)

### Parameters

| name| description|
| -------- | -------- |
|  attempts account | quantity of attempts in the case of unsuccessful performance |
|  event on failure | this event will be generated after failed send SMS |
|  event on success | this event will be generated after succeed send SMS |
|  event | event to activate the script |
|  skip errors | if set up, then all errors on sending SMS will be ignored |
|  interval between attempts | interval of time between attempts in the case of error |
|  number pattern | regular expression, which defines  numbers where sms was sent |
|  path in registry | path to the registry key, which contains numbers for call. Number take randomly from registry. If registry empty, will use number pattern |
|  lock on fail | need of locking card in case of unsuccessful performance |
|  SMS count | number of SMS sending in one script execution |
|  empty text | to send empty sms |
|  interval between sms | interval of time between successfully sent sms |



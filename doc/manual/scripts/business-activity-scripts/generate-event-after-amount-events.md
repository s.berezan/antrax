## generate event after amount events

Given script is designed for the generate event after a particular quantity of events.

![generate-event-after-amount-events](generate-event-after-amount-events.assets/generate-event-after-call.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | the event which will be counting |
|  amount events per day | an amount of events which will be count. This filed allow set a range. At start of count, script generate random value from this range |
|  reset count a day | flag for reset count of events at start of day(at 00:00) |
|  generateEvent | generate this event after the amount of events specified by this script |

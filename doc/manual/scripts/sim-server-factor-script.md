## Sim Server Factor Script

SIM Server Factor Script is responsible for chosing  SIM card for Voice server. For every SIM card script defines numeric value (factor) according to which SIM cards are sorted on server. When Voice server addresses SIM server for new card, the card with bigger factor is chosen. That is, the more the factor, the earlier SIM card will go to Voice server.

![menu](sim-server-factor-script.assets/menu.png)

### Constant

![constant](sim-server-factor-script.assets/constant.png)

| Name | Description|
| -------- | -------- |
|  factor | priority of choosing sim card  from particular sim group by gsm channel |
|  sort order | sorting order: ascending or descending |
|  priority | selection of sim group priority  |

### Statistic based factor

![statistic_based_factor](sim-server-factor-script.assets/statistic_based_factor.png)

| Name| Description|
| -------- | -------- |
| statistic value | characteristic which is responsible for sorting |
| sort order | sorting order: ascending or descending |
| priority | selection of sim group priority |

### Statistic based factor per period

![statistic_based_factor_per_period](sim-server-factor-script.assets/statistic_based_factor_per_period.png)

| Name| Description|
| -------- | -------- |
| time period| period of script updating (unit of measurement: day) |
| statistic value | characteristic responsible for sorting |
| sort order | the order of sorting: ascending or descending |
| priority | selection of sim group priority |

### Parameters of Sim Server Factor Script

| Name of parameter| Types of parameter| Description|
| -------- | -------- | -------- |
| factor | 1-... | the priority of sim card selection from particular sim group by gsm channel |
| time period | 1-... | period of script updating (unit of measurement: day) |
| statistic value | called duration <br> successful calls count <br> total calls count <br> total sms count | sorting on call duration <br> sorting on quantity of successful calls <br> sorting on overall quantity of calls <br> sorting on overall quantity of SMS |
| sort order | ascending <br> descending | ascending sorting order <br>  descending sorting order|
|  priority | neutral(0) <br> 1-9 <br> agressive(10) |  minimal sim group priority <br> range of sim group priority selection <br> maximal sim group priority |

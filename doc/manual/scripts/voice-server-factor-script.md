## Voice Server Factor Script

Voice Server Factor Script is responsible for voice channel selection for call. The script defines numeric value (factor) for each SIM card according to which channels are sorted on server. Routing a new call, channel which has bigger factor is chosen. That is, the more the factor, the earlier the channel with SIM card will make a call.

![vs_menu](voice-server-factor-script.assets/vs_menu.png)

### Constant

![constant](voice-server-factor-script.assets/vs_constant.png)

| Name| Description|
| -------- | -------- |
|  factor | the priority of selecting SIM card from definite SIM group by GSM channel |
|  sort order | sorting order: ascending or descending |
|  priority | selection of GSM channel value with registered SIM card on it |

### Statistic based factor

![vs_statistic_based_factor](voice-server-factor-script.assets/vs_statistic_based_factor.png)

| Name| Description|
| -------- | -------- |
|  statistic value | characteristic which is responsible for sorting |
|  sort order | sorting order: ascending or descending |
|  priority | selection of GSM channel value with registered SIM card on it |

### Statistic based factor per period

![vs_statistic_based_factor_per_period](voice-server-factor-script.assets/vs_statistic_based_factor_per_period.png)

| Name| Description|
| -------- | -------- |
| time period|periodic script reset (one day period)|
|  statistic value | characteristic responsible for sorting |
|  sort order | sorting order: ascending or descending |
|  priority | selection of GSM channel value with registered SIM card on it |

### Parameters Voice Server Factor Script

| Parameter name| Parameter types| Description|
| -------- | -------- | -------- |
|  factor |1-...|priority of SIM card selection from definite SIM group by gsm channel |
| time period|1-...| the period of script updating (a day is unit of measurement) |
|  statistic value |called duration <br> successful calls count <br> total calls count <br> total sms count| sorting on call duration <br> sorting on amount of successful calls <br> sorting on overall amount of calls <br> sorting on overall amount of  SMS messages |
|  sort order |ascending <br> descending| ascending sorting order <br> descending sorting order |
|  priority|neutral(0) <br> 1-9 <br> agressive(10)| minimal value of GSM channel with registered SIM card on it <br> range of GSM channel value selection with registered SIM card on it <br> maximal value of GSM channel with registered SIM card on it |

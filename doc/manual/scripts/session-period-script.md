## Session Period Script

Session - is a time interval, during which SIM card is located on one of Voice servers. Session starts, when Voice server receives SIM card from SIM server. After session termination SIM card returns back to SIM server.

Session and activity concepts do not intersect, for instance, in the frames of one session several activities are possible or vice verse one activity can last during several sessions (in this case activity will discontinue when the session is over, as the card loses registration in GSM during transition between Voice servers).

Session Period Script - the script which manages SIM card session. Given script can forbid session (in this case cards, which are on Voice server, leave it) or permit session (free cards on  SIM server can be directed to Voice server). After leaving SIM card will return back to any Voice server. Session Period Script is performed on SIM server.

![sps_menu](session-period-script.assets/sps_menu.png)

### Infinite

It sets up the unlimited regime of SIM-card usage for quantity of attempts or time framework.

![sps_infinite](session-period-script.assets/sps_infinite.png)

### Limit call attempts

It limits SIM-card activity by stated quantity of attempts. By default the quantity is chosen randomly from the range of 10 to 15 attempts.

![sps_limit_call_attempts](session-period-script.assets/sps_limit_call_attempts.png)

### Complex activity script

It represents a set of scrip-lets - elementary scripts, that are able to permit or forbid the activity depending on any parameter value.

![sps_complex_activity_script](session-period-script.assets/sps_complex_activity_script.png)

By click the selection list of limit parameters will open on **(activity timeout by event)**:

![menu_aps_sps](session-period-script.assets/menu_aps_sps.png)

The list enables to chose several limit parameters. Herein parameters will be united by logical operand  **and** or **or**.
Logical operand **and** means, that in order to perform scrip-let both conditions should be performed. Logical operand **or** means, that at least one of conditions must be performed.


|  Script name |  Description|
| -------- | -------- |
| Break activity timeout passed  | Termination of activity after particular period of time |
| Allow activity after call duration passed | Permit the activity after stated duration of call |
| Allow activity in desired period | Permit the activity at particular time of the day |
| Limit calls per day | Indication of all calls'quantity limit during the day |
| Limit call attempts per day | Indication call attempts' quantity limit during the day |
| Allow activity after 24h passed | Indication of pause per day before activity |
| Limit sms count per activity | Indication of sent SMS limit during activity |
| Limit call duration per month | Indication of overall call duration limit during the month |
| Limit call duration per day | Indication of overall call duration limit during the day |
| Limit call attempts per activity | Indication of call attempts limit during activity |
| Limit sms count per day | Indication of sent SMS limit during the day |
| Limit call duration per activity | Indication of overall call duration limit during activity |
| Separate activity with delay | Pause indication before activity |
| Limit successfull calls per activity | Indication of successful calls limit during activity |

### Period Scripts

#### Break activity timeout passed

![break_activity_timeout_passed](period-script.assets/break_activity_timeout_passed.png)

#### Allow activity after call duration passed

![allow_activity_after_duration_passed](period-script.assets/allow_activity_after_duration_passed.png)

#### Allow activity in desired period

![allow_activity_in_desired_period](period-script.assets/allow_activity_in_desired_period.png)

#### Limit calls per day

![limit_calls_per_day](period-script.assets/limit_calls_per_day.png)

#### Limit call attempts per day

![limit_call_attempts_per_day](period-script.assets/limit_call_attempts_per_day.png)

#### Allow activity after24h passed

![allow_activity_after24h_passed](period-script.assets/allow_activity_after24h_passed.png)

#### Limit sms count per activity

![limit_sms_count_per_activity](period-script.assets/limit_sms_count_per_activity.png)

#### Limit call duration per month

![limit_call_duration_per_month](period-script.assets/limit_call_duration_per_month.png)

#### Activity timeout by event

![activity_timeout_by_event](period-script.assets/activity_timeout_by_event.png)

#### Limit call duration per day

![limit_call_duration_per_day](period-script.assets/limit_call_duration_per_day.png)

#### Limit call attempts per activity

![limit_call_attempts_per_activity](period-script.assets/limit_call_attempts_per_activity.png)

#### Limit sms count per day

![limit_sms_count_per_day](period-script.assets/limit_sms_count_per_day.png)

#### Limit call duration per activity

![limit_call_duration_per_activity](period-script.assets/limit_call_duration_per_activity.png)

#### Separate activity with delay

![separate_activity_with_delay](period-script.assets/separate_activity_with_delay.png)

#### Limit successfull calls per activity

![limit_successfull_calls_per_activity](period-script.assets/limit_successfull_calls_per_activity.png)

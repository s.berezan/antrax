## Call Action

The script enables one card to "ask" randomly chosen card to call it back.Given script calls to the card of N group, where the request for askForCall or ExecuteAction was performed.

![call_action](call-action.assets/call_action.png)

### Parameters

| name| description|
| -------- | -------- |
|  number pattern | regular expression which defines the number.\\ Used in the connective with number replace pattern |
|  number replace pattern | regular expression which defines part of number |
|  ask for call back | request for the call back to the card which generates a call by this script |
|  ask for call back event | event for the callback request |
|  event | event generated after action fulfillment |
|  make calls inside one group | if selected, this action will be fulfilled by card of the same sim group as the card which requested for that action |
|  action name |name of the initial action  |
|  event on success |generates the event in case of successful call performance  |
|  event on failure | generates the event in case of failure |
|  ignore incoming calls | if the option is chosen, the script will be fulfilled even in case of active inbound calls|
|  attempts count | quantity of attempts in case of failure |
|  min active duration | minimal call duration |
|  max active duration | limitation of call duration |
|  max attempt duration | maximal dial time wait |
|  skip errors | the option defines the need of errors being skipped or not |
|  interval between calls | time interval between sequential calls |
|  calls count | amount of calls |
|  require active | on activation of option, the call will be successful if the subscriber answers the call |

### Configuring number pattern and number replace pattern fields

These settings are used in the case of number conversion is needed. For example, remove the country code from the number, special characters.
number pattern usually is a regular expression which consists of two parts: the part which needs to be removed and a part which needs to be used.
For example : \ +5021(.*)

Dot-asterisk in braces defines the number with any number of digits or at least one digit. Special characters need to be screened by backslash “\”.

number replace pattern usually set to  $1 - in this case, the part in braces will be chosen.
Thus any number which begins with +5021  will be converted. For example, +50219873452 will be changed to 9873452.

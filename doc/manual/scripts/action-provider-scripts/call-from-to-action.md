## Call from - to action

This script is a part of the scripts chain for calls between cards. The script is called only from a business script ask for call back.
Script makes a call from specified source number to the given number-receiver.
Numbers of source and receiver are set on the execution of this script through the script ask for call back.

![call_from_to](call-from-to-action.assets/call_from_to.png)

### Parameters

| Name |  Description|
| -------- | -------- |
|  event | event generated after action fulfillment |
|  action name |name of the initial action  |
|  number pattern | regular expression which defines the number.\\ Used in the connective with number replace pattern |
|  number replace pattern | regular expression which defines part of number |
|  ignore incoming calls | if the option is chosen, the script will be fulfilled even in case of active inbound calls|
|  event on success |generates the event in case of successful call performance  |
|  event on failure | generates the event in case of failure |
|  attempts count | quantity of attempts in case of failure |
|  min active duration | minimal call duration |
|  max active duration | limitation of call duration |
|  max attempt duration | maximal dial time wait |
|  skip errors | the option defines the need of errors being skipped or not |
|  interval between calls | time interval between sequential calls |
|  calls count | amount of calls |
|  require active | on activation of option, the call will be successful if the subscriber answers the call |

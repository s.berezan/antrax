## Call Filter

With the help of call filter parameters for out-coming call numbers are set in GSM network. Pattern parameters are set with the help of regular expressions.

### Complex Call Filter

![complex_call_filter](call-filter.assets/call_filter_complex.png)

| Name | Description|
| -------- | -------- |
| rule | rules that establish parameters of filter operation |
| allowed A number | pattern for allowed numbers A numbers |
| denied A number | pattern for denied numbers A numbers |
| allowed B number  | pattern for allowed numbers B numbers |
| denied B number | pattern for denied numbers A numbers |
| replace B number pattern | B number pattern for calls to GSM network |

### Pattern based Call Filter

![patern_based_call_filter](call-filter.assets/call_filter_pattern.png)

| Name | Description|
| -------- | -------- |
| allowed A number | pattern for allowed numbers A numbers |
| denied A number | pattern for denied numbers A numbers |
| allowed B number  | pattern for allowed numbers B numbers |
| denied B number | pattern for denied numbers B numbers |
| replace B number pattern | B number pattern for calls to GSM network |

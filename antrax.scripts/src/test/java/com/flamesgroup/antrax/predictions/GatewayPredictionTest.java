/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.net.UnknownHostException;

public class GatewayPredictionTest {

  @Test
  public void testGwOrGw() throws UnknownHostException {
    IncludeGatewayPrediction gwPrediction1 = new IncludeGatewayPrediction();
    assertEquals("none", gwPrediction1.or(gwPrediction1).toLocalizedString());

    String gw1 = "gateway_1";
    String gw2 = "gateway_2";
    String gw3 = "gateway_3";

    gwPrediction1 = new IncludeGatewayPrediction();
    IncludeGatewayPrediction gwPrediction2 = new IncludeGatewayPrediction(gw1);
    assertEquals("gateway_1", gwPrediction1.or(gwPrediction2).toLocalizedString());

    gwPrediction1 = new IncludeGatewayPrediction(gw1);
    gwPrediction2 = new IncludeGatewayPrediction();
    assertEquals("gateway_1", gwPrediction1.or(gwPrediction2).toLocalizedString());

    gwPrediction1 = new IncludeGatewayPrediction(gw1);
    gwPrediction2 = new IncludeGatewayPrediction(gw1);
    assertEquals("gateway_1", gwPrediction1.or(gwPrediction2).toLocalizedString());

    gwPrediction1 = new IncludeGatewayPrediction(gw1);
    gwPrediction2 = new IncludeGatewayPrediction(gw2);
    assertEquals("any of gateway_2 or gateway_1", gwPrediction1.or(gwPrediction2).toLocalizedString());

    gwPrediction1 = new IncludeGatewayPrediction(gw1, gw3);
    gwPrediction2 = new IncludeGatewayPrediction(gw2);
    assertEquals("any of gateway_2 or gateway_1 or gateway_3", gwPrediction1.or(gwPrediction2).toLocalizedString());

    gwPrediction1 = new IncludeGatewayPrediction(gw1, gw3);
    gwPrediction2 = new IncludeGatewayPrediction(gw2, gw3);
    assertEquals("any of gateway_2 or gateway_1 or gateway_3", gwPrediction1.or(gwPrediction2).toLocalizedString());
  }

  @Test
  public void testGwOrNotGw() throws UnknownHostException {
    IncludeGatewayPrediction gwPrediction = new IncludeGatewayPrediction();
    ExcludeGatewayPrediction notGwPrediction = new ExcludeGatewayPrediction();
    assertEquals("any", gwPrediction.or(notGwPrediction).toLocalizedString());

    String gw1 = "gateway_1";
    String gw2 = "gateway_2";
    String gw3 = "gateway_3";

    gwPrediction = new IncludeGatewayPrediction();
    notGwPrediction = new ExcludeGatewayPrediction(gw1);
    assertEquals("any except gateway_1", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1);
    notGwPrediction = new ExcludeGatewayPrediction();
    assertEquals("any", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1);
    notGwPrediction = new ExcludeGatewayPrediction(gw1);
    assertEquals("any", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1);
    notGwPrediction = new ExcludeGatewayPrediction(gw2);
    assertEquals("any except gateway_2", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1, gw3);
    notGwPrediction = new ExcludeGatewayPrediction(gw2);
    assertEquals("any except gateway_2", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1, gw3);
    notGwPrediction = new ExcludeGatewayPrediction(gw2, gw3);
    assertEquals("any except gateway_2", gwPrediction.or(notGwPrediction).toLocalizedString());

    gwPrediction = new IncludeGatewayPrediction(gw1);
    notGwPrediction = new ExcludeGatewayPrediction(gw2, gw3);
    assertEquals("any except gateway_2 and gateway_3", gwPrediction.or(notGwPrediction).toLocalizedString());
  }

  @Test
  public void testNotGwOrNotGw() throws UnknownHostException {
    ExcludeGatewayPrediction notGwPrediction1 = new ExcludeGatewayPrediction();
    assertEquals("any", notGwPrediction1.or(notGwPrediction1).toLocalizedString());

    String gw1 = "gateway_1";
    String gw2 = "gateway_2";
    String gw3 = "gateway_3";

    notGwPrediction1 = new ExcludeGatewayPrediction();
    ExcludeGatewayPrediction notGwPrediction2 = new ExcludeGatewayPrediction(gw1);
    assertEquals("any", notGwPrediction1.or(notGwPrediction2).toLocalizedString());

    notGwPrediction1 = new ExcludeGatewayPrediction(gw1);
    notGwPrediction2 = new ExcludeGatewayPrediction();
    assertEquals("any", notGwPrediction1.or(notGwPrediction2).toLocalizedString());

    notGwPrediction1 = new ExcludeGatewayPrediction(gw1);
    notGwPrediction2 = new ExcludeGatewayPrediction(gw1);
    assertEquals("any except gateway_1", notGwPrediction1.or(notGwPrediction2).toLocalizedString());

    notGwPrediction1 = new ExcludeGatewayPrediction(gw1);
    notGwPrediction2 = new ExcludeGatewayPrediction(gw2);
    assertEquals("any", notGwPrediction1.or(notGwPrediction2).toLocalizedString());

    notGwPrediction1 = new ExcludeGatewayPrediction(gw1, gw3);
    notGwPrediction2 = new ExcludeGatewayPrediction(gw2);
    assertEquals("any", notGwPrediction1.or(notGwPrediction2).toLocalizedString());

    notGwPrediction1 = new ExcludeGatewayPrediction(gw1, gw3);
    notGwPrediction2 = new ExcludeGatewayPrediction(gw2, gw3);
    assertEquals("any except gateway_3", notGwPrediction1.or(notGwPrediction2).toLocalizedString());
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class VaucherHelperTest {

  public static class TestRegistryEntry implements RegistryEntry, Cloneable {

    private static final AtomicInteger ids = new AtomicInteger();
    private static final long serialVersionUID = -5618830470991192370L;

    private final Integer id;
    private String path;
    private final String value;

    private Date updatingTime;

    public TestRegistryEntry(final String path, final String value) {
      this.path = path;
      this.value = value;
      this.id = ids.incrementAndGet();
      this.updatingTime = new Date();
    }

    @Override
    public String getPath() {
      return path;
    }

    @Override
    public Date getUpdatingTime() {
      return updatingTime;
    }

    @Override
    public String getValue() {
      return value;
    }

    @Override
    public TestRegistryEntry clone() {
      try {
        return (TestRegistryEntry) super.clone();
      } catch (CloneNotSupportedException e) {
        return null;
      }
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj instanceof TestRegistryEntry) {
        return this.id.equals(((TestRegistryEntry) obj).id);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return id;
    }

    public void setPath(final String path) {
      this.path = path;
    }

    public void setUpdateTime(final Date updateTime) {
      this.updatingTime = updateTime;
    }

  }

  public static class TestRegistry implements RegistryAccess {
    private final List<TestRegistryEntry> entries = new
        ArrayList<>();

    @Override
    public void add(final String path, final String value, final int maxSize) {
      entries.add(new TestRegistryEntry(path, value));
    }

    @Override
    public RegistryEntry[] listEntries(final String path, final int maxSize) {
      List<RegistryEntry> retval = new LinkedList<>();
      for (TestRegistryEntry e : entries) {
        if (path.equals(e.getPath())) {
          retval.add(e.clone());
        }
      }
      return retval.toArray(new RegistryEntry[retval.size()]);
    }

    @Override
    public RegistryEntry move(final RegistryEntry entry, final String path) throws
        UnsyncRegistryEntryException {
      int index = entries.indexOf(entry);
      if (index < 0) {
        throw new UnsyncRegistryEntryException("");
      }
      RegistryEntry myEntry = entries.get(index);
      if (!myEntry.getUpdatingTime().equals(entry.getUpdatingTime())) {
        throw new UnsyncRegistryEntryException("");
      }
      Date updateTime = new Date();
      ((TestRegistryEntry) myEntry).setPath(path);
      ((TestRegistryEntry) entry).setPath(path);
      ((TestRegistryEntry) myEntry).setUpdateTime(updateTime);
      ((TestRegistryEntry) entry).setUpdateTime(updateTime);
      return entry;
    }

    @Override
    public void remove(final RegistryEntry entry) throws UnsyncRegistryEntryException {
      throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public String[] listAllPaths() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public void removePath(final String path) {
      // TODO Auto-generated method stub

    }
  }

  @Test
  public void testSuccessfullPayment() throws Exception {
    testSuccessfullPaymentWithPath("test.10.new");
    testSuccessfullPaymentWithPath("test.10.failed.1");
    testSuccessfullPaymentWithPath("test.10.failed.2");
    testSuccessfullPaymentWithPath("test.10.failed.3");
  }

  @Test
  public void testFailedEntries() throws Exception {
    TestRegistry access = new TestRegistry();
    access.add("test.10.new", "vaucher", 5);

    VaucherHelper helper = new VaucherHelper("test", access, 10, false) {

      @Override
      protected boolean executePayment(final String vaucher) throws Exception {
        return false;
      }
    };

    exceptionlessPay(helper);
    assertEquals("test.10.failed.1", access.entries.get(0).getPath());
    exceptionlessPay(helper);
    assertEquals("test.10.failed.2", access.entries.get(0).getPath());
    exceptionlessPay(helper);
    assertEquals("test.10.failed.3", access.entries.get(0).getPath());
    exceptionlessPay(helper);
    assertEquals("test.10.bad", access.entries.get(0).getPath());
  }

  private void exceptionlessPay(final VaucherHelper helper) {
    try {
      helper.pay();
    } catch (Exception e) {
    }
  }

  private void testSuccessfullPaymentWithPath(final String path) throws Exception {
    TestRegistry access = new TestRegistry();
    access.add(path, "test-vaucher", 5);

    VaucherHelper vaucherHelper = new VaucherHelper("test", access, 10, false) {

      @Override
      protected boolean executePayment(final String vaucher) throws Exception {
        assertEquals("test-vaucher", vaucher);
        return true;
      }
    };

    vaucherHelper.pay();
    assertEquals("test-vaucher", access.listEntries("test.10.used",
        10)[0].getValue());
  }
}

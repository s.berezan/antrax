/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.junit.Test;

import java.io.Serializable;

public class GenerateEventInstantlyTest {

  private final static class Channel extends RegisteredInGSMChannelAdapter {
    private String event;
    private Serializable[] args;

    @Override
    public void fireGenericEvent(final String event, final Serializable... args) {
      this.event = event;
      this.args = args;
    }

    @Override
    public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
      return null;
    }
  }

  @Test
  public void testGeneratesInfinite() throws Exception {
    GenerateEventInstantly script = new GenerateEventInstantly();
    script.setEvent("event");
    script.setEventTimeout(new TimePeriod(1000));
    script.setEventLimit(1);
    script.setLockOnFinish(false);

    script.handleActivityStarted(null);
    assertTrue(script.shouldStartBusinessActivity());
    Channel channel = new Channel();
    script.invokeBusinessActivity(channel);
    assertEquals("event", channel.event);
    assertFalse(script.shouldStartBusinessActivity());
    script.handleGenericEvent(channel.args[1].toString());
    assertTrue(script.shouldStartBusinessActivity());

  }

  @Test
  public void testOncePerActivity() throws Exception {
    GenerateEventInstantly script = new GenerateEventInstantly();
    script.setEvent("event");
    script.setEventTimeout(new TimePeriod(100));
    script.setEventLimit(1);

    script.handleActivityStarted(null);
    assertTrue(script.shouldStartBusinessActivity());
    Channel channel = new Channel();
    script.invokeBusinessActivity(channel);
    assertEquals("event", channel.event);
    assertFalse(script.shouldStartBusinessActivity());
    script.handleGenericEvent(channel.args[1].toString());
    Thread.sleep(150);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
  }
}

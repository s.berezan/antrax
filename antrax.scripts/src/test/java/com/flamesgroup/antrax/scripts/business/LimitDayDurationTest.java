/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

public class LimitDayDurationTest {

  @Test
  public void test() throws Exception {
    final AtomicLong midnight = new AtomicLong(System.currentTimeMillis());
    LimitDayDuration script = new LimitDayDuration() {

      private static final long serialVersionUID = -3407938433093564890L;

      @Override
      long getCurrentMidnight() {
        return midnight.get();
      }
    };
    script.setDurationLimit(new TimeInterval(500, 500));
    assertFalse(script.shouldStartBusinessActivity());

    script.handleCallStart(null);
    assertFalse(script.shouldStartBusinessActivity());
    Thread.sleep(100);
    assertFalse(script.shouldStartBusinessActivity());
    script.handleCallEnd(100, 0);

    assertFalse(script.shouldStartBusinessActivity());
    script.handleCallStart(null);
    Thread.sleep(200);
    assertFalse(script.shouldStartBusinessActivity());
    script.handleCallEnd(200, 0);

    assertFalse(script.shouldStartBusinessActivity());
    script.handleCallStart(null);
    Thread.sleep(300);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter());
    script.handleCallEnd(350, 0);

    assertFalse(script.shouldStartBusinessActivity());
    assertFalse(script.shouldStartBusinessActivity());

    midnight.set(System.currentTimeMillis());
    assertFalse(script.shouldStartBusinessActivity());
    script.handleCallStart(null);
    Thread.sleep(600);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter());
    script.handleCallEnd(600, 0);

    assertFalse(script.shouldStartBusinessActivity());
    assertFalse(script.shouldStartBusinessActivity());
  }

}

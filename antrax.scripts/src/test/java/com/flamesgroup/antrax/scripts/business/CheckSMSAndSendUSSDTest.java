/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.junit.Test;

public class CheckSMSAndSendUSSDTest {

  private String sendUssd;

  @Test
  public void invokeBusinessActivitySuccess() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");
    checkSMSAndSendUSSD.setCodeRegex(".*\\s(\\d+).*");
    checkSMSAndSendUSSD.setSendUssd("*145*$1#");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", "cod 333 , mts");

    assertTrue(checkSMSAndSendUSSD.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public String sendUSSD(final String ussd) throws Exception {
        sendUssd = ussd;
        return sendUssd;
      }
    };

    checkSMSAndSendUSSD.invokeBusinessActivity(registeredInGSMChannelAdapter);

    assertEquals(sendUssd, "*145*333#");
  }

  @Test
  public void invokeBusinessActivityWaitEvent() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");
    checkSMSAndSendUSSD.setCodeRegex(".*\\s(\\d+).*");
    checkSMSAndSendUSSD.setSendUssd("*145*$1#");

    checkSMSAndSendUSSD.handleIncomingSMS("", "cod 333 , mts");

    assertFalse(checkSMSAndSendUSSD.shouldStartBusinessActivity());
  }

  @Test
  public void invokeBusinessActivityWrongSms() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");
    checkSMSAndSendUSSD.setCodeRegex("cod (\\d+).*");
    checkSMSAndSendUSSD.setSendUssd("*145*$1#");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", "333 , mts");

    assertFalse(checkSMSAndSendUSSD.shouldStartBusinessActivity());
  }

  @Test
  public void invokeBusinessActivitySmsTimeout() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");
    checkSMSAndSendUSSD.setSmsTimeout(new TimePeriod(TimePeriod.inMillis(25)));
    checkSMSAndSendUSSD.setCodeRegex("cod (\\d+).*");
    checkSMSAndSendUSSD.setSendUssd("*145*$1#");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", "333 , mts");

    assertFalse(checkSMSAndSendUSSD.shouldStartBusinessActivity());

    Thread.sleep(50);

    assertTrue(checkSMSAndSendUSSD.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public String sendUSSD(final String ussd) throws Exception {
        sendUssd = ussd;
        return sendUssd;
      }
    };

    checkSMSAndSendUSSD.invokeBusinessActivity(registeredInGSMChannelAdapter);

    assertNull(sendUssd);
  }

  @Test
  public void invokeBusinessActivityEmptySMS() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", "");

    assertFalse(checkSMSAndSendUSSD.shouldStartBusinessActivity());
  }

  @Test
  public void invokeBusinessActivityNullSMS() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", null);

    assertFalse(checkSMSAndSendUSSD.shouldStartBusinessActivity());
  }

  @Test
  public void invokeBusinessActivityException() throws Exception {
    sendUssd = null;
    CheckSMSAndSendUSSD checkSMSAndSendUSSD = new CheckSMSAndSendUSSD();
    checkSMSAndSendUSSD.setEvent("event");
    checkSMSAndSendUSSD.setCodeRegex(".*cod (\\d+).*");
    checkSMSAndSendUSSD.setSendUssd("*145*$1#");

    checkSMSAndSendUSSD.handleGenericEvent("event");
    checkSMSAndSendUSSD.handleIncomingSMS("", "cod 333 , mts");

    assertTrue(checkSMSAndSendUSSD.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public String sendUSSD(final String ussd) throws Exception {
        throw new Exception();
      }
    };

    checkSMSAndSendUSSD.invokeBusinessActivity(registeredInGSMChannelAdapter);

    assertNull(sendUssd);
  }

}

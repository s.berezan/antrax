/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.activity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.activity.AllowActivityAfterCallDurationPassed;
import com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerActivity;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import org.junit.Test;

public class AllowSessionAfterCallDurationPassedTest {

  @Test
  public void testForbids() {
    AllowActivityAfterCallDurationPassed script = new AllowActivityAfterCallDurationPassed();
    script.setCallDurationLimit(new TimeInterval(new TimePeriod(100), new TimePeriod(100)));
    SimData simData = new SimData();
    script.setSimData(simData);
    assertFalse(script.isActivityAllowed());
    simData.setCallDuration(50);
    assertFalse(script.isActivityAllowed());
  }

  @Test
  public void testAllows() {
    AllowActivityAfterCallDurationPassed script = new AllowActivityAfterCallDurationPassed();
    script.setCallDurationLimit(new TimeInterval(new TimePeriod(100), new TimePeriod(100)));
    SimData simData = new SimData();
    script.setSimData(simData);
    simData.setCallDuration(100);
    assertTrue(script.isActivityAllowed());
  }

  @Test
  public void testCombinations() {
    AllowActivityAfterCallDurationPassed duration = new AllowActivityAfterCallDurationPassed();
    duration.setCallDurationLimit(new TimeInterval(new TimePeriod(100), new TimePeriod(100)));

    LimitCallAttemptsPerActivity limitAttemts = new LimitCallAttemptsPerActivity();
    limitAttemts.setAttemptsLimit(new VariableLong(5, 5));

    SimpleActivityScript script = duration.or(limitAttemts);

    SimData simData = new SimData();
    script.setSimData(simData);
    assertTrue(script.isActivityAllowed());
    script.handlePeriodStart();
    assertTrue(script.isActivityAllowed());
    simData.setTotalCallsCount(6);
    assertFalse(script.isActivityAllowed());
    simData.setCallDuration(100);
    assertTrue(script.isActivityAllowed());
  }
}

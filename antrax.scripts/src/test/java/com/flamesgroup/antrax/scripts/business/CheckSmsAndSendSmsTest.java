/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.unit.PhoneNumber;
import org.junit.Test;

public class CheckSmsAndSendSmsTest {

  @Test
  public void invokeBusinessActivitySuccessWithHardCodedText() throws Exception {
    final String incomingSmsPhoneNumber = "123";
    final String sendSmsText = "HardCodedText";
    CheckSmsAndSendSms checkSmsAndSendSms = new CheckSmsAndSendSms();
    checkSmsAndSendSms.setEvent("event");
    checkSmsAndSendSms.setIncomingTextRegex(".*\\s(\\d+).*");
    checkSmsAndSendSms.setSmsNumberRegex("(.*)");
    checkSmsAndSendSms.setSmsNumberReplacePattern("$1");
    checkSmsAndSendSms.setSendSmsTextReplacePattern(sendSmsText);

    checkSmsAndSendSms.handleGenericEvent("event");
    checkSmsAndSendSms.handleIncomingSMS(incomingSmsPhoneNumber, "cod 333 , mts");

    assertTrue(checkSmsAndSendSms.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public void sendSMS(final PhoneNumber phoneNumber, final String text) throws Exception {
        assertEquals("Phone number should equals", incomingSmsPhoneNumber, phoneNumber.getValue());
        assertEquals("Text should equals", sendSmsText, text);
      }
    };

    checkSmsAndSendSms.invokeBusinessActivity(registeredInGSMChannelAdapter);
  }

  @Test
  public void invokeBusinessActivitySuccessWithRegexpText() throws Exception {
    final String incomingSmsPhoneNumber = "123";
    CheckSmsAndSendSms checkSmsAndSendSms = new CheckSmsAndSendSms();
    checkSmsAndSendSms.setEvent("event");
    checkSmsAndSendSms.setIncomingTextRegex(".*\\s(\\d+).*\\s(\\d+).*");
    checkSmsAndSendSms.setSmsNumberRegex("(.*)");
    checkSmsAndSendSms.setSmsNumberReplacePattern("$1");
    checkSmsAndSendSms.setSendSmsTextReplacePattern("$1 HardCodedText $2");

    checkSmsAndSendSms.handleGenericEvent("event");
    checkSmsAndSendSms.handleIncomingSMS(incomingSmsPhoneNumber, "cod 333 , mts from code 222");

    assertTrue(checkSmsAndSendSms.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public void sendSMS(final PhoneNumber phoneNumber, final String text) throws Exception {
        assertEquals("Phone number should equals", incomingSmsPhoneNumber, phoneNumber.getValue());
        assertEquals("Text should equals", "333 HardCodedText 222", text);
      }
    };

    checkSmsAndSendSms.invokeBusinessActivity(registeredInGSMChannelAdapter);
  }

  @Test
  public void invokeBusinessActivitySuccessSendTextFromIncomingSms() throws Exception {
    final String incomingSmsPhoneNumber = "123";
    CheckSmsAndSendSms checkSmsAndSendSms = new CheckSmsAndSendSms();
    checkSmsAndSendSms.setEvent("event");
    checkSmsAndSendSms.setIncomingTextRegex("(.*)");
    checkSmsAndSendSms.setSmsNumberRegex("(.*)");
    checkSmsAndSendSms.setSmsNumberReplacePattern("$1");
    checkSmsAndSendSms.setSendSmsTextReplacePattern("$1");

    checkSmsAndSendSms.handleGenericEvent("event");
    checkSmsAndSendSms.handleIncomingSMS(incomingSmsPhoneNumber, "cod 333 , mts from code 222");

    assertTrue(checkSmsAndSendSms.shouldStartBusinessActivity());

    RegisteredInGSMChannelAdapter registeredInGSMChannelAdapter = new RegisteredInGSMChannelAdapter() {
      @Override
      public void sendSMS(final PhoneNumber phoneNumber, final String text) throws Exception {
        assertEquals("Phone number should equals", incomingSmsPhoneNumber, phoneNumber.getValue());
        assertEquals("Text should equals", "cod 333 , mts from code 222", text);
      }
    };

    checkSmsAndSendSms.invokeBusinessActivity(registeredInGSMChannelAdapter);
  }

}

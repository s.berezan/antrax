/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.antrax.scripts.utils.BlackListNumberStatus;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;

@Script(name = "add to black list number", doc = "add to black list called number")
public class AddToBlackListNumber implements BusinessActivityScript, GenericEventListener, CallsListener {

  private static final Logger logger = LoggerFactory.getLogger(AddToBlackListNumber.class);

  private static final long serialVersionUID = 6259233100951310942L;

  private String event = "addToBlackListNumber";
  private BlackListNumberStatus status = BlackListNumberStatus.IVR;

  private volatile boolean isStartActivity;
  private volatile PhoneNumber lastCalledPhoneNumber;

  @ScriptParam(name = "event", doc = "add to black list number after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "status", doc = "will be added to black list with this status")
  public void setStatus(final BlackListNumberStatus status) {
    this.status = status;
  }

  public BlackListNumberStatus getStatus() {
    return status;
  }

  @Override
  public String describeBusinessActivity() {
    return "add to black list called number";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (lastCalledPhoneNumber == null) {
      logger.warn("lastCalledPhoneNumber is Null");
    } else {
      channel.addToBlackList(crateBlackListNumber(lastCalledPhoneNumber));
      lastCalledPhoneNumber = null;
    }
    isStartActivity = false;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return isStartActivity;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      isStartActivity = true;
    }
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
    lastCalledPhoneNumber = phoneNumber;
    logger.debug("handleCallSetup for call {}", lastCalledPhoneNumber);
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
    lastCalledPhoneNumber = null;
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  private BlackListNumber crateBlackListNumber(final PhoneNumber phoneNumber) {
    if (status == BlackListNumberStatus.IVR) {
      return new BlackListNumber(phoneNumber.getValue(), VoipAntiSpamListNumbersStatus.IVR, 1, "Added by IVR detect", new Date());
    } else if (status == BlackListNumberStatus.DROP_CODE) {
      return new BlackListNumber(phoneNumber.getValue(), VoipAntiSpamListNumbersStatus.DROP_CODE, 1, "Added by drop code", new Date());
    } else {
      return null;
    }
  }

}

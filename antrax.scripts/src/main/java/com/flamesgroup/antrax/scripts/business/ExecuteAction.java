/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

@Script(name = "Execute action", doc = "waits until event occured and execute user specified action")
public class ExecuteAction implements BusinessActivityScript, GenericEventListener {

  private String action = "action";
  private int attemptsCount = 3;
  private String event = "exec_action";
  private TimePeriod actionTimeout = new TimePeriod(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(40));

  private volatile GenericEvent caughtEvent;

  @ScriptParam(name = "action name", doc = "executable action name")
  public void setAction(final String action) {
    this.action = action;
  }

  public String getAction() {
    return action;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on execute action failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "event", doc = "event to execute action")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "action timeout", doc = "max time to wait action executing")
  public void setActionTimeout(final TimePeriod actionTimeout) {
    this.actionTimeout = actionTimeout;
  }

  public TimePeriod getActionTimeout() {
    return actionTimeout;
  }

  @Override
  public String describeBusinessActivity() {
    return String.format("executing action [%s]", action);
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    for (int i = 0; i < attemptsCount; i++) {
      try {
        PhoneNumber number = channel.getSimData().getPhoneNumber();
        if (number == null) {
          channel.lock(String.format("Failed to execute action [%s] cause %s has no number", action, channel));
        } else {
          channel.executeAction(new Action(action, number), actionTimeout.getPeriod());
          caughtEvent.respondSuccess(channel);
        }
        break;
      } catch (Exception e) {
        if (i == attemptsCount) {
          caughtEvent.respondFailure(channel, e.getMessage());
        }
      }
    }
    caughtEvent = null;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}

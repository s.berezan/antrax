/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "change group", doc = "waits until event occurs, then changes group to specified one")
public class ChangeSimGroup implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(ChangeSimGroup.class);

  private volatile GenericEvent caughtEvent = null;

  private String event = "changeGroup";

  private SimGroupReference group = null;

  @ScriptParam(name = "event", doc = "this event causes group change")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "group", doc = "script will change channels group to this value")
  public void setGroup(final SimGroupReference simGroup) {
    this.group = simGroup;
  }

  public SimGroupReference getGroup() {
    return group;
  }

  @Override
  public String describeBusinessActivity() {
    return "change group";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - begin change group", this);
    try {
      if (group != null)
        channel.changeGroup(group);
      caughtEvent.respondSuccess(channel);
    } catch (Exception e) {
      logger.debug("[{}] - fail to change group", this, e);
      caughtEvent.respondFailure(channel, e.getMessage());
    } finally {
      caughtEvent = null;
    }
    logger.debug("[{}] - end change group", this);
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

}

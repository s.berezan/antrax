/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Script(name = "sleep", doc = "sleeps for specified timeout")
public class Sleep implements BusinessActivityScript, GenericEventListener {

  private TimeInterval timeout = new TimeInterval(TimePeriod.inSeconds(20), TimePeriod.inMinutes(1));
  private String event = "sleep";
  private String eventAfterSleep = "event";
  private String interruptedEvent = "interruptedEvent";

  private GenericEvent caughtEvent;

  private volatile long sleepTime = 0;

  private final Lock lock = new ReentrantLock();
  private final Condition condition = lock.newCondition();

  @ScriptParam(name = "event", doc = "trigger event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event after sleep", doc = "event will be generated after sleep")
  public void setEventAfterSleep(final String eventAfterSleep) {
    this.eventAfterSleep = eventAfterSleep;
  }

  public String getEventAfterSleep() {
    return eventAfterSleep;
  }

  @ScriptParam(name = "interrupt event", doc = "trigger event if need interrupt this script")
  public void setInterruptedEvent(final String interruptedEvent) {
    this.interruptedEvent = interruptedEvent;
  }

  public String getInterruptedEvent() {
    return interruptedEvent;
  }

  @ScriptParam(name = "sleep time", doc = "time of sleep")
  public void setSleepTime(final TimeInterval interval) {
    this.timeout = interval;
  }

  public TimeInterval getSleepTime() {
    return timeout;
  }

  @Override
  public String describeBusinessActivity() {
    return "sleep " + new TimePeriod(countSleepTime());
  }

  private long countSleepTime() {
    if (sleepTime == 0) {
      sleepTime = timeout.random();
    }
    return sleepTime;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    lock.lock();
    try {
      condition.await(countSleepTime(), TimeUnit.MILLISECONDS);
    } finally {
      lock.unlock();
      sleepTime = 0;
      caughtEvent.respondSuccess(channel);
      caughtEvent = null;
    }
    channel.fireGenericEvent(eventAfterSleep);
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
    if (this.interruptedEvent.equals(event)) {
      lock.lock();
      try {
        condition.signal();
      } finally {
        lock.unlock();
      }
    }
  }

}

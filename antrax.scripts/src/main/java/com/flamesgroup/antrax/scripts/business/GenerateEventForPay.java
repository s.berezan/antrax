/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

@Script(name = "generate event for pay", doc = "generate event for pay from other SIM card")
public class GenerateEventForPay implements BusinessActivityScript, GenericEventListener {

  private static final long serialVersionUID = -7998959674179855754L;

  private volatile GenericEvent caughtEvent = null;

  private String event = "event";
  private String generateEvent = "generate_event";
  private Integer amountForPay = 10;

  @ScriptParam(name = "event", doc = "event for execute this script")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "generateEvent", doc = "generate event with phone number and amount for pay")
  public void setGenerateEvent(final String generateEvent) {
    this.generateEvent = generateEvent;
  }

  public String getGenerateEvent() {
    return generateEvent;
  }

  @ScriptParam(name = "amountForPay", doc = "amount for pay")
  public void setAmountForPay(final Integer amountForPay) {
    this.amountForPay = amountForPay;
  }

  public Integer getAmountForPay() {
    return amountForPay;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    PhoneNumber phoneNumber = channel.getSimData().getPhoneNumber();
    if(phoneNumber == null) {
      caughtEvent.respondFailure(channel, "Phone number is not set to SIM card");
      return;
    }
    caughtEvent.respondSuccess(channel);
    channel.fireGenericEvent(generateEvent, phoneNumber, amountForPay);
    caughtEvent = null;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event for pay";
  }

}

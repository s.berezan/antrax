/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.EventHelper;
import com.flamesgroup.antrax.helper.business.EventStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Script(name = "split events", doc = "splits one event to the couple of them")
public class SplitEvent implements BusinessActivityScript, StatefullScript, GenericEventListener {

  private static final long serialVersionUID = -1938752963472180132L;

  private static final Logger logger = LoggerFactory.getLogger(SplitEvent.class);

  private final List<GenericEvent> events = new LinkedList<>();
  private TimePeriod eventExecutionTimeLimit = new TimePeriod(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(40));
  private String initialEvent = "splitEvents";
  private boolean lockOnFailure = false;

  @StateField
  private final List<GenericEvent> incomingEvents = Collections.synchronizedList(new LinkedList<GenericEvent>());
  @StateField
  private final List<GenericEvent> alreadyDoneEvents = new LinkedList<>();

  private volatile EventHelper eventHelper;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "lock on failure", doc = "locks card in case of failure at least one of the events")
  public void setLockOnFailure(final boolean lockOnFailure) {
    this.lockOnFailure = lockOnFailure;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @ScriptParam(name = "initial event", doc = "initial event for spliting")
  public void setInitialEvent(final String event) {
    this.initialEvent = event;
  }

  public String getInitialEvent() {
    return initialEvent;
  }

  @ScriptParam(name = "event", doc = "event to generate")
  public void addEvent(final String event) {
    this.events.add(GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event)));
  }

  public String getEvent() {
    return "event";
  }

  @ScriptParam(name = "time limit", doc = "event execution time limit")
  public void setEventExecutionTimeLimit(final TimePeriod limit) {
    this.eventExecutionTimeLimit = limit;
  }

  public TimePeriod getEventExecutionTimeLimit() {
    return eventExecutionTimeLimit;
  }

  private String successedResponse(final String event) {
    return event + " success " + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + " failure " + Math.random() + System.nanoTime();
  }

  @Override
  public String describeBusinessActivity() {
    return "split event";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (eventHelper == null) {
      logger.debug("[{}] - counting current event from done events {} and events {}", this, alreadyDoneEvents, events);
      GenericEvent currentEvent = getCurrentEvent();
      if (currentEvent == null) {
        incomingEvents.remove(0).respondSuccess(channel);
        alreadyDoneEvents.clear();
      } else {
        eventHelper = new EventHelper(currentEvent, eventExecutionTimeLimit.getPeriod());
        logger.debug("[{}] - generate event {}", this, currentEvent);
        eventHelper.generateEvent(channel);
      }
    } else {
      if (eventHelper.getStatus() == EventStatus.FAILED || eventHelper.getStatus() == EventStatus.TIMEOUT) {
        logger.debug("[{}] - {}", this, eventHelper.getFailReason());
        incomingEvents.remove(0).respondFailure(channel, eventHelper.getFailReason());
        alreadyDoneEvents.clear();
        if (lockOnFailure) {
          logger.debug("[{}] - going to lock card", this);
          channel.lock("Locked: " + eventHelper.getFailReason());
        }
      } else if (eventHelper.getStatus() == EventStatus.SUCCEED) {
        alreadyDoneEvents.add(getCurrentEvent());
      }
      eventHelper = null;
    }
    saver.save();
  }

  synchronized GenericEvent getCurrentEvent() {
    Iterator<GenericEvent> doneIter = alreadyDoneEvents.iterator();
    Iterator<GenericEvent> newIter = events.iterator();
    while (doneIter.hasNext() && newIter.hasNext()) {
      GenericEvent doneE = doneIter.next();
      GenericEvent newE = newIter.next();

      if (!doneE.getEvent().equals(newE.getEvent())) {
        doneIter.remove();
        while (doneIter.hasNext()) {
          doneIter.next();
          doneIter.remove();
        }
        return newE;
      }
    }
    if (newIter.hasNext()) {
      return newIter.next();
    }
    return null;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (eventHelper == null) {
      return !incomingEvents.isEmpty();
    } else {
      return eventHelper.isFinished();
    }
    //    return !incomingEvents.isEmpty() && !isProcessingEvent();
  }

  //  private boolean isProcessingEvent() {
  //    if (lastEventTime == 0) {
  //      return false;
  //    }
  //    if (System.currentTimeMillis() - lastEventTime > eventExecutionTimeLimit.getPeriod()) {
  //      return false;
  //    }
  //    return true;
  //  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (initialEvent.equals(event)) {
      incomingEvents.add(GenericEvent.wrapEvent(event, args));
    } else if (eventHelper != null) {
      eventHelper.handleEvent(event, args);
      saver.save();
    }
  }
}

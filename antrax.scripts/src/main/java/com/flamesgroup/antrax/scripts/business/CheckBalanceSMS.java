/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.checkbalance.BadResponseException;
import com.flamesgroup.antrax.helper.business.checkbalance.sms.SMSCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.sms.SMSResponsePattern;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Script(name = "check balance SMS", doc = "analyzes incoming SMS on balance information and generates events according to the value of balance")
public class CheckBalanceSMS implements BusinessActivityScript, SMSListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(CheckBalanceSMS.class);

  private double minimumAmount = 5;

  private String event = "check_balance_sms";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));

  private String lessThanMinimumEvent = "payMe";
  private String moreThanMinimumEvent = "balanceFull";
  private SMSCheckBalanceHelper checkBalanceHelper;

  private volatile String smsText;

  private boolean parseAlternativeSMS = false;
  private final Set<SMSResponsePattern> smsResponsePatterns = new HashSet<>();

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private final AtomicLong smsTime = new AtomicLong();

  @ScriptParam(name = "event", doc = "check sms will be started after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  @ScriptParam(name = "balance check data", doc = "balance checking data")
  public void setCheckBalanceHelper(final SMSCheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public SMSCheckBalanceHelper getCheckBalanceHelper() {
    return new SMSCheckBalanceHelper(".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*");
  }

  @ScriptParam(name = "minimum amount of money", doc = "required amount of money in the balance")
  public void setMinimumAmount(final double minimumAmount) {
    this.minimumAmount = minimumAmount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "event on less", doc = "event on amount of money in the balance that less than minimum")
  public void setLessThanMinimumEvent(final String lessThanMinimumEvent) {
    this.lessThanMinimumEvent = lessThanMinimumEvent;
  }

  public String getLessThanMinimumEvent() {
    return lessThanMinimumEvent;
  }

  @ScriptParam(name = "event on more", doc = "event on amount of money in the balance that more than minimum")
  public void setMoreThanMinimumEvent(final String moreThanMinimumEvent) {
    this.moreThanMinimumEvent = moreThanMinimumEvent;
  }

  public String getMoreThanMinimumEvent() {
    return moreThanMinimumEvent;
  }

  public boolean getParseAlternativeSMS() {
    return parseAlternativeSMS;
  }

  @ScriptParam(name = "parse alternative sms responses", doc = "allow to parse alternative sms response")
  public void setParseAlternativeSMS(final boolean parseAlternativeSMS) {
    this.parseAlternativeSMS = parseAlternativeSMS;
  }

  @ScriptParam(name = "alternative sms responses", doc = "if bad balance response and allowed, will parse and generate event")
  public void addSMSResponsePattern(final SMSResponsePattern smsResponsePattern) {
    smsResponsePatterns.add(smsResponsePattern);
  }

  public SMSResponsePattern getSMSResponsePattern() {
    return new SMSResponsePattern(".*", "event");
  }

  @Override
  public String describeBusinessActivity() {
    return "check balance SMS";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (smsText == null) {
      smsTime.set(0);
      logger.debug("[{}] - waiting sms timeout", this);
      return;
    }

    double balanceValue;
    try {
      balanceValue = checkBalanceHelper.getBalanceValue(smsText);
    } catch (BadResponseException e) {
      if (parseAlternativeSMS) {
        SMSResponsePattern responsePattern = checkSMS(e.getResponse());
        if (responsePattern == null) {
          logger.debug("[{}] - wrong alternative sms response: {}", this, e.getResponse());
        } else {
          channel.fireGenericEvent(responsePattern.getResponseEvent());
        }
      } else {
        logger.debug("[{}] - while check balance sms", this, e);
      }

      return;
    } finally {
      smsTime.set(0);
      smsText = null;
    }

    logger.debug("[{}] - balance value: {}", this, balanceValue);
    channel.setBalance(balanceValue);
    if (balanceValue < minimumAmount) {
      logger.debug("[{}] - balance value is less then minimum [balance: {} < minimum: {}]", this, balanceValue, minimumAmount);
      channel.fireGenericEvent(lessThanMinimumEvent, balanceValue);
    } else {
      channel.fireGenericEvent(moreThanMinimumEvent, balanceValue);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (smsText != null) {
      return true;
    } else if (smsWait.get() && System.currentTimeMillis() - smsTime.get() > smsTimeout.getPeriod()) {
      smsWait.set(false);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && text != null && Arrays.stream(checkBalanceHelper.getRegexes()).anyMatch(text::matches)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches", this, phoneNumber, text);
      smsWait.set(false);
      smsText = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - start waiting to receive sms with balance", this);
      smsWait.set(true);
      smsTime.set(System.currentTimeMillis());
    }
  }

  private SMSResponsePattern checkSMS(final String response) {
    for (SMSResponsePattern smsResponsePattern : smsResponsePatterns) {
      if (response.matches(smsResponsePattern.getResponsePattern())) {
        return smsResponsePattern;
      }
    }
    return null;
  }

}

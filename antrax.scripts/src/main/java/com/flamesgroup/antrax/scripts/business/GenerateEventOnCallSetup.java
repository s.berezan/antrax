/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

@Script(name = "generate event on call setup", doc = "generates event on call setup after number dialing begins")
public class GenerateEventOnCallSetup implements BusinessActivityScript, CallsListener {

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnCallSetup.class);

  private GenericEvent event = GenericEvent.uncheckedEvent("call_setup");

  private final AtomicBoolean callSetup = new AtomicBoolean();

  @ScriptParam(name = "event", doc = "event on call setup")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return this.event.getEvent();
  }

  @Override
  public String describeBusinessActivity() {
    return "event on call setup";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    /*
     * For the correct execution of the script sleep time must not be less than 1 second.
     * If sleep time is less than 1 second, then the generated event maybe not executed,
     * because the gsm module is busy - makes call setup.
     * Sleep time more than 1 second is also not recommended.
     */
    Thread.sleep(1000);

    if (callSetup.getAndSet(false)) {
      logger.debug("[{}] - {} generate event {}", this, channel, event);
      event.fireEvent(channel);
    } else {
      logger.debug("[{}] - call ended before it generated the event {}", this, event);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return callSetup.get();
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    callSetup.set(false);
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
    callSetup.set(true);
  }

}

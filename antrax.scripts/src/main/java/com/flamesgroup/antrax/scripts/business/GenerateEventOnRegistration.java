/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.CalendarPeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Script(name = "generate event on registration", doc = "generates specified number of events on register of sim card in GSM")
public class GenerateEventOnRegistration implements BusinessActivityScript, StatefullScript, ActivityListener {

  private static final long serialVersionUID = 7832386722824665935L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnRegistration.class);

  private GenericEvent event = GenericEvent.uncheckedEvent("registrationEvent");

  private CalendarPeriod period = CalendarPeriod.HOUR;
  private int countLimit;

  private boolean isShouldStartBusinessActivity;

  @StateField
  private volatile int eventCount;
  @StateField
  private volatile long lastPeriod;
  @StateField
  private CalendarPeriod lastPeriodState;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event to generate")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "period", doc = "time period to generate events")
  public void setPeriod(final CalendarPeriod period) {
    this.period = period;
  }

  public CalendarPeriod getPeriod() {
    return period;
  }

  @ScriptParam(name = "count limit", doc = "numbers of events that will be generated per period")
  public void setCountLimit(final int limit) {
    this.countLimit = limit;
  }

  public int getCountLimit() {
    return countLimit;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - generate event {}", this, event);
    event.fireEvent(channel);
    eventCount++;
    isShouldStartBusinessActivity = false;
    lastPeriodState = period;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (!isShouldStartBusinessActivity) {
      return false;
    }

    if (countLimit == 0) {
      return true;
    }

    long currentPeriod = period.getCurrentPeriod();
    if (currentPeriod > lastPeriod || period != lastPeriodState) {
      logger.debug("[{}] - starting new period", this);
      eventCount = 0;
      lastPeriod = currentPeriod;
      lastPeriodState = period;
      saver.save();
    }

    if (eventCount < countLimit) {
      return true;
    } else {
      isShouldStartBusinessActivity = false;
      return false;
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "reg event [" + event + "]: eventCount:" + eventCount;
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    isShouldStartBusinessActivity = true;
  }

  @Override
  public void handleActivityEnded() {
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.ReplacePattern;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import org.slf4j.Logger;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsFilterBase extends FilterBase implements StatefullScript, SMSListener {

  private static final long serialVersionUID = 3542689119253033519L;

  @StateField
  protected volatile long limit;
  @StateField
  protected volatile long countSendSms;
  @StateField
  protected volatile LocalDate prevDay;

  protected VariableLong limitPerDay = new VariableLong(40, 50);
  private ScriptSaver saver = new ScriptSaver();
  private List<String> textDeniedPatternList = new ArrayList<>();
  private List<String> textAllowedPatternList = new ArrayList<>();

  private List<ReplacePattern> replacePatterns = new ArrayList<>();
  private boolean isReplaceAllPatterns;


  public SmsFilterBase(final Logger logger) {
    super(logger);
  }

  @ScriptParam(name = "limit per day", doc = "limit of sms per day")
  public void setLimitPerDay(final VariableLong limitPerDay) {
    this.limitPerDay = limitPerDay;
  }

  public VariableLong getLimitPerDay() {
    return limitPerDay;
  }

  @ScriptParam(name = "denied text pattern", doc = "denied text pattern")
  public void addDeniedTextPattern(final String textPattern) {
    this.textDeniedPatternList.add(textPattern);
  }

  @ScriptParam(name = "allowed text pattern", doc = "allowed text pattern")
  public void addAllowedTextPattern(final String textPattern) {
    this.textAllowedPatternList.add(textPattern);
  }

  public String getDeniedTextPattern() {
    return "$^";
  }

  public String getAllowedTextPattern() {
    return ".*(?s).*";
  }

  @ScriptParam(name = "replace text pattern", doc = "regular expression which marked what will be replaced by text replace by")
  public void addTextReplacePattern(final ReplacePattern replacePattern) {
    this.replacePatterns.add(replacePattern);
  }

  public ReplacePattern getTextReplacePattern() {
    return new ReplacePattern("$", "");
  }

  @ScriptParam(name = "replace all patterns", doc = "if true replace all patterns or else replace only first matches pattern")
  public void setReplaceAllPatterns(final boolean isReplaceAllPatterns) {
    this.isReplaceAllPatterns = isReplaceAllPatterns;
  }

  public boolean getReplaceAllPatterns() {
    return isReplaceAllPatterns;
  }

  protected String baseSubstituteText(final String originText) {
    String res = originText;
    try {
      for (ReplacePattern replacePattern : replacePatterns) {
        Pattern atResponsePattern = Pattern.compile(replacePattern.getPattern());
        Matcher matcher = atResponsePattern.matcher(res);
        if (matcher.matches() && matcher.groupCount() != 0) {
          res = res.replaceAll(replacePattern.getPattern(), replacePattern.getReplaceBy());
          if (!isReplaceAllPatterns) {
            return res;
          }
        }
      }
    } catch (Exception e) {
      logger.error("[{}] - return original res: [{}], such as Exception occurred", this, res, e);
    }
    return res;
  }

  public boolean isAcceptsSmsParts(final int parts) {
    logger.info("[{}] - parts: [{}]", this, parts);
    if (prevDay == null || LocalDate.now(ZoneId.systemDefault()).isAfter(prevDay)) {
      logger.info("[{}] - in", this);
      prevDay = LocalDate.now(ZoneId.systemDefault());
      limit = limitPerDay.random();
      countSendSms = 0;
      saver.save();
    }
    logger.info("[{}] - countSendSms: [{}], limit: [{}]", this, countSendSms, limit);
    return countSendSms + parts <= limit;
  }

  public boolean isAcceptsSmsText(final String text) {
    for (String pattern : textDeniedPatternList) {
      if (text.matches(pattern)) {
        return false;
      }
    }
    for (String pattern : textAllowedPatternList) {
      if (text.matches(pattern)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    countSendSms += parts;
    saver.save();
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

}

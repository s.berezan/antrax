/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.CallHelper;
import com.flamesgroup.antrax.helper.business.CallHelper.CallStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

@Script(name = "call", doc = "waits until event occured and then calls to the generated phone number")
public class Call implements BusinessActivityScript, GenericEventListener, RegistryAccessListener, CallsListener, IncomingCallsListener {

  private static final Logger logger = LoggerFactory.getLogger(Call.class);

  private PhoneNumberGenerator phoneNumberGenerator = new PhoneNumberGenerator("(8050).......");
  private TimeInterval minimumActiveDuration = new TimeInterval(TimePeriod.inSeconds(10), TimePeriod.inSeconds(15));
  private TimeInterval maximumActiveDuration = new TimeInterval(TimePeriod.inMinutes(1), TimePeriod.inMinutes(2));
  private TimeInterval maximumAttemptDuration = new TimeInterval(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(30), TimePeriod.inMinutes(1) + TimePeriod.inSeconds(50));
  private int callAttemptsCount = 3;
  private String event = "call";
  private boolean requireActive = true;
  private String eventOnTrue = "call_true";
  private String eventOnFalse = "call_false";
  private String registryPath = "call.numbers";
  private boolean dropCalls = true;
  private int activityAttemptsCount = 0;

  private volatile GenericEvent caughtEvent;
  private transient RegistryAccess registry;

  private final AtomicBoolean inCall = new AtomicBoolean();

  @ScriptParam(name = "number pattern", doc = "phone number pattern. Pattern consists of numbers, asterisk (any number) and set of number put in []. For example 8(068|063|099)******** will generate one of the 8068, 8063 or 8099 and random number")
  public void setPhoneNumberGenerator(final PhoneNumberGenerator phoneNumberGenerator) {
    this.phoneNumberGenerator = phoneNumberGenerator;
  }

  public PhoneNumberGenerator getPhoneNumberGenerator() {
    return phoneNumberGenerator;
  }

  @ScriptParam(name = "max active duration", doc = "call will be limited with this duration")
  public void setMaximumActiveDuration(final TimeInterval maximumActiveTime) {
    this.maximumActiveDuration = maximumActiveTime;
  }

  public TimeInterval getMaximumActiveDuration() {
    return maximumActiveDuration;
  }

  @ScriptParam(name = "min active duration", doc = "if call duration will be less than this value, call comes failed")
  public void setMinimumActiveDuration(final TimeInterval minimumActiveTime) {
    this.minimumActiveDuration = minimumActiveTime;
  }

  public TimeInterval getMinimumActiveDuration() {
    return minimumActiveDuration;
  }

  @ScriptParam(name = "max attempt duration", doc = "max waiting time before the call will become active")
  public void setMaximumAttemptDuration(final TimeInterval maximumAttemptDuration) {
    this.maximumAttemptDuration = maximumAttemptDuration;
  }

  public TimeInterval getMaximumAttemptDuration() {
    return maximumAttemptDuration;
  }

  @ScriptParam(name = "call attempts count", doc = "how many call attempts should be made on failure to the same number")
  public void setCallAttemptsCount(final int callAttemptsCount) {
    this.callAttemptsCount = callAttemptsCount;
  }

  public int getCallAttemptsCount() {
    return callAttemptsCount;
  }

  @ScriptParam(name = "require active", doc = "call be success if require active is false")
  public void setRequireActive(final boolean requireActive) {
    this.requireActive = requireActive;
  }

  public boolean getRequireActive() {
    return requireActive;
  }

  @ScriptParam(name = "event on true", doc = "this event will be generated when call was true")
  public void setEventOnTrue(final String eventOnTrue) {
    this.eventOnTrue = eventOnTrue;
  }

  public String getEventOnTrue() {
    return eventOnTrue;
  }

  @ScriptParam(name = "event on false", doc = "this event will be generated when call was false")
  public void setEventOnFalse(final String eventOnFalse) {
    this.eventOnFalse = eventOnFalse;
  }

  public String getEventOnFalse() {
    return eventOnFalse;
  }

  @ScriptParam(name = "event", doc = "event to call")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "path in registry", doc = "path to the registry key, which contains numbers for call. Number take randomly from registry. If registry empty, will use number pattern")
  public void setRegistryPath(final String path) {
    this.registryPath = path;
  }

  public String getRegistryPath() {
    return registryPath;
  }

  @ScriptParam(name = "drop calls", doc = "drop outgoing or incoming call to processing script call. If drop calls doesn't set and call is active on channel - script call will be failed")
  public void setDropCalls(final boolean dropCalls) {
    this.dropCalls = dropCalls;
  }

  @ScriptParam(name = "activity attempts count", doc = "how many activity attempts should be made on failure to another number")
  public void setActivityAttemptsCount(final int activityAttemptsCount) {
    this.activityAttemptsCount = activityAttemptsCount;
  }

  public boolean getDropCalls() {
    return dropCalls;
  }

  public int getActivityAttemptsCount() {
    return activityAttemptsCount;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  @Override
  public String describeBusinessActivity() {
    return "call";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (inCall.get() && !dropCalls) {
      logger.info("[{}] - can't process call by event [{}], while another call is active", this, caughtEvent);
      caughtEvent.respondFailure(channel, "failedToCall: another call is active");
      caughtEvent = null;
      return;
    }

    PhoneNumberGenerator localPhoneNumberGenerator = phoneNumberGenerator;
    RegistryEntry[] entries = null;
    if (registry != null) {
      entries = registry.listEntries(registryPath, Integer.MAX_VALUE);
    }

    CallStatus callStatus;
    int activityAttempts = 0;
    do {
      if (entries != null && entries.length != 0) {
        int index = new Random(System.currentTimeMillis()).nextInt(entries.length);
        String value = entries[index].getValue();
        logger.debug("[{}] - use number [{}] from registry", this, value);
        try {
          localPhoneNumberGenerator = new PhoneNumberGenerator(value);
        } catch (IllegalArgumentException e) {
          logger.warn("[{}] - illegal phone number generator pattern {}", this, value);
          caughtEvent = null;
          return;
        }
      } else {
        logger.debug("[{}] - use phoneNumberGenerator [{}]", this, phoneNumberGenerator);
      }

      CallHelper callHelper = new CallHelper(localPhoneNumberGenerator);
      callHelper.setRequiresActive(requireActive);
      callHelper.setMaximumActiveDuration(maximumActiveDuration);
      callHelper.setMaximumAttemptDuration(maximumAttemptDuration);
      callHelper.setMinimumActiveDuration(minimumActiveDuration);

      try {
        callStatus = callHelper.call(channel, callAttemptsCount);
      } catch (Exception e) {
        callStatus = CallStatus.ERROR;
      }

      if (callStatus == CallStatus.OK) {
        break;
      } else {
        Thread.sleep(new TimeInterval(TimePeriod.inSeconds(5), TimePeriod.inSeconds(10)).random());
      }
    } while (++activityAttempts <= activityAttemptsCount);

    if (callStatus == CallStatus.OK) {
      caughtEvent.respondSuccess(channel);
      logger.debug("[{}] - event {} succeed", this, caughtEvent);
      channel.fireGenericEvent(eventOnTrue);
    } else {
      caughtEvent.respondFailure(channel, "failedToCall: " + callStatus);
      logger.debug("[{}] - event {} failed", this, caughtEvent);
      channel.fireGenericEvent(eventOnFalse);
    }

    caughtEvent = null;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - caught event: {}", this, event);
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public String toString() {
    return "Call script";
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
    inCall.compareAndSet(false, true);
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    inCall.compareAndSet(true, false);
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    inCall.compareAndSet(false, true);
  }

  @Override
  public void handleIncomingCallAnswered() {
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    inCall.compareAndSet(true, false);
  }

}

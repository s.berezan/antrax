/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils.scriplet;

import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.commons.Pair;

import java.util.LinkedList;
import java.util.List;

public class ScripletBlock extends BaseScriplet {

  private static final long serialVersionUID = -3949520490882636800L;

  ActivityScriplet first;
  private final LinkedList<Pair<ScripletOperation, ActivityScriplet>> siblings = new LinkedList<>();

  public ScripletBlock(final ActivityScriplet first) {
    this.first = first;
    setMeAsParent(first);
  }

  private void setMeAsParent(final ActivityScriplet scriplet) {
    if (scriplet instanceof ScripletBlock) {
      ((ScripletBlock) scriplet).setParent(this);
    } else if (scriplet instanceof ScripletAtom) {
      ((ScripletAtom) scriplet).setParent(this);
    }
  }

  public void addSibling(final ScripletOperation op, final ActivityScriplet scriplet) {
    this.siblings.add(new Pair<>(op, scriplet));
    setMeAsParent(scriplet);
  }

  @Override
  public String toString() {
    StringBuilder retval = new StringBuilder();
    retval.append("(");
    retval.append(first);
    for (Pair<ScripletOperation, ActivityScriplet> p : siblings) {
      retval.append(" ");
      retval.append(p.first());
      retval.append(" ");
      retval.append(p.second());
    }
    retval.append(")");
    return retval.toString();
  }

  public ActivityScriplet getFirst() {
    return first;
  }

  public List<Pair<ScripletOperation, ActivityScriplet>> getSiblings() {
    return siblings;
  }

  public void replaceScriplet(final ActivityScriplet oldChild, final ActivityScriplet newChild) {
    if (first == oldChild) {
      first = newChild;
      setMeAsParent(newChild);
      return;
    }
    Pair<ScripletOperation, ActivityScriplet> pair = findPair(oldChild);
    int index = siblings.indexOf(pair);
    siblings.remove(index);
    siblings.add(index, new Pair<>(pair.first(), newChild));
    setMeAsParent(newChild);
  }

  public void setOperation(final ActivityScriplet scriplet, final ScripletOperation op) {
    Pair<ScripletOperation, ActivityScriplet> pair = findPair(scriplet);
    int index = siblings.indexOf(pair);
    siblings.remove(index);
    siblings.add(index, new Pair<>(op, scriplet));
  }

  public void insertSibling(final ActivityScriplet locator, final ScripletOperation op, final ActivityScriplet candidate) {
    Pair<ScripletOperation, ActivityScriplet> candPair = new Pair<>(op, candidate);
    setMeAsParent(candidate);
    if (locator == first) {
      siblings.addFirst(candPair);
    } else {
      int index = siblings.indexOf(findPair(locator));
      siblings.add(index + 1, candPair);
    }
  }

  private Pair<ScripletOperation, ActivityScriplet> findPair(final ActivityScriplet scriplet) {
    Pair<ScripletOperation, ActivityScriplet> pair = null;
    for (Pair<ScripletOperation, ActivityScriplet> p : siblings) {
      if (p.second() == scriplet) {
        pair = p;
      }
    }
    if (pair == null) {
      throw new IllegalArgumentException("Can't find " + scriplet + " in siblings");
    }
    return pair;
  }

  public boolean canJoin() {
    return siblings.size() >= 2;
  }

  public void join(final ActivityScriplet a, final ActivityScriplet b) {
    if (!canJoin()) {
      throw new IllegalStateException(this + " is already joined");
    }
    if (a == first) {
      if (siblings.getFirst().second() != b) {
        throw new IllegalArgumentException("First and Second arguments must be followed");
      }
      Pair<ScripletOperation, ActivityScriplet> second = siblings.removeFirst();
      ScripletBlock newFirst = new ScripletBlock(first);
      newFirst.addSibling(second.first(), second.second());
      this.first = newFirst;
      newFirst.setParent(this);
    } else {
      Pair<ScripletOperation, ActivityScriplet> pa = findPair(a);
      Pair<ScripletOperation, ActivityScriplet> pb = findPair(b);
      int indexA = siblings.indexOf(pa);
      siblings.remove(pa);
      siblings.remove(pb);
      ScripletBlock joined = new ScripletBlock(pa.second());
      joined.addSibling(pb.first(), pb.second());
      siblings.add(indexA, new Pair<>(pa.first(), joined));
      joined.setParent(this);
    }
  }

  public boolean canSplit() {
    return siblings.size() == 1 && getParent() != null;
  }

  public void split() {
    if (!canSplit()) {
      throw new IllegalStateException("Can't split");
    }
    ScripletBlock parent = getParent();
    if (parent.first == this) {
      parent.first = this.first;
      parent.siblings.addFirst(siblings.getFirst());
      parent.setMeAsParent(this.first);
    } else {
      Pair<ScripletOperation, ActivityScriplet> pair = parent.findPair(this);
      int index = parent.siblings.indexOf(pair);
      parent.siblings.remove(pair);
      parent.siblings.add(index, new Pair<>(pair.first(), first));
      parent.siblings.add(index + 1, siblings.getFirst());
      parent.setMeAsParent(siblings.getFirst().second());
      parent.setMeAsParent(first);
    }
  }

  private ScripletAtom find(final String script, final ActivityScriplet scriplet) {
    ScripletAtom retval;
    if (scriplet instanceof ScripletBlock) {
      ScripletBlock block = (ScripletBlock) scriplet;
      retval = find(script, block.getFirst());
      if (retval != null) {
        return retval;
      }
      for (Pair<ScripletOperation, ActivityScriplet> p : block.getSiblings()) {
        retval = find(script, p.second());
        if (retval != null) {
          return retval;
        }
      }

    } else if (scriplet instanceof ScripletAtom) {
      if (((ScripletAtom) scriplet).getScript().equals(script)) {
        return (ScripletAtom) scriplet;
      }
    }
    return null;
  }

  public ScripletAtom find(final String s) {
    return find(s, this);
  }

  public boolean canMoveLeft(final ScripletAtom item) {
    if (siblings.isEmpty()) {
      return false;
    }
    if (item == first) {
      return getParent() != null;
    }
    return true;
  }

  public void moveLeft(final ActivityScriplet item) {
    if (siblings.isEmpty()) {
      throw new IllegalStateException("Can't move left, I'am alone here");
    } else {
      if (item == first) {
        if (getParent() == null) {
          throw new IllegalStateException("Can't move left");
        }
        getParent().replaceScriplet(this, this.first);
        if (siblings.size() > 1) {
          getParent().insertSibling(this.first, siblings.getFirst().first(), this);
          this.first = siblings.removeFirst().second();
        } else {
          getParent().insertSibling(this.first, siblings.getFirst().first(), siblings.getFirst().second());
        }
      } else {
        int index = siblings.indexOf(findPair(item));
        if (index == 0) {
          if (first instanceof ScripletBlock) {
            ((ScripletBlock) first).addSibling(siblings.getFirst().first(), siblings.getFirst().second());
            siblings.removeFirst();
            if (siblings.isEmpty()) {
              setSiblings(((ScripletBlock) first).siblings);
              setFirst(((ScripletBlock) first).first);
            }
          } else {
            Pair<ScripletOperation, ActivityScriplet> next = siblings.removeFirst();
            siblings.addFirst(new Pair<>(next.first(), first));
            this.first = next.second();
          }
        } else {
          Pair<ScripletOperation, ActivityScriplet> prev = siblings.get(index - 1);
          Pair<ScripletOperation, ActivityScriplet> curr = siblings.get(index);
          if (prev.second() instanceof ScripletBlock) {
            ((ScripletBlock) prev.second()).addSibling(curr.first(), curr.second());
            remove(curr.second());
          } else {
            siblings.remove(prev);
            siblings.remove(curr);
            siblings.add(index - 1, new Pair<>(prev.first(), curr.second()));
            siblings.add(index, new Pair<>(curr.first(), prev.second()));
          }
        }
      }
    }
  }

  private void setFirst(final ActivityScriplet first) {
    this.first = first;
    setMeAsParent(this.first);
  }

  private void setSiblings(final LinkedList<Pair<ScripletOperation, ActivityScriplet>> siblings) {
    this.siblings.clear();
    for (Pair<ScripletOperation, ActivityScriplet> p : siblings) {
      addSibling(p.first(), p.second());
    }
  }

  public boolean canMoveRight(final ScripletAtom item) {
    if (siblings.isEmpty()) {
      return false;
    }
    if (item == siblings.getLast().second()) {
      return getParent() != null;
    }
    return true;
  }

  public void moveRight(final ActivityScriplet item) {
    if (siblings.isEmpty()) {
      throw new IllegalStateException("Can't move right");
    } else {
      if (item == first) {
        Pair<ScripletOperation, ActivityScriplet> next = siblings.removeFirst();
        if (next.second() instanceof ScripletBlock) {
          ((ScripletBlock) next.second()).prependSibling(this.first, next.first());
          this.setFirst(next.second());
          if (siblings.isEmpty()) {
            setSiblings(((ScripletBlock) first).siblings);
            setFirst(((ScripletBlock) first).first);
          }
        } else {
          siblings.addFirst(new Pair<>(next.first(), first));
          this.first = next.second();
        }
      } else if (item == siblings.getLast().second()) {
        if (getParent() == null) {
          throw new IllegalStateException("Can't move righ, I'm the most top");
        }
        Pair<ScripletOperation, ActivityScriplet> last = siblings.removeLast();
        getParent().insertSibling(this, last.first(), last.second());
        if (siblings.isEmpty()) {
          getParent().replaceScriplet(this, this.first);
        }
      } else {
        int index = siblings.indexOf(findPair(item));
        Pair<ScripletOperation, ActivityScriplet> curr = siblings.get(index);
        Pair<ScripletOperation, ActivityScriplet> next = siblings.get(index + 1);
        if (next.second() instanceof ScripletBlock) {
          ((ScripletBlock) next.second()).prependSibling(curr.second(), curr.first());
          remove(curr.second());
        } else {
          remove(curr.second());
          remove(next.second());
          siblings.add(index, new Pair<>(curr.first(), next.second()));
          siblings.add(index + 1, new Pair<>(next.first(), curr.second()));
        }
      }
    }
  }

  private void prependSibling(final ActivityScriplet item, final ScripletOperation op) {
    siblings.addFirst(new Pair<>(op, first));
    setFirst(item);
  }

  public boolean canRemove(final ActivityScriplet item) {
    return siblings.size() > 0;
  }

  public void remove(final ActivityScriplet item) {
    if (first == item) {
      first = siblings.removeFirst().second();
    } else {
      siblings.remove(findPair(item));
    }

    if (getParent() != null && siblings.isEmpty()) {
      getParent().replaceScriplet(this, this.first);
    }
  }

  @Override
  public SimpleActivityScript createActivityScript() throws Exception {
    SimpleActivityScript curr = first.createActivityScript();
    for (Pair<ScripletOperation, ActivityScriplet> p : siblings) {
      if (p.first() == ScripletOperation.AND) {
        curr = curr.and(p.second().createActivityScript());
      } else {
        curr = curr.or(p.second().createActivityScript());
      }
    }
    return curr;
  }
}

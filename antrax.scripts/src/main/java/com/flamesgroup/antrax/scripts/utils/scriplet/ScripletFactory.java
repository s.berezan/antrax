/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils.scriplet;

import com.flamesgroup.antrax.helper.activity.ActivityTimeoutByEvent;
import com.flamesgroup.antrax.helper.activity.AllowActivityAfter24hPassed;
import com.flamesgroup.antrax.helper.activity.AllowActivityAfterCallDurationPassed;
import com.flamesgroup.antrax.helper.activity.AllowActivityAfterMidnight;
import com.flamesgroup.antrax.helper.activity.AllowActivityAfterTimeoutPassed;
import com.flamesgroup.antrax.helper.activity.AllowActivityInDesiredPeriod;
import com.flamesgroup.antrax.helper.activity.AllowActivityInRandomPeriod;
import com.flamesgroup.antrax.helper.activity.AllowRegistrationInDesiredPeriod;
import com.flamesgroup.antrax.helper.activity.BreakActivityAfterTimeoutPassed;
import com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerActivity;
import com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerDay;
import com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerHour;
import com.flamesgroup.antrax.helper.activity.LimitCallDurationPerActivity;
import com.flamesgroup.antrax.helper.activity.LimitCallDurationPerDay;
import com.flamesgroup.antrax.helper.activity.LimitCallDurationPerMonth;
import com.flamesgroup.antrax.helper.activity.LimitCallDurationPerMonthPerMinuteBilling;
import com.flamesgroup.antrax.helper.activity.LimitCallsPerDay;
import com.flamesgroup.antrax.helper.activity.LimitSmsCountPerActivity;
import com.flamesgroup.antrax.helper.activity.LimitSmsCountPerDay;
import com.flamesgroup.antrax.helper.activity.LimitSuccessfullCallsPerActivity;
import com.flamesgroup.antrax.helper.activity.LimitSuccessfullCallsPerHour;
import com.flamesgroup.antrax.helper.activity.SeparateActivityWithDelay;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.ActivityTimeoutByEventScriplet;
import com.flamesgroup.antrax.scripts.utils.AllowActivityInRandomPeriodScriplet;
import com.flamesgroup.antrax.scripts.utils.LimitCallDurationPerMonthScriplet;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

import java.util.HashMap;
import java.util.Map;

public class ScripletFactory {

  private static final Map<String, Class<? extends SimpleActivityScript>> scripts = new HashMap<>();
  private static final Map<Class<? extends SimpleActivityScript>, String> setters = new HashMap<>();
  private static final Map<Class<? extends SimpleActivityScript>, Object> defaults = new HashMap<>();

  static {
    add(AllowActivityAfterCallDurationPassed.class, "setCallDurationLimit", new TimeInterval(TimePeriod.inMinutes(15), TimePeriod.inMinutes(20)));
    add(AllowActivityAfter24hPassed.class, null, null);
    add(AllowActivityAfterTimeoutPassed.class, "setTimeoutLimit", new TimeInterval(TimePeriod.inHours(2), TimePeriod.inHours(24)));
    add(LimitCallAttemptsPerActivity.class, "setAttemptsLimit", new VariableLong(10, 15));
    add(LimitSuccessfullCallsPerActivity.class, "setCallsLimit", new VariableLong(2, 10));
    add(LimitCallAttemptsPerDay.class, "setCallAttemptsLimit", new VariableLong(30, 40));
    add(LimitCallDurationPerActivity.class, "setCallDurationLimit", new TimeInterval(TimePeriod.inMinutes(2), TimePeriod.inMillis(5)));
    add(LimitCallDurationPerDay.class, "setCallDurationLimit", new TimeInterval(TimePeriod.inMinutes(30), TimePeriod.inHours(1)));
    add(LimitCallDurationPerMonth.class, "setActivityTimeoutByEventScriplet", new LimitCallDurationPerMonthScriplet(1, new TimeInterval(TimePeriod.inHours(5), TimePeriod.inHours(10))));
    add(LimitCallsPerDay.class, "setCallsLimit", new VariableLong(20, 30));
    add(SeparateActivityWithDelay.class, "setDelay", new TimeInterval(TimePeriod.inHours(1), TimePeriod.inHours(2)));
    add(AllowActivityInDesiredPeriod.class, "setPeriod", new TimeInterval(TimePeriod.inHours(0), TimePeriod.inHours(24)));
    add(BreakActivityAfterTimeoutPassed.class, "setTimeoutLimit", new TimeInterval(TimePeriod.inHours(3), TimePeriod.inHours(3)));
    add(ActivityTimeoutByEvent.class, "setActivityTimeoutByEventScriplet", new ActivityTimeoutByEventScriplet("activity_timeout", new TimeInterval(TimePeriod.inSeconds(20), TimePeriod.inMinutes(1))));
    add(LimitSmsCountPerActivity.class, "setSMSCountLimit", new VariableLong(2, 10));
    add(LimitSmsCountPerDay.class, "setSMSCountLimit", new VariableLong(10, 15));
    add(LimitCallDurationPerMonthPerMinuteBilling.class, "setCallDurationLimit", new TimeInterval(TimePeriod.inHours(5), TimePeriod.inHours(10)));
    add(AllowRegistrationInDesiredPeriod.class, "setPeriod", new TimeInterval(TimePeriod.inHours(0), TimePeriod.inHours(24)));
    add(AllowActivityAfterMidnight.class, null, null);
    add(AllowActivityInRandomPeriod.class, "setRandomPeriodScriplet", new AllowActivityInRandomPeriodScriplet(new TimeInterval(TimePeriod.inHours(7), TimePeriod.inHours(8)),
        new TimeInterval(TimePeriod.inHours(22), TimePeriod.inHours(23))));
    add(LimitCallAttemptsPerHour.class, "setCallAttemptsLimit", new VariableLong(10, 20));
    add(LimitSuccessfullCallsPerHour.class, "setCallsLimit", new VariableLong(5, 10));
  }

  private static void add(final Class<? extends SimpleActivityScript> s, final String setter, final Object defaultValue) {
    String name = camelCaseToHumanString(s.getSimpleName());
    scripts.put(name, s);
    if (setter != null) {
      setters.put(s, setter);
    }
    defaults.put(s, defaultValue);
  }

  private static String camelCaseToHumanString(final String name) {
    StringBuilder retval = new StringBuilder();
    for (int i = 0; i < name.length(); ++i) {
      char c = name.charAt(i);
      if (Character.isUpperCase(c)) {
        if (i > 0) {
          retval.append(" ");
        }
        retval.append(Character.toLowerCase(c));
      } else {
        retval.append(c);
      }
    }

    return retval.toString();
  }

  public static String[] listScripts() {
    return scripts.keySet().toArray(new String[0]);
  }

  public static ScripletAtom createScripletFor(final String script) {
    Class<? extends SimpleActivityScript> clazz = scripts.get(script);
    if (clazz == null) {
      throw new IllegalArgumentException("Can't find script " + script);
    }
    return new ScripletAtom(script, clazz, setters.get(clazz), defaults.get(clazz));
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.factor;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.helper.factor.PriorityField;
import com.flamesgroup.antrax.helper.factor.SortOrderField;

public abstract class FactorBase implements FactorEvaluationScript {

  private SortOrderField sortOrder = SortOrderField.ASCENDING;
  private PriorityField priority = PriorityField.ZERO;

  @ScriptParam(name = "priority", doc = "defines global priority for factor (in practice it is PRIORITY*HUGE_NUMBER)")
  public void setPriority(final PriorityField priority) {
    this.priority = priority;
  }

  public PriorityField getPriority() {
    return priority;
  }

  @ScriptParam(name = "sort order", doc = "factor value will be multiplied on this value")
  public void setSortOrder(final SortOrderField sortOrder) {
    this.sortOrder = sortOrder;
  }

  public SortOrderField getSortOrder() {
    return sortOrder;
  }

  protected int getMultiplier() {
    switch (sortOrder) {
      case ASCENDING:
        return 1;
      case DESCENDING:
        return -1;
      default:
        return 0;
    }
  }

}

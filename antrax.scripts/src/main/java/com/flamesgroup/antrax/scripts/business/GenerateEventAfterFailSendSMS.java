/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

@Script(name = "generate event after fail send SMS", doc = "generate event after fail send SMS")
public class GenerateEventAfterFailSendSMS implements BusinessActivityScript, SMSListener, StatefullScript {

  private static final long serialVersionUID = 1569716013821756160L;

  @StateField
  private volatile int smsCount;
  @StateField
  private long smsLimitValue;

  private boolean countConsecutiveSms = true;
  private VariableLong smsLimit = new VariableLong(20, 20);
  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "count consecutive sms", doc = "if an option is active it will count consecutive sms")
  public void setCountConsecutiveSms(final boolean countConsecutiveSms) {
    this.countConsecutiveSms = countConsecutiveSms;
  }

  public boolean getCountConsecutiveSms() {
    return countConsecutiveSms;
  }

  @ScriptParam(name = "event", doc = "name of generated event")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "sms limit", doc = "after this count of sms event will be generated")
  public void setSMSLimit(final VariableLong smsLimit) {
    this.smsLimit = smsLimit;
    smsLimitValue = smsLimit.random();
  }

  public VariableLong getSMSLimit() {
    return smsLimit;
  }

  @Override
  public String describeBusinessActivity() {
    return "event after fail SMS";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    smsCount = 0;
    smsLimitValue = smsLimit.random();
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return smsCount >= smsLimitValue;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    if (countConsecutiveSms) {
      smsCount = 0;
      saver.save();
    }
  }

  @Override
  public void handleFailSentSMS(final int parts) {
    smsCount += parts;
    saver.save();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Script(name = "events chain", doc = "generates events sequentially until one of them will successfully")
public class EventsChain implements BusinessActivityScript, GenericEventListener, StatefullScript {

  private static final long serialVersionUID = 1917290696263537300L;

  private static final Logger logger = LoggerFactory.getLogger(EventsChain.class);

  private String initialEvent = "chainInitialEvent";
  private final List<GenericEvent> events = new LinkedList<>();
  private TimePeriod waitEventTimeout = new TimePeriod(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(40));

  @StateField
  private final List<GenericEvent> alreadyDoneEvents = new LinkedList<>();
  @StateField
  private volatile boolean eventProcessing = false;
  @StateField
  private volatile long generateEventTime;
  @StateField
  private volatile boolean success = false;
  @StateField
  private final List<GenericEvent> incomingEvents = Collections.synchronizedList(new LinkedList<GenericEvent>());

  private final ScriptSaver saver = new ScriptSaver();

  synchronized GenericEvent getCurrentEvent() {
    Iterator<GenericEvent> doneIter = alreadyDoneEvents.iterator();
    Iterator<GenericEvent> newIter = events.iterator();
    while (doneIter.hasNext() && newIter.hasNext()) {
      GenericEvent doneE = doneIter.next();
      GenericEvent newE = newIter.next();

      if (!doneE.getEvent().equals(newE.getEvent())) {
        doneIter.remove();
        while (doneIter.hasNext()) {
          doneIter.next();
          doneIter.remove();
        }
        return newE;
      }
    }
    if (newIter.hasNext()) {
      return newIter.next();
    }
    return null;
  }

  private synchronized boolean hasEvent() {
    return getCurrentEvent() != null;
  }

  synchronized void nextEvent() {
    alreadyDoneEvents.add(getCurrentEvent());
    saver.save();
  }

  @ScriptParam(name = "initial event", doc = "after this event chain begins sending events")
  public void setInitialEvent(final String event) {
    this.initialEvent = event;
  }

  public String getInitialEvent() {
    return initialEvent;
  }

  @ScriptParam(name = "event", doc = "event in chain")
  public void addEvent(final String event) {
    GenericEvent gevent = GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event));
    this.events.add(gevent);
  }

  public String getEvent() {
    return "chain event";
  }

  @ScriptParam(name = "wait event timeout", doc = "max time to wait event processing")
  public void setWaitEventTimeout(final TimePeriod waitEventTimeout) {
    this.waitEventTimeout = waitEventTimeout;
  }

  public TimePeriod getWaitEventTimeout() {
    return waitEventTimeout;
  }

  @Override
  public String describeBusinessActivity() {
    if (!hasEvent()) {
      return "none";
    } else {
      return "generate event [" + getCurrentEvent() + "] in chain";
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - counting current event from done events {} and events {}", this, alreadyDoneEvents, events);
    GenericEvent currentEvent = getCurrentEvent();
    if (currentEvent == null) {
      logger.debug("[{}] - event {} failed", this, initialEvent);
      incomingEvents.remove(0).respondFailure(channel, "All events from [" + events + "] failure");
      reset();
    } else {
      if (success) {
        logger.debug("[{}] - event {} succeed", this, currentEvent);
        incomingEvents.remove(0).respondSuccess(channel);
        reset();
      } else {
        logger.debug("[{}] - generate event {} ", this, currentEvent);
        currentEvent.fireEvent(channel);
        generateEventTime = System.currentTimeMillis();
        eventProcessing = true;
      }
    }
    saver.save();
  }

  private void reset() {
    alreadyDoneEvents.clear();
    success = false;
  }

  private boolean isEventProcessing() {
    if (eventProcessing && System.currentTimeMillis() - generateEventTime > waitEventTimeout.getPeriod()) {
      nextEvent();
      eventProcessing = false;
      saver.save();
    }
    return eventProcessing;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return !isEventProcessing() && !incomingEvents.isEmpty(); //&& hasEvent();
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    logger.debug("[{}] - caught event {}", this, event);
    if (this.initialEvent.equals(event)) {
      incomingEvents.add(GenericEvent.wrapEvent(event, args));
      return;
    }
    GenericEvent currentEvent = getCurrentEvent();
    if (currentEvent != null) {
      if (currentEvent.isFailureResponce(event, args)) {
        nextEvent();
        eventProcessing = false;
      } else if (currentEvent.isSuccessResponce(event, args)) {
        success = true;
        eventProcessing = false;
      }
      saver.save();
    }
  }

  private String successedResponse(final String event) {
    return event + " success " + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + " failure " + Math.random() + System.nanoTime();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public String toString() {
    return String.format("EventsChain(done=%s, all=%s)", alreadyDoneEvents, events);
  }

}

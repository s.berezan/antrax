/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;

import java.io.Serializable;

@Script(name = "lock on event", doc = "lock if event occurs")
public class LockOnEvent implements BusinessActivityScript, GenericEventListener {

  public String event = "lockMe";

  private volatile GenericEvent eventCaugth;

  @ScriptParam(name = "event", doc = "event to lock")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return this.event;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      eventCaugth = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "lock sim if event occurs";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      channel.lock("Locked by event: " + eventCaugth.getEvent());
      eventCaugth.respondSuccess(channel);
    } finally {
      eventCaugth = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return eventCaugth != null;
  }

}

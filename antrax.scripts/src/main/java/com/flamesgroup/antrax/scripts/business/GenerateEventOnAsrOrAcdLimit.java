/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.PhoneNumber;

import java.util.Calendar;

@Script(name = "generate event on asr or acd limit", doc = "generates event when ASR or ACD reached the limit")
public class GenerateEventOnAsrOrAcdLimit implements BusinessActivityScript, CallsListener, SimDataListener {

  private TimePeriod beginPeriod = new TimePeriod(TimePeriod.inHours(6));
  private TimePeriod endPeriod = new TimePeriod(TimePeriod.inHours(24));
  private TimePeriod period = new TimePeriod(TimePeriod.inHours(1));
  private TimePeriod acdLimit = new TimePeriod(TimePeriod.inMinutes(4));
  private Chance asrLimit = new Chance(30);
  private boolean skipZeroCalls = false;
  private int minCalls = 10;
  private GenericEvent event = GenericEvent.uncheckedEvent("event");

  private SimData simData;

  @StateField
  private long startPeriod;
  @StateField
  private long calls;
  @StateField
  private boolean shouldInvoke = true;

  public TimePeriod getBeginPeriod() {
    return beginPeriod;
  }

  @ScriptParam(name = "begin time", doc = "begin time of period in which script will be work")
  public void setBeginPeriod(final TimePeriod beginPeriod) {
    this.beginPeriod = beginPeriod;
  }

  public TimePeriod getEndPeriod() {
    return endPeriod;
  }

  @ScriptParam(name = "end time", doc = "start time of period in which script will be work")
  public void setEndPeriod(final TimePeriod endPeriod) {
    this.endPeriod = endPeriod;
  }

  public TimePeriod getPeriod() {
    return period;
  }

  @ScriptParam(name = "period", doc = "period in which calculated minimum not zero calls to check ASR and ACD")
  public void setPeriod(final TimePeriod period) {
    this.period = period;
  }

  public TimePeriod getAcdLimit() {
    return acdLimit;
  }

  @ScriptParam(name = "acd", doc = "limit acd to generate event")
  public void setAcdLimit(final TimePeriod acdLimit) {
    this.acdLimit = acdLimit;
  }

  public Chance getAsrLimit() {
    return asrLimit;
  }

  @ScriptParam(name = "asr", doc = "limit asr to generate event")
  public void setAsrLimit(final Chance asrLimit) {
    this.asrLimit = asrLimit;
  }

  public boolean getSkipZeroCalls() {
    return skipZeroCalls;
  }

  @ScriptParam(name = "skip zero calls", doc = "skip zero calls when calculating the min calls")
  public void setSkipZeroCalls(final boolean skipZeroCalls) {
    this.skipZeroCalls = skipZeroCalls;
  }

  public int getMinCalls() {
    return minCalls;
  }

  @ScriptParam(name = "min calls", doc = "minimum calls to check ASR and ACD")
  public void setMinCalls(final int minCalls) {
    this.minCalls = minCalls;
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "event", doc = "event to generate")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    shouldInvoke = false;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    long current = getTimeOfDay();
    if (current > beginPeriod.getPeriod() && current < endPeriod.getPeriod()) {
      long period = this.period.getPeriod();
      if (period > 0 && current - startPeriod > period) {
        startPeriod = current;
        calls = 0;
        shouldInvoke = true;
      }

      int asr = calculateASR(simData.getSuccessfulCallsCount(), simData.getTotalCallsCount());
      long acd = calculateACD(simData.getCallDuration(), simData.getSuccessfulCallsCount());
      if (calls >= minCalls && (asr <= asrLimit.getChance() || acd <= acdLimit.getPeriod())) {
        return shouldInvoke;
      }
    }
    return false;
  }

  @Override
  public String describeBusinessActivity() {
    return "check asr and acd";
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (duration <= 0 && skipZeroCalls) {
      return;
    }
    calls++;
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  private int calculateASR(final int successfullCallsCount, final int totalCallsCount) {
    return (totalCallsCount == 0) ? 0 : 100 * successfullCallsCount / totalCallsCount;
  }

  private long calculateACD(final long callsDuration, final int successfullCallsCount) {
    return (successfullCallsCount == 0) ? 0 : (callsDuration / successfullCallsCount);
  }

  private long getTimeOfDay() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_YEAR, 1);
    cal.clear(Calendar.MONTH);
    cal.clear(Calendar.YEAR);
    cal.set(Calendar.ZONE_OFFSET, 0);
    return cal.getTimeInMillis();
  }

}

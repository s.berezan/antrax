/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.EventHelper;
import com.flamesgroup.antrax.helper.business.EventStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "generate event on fas", doc = "generates event after continious fas")
public class GenerateEventOnFAS implements BusinessActivityScript, GenericEventListener, CallsListener, StatefullScript {

  private static final long serialVersionUID = 1503835186399188471L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnFAS.class);

  private TimePeriod waitEventTimeout = new TimePeriod(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(40));
  private boolean lockOnFailure = true;
  private int limit = 3;
  volatile GenericEvent event;

  @StateField
  private volatile int fasCount = 0;
  @StateField
  private volatile boolean wasFAS = false;
  private volatile EventHelper eventHelper;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event to generate on fas")
  public void setEvent(final String event) {
    String failureResponce = "'" + event + "' fas failed " + Math.random() + System.nanoTime();
    String successResponce = "'" + event + "' fas successed " + Math.random() + System.nanoTime();
    this.event = GenericEvent.checkedEvent(event, successResponce, failureResponce);
  }

  public String getEvent() {
    return "lockMe";
  }

  @ScriptParam(name = "wait event timeout", doc = "max time to wait event processing")
  public void setWaitEventTimeout(final TimePeriod waitEventTimeout) {
    this.waitEventTimeout = waitEventTimeout;
  }

  public TimePeriod getWaitEventTimeout() {
    return waitEventTimeout;
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @ScriptParam(name = "FAS limit", doc = "after this count of FAS event will be generated")
  public void setLimit(final int limit) {
    this.limit = limit;
  }

  public int getLimit() {
    return limit;
  }

  @Override
  public String describeBusinessActivity() {
    if (eventHelper == null) {
      return "generating event [" + event + "] on fas";
    } else {
      return "locking on failure";
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (eventHelper == null) {
      eventHelper = new EventHelper(event, waitEventTimeout.getPeriod());
      logger.debug("[{}] - generate event {}", this, event);
      eventHelper.generateEvent(channel);
      fasCount = 0;
      logger.debug("[{}] - FAS count after event: {}", this, fasCount);
    } else {
      if (eventHelper.getStatus() == EventStatus.FAILED || eventHelper.getStatus() == EventStatus.TIMEOUT) {
        logger.debug("[{}] - {}", this, eventHelper.getFailReason());
        if (lockOnFailure) {
          logger.debug("[{}] - going to lock card", this);
          channel.lock("Locked: " + eventHelper.getFailReason());
        }
      }
      eventHelper = null;
    }
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (eventHelper == null) {
      return fasCount >= limit;
    } else {
      return eventHelper.isFinished();
    }
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (wasFAS) {
      fasCount++;
    } else {
      fasCount = 0;
    }
    logger.debug("[{}] - FAS count after call: {}", this, fasCount);
    wasFAS = false;
    logger.debug("[{}] - wasFAS: {}", this, wasFAS);
    saver.save();
  }

  @Override
  public void handleFAS() {
    wasFAS = true;
    logger.debug("[{}] - wasFAS: {}", this, wasFAS);
    saver.save();
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleGenericEvent(final String handledEvent, final Serializable... args) {
    logger.debug("[{}] - caught event {}", this, handledEvent);
    if (eventHelper != null) {
      eventHelper.handleEvent(handledEvent, args);
      saver.save();
    }
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.sms.SMSResponsePattern;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.PatternSyntaxException;

@Script(name = "check SMS", doc = "analyzes incoming SMS and generates events")
public class CheckSMS implements BusinessActivityScript, SMSListener, StatefullScript, GenericEventListener {

  private static final long serialVersionUID = -7420643424379964339L;

  private static final Logger logger = LoggerFactory.getLogger(CheckSMS.class);

  private String event = "check_sms";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));

  private final List<SMSResponsePattern> SMSResponsePatternList = new LinkedList<>();
  private boolean startFirst = true;
  private int amountOfSms = 1;

  private volatile String smsText;

  @StateField
  private final Map<SMSResponsePattern, Integer> smsResponsePatternCount = new HashMap<>();

  private final ScriptSaver saver = new ScriptSaver();

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private final AtomicLong smsTime = new AtomicLong();

  @ScriptParam(name = "event", doc = "check sms will be started after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  @ScriptParam(name = "start first", doc = "if checked, only the first event will be generate after coincidence")
  public void setStartFirst(final boolean startFirst) {
    this.startFirst = startFirst;
  }

  public boolean getStartFirst() {
    return startFirst;
  }

  @ScriptParam(name = "amount of sms", doc = "generate event after amount of sms")
  public void setAmountOfSms(final int amountOfSms) {
    this.amountOfSms = amountOfSms;
  }

  public int getAmountOfSms() {
    return amountOfSms;
  }

  @ScriptParam(name = "pattern for SMS/event for generate", doc = "regular expressins for parsing response and generating events")
  public void addSMSResponsePatternList(final SMSResponsePattern smsResponsePattern) {
    SMSResponsePatternList.add(smsResponsePattern);
  }

  public SMSResponsePattern getSMSResponsePatternList() {
    return new SMSResponsePattern(".*", "event");
  }

  @Override
  public String describeBusinessActivity() {
    return "check SMS";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (smsText == null) {
      smsTime.set(0);
      logger.debug("[{}] - waiting sms timeout", this);
      return;
    }

    try {
      for (SMSResponsePattern smsResponsePattern : SMSResponsePatternList) {
        if (smsText.matches(smsResponsePattern.getResponsePattern())) {
          logger.debug("[{}] - find pattern match {}", this, smsResponsePattern.getResponsePattern());
          Integer count = smsResponsePatternCount.compute(smsResponsePattern, (pattern, countSms) -> countSms == null ? 1 : ++countSms);
          if (count != null && count == amountOfSms) {
            logger.debug("[{}] - fireGenericEvent {}", this, smsResponsePattern.getResponseEvent());
            channel.fireGenericEvent(smsResponsePattern.getResponseEvent());
            smsResponsePatternCount.remove(smsResponsePattern);
          }
          saver.save();
          if (startFirst) {
            break;
          }
        }
      }
    } catch (PatternSyntaxException e) {
      logger.debug("[{}] - while check incoming SMS", this, e);
    } finally {
      smsTime.set(0);
      smsText = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (smsText != null) {
      return true;
    } else if (smsWait.get() && System.currentTimeMillis() - smsTime.get() > smsTimeout.getPeriod()) {
      smsWait.set(false);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && text != null && SMSResponsePatternList.stream().map(SMSResponsePattern::getResponsePattern).anyMatch(text::matches)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches", this, phoneNumber, text);
      smsWait.set(false);
      smsText = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - start waiting to receive sms", this);
      smsWait.set(true);
      smsTime.set(System.currentTimeMillis());
    }
  }
}

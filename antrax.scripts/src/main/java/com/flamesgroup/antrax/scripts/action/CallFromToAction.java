/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "call from -> to action", doc = "Makes call from one determined number to another one")
public class CallFromToAction extends CallBase {

  public CallFromToAction() {
    super("callFromTo", LoggerFactory.getLogger(CallFromToAction.class));
  }

  String extractSrcPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    return args[0].toString();
  }

  String extractDstPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    return args[1].toString();
  }

  @Override
  public boolean ensureCanExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!action.getAction().equals(actionName)) {
      return false;
    }

    PhoneNumber srcNumber = new PhoneNumber(extractSrcPhoneNumber(action));
    if (channel.getSimData().getPhoneNumber().equals(srcNumber)) {
      logger.debug("[{}] - found source card: {}", this, srcNumber);
      return true;
    }
    return false;
  }

  @Override
  public void executeConcreteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - channel {} is executing action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
    String dstNumberStr = replaceNumber(extractDstPhoneNumber(action));
    PhoneNumber dstNumber = new PhoneNumber(dstNumberStr);

    logger.debug("[{}] - channel {} is calling to {}", this, channel.getSimData().getPhoneNumber().toString(), dstNumber.toString());
    makeCall(channel, dstNumber);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.payment.UssdDialogOperator;
import com.flamesgroup.antrax.helper.business.payment.VaucherHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.helper.business.transfer.WrongResponseException;
import com.flamesgroup.antrax.helper.business.ussd.USSDResponsePattern;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Script(name = "USSD dialog pay", doc = "ussd dialog pays money after event caught")
public class UssdDialogPay implements BusinessActivityScript, RegistryAccessListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(UssdDialogPay.class);

  private static final String PAY_ANSWER_PATTERN = "\\$((?<begin>\\d{1,2})\\+(?<end>\\d{1,2}))?";
  private static final Pattern payAnswerPattern = Pattern.compile(PAY_ANSWER_PATTERN);

  private volatile GenericEvent eventCaugth;
  private transient RegistryAccess registry;
  private String event = "payMe";
  private int amount = 10;
  private double minimumAmount = 5;
  private boolean lockOnFail = true;
  private boolean skipIntermStates = true;
  private CheckBalanceHelper checkBalanceHelper;
  private UssdDialogOperator operator;
  private String ussdRequest = "*111#";
  private final List<USSDResponseAnswer> ussdResponseAnswers = new LinkedList<>();
  private final List<USSDResponsePattern> ussdResponsePatterns = new LinkedList<>();

  @ScriptParam(name = "balance check data", doc = "Check current balance of SIM card. Checking may take up to 4 minutes")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*");
  }

  @ScriptParam(name = "event", doc = "payment will be performed after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "amount", doc = "amount of money to pay")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "lock on fail", doc = "locks card on payment failure")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @ScriptParam(name = "skip intermediate states", doc = "skip the intermediate states like failed.1, failed.2, failed.3")
  public void setSkipIntermStates(final boolean skipIntermStates) {
    this.skipIntermStates = skipIntermStates;
  }

  public boolean getSkipIntermStates() {
    return skipIntermStates;
  }

  @ScriptParam(name = "minimum amount", doc = "minimum amount of money which is enough to cancel payment. Put 0 here to discard balance check")
  public void setMinimumAmount(final double amount) {
    this.minimumAmount = amount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "operator", doc = "operator name is used to determine registry key where to look for vauchers")
  public void setOperator(final UssdDialogOperator operator) {
    this.operator = operator;
  }

  public UssdDialogOperator getOperator() {
    return new UssdDialogOperator("");
  }

  @ScriptParam(name = "ussd", doc = "ussd dialog request")
  public void setUssdRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getUssdRequest() {
    return ussdRequest;
  }

  @ScriptParam(name = "ussd responses", doc = "regular expressing for parsing ussd response and generating answers. Allowed regex '$(\\d{1,2}+\\d{1,2})?' to send split or all digits of vaucher")
  public void addUSSDResponseAnswers(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswers.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswers() {
    return new USSDResponseAnswer(".*", "$", true);
  }

  @ScriptParam(name = "payed responses", doc = "regular expressions for parsing payed response and generating events")
  public void addUSSDResponsePattern(final USSDResponsePattern ussdResponsePattern) {
    ussdResponsePatterns.add(ussdResponsePattern);
  }

  public USSDResponsePattern getUSSDResponsePattern() {
    return new USSDResponsePattern(".*", "event");
  }

  @Override
  public String describeBusinessActivity() {
    return "ussd dialog payment";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) {
    logger.debug("[{}] - going to pay {}", this, channel);
    try {
      if (minimumAmount < 1 || checkBalanceHelper.getBalanceValue(channel, registry) < minimumAmount) {
        new VaucherHelper(operator.getOperator(), registry, amount, skipIntermStates) {

          @Override
          protected boolean executePayment(final String vaucher) throws Exception {
            logger.debug("[{}] - execute payment by ussd dialog with voucher [{}]", this, vaucher);

            logger.debug("[{}] - sending ussd: {}", this, ussdRequest);
            if (null == ussdRequest || 0 == ussdRequest.length()) {
              logger.error("[{}] - wrong ussd request: {}", this, ussdRequest);
              throw new Exception("Wrong ussd request: " + ussdRequest);
            }

            USSDSession ussdSession = null;
            try {
              ussdSession = channel.startUSSDSession(ussdRequest);

              String response = ussdSession.readResponse();
              logger.debug("[{}] - got response: {}", this, response);

              Iterator<USSDResponseAnswer> iterator = ussdResponseAnswers.iterator();
              while (iterator.hasNext()) {
                USSDResponseAnswer ussdResponseAnswer = iterator.next();
                if (!response.matches(ussdResponseAnswer.getResponsePattern())) {
                  logger.debug("[{}] - wrong response: {}, for regex: {}", this, response, ussdResponseAnswer.getResponsePattern());
                  throw new WrongResponseException("Wrong response: " + response);
                }

                Thread.sleep(new TimeInterval(TimePeriod.inSeconds(1), TimePeriod.inSeconds(2)).random());
                if (!ussdResponseAnswer.hasResponseAnswer()) {
                  logger.debug("[{}] - has no answer to response pattern [{}]", this, ussdResponseAnswer.getResponsePattern());
                  break;
                }

                String replacedResponse = response.replaceFirst(ussdResponseAnswer.getResponsePattern(), ussdResponseAnswer.getResponseAnswer().replaceFirst("\\$", "\\\\\\$"));
                Matcher payMatcher = payAnswerPattern.matcher(replacedResponse);
                if (payMatcher.matches()) { // sending vaucher
                  String begin = payMatcher.group("begin");
                  String end = payMatcher.group("end");

                  if (begin == null || end == null) { // the last answer with '$' symbol to send all digits of vaucher
                    logger.debug("[{}] - send voucher: {}", this, vaucher);
                    ussdSession.sendCommand(vaucher);
                    response = ussdSession.readResponse();
                    logger.debug("[{}] - got response: {}", this, response);

                    return checkPayedResponses(response, channel);
                  } else { // answer with regex to send one part of digits of vaucher
                    int beginIndex = Integer.parseInt(begin);
                    int endIndex = beginIndex + Integer.parseInt(end);
                    String partVaucher = vaucher.substring(beginIndex, endIndex);
                    logger.debug("[{}] - send one part of voucher: {}", this, partVaucher);
                    ussdSession.sendCommand(partVaucher);
                    response = ussdSession.readResponse();
                    logger.debug("[{}] - got response: {}", this, response);

                    if (!iterator.hasNext()) { // the last answer with last part of digits of vaucher
                      return checkPayedResponses(response, channel);
                    }
                  }
                } else { // sending next ussd dialog command
                  logger.debug("[{}] - send command: {}", this, replacedResponse);
                  ussdSession.sendCommand(replacedResponse);
                  response = ussdSession.readResponse();
                  logger.debug("[{}] - got response: {}", this, response);
                }
              }
              return false;
            } finally {
              if (null != ussdSession) {
                ussdSession.endSession();
              }
            }
          }

        }.pay();
      }
    } catch (Exception e) {
      logger.warn("failed to pay", e);
      eventCaugth.respondFailure(channel, e.getMessage());
      if (lockOnFail) {
        channel.lock("PaymentFailed: " + e.getMessage());
      }
    } finally {
      eventCaugth = null;
    }
  }

  private boolean checkPayedResponses(final String response, final RegisteredInGSMChannel channel) {
    for (USSDResponsePattern ussdResponsePattern : ussdResponsePatterns) {
      if (response.matches(ussdResponsePattern.getResponsePattern())) {
        eventCaugth.respondSuccess(channel);
        channel.fireGenericEvent(ussdResponsePattern.getResponseEvent());
        return true;
      }
    }
    logger.debug("[{}] - no one payed response pattern match with response: {}", this, response);
    return false;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return eventCaugth != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      eventCaugth = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

}

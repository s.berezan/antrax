/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.helper.business.transfer.WrongResponseException;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Script(name = "ussd action", doc = "Makes sending ussd")
public class USSDAction implements ActionProviderScript, IncomingCallsListener {

  private static final Logger logger = LoggerFactory.getLogger(USSDAction.class);

  private boolean sendUSSDInsideOneGroup = true;
  private String actionName = "ussd";
  private GenericEvent event = GenericEvent.uncheckedEvent("afterSendUSSD");
  private String numberPattern = "";
  private String numberReplacePattern = "";
  private int amount = 5;
  private String request;
  private final List<USSDResponseAnswer> responseAnswerList = new LinkedList<>();
  private boolean skipErrors = false;
  private GenericEvent eventOnSuccess = GenericEvent.uncheckedEvent("afterSuccessSendUSSD");
  private GenericEvent eventOnFailure = GenericEvent.uncheckedEvent("afterFailedSendUSSD");
  private int attemptsCount = 1;
  private TimeInterval attemptsInterval = new TimeInterval(10000, 15000);

  private final AtomicBoolean inIcomingCall = new AtomicBoolean(false);

  @ScriptParam(name = "send ussd inside one group", doc = "send ussd inside one group only")
  public void setSendUSSDInsideOneGroup(final boolean sendUSSDInsideOneGroup) {
    this.sendUSSDInsideOneGroup = sendUSSDInsideOneGroup;
  }

  public boolean getSendUSSDInsideOneGroup() {
    return sendUSSDInsideOneGroup;
  }

  @ScriptParam(name = "action name", doc = "action name")
  public void setAction(final String actionName) {
    this.actionName = actionName;
  }

  public String getAction() {
    return actionName;
  }

  @ScriptParam(name = "event", doc = "event to generate after action")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "number pattern", doc = "number pattern")
  public void setNumberPattern(final String numberPattern) {
    this.numberPattern = numberPattern;
  }

  public String getNumberPattern() {
    return numberPattern;
  }

  @ScriptParam(name = "number replace pattern", doc = "number replace pattern")
  public void setNumberReplacePattern(final String numberReplacePattern) {
    this.numberReplacePattern = numberReplacePattern;
  }

  public String getNumberReplacePattern() {
    return numberReplacePattern;
  }

  @ScriptParam(name = "amount", doc = "amount of money to transfer")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "ussd request", doc = "Sending ussd request pattern")
  public void setRequest(final String request) {
    this.request = request;
  }

  public String getRequest() {
    return "*121*number*amount*pin#";
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressins for parsing response and generating answers")
  public void addResponseAnswerList(final USSDResponseAnswer responseAnswer) {
    responseAnswerList.add(responseAnswer);
  }

  public USSDResponseAnswer getResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "skip errors", doc = "flag for skip errors or not")
  public void setSkipErrors(final boolean skipErrors) {
    this.skipErrors = skipErrors;
  }

  public boolean getSkipErrors() {
    return this.skipErrors;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if send ussd was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = GenericEvent.uncheckedEvent(eventOnSuccess);
  }

  public String getEventOnSuccess() {
    return eventOnSuccess.getEvent();
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if send ussd failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = GenericEvent.uncheckedEvent(eventOnFailure);
  }

  public String getEventOnFailure() {
    return eventOnFailure.getEvent();
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on send ussd failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "interval between attempts", doc = "Time interval between attempts sending ussd in case of error")
  public void setAttemptsInterval(final TimeInterval attemptsInterval) {
    this.attemptsInterval = attemptsInterval;
  }

  public TimeInterval getAttemptsInterval() {
    return attemptsInterval;
  }

  @Override
  public String getProvidedAction() {
    return actionName;
  }

  @Override
  public boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    String phoneNumber = replacePhoneNumber(extractPhoneNumber(action.getArgs()));
    if (!PhoneNumber.isValid(phoneNumber)) {
      throw new IllegalArgumentException("Invalid phoneNumber: " + phoneNumber);
    }

    if (sendUSSDInsideOneGroup) {
      return channel.getSimData().getSimGroup().getID() == extractSIMGroupID(action.getArgs());
    } else {
      return true;
    }
  }

  @Override
  public void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - channel {} is executing action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
    String phoneNumber = replacePhoneNumber(extractPhoneNumber(action.getArgs()));

    logger.debug("[{}] - channel {} is send USSD to {}", this, channel.getSimData().getPhoneNumber().toString(), phoneNumber);
    try {
      sendUSSD(channel, new PhoneNumberGenerator(phoneNumber));
    } catch (Exception e) {
      if (!skipErrors) {
        event.fireEvent(channel);
        eventOnFailure.fireEvent(channel);
        throw e;
      }
    }
    event.fireEvent(channel);
    eventOnSuccess.fireEvent(channel);
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    this.inIcomingCall.set(true);
  }

  @Override
  public void handleIncomingCallAnswered() {
    this.inIcomingCall.set(true);
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    this.inIcomingCall.set(false);
  }

  private String extractPhoneNumber(final Serializable[] args) {
    if (args.length < 1) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }

    return args[0].toString();
  }

  private Long extractSIMGroupID(final Serializable[] args) {
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }

    return Long.parseLong(args[1].toString());
  }

  private String replacePhoneNumber(String phoneNumber) {
    if (!numberPattern.isEmpty() && !numberReplacePattern.isEmpty()) {
      logger.debug("[{}] - replaceNumber: numberPattern: {}", this, numberPattern);
      logger.debug("[{}] - replaceNumber: numberReplacePattern: {}", this, numberReplacePattern);
      phoneNumber = phoneNumber.replaceFirst(numberPattern, numberReplacePattern);
      logger.debug("[{}] - resultNumber: {}", this, phoneNumber);
    }

    return phoneNumber;
  }

  private void sendUSSD(final RegisteredInGSMChannel channel, final PhoneNumberGenerator phoneNumberGenerator) throws Exception {
    int attemptCount = 0;
    USSDSession ussdSession = null;
    while (true) {
      try {
        PhoneNumber srcNumber = channel.getSimData().getPhoneNumber();
        PhoneNumber dstNumber = phoneNumberGenerator.generate();
        String ussd = generateUSSD(srcNumber.getValue(), dstNumber.getValue());

        logger.debug("[{}] - {} is trying to send USSD [{}]", this, channel, ussd);
        ussdSession = channel.startUSSDSession(ussd);
        String response = ussdSession.readResponse();
        logger.debug("[{}] - got response: {}", this, response);
        for (USSDResponseAnswer responseAnswer : responseAnswerList) {
          if (!response.matches(responseAnswer.getResponsePattern())) {
            logger.debug("[{}] - wrong response: {}", this, response);
            throw new WrongResponseException("Wrong response: " + response);
          }
          Thread.sleep(new TimeInterval(TimePeriod.inSeconds(10), TimePeriod.inSeconds(12)).random());
          if (!responseAnswer.hasResponseAnswer()) {
            break;
          }

          ussdSession.sendCommand(response.replaceFirst(responseAnswer.getResponsePattern(), responseAnswer.getResponseAnswer()));
          response = ussdSession.readResponse();
          logger.debug("[{}] - got response: {}", this, response);
        }
        logger.debug("[{}] - sending is finished", this);
        break;
      } catch (Exception e) {
        attemptCount++;
        logger.info("[{}] - while sending USSD", this, e);
        if (attemptCount >= attemptsCount) {
          throw e;
        }
      } finally {
        if (ussdSession != null) {
          ussdSession.endSession();
        }
      }
      Thread.sleep(attemptsInterval.random());
    }
  }

  private String generateUSSD(final String srcNumber, final String dstNumber) {
    return request.replaceFirst("pin", srcNumber).replaceFirst("amount", Integer.toString(amount)).replaceAll("number", dstNumber);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

@Script(name = "USSD get number", doc = "waits until event occured and then sends USSD request to get phone number and then set it to sim card")
public class USSDGetNumber implements BusinessActivityScript, GenericEventListener {

  private String request = "*161#";
  private String pattern = ".*(\\d+).*";
  private String replacePattern = "$1";
  private String event = "send_ussd";
  private String eventOnFail = "ussd_failed";
  private String eventOnSuccess = "ussd_success";

  private GenericEvent caughtEvent;

  public String getRequest() {
    return request;
  }

  @ScriptParam(name = "USSD request", doc = "USSD request")
  public void setRequest(final String number) {
    this.request = number;
  }

  public String getPattern() {
    return pattern;
  }

  @ScriptParam(name = "USSD response pattern", doc = "pattern for checking USSD response")
  public void setPattern(final String pattern) {
    this.pattern = pattern;
  }

  public String getReplacePattern() {
    return replacePattern;
  }

  @ScriptParam(name = "USSD replace response pattern", doc = "replace pattern for checking USSD response")
  public void setReplacePattern(final String replacePattern) {
    this.replacePattern = replacePattern;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event", doc = "event to send USSD")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when USSD sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on success", doc = "this event will be generated when USSD sending succeed")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      String response = channel.sendUSSD(request);
      channel.addUserMessage("Last USSD: " + response);
      if (response.matches(pattern)) {
        PhoneNumber phoneNumber = new PhoneNumber(response.replaceFirst(pattern, replacePattern));
        channel.updatePhoneNumber(phoneNumber);
        caughtEvent.respondSuccess(channel);
        channel.fireGenericEvent(eventOnSuccess);
      } else {
        throw new IllegalArgumentException("response [" + response + "] not matches");
      }
    } catch (Exception e) {
      caughtEvent.respondFailure(channel, e.getMessage());
      channel.fireGenericEvent(eventOnFail);
    } finally {
      caughtEvent = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "ussd get number";
  }

}

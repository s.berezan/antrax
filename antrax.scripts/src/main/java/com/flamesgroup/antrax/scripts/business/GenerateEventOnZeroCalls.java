/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "generate event on zero calls", doc = "generates event when count of zero calls is not separated with good calls and is greater than maximumZeroCallsCount")
public class GenerateEventOnZeroCalls implements BusinessActivityScript, CallsListener, StatefullScript {

  private static final long serialVersionUID = -331220775065097988L;

  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");
  @StateField
  private int zeroCallsCount;
  private int maximumZeroCallsCount = 7;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "maximum zero calls", doc = "count of zero calls to generate event")
  public void setMaximumZeroCallsCount(final int maximumZeroCallsCount) {
    this.maximumZeroCallsCount = maximumZeroCallsCount;
  }

  public int getMaximumZeroCallsCount() {
    return maximumZeroCallsCount;
  }

  @ScriptParam(name = "event", doc = "event on zero calls")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return this.event.getEvent();
  }

  @Override
  public String describeBusinessActivity() {
    return "event on zero calls";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    zeroCallsCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return zeroCallsCount >= maximumZeroCallsCount;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (duration == 0) {
      zeroCallsCount++;
    } else {
      zeroCallsCount = 0;
    }
    saver.save();
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}

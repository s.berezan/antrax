/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "generate event on incoming call", doc = "generates event after incoming call drop")
public class GenerateEventOnIncomingCall implements BusinessActivityScript, IncomingCallsListener {

  private GenericEvent event = GenericEvent.uncheckedEvent("incoming_call");
  private TimeInterval durationBounds = new TimeInterval(TimePeriod.inMinutes(20), TimePeriod.inMinutes(40));
  private boolean requireAnswer;

  private boolean answered;
  private volatile boolean shouldStartBusinessActivity;

  @ScriptParam(name = "event", doc = "event after incoming call drop")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return this.event.getEvent();
  }

  @ScriptParam(name = "require answer", doc = "generate event for incoming call with answer")
  public void setRequireAnswer(final boolean requireAnswer) {
    this.requireAnswer = requireAnswer;
  }

  public boolean getRequireAnswer() {
    return requireAnswer;
  }

  @ScriptParam(name = "duration bounds", doc = "set call duration bounds to generate event")
  public void setDurationBounds(final TimeInterval durationBounds) {
    this.durationBounds = durationBounds;
  }

  public TimeInterval getDurationBounds() {
    return durationBounds;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    answered = false;
    shouldStartBusinessActivity = false;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return shouldStartBusinessActivity;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event on incoming call";
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
  }

  @Override
  public void handleIncomingCallAnswered() {
    answered = true;
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    if (requireAnswer) {
      if (answered && durationBounds.getMin().getPeriod() < callDuration && durationBounds.getMax().getPeriod() > callDuration) {
        shouldStartBusinessActivity = true;
      }
    } else {
      shouldStartBusinessActivity = true;
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

@Script(name = "sms action", doc = "Makes sending sms")
public class SMSAction implements ActionProviderScript, IncomingCallsListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(SMSAction.class);

  private static final int MAX_SMS_TEMPLATES_READ_COUNT = 1024;

  private boolean sendSMSInsideOneGroup = true;
  private String actionName = "sms";
  private GenericEvent event = GenericEvent.uncheckedEvent("afterSendSMS");
  private String numberPattern = "";
  private String numberReplacePattern = "";
  private boolean ignoreIncomingCalls = false;
  private boolean skipErrors = false;
  private GenericEvent eventOnSuccess = GenericEvent.uncheckedEvent("afterSuccessSendSMS");
  private GenericEvent eventOnFailure = GenericEvent.uncheckedEvent("afterFailedSendSMS");
  private VariableLong sendSMSCount = new VariableLong(1, 1);
  private TimeInterval intervalBetweenSendSMS = new TimeInterval(TimePeriod.inSeconds(1), TimePeriod.inSeconds(5));
  private int attemptsCount = 1;
  private TimeInterval attemptsInterval = new TimeInterval(10000, 15000);
  private String registryPath = "sms.templates";

  private final AtomicBoolean inIcomingCall = new AtomicBoolean(false);
  private RegistryAccess registryAccess;

  @ScriptParam(name = "send sms inside one group", doc = "send sms inside one group only")
  public void setSendSMSInsideOneGroup(final boolean sendSMSInsideOneGroup) {
    this.sendSMSInsideOneGroup = sendSMSInsideOneGroup;
  }

  public boolean getSendSMSInsideOneGroup() {
    return sendSMSInsideOneGroup;
  }

  @ScriptParam(name = "action name", doc = "action name")
  public void setAction(final String actionName) {
    this.actionName = actionName;
  }

  public String getAction() {
    return actionName;
  }

  @ScriptParam(name = "event", doc = "event to generate after action")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "number pattern", doc = "number pattern")
  public void setNumberPattern(final String numberPattern) {
    this.numberPattern = numberPattern;
  }

  public String getNumberPattern() {
    return numberPattern;
  }

  @ScriptParam(name = "number replace pattern", doc = "number replace pattern")
  public void setNumberReplacePattern(final String numberReplacePattern) {
    this.numberReplacePattern = numberReplacePattern;
  }

  public String getNumberReplacePattern() {
    return numberReplacePattern;
  }

  @ScriptParam(name = "ignore incoming calls", doc = "call anytime, even in incoming call")
  public void setIgnoreIncomingCalls(final boolean ignoreIncomingCalls) {
    this.ignoreIncomingCalls = ignoreIncomingCalls;
  }

  public boolean getIgnoreIncomingCalls() {
    return this.ignoreIncomingCalls;
  }

  @ScriptParam(name = "skip errors", doc = "flag for skip errors or not")
  public void setSkipErrors(final boolean skipErrors) {
    this.skipErrors = skipErrors;
  }

  public boolean getSkipErrors() {
    return this.skipErrors;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if send sms was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = GenericEvent.uncheckedEvent(eventOnSuccess);
  }

  public String getEventOnSuccess() {
    return eventOnSuccess.getEvent();
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if send sms failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = GenericEvent.uncheckedEvent(eventOnFailure);
  }

  public String getEventOnFailure() {
    return eventOnFailure.getEvent();
  }

  @ScriptParam(name = "send sms count", doc = "count of generated sms")
  public void setSendSMSCount(final VariableLong sendSMSCount) {
    this.sendSMSCount = sendSMSCount;
  }

  public VariableLong getSendSMSCount() {
    return this.sendSMSCount;
  }

  @ScriptParam(name = "interval between send sms", doc = "interval between SMSes sending")
  public void setIntervalBetweenSendSMS(final TimeInterval intervalBetweenSendSMS) {
    this.intervalBetweenSendSMS = intervalBetweenSendSMS;
  }

  public TimeInterval getIntervalBetweenSendSMS() {
    return this.intervalBetweenSendSMS;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on send sms failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "interval between attempts", doc = "Time interval between attempts sending SMS in case of error")
  public void setAttemptsInterval(final TimeInterval attemptsInterval) {
    this.attemptsInterval = attemptsInterval;
  }

  public TimeInterval getAttemptsInterval() {
    return attemptsInterval;
  }

  @ScriptParam(name = "path in registry", doc = "path to the registry keys, which contains text for SMS")
  public void setRegistryPath(final String path) {
    this.registryPath = path;
  }

  public String getRegistryPath() {
    return registryPath;
  }

  @Override
  public String getProvidedAction() {
    return actionName;
  }

  @Override
  public boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!ignoreIncomingCalls && inIcomingCall.get()) {
      return false;
    }

    String phoneNumber = replacePhoneNumber(extractPhoneNumber(action.getArgs()));
    if (!PhoneNumber.isValid(phoneNumber)) {
      throw new IllegalArgumentException("Invalid phoneNumber: " + phoneNumber);
    }

    if (sendSMSInsideOneGroup) {
      return channel.getSimData().getSimGroup().getID() == extractSIMGroupID(action.getArgs());
    } else {
      return true;
    }
  }

  @Override
  public void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!ignoreIncomingCalls && inIcomingCall.get()) {
      throw new IllegalStateException("Can't execute action in the middle of incoming call");
    }

    logger.debug("[{}] - channel {} is executing action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
    String phoneNumber = replacePhoneNumber(extractPhoneNumber(action.getArgs()));

    logger.debug("[{}] - channel {} is send SMS to {}", this, channel.getSimData().getPhoneNumber().toString(), phoneNumber);
    sendSMSes(channel, new PhoneNumberGenerator(phoneNumber));
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    this.inIcomingCall.set(true);
  }

  @Override
  public void handleIncomingCallAnswered() {
    this.inIcomingCall.set(true);
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    this.inIcomingCall.set(false);
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registryAccess = registry;
  }

  private String extractPhoneNumber(final Serializable[] args) {
    if (args.length < 1) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }

    return args[0].toString();
  }

  private Long extractSIMGroupID(final Serializable[] args) {
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }

    return Long.parseLong(args[1].toString());
  }

  private String replacePhoneNumber(String phoneNumber) {
    if (!numberPattern.isEmpty() && !numberReplacePattern.isEmpty()) {
      logger.debug("[{}] - replaceNumber: numberPattern: {}", this, numberPattern);
      logger.debug("[{}] - replaceNumber: numberReplacePattern: {}", this, numberReplacePattern);
      phoneNumber = phoneNumber.replaceFirst(numberPattern, numberReplacePattern);
      logger.debug("[{}] - resultNumber: {}", this, phoneNumber);
    }

    return phoneNumber;
  }

  private void sendSMSes(final RegisteredInGSMChannel channel, final PhoneNumberGenerator phoneNumberGenerator) throws Exception {
    for (int i = 0; i < sendSMSCount.random(); i++) {
      try {
        sendSMS(channel, phoneNumberGenerator);
      } catch (Exception e) {
        if (!skipErrors) {
          event.fireEvent(channel);
          eventOnFailure.fireEvent(channel);
          throw e;
        }
      }
      Thread.sleep(intervalBetweenSendSMS.random());
    }
    event.fireEvent(channel);
    eventOnSuccess.fireEvent(channel);
  }

  private void sendSMS(final RegisteredInGSMChannel channel, final PhoneNumberGenerator phoneNumberGenerator) throws Exception {
    int attemptCount = 0;
    while (true) {
      try {
        String smsText = generateSMSText(channel);
        PhoneNumber number = phoneNumberGenerator.generate();
        logger.debug("[{}] - {} is trying to send SMS '{}' to {}", this, channel, smsText, number);

        channel.sendSMS(number, smsText);
        break;
      } catch (Exception e) {
        attemptCount++;
        logger.info("[{}] - while sending SMS", this, e);
        if (attemptCount >= attemptsCount) {
          throw e;
        }
      }
      Thread.sleep(attemptsInterval.random());
    }
  }

  private String generateSMSText(final RegisteredInGSMChannel channel) {
    RegistryEntry[] entries = registryAccess.listEntries(registryPath, MAX_SMS_TEMPLATES_READ_COUNT);
    if (entries.length == 0) {
      throw new IllegalStateException("There is no any template under register key = " + registryPath);
    }

    int index = new Random(System.currentTimeMillis()).nextInt(entries.length);
    return entries[index].getValue();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.TimeUtils;

import java.io.Serializable;

@Script(name = "check calls duration", doc = "check calls duration if event occurs and generates events according to the value of sim card call duration")
public class CheckCallsDuration implements BusinessActivityScript, StatefullScript, GenericEventListener {

  private static final long serialVersionUID = 3805934256395921247L;

  private String event = "checkCallsDuration";
  private String eventOnLess = "eventOnLess";
  private String eventOnMore = "eventOnMore";

  private volatile GenericEvent eventCaught;

  private TimeInterval durationBounds = new TimeInterval(TimePeriod.inMinutes(20), TimePeriod.inMinutes(40));

  private final ScriptSaver saver = new ScriptSaver();

  @StateField
  private long durationLimit;

  public String getEvent() {
    return this.event;
  }

  @ScriptParam(name = "event", doc = "event to check calls duration")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEventOnLess() {
    return eventOnLess;
  }

  @ScriptParam(name = "event on less", doc = "this event will be generated when duration is less than random value from duration bounds")
  public void setEventOnLess(final String eventOnLess) {
    this.eventOnLess = eventOnLess;
  }

  public String getEventOnMore() {
    return eventOnMore;
  }

  @ScriptParam(name = "event on more", doc = "this event will be generated when duration is more than random value from duration bounds")
  public void setEventOnMore(final String eventOnMore) {
    this.eventOnMore = eventOnMore;
  }

  public TimeInterval getDurationBounds() {
    return durationBounds;
  }

  @ScriptParam(name = "duration bounds", doc = "set call duration bounds to generate event")
  public void setDurationBounds(final TimeInterval durationBounds) {
    this.durationBounds = durationBounds;
  }

  @Override
  public String describeBusinessActivity() {
    return "check calls duration if event occurs: duration limit " + TimeUtils.writeTimeMs(durationLimit) + " generated from duration bounds: " + durationBounds;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    String fireEvent;
    if (channel.getSimData().getCallDuration() > durationLimit) {
      fireEvent = eventOnMore;
    } else if (channel.getSimData().getCallDuration() < durationLimit) {
      fireEvent = eventOnLess;
    } else {
      return;
    }

    try {
      eventCaught.respondSuccess(channel);
      channel.fireGenericEvent(fireEvent);
    } catch (Exception e) {
      eventCaught.respondFailure(channel, e.getMessage());
    } finally {
      eventCaught = null;
    }

    durationLimit = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (durationLimit < durationBounds.getMin().getPeriod() || durationLimit > durationBounds.getMax().getPeriod()) {
      durationLimit = durationBounds.random();
      saver.save();
    }

    return eventCaught != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      eventCaught = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

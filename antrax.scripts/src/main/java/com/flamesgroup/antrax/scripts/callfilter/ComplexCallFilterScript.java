/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.CallFilterScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "complex call filter", doc = "filters calls by both patern and advanced rules, which can be selected from set")
public class ComplexCallFilterScript extends CallFilterBase
    implements CallFilterScript, StatefullScript, CallsListener, IncomingCallsListener, SimDataListener, SessionListener, ActivityPeriodListener,
    ActivityListener, GenericEventListener {
  private static final long serialVersionUID = 2890623485115249288L;

  @StateField
  private SimpleActivityScript rule;

  private SimData simData;

  private transient PhoneNumber currANumber;
  private transient PhoneNumber currBNumber;

  private ScriptSaver scriptSaver = new ScriptSaver();

  public ComplexCallFilterScript() {
    super(LoggerFactory.getLogger(ComplexCallFilterScript.class));
    allowedANumber = "(.*)";
    deniedANumber = "";
    allowedBNumber = "\\+?3?8?(.*)";
    deniedBNumber = "";
    replaceBNumber = "$1";
  }

  @ScriptParam(name = "rule", doc = "Additional rule which controls whether call should be accepted")
  public void setRule(final ActivityScriplet scriplet) throws Exception {
    this.rule = scriplet.createActivityScript();
    this.scriptSaver = new ScriptSaver(rule.getScriptSaver());
  }

  public ActivityScriplet getRule() {
    return new ScripletBlock(ScripletFactory.createScripletFor(ScripletFactory.listScripts()[0]));
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return scriptSaver;
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
    rule.setSimData(simData);
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
  }

  @Override
  public void handleIncomingCallAnswered() {
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    simData.setIncomingCallDuration(simData.getIncomingCallDuration() + callDuration);
    simData.setIncomingTotalCallsCount(simData.getIncomingTotalCallsCount() + 1);
    if (callDuration > 0) {
      simData.setIncomingSuccessfulCallsCount(simData.getIncomingSuccessfulCallsCount() + 1);
    }
  }

  @Override
  public boolean isCallAccepted(final PhoneNumber aPhoneNumber, final PhoneNumber bPhoneNumber) {
    rule.setSimData(simData);
    this.currANumber = aPhoneNumber;
    this.currBNumber = bPhoneNumber;
    return isNumberMatches(aPhoneNumber, bPhoneNumber) && rule.isActivityAllowed();
  }

  @Override
  public PhoneNumber substituteBNumber(final PhoneNumber bPhoneNumber) {
    return super.substituteBNumber(bPhoneNumber);
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (currANumber == null || currBNumber == null || !isNumberMatches(currANumber, currBNumber)) {
      return;
    }

    simData.setCallDuration(simData.getCallDuration() + duration);
    simData.setTotalCallsCount(simData.getTotalCallsCount() + 1);
    if (duration > 0) {
      simData.setSuccessfulCallsCount(simData.getSuccessfulCallsCount() + 1);
    }
  }

  @Override
  public void handleActivityEnded() {
    rule.handleActivityEnded();
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    rule.handleActivityStarted(gsmGroup);
  }

  @Override
  public void handlePeriodEnd() {
    rule.handlePeriodEnd();
  }

  @Override
  public void handlePeriodStart() {
    rule.handlePeriodStart();
  }

  @Override
  public void handleSessionEnded() {
    rule.handleSessionEnded();
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
    rule.handleSessionStarted(gateway);
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    rule.handleGenericEvent(event, args);
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", getClass().getSimpleName(), rule);
  }

}

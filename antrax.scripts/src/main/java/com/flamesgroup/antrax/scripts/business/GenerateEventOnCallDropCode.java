/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

@Script(name = "generate event on call drop code", doc = "generates event on call drop code")
public class GenerateEventOnCallDropCode implements BusinessActivityScript, CallsListener, StatefullScript {

  private static final long serialVersionUID = -7463990086853753126L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnCallDropCode.class);

  private int dropCodeLimit;

  private final List<Integer> dropCodes = new LinkedList<>();

  @StateField
  private volatile int dropCodeCount;

  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event on call drop code")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "call drop code limit", doc = "limit of getting drop codes after which event will be generated")
  public void setDropCodeLimit(final int dropCodeLimit) {
    this.dropCodeLimit = dropCodeLimit;
  }

  public int getDropCodeLimit() {
    return dropCodeLimit;
  }

  @ScriptParam(name = "call drop code", doc = "call drop code on which need generate event")
  public void addDropCode(final int dropCode) {
    this.dropCodes.add(dropCode);
  }

  public int getDropCode() {
    return 0;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event on call drop code";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    dropCodeCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return dropCodeLimit > 0 && dropCodeCount >= dropCodeLimit;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    logger.debug("[{}] - got call drop code {}", this, causeCode);
    if (isDropCodeMatch(causeCode)) {
      dropCodeCount++;
      logger.debug("[{}] - call drop code '{}' match and increase call drop code count: {}", this, causeCode, dropCodeCount);
      saver.save();
    }
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  private boolean isDropCodeMatch(final int dropCode) {
    if (dropCodes.isEmpty()) {
      return false;
    }

    for (Integer cause : dropCodes) {
      if (cause == 0 || cause == dropCode) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.factor;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.helper.factor.StatisticField;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

@Script(name = "statistic based factor", doc = "calculates factor basing on one of the statistic value")
public class StatisticBasedFactor extends FactorBase implements SimDataListener {

  private transient SimData simData;

  private StatisticField field = StatisticField.CALLED_DURATION;

  @ScriptParam(name = "statistic value", doc = "sttaistic value used to count factor")
  public void setStatisticValue(final StatisticField field) {
    this.field = field;
  }

  public StatisticField getStatisticValue() {
    return field;
  }

  @Override
  public long countFactor() {
    return getPriority().getValue() + getMultiplier() * getFieldVal();
  }

  private int getFieldVal() {
    switch (field) {
      case CALLED_DURATION:
        return (int) simData.getCallDuration();
      case SUCCESSFUL_CALLS_COUNT:
        return simData.getSuccessfulCallsCount();
      case TOTAL_CALLS_COUNT:
        return simData.getTotalCallsCount();
      case TOTAL_SMS_COUNT:
        return simData.getSuccessOutgoingSmsCount();
      default:
        return 0;
    }
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

}

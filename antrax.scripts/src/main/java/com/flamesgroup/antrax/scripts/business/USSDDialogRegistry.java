/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.helper.business.transfer.WrongResponseException;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Script(name = "USSDDialogRegistry", doc = "initiates USSD dialog, waits for responses and answers them from registry")
public class USSDDialogRegistry implements BusinessActivityScript, GenericEventListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(USSDDialogRegistry.class);

  private String ussdRequest = "*111#";
  private final List<USSDResponseAnswer> ussdResponseAnswerList = new LinkedList<>();
  private String event = "ussd_dialog";
  private String eventOnFail = "ussd_dialog_failed";
  private String eventOnSuccess = "ussd_dialog_success";
  private String idNo = "";

  private volatile GenericEvent caughtEvent;
  private transient RegistryAccess registry;

  @ScriptParam(name = "USSD request", doc = "USSD request, it can be contains '<phone_number>' or '<ID_no>'. '<phone_number>' - it is paste phone number from sim data. '<ID_no>' - it is number from registry")
  public void setRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getRequest() {
    return ussdRequest;
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressions for parsing response and generating answers from registry")
  public void addUSSDResponseAnswerList(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswerList.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "event", doc = "event to start USSD session")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when USSD sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on success", doc = "this event will be generated when USSD sending succeed")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "<ID_no> registry path", doc = "path to registry path for <ID_no>")
  public void setIdNo(final String idNo) {
    this.idNo = idNo;
  }

  public String getIdNo() {
    return idNo;
  }

  @Override
  public String describeBusinessActivity() {
    return "initiates USSD dialog with answers from a registry";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    USSDSession ussdSession = null;
    List<RegistryEntry> ussdResponseAnswersRegistryEntry = new ArrayList<>();
    try {
      logger.debug("[{}] - sending ussd: {}", this, ussdRequest);
      if (null == ussdRequest || 0 == ussdRequest.length()) {
        logger.error("[{}] - wrong ussd request: {}", this, ussdRequest);
        throw new Exception("Wrong ussd request: " + ussdRequest);
      }

      if (!ussdRequest.contains("<phone_number>")) {
        logger.debug("[{}] - ussdRequest doesn't contain <phone_number>", this);
      }

      if (!ussdRequest.contains("<ID_no>")) {
        logger.debug("[{}] - ussdRequest doesn't contain <ID_no>", this);
      }

      String ussdRequestLocal = ussdRequest.replaceFirst("<phone_number>", channel.getSimData().getPhoneNumber().getValue());
      RegistryEntry[] entries = null;
      if (registry != null) {
        entries = registry.listEntries(idNo, Integer.MAX_VALUE);
      }
      String idNoValue = "";
      if (entries != null && entries.length != 0) {
        idNoValue = entries[0].getValue();
        logger.debug("[{}] - use idNo [{}] from registry", this, idNoValue);
        registry.move(entries[0], idNo + ".used");
      } else {
        logger.debug("[{}] - doesn't find value for idNo [{}]", this, idNo);
      }

      ussdRequestLocal = ussdRequestLocal.replaceFirst("<ID_no>", idNoValue);

      for (USSDResponseAnswer ussdResponseAnswer : ussdResponseAnswerList) {
        entries = null;
        if (registry != null) {
          entries = registry.listEntries(ussdResponseAnswer.getResponseAnswer(), Integer.MAX_VALUE);
        }
        if (entries != null && entries.length != 0) {
          logger.debug("[{}] - use {} [{}] from registry", this, ussdResponseAnswer.getResponseAnswer(), entries[0].getValue());
          ussdResponseAnswersRegistryEntry.add(entries[0]);
        } else {
          logger.debug("[{}] - doesn't find value for [{}]", this, ussdResponseAnswer.getResponseAnswer());
        }
      }

      if (ussdResponseAnswersRegistryEntry.size() != ussdResponseAnswerList.size()) {
        throw new Exception("Not found all values");
      }

      ussdSession = channel.startUSSDSession(ussdRequestLocal);

      String response = ussdSession.readResponse();
      logger.debug("[{}] - got response: {}", this, response);

      int i = 0;
      for (USSDResponseAnswer usssdResponseAnswer : ussdResponseAnswerList) {
        if (!response.matches(usssdResponseAnswer.getResponsePattern())) {
          logger.debug("[{}] - wrong response: {}", this, response);
          throw new WrongResponseException("Wrong response: " + response);
        }

        Thread.sleep(new TimeInterval(TimePeriod.inSeconds(1), TimePeriod.inSeconds(2)).random());
        if (!usssdResponseAnswer.hasResponseAnswer()) {
          break;
        }

        ussdSession.sendCommand(ussdResponseAnswersRegistryEntry.get(i++).getValue());
        response = ussdSession.readResponse();
        logger.debug("[{}] - got response: {}", this, response);
      }

      caughtEvent.respondSuccess(channel);
      channel.fireGenericEvent(eventOnSuccess);
    } catch (Exception e) {
      caughtEvent.respondFailure(channel, e.getMessage());
      channel.fireGenericEvent(eventOnFail);
    } finally {
      if (null != ussdSession) {
        ussdSession.endSession();
      }
      caughtEvent = null;
      if (registry != null) {
        for (RegistryEntry registryEntry : ussdResponseAnswersRegistryEntry) {
          registry.move(registryEntry, registryEntry.getPath() + ".used");
        }
      }
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

}

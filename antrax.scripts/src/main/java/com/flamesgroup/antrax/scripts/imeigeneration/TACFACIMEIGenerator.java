/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.imeigeneration;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.payment.RegistryHelper;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.IMEIException;
import com.flamesgroup.commons.IMEIPattern;
import com.flamesgroup.commons.SNRPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Script(name = "IMEI based on TAC and FAC", doc = "generates IMEI with TAC and FAC from registry and random SNR")
public class TACFACIMEIGenerator extends IMEIGenerator implements RegistryAccessListener {

  private static final long serialVersionUID = -5464236916743252995L;

  private static final Logger logger = LoggerFactory.getLogger(TACFACIMEIGenerator.class);

  private transient RegistryAccess registry;
  private String path = "imei.tacfac";
  private SNRPattern snrPattern;

  @ScriptParam(name = "path", doc = "path to TAC and FAC in registry")
  public void setPath(final String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  @ScriptParam(name = "snrPattern", doc = "SNR pattern for IMEI generation")
  public void setSNRPattern(final SNRPattern snrPattern) {
    this.snrPattern = snrPattern;
  }

  public SNRPattern getSNRPattern() {
    return new SNRPattern("XXXXXX");
  }

  @Override
  public IMEI generateIMEI() throws IMEIException {
    String tacFac = null;
    try {
      RegistryHelper helper = new RegistryHelper(registry);
      tacFac = helper.getRegistryValue(path, 100);
    } catch (final Exception e) {
      logger.info("[{}] - can't get IMEI TAC and FAC from registry", this, e);
    }
    if (tacFac == null) {
      throw new IMEIException("IMEI TAC and FAC isn't present in registry");
    }
    IMEIPattern imeiPattern;
    try {
      imeiPattern = new IMEIPattern(tacFac + snrPattern);
    } catch (IllegalArgumentException e) {
      throw new IMEIException("Can't create IMEIPattern from tacfac [" + tacFac + "], snrPattern [" + snrPattern + "]", e);
    }
    return IMEI.generate(imeiPattern, getSimData().getPhoneNumber());
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

}

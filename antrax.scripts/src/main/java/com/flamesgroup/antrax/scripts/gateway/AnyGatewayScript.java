/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.gateway;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.predictions.GatewaysPredictionFactory;
import com.flamesgroup.antrax.storage.commons.IServerData;

@Script(name = "any gateway", doc = "accepts any gateway")
public class AnyGatewayScript implements GatewaySelectorScript {

  @Override
  public boolean isGatewayApplies(final IServerData gateway) {
    return true;
  }

  @Override
  public Prediction predictNextGateway() {
    return GatewaysPredictionFactory.any();
  }

}

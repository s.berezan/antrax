/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "limit call duration", doc = "drop call if it exceeds specified duration")
public class LimitCallDuration implements BusinessActivityScript, CallsListener, StatefullScript, ActivityListener {

  private static final long serialVersionUID = -6745758131360910139L;

  @StateField
  private volatile long durationLimitValue;
  @StateField
  private volatile boolean inCall;
  @StateField
  private volatile long timestamp;

  private TimeInterval durationLimit = new TimeInterval(TimePeriod.inMinutes(3), TimePeriod.inMinutes(5));

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "duration limit", doc = "limit call duration per day")
  public void setDurationLimit(final TimeInterval durationLimit) {
    this.durationLimit = durationLimit;
  }

  public TimeInterval getDurationLimit() {
    return durationLimit;
  }

  @Override
  public String describeBusinessActivity() {
    return "drop call";
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    if (inCall || timestamp > 0) {
      inCall = false;
      timestamp = 0;
      saver.save();
    }
  }

  @Override
  public void handleActivityEnded() {
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    inCall = false;
    timestamp = 0;
    channel.dropCall();
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (!inCall) {
      return false;
    }
    return System.currentTimeMillis() - timestamp > durationLimitValue;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    inCall = false;
    timestamp = 0;
    saver.save();
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
    timestamp = System.currentTimeMillis();
    durationLimitValue = durationLimit.random();
    inCall = true;
    saver.save();
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}

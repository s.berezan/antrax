/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.helper.business.transfer.WrongResponseException;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Script(name = "USSD dialog check balance", doc = "initiates USSD dialog, waits for last response with balance and generates events according to the value of balance")
public class USSDDialogCheckBalance implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(USSDDialogCheckBalance.class);

  private String ussdRequest = "*111#";
  private final List<USSDResponseAnswer> ussdResponseAnswerList = new LinkedList<>();
  private String event = "ussd_dialog_check_balance";
  private String eventOnTrue = "ussd_dialog_true";
  private String eventOnFail = "ussd_dialog_failed";
  private double minimumAmount = 5;
  private String lessThanMinimumEvent = "payMe";
  private String moreThanMinimumEvent = "balanceFull";

  private volatile GenericEvent caughtEvent;

  @ScriptParam(name = "USSD request", doc = "USSD request")
  public void setRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getRequest() {
    return ussdRequest;
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressing for parsing response and generating answers. Last answer must contain expression for check balance")
  public void addUSSDResponseAnswerList(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswerList.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "event", doc = "event to start USSD session")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "minimum amount of money", doc = "required amount of money in the balance")
  public void setMinimumAmount(final double minimumAmount) {
    this.minimumAmount = minimumAmount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "event on less", doc = "event on amount of money in the balance that less than minimum")
  public void setLessThanMinimumEvent(final String lessThanMinimumEvent) {
    this.lessThanMinimumEvent = lessThanMinimumEvent;
  }

  public String getLessThanMinimumEvent() {
    return lessThanMinimumEvent;
  }

  @ScriptParam(name = "event on more", doc = "event on amount of money in the balance that more than minimum")
  public void setMoreThanMinimumEvent(final String moreThanMinimumEvent) {
    this.moreThanMinimumEvent = moreThanMinimumEvent;
  }

  public String getMoreThanMinimumEvent() {
    return moreThanMinimumEvent;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when USSD sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on true", doc = "this event will be generated when USSD sending true")
  public void setEventOnTrue(final String eventOnTrue) {
    this.eventOnTrue = eventOnTrue;
  }

  public String getEventOnTrue() {
    return eventOnTrue;
  }

  @Override
  public String describeBusinessActivity() {
    return "USSD dialog check balance";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    USSDSession ussdSession = null;
    try {
      logger.debug("[{}] - sending ussd: {}", this, ussdRequest);
      if (null == ussdRequest || 0 == ussdRequest.length()) {
        logger.error("[{}] - wrong ussd request: {}", this, ussdRequest);
        throw new Exception("Wrong ussd request: " + ussdRequest);
      }
      ussdSession = channel.startUSSDSession(ussdRequest);

      String response = ussdSession.readResponse();
      logger.debug("[{}] - got response: {}", this, response);

      Iterator<USSDResponseAnswer> iterator = ussdResponseAnswerList.iterator();
      while (iterator.hasNext()) {
        USSDResponseAnswer ussdResponseAnswer = iterator.next();
        if (!response.matches(ussdResponseAnswer.getResponsePattern())) {
          logger.debug("[{}] - wrong response: {}, for regex: {}", this, response, ussdResponseAnswer.getResponsePattern());
          throw new WrongResponseException("Wrong response: " + response);
        }

        Thread.sleep(new TimeInterval(TimePeriod.inSeconds(1), TimePeriod.inSeconds(2)).random());
        if (!ussdResponseAnswer.hasResponseAnswer()) {
          break;
        }

        String replacedResponse = response.replaceFirst(ussdResponseAnswer.getResponsePattern(), ussdResponseAnswer.getResponseAnswer());
        if (iterator.hasNext()) {
          logger.debug("[{}] - send command: {}", this, replacedResponse);
          ussdSession.sendCommand(replacedResponse);
          response = ussdSession.readResponse();
          logger.debug("[{}] - got response: {}", this, response);
        } else {
          String responsePart = replacedResponse.replaceAll("\\D+", ".");
          double balanceValue = Double.parseDouble(responsePart);
          if (balanceValue < minimumAmount) {
            logger.debug("[{}] - balance value is less then minimum [balance: {} < minimum: {}]", this, balanceValue, minimumAmount);
            channel.fireGenericEvent(lessThanMinimumEvent, balanceValue);
          } else {
            logger.debug("[{}] - balance value is more then minimum [balance: {} < minimum: {}]", this, balanceValue, minimumAmount);
            channel.fireGenericEvent(moreThanMinimumEvent, balanceValue);
          }
        }
      }

      caughtEvent.respondSuccess(channel);
      channel.fireGenericEvent(eventOnTrue);
    } catch (Exception e) {
      caughtEvent.respondFailure(channel, e.getMessage());
      channel.fireGenericEvent(eventOnFail);
    } finally {
      if (null != ussdSession) {
        ussdSession.endSession();
      }
      caughtEvent = null;
    }
  }


  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}

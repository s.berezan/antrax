/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "USSD", doc = "waits until event occured and then sends USSD to the specified phone number. Checks USSD response using pattern")
public class USSD implements BusinessActivityScript, GenericEventListener, CallsListener {

  private static final Logger logger = LoggerFactory.getLogger(USSD.class);

  private String request = "*101#";
  private String pattern = ".*";
  private int attemptsCount = 3;
  private String event = "send_ussd";
  private String eventOnFail = "ussd_failed";
  private String eventOnTrue = "ussd_true";

  private volatile GenericEvent caughtEvent;
  private volatile PhoneNumber lastNumber;

  @ScriptParam(name = "USSD request", doc = "USSD request")
  public void setRequest(final String number) {
    this.request = number;
  }

  public String getRequest() {
    return request;
  }

  @ScriptParam(name = "USSD response pattern", doc = "pattern for checking USSD response")
  public void setPattern(final String pattern) {
    this.pattern = pattern;
  }

  public String getPattern() {
    return pattern;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on USSD sending failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "event", doc = "event to send USSD")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when USSD sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on true", doc = "this event will be generated when USSD sending true")
  public void setEventOnTrue(final String eventOnTrue) {
    this.eventOnTrue = eventOnTrue;
  }

  public String getEventOnTrue() {
    return eventOnTrue;
  }



  @Override
  public String describeBusinessActivity() {
    return "sending USSD";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      int attemptCount = 0;
      while (true) {
        try {
          String response = sendUSSD(channel);
          if (checkUSSD(response)) {
            caughtEvent.respondSuccess(channel);
            channel.fireGenericEvent(eventOnTrue);
          } else {
            caughtEvent.respondFailure(channel, "Wrong USSD response: " + response);
            channel.fireGenericEvent(eventOnFail);
          }
          return;
        } catch (Exception e) {
          attemptCount++;
          if (attemptCount >= attemptsCount) {
            caughtEvent.respondFailure(channel, e.getMessage());
            channel.fireGenericEvent(eventOnFail);
            return;
          }
        }
        Thread.sleep(2000);
      }
    } finally {
      caughtEvent = null;
    }

  }

  private String sendUSSD(final RegisteredInGSMChannel channel) throws Exception {
    if (lastNumber == null) {
      logger.debug("[{}] - lastNumber is null", this);
    } else {
      request = request.replaceFirst("[@]", lastNumber.getValue());
    }
    String response = channel.sendUSSD(request);
    channel.addUserMessage("Last USSD: " + response);
    return response;
  }

  private boolean checkUSSD(final String response) {
    return response.matches(pattern);
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
    lastNumber = phoneNumber;
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

}

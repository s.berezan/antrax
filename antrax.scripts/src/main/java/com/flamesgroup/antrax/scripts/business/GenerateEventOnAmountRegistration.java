/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Script(name = "generate event on amount registration", doc = "generates specified number of events on amount of registrations")
public class GenerateEventOnAmountRegistration implements BusinessActivityScript, StatefullScript, ActivityListener {

  private static final long serialVersionUID = -7856857316444763187L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnAmountRegistration.class);

  private GenericEvent event = GenericEvent.uncheckedEvent("registrationEvent");

  private int registrationAmount;

  @StateField
  private volatile int registrationCount;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event to generate")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "registration amount", doc = "amount of registration after which will be generate event")
  public void setRegistrationAmount(final int registrationAmount) {
    this.registrationAmount = registrationAmount;
  }

  public int getRegistrationAmount() {
    return registrationAmount;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - generate event {}", this, event);
    event.fireEvent(channel);
    registrationCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return registrationAmount > 0 && registrationCount >= registrationAmount;
  }

  @Override
  public String describeBusinessActivity() {
    return "reg event [" + event + "]: reg count:" + registrationCount;
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    registrationCount++;
    saver.save();
  }

  @Override
  public void handleActivityEnded() {
  }

}

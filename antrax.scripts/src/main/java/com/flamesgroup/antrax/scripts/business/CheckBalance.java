/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.BadResponseException;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.ussd.USSDResponsePattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Script(name = "check balance", doc = "checks balance and generates events according to the value of balance")
public class CheckBalance implements BusinessActivityScript, GenericEventListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(CheckBalance.class);

  private volatile GenericEvent caughtEvent = null;

  private String event = "checkBalance";

  private double minimumAmount = 5;

  private String lessThanMinimumEvent = "payMe";
  private String moreThanMinimumEvent = "balanceFull";

  private CheckBalanceHelper checkBalanceHelper;

  private boolean failOnLessThanMinimum = true;

  private transient RegistryAccess registry;

  private boolean parseAlternativeUSSD = false;
  private final Set<USSDResponsePattern> ussdResponsePatterns = new HashSet<>();

  @ScriptParam(name = "balance check data", doc = "balance checking data")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*");
  }

  @ScriptParam(name = "event", doc = "this event causes balance check")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "minimum amount of money", doc = "required amount of money in the balance")
  public void setMinimumAmount(final double minimumAmount) {
    this.minimumAmount = minimumAmount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "event on less", doc = "event on amount of money in the balance that less than minimum")
  public void setLessThanMinimumEvent(final String lessThanMinimumEvent) {
    this.lessThanMinimumEvent = lessThanMinimumEvent;
  }

  public String getLessThanMinimumEvent() {
    return lessThanMinimumEvent;
  }

  @ScriptParam(name = "event on more", doc = "event on amount of money in the balance that more than minimum")
  public void setMoreThanMinimumEvent(final String moreThanMinimumEvent) {
    this.moreThanMinimumEvent = moreThanMinimumEvent;
  }

  public String getMoreThanMinimumEvent() {
    return moreThanMinimumEvent;
  }

  @ScriptParam(name = "fail on less", doc = "if checked, event will be failed if amount of money in the balance less than minimum")
  public void setFailOnLessThanMinimum(final boolean failOnLessThanMinimum) {
    this.failOnLessThanMinimum = failOnLessThanMinimum;
  }

  public boolean getFailOnLessThanMinimum() {
    return failOnLessThanMinimum;
  }

  public boolean getParseAlternativeUSSD() {
    return parseAlternativeUSSD;
  }

  @ScriptParam(name = "parse alternative ussd responses", doc = "allow to parse alternative ussd response")
  public void setParseAlternativeUSSD(final boolean parseAlternativeUSSD) {
    this.parseAlternativeUSSD = parseAlternativeUSSD;
  }

  @ScriptParam(name = "alternative ussd responses", doc = "if bad balance response and allowed, will parse and generate event")
  public void addUSSDResponsePattern(final USSDResponsePattern ussdResponsePattern) {
    ussdResponsePatterns.add(ussdResponsePattern);
  }

  public USSDResponsePattern getUSSDResponsePattern() {
    return new USSDResponsePattern(".*", "event");
  }

  @Override
  public String describeBusinessActivity() {
    return "check balance";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - begin checking balance - helper: {}", this, checkBalanceHelper);
    double balanceValue;
    try {
      balanceValue = checkBalanceHelper.getBalanceValue(channel, registry);
    } catch (BadResponseException e) {
      if (parseAlternativeUSSD) {
        USSDResponsePattern responsePattern = checkUSSD(e.getResponse());
        if (responsePattern == null) {
          caughtEvent.respondFailure(channel, "Wrong alternative ussd response: " + e.getResponse());
        } else {
          caughtEvent.respondSuccess(channel);
          channel.fireGenericEvent(responsePattern.getResponseEvent());
        }

        caughtEvent = null;
        return;
      }
      throw e;
    } catch (Exception e) {
      logger.debug("[{}] - while check balance", this, e);
      caughtEvent.respondFailure(channel, e.getMessage());
      caughtEvent = null;
      return;
    }

    logger.debug("[{}] - balance value: {}", this, balanceValue);
    if (balanceValue < minimumAmount) {
      logger.debug("[{}] - balance value is less then minimum [balance: {} < minimum: {}]", this, balanceValue, minimumAmount);
      channel.fireGenericEvent(lessThanMinimumEvent, balanceValue);
      if (failOnLessThanMinimum) {
        caughtEvent.respondFailure(channel, "not enough money in the balance: " + balanceValue);
        logger.debug("[{}] - event {} failed", this, caughtEvent);
      } else {
        caughtEvent.respondSuccess(channel);
        logger.debug("[{}] - event {} succeed", this, caughtEvent);
      }
    } else {
      channel.fireGenericEvent(moreThanMinimumEvent, balanceValue);
      caughtEvent.respondSuccess(channel);
      logger.debug("[{}] - event {} succeed", this, caughtEvent);
    }
    caughtEvent = null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  private USSDResponsePattern checkUSSD(final String response) {
    for (USSDResponsePattern ussdResponsePattern : ussdResponsePatterns) {
      if (response.matches(ussdResponsePattern.getResponsePattern())) {
        return ussdResponsePattern;
      }
    }
    return null;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/*
 * This script should be activated with FindDstCardAction script only because
 * of using parameterized event. 
 * */

@Script(name = "TF Start transfer", doc = "starts transfer")
public class TFStartTransfer implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(TFStartTransfer.class);

  protected String event = "TFStartTransfer";
  private String actionName = "TFTransferAction";

  protected volatile GenericEvent incomingEvent = null;

  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "action", doc = "name of action, which can provide money transferEx")
  public void setAction(final String action) {
    this.actionName = action;
  }

  public String getAction() {
    return actionName;
  }

  @Override
  public String describeBusinessActivity() {
    return "starts transfer";
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return null != incomingEvent;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    String srcNumber = incomingEvent.getSuccessResponce();
    String dstNumber = incomingEvent.getErrorResponce();

    logger.debug("[{}] - my number: {}. Initiate money transfer: {} -> {}", this, channel.getSimData().getPhoneNumber().getValue(), srcNumber, dstNumber);

    /*
    try{
      channel.executeAction(new Action(actionName, srcNumber, dstNumber), 3, TimePeriod.inMinutes(2));
    } catch (Exception e){
      logger.error("failed to initiate transfer money", e);
    } finally {
    */
    incomingEvent = null;
    //}
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}

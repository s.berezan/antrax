/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import com.flamesgroup.antrax.automation.predictions.Prediction;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class ExcludeGatewayPrediction implements Prediction {

  private static final long serialVersionUID = 5070692261837422752L;

  private final Set<String> servers;

  public ExcludeGatewayPrediction(final String... servers) {
    this.servers = new HashSet<>(Arrays.asList(servers));
  }

  @Override
  public String toLocalizedString() {
    String[] serverArr = servers.toArray(new String[servers.size()]);
    if (serverArr.length == 0) {
      return "any";
    }
    StringBuilder retval = new StringBuilder("any except ");
    for (int i = 0; i < serverArr.length; ++i) {
      retval.append(serverArr[i]);
      if (i + 1 < serverArr.length) {
        retval.append(" and ");
      }
    }
    return retval.toString();
  }

  @Override
  public Prediction and(final Prediction prediction) {
    if (prediction instanceof IncludeGatewayPrediction) {
      return prediction.and(this);
    } else if (prediction instanceof ExcludeGatewayPrediction) {
      ExcludeGatewayPrediction excludeGatewaysPrediction = (ExcludeGatewayPrediction) prediction;
      Set<String> union = new HashSet<>(this.servers);
      union.addAll(excludeGatewaysPrediction.servers);
      return new ExcludeGatewayPrediction(union.toArray(new String[union.size()]));
    } else {
      throw new IllegalArgumentException("Unknown class: " + prediction.getClass().getName());
    }
  }

  @Override
  public Prediction or(final Prediction prediction) {
    if (prediction instanceof IncludeGatewayPrediction) {
      return prediction.or(this);
    } else if (prediction instanceof ExcludeGatewayPrediction) {
      ExcludeGatewayPrediction excludeGatewaysPrediction = (ExcludeGatewayPrediction) prediction;
      Set<String> intersection = new HashSet<>(this.servers);
      intersection.retainAll(excludeGatewaysPrediction.servers);
      return new ExcludeGatewayPrediction(intersection.toArray(new String[intersection.size()]));
    } else {
      throw new IllegalArgumentException("Unknown class: " + prediction.getClass().getName());
    }
  }

  @Override
  public Prediction trimToBigger(final Prediction other) {
    return null;
  }

  @Override
  public Prediction trimToSmaller(final Prediction other) {
    return null;
  }

  public Set<String> getServers() {
    return servers;
  }

  Locale getLocale() {
    return Locale.getDefault();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import com.flamesgroup.antrax.automation.predictions.BasePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;

public class CallAttemptsPrediction extends BasePrediction {

  private static final long serialVersionUID = 2934538912163119629L;

  private final int leftAttempts;

  public CallAttemptsPrediction(final int leftAttempts) {
    this.leftAttempts = leftAttempts;
  }

  @Override
  public String toLocalizedString() {
    return leftAttempts + " call attempts";
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof CallAttemptsPrediction;
  }

  @Override
  protected int compareTo(final Prediction other) {
    return leftAttempts - ((CallAttemptsPrediction) other).leftAttempts;
  }

}

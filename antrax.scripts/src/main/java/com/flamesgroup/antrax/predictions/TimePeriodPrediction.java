/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import com.flamesgroup.antrax.automation.predictions.BasePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.commons.TimeUtils;

public class TimePeriodPrediction extends BasePrediction {

  private static final long serialVersionUID = 286224989865011321L;

  private final long timeout;

  public TimePeriodPrediction(final long timeout) {
    this.timeout = timeout;
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof TimePeriodPrediction || other instanceof TimePrediction;
  }

  @Override
  protected int compareTo(final Prediction other) {
    if (other instanceof TimePeriodPrediction) {
      return (int) (timeout - ((TimePeriodPrediction) other).timeout);
    }

    if (other instanceof TimePrediction) {
      TimePrediction that = (TimePrediction) other;
      return (int) (timeout - (that.getTime() - System.currentTimeMillis()));
    }
    throw new IllegalStateException();
  }

  @Override
  public String toLocalizedString() {
    return "timeout passed " + TimeUtils.writeTime(timeout);
  }

  public long getTimeout() {
    return timeout;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

import java.io.Serializable;

public abstract class SimpleActivityScriptComposite implements SimpleActivityScript, StatefullScript {

  private static final long serialVersionUID = 5783373845449246769L;

  @StateField
  private final SimpleActivityScript first;
  @StateField
  private final SimpleActivityScript second;

  public SimpleActivityScriptComposite(final SimpleActivityScript first, final SimpleActivityScript second) {
    if (first == null) {
      throw new NullPointerException("first can't be null");
    }
    if (second == null) {
      throw new NullPointerException("second can't be null");
    }
    this.first = first;
    this.second = second;
  }

  @Override
  public void handleActivityEnded() {
    first.handleActivityEnded();
    second.handleActivityEnded();
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    first.handleActivityStarted(gsmGroup);
    second.handleActivityStarted(gsmGroup);
  }

  @Override
  public void handlePeriodStart() {
    first.handlePeriodStart();
    second.handlePeriodStart();
  }

  @Override
  public void handlePeriodEnd() {
    first.handlePeriodEnd();
    second.handlePeriodEnd();
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    first.handleGenericEvent(event, args);
    second.handleGenericEvent(event, args);
  }

  @Override
  public void setSimData(final SimData simData) {
    first.setSimData(simData);
    second.setSimData(simData);
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
    first.handleSessionStarted(gateway);
    second.handleSessionStarted(gateway);
  }

  @Override
  public void handleSessionEnded() {
    first.handleSessionEnded();
    second.handleSessionEnded();
  }

}

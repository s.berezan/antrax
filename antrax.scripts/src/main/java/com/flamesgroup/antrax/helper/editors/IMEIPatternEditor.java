/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.commons.IMEIPattern;

import java.awt.*;

import javax.swing.*;

public class IMEIPatternEditor extends BasePropertyEditor<IMEIPattern> {

  private final Panel panel;
  private final JTextField textField;
  private IMEIPattern imeiPattern;

  public IMEIPatternEditor() {
    panel = new Panel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    textField = new JTextField();
    textField.setText("XXXXXXXXXXXXXX");
    JLabel label = new JLabel("IMEI pattern:");
    label.setLabelFor(textField);
    panel.add(label);
    panel.add(textField);
  }

  @Override
  public Component getEditorComponent() {
    return panel;
  }

  @Override
  public Class<? extends IMEIPattern> getType() {
    return IMEIPattern.class;
  }

  @Override
  public IMEIPattern getValue() {
    return imeiPattern;
  }

  @Override
  public void setValue(final IMEIPattern value) {
    textField.setText(value.getPattern());
  }

  @Override
  public boolean isValid() {
    try {
      imeiPattern = new IMEIPattern(textField.getText());
      return true;
    } catch (final IllegalArgumentException e) {
      JOptionPane.showMessageDialog(textField, e.getMessage());
      return false;
    }
  }

}

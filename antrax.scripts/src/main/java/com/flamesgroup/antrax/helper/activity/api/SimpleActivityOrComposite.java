/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;

public class SimpleActivityOrComposite extends SimpleActivityScriptComposite implements SimpleActivityScript {

  private static final long serialVersionUID = -3833803271867094732L;

  @StateField
  private final SimpleActivityScript first;
  @StateField
  private final SimpleActivityScript second;

  public SimpleActivityOrComposite(final SimpleActivityScript first, final SimpleActivityScript second) {
    super(first, second);
    this.first = first;
    this.second = second;
  }

  @Override
  public SimpleActivityScript and(final SimpleActivityScript other) {
    if (other == null) {
      throw new NullPointerException("You can't join script with null");
    }
    return new SimpleActivityAndComposite(this, other);
  }

  @Override
  public SimpleActivityScript or(final SimpleActivityScript other) {
    if (other == null) {
      throw new NullPointerException("You can't join script with null");
    }
    return new SimpleActivityOrComposite(this, other);
  }

  @Override
  public boolean isActivityAllowed() {
    return first.isActivityAllowed() || second.isActivityAllowed();
  }

  @Override
  public String toString() {
    return String.format("%s.or(%s)", first, second);
  }

  @Override
  public Prediction predictStart() {
    return first.predictStart().or(second.predictStart());
  }

  @Override
  public Prediction predictEnd() {
    return first.predictEnd().and(second.predictEnd());
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return new ScriptSaver(first.getScriptSaver(), second.getScriptSaver());
  }

}

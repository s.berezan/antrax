/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerMonth;
import com.flamesgroup.antrax.predictions.CallDurationPrediction;
import com.flamesgroup.antrax.scripts.utils.LimitCallDurationPerMonthScriplet;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public class LimitCallDurationPerMonth extends BaseLimitCharacteristicPerMonth {

  private static final long serialVersionUID = -1487655116418348538L;

  private LimitCallDurationPerMonthScriplet activityTimeoutByEventScriplet;

  public void setActivityTimeoutByEventScriplet(final LimitCallDurationPerMonthScriplet limitCallDurationPerMonthScriplet) {
    this.activityTimeoutByEventScriplet = limitCallDurationPerMonthScriplet;
    setLimitValue(new VariableLong(limitCallDurationPerMonthScriplet.getTimeout().getMin().getPeriod(), limitCallDurationPerMonthScriplet.getTimeout().getMax().getPeriod()));
    setStartDayMonth(limitCallDurationPerMonthScriplet.getStartDayOfMonth());
  }

  public LimitCallDurationPerMonthScriplet getActivityTimeoutByEventScriplet() {
    return activityTimeoutByEventScriplet;
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    return simData.getCallDuration();
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new CallDurationPrediction(value);
  }

}

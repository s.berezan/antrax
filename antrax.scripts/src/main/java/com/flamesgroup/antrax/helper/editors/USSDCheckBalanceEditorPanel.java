/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.editors.components.LabeledPanel;
import com.flamesgroup.antrax.helper.editors.components.RegexesPanel;

import java.awt.*;

import javax.swing.*;

public class USSDCheckBalanceEditorPanel extends JPanel implements CheckBalanceHelperEditor.CheckBalanceHelperProvider {

  private static final long serialVersionUID = -5906834890640747802L;

  private final JComboBox txtUssd = new JComboBox();
  private final RegexesPanel regexes = new RegexesPanel(400);

  public USSDCheckBalanceEditorPanel() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    txtUssd.setEditable(true);
    add(new LabeledPanel("ussd:", txtUssd));
    add(regexes);
  }

  @Override
  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper((String) txtUssd.getSelectedItem(), regexes.listRegexes());
  }

  @Override
  public Component getEditorComponent() {
    return this;
  }

  @Override
  public Class<? extends CheckBalanceHelper> getProvidedHelperClass() {
    return USSDCheckBalanceHelper.class;
  }

  @Override
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    USSDCheckBalanceHelper helper = (USSDCheckBalanceHelper) checkBalanceHelper;
    txtUssd.setSelectedItem(helper.getUSSD());
    regexes.setRegexes(helper.getRegexes(), new String[0]);
  }

}

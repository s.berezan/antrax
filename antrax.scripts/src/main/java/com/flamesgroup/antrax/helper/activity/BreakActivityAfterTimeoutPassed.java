/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;

public class BreakActivityAfterTimeoutPassed extends BaseSimpleActivityScript {

  private static final long serialVersionUID = 3669551584467588023L;

  @StateField
  private long startTime;

  private volatile long limit;

  private volatile TimeInterval limitValue;

  private final ScriptSaver saver = new ScriptSaver();

  public void setTimeoutLimit(final TimeInterval interval) {
    this.limitValue = interval;
    calculateLimit();
  }

  private void calculateLimit() {
    limit = limitValue.random();
  }

  @Override
  public boolean isActivityAllowed() {
    if (startTime == 0) {
      return true;
    }
    return System.currentTimeMillis() - startTime <= limit;
  }

  @Override
  public void handlePeriodStart() {
    startTime = System.currentTimeMillis();
    saver.save();
  }

  @Override
  public void handlePeriodEnd() {
    startTime = 0;
    saver.save();
  }

  @Override
  public Prediction predictEnd() {
    if (isActivityAllowed()) {
      if (startTime == 0) {
        handlePeriodStart(); //was reset script state
      }
      return new TimePeriodPrediction(limit - (System.currentTimeMillis() - startTime));
    }
    return new AlwaysTruePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (startTime != 0) {
      handlePeriodEnd(); //was unplug sim unit
    }
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new AlwaysFalsePrediction();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.helper.editors.components.TimePeriodEditor;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;

import java.awt.*;

import javax.swing.*;

public class IntervalEditor extends BasePropertyEditor<TimeInterval> {

  private static class EditorComponent extends JPanel {
    private static final long serialVersionUID = 8381673742485638317L;
    private final TimePeriodEditor min = new TimePeriodEditor();
    private final TimePeriodEditor max = new TimePeriodEditor();

    public EditorComponent() {
      setOpaque(false);
      setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
      add(min, 0);
      add(new JLabel(".."), 1);
      add(max, 2);
    }

    public TimeInterval getValue() {
      return new TimeInterval((TimePeriod) min.getValue(), (TimePeriod) max.getValue());
    }

    public void setValue(final TimeInterval i) {
      if (i == null) {
        min.setValue(0);
        max.setValue(0);
      } else {
        min.setValue(i.getMin());
        max.setValue(i.getMax());
      }
    }
  }

  private final EditorComponent editorComponent = new EditorComponent();

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Class<? extends TimeInterval> getType() {
    return TimeInterval.class;
  }

  @Override
  public TimeInterval getValue() {
    return editorComponent.getValue();
  }

  @Override
  public void setValue(final TimeInterval value) {
    editorComponent.setValue(value);
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    IntervalEditor intervalEditor = new IntervalEditor();

    frame.add(intervalEditor.getEditorComponent());

    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.predictions.TimePrediction;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

import java.util.Calendar;

/**
 * Base class to create activity scripts, which will limit activity with some
 * characteristic value per month. Such script will remember initial
 * characteristic value each first day of the month
 */
public abstract class BaseLimitCharacteristicPerMonth extends BaseSimpleActivityScript {

  /**
   * Extracts characteristic, which is used for limit. For example: <code>
   * return simData.getTotalCount()
   * </code>
   *
   * @param simData
   * @return
   */
  protected abstract long getCharacteristicValue(SimData simData);

  protected abstract Prediction getCharacteristicPrediction(long value);

  private static final long serialVersionUID = 6341022929052249519L;
  @StateField
  private volatile long limit;

  private volatile SimData simData;
  @StateField
  private volatile long lastMonthCharacteristicValue;
  @StateField
  private volatile long nextMonthTimestamp;

  private VariableLong limitValue;
  private Integer startDayMonth = 1;

  private final ScriptSaver saver = new ScriptSaver();

  protected void setLimitValue(final VariableLong limit) {
    this.limitValue = limit;
  }

  protected void setStartDayMonth(final Integer startDayMonth) {
    this.startDayMonth = startDayMonth;
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
    saver.save();
  }

  @Override
  public final boolean isActivityAllowed() {
    return getCharacteristic() < limit;
  }

  private long getCharacteristic() {
    if (getCurrentDate() >= nextMonthTimestamp) {
      nextMonthTimestamp = getNextMonth();
      lastMonthCharacteristicValue = getCharacteristicValue(simData);
      limit = limitValue.random();
      saver.save();
    }
    return getCharacteristicValue(simData) - lastMonthCharacteristicValue;
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", getClass().getSimpleName(), limitValue);
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new TimePrediction(getNextMonth());
  }

  @Override
  public Prediction predictEnd() {
    if (!isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return getCharacteristicPrediction(limit - (getCharacteristicValue(simData) - lastMonthCharacteristicValue));
  }

  private long getCurrentDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.getTimeInMillis();
  }

  private long getNextMonth() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    if (cal.get(Calendar.DAY_OF_MONTH) >= startDayMonth) {
      cal.add(Calendar.MONTH, 1);
    }
    cal.set(Calendar.DAY_OF_MONTH, startDayMonth);
    return cal.getTimeInMillis();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.business.ReplacePattern;

import java.awt.*;

import javax.swing.*;

public class ReplacePatternPanel extends JPanel {

  private static final long serialVersionUID = -2237568232087637203L;

  private final JTextField pattern;
  private final JTextField replaceBy;

  public ReplacePatternPanel() {
    pattern = new JTextField();
    replaceBy = new JTextField();
    JLabel patternLabel = new JLabel("Pattern");
    patternLabel.setLabelFor(pattern);
    JLabel replaceByLabel = new JLabel("ReplaceBy");
    replaceByLabel.setLabelFor(replaceBy);
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    pattern.setEditable(true);
    replaceBy.setEditable(true);

    add(patternLabel);
    add(pattern);
    add(replaceByLabel);
    add(replaceBy);
    this.setPreferredSize(new Dimension(400, this.getPreferredSize().height));
  }

  private String getPattern() {
    String patternString = pattern.getText();
    if (patternString == null) {
      return "";
    }
    return patternString;
  }

  private String getReplaceBy() {
    String eventString = replaceBy.getText();
    if (eventString == null) {
      return "";
    }
    return eventString;
  }

  public ReplacePattern getReplacePattern() {
    return new ReplacePattern(getPattern(), getReplaceBy());
  }

  public void setReplacePattern(final ReplacePattern replacePattern) {
    pattern.setText(replacePattern.getPattern());
    replaceBy.setText(replacePattern.getReplaceBy());
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.AllowActivityInRandomPeriodScriplet;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;

import java.util.Calendar;

public class AllowActivityInRandomPeriod extends BaseSimpleActivityScript {

  private static final long serialVersionUID = -6017131247669481709L;

  private AllowActivityInRandomPeriodScriplet randomPeriodScriplet;

  private final TimePeriod midnight = new TimePeriod(TimePeriod.inHours(24));

  private final ScriptSaver saver = new ScriptSaver();

  private long startPeriod;
  private long endPeriod;

  public void setRandomPeriodScriplet(final AllowActivityInRandomPeriodScriplet randomPeriodScriplet) {
    this.randomPeriodScriplet = randomPeriodScriplet;

    startPeriod = randomPeriodScriplet.getStartPeriod().random();
    endPeriod = randomPeriodScriplet.getEndPeriod().random();
  }

  @Override
  public boolean isActivityAllowed() {
    if (startPeriod == 0 && endPeriod == 0) {
      startPeriod = randomPeriodScriplet.getStartPeriod().random();
      endPeriod = randomPeriodScriplet.getEndPeriod().random();
    }

    long now = getOffsetAfterMidnight();
    return now > startPeriod && now < endPeriod;
  }

  @Override
  public void handlePeriodEnd() {
    startPeriod = 0;
    endPeriod = 0;
  }

  @Override
  public Prediction predictEnd() {
    long timeout = endPeriod - getOffsetAfterMidnight();
    if (timeout < 0) {
      return new AlwaysTruePrediction();
    }
    return new TimePeriodPrediction(timeout);
  }

  @Override
  public Prediction predictStart() {
    long now = getOffsetAfterMidnight();
    long timeout = startPeriod - now;
    if (timeout < 0) {
      return isActivityAllowed() ? new AlwaysTruePrediction() : new TimePeriodPrediction(midnight.getPeriod() - Math.abs(timeout));
    }
    return new TimePeriodPrediction(timeout);
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  private long getOffsetAfterMidnight() {
    long now = System.currentTimeMillis();
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(now);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    now -= cal.getTimeInMillis();
    return now;
  }

}

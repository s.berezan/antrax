/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.checkbalance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexBalanceParser {

  private static final Logger logger = LoggerFactory.getLogger(RegexBalanceParser.class);

  private RegexBalanceParser() {
  }

  public static double parseBalance(final String balanceString, final String... regexes) throws BadResponseException {
    logger.debug("[{}] - matching balance string with regexes: {}", RegexBalanceParser.class, Arrays.toString(regexes));
    for (String r : regexes) {
      Matcher matcher = Pattern.compile(r).matcher(balanceString);
      if (matcher.matches()) {
        logger.debug("[{}] - balance string [{}] matched by {}", RegexBalanceParser.class, balanceString, r);

        String firstPart = matcher.groupCount() > 0 ? matcher.replaceFirst("$1") : "";
        if (firstPart.isEmpty()) {
          logger.debug("[{}] - no group found, so return zero balance", RegexBalanceParser.class);
          return 0;
        }

        firstPart = firstPart.replaceAll("\\D+", "");
        String secondPart = "";
        if (matcher.groupCount() == 2) {
          secondPart = matcher.replaceFirst("$2");
          secondPart = secondPart.replaceAll("\\D+", "");
        }
        double balance = Double.parseDouble(firstPart + "." + secondPart);
        logger.debug("[{}] - returning {}", RegexBalanceParser.class, balance);
        return balance;
      }
    }

    logger.warn("[{}] - any regex matches balance string, failing", RegexBalanceParser.class);
    throw new BadResponseException(balanceString);
  }

}

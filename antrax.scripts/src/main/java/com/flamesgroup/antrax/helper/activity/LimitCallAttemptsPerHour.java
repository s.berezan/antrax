/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerHour;
import com.flamesgroup.antrax.predictions.CallAttemptsPrediction;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

/**
 * Allows activity to be started when the number of call attempts during a current
 * hour is less than limit. Also it will terminate activity when limit exceeds.
 * <p>
 * It limits call attempts with random number in ranges of min and max values.
 * This number recounts every new activity start.
 */
public class LimitCallAttemptsPerHour extends BaseLimitCharacteristicPerHour {

  private static final long serialVersionUID = -678105843863202986L;

  public void setCallAttemptsLimit(final VariableLong limit) {
    setLimitValue(limit);
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    return simData.getTotalCallsCount();
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new CallAttemptsPrediction((int) value);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class CheckBalanceHelperEditor extends BasePropertyEditor<CheckBalanceHelper> {

  public interface CheckBalanceHelperProvider {
    CheckBalanceHelper getCheckBalanceHelper();

    void setCheckBalanceHelper(CheckBalanceHelper checkBalanceHelper);

    Class<? extends CheckBalanceHelper> getProvidedHelperClass();

    Component getEditorComponent();
  }

  private static class Editor extends JPanel implements RegistryAccessListener {
    private static final long serialVersionUID = 317782694197854193L;

    private final JComboBox typeComboBox = new JComboBox();
    private final JPanel paymentsSettingsPanel = new JPanel();
    private final CardLayout cardLayout = new CardLayout();
    private final Map<Class<?>, String> editors = new HashMap<>();
    private final Map<String, CheckBalanceHelperProvider> editorsByName = new HashMap<>();
    private CheckBalanceHelperProvider currEditor;

    public Editor() {
      this.setLayout(new BorderLayout());
      this.add(typeComboBox, BorderLayout.NORTH);
      this.add(paymentsSettingsPanel, BorderLayout.CENTER);
      paymentsSettingsPanel.setLayout(cardLayout);

      typeComboBox.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          String selectedItem = (String) typeComboBox.getSelectedItem();
          cardLayout.show(paymentsSettingsPanel, selectedItem);
          currEditor = editorsByName.get(selectedItem);
        }
      });
      addPanel(new USSDCheckBalanceEditorPanel(), "ussd based");
    }

    private void addPanel(final CheckBalanceHelperProvider panel, final String name) {
      ((DefaultComboBoxModel) typeComboBox.getModel()).addElement(name);
      paymentsSettingsPanel.add(panel.getEditorComponent(), name);
      editors.put(panel.getProvidedHelperClass(), name);
      editorsByName.put(name, panel);
      if (currEditor == null) {
        currEditor = panel;
      }
    }

    public CheckBalanceHelper getValue() {
      return currEditor.getCheckBalanceHelper();
    }

    public void setValue(final CheckBalanceHelper value) {
      typeComboBox.setSelectedItem(editors.get(value.getClass()));
      currEditor.setCheckBalanceHelper(value);
    }

    @Override
    public void setRegistryAccess(final RegistryAccess registry) {
      for (Object o : editorsByName.values()) {
        if (o instanceof RegistryAccessListener) {
          ((RegistryAccessListener) o).setRegistryAccess(registry);
        }
      }
    }

  }

  private final Editor editor = new Editor();

  @Override
  public Component getEditorComponent() {
    return editor;
  }

  @Override
  public Class<? extends CheckBalanceHelper> getType() {
    return CheckBalanceHelper.class;
  }

  @Override
  public CheckBalanceHelper getValue() {
    return editor.getValue();
  }

  @Override
  public void setValue(final CheckBalanceHelper value) {
    editor.setValue(value);
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    final CheckBalanceHelperEditor paymentHelperEditor = new CheckBalanceHelperEditor();
    frame.getContentPane().add(paymentHelperEditor.getEditorComponent());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        System.out.println(paymentHelperEditor.getValue());
        paymentHelperEditor.setValue(new USSDCheckBalanceHelper("*111#", ".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*"));
        frame.setVisible(true);
      }
    });
  }

}

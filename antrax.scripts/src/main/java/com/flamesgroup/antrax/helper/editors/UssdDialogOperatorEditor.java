/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.payment.UssdDialogOperator;
import com.flamesgroup.antrax.helper.editors.components.UssdDialogOperatorEditorPanel;

import java.awt.*;

public class UssdDialogOperatorEditor extends BasePropertyEditor<UssdDialogOperator> implements RegistryAccessListener {

  private final UssdDialogOperatorEditorPanel editor = new UssdDialogOperatorEditorPanel();

  @Override
  public Class<? extends UssdDialogOperator> getType() {
    return UssdDialogOperator.class;
  }

  @Override
  public UssdDialogOperator getValue() {
    return editor.getUssdDialogOperator();
  }

  @Override
  public void setValue(final UssdDialogOperator value) {
    editor.setUssdDialogOperator(value);
  }

  @Override
  public Component getEditorComponent() {
    return editor;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    editor.setRegistryAccess(registry);
  }

}

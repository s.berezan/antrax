/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.ActivityTimeoutByEventScriplet;

import java.io.Serializable;

public class ActivityTimeoutByEvent extends BaseSimpleActivityScript {

  private static final long serialVersionUID = -2351011451563158408L;

  @StateField
  private long startTime;

  private final ScriptSaver saver = new ScriptSaver();

  private ActivityTimeoutByEventScriplet activityTimeoutByEventScriplet;

  private volatile boolean isActivityTimeoutEvent;
  private long limit;

  public void setActivityTimeoutByEventScriplet(final ActivityTimeoutByEventScriplet activityTimeoutByEventScriplet) {
    this.activityTimeoutByEventScriplet = activityTimeoutByEventScriplet;
    limit = activityTimeoutByEventScriplet.getTimeout().random();
  }

  public ActivityTimeoutByEventScriplet getActivityTimeoutByEventScriplet() {
    return activityTimeoutByEventScriplet;
  }

  @Override
  public boolean isActivityAllowed() {
    if (isActivityTimeoutEvent) {
      startTime = System.currentTimeMillis();
      isActivityTimeoutEvent = false;
      saver.save();
      return isActivityTimeoutEvent;
    } else if (startTime == 0) {
      return true;
    } else {
      return System.currentTimeMillis() - startTime >= limit;
    }
  }

  @Override
  public Prediction predictEnd() {
    if (isActivityAllowed()) {
      return new AlwaysFalsePrediction();
    }
    return new AlwaysTruePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new TimePeriodPrediction(limit - (System.currentTimeMillis() - startTime));
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.activityTimeoutByEventScriplet.getEvent().equals(event)) {
      isActivityTimeoutEvent = true;
      saver.save();
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerActivity;
import com.flamesgroup.antrax.predictions.CallAttemptsPrediction;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public class LimitCallAttemptsPerActivity extends BaseLimitCharacteristicPerActivity {

  private static final long serialVersionUID = 8355308804525072647L;

  public void setAttemptsLimit(final VariableLong limit) {
    setLimitValue(limit);
  }

  public VariableLong getAttemptsLimit() {
    return new VariableLong(10, 15);
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    return simData.getTotalCallsCount();
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new CallAttemptsPrediction((int) value);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

import java.io.Serializable;

public abstract class BaseSimpleActivityScript implements SimpleActivityScript, SessionListener, ActivityPeriodListener, ActivityListener, SimDataListener, GenericEventListener {

  private static final long serialVersionUID = -463085834826102393L;

  @Override
  public SimpleActivityScript and(final SimpleActivityScript other) {
    if (other == null) {
      throw new NullPointerException("You can't join script with null");
    }
    return new SimpleActivityAndComposite(this, other);
  }

  @Override
  public SimpleActivityScript or(final SimpleActivityScript other) {
    if (other == null) {
      throw new NullPointerException("You can't join script with null");
    }
    return new SimpleActivityOrComposite(this, other);
  }

  @Override
  public void handleActivityEnded() {
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
  }

  @Override
  public void setSimData(final SimData simData) {
  }

  @Override
  public void handlePeriodEnd() {
  }

  @Override
  public void handlePeriodStart() {
  }

  @Override
  public void handleSessionEnded() {
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
  }

}

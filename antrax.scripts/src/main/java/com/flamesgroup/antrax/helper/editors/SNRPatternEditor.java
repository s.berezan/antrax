/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.commons.SNRPattern;

import java.awt.*;

import javax.swing.*;

public class SNRPatternEditor extends BasePropertyEditor<SNRPattern> {

  private final Panel panel;
  private final JTextField textField;
  private SNRPattern snrPattern;

  public SNRPatternEditor() {
    panel = new Panel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    textField = new JTextField();
    textField.setText("XXXXXX");
    JLabel label = new JLabel("SNR pattern:");
    label.setLabelFor(textField);
    panel.add(label);
    panel.add(textField);
  }

  @Override
  public Component getEditorComponent() {
    return panel;
  }

  @Override
  public Class<? extends SNRPattern> getType() {
    return SNRPattern.class;
  }

  @Override
  public SNRPattern getValue() {
    return snrPattern;
  }

  @Override
  public void setValue(final SNRPattern value) {
    textField.setText(value.getPattern());
  }

  @Override
  public boolean isValid() {
    try {
      snrPattern = new SNRPattern(textField.getText());
      return true;
    } catch (final IllegalArgumentException e) {
      JOptionPane.showMessageDialog(textField, e.getMessage());
      return false;
    }
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.payment.CallPaymentHelper;
import com.flamesgroup.antrax.helper.business.payment.PaymentHelper;
import com.flamesgroup.antrax.helper.editors.PaymentHelperEditor;
import com.flamesgroup.antrax.scripts.utils.UIComponentRegistryAccess;

import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.*;

public class CallBasedPaymentEditorPanel extends JPanel implements PaymentHelperEditor.PaymentHelperProvider, RegistryAccessListener {

  private static final long serialVersionUID = -7232452633375340968L;

  private final JComboBox<String> txtCall = new JComboBox<>();
  private final JComboBox<String> txtOperator = new JComboBox<>();
  private final JCheckBox enableCheckResponse = new JCheckBox();
  private final JTextField waitingSmsTimeOut = new JTextField();
  private String[] regexesModel = new String[0];
  private final RegexesPanel regexesPanel = new RegexesPanel(200);
  private final JTextArea txtHelp = new JTextArea();

  private UIComponentRegistryAccess registry;

  public CallBasedPaymentEditorPanel() {
    initGUI();
  }

  @Override
  public Component getEditorComponent() {
    return this;
  }

  @Override
  public void setPaymentHelper(final PaymentHelper paymentHelper) {
    CallPaymentHelper helper = (CallPaymentHelper) paymentHelper;
    txtCall.setSelectedItem(helper.getCall());
    txtOperator.setSelectedItem(helper.getOperator());
    enableCheckResponse.setSelected(helper.isEnableCheckResponse());
    waitingSmsTimeOut.setText(String.valueOf(helper.getWaitingTimeOut()));
    regexesPanel.setRegexes(helper.getResponseCheckerRegexes(), regexesModel);
    txtHelp.setText(getTextMessage());
  }

  @Override
  public PaymentHelper getPaymentHelper() {
    if (registry != null) {
      registry.addValue("antrax-scripts.payment.call", (String) txtCall.getSelectedItem());
    }
    String operator = (String) txtOperator.getSelectedItem();
    String call = (String) txtCall.getSelectedItem();
    int waitingTimeOut = Integer.parseInt(waitingSmsTimeOut.getText());
    String[] listRegexps = regexesPanel.listRegexes();
    if (registry != null) {
      registry.addValue("antrax-scripts.payment.regexes", listRegexps);
    }
    return new CallPaymentHelper(operator, call, enableCheckResponse.isSelected(), waitingTimeOut, listRegexps);
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = new UIComponentRegistryAccess(registry);
    readRegistry();
  }

  private void readRegistry() {
    String[] paths = registry.listPaths(Pattern.compile("(vauchers[.][^.]*)[.].*"));
    Set<String> pathsSet = new HashSet<>(Arrays.asList(paths));
    txtOperator.setModel(new DefaultComboBoxModel<>(pathsSet.toArray(new String[pathsSet.size()])));
    txtCall.setModel(new DefaultComboBoxModel<>(registry.listValues("antrax-scripts.payment.ussd")));
    regexesModel = registry.listValues("antrax-scripts.payment.regexes");
    regexesPanel.removeAll();
    regexesPanel.addResponceChecker(null, regexesModel);
  }

  @Override
  public Class<? extends PaymentHelper> getProvidedHelperClass() {
    return CallPaymentHelper.class;
  }

  private void initGUI() {
    txtCall.setEditable(true);
    regexesPanel.setBorder(BorderFactory.createTitledBorder("Check SMS regex (****)"));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(new LabeledPanel("operator (*):", txtOperator));
    add(new LabeledPanel("call (**):", txtCall));
    add(new LabeledPanel("enable check response:", enableCheckResponse));
    LabeledPanel timeLabeledPanel = new LabeledPanel("time (***):", waitingSmsTimeOut);
    add(timeLabeledPanel);
    add(regexesPanel);
    
    enableCheckResponse.addItemListener(e -> {
      txtHelp.setText(getTextMessage());
      waitingSmsTimeOut.setVisible(enableCheckResponse.isSelected());
      regexesPanel.setVisible(enableCheckResponse.isSelected());
      timeLabeledPanel.setVisible(enableCheckResponse.isSelected());
    });

    enableCheckResponse.setSelected(true);
    add(txtHelp);
  }

  private String getTextMessage() {
    if(enableCheckResponse.isSelected()) {
      return "* this name is used to determine registry key where to look for vauchers\n" +
    /*   */"** use $ to mark the place for vaucher. For example: *100*$#\n" +
    /*   */"*** time in seconds for waiting SMS response\n" +
    /*   */"**** place regexes for checking SMS here";
    } else {
      return "* this name is used to determine registry key where to look for vauchers\n" +
    /*   */"** use $ to mark the place for vaucher. For example: *100*$#";
    }
  }

}

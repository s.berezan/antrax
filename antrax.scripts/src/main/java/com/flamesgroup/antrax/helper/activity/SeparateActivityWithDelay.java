/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;

public class SeparateActivityWithDelay extends BaseSimpleActivityScript {

  private static final long serialVersionUID = 808472384548291419L;

  @StateField
  private volatile long delay;
  @StateField
  private volatile long lastActivityEndTimeout;

  private volatile TimeInterval delayValue;

  private final ScriptSaver saver = new ScriptSaver();

  public void setDelay(final TimeInterval delay) {
    this.delayValue = delay;
    calculateDelay();
  }

  private void calculateDelay() {
    delay = delayValue.random();
    saver.save();
  }

  @Override
  public boolean isActivityAllowed() {
    return System.currentTimeMillis() - lastActivityEndTimeout >= delay;
  }

  @Override
  public void handlePeriodEnd() {
    this.lastActivityEndTimeout = System.currentTimeMillis();
    calculateDelay();
  }

  @Override
  public Prediction predictEnd() {
    if (!isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new AlwaysFalsePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new TimePeriodPrediction(delay - (System.currentTimeMillis() - lastActivityEndTimeout));
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.editors.IntervalEditor;
import com.flamesgroup.antrax.scripts.utils.AllowActivityInRandomPeriodScriplet;

import java.awt.*;

import javax.swing.*;

public class AllowActivityInRandomPeriodScripletPanel extends JPanel {

  private static final long serialVersionUID = 3258020107070881403L;

  private final IntervalEditor startPeriodEditor = new IntervalEditor();
  private final IntervalEditor endPeriodEditor = new IntervalEditor();

  public AllowActivityInRandomPeriodScripletPanel() {
    setOpaque(false);
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    JPanel startPeriodPanel = new JPanel();
    startPeriodPanel.setLayout(new FlowLayout());

    startPeriodPanel.add(new JLabel("start period"));
    startPeriodPanel.add(startPeriodEditor.getEditorComponent());

    JPanel endPeriodPanel = new JPanel();
    endPeriodPanel.setLayout(new FlowLayout());

    endPeriodPanel.add(new JLabel("end period"));
    endPeriodPanel.add(endPeriodEditor.getEditorComponent());

    add(startPeriodPanel);
    add(endPeriodPanel);
  }

  public AllowActivityInRandomPeriodScriplet getAllowActivityInRandomPeriodScriplet() {
    return new AllowActivityInRandomPeriodScriplet(startPeriodEditor.getValue(), endPeriodEditor.getValue());
  }

  public void setAllowActivityInRandomPeriodScriplet(final AllowActivityInRandomPeriodScriplet scriplet) {
    startPeriodEditor.setValue(scriplet.getStartPeriod());
    endPeriodEditor.setValue(scriplet.getEndPeriod());
  }

}

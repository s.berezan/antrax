/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.scriplet;

import com.flamesgroup.antrax.helper.editors.ActivityScripletEditor;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletAtom;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletOperation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ScripletComponent extends JPanel {

  private static final long serialVersionUID = 7655812152881621911L;

  private final JLabel lblScriplet = ActivityScripletEditor.createLabel("", false);
  private final JLabel lblScripletValue = ActivityScripletEditor.createLabel("", false);

  private final JButton btnMoveLeft;
  private final JButton btnEdit;
  private final JButton btnRemove;
  private final JButton btnMoveRight;
  private final JButton btnAdd;

  private JToolBar toolbar;

  private final ScripletAtom scriplet;

  private final ActivityScripletEditor editor;
  private final JPopupMenu popupMenu = new JPopupMenu();

  public ScripletComponent(final ScripletAtom scriplet, final ActivityScripletEditor editor) {
    setLayout(new GridBagLayout());
    this.scriplet = scriplet;
    this.editor = editor;
    setOpaque(true);
    if (scriplet.getParent().canMoveLeft(scriplet)) {
      btnMoveLeft = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.LEFT_ICON));
    } else {
      btnMoveLeft = null;
    }
    btnEdit = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.EDIT_ICON));
    if (scriplet.getParent().canRemove(scriplet)) {
      btnRemove = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.REMOVE_ICON));
    } else {
      btnRemove = null;
    }
    if (scriplet.getParent().canMoveRight(scriplet)) {
      btnMoveRight = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.RIGHT_ICON));
    } else {
      btnMoveRight = null;
    }
    btnAdd = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.ADD_ICON));

    add(createToolbar(), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,
        0));
    add(lblScriplet, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    add(lblScripletValue, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
        0));

    lblScriplet.setText(String.format("<html><body><span style='font-size:10.5px; color:blue'>%s", scriplet.getScript()));
    lblScripletValue.setText(String.format("<html><body><span style='font-size:9px'>%s", scriplet.getValue()));
    initiazePopupMenu();
    initializeListeners();
  }

  private JToolBar createToolbar() {
    toolbar = new JToolBar();
    toolbar.setFloatable(false);
    toolbar.setRollover(true);
    if (btnMoveLeft != null) {
      toolbar.add(btnMoveLeft);
    }
    toolbar.add(btnEdit);
    if (btnRemove != null) {
      toolbar.add(btnRemove);
    }
    if (btnMoveRight != null) {
      toolbar.add(btnMoveRight);
    }
    toolbar.add(btnAdd);
    return toolbar;
  }

  private void initiazePopupMenu() {
    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        ScripletAtom newScriplet = ScripletFactory.createScripletFor(e.getActionCommand());
        if (scriplet.getParent() == null) {
          ScripletBlock block = new ScripletBlock(scriplet);
          block.addSibling(ScripletOperation.AND, newScriplet);
          editor.setValue(block);
        } else {
          (scriplet.getParent()).insertSibling(scriplet, ScripletOperation.AND, newScriplet);
          editor.refresh();
        }
      }
    };

    ButtonGroup group = new ButtonGroup();
    for (String s : ScripletFactory.listScripts()) {
      JRadioButtonMenuItem mi = new JRadioButtonMenuItem(s);
      group.add(mi);
      popupMenu.add(mi);
      mi.addActionListener(listener);
    }
  }

  private void initializeListeners() {
    btnAdd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        popupMenu.show(btnAdd, 0, 0);
      }
    });
    btnEdit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        Window parentWindow = SwingUtilities.getWindowAncestor(editor);
        EditActionScripletDialog dialog = new EditActionScripletDialog(parentWindow, scriplet);
        dialog.setLocationRelativeTo(editor);
        dialog.setVisible(true);
        editor.refresh();
      }
    });
    if (btnMoveLeft != null) {
      btnMoveLeft.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          scriplet.getParent().moveLeft(scriplet);
          editor.refresh();
        }
      });
    }
    if (btnMoveRight != null) {
      btnMoveRight.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          scriplet.getParent().moveRight(scriplet);
          editor.refresh();
        }
      });
    }
    if (btnRemove != null) {
      btnRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          scriplet.getParent().remove(scriplet);
          editor.refresh();
        }
      });
    }
  }

}

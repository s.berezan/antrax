/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RegexesPanel extends JPanel {

  private static final long serialVersionUID = 1195494831936376261L;

  private class ResponceCheckerPane extends JPanel {
    private static final long serialVersionUID = 7375725146428155586L;
    private final JButton btn;
    private final JComboBox editor;

    public ResponceCheckerPane(final String[] items, final boolean add, final int width) {
      this.btn = new JButton();
      this.editor = new JComboBox(new DefaultComboBoxModel(items));
      editor.setEditable(true);
      editor.setPreferredSize(new Dimension(width, editor.getPreferredSize().height));
      add(editor);
      JToolBar toolbar = new JToolBar();
      toolbar.setFloatable(false);
      toolbar.add(btn);
      add(toolbar);

      if (add) {
        btn.setText("+");
      } else {
        btn.setText("-");
      }

      btn.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          if (add) {
            addResponceChecker(null, items);
          } else {
            removeResponceChecker(ResponceCheckerPane.this);
          }

        }
      });
    }

    public String getRegex() {
      return (String) editor.getSelectedItem();
    }

    public void setRegex(final String regex) {
      editor.setSelectedItem(regex);
    }
  }

  private final int editorWidth;

  public RegexesPanel(final int width) {
    this.editorWidth = width;
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    addResponceChecker(null, new String[0]);
  }

  public void addResponceChecker(final String selected, final String[] regexes) {
    ResponceCheckerPane pane = new ResponceCheckerPane(regexes, getComponentCount() == 0, editorWidth);
    add(pane);
    pane.setRegex(selected);
    changeSize();
  }

  public void removeResponceChecker(final ResponceCheckerPane pane) {
    remove(pane);
    changeSize();
  }

  private void changeSize() {
    Window window = SwingUtilities.getWindowAncestor(this);
    if (window != null) {
      window.pack();
    }
  }

  public String[] listRegexes() {
    String[] retval = new String[getComponentCount()];
    for (int i = 0; i < retval.length; ++i) {
      retval[i] = ((ResponceCheckerPane) getComponent(i)).getRegex();
    }
    return retval;
  }

  public void setRegexes(final String[] selected, final String[] allValues) {
    removeAll();
    for (String s : selected) {
      addResponceChecker(s, allValues);
    }
  }

}

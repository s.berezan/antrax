/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.editors.IntervalEditor;
import com.flamesgroup.antrax.scripts.utils.LimitCallDurationPerMonthScriplet;

import java.awt.*;

import javax.swing.*;

public class LimitCallDurationPerMonthScripletPanel extends JPanel {

  private static final long serialVersionUID = -2025949380917900151L;

  private final JSpinner startDayMonth;
  private final IntervalEditor timeout;

  public LimitCallDurationPerMonthScripletPanel() {
    setOpaque(false);
    setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

    startDayMonth = new JSpinner(new SpinnerNumberModel(1, 1, 31, 1));
    startDayMonth.setToolTipText("startDayMonth");
    timeout = new IntervalEditor();
    ((JComponent) timeout.getEditorComponent()).setToolTipText("activity timeout period");

    add(startDayMonth);
    add(new JLabel("; "));
    add(timeout.getEditorComponent());
  }

  private Integer getStartDayMonth() {
    return (Integer) startDayMonth.getValue();
  }

  public LimitCallDurationPerMonthScriplet getActivityTimeoutScriplet() {
    return new LimitCallDurationPerMonthScriplet(getStartDayMonth(), timeout.getValue());
  }

  public void setActivityTimeoutScriplet(final LimitCallDurationPerMonthScriplet limitCallDurationPerMonthScriplet) {
    startDayMonth.setValue(limitCallDurationPerMonthScriplet.getStartDayOfMonth());
    timeout.setValue(limitCallDurationPerMonthScriplet.getTimeout());
  }

}

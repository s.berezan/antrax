/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerActivity;
import com.flamesgroup.antrax.predictions.CallDurationPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public class LimitCallDurationPerActivity extends BaseLimitCharacteristicPerActivity {

  private static final long serialVersionUID = -9178230205465817843L;

  public void setCallDurationLimit(final TimeInterval callDurationLimit) {
    setLimitValue(new VariableLong(callDurationLimit.getMin().getPeriod(), callDurationLimit.getMax().getPeriod()));
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    return simData.getCallDuration();
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new CallDurationPrediction((int) value);
  }

}
